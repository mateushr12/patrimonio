 <nav class="navbar-sidebar2">


            <ul class="list-unstyled navbar__list">
                <li><a href="/paginainicial"><i class="fas fa-shopping-basket"></i>Home</a></li>

                <li class="active has-sub">
                    <a class="js-arrow" href="#"><i class="fas fa-tachometer-alt"></i>ERP<span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li class="active has-sub">
                            <a class="js-arrow" href="#"><i class="fas fa-tachometer-alt"></i>Gestão e Controle<span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Fornecedores</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Contas a Pagar</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Contas a Receber</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Controle Orçamentário</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Suprimento de Fundos</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Gestão de Contratos</a></li>
                            </ul>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="#"><i class="fas fa-tachometer-alt"></i>Controle Patrimonial<span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Material de Consumo</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Imobilizado</a></li>
                            </ul>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="#"><i class="fas fa-tachometer-alt"></i>Recursos Humanos<span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Fornecedores</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Contas a Pagar</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Contas a Receber</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Controle Orçamentário</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Suprimento de Fundos</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Gestão de Contratos</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="active has-sub">
                    <a class="js-arrow" href="#"><i class="fas fa-tachometer-alt"></i>Consultas<span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li class="active has-sub">
                            <a class="js-arrow" href="#"><i class="fas fa-tachometer-alt"></i>Gestão e Controle<span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Fornecedores</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Contas a Pagar</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Contas a Receber</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Controle Orçamentário</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Suprimento de Fundos</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Gestão de Contratos</a></li>
                            </ul>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="#"><i class="fas fa-tachometer-alt"></i>Controle Patrimonial<span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Material de Consumo</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Imobilizado</a></li>
                            </ul>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="#"><i class="fas fa-tachometer-alt"></i>Recursos Humanos<span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Fornecedores</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Contas a Pagar</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Contas a Receber</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Controle Orçamentário</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Suprimento de Fundos</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Gestão de Contratos</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="active has-sub">
                    <a class="js-arrow" href="#"><i class="fas fa-tachometer-alt"></i>Tabelas Auxiliares<span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li class="active has-sub">
                            <a class="js-arrow" href="#"><i class="fas fa-tachometer-alt"></i>Gestão e Controle<span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Fornecedores</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Contas a Pagar</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Contas a Receber</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Controle Orçamentário</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Suprimento de Fundos</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Gestão de Contratos</a></li>
                            </ul>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="#"><i class="fas fa-tachometer-alt"></i>Controle Patrimonial<span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Material de Consumo</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Imobilizado</a></li>
                            </ul>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="#"><i class="fas fa-tachometer-alt"></i>Recursos Humanos<span class="arrow"><i class="fas fa-angle-down"></i></span></a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Fornecedores</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Contas a Pagar</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Contas a Receber</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Controle Orçamentário</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Suprimento de Fundos</a></li>
                                <li><a href="index.html"><i class="fas fa-tachometer-alt"></i>Gestão de Contratos</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li><a href="inbox.html"><i class="fas fa-chart-bar"></i>Caixa de Mensagens</a><span class="inbox-num">3</span></li>

                <li><a href="inbox.html"><i class="fas fa-chart-bar"></i>Mudar Senha</a></li>

                <li><a href="inbox.html"><i class="fas fa-chart-bar"></i>Instruções</a></li>

            </ul>

        </nav>