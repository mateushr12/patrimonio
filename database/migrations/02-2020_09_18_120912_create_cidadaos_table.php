<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCidadaosTable extends Migration
{
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::connection('mysqlCallcenter')->create('cidadaos', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('cpf', 11)->nullable(false)->unique();
            $table->string('nome', 255)->nullable(false);
            $table->string('fone_fixo', 14)->nullable(true);
            $table->string('fone_celular', 15)->nullable(true);
            $table->string('fone_celular2', 15)->nullable(true);
            $table->string('email', 255)->nullable(true);
            $table->boolean('receber_informativo')->default(0);

            $table->timestamps();

            $table->unsignedBigInteger('tipocidadao_id');
            $table->unsignedBigInteger('user_id');

            $table->foreign('tipocidadao_id')->references('id')->on('tipocidadaos');
        });
        Schema::enableForeignKeyConstraints();
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('cidadaos');
        Schema::enableForeignKeyConstraints();
    }
}
