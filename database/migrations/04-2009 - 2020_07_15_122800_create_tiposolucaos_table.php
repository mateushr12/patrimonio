<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiposolucaosTable extends Migration
{
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::connection('mysqlCallcenter')->create('tiposolucaos', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('descricao', 255)->nullable(false)->unique();

            $table->timestamps();

            $table->unsignedBigInteger('user_id');
        });
        Schema::enableForeignKeyConstraints();
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('tiposolucaos');
        Schema::enableForeignKeyConstraints();
    }
}
