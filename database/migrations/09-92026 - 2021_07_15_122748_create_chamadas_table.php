<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamadasTable extends Migration
{
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::connection('mysqlCallcenter')->create('chamadas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('protocolo', 20)->unique()->nullable(false);
            $table->dateTime('data_hora_inicio')->nullable(false);
            $table->dateTime('data_hora_fim')->nullable(false);
            $table->string('complemento_problema', 255)->nullable();
            $table->string('complemento_solucao', 255)->nullable();
            $table->string('id_agiliza', 13)->nullable();
            $table->string('id_chamada_ref_red', 20)->nullable(); // chamada da qual foi redirecionada
            $table->dateTime('data_hora_red_finalizado')->nullable(); // data/hora da finalização do atendimento redirecionado

            $table->timestamps();

            $table->unsignedBigInteger('gruposuser_id_red')->nullable(); // grupo a ser redirecionado a chamada
            $table->unsignedBigInteger('user_id_red')->nullable();  // usuario a ser redirecionado a chamada
            $table->unsignedBigInteger('user_id_atendeu_red')->nullable();
            $table->unsignedBigInteger('tipoproblema_id')->nullable();
            $table->unsignedBigInteger('tiposolucao_id')->nullable();
            $table->unsignedBigInteger('tipoatendimento_id')->nullable();
            $table->unsignedBigInteger('cidadao_id')->nullable();
            $table->unsignedBigInteger('gruposuser_id')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('tipocidadao_id');

            $table->foreign('tipocidadao_id')->references('id')->on('tipocidadaos');
            $table->foreign('tipoproblema_id')->references('id')->on('tipoproblemas');
            $table->foreign('tiposolucao_id')->references('id')->on('tiposolucaos');
            $table->foreign('tipoatendimento_id')->references('id')->on('tipoatendimentos');
            $table->foreign('cidadao_id')->references('id')->on('cidadaos');
        });
        Schema::enableForeignKeyConstraints();
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('chamadas');
        Schema::enableForeignKeyConstraints();
    }
}
