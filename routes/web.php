<?php

//LOGIN
Route::get('/', "UsersController@index")->name('login');
Route::post('/login', "UsersController@entrar");

//SAIR
Route::get('/sair', function () {
  Auth::logout();
  return redirect()->route('login');
})->name('sair');

Route::get('/unauthorized', function () {
  return view('erp.unauthorized');
});

Route::get('/naocadastrado', function () {
  return view('erp.naocadastrada');
});

Route::group(['middleware' => ['auth']], function () {
  //CONTAS USUARIO
  Route::get('/contausuario', "UsersController@conta");
  Route::post('/contausuario/atualizar', "UsersController@updateconta")->name('contausuario.update');
  Route::get('/contausuario/alterarSenha', "UsersController@alterarSenha")->name('contausuario.editar');
  Route::post('/contausuario/alterarSenha', "UsersController@updateSenha")->name('contausuario.updatesenha');

  // CRIAÇÃO E EDIÇÃO DOS USUARIOS
  Route::get('/usuarios/listar', "UsersController@all")->name('usuarios.listar');
  Route::get('/usuarios/cadastrar', "UsersController@create")->name('usuarios.cadastrar');
  Route::post('/usuarios/cadastrar', "UsersController@store")->name('usuarios.salvar');
  Route::get('/usuarios/editar/{id}', "UsersController@edit")->name('usuarios.editar');
  Route::post('/usuarios/editar/{id}', "UsersController@update")->name('usuarios.atualizar');
  Route::get('/usuarios/alterarSenha/{id}', "UsersController@alterar")->name('usuarios.alterar');
  Route::get('/usuarios/inativarAtivar/{id}', "UsersController@inativarAtivar")->name('usuarios.inativarAtivar');

  //GRUPOS
  Route::get('/autorizacao/grupos', "AutorizacaoController@listarGrupo")->name('grupoUsuario');
  Route::get('/autorizacao/grupos/excluir/{id}', "AutorizacaoController@excluirGrupo")->name('grupoUsuario.excluir');
  Route::post('/autorizacao/grupos/salvar', "AutorizacaoController@cadastrarGrupo")->name('grupoUsuario.salvar');
  //PERMISSÕES DOS GRUPOS
  Route::get('/autorizacao/grupos/permissoes/inserir/{id}', "AutorizacaoController@listarPermissoesGrupo")->name('grupo.permissoes.inserir');
  Route::get('/autorizacao/grupos/permissoes/excluir/{id}', "AutorizacaoController@excluirPermissoesGrupo")->name('grupo.permissoes.excluir');
  Route::post('/autorizacao/grupos/permissoes/salvar', "AutorizacaoController@cadastrarPermissoesGrupo")->name('grupo.permissoes.salvar');
  //PERMISSÕES
  Route::get('/autorizacao/permissao', "AutorizacaoController@listarPermissao")->name('permissao');
  Route::get('/autorizacao/permissao/excluir/{id}', "AutorizacaoController@excluirPermissao")->name('permissao.excluir');
  Route::post('/autorizacao/permissao/salvar', "AutorizacaoController@cadastrarPermissao")->name('permissao.salvar');

  ///////////////////////////GRUPO ROTAS ERP_JUCESE - MODULO FINANCEIRO///////////////////////////////////////////////////////////
  Route::namespace('ERP')->group(function () {

    //
    Route::get( '/erp/contratos/anexar/{id}' , 'ContratoController@anexar')->name('contratos.anexar');
    Route::get( '/erp/contratos/anexar/pdf/{id}' , 'ContratoController@pdf')->name('contratos.pdf');
    Route::get( '/erp/contratos/anexar/deletepdf/{id}' , 'ContratoController@deletepdf')->name('contratos.pdf.delete');
    //não usado **************************************************************************
    Route::get( '/erp/cota/acao/{id}' , 'CotaMensalController@valor')->name('cota.valor');
    //************************************************************************************
    Route::get( '/erp/empenho/pdf/{id}' , 'EmpenhoController@pdf')->name('empenho.pdf');

    Route::get( '/erp/empenho/elemento/{id}' , 'EmpenhoController@valor')->name('empenho.valor');
    Route::get( '/erp/empenho/ficha/{id}' , 'EmpenhoController@ficha')->name('empenho.ficha');

    //-----------------RELATORIOS ///////////////////////////////////////////////////////////////////////////////////////
    Route::get('/erp/relatorio/acao', "AcoesController@inicioRelatorio")->name('acao.relatorio.buscar');
    Route::post('/erp/relatorio/acao', "AcoesController@resultRelatorio")->name('acao.relatorio.resultado');

    Route::get('/erp/relatorio/contrato', "ContratoController@inicioRelatorio")->name('contrato.relatorio.buscar');
    Route::post('/erp/relatorio/contrato', "ContratoController@resultRelatorio")->name('contrato.relatorio.resultado');

    Route::get('/erp/relatorio/pagamento', "PagamentoController@inicioRelatorio")->name('pagamento.relatorio.buscar');
    Route::post('/erp/relatorio/pagamento', "PagamentoController@resultRelatorio")->name('pagamento.relatorio.resultado');
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////// CONTAS A PAGAR /////////////////////////////////////////////////////////////////////////////////
    Route::get('/porgrupo', "EmpenhoController@porgrupo")->name('contasporgrupo');
    Route::post('/porgrupo', "EmpenhoController@porgrupoResult")->name('porgrupo');
    Route::get('/imprimirporgrupo', "EmpenhoController@imprimirPorgrupo")->name('imprimirporgrupo');
    Route::get('/porelemento', "EmpenhoController@porelemento")->name('contasporelemento');;
    Route::post('/porelemento', "EmpenhoController@porelementoResult")->name('porelemento');
    Route::get('/imprimirporelemento', "EmpenhoController@imprimirPorelem")->name('imprimirporelem');
    //-------------------//////////////////////////////////////////////////////////////////////////////////////////////////

    //não usado**********************************************************************************************************
    Route::get( '/erp/empenho/retencao/pagar/{id}' , 'PagamentoController@pagamentoret')->name('retencao.pagamento');
    Route::post( '/erp/empenho/retencao/pagar/{id}' , 'PagamentoController@pagarret')->name('retencao.pagar');
    //*******************************************************************************************************************

    Route::post( '/erp/contratos/anexar/salvar' , 'ContratoController@anexarSalvar')->name('contratos.anexar.salvar');

    //ver orçamento depois mover para auth
    Route::get('/erp/orcamentoinicial/{id}/ver', 'OrcamentoInicialController@ver')->name('orcamentoinicial.ver');

    Route::get('/erp/taxas/serasa' , 'TaxasController@novaSerasa')->name('taxas.inicio.serasa');
    Route::post('/erp/taxas/serasa' , 'TaxasController@salvarSerasa')->name('taxas.serasa');

    Route::get('/erp/taxas/serasaBoleto' , 'TaxasController@serasaBoleto')->name('taxas.serasa.boleto');
    Route::post('/erp/taxas/serasaBuscar' , 'TaxasController@serasaBuscar')->name('taxas.serasa.buscar');
    Route::put('/erp/taxas/serasaAtualizar' , 'TaxasController@serasaAtualizar')->name('taxas.serasa.atualizar');

    //Aditivo do contrato
    Route::get('/erp/contratos/aditivo/{id}', 'ContratoAditivoController@index')->name('aditivo.index');
    Route::post( '/erp/contratos/aditivo/salvar' , 'ContratoAditivoController@salvar')->name('aditivo.salvar');
    Route::get( '/erp/contratos/aditivo/deletar/{id}' , 'ContratoAditivoController@deletar')->name('aditivo.deletar');
    Route::get( '/erp/contratos/aditivo/anexar/{id}' , 'ContratoAditivoController@anexar')->name('aditivo.anexar');
    Route::post( '/erp/contratos/aditivo/anexar/salvar' , 'ContratoAditivoController@anexarSalvar')->name('aditivo.anexar.salvar');
    Route::get( '/erp/contratos/aditivo/anexar/pdf/{id}' , 'ContratoAditivoController@pdf')->name('aditivo.pdf');
    Route::get( '/erp/contratos/aditivo/anexar/deletepdf/{id}' , 'ContratoAditivoController@deletepdf')->name('aditivo.pdf.delete');
    //----------------------------------------------------------------------------------------------------------------

    //AÇÕES
    Route::get('/erp/acoes', ['as' => 'erp.acoes', 'uses' => 'AcoesController@index']);
    Route::post('/erp/acoes/salvar', ['as' => 'erp.acoes.salvar', 'uses' => 'AcoesController@salvar']);
    Route::get('/erp/acoes/editar/{id}', ['as' => 'erp.acoes.editar', 'uses' => 'AcoesController@editar']);
    Route::put('/erp/acoes/editar/{id}', ['as' => 'erp.acoes.atualizar', 'uses' => 'AcoesController@atualizar']);
    Route::get('/erp/acoes/deletar/{id}', ['as' => 'erp.acoes.deletar', 'uses' => 'AcoesController@deletar']);
    Route::get('/erp/acoes/empenhos/{id}', ['as' => 'erp.acoes.empenhos', 'uses' => 'AcoesController@empenhos']);

    //ORÇAMENTO INICIAL
    Route::get('/erp/orcamentoinicial', 'OrcamentoInicialController@index')->name('orcamentoinicial.index');
    Route::post('/erp/orcamentoinicial', 'OrcamentoInicialController@store')->name('orcamentoinicial.store');
    Route::get('/erp/orcamentoinicial/{id}/edit', 'OrcamentoInicialController@edit')->name('orcamentoinicial.edit');
    Route::put('/erp/orcamentoinicial/{id}/edit', 'OrcamentoInicialController@update')->name('orcamentoinicial.update');

    //SUPLEMENTAR
    Route::get('/erp/orcamentosuplementar', 'OrcamentoSuplementarController@index')->name('orcamentosuplementar.index');
    Route::post('/erp/orcamentosuplementar', 'OrcamentoSuplementarController@store')->name('orcamentosuplementar.store');
    Route::get('/erp/orcamentosuplementar/{id}/edit', 'OrcamentoSuplementarController@edit')->name('orcamentosuplementar.edit');
    Route::put('/erp/orcamentosuplementar/{id}/edit', 'OrcamentoSuplementarController@update')->name('orcamentosuplementar.update');
    Route::get('/erp/orcamentosuplementar/{id}/delete', 'OrcamentoSuplementarController@destroy')->name('orcamentosuplementar.delete');

    //REMANEJAMENTO
    Route::get('/erp/remanejamento', 'RemanejamentoController@index')->name('remanejamento.index');
    Route::post('/erp/remanejamento', 'RemanejamentoController@store')->name('remanejamento.store');
    Route::get('/erp/remanejamento/{id}/edit', 'RemanejamentoController@edit')->name('remanejamento.edit');
    Route::put('/erp/remanejamento/{id}/edit', 'RemanejamentoController@update')->name('remanejamento.update');

    //CONTRATOS
    Route::post('/erp/contratos/salvar', 'ContratoController@salvar')->name('contratos.salvar');
    Route::put( '/erp/contratos/editar/{id}', 'ContratoController@atualizar')->name('contratos.atualizar');
    Route::get( '/erp/contratos/editar/{id}', 'ContratoController@editar')->name('contratos.editar');
    Route::get( '/erp/contratos', 'ContratoController@index')->name('contratos.index');
    Route::get( '/erp/contratos/deletar/{id}' , 'ContratoController@deletar')->name('contratos.deletar');
    Route::get( '/erp/contratos/ver/{id}' , 'ContratoController@ver')->name('contratos.ver');
    Route::get( '/erp/contratos/encerrados', 'ContratoController@encerrados')->name('contratos.encerrados');

    //Fornecedor
    Route::get( '/erp/fornecedor' , 'FornecedorController@index')->name('fornecedor.index');
    Route::post('/erp/fornecedor/salvar' , 'FornecedorController@salvar')->name('fornecedor.salvar');
    Route::get( '/erp/fornecedor/editar/{id}' , 'FornecedorController@editar')->name('fornecedor.editar');
    Route::put( '/erp/fornecedor/editar/{id}' , 'FornecedorController@atualizar')->name('fornecedor.atualizar');
    Route::get( '/erp/fornecedor/ver/{id}' , 'FornecedorController@ver')->name('fornecedor.ver');

    //COTA MENSAL
    Route::get( '/erp/cota' , 'CotaMensalController@index')->name('cota.index');
    Route::post( '/erp/cota/salvar' , 'CotaMensalController@salvar')->name('cota.salvar');
    Route::get( '/erp/cota/editar/{id}' , 'CotaMensalController@editar')->name('cota.editar');
    Route::put( '/erp/cota/editar/{id}' , 'CotaMensalController@atualizar')->name('cota.atualizar');
    Route::get( '/erp/cota/deletar/{id}' , 'CotaMensalController@deletar')->name('cota.deletar');

    //EMPENHO
    Route::get( '/erp/empenho' , 'EmpenhoController@index')->name('empenho.index');
    Route::post( '/erp/empenho/salvar' , 'EmpenhoController@salvar')->name('empenho.salvar');
    Route::get( '/erp/empenho/editar/{id}' , 'EmpenhoController@editar')->name('empenho.editar');
    Route::put( '/erp/empenho/editar/{id}' , 'EmpenhoController@atualizar')->name('empenho.atualizar');
    Route::get( '/erp/empenho/deletar/{id}' , 'EmpenhoController@deletar')->name('empenho.deletar');
    Route::get( '/erp/empenho/reforco/{id}' , 'EmpenhoController@reforco')->name('empenho.reforco');
    Route::post( '/erp/empenho/newreforco/{id}' , 'EmpenhoController@newreforco')->name('empenho.newreforco');

    //liquidacao *nao usado****************************************************************************************
    Route::get( '/erp/empenho/liquidar/{id}' , 'LiquidarController@liquidar')->name('liquidar');
    Route::post( '/erp/empenho/liquidar/salvar/{id}' , 'LiquidarController@liquidarsalvar')->name('liquidar.salvar');
    Route::get( '/erp/empenho/liquidar/deletar/{id}' , 'LiquidarController@deletar')->name('liquidar.deletar');
    //*************************************************************************************************************

    //PAGAMENTO
    Route::get( '/erp/empenho/pagamento/{id}' , 'PagamentoController@pagamento')->name('pagamento');
    Route::post( '/erp/empenho/pagamento/{id}' , 'PagamentoController@salvarpagamento')->name('pagamento.salvar');
    Route::get( '/erp/empenho/pagamento/deletar/{id}' , 'PagamentoController@deletarpag')->name('pagamento.deletar');

    //RETENÇÃO *nao usado**********************************************************************************************
    Route::get( '/erp/empenho/retencao/{id}' , 'PagamentoController@retencao')->name('retencao');
    Route::post( '/erp/empenho/retencao/{id}' , 'PagamentoController@salvarretencao')->name('retencao.salvar');
    Route::get( '/erp/empenho/retencao/deletar/{id}' , 'PagamentoController@deletarret')->name('retencao.deletar');
    //****************************************************************************************************************

    //CADASTRO
    Route::get('/erp/cadastro/elemento', "CadastroController@indexEle")->name('elemento');
    Route::post('/erp/cadastro/elemento/salvar', "CadastroController@salvarEle")->name('elemento.salvar');
    Route::get( '/erp/cadastro/elemento/deletar/{id}' , 'CadastroController@deletarEle')->name('elemento.deletar');

    Route::get('/erp/cadastro/subelemento/{id}', "CadastroController@indexSubele")->name('subelemento');
    Route::post('/erp/cadastro/subelemento/salvar', "CadastroController@salvarSubele")->name('subelemento.salvar');
    Route::get( '/erp/cadastro/subelemento/deletar/{id}' , 'CadastroController@deletarSubele')->name('subelemento.deletar');

    Route::get('/erp/cadastro/modalidadelic', "CadastroController@indexMod")->name('modalidadelic');
    Route::post('/erp/cadastro/modalidadelic/salvar', "CadastroController@salvarMod")->name('modalidadelic.salvar');
    Route::get( '/erp/cadastro/modalidadelic/deletar/{id}' , 'CadastroController@deletarMod')->name('modalidadelic.deletar');

    Route::get('/erp/cadastro/reflegal', "CadastroController@indexRefLegal")->name('reflegal');
    Route::post('/erp/cadastro/reflegal/salvar', "CadastroController@salvarRefLegal")->name('reflegal.salvar');
    Route::get( '/erp/cadastro/reflegal/deletar/{id}' , 'CadastroController@deletarRefLegal')->name('reflegal.deletar');
    // não usado***********************************************************************************************************
    Route::get('/erp/cadastro/tpretencao', "CadastroController@indexTpRetencao")->name('tpretencao');
    Route::post('/erp/cadastro/tpretencao/salvar', "CadastroController@salvarTpRetencao")->name('tpretencao.salvar');
    Route::get( '/erp/cadastro/tpretencao/deletar/{id}' , 'CadastroController@deletarTpRetencao')->name('tpretencao.deletar');
    //*********************************************************************************************************************
    Route::get('/erp/cadastro/listCorrigir', "PagamentoController@listCorrigir")->name('listCorrigir');
    Route::put('/erp/cadastro/corrigirOb', "PagamentoController@corrigirObPost")->name('corrigirObPost');

    //TAXAS
    Route::get('/erp/taxas' , 'TaxasController@index')->name('taxas');
    Route::any('/erp/taxas/resultado' , 'TaxasController@resultado')->name('taxas.resultado');
    Route::get('/erp/taxas/detalhes/{id}' , 'TaxasController@detalhes')->name('taxas.detalhes');
    Route::get('/erp/taxas/sessao' , 'TaxasController@sessao')->name('taxas.sessao');
    Route::get('/erp/taxas/pdf', 'TaxasController@pdf')->name('taxas.pdf');

    Route::get( '/erp/empenho/{order}/{par}' , 'EmpenhoController@index')->name('empenho.ordenar');
    Route::get('/erp/taxas/deletar/{id}' , 'TaxasController@deletarserasa')->name('taxas.deletar');
    Route::get('/erp/empenho/showcotas', 'EmpenhoController@showcotas')->name('empenho.cota');

    ////////////////////////////// GRAFICOS ////////////////////////////////////////////////////////////////////////////////////
    Route::get('/erp/graficos/tipoevento', 'GraficosController@tipoevento')->name('graficos.eventos');
    Route::get('/erp/graficos/graficotipoevento', 'GraficosController@graficotipoevento')->name('graficos.eventos.resultado');

    Route::get('/erp/graficos/empenhobuscar', 'GraficosController@empenhobuscar')->name('graficos.empenho');
    Route::get('/erp/graficos/empenho', 'GraficosController@graficoempenho')->name('graficos.empenho.resultado');
    Route::get('/erp/graficos/empenhoAll', 'GraficosController@graficoempenhoAll')->name('graficos.empenho.all');

    Route::get('/erp/graficos/taxamesbuscar', 'GraficosController@taxasmes')->name('graficos.taxames');
    Route::get('/erp/graficos/taxasmes', 'GraficosController@graficotaxasmes')->name('graficos.taxasmes.resultado');

    Route::get('/erp/graficos/pagamentoForn', 'GraficosController@pagamentoForn')->name('graficos.pagForn');
    Route::get('/erp/graficos/graficopagamentoForn', 'GraficosController@graficopagamentoForn')->name('graficos.pagForn.resultado');
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //TAXAS RETORNO PAGAMENTO BANESE
    Route::any('/erp/taxasRetorno/resultadoretorno' , 'TaxaRetornosController@resultados')->name('taxasRetorno.resultadoretorno');
    Route::get('/erp/taxasRetorno/busca' , 'TaxaRetornosController@busca')->name('taxasRetorno.busca');
    Route::get('/erp/taxasRetorno/exibir' , 'TaxaRetornosController@exibir')->name('taxasRetorno.exibir');
    Route::get('/erp/taxasRetorno/retorno' , 'TaxaRetornosController@retorno')->name('taxasRetorno.retorno');
    Route::get('/erp/taxasRetorno/pocessaArquivoSPC/{id}', 'TaxaRetornosController@processaArquivoSPC')->name('taxasRetorno.ProcessaArquivoSPC');
    Route::get('/erp/taxasRetorno/pocessaArquivoCOB/{id}', 'TaxaRetornosController@processaArquivoCOB')->name('taxasRetorno.ProcessaArquivoCOB');
    Route::resource('/erp/taxasRetorno', 'TaxaRetornosController');

    Route::get('/erp/taxaRetorno/buscarpordia', 'TaxaRetornosController@buscarpordia')->name('taxasRetorno.buscarpordia');
    Route::post('/erp/taxaRetorno/resultadopordia', 'TaxaRetornosController@resultadopordia')->name('taxasRetorno.resultadopordia');

    //SUPRIMENTOS//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    Route::get('/erp/suprimentos', "SuprimentosController@index")->name('suprimento');
    Route::post('/erp/suprimentos', "SuprimentosController@salvar")->name('suprimento.salvar');
    Route::get('/erp/suprimentos/deletar/{id}', "SuprimentosController@deletar")->name('suprimento.deletar');
    Route::get('/erp/suprimentos/empenho/{id}', "SuprimentosController@empenho")->name('suprimento.empenho');
    Route::post( '/erp/suprimentos/salvarempenho/{id}' , 'SuprimentosController@salvarEmpenho')->name('suprimento.empenho.salvar');
    Route::get('/erp/suprimentos/imprimir/{id}', "SuprimentosController@imprimirSuprimento")->name('suprimento.imprimir');

    Route::get( '/erp/suprimentos/anexar/{id}' , 'SuprimentosController@anexar')->name('suprimento.anexar');
    Route::post( '/erp/suprimentos/anexarsalvar' , 'SuprimentosController@anexarSalvar')->name('suprimento.anexarsalvar');
    Route::get( '/erp/suprimentos/arquivo/{id}' , 'SuprimentosController@arquivo')->name('suprimento.arquivo');
    Route::get( '/erp/suprimentos/anexardelete/{id}' , 'SuprimentosController@deleteArquivo')->name('suprimento.anexardelete');
    //despesa
    Route::get('/erp/suprimentos/despesa/{id}', "SuprimentosController@despesa")->name('suprimento.despesa');
    Route::post( '/erp/suprimentos/salvardespesa/{id}' , 'SuprimentosController@salvarDespesa')->name('suprimento.despesa.salvar');
    Route::get('/erp/suprimentos/deletardespesa/{id}', "SuprimentosController@deletarDespesa")->name('suprimento.despesa.deletar');

    Route::get( '/erp/suprimentos/despesaanexar/{id}' , 'SuprimentosController@anexarDespesa')->name('suprimento.despesa.anexar');
    Route::post( '/erp/suprimentos/despesaanexar/salvar' , 'SuprimentosController@anexardepesaSalvar')->name('suprimento.despesa.anexarsalvar');
    Route::get( '/erp/suprimentos/despesaanexar/arquivo/{id}' , 'SuprimentosController@arquivoDespesa')->name('suprimento.despesa.arquivo');
    Route::get( '/erp/suprimentos/despesaanexar/delete/{id}' , 'SuprimentosController@deletearquivoDel')->name('suprimento.despesa.anexardelete');
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////// CAIXAS //////////////////////////////////////////////////////////////////
    Route::get('/erp/caixas', "CaixasController@caixas")->name('caixas');
    Route::post('/erp/caixas', "CaixasController@novaCaixa")->name('caixa.salvar');
    Route::get('/erp/caixas/deletar/{id}', "CaixasController@deletarCaixa")->name('caixa.deletar');
    Route::get('/erp/caixas/imprimirConteudo/{id}', "CaixasController@imprimirConteudo")->name('caixa.imprimir');

    Route::post('/erp/geraretiqueta', "CaixasController@geraretiqueta")->name('caixa.geraretiqueta');
    //processos
    Route::get('/erp/caixas/processo/{id}', "CaixasController@processos")->name('caixa.processo');
    Route::post( '/erp/caixas/salvarProcesso' , 'CaixasController@salvarProcesso')->name('caixa.processo.salvar');
    Route::get('/erp/caixas/deletarProcesso/{id}', "CaixasController@deletarProcesso")->name('caixa.processo.deletar');
    //buscar conteudo
    Route::get('/erp/caixas/buscar', "CaixasController@buscarProcesso")->name('buscar');
    //tipo
    Route::get('/erp/caixas/tipo', "CaixasController@tipo")->name('caixa.tipo');
    Route::post( '/erp/caixas/salvarTipo' , 'CaixasController@salvarTipo')->name('caixa.salvarTipo');
    Route::get( '/erp/caixas/deletarTipo/{id}' , 'CaixasController@deletarTipo')->name('caixa.deletarTipo');

    Route::get( '/erp/caixas/auto' , 'CaixasController@getAutocompleteData')->name('auto');

    Route::get( '/erp/caixas/anexar/{id}' , 'CaixasController@anexar')->name('caixa.anexar');
    Route::post( '/erp/caixas/anexarsalvar' , 'CaixasController@anexarSalvar')->name('caixa.anexarsalvar');
    Route::get( '/erp/caixas/arquivo/{id}' , 'CaixasController@arquivo')->name('caixa.arquivo');
    Route::get( '/erp/caixas/anexardelete/{id}' , 'CaixasController@deleteArquivo')->name('caixa.anexardelete');
    ////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////// LOG ////////////////////////////////////////////////
    Route::get('/erp/log', "LogController@index")->name('log');
    Route::post( '/erp/log/resultado' , 'LogController@resultado')->name('log.resultado');
    Route::post('/erp/log/tabela', "LogController@tabelas")->name('log.tabela');
    ////////////////////////////////////////////////////////////////////////////

    //////////////// CONTRATOS PRESIDENCIA /////////////////////////////////////////////////////////////////////
    Route::get( '/erp/contratosPres', 'ContratoPresiController@index')->name('contratosPres');
    Route::post('/erp/contratosPres/salvar', 'ContratoPresiController@salvar')->name('contratosPres.salvar');
    Route::get( '/erp/contratosPres/editar/{id}', 'ContratoPresiController@editar')->name('contratosPres.editar');
    Route::put( '/erp/contratosPres/atualizar/{id}', 'ContratoPresiController@atualizar')->name('contratosPres.atualizar');
    Route::get( '/erp/contratosPres/deletar/{id}' , 'ContratoPresiController@deletar')->name('contratosPres.deletar');
    Route::get( '/erp/contratosPres/ver/{id}' , 'ContratoPresiController@ver')->name('contratosPres.ver');
    Route::get( '/erp/contratosPres/encerrados', 'ContratoPresiController@encerrados')->name('contratosPres.encerrados');
    //---Tipos de termo
    Route::get( '/erp/contratosPres/termo', 'ContratoPresiController@termoIndex')->name('termo');
    Route::post('/erp/contratosPres/termo/salvar', 'ContratoPresiController@termoSalvar')->name('termo.salvar');
    Route::get( '/erp/contratosPres/termo/deletar/{id}' , 'ContratoPresiController@termoDeletar')->name('termo.deletar');
    //-- Aditivo
    Route::get( '/erp/contratosPres/aditivo/{id}', 'ContratoPresiController@aditivoIndex')->name('contratoPres.aditivo');
    Route::post('/erp/contratosPres/aditivo/salvar', 'ContratoPresiController@aditivoSalvar')->name('contratoPres.aditivo.salvar');
    Route::get( '/erp/contratosPres/aditivo/deletar/{id}' , 'ContratoPresiController@aditivoDeletar')->name('contratoPres.aditivo.deletar');
    //--Anexo
    Route::get( '/erp/contratosPres/anexar/{id}' , 'ContratoPresiController@anexar')->name('contratoPres.anexar');
    Route::post( '/erp/contratosPres/anexar/salvar' , 'ContratoPresiController@anexarSalvar')->name('contratoPres.anexar.salvar');
    Route::get( '/erp/contratosPres/anexar/pdf/{id}' , 'ContratoPresiController@pdf')->name('contratoPres.pdf');
    Route::get( '/erp/contratosPres/anexar/deletepdf/{id}' , 'ContratoPresiController@deletepdf')->name('contratoPres.pdf.delete');

    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    //NOTAS (TAXAS)
    //Route::get('/erp/taxas/resultados', "NotasController@resultado");
    //Route::get('/erp/taxas', "NotasController@inicio");
    //Route::get('/erp/taxas/detalhe/{id}', "NotasController@detalhe");
  });
  /////////////////////////////////////////////////////////////////////////////////////////


    /////////////////////////////// MÓDULO CALLCENTER
    Route::namespace('Callcenter')->group(function () {
      /////////// CHAMADA
        //                                                                  classe                  metodo         apelido
        Route::get('/callcenter/chamada/acoes',                            'Chamada\AcoesController@index')->name('chamada.index');
        Route::get('/callcenter/chamada/acoes/pesquisarcpf',               'Chamada\AcoesController@pesquisarcpf')->name('chamada.pesquisarcpf');
        Route::get('/callcenter/chamada/acoes/cadastroredirecionado/{id}', 'Chamada\AcoesController@cadastroredirecionado')->name('chamada.cadastroredirecionado');
        Route::post('/callcenter/chamada/acoes/salvar',                    'Chamada\AcoesController@salvar')->name('chamada.salvar');
        Route::post('/callcenter/chamada/acoes/salvarredirecionado',       'Chamada\AcoesController@salvarredirecionado')->name('chamada.salvarredirecionado');
        Route::get('/callcenter/chamada/acoes/editar/{id}',                'Chamada\AcoesController@editar')->name('chamada.editar');
        Route::get('/callcenter/chamada/acoes/adicionar/{id}',             'Chamada\AcoesController@adicionar')->name('chamada.adicionar');
        Route::put('/callcenter/chamada/acoes/atualizar/{id}',             'Chamada\AcoesController@atualizar')->name('chamada.atualizar');
        Route::get('/callcenter/chamada/acoes/deletar/{id}',               'Chamada\AcoesController@deletar')->name('chamada.deletar');
      /////////// TIPO DE PROBLEMA
        Route::get('/callcenter/tipoproblema/acoes',                'Tipoproblema\AcoesController@index')->name('tipoproblema.index');
        Route::post('/callcenter/tipoproblema/acoes/salvar',        'Tipoproblema\AcoesController@salvar')->name('tipoproblema.salvar');
        Route::get('/callcenter/tipoproblema/acoes/editar/{id}',    'Tipoproblema\AcoesController@editar')->name('tipoproblema.editar');
        Route::put('/callcenter/tipoproblema/acoes/atualizar/{id}', 'Tipoproblema\AcoesController@atualizar')->name('tipoproblema.atualizar');
        Route::get('/callcenter/tipoproblema/acoes/deletar/{id}',   'Tipoproblema\AcoesController@deletar')->name('tipoproblema.deletar');
        Route::get('/callcenter/tipoproblema/acoes/imprimir',       'Tipoproblema\AcoesController@imprimir')->name('tipoproblema.imprimir');
      /////////// TIPO DE SOLUÇÃO
        Route::get('/callcenter/tiposolucao/acoes',                'Tiposolucao\AcoesController@index')->name('tiposolucao.index');
        Route::post('/callcenter/tiposolucao/acoes/salvar',        'Tiposolucao\AcoesController@salvar')->name('tiposolucao.salvar');
        Route::get('/callcenter/tiposolucao/acoes/editar/{id}',    'Tiposolucao\AcoesController@editar')->name('tiposolucao.editar');
        Route::put('/callcenter/tiposolucao/acoes/atualizar/{id}', 'Tiposolucao\AcoesController@atualizar')->name('tiposolucao.atualizar');
        Route::get('/callcenter/tiposolucao/acoes/deletar/{id}',   'Tiposolucao\AcoesController@deletar')->name('tiposolucao.deletar');
        Route::get('/callcenter/tiposolucao/acoes/imprimir',       'Tiposolucao\AcoesController@imprimir')->name('tiposolucao.imprimir');
      /////////// TIPO DE CIDADÃO
        Route::get('/callcenter/tipocidadao/acoes',                'Tipocidadao\AcoesController@index')->name('tipocidadao.index');
        Route::post('/callcenter/tipocidadao/acoes/salvar',        'Tipocidadao\AcoesController@salvar')->name('tipocidadao.salvar');
        Route::get('/callcenter/tipocidadao/acoes/editar/{id}',    'Tipocidadao\AcoesController@editar')->name('tipocidadao.editar');
        Route::put('/callcenter/tipocidadao/acoes/atualizar/{id}', 'Tipocidadao\AcoesController@atualizar')->name('tipocidadao.atualizar');
        Route::get('/callcenter/tipocidadao/acoes/deletar/{id}',   'Tipocidadao\AcoesController@deletar')->name('tipocidadao.deletar');
        Route::get('/callcenter/tipocidadao/acoes/imprimir',       'Tipocidadao\AcoesController@imprimir')->name('tipocidadao.imprimir');
      /////////// PALAVRA
      Route::get('/callcenter/palavra/acoes',                'Palavra\AcoesController@index')->name('palavra.index');
      Route::post('/callcenter/palavra/acoes/salvar',        'Palavra\AcoesController@salvar')->name('palavra.salvar');
      Route::get('/callcenter/palavra/acoes/editar/{id}',    'Palavra\AcoesController@editar')->name('palavra.editar');
      Route::put('/callcenter/palavra/acoes/atualizar/{id}', 'Palavra\AcoesController@atualizar')->name('palavra.atualizar');
      Route::get('/callcenter/palavra/acoes/deletar/{id}',   'Palavra\AcoesController@deletar')->name('palavra.deletar');
      Route::get('/callcenter/palavra/acoes/imprimir',       'Palavra\AcoesController@imprimir')->name('palavra.imprimir');
    /////////// CIDADÃO
        Route::get('/callcenter/cidadao/acoes',                'Cidadao\AcoesController@index')->name('cidadao.index');
        Route::post('/callcenter/cidadao/acoes/salvar',        'Cidadao\AcoesController@salvar')->name('cidadao.salvar');
        Route::get('/callcenter/cidadao/acoes/editar/{id}',    'Cidadao\AcoesController@editar')->name('cidadao.editar');
        Route::put('/callcenter/cidadao/acoes/atualizar/{id}', 'Cidadao\AcoesController@atualizar')->name('cidadao.atualizar');
        Route::get('/callcenter/cidadao/acoes/deletar/{id}',   'Cidadao\AcoesController@deletar')->name('cidadao.deletar');
        Route::get('/callcenter/cidadao/acoes/imprimir',       'Cidadao\AcoesController@imprimir')->name('cidadao.imprimir');
      /////////// TIPO DE ATENDIMENTO
        Route::get('/callcenter/tipoatendimento/acoes',                'Tipoatendimento\AcoesController@index')->name('tipoatendimento.index');
        Route::post('/callcenter/tipoatendimento/acoes/salvar',        'Tipoatendimento\AcoesController@salvar')->name('tipoatendimento.salvar');
        Route::get('/callcenter/tipoatendimento/acoes/editar/{id}',    'Tipoatendimento\AcoesController@editar')->name('tipoatendimento.editar');
        Route::put('/callcenter/tipoatendimento/acoes/atualizar/{id}', 'Tipoatendimento\AcoesController@atualizar')->name('tipoatendimento.atualizar');
        Route::get('/callcenter/tipoatendimento/acoes/deletar/{id}',   'Tipoatendimento\AcoesController@deletar')->name('tipoatendimento.deletar');
        Route::get('/callcenter/tipoatendimento/acoes/imprimir',       'Tipoatendimento\AcoesController@imprimir')->name('tipoatendimento.imprimir');
      /////////// CONSULTAS
        Route::get('/callcenter/consulta/chamadageral',                       'Consulta\AcoesController@acao_ctrl_chamadageral_parametros')->name('consulta.rota_chamadageral_parametros');
        Route::post('/callcenter/consulta/chamadageral/resultado',            'Consulta\AcoesController@acao_ctrl_chamadageral_resultado')->name('consulta.rota_chamadageral_resultado');
        Route::get('/callcenter/consulta/chamadageral/imprimir',              'Consulta\AcoesController@acao_ctrl_chamadageral_imprimir')->name('consulta.rota_chamadageral_imprimir');
        Route::get('/callcenter/consulta/problemasportipocidadao',            'Consulta\AcoesController@acao_ctrl_problemasportipocidadao_parametros')->name('consulta.rota_problemasportipocidadao_parametros');
        Route::post('/callcenter/consulta/problemasportipocidadao/resultado', 'Consulta\AcoesController@acao_ctrl_problemasportipocidadao_resultado')->name('consulta.rota_problemasportipocidadao_resultado');
        Route::get('/callcenter/consulta/problemasportipocidadao/imprimir',   'Consulta\AcoesController@acao_ctrl_problemasportipocidadao_imprimir')->name('consulta.rota_problemasportipocidadao_imprimir');
        Route::get('/callcenter/consulta/atendimentosporcpf',                 'Consulta\AcoesController@acao_ctrl_atendimentosporcpf_parametros')->name('consulta.rota_atendimentosporcpf_parametros');
        Route::post('/callcenter/consulta/atendimentosporcpf/resultado',      'Consulta\AcoesController@acao_ctrl_atendimentosporcpf_resultado')->name('consulta.rota_atendimentosporcpf_resultado');
        Route::get('/callcenter/consulta/atendimentosporcpf/imprimir',        'Consulta\AcoesController@acao_ctrl_atendimentosporcpf_imprimir')->name('consulta.rota_atendimentosporcpf_imprimir');
        Route::get('/callcenter/consulta/painelonline',                                              'Consulta\AcoesController@painel')->name('consulta.painelonline');
        Route::get('/callcenter/consulta/dashboard',                                                 'Consulta\AcoesController@dashboard')->name('consulta.dashboard');
        Route::get('/callcenter/consulta/cpfdistintoportipocidadao/cpfdistintoportipocidadao',       'Consulta\AcoesController@cpfdistintoportipocidadao')->name('consulta.cpfdistintoportipocidadao');
        Route::get('/callcenter/consulta/painelonline/imprimir_painelonline',                        'Consulta\AcoesController@imprimir_painelonline')->name('consulta.imprimir_painelonline');
        Route::get('/callcenter/consulta/produtividade',                                             'Consulta\AcoesController@produtividade_parametros')->name('consulta.produtividade');
        Route::post('/callcenter/consulta/produtividade',                                            'Consulta\AcoesController@produtividade_parametros_aplicar')->name('consulta.produtividadeparametrosaplicar');
        Route::get('/callcenter/consulta/produtividade/imprimir/imprimir_poranomes',                 'Consulta\AcoesController@imprimir_poranomes')->name('consulta.imprimir_poranomes');
        Route::get('/callcenter/consulta/produtividade/imprimir/imprimir_agilizaxcpf',               'Consulta\AcoesController@imprimir_agilizaxcpf')->name('consulta.imprimir_agilizaxcpf');
        Route::get('/callcenter/consulta/produtividade/imprimir/imprimir_clientescomchamadas',       'Consulta\AcoesController@imprimir_clientescomchamadas')->name('consulta.imprimir_clientescomchamadas');
        Route::get('/callcenter/consulta/produtividade/imprimir/imprimir_cpfdistintoportipocidadao', 'Consulta\AcoesController@imprimir_cpfdistintoportipocidadao')->name('consulta.imprimir_cpfdistintoportipocidadao');
        Route::get('/callcenter/consulta/produtividade/imprimir/imprimir_poratendente',              'Consulta\AcoesController@imprimir_poratendente')->name('consulta.imprimir_poratendente');
        Route::get('/callcenter/consulta/produtividade/imprimir/imprimir_pordataatendente',          'Consulta\AcoesController@imprimir_pordataatendente')->name('consulta.imprimir_pordataatendente');
        Route::get('/callcenter/consulta/produtividade/imprimir/imprimir_pordatahora',               'Consulta\AcoesController@imprimir_pordatahora')->name('consulta.imprimir_pordatahora');
        Route::get('/callcenter/consulta/produtividade/imprimir/imprimir_pordia',                    'Consulta\AcoesController@imprimir_pordia')->name('consulta.imprimir_pordia');
        Route::get('/callcenter/consulta/produtividade/imprimir/imprimir_porhora',                   'Consulta\AcoesController@imprimir_porhora')->name('consulta.imprimir_porhora');
        Route::get('/callcenter/consulta/produtividade/imprimir/imprimir_portipocidadao',            'Consulta\AcoesController@imprimir_portipocidadao')->name('consulta.imprimir_portipocidadao');
        Route::get('/callcenter/consulta/produtividade/imprimir/imprimir_portipoproblema',           'Consulta\AcoesController@imprimir_portipoproblema')->name('consulta.imprimir_portipoproblema');
        Route::get('/callcenter/consulta/produtividade/imprimir/imprimir_portiposolucao',            'Consulta\AcoesController@imprimir_portiposolucao')->name('consulta.imprimir_portiposolucao');
        Route::get('/callcenter/consulta/produtividade/imprimir/imprimir_problemascomocorrencias',   'Consulta\AcoesController@imprimir_problemascomocorrencias')->name('consulta.imprimir_problemascomocorrencias');
        Route::get('/callcenter/consulta/produtividade/imprimir/imprimir_solucoescomocorrencias',    'Consulta\AcoesController@imprimir_solucoescomocorrencias')->name('consulta.imprimir_solucoescomocorrencias');
    });
    /////////////////////////////// FINAL - MÓDULO CALLCENTER

    /////////////////////////////////// MODULO PATRIMONIO //////////////////////////////////////////////////////////////
    Route::namespace('Patrimonio')->group(function () {

    /////////////////////// ATENDIMENTO ////////////////////////////////////////////////

    Route::get('/erp/patrimonio/atendimentos', "AtendimentoController@index")->name('atendimentos');
    Route::get( '/erp/patrimonio/novoAtendimento' , 'AtendimentoController@novoAtendimento')->name('atendimentos.novo');
    Route::post( '/erp/patrimonio/buscaAtendimento' , 'AtendimentoController@buscaAtendimento')->name('atendimentos.busca');
    Route::post( '/erp/patrimonio/salvarAtendimento' , 'AtendimentoController@salvarAtendimento')->name('atendimentos.salvar');
    Route::get( '/erp/patrimonio/verAtendimento/{id}' , 'AtendimentoController@verAtendimento')->name('atendimentos.ver');
    Route::get( '/erp/patrimonio/atribuirAtendimento/{id}' , 'AtendimentoController@atribuirAtendimento')->name('atendimentos.atribuir');
    Route::get( '/erp/patrimonio/fecharAtendimento/{id}' , 'AtendimentoController@fecharAtendimento')->name('atendimentos.fechar');
    Route::put( '/erp/patrimonio/atualizarAtendimento/{id}', 'AtendimentoController@atualizarAtendimento')->name('atendimentos.atualizar');

    //PERMISSÕES DOS GRUPOS
    Route::get('/erp/patrimonio/gestor/inserir/{id}', "AtendimentoController@inserirGestor")->name('atendimentos.gestor.inserir');
    Route::get('/erp/patrimonio/gestor/excluir/{id}', "AtendimentoController@excluirGestor")->name('atendimentos.gestor.excluir');
    Route::post('/erp/patrimonio/gestor/salvar', "AtendimentoController@salvarGestor")->name('atendimentos.gestor.salvar');
    Route::get('/erp/patrimonio/grupos', "AtendimentoController@grupos")->name('atendimentos.grupos');

    ////////////////////////////////////////////////////////////////////////////

      // OUTROS CADASTROS /////////////////
      //---Setor----
      Route::get('/patrimonio/outrocadastro/setor', "OutrosCadastrosController@inicioSetor")->name('setor');
      Route::post('/patrimonio/outrocadastro/setor/salvar', "OutrosCadastrosController@salvarSetor")->name('setor.salvar');
      Route::get( '/patrimonio/outrocadastro/setor/deletar/{id}' , 'OutrosCadastrosController@deletarSetor')->name('setor.deletar');
      //------------
      //---Conservacao----
      Route::get('/patrimonio/outrocadastro/conservacao', "OutrosCadastrosController@inicioConservacao")->name('conservacao');
      Route::post('/patrimonio/outrocadastro/conservacao/salvar', "OutrosCadastrosController@salvarConservacao")->name('conservacao.salvar');
      Route::get( '/patrimonio/outrocadastro/conservacao/deletar/{id}' , 'OutrosCadastrosController@deletarConservacao')->name('conservacao.deletar');
      //------------------
      //---Proprietario----
      Route::get('/patrimonio/outrocadastro/proprietario', "OutrosCadastrosController@inicioProprietario")->name('proprietario');
      Route::post('/patrimonio/outrocadastro/proprietario/salvar', "OutrosCadastrosController@salvarProprietario")->name('proprietario.salvar');
      Route::get( '/patrimonio/outrocadastro/proprietario/deletar/{id}' , 'OutrosCadastrosController@deletarProprietario')->name('proprietario.deletar');
      //------------------
      //---TpMaterial----
      Route::get('/patrimonio/outrocadastro/tpMaterial', "OutrosCadastrosController@inicioTpMaterial")->name('tpMaterial');
      Route::post('/patrimonio/outrocadastro/tpMaterial/salvar', "OutrosCadastrosController@salvarTpMaterial")->name('tpMaterial.salvar');
      Route::get( '/patrimonio/outrocadastro/tpMaterial/deletar/{id}' , 'OutrosCadastrosController@deletarTpMaterial')->name('tpMaterial.deletar');
      //------------------
      /////////////////////////////////////

      // MATERIAL //////////////////////////////
      Route::get('/patrimonio/material', "MaterialController@index")->name('material');
      Route::post('/patrimonio/material/salvar', "MaterialController@salvar")->name('material.salvar');
      Route::get('/patrimonio/material/editar/{id}', "MaterialController@editar")->name('material.editar');
      Route::put( '/patrimonio/material/atualizar/{id}', 'MaterialController@atualizar')->name('material.atualizar');
      Route::get( '/patrimonio/material/deletar/{id}' , 'MaterialController@deletar')->name('material.deletar');
      Route::get('/patrimonio/material/ver/{id}', "MaterialController@ver")->name('material.ver');
      Route::get('/patrimonio/material/etiqueta', "MaterialController@etiqueta")->name('material.etiqueta');
      Route::post('/patrimonio/material/geraretiqueta', "MaterialController@geraretiqueta")->name('material.geraretiqueta');
      Route::get('/patrimonio/material/devolver/{id}', "MaterialController@devolver")->name('material.devolver');
      //--Componente-----
      Route::get('/patrimonio/componente/{id}', "ComponenteController@index")->name('componente');
      Route::post('/patrimonio/componente/salvar', "ComponenteController@salvar")->name('componente.salvar');
      //-Relatório-------
      Route::get('/patrimonio/material/relatorio/filtro', "MaterialController@filtro")->name('material.relatorio');
      Route::post('/patrimonio/material/relatorio/resultado', "MaterialController@resultadoRel")->name('material.relatorio.resultado');
      ////////////////////////////////////////////

      // MOVIMENTAÇÃO ///////////////////////////////////////////////////////////////
      Route::get('/patrimonio/movimentacao', "MovimentacaoController@index")->name('movimentacao');
      Route::post('/patrimonio/movimentacao/salvar', "MovimentacaoController@salvar")->name('movimentacao.salvar');
      Route::get( '/patrimonio/movimentacao/origem/{id}' , 'MovimentacaoController@origem')->name('movimentacao.origem');
      Route::get('/patrimonio/movimentacao/editar/{id}', "MovimentacaoController@editar")->name('movimentacao.editar');
      Route::put( '/patrimonio/movimentacao/atualizar/{id}', 'MovimentacaoController@atualizar')->name('movimentacao.atualizar');
      // Route::get( '/patrimonio/movimentacao/deletar/{id}' , 'MovimentacaoController@deletar')->name('movimentacao.deletar');
      Route::get('/patrimonio/movimentacao/ver/{id}', "MovimentacaoController@ver")->name('movimentacao.ver');
      //-Relatório-------
      Route::get('/patrimonio/movimentacao/relatorio/filtro', "MovimentacaoController@filtro")->name('movimentacao.relatorio');
      Route::post('/patrimonio/movimentacao/relatorio/resultado', "MovimentacaoController@resultadoRel")->name('movimentacao.relatorio.resultado');      


    });
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


});
