<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Route;
use App\User;
use App\Gruposuser;
use App\Permissao;
use App\Permissaogrupo;

class UsersController extends Controller
{
	public function all()
	{
        $users = User::all();
        //$users = User::where('ativo', 1)->get();
		return view('erp.usuarios.listar', compact('users') );
	}


	public function entrar(Request $r)
	{
		$r->validate([
			'email' => 'required|email',
			'password' => 'required'
    ]);

    $credenciais = ['email' => $r->email, 'password' => $r->password];

    if (Auth::attempt($credenciais)){
				//////////////////////////////////////////////////////////////////////////
				$grupo = Auth::user()->grupoid;
				$permGrupo = Permissao::join('permissao_grupo', function ($join) {
						$join->on('permissoes.id', '=', 'permissao_grupo.permissoes_id');
						})
						->where('permissao_grupo.grupouser_id', '=', $grupo)
						->select('permissoes.nome')->get();

				$nomes[] = null;

				foreach ($permGrupo as $key => $value) {
					$nomes[] = $value['nome'];
				}
				// Subsistema Financeiro
					$planejamento = array('erp/acoes','erp/orcamentoinicial','erp/orcamentosuplementar','erp/remanejamento');
					$execucao = array('erp/empenho','erp/empenho/showcotas','erp/suprimentos');
					$gestao = array('erp/contratos','erp/fornecedor');
					$contasareceber = array('erp/taxasRetorno','erp/taxasRetorno/exibir','erp/taxas','erp/taxas/serasa');
					$contaspagar = array('porgrupo', 'porelemento');
					$relatorios = array('erp/relatorio/acao','erp/relatorio/contrato','erp/relatorio/pagamento');
					$caixas = array('erp/caixas','erp/caixas/buscar','erp/caixas/tipo');
					$outroscadastros = array('erp/cadastro/elemento', 'erp/cadastro/modalidadelic', 'erp/cadastro/reflegal', 'erp/cadastro/listCorrigir');
					$graficos = array('erp/graficos/tipoevento');
				// Final Subsistema Financeiro

				// Subsistema Callcenter - somente itens de menu
					$callcenter_chamada = array(
										'callcenter/item_menu/chamada'
									);
					$callcenter_tabelas = array(
										'callcenter/item_menu/cidadao',
										'callcenter/item_menu/tipoatendimento',
										'callcenter/item_menu/tipocidadao',
										'callcenter/item_menu/tipoproblema',
										'callcenter/item_menu/tiposolucao'
									);
					$callcenter_consultas = array(
										'callcenter/item_menu/consulta/atendimentosporcpf',
										'callcenter/item_menu/consulta/cpfdistintoportipocidadao',
										'callcenter/item_menu/consulta/chamadageral',
										'callcenter/item_menu/consulta/problemasporcidadao',
										'callcenter/item_menu/consulta/produtividade',
										'callcenter/item_menu/consulta/dashboard',
										'callcenter/item_menu/consulta/painelonline'
									);
				// Final Subsistema Callcenter

				//Patrimonio
				$patrimonio = array('erp/patrimonio/atendimentos','erp/patrimonio/novoAtendimento','patrimonio/material','patrimonio/material/etiqueta','patrimonio/movimentacao');
				//Fim Patrimonio

				// Subsistema Financeiro
					$planejamento = count(array_intersect($nomes,$planejamento));
					$execucao = count(array_intersect($nomes,$execucao));
					$gestao = count(array_intersect($nomes,$gestao));
					$contasareceber = count(array_intersect($nomes,$contasareceber));
					$contaspagar = count(array_intersect($nomes,$contaspagar));
					$relatorios = count(array_intersect($nomes,$relatorios));
					$outroscadastros = count(array_intersect($nomes,$outroscadastros));
					$caixas = count(array_intersect($nomes,$caixas));
					$graficos = count(array_intersect($nomes,$graficos));
				// Final Subsistema Financeiro

				// Subsistema Callcenter
					$callcenter_chamada = count(array_intersect($nomes,$callcenter_chamada));
					$callcenter_tabelas = count(array_intersect($nomes,$callcenter_tabelas));
					$callcenter_consultas = count(array_intersect($nomes,$callcenter_consultas));
				// Final Subsistema Callcenter

				// Subsistema Patrimonio
				$patrimonio = count(array_intersect($nomes,$patrimonio));
				// Fim patrimonio

				\Session::put('permissoes', [
					'nomes' => $nomes,
					'plan' => $planejamento,
					'gestao' => $gestao,
					'exe' => $execucao,
					'contas' => $contasareceber,
					'contasp' => $contaspagar,
					'relatorio' => $relatorios,
					'outroscad' => $outroscadastros,
					'caixas' => $caixas,
					'graficos' => $graficos,

					'callcenter_chamada' => $callcenter_chamada,
					'callcenter_tabelas' => $callcenter_tabelas,
					'callcenter_consultas' => $callcenter_consultas,

					'patrimonio' => $patrimonio
				]);
				///////////////////////////////////////////////////////////////////////////
        $ativo = Auth::user()->ativo;
        if ($ativo != 1){
              return redirect()->back()->with('msg', 'Usuário inativado, entre em contato com o administrador.');
        }else{
            	return redirect()->intended('/contausuario');
        }
    }else{
        return redirect()->back()->with('msg', 'Usuário e/ou senha incorretos.');
    }
	}

	/**
	 * Mostra uma lista de registros
	 *
	 * @return Response
	 */
	public function index()
	{
		//Auth::logout();
		return view('login.index', [ 'rota' => Route::getRoutes()]);
	}

	/**
	 * Exibe um formulário de criação de registro
	 *
	 * @return Response
	 */
	public function create()
	{
        $grupo = Gruposuser::all();
		return view('erp.usuarios.cadastrar', compact('grupo'));
	}

	/**
	 * Armazena um novo registro
	 *
	 * @return Response
	 */
	public function store(Request $r)
	{
        // $r->validate([
		// 	'email' => 'required|email',
		// 	'password' => 'required'
        // ]);

        $credenciais = [
            'name' => $r->input('nome'),
            'email' => $r->Email,
            'password' => bcrypt($r->Password),
            'grupoid' => $r->input('grupouser')
        ];

        try
        {
            User::create($credenciais);
            return redirect()->route('usuarios.listar')->with('sucesso', 'Usuario cadastrado com sucesso.');
        }
        catch(\Illuminate\Database\QueryException $e)
        {
            return redirect()->back()->with('erro', 'E-mail ja cadastrada.');
        }
        catch(PDOException $e)
        {
            return redirect()->back()->with('erro', 'Algo errado aconteceu, tente novamente!');
        }
	}

	/**
	 * Exibe um registro específico
	 *
	 * @param int $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Exibe um formulário de edição de registros
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id)
	{
        $user = User::find($id);
        $grupo = Gruposuser::where('id', '!=', $user->grupoid)->get();
        return view('erp.usuarios.editar', compact('user', 'grupo'));
	}

	/**
	 * Atualiza um registro específico
	 *
	 * @param int $id
	 * @return Response
	 */
	public function update(Request $r, $id)
	{
        //$id = $r->input('id');
        $user = User::find($id);
        $user->name = $r->input('nome', $user->name);
        $user->grupoid = $r->input('grupouser') == null ? $user->grupoid : $r->input('grupouser', $user->grupoid);

        $user->save();
        return redirect()->route('usuarios.listar')->with('sucesso', 'Usuario alterado com sucesso.');
	}

	/**
	 * Remove um registro específico
	 *
	 * @param int $id
	 * @return Response
	 */
	// public function destroy($id)
	// {
    //     $user = User::find($id);
    //     $user->delete();

    //     bcrypt('123456');
    // }

    public function alterar($id){

        $user = User::find($id);
        $user->password = bcrypt('123456');
        $user->save();
        return redirect()->back()->with('sucesso', 'A nova senha do usuário é 123456.');
    }

    public function inativarAtivar($id)
	{
        $user = User::find($id);

        if ($user->ativo == 1)
        {
            $user->ativo = 0;
            $msg = 'Usuario inativado';
        }else
        {
            $user->ativo = 1;
            $msg = 'Usuario ativado';
        }

        $user->save();

        return redirect()->back()->with('delete', $msg);
    }


    public function conta()
    {

        $id = Auth::user()->id;
        $user = User::find($id);
        //dd($user);

        return view('erp.contausuario.editar', compact('user'));

    }


    public function updateconta(Request $r)
		{
        $id = $r->input('id');
        $user = User::find($id);
        $user->name = $r->input('nome', $user->name);

        $user->save();
        return redirect()->route('contausuario.editar')->with('sucesso', 'Conta atualizada com sucesso.');
    }


    public function alterarSenha()
    {
        //if ( !Auth::check() ){ return redirect()->route('login'); }
        $id = Auth::user()->id;
        $user = User::find($id);
        return view('erp.contausuario.altsenha');
    }


    public function updateSenha(Request $r)
    {
        $senha = $r->input('senha');
        $csenha = $r->input('csenha');
        if ($senha != $csenha){
            return redirect()->back()->with('erro', 'Senhas não são iguais.');
        }
        $id = Auth::user()->id;
        $user = User::find($id);
        $user->password = bcrypt($senha);
        $user->save();
        //dd($user);

        return redirect()->back()->with('sucesso', 'Senha alterada com sucesso.');
    }
}
