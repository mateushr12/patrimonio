<?php

namespace App\Http\Controllers\Callcenter\Chamada;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Acoes;
use App\User;
use App\Gruposuser;
use App\Callcenter\Chamada;
use App\Callcenter\Tipoproblema;
use App\Callcenter\Tiposolucao;
use App\Callcenter\Tipoatendimento;
use App\Callcenter\Tipocidadao;
use App\Callcenter\Cidadao;
use App\Callcenter\Palavra;
use DateTime;
use Illuminate\Support\Facades\DB;

class AcoesController extends Controller
{
    public $itensPorPagina = 3;
    public $agora = '';
    public $DataInicial = '';
    public $dataFinal = '';

    public function index()
    {
        $antes = new DateTime('2020-02-26 13:10:15');
        $agora = new DateTime('now');
        $usuarioatual = auth()->user()->id;
        $gruposuserLogado = auth()->user()->grupoid;
        $dataatual = date( 'Y-m-d', strtotime( now() ) );

        $individuals = Chamada::whereRaw( '( chamadas.cidadao_id is not null ) AND 
                                           ( ( chamadas.user_id = "' . $usuarioatual . '" AND CAST(data_hora_inicio AS DATE) = "' . $dataatual . '" ) || 
                                             ( user_id_atendeu_red = "' . $usuarioatual . '" AND CAST(data_hora_inicio AS DATE) = "' . $dataatual . '" ) || 
                                             ( gruposuser_id_red = "' . $gruposuserLogado . '" AND data_hora_red_finalizado is null ) ||
                                             ( user_id_red = "' . $usuarioatual . '" AND data_hora_red_finalizado is null ) 
                                           )' )->
                                orderByDesc( 'chamadas.id' )->get();

        $totalRegistros = 0;
        $totalRegistrosPendentes = 0;
        foreach ($individuals as $keyItem => $itemValue) { $individuals[ $keyItem ]->item = $keyItem+1; }

        foreach( $individuals as $individual )
        {   $individual->diferencadatas = strtotime( $individual->data_hora_fim ) - strtotime( $individual->data_hora_inicio );
            $totalRegistros = $totalRegistros +1;
            if ( $individual->user_id_red && !$individual->data_hora_red_finalizado ) { $totalRegistrosPendentes = $totalRegistrosPendentes + 1; } }

        $titulo = 'Lista de Chamadas ( ' . dataBR( $dataatual ) . ') - ' . '( atualização automática a cada 10 segundos )';

        if ( $totalRegistros > 0 ) { $titulo = $titulo . ' => ' . $totalRegistros . ' registro(s)'; }
        if ( $totalRegistrosPendentes > 0 ) { $titulo = $titulo . ' => ' . $totalRegistrosPendentes . ' registros pendente(s)'; }

        return view('callcenter.chamada.acoes.index', compact( 'titulo', 'dataatual', 'individuals', 'usuarioatual', 'gruposuserLogado' ) );
    }

    public function pesquisarcpf()
    {
        return view('callcenter.chamada.acoes.pesquisarcpf');
    }

    public function cadastroredirecionado( $chave )
    {
        $individuals = Chamada::find( $chave );
        $individualsOLD = $individuals;

        $tipoproblema = Tipoproblema::all();
        $tiposolucao = Tiposolucao::all();
        $gruposuser = Gruposuser::all();
        $gruposuserCombo = Gruposuser::where('id_rec_red', '=', 1)->get();
        $usuariosgrupoCombo = User::selectRaw( 'users.id, users.name' )->leftJoin( 'gruposuser', 'users.grupoid', 'gruposuser.id' )->whereRaw( 'gruposuser.id_rec_red = 1' )->whereRaw( 'users.ativo = 1' )->get();
        $tipocidadao = Tipocidadao::all();
        $tipoatendimento = Tipoatendimento::all();

        if ( $chave != null )
        {   $complementotitulo = ' de ' .
                                $individuals->usuariocadastro->name .
                                ' ocorrida em ' . datahoraBR( $individuals->data_hora_inicio ) .
                                ' [ Problema: ' . $individuals->tipoproblema_id . ' Complemento: ' . $individuals->complemento_problema . ' ]' .
                                ' [ Solução: ' . $individuals->tiposolucao_id . ' Complemento: ' . $individuals->complemento_solucao . ' ]'; }

        $datainicial = date('Y-m-d H:i:s');

        return view('callcenter.chamada.acoes.cadastroredirecionado', compact( 'datainicial', 'individuals', 'individualsOLD', 'usuariosgrupoCombo', 'complementotitulo', 'chave', 
                                                                               'tipoatendimento', 'tipoproblema', 'tiposolucao', 'gruposuser', 'gruposuserCombo', 'tipocidadao' ) );
    }

    public function novachamada( Request $req )
    {
        $tipoproblema = Tipoproblema::all();
        $tiposolucao = Tiposolucao::all();
        $gruposuser = Gruposuser::all();
        $gruposuserCombo = Gruposuser::where('id_rec_red', '=', 1)->get();
        $tipocidadao = Tipocidadao::all();

        $datainicial = date('Y-m-d H:i:s');
        return view('callcenter.chamada.acoes.novachamada', compact( 'datainicial', 'tipoproblema', 'tiposolucao', 'gruposuser', 'gruposuserCombo', 'tipocidadao' ) );
    }

    public function adicionar( Request $req )
    {
        $validaCPF = validaCPF( retirarMascaraCPF( $req->cpf ) );
        
        if ( $validaCPF === false ) { return redirect()->back()->with('msg', 'C.P.F. Inválido.'); } 
        else
        {   $tipoproblema = Tipoproblema::all();
            $tiposolucao = Tiposolucao::all();
            $gruposuser = Gruposuser::all();
            $gruposuserCombo = Gruposuser::where('id_rec_red', '=', 1)->get();
            $usuariosgrupoCombo = User::selectRaw( 'users.id, users.name' )->leftJoin( 'gruposuser', 'users.grupoid', 'gruposuser.id' )->whereRaw( 'gruposuser.id_rec_red = 1' )->whereRaw( 'users.ativo = 1' )->get();
            $tipocidadao = Tipocidadao::where('id', '!=', 4)->get();
            $cidadao = Cidadao::all();
            $tipoatendimento = Tipoatendimento::all();

            $cpfinformado = retirarMascaraCPF( $req->cpf );
            $nCidadaoCPFInformado = Cidadao::where( 'cpf', '=', $cpfinformado )->count();

            $datainicial = date('Y-m-d H:i:s');
            $individuals = Cidadao::where( 'cpf', $cpfinformado )->get();

                $agilizaInformado = $req->id_agiliza;
                $chamadasAgiliza = '';

                if ( $req->id_agiliza != '' )
                {
                    $chamadasAgiliza = Chamada::select( 'chamadas.*', 'cpf', 'cidadao_id', 'tipoproblema_id' )->
                                    leftJoin( 'cidadaos', 'chamadas.cidadao_id', '=', 'cidadaos.id' )->
                                    whereRaw( '( chamadas.id_agiliza = "' . $req->id_agiliza . '" )' )->
                                    orderBy( 'chamadas.data_hora_inicio' )->get();

                    foreach( $chamadasAgiliza as $keyitem => $itemValue ) 
                    { 
                        $chamadasAgiliza[ $keyitem ]->cidadao_nome = isset( $chamadasAgiliza[ $keyitem ]->cidadao->nome ) ? $chamadasAgiliza[ $keyitem ]->cidadao->nome : ''; 
                        $chamadasAgiliza[ $keyitem ]->usuariocadastro_name = isset( $chamadasAgiliza[ $keyitem ]->usuariocadastro->name ) ? $chamadasAgiliza[ $keyitem ]->usuariocadastro->name : ''; 
                        $chamadasAgiliza[ $keyitem ]->tipoproblema_descricao = isset( $chamadasAgiliza[ $keyitem ]->tipoproblema->descricao ) ? $chamadasAgiliza[ $keyitem ]->tipoproblema->descricao : ''; 
                        $chamadasAgiliza[ $keyitem ]->tiposolucao_descricao = isset( $chamadasAgiliza[ $keyitem ]->tiposolucao->descricao ) ? $chamadasAgiliza[ $keyitem ]->tiposolucao->descricao : ''; 
                    }
                }

            if ( $nCidadaoCPFInformado > 0 )
            {   
                $ultimaschamadas = Chamada::select( 'chamadas.*', 'cpf', 'cidadao_id', 'cidadaos.nome', 'tipoproblema_id' )->
                                            leftJoin( 'cidadaos', 'chamadas.cidadao_id', '=', 'cidadaos.id' )->
                                            whereRaw( '( cidadaos.cpf = "' . $cpfinformado . '" )' )->
                                            orderByDesc( 'chamadas.id' )->limit( 3 )->get();
                // dd( $ultimaschamadas[0]->usuariocadastro->grupo->nome, $ultimaschamadas[0]->usuariocadastro->name, $ultimaschamadas );
                foreach( $ultimaschamadas as $keyitem => $itemValue ) 
                { 
                    // dd( $ultimaschamadas, $ultimaschamadas[ $keyitem ]->data_hora_fim, $ultimaschamadas[ $keyitem ]->data_hora_inicio );
                    $ultimaschamadas[ $keyitem ]->diferencadatas = strtotime( $ultimaschamadas[ $keyitem ]->data_hora_fim ) - strtotime( $ultimaschamadas[ $keyitem ]->data_hora_inicio ); 
                    $ultimaschamadas[ $keyitem ]->usuariocadastro_name = isset( $ultimaschamadas[ $keyitem ]->usuariocadastro->name ) ? $ultimaschamadas[ $keyitem ]->usuariocadastro->name : ''; 
                    $ultimaschamadas[ $keyitem ]->usuariocadastro_grupo_nome = isset( $ultimaschamadas[ $keyitem ]->usuariocadastro->grupo->nome ) ? $ultimaschamadas[ $keyitem ]->usuariocadastro->grupo->nome : ''; 
                    $ultimaschamadas[ $keyitem ]->grupousuariored_grupo_nome = isset( $ultimaschamadas[ $keyitem ]->grupousuariored->nome ) ? $ultimaschamadas[ $keyitem ]->grupousuariored->nome : ''; 
                    $ultimaschamadas[ $keyitem ]->usuariored_name = isset( $ultimaschamadas[ $keyitem ]->usuariored->name ) ? $ultimaschamadas[ $keyitem ]->usuariored->name : ''; 
                    $ultimaschamadas[ $keyitem ]->usuariored_grupo_nome = isset( $ultimaschamadas[ $keyitem ]->usuariored->grupo->nome ) ? $ultimaschamadas[ $keyitem ]->usuariored->grupo->nome : ''; 
                    $ultimaschamadas[ $keyitem ]->usuarioatendeu_name = isset( $ultimaschamadas[ $keyitem ]->usuarioatendeu->name ) ? $ultimaschamadas[ $keyitem ]->usuarioatendeu->name : ''; 
                    $ultimaschamadas[ $keyitem ]->usuarioatendeu_grupo_nome = isset( $ultimaschamadas[ $keyitem ]->usuarioatendeu->grupo->nome ) ? $ultimaschamadas[ $keyitem ]->usuarioatendeu->grupo->nome : ''; 
                    $ultimaschamadas[ $keyitem ]->cidadao_nome = isset( $ultimaschamadas[ $keyitem ]->cidadao->nome ) ? $ultimaschamadas[ $keyitem ]->cidadao->nome : ''; 
                    $ultimaschamadas[ $keyitem ]->cidadao_fone_fixo = isset( $ultimaschamadas[ $keyitem ]->cidadao->fone_fixo ) ? $ultimaschamadas[ $keyitem ]->cidadao->fone_fixo : ''; 
                    $ultimaschamadas[ $keyitem ]->cidadao_fone_celular = isset( $ultimaschamadas[ $keyitem ]->cidadao->fone_celular ) ? $ultimaschamadas[ $keyitem ]->cidadao->fone_celular : ''; 
                    $ultimaschamadas[ $keyitem ]->cidadao_fone_celular2 = isset( $ultimaschamadas[ $keyitem ]->cidadao->fone_celular2 ) ? $ultimaschamadas[ $keyitem ]->cidadao->fone_celular2 : ''; 
                    $ultimaschamadas[ $keyitem ]->cidadao_email = isset( $ultimaschamadas[ $keyitem ]->cidadao->email ) ? $ultimaschamadas[ $keyitem ]->cidadao->email : ''; 
                    $ultimaschamadas[ $keyitem ]->cidadao_receber_informativo = isset( $ultimaschamadas[ $keyitem ]->cidadao->receber_informativo ) ? $ultimaschamadas[ $keyitem ]->cidadao->receber_informativo : '';
                    $ultimaschamadas[ $keyitem ]->tipoproblema_descricao = isset( $ultimaschamadas[ $keyitem ]->tipoproblema->descricao ) ? $ultimaschamadas[ $keyitem ]->tipoproblema->descricao : ''; 
                    $ultimaschamadas[ $keyitem ]->tiposolucao_descricao = isset( $ultimaschamadas[ $keyitem ]->tiposolucao->descricao ) ? $ultimaschamadas[ $keyitem ]->tiposolucao->descricao : ''; 
                    $ultimaschamadas[ $keyitem ]->tipocidadao_descricao = isset( $ultimaschamadas[ $keyitem ]->tipocidadao->descricao ) ? $ultimaschamadas[ $keyitem ]->tipocidadao->descricao : ''; 
                    $ultimaschamadas[ $keyitem ]->tipoatendimento_descricao = isset( $ultimaschamadas[ $keyitem ]->tipoatendimento->descricao ) ? $ultimaschamadas[ $keyitem ]->tipoatendimento->descricao : ''; 
            }
            // dd( $chamadasAgiliza, $ultimaschamadas );

                // verificar a existencia do protocolo digitado
                // caso encontrado registros com o protocolo digitado desviar para outro formulario para verificação dos atendimentos do protocolo informado
                    // neste formulario pedir confirmação do que o usuario pretende: continuar o atendimento ( com o cpf e o protocolo informado ou sair após informar ao cidadão a ocorrencia )

                return view('callcenter.chamada.acoes.cpfencontrado', compact( 'chamadasAgiliza', 'agilizaInformado', 'ultimaschamadas', 'cpfinformado', 'datainicial', 'individuals', 'tipoproblema', 'tiposolucao', 'tipoatendimento', 'usuariosgrupoCombo', 'gruposuser', 'gruposuserCombo', 'tipocidadao' ) ); 
            }
            else
            { return view('callcenter.chamada.acoes.novachamada', compact( 'chamadasAgiliza', 'agilizaInformado', 'datainicial', 'cpfinformado', 'tipoproblema', 'tiposolucao', 'tipoatendimento', 'usuariosgrupoCombo', 'gruposuser', 'gruposuserCombo', 'tipocidadao' ) ); }
        }
    }

    public function salvar( Request $req )
    {
        $dados = $req->all();
        $validaCPF = validaCPF( retirarMascaraCPF( $dados['cpf'] ) ); if ($validaCPF === false) { return redirect()->back()->with('msg', 'C.P.F. Inválido.'); } 

        $dadosChamada = $req->all();
        $dadosCidadao = $req->all();

        // gravar dadosChamada e dadosCidadao
            // salvar dados do cidadão, caso exista atualizar os dados: nome, tipo de cidadão, telefones, e-mail, receber_informativo, usuário que atualizou o registro.
            //                          caso não exista: criar um novo registro de cidadão: cpf, nome, tipo de cidadão, telefones, e-mail, receber_informativo, usuário que criou o registro.
        
            // $dadosCidadaoAtualizar['cpf'] = preg_replace("/[^0-9]/", "", $dadosCidadao['cpf']);
            $dadosCidadao['cpf'] = preg_replace("/[^0-9]/", "", $dadosCidadao['cpf']);
            $dadosCidadaoAtualizar['fone_fixo'] = preg_replace("/[^0-9]/", "", $dadosCidadao['fone_fixo']);
            $dadosCidadaoAtualizar['fone_celular'] = preg_replace("/[^0-9]/", "", $dadosCidadao['fone_celular']);
            $dadosCidadaoAtualizar['fone_celular2'] = preg_replace("/[^0-9]/", "", $dadosCidadao['fone_celular2']);
            $dadosCidadaoAtualizar['nome'] = $dadosCidadao['nome'];
            $dadosCidadaoAtualizar['email'] = $dadosCidadao['email'];
            $dadosCidadaoAtualizar['receber_informativo'] = $dadosCidadao['receber_informativo'];
            $dadosCidadaoAtualizar['tipocidadao_id'] = $dadosCidadao['tipocidadao_id'];
            $dadosCidadaoAtualizar['user_id'] = $dadosCidadao['user_id'];

            $nCPFEncontrado = '';
            $nCPFEncontrado = Cidadao::where( 'cpf', $dadosCidadao['cpf'] )->get();

            if ( isset( $nCPFEncontrado[0]->cpf ) ) // $nCidadao != '' )
            { Cidadao::find( $nCPFEncontrado[0]->id )->update( $dadosCidadaoAtualizar ); }
            else
            {   $dadosCidadaoNovo = $dadosCidadaoAtualizar;
                $dadosCidadaoNovo['cpf'] = $dadosCidadao['cpf'];
                $nCidadao = Cidadao::create( $dadosCidadaoNovo ); }

            $nCPFEncontrado = '';
            $nCPFEncontrado = Cidadao::where( 'cpf', $dadosCidadao['cpf'] )->get();
            $nCidadao = '';
            foreach($nCPFEncontrado as $val => $v){ $nCidadao = $v['id']; }
            $nc2 = $nCidadao;

            // salvar nova chamada
                    $novoProtocolo = novoProtocolo();
                    // $dados['id'] = $novoProtocolo;
                    $dadosChamadaNova['protocolo'] = $novoProtocolo;
                    $dadosChamadaNova['data_hora_inicio'] = substr( $dadosChamada['data_hora_inicio'], 0, 10 ) . ' ' . substr( $dadosChamada['hora_inicio'], 0, 8 );
                    $dadosChamadaNova['data_hora_fim'] = date('Y-m-d H:i:s');
                    $dadosChamadaNova['complemento_problema'] = $dadosChamada['complemento_problema'];
                    $dadosChamadaNova['complemento_solucao'] = $dadosChamada['complemento_solucao'];
                    $dadosChamadaNova['id_agiliza'] = $dadosChamada['id_agiliza'];
                    // $dadosChamadaNova['id_chamada_ref_red'] = $dadosChamada['id_chamada_ref_red'];
                    // $dadosChamadaNova['data_hora_red_finalizado'] = $dadosChamada['data_hora_red_finalizado'];

                    // if ( isset( $dadosChamada['gruposuser_id_red'] ) ) { $dadosChamadaNova['gruposuser_id_red'] = $dadosChamada['gruposuser_id_red']; }
                    // if ( isset( $dadosChamada['user_id_red'] ) ) { $dadosChamadaNova['user_id_red'] = $dadosChamada['user_id_red']; }
                    // if ( isset( $dadosChamada['user_id_atendeu_red'] ) ) { $dadosChamadaNova['user_id_atendeu_red'] = $dadosChamada['user_id_atendeu_red']; }

                    $dadosChamadaNova['gruposuser_id_red'] = $dadosChamada['gruposuser_id_red'];
                    $dadosChamadaNova['user_id_red'] = $dadosChamada['user_id_red'];
                    // $dadosChamadaNova['user_id_atendeu_red'] = $dadosChamada['user_id_atendeu_red'];

                    $dadosChamadaNova['tipoproblema_id'] = $dadosChamada['tipoproblema_id'];
                    $dadosChamadaNova['tiposolucao_id'] = $dadosChamada['tiposolucao_id'];
                    $dadosChamadaNova['tipoatendimento_id'] = $dadosChamada['tipoatendimento_id'];
                    $dadosChamadaNova['tipocidadao_id'] = $dadosChamada['tipocidadao_id'];
                    $dadosChamadaNova['cidadao_id'] = $nCidadao;
                    
                    // $dadosChamadaNova['gruposuser_id'] = $dadosChamada['gruposuser_id'];
                    $dadosChamadaNova['user_id'] = $dadosChamada['user_id'];

                    Chamada::create($dadosChamadaNova);

        return redirect()->route('chamada.index');
    }

    public function salvarredirecionado( Request $req )
    {
        $dados = $req->all();
        $validaCPF = validaCPF( retirarMascaraCPF( $dados['cpf'] ) ); if ($validaCPF === false) { return redirect()->back()->with('msg', 'C.P.F. Inválido.'); } 

        $dadosNovaChamada = array();
        $dadosChamadaRedirecionada = array();
        $dadosCidadao = array();

        // gravar dadosNovaChamada e dadosCidadao
            // salvar dados do cidadão, caso exista atualizar os dados: nome, tipo de cidadão, telefones, e-mail, receber_informativo, usuário que atualizou o registro.
            //                          caso não exista: criar um novo registro de cidadão: cpf, nome, tipo de cidadão, telefones, e-mail, receber_informativo, usuário que criou o registro.
        
            $dadosCidadaoAtualizar['cpf'] = preg_replace("/[^0-9]/", "", $dados['cpf']);
            $dadosCidadaoAtualizar['fone_fixo'] = preg_replace("/[^0-9]/", "", $dados['fone_fixo']);
            $dadosCidadaoAtualizar['fone_celular'] = preg_replace("/[^0-9]/", "", $dados['fone_celular']);
            $dadosCidadaoAtualizar['fone_celular2'] = preg_replace("/[^0-9]/", "", $dados['fone_celular2']);
            $dadosCidadaoAtualizar['nome'] = $dados['nome'];
            $dadosCidadaoAtualizar['email'] = $dados['email'];
            $dadosCidadaoAtualizar['receber_informativo'] = $dados['receber_informativo'];
            $dadosCidadaoAtualizar['tipocidadao_id'] = $dados['tipocidadao_id'];
            $dadosCidadaoAtualizar['user_id'] = $dados['user_id'];

            $nCPFEncontrado = Cidadao::where( 'cpf', $dados['cpf'] )->get();

            if ( $nCPFEncontrado[0]->id != null ) { Cidadao::find( $nCPFEncontrado[0]->id )->update( $dadosCidadaoAtualizar ); } 
            else {  // Cidadao::create( $dadosCidadaoNovo ); 
            }

            // salvar nova chamada
                $novoProtocolo = novoProtocolo();
                $dadosNovaChamada['protocolo'] = $novoProtocolo;
                $dadosNovaChamada['data_hora_inicio'] = substr( $dados['data_hora_inicio'], 0, 10 ) . ' ' . substr( $dados['hora_inicio'], 0, 8 );
                $dadosNovaChamada['data_hora_fim'] = date('Y-m-d H:i:s');
                $dadosNovaChamada['complemento_problema'] = $dados['complemento_problema'];
                $dadosNovaChamada['complemento_solucao'] = $dados['complemento_solucao'];
                $dadosNovaChamada['id_agiliza'] = $dados['id_agiliza'];
                $dadosNovaChamada['gruposuser_id_red'] = $dados['gruposuser_id_red'];
                $dadosNovaChamada['user_id_red'] = $dados['user_id_red'];
                $dadosNovaChamada['tipoproblema_id'] = $dados['tipoproblema_id'];
                $dadosNovaChamada['tiposolucao_id'] = $dados['tiposolucao_id'];
                $dadosNovaChamada['tipoatendimento_id'] = $dados['tipoatendimento_id'];
                $dadosNovaChamada['tipocidadao_id'] = $dados['tipocidadao_id'];
                $dadosNovaChamada['cidadao_id'] = $nCPFEncontrado[0]->id; //$nCidadao;
                $dadosNovaChamada['user_id'] = $dados['user_id'];

                $NovaChamadaCriada = Chamada::create($dadosNovaChamada);

            // salvar nova chamada redirecionada
                // $dadosChamadaRedirecionada['id'] = $dadosChamadaRedirecionada['OLD_id'];
                //// $dadosChamadaRedirecionada['id_chamada_ref_red'] = $dados['id_chamada_ref_red'];
                $dadosChamadaRedirecionada['data_hora_red_finalizado'] = $dadosNovaChamada['data_hora_fim'];
                $dadosChamadaRedirecionada['user_id_atendeu_red'] = $dados['user_id'];
                $dadosChamadaRedirecionada['id_chamada_ref_red'] = $NovaChamadaCriada['id']; //$idnovachamada[0]->id;

                // $retornoChamadaRedirecionada = Chamada::find( $dados['OLD_id'] )->update($dadosChamadaRedirecionada);
                $DadosAtualizadosChamadaOriginal = Chamada::find( $dados['OLD_id'] )->update($dadosChamadaRedirecionada);

        return redirect()->route('chamada.index');
    }

    public function deletar( $chave )
    {
        $ac = Chamada::find($chave);
        if (isset($ac)){
            $ac->delete();
        }
        return redirect()->route('chamada.index');
    }

    public function editar( $chave ) // não usada
    {
        // $acoessalvas = Chamada::all();
        $individual = Chamada::find($chave);

        $tipoproblema = Tipoproblema::all();
        $tiposolucao = Tiposolucao::all();
        $gruposuser = Gruposuser::all();
        $gruposuserCombo = Gruposuser::where('id_rec_red', '=', 1)->get();
        $tipocidadao = Tipocidadao::all();

        $individuals = DB::connection('mysqlCallcenter')->select( 'SELECT  chamadas.id, chamadas.data_hora_inicio, chamadas.data_hora_fim, chamadas.data_hora_red_finalizado,
                                            time_to_sec( timediff(chamadas.data_hora_fim, chamadas.data_hora_inicio) ) as diferencadatas,
                                            chamadas.fone_fixo, chamadas.fone_celular, chamadas.fone_celular2,
                                            chamadas.nome, chamadas.email, chamadas.cpf,
                                            chamadas.descricao, chamadas.complemento_solucao,
                                            chamadas.id_agiliza,
                                            chamadas.tipoproblema_id, tipoproblemas.descricao as descricao,
                                            chamadas.tiposolucao_id, tiposolucaos.descricao as descricao,
                                            chamadas.user_id, users.id, users.name as nome_usuario,
                                            cidadao.tipocidadao_id, tipocidadaos.descricao as descricao,
                                            gruposuser.id, gruposuser.descricao as descricao_gruposuser, gruposuser2.descricao as descricao_gruposuser_usuario_atendimento
                                    FROM chamadas chamadas
                                    LEFT JOIN users ON chamadas.user_id = users.id
                                    LEFT JOIN gruposuser AS gruposuser2 ON users.grupoid = gruposuser2.id
                                    LEFT JOIN gruposuser ON chamadas.gruposuser_id = gruposuser.id
                                    LEFT JOIN tipoproblemas ON chamadas.tipoproblema_id = tipoproblemas.id
                                    LEFT JOIN tipocidadaos ON cidadao.tipocidadao_id = tipocidadaos.id
                                    LEFT JOIN tiposolucaos ON chamadas.tiposolucao_id = tiposolucaos.id
                                    WHERE chamadas.id = ' . '"' .  $chave . '"' );

        $individualsOLD = $individuals;

        if ( $chave != null )
        {
            foreach( $individuals as $individual )
            {
                $complementotitulo = ' redirecioanda de ' .
                                     $individual->nome_usuario .
                                     ' [ Problema: ' . $individual->descricao . ' Complemento: ' . $individual->descricao . ' ]' .
                                     ' [ Solução: ' . $individual->descricao . ' Complemento: ' . $individual->solucao . ' ]';

            }
        }

        $datainicial = date('Y-m-d H:i:s');
        return view('callcenter.chamada.acoes.editar', compact( 'datainicial', 'individuals', 'individualsOLD', 'complementotitulo', 'chave', 'tipoproblema', 'tiposolucao', 'gruposuser', 'gruposuserCombo', 'tipocidadao' ) );
    }

    public function atualizar( Request $req, $chave ) // não usada
    {
        $dadosOLD = $req->all();

        // fazer modificações e salvar nos dados antigos antes de salvar

            $dadosOLD["user_id"]                 = $dadosOLD["user_idOLD"];
            $dadosOLD["id"]              = $dadosOLD["idOLD"];
            $dadosOLD["data_hora_inicio"]        = $dadosOLD["data_hora_inicioOLD"];
            $dadosOLD["hora_inicio"]             = $dadosOLD["hora_inicioOLD"];
            $dadosOLD["data_hora_fim"]           = $dadosOLD["data_hora_fimOLD"];
            $dadosOLD["gruposuser_id"]                = $dadosOLD["gruposuser_user_idOLD"];
            $dadosOLD["data_hora_red_finalizado"]        = $dadosOLD["data_hora_red_finalizadoOLD"];
            // $dadosOLD["user_id_atendeu_red"]  = $dadosOLD["id"];
            $dadosOLD["cpf"]             = $dadosOLD["cpfOLD"];
            $dadosOLD["tipocidadao_id"]          = $dadosOLD["tipocidadao_user_idOLD"];
            $dadosOLD['fone_fixo']       = preg_replace("/[^0-9]/", "", $dadosOLD['fone_fixoOLD']);
            $dadosOLD['fone_celular']    = preg_replace("/[^0-9]/", "", $dadosOLD['fone_celularOLD']);
            $dadosOLD['fone_celular2']   = preg_replace("/[^0-9]/", "", $dadosOLD['fone_celular2OLD']);
            $dadosOLD["nome"]            = $dadosOLD["nomeOLD"];
            $dadosOLD["email"]           = $dadosOLD["emailOLD"];
            $dadosOLD["id_agiliza"]              = $dadosOLD["id_agilizaOLD"];
            $dadosOLD["tipoproblema_id"]         = $dadosOLD["tipoproblema_user_idOLD"];
            $dadosOLD["complemento_problema"]      = $dadosOLD["complemento_problemaOLD"];
            $dadosOLD["tiposolucao_id"]          = $dadosOLD["tiposolucao_user_idOLD"];
            $dadosOLD["complemento_solucao"]        = $dadosOLD["complemento_solucaoOLD"];
            $dadosOLD['data_hora_red_finalizado']        = date( 'Y-m-d H:i:s' );

            // App\Flight::where('active', 1)->where('destination', 'San Diego')->update(['delayed' => 1]);

            Chamada::where( 'id', $dadosOLD['id'] )->update( [ 'data_hora_red_finalizado' => date( 'Y-m-d H:i:s' ) ] );

            // Chamada::find( $dadosOLD['id'] )->update( $dadosOLD ); // rever o update fazendo somente os campos necessarios

        // salvar dados novos
            $dados = $req->all();

            $dados['data_hora_inicio'] = substr( $dados['data_hora_inicio'], 0, 10 ) . ' ' . substr( $dados['hora_inicio'], 0, 8 );
            $dados['data_hora_fim'] = date( 'Y-m-d H:i:s' );

            $dados['fone_fixo'] = preg_replace("/[^0-9]/", "", $dados['fone_fixo']);
            $dados['fone_celular'] = preg_replace("/[^0-9]/", "", $dados['fone_celular']);
            $dados['fone_celular2'] = preg_replace("/[^0-9]/", "", $dados['fone_celular2']);

            $novoProtocolo = novoProtocolo();

            $dados['id'] = $novoProtocolo;

            // $dados['gruposuser_id'] = $dados['gruposuser_id'];

            Chamada::create( $dados );

        return redirect()->route('chamada.index');
    }
}
