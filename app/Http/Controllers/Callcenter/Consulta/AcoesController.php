<?php

namespace App\Http\Controllers\Callcenter\Consulta;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Gruposuser;
use App\Callcenter\Chamada;
use App\Callcenter\Tipoatendimento;
use App\Callcenter\Tipoproblema;
use App\Callcenter\Tiposolucao;
use App\Callcenter\Tipocidadao;
use App\Callcenter\Cidadao;
use App\Callcenter\Consulta;
use DateTime;
use CreateCpfFornecedorTable;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Charts;
use Dompdf\Dompdf;
use PDF;


class AcoesController extends Controller
{
    public $itensPorPagina = 10;

    // chamada geral
        public function acao_ctrl_chamadageral_parametros( Request $request )
            {
                $tipoproblema    = Tipoproblema::all();
                $tiposolucao     = Tiposolucao::all();
                $tipoatendimento = Tipoatendimento::all();
                $tipocidadao     = Tipocidadao::all();

                $usuarioatendimento   = Chamada::selectRaw( 'DISTINCT user_id' )->where( 'user_id', '>', 0 )->get();
                $gruporedirecionado   = Chamada::selectRaw( 'DISTINCT gruposuser_id_red' )->where( 'gruposuser_id_red', '>', 0 )->get();
                $usuarioredirecionado = Chamada::selectRaw( 'DISTINCT user_id_red' )->where( 'user_id_red', '>', 0 )->get();

                return view('callcenter.consulta.chamadageral.view_chamadageral_parametros', compact( 'gruporedirecionado', 'usuarioredirecionado', 'usuarioatendimento', 'tipoatendimento', 'tipoproblema', 'tiposolucao', 'tipocidadao' ));
            }

        public function acao_ctrl_chamadageral_resultado( Request $request )
            {
                $datainicial = $request->data_inicial; $datafinal = $request->data_final; $titulo = 'Consulta Geral de Chamadas ';

                $titulo = $titulo . ': [ Limite Registros: ' . $request->max_registros . ' ] - [ Período: ' . dataDiaSemana( $datainicial ) . ' a ' . dataDiaSemana( $datafinal ) . ' ]';

                $request->gruposuser_id_red != 0 ? $gruposuserredirecionadoencontrado = Gruposuser::find( $request->gruposuser_id_red ) : ''; 
                $request->gruposuser_id_red != 0 ? $titulo = $titulo . ' - [ Redirecionado para Grupo: ' . $gruposuserredirecionadoencontrado->descricao . ' ]' : '';

                $request->id_agiliza != null ? $titulo = $titulo . ' - [ Protocolo Agiliza: ' . $request->id_agiliza . ' ]' : '';
                $request->tipocidadao_id != 0 ? $tipocidadaoencontrado = Tipocidadao::find( $request->tipocidadao_id ) : ''; $request->tipocidadao_id != 0 ? $titulo = $titulo . ' - [ Tipo de Cidadão: ' . $tipocidadaoencontrado->descricao . ' ]' : '';
                $request->cpf != null ? $titulo = $titulo . ' - [ C.P.F.: ' . $request->cpf . ' ]' : '';
                $request->email != null ? $titulo = $titulo . ' - [ E-mail: ' . $request->email . ' ]' : '';
                $request->nome != null ? $titulo = $titulo . ' - [ Nome Cidadão: ' . $request->nome . ' ]' : '';
                $request->tipoproblema_id > 0 ? $tipoproblemaencontrado = Tipoproblema::find( $request->tipoproblema_id ) : ''; $request->tipoproblema_id > 0 ? $titulo = $titulo . ' - [ Tipo de Problema: ' . $tipoproblemaencontrado->descricao . ' ]' : '';
                $request->complemento_problema != null ? $titulo = $titulo . ' - [ Complemento Problema: ' . $request->complemento_problema . ' ]' : '';
                $request->tiposolucao_id > 0 ? $tiposolucaoencontrado = Tiposolucao::find( $request->tiposolucao_id ) : ''; $request->tiposolucao_id > 0 ? $titulo = $titulo . ' - [ Tipo de Solução: ' . $tiposolucaoencontrado->descricao . ' ]' : '';
                $request->complemento_solucao != null ? $titulo = $titulo . ' - [ Complemento Solução: ' . $request->complemento_solucao . ' ]' : '';
                $request->user_id > 0 ? $usuarioencontrado = User::find( $request->user_id ) : ''; $request->user_id > 0 ? $titulo = $titulo . ' - [ Usuário: ' . $usuarioencontrado->name . ' ]' : '';

                $request->user_id_red != 0 ? $usuarioredirecionadoencontrado = User::find($request->user_id_red) : ''; $request->user_id_red > 0 ? $titulo = $titulo . ' - [ Usuário Redirecionado: ' . $usuarioredirecionadoencontrado->name . ' ]' : '';

                if ( $request->receber_informativo === 0 ) { $titulo = $titulo . ' - [ Recebe Informativo: Não ]'; }
                elseif ( $request->receber_informativo === 1 ) { $titulo = $titulo . ' - [ Recebe Informativo: Sim ]'; }

                $request->tipoatendimento_id > 0 ? $tipoatendimentoencontrado = Tipoatendimento::find($request->tipoatendimento_id) : ''; $request->tipoatendimento_id > 0 ? $titulo = $titulo . ' - [ Tipo Atendimento: ' . $tipoatendimentoencontrado->descricao . ' ]' : '';

                $cpf                     = retirarMascaraCPF($request->cpf);
                $usuario                 = $request->user_id;
                $complemento_problema    = $request->complemento_problema;
                $complemento_solucao     = $request->complemento_solucao;
                $nome                    = $request->nome;
                $email                   = $request->email;
                $protocolo_agiliza       = $request->id_agiliza;
                $gruposuserredirecionado = $request->gruposuser_id_red;
                $tipocidadao             = $request->tipocidadao_id;
                $tipoatendimento         = $request->tipoatendimento_id;
                $usuarioredirecionado    = $request->user_id_red;
                $recebe_informativo      = $request->receber_informativo;

                $usuarioatual = auth()->user()->id;
                $gruposuserLogado = auth()->user()->grupoid;

                $queryEstrutura = '';
                $request->gruposuser_id_red > 0 ? $queryEstrutura = $queryEstrutura . ' AND (chamadas.gruposuser_id_red = ' . $request->gruposuser_id_red . ')' : '';
                $request->id_agiliza != null ? $queryEstrutura = $queryEstrutura . ' AND (chamadas.id_agiliza like "%' . $request->id_agiliza . '%"' . ')' : '';
                $request->tipocidadao_id > 0 ? $queryEstrutura = $queryEstrutura . ' AND (chamadas.tipocidadao_id = ' . $request->tipocidadao_id . ')' : '';
                $request->cpf != null ? $queryEstrutura = $queryEstrutura . ' AND (cidadaos.cpf like "%' . $cpf . '%"' . ')' : '';
                $request->email != null ? $queryEstrutura = $queryEstrutura . ' AND (cidadaos.email like "%' . $request->email . '%"' . ')' : '';
                $request->nome != null ? $queryEstrutura = $queryEstrutura . ' AND (cidadaos.nome like "%' . $request->nome . '%"' . ')' : '';
                $request->tipoproblema_id > 0 ? $queryEstrutura = $queryEstrutura . ' AND (chamadas.tipoproblema_id = ' . $request->tipoproblema_id . ')' : '';
                $request->complemento_problema != null ? $queryEstrutura = $queryEstrutura . ' AND (chamadas.complemento_problema like "%' . $request->complemento_problema . '%"' . ')' : '';
                $request->tiposolucao_id > 0 ? $queryEstrutura = $queryEstrutura . ' AND (chamadas.tiposolucao_id = ' . $request->tiposolucao_id . ')' : '';
                $request->complemento_solucao != null ? $queryEstrutura = $queryEstrutura . ' AND (chamadas.complemento_solucao like "%' . $request->complemento_solucao . '%"' . ')' : '';
                $request->user_id > 0 ? $queryEstrutura = $queryEstrutura . ' AND (chamadas.user_id = ' . $request->user_id . ')' : '';
                $request->user_id_red > 0 ? $queryEstrutura = $queryEstrutura . ' AND (chamadas.user_id_red = ' . $request->user_id_red . ')' : '';
                $request->tipoatendimento_id > 0 ? $queryEstrutura = $queryEstrutura . ' AND (chamadas.tipoatendimento_id = ' . $request->tipoatendimento_id . ')' : '';

                $queryEstrutura = $queryEstrutura; // . ' ORDER BY chamadas.id desc' . ' LIMIT ' . $request->max_registros;

                $individuals = Chamada::selectRaw( 'protocolo, data_hora_inicio, chamadas.tipocidadao_id, chamadas.cidadao_id, tipoatendimento_id, tipoproblema_id, complemento_problema, tiposolucao_id, complemento_solucao, 
                                                    id_agiliza, data_hora_red_finalizado, 
                                                    data_hora_fim, data_hora_red_finalizado, chamadas.user_id, user_id_red, user_id_atendeu_red, gruposuser_id_red, gruposuser_id, id_chamada_ref_red,
                                                    cidadaos.cpf as cpf, cidadaos.nome as nome, cidadaos.fone_fixo, cidadaos.fone_celular, cidadaos.fone_celular2, cidadaos.email, cidadaos.receber_informativo,
                                                    tipocidadaos.descricao as descricao' )->
                                        leftJoin( 'jucese_callcenter.cidadaos as cidadaos', 'chamadas.cidadao_id', 'cidadaos.id' )->
                                        leftJoin( 'jucese_callcenter.tipocidadaos as tipocidadaos', 'chamadas.tipocidadao_id', 'tipocidadaos.id' )->
                                        whereRaw( ' chamadas.cidadao_id is not null AND ( CAST(data_hora_inicio AS DATE) BETWEEN "' . $datainicial . '" AND "' . $datafinal . '" ) ' .  $queryEstrutura )->
                                        orderBy( 'chamadas.id', 'DESC' )->
                                        limit( $request->max_registros )->
                                        get();

                if ( count( $individuals ) > 0 )
                {
                    foreach ($individuals as $keyItem => $itemValue) { $individuals[ $keyItem ]->item = $keyItem+1; }
                    foreach( $individuals as $individual ) { $individual->diferencadatas = strtotime( $individual->data_hora_fim ) - strtotime( $individual->data_hora_inicio ); }

                    session()->forget( [ 'individuals', 'totalregistros' ] );
                    session()->put( 'individuals', $individuals );
                    $totalregistros = count( $individuals );
                    session()->put( 'totalregistros', $totalregistros );

                    return view('callcenter.consulta.chamadageral.view_chamadageral_resultado', compact( 'titulo', 'gruposuserLogado' ));
                }
                else
                {
                    return redirect()->back()->with( 'msg', 'Registro não encontrado.' );
                }
            }

        public function acao_ctrl_chamadageral_imprimir( Request $request )
            {
                $individuals = $request->individuals;
                $titulo = $request->titulo;
                $nTotalAtendimentos = $request->nTotalAtendimentos;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.chamadageral.view_chamadageral_imprimir', compact( 'individuals', 'titulo', 'nTotalAtendimentos' ) ); //->setPaper('a4', 'portrait');

                return $pdf->stream('JUCESE-consulta-chamadageral.pdf');
            }
        
    // cpf distinto por tipo de cidadão
        public function cpfdistintoportipocidadao( Request $request )
            {
                $individuals = Cidadao::selectRaw( 'tipocidadao_id, count(*) as contador' )
                                        ->leftJoin( 'tipocidadaos as tipocidadaos', 'tipocidadao_id', '=', 'tipocidadaos.id' )
                                        ->groupBy( 'tipocidadao_id' )
                                        ->orderByDesc( 'contador' )->limit( $request->max_registros )->get();

                if ( count( $individuals ) > 0 )
                {
                    $titulo = "Consulta de C.P.F.'s Distintos Por Tipo de Cidadão" . ": " . " [ Limite Registros:" . $request->max_registros . " ]";

                    $percentualtotal = 0;
                    foreach ( $individuals as $keyItem => $itemValue )
                    {   
                        $percentualtotal = $percentualtotal + $individuals[ $keyItem ]->percentualcontador;
                        $individuals[ $keyItem ]->descricao_tipocidadao = $individuals[ $keyItem ]->tipocidadao->descricao;
                    }

                    $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );
                    $individuals = $arrayResultadoCalculo;

                    foreach ( $individuals as $keyItem => $itemValue )
                    {   
                        $individuals[ $keyItem ]->percentualtotal = $individuals[ $keyItem ]->percentual;
                    }

                    $tituloGrafico = 'Qtde C.P.F.s Distintos por Tipo de Cidadão';

                    $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );
                    $individuals = $arrayResultadoCalculo;


                    session()->forget( [ 'individuals', 'titulo', 'totalregistros', 'tituloGrafico', 'eixoX', 'textoEixoY', 'valoresEixoYCategoria1', 'tituloSeries' ] );

                    // inicio - dados dos gráficos
                        $grafico_A_Nome_Grafico = 'grafico1';
                        $grafico_A_Titulo = 'C.P.F.s Distintos por Tipo de Cidadão';
                        $grafico_A_Titulo_Eixo_Y = 'Qtde C.P.F.s';
                        $grafico_A_Serie_1_Legenda = 'Tipo de Cidadão';
                        $grafico_A_Serie_1_Tipo_Grafico = 'column';

                        foreach ( $individuals as $keyItem => $itemValue )
                        {
                            $grafico_A_Rotulos_Series_Eixo_X[] = $individuals[ $keyItem ]->descricao_tipocidadao;
                            $grafico_A_Serie_1_Dados[] = $individuals[ $keyItem ]->contador;
                        }
                        
                        $totalregistros = count( $individuals );
                        session()->put( 'totalregistros', $totalregistros );
                        session()->put( 'individuals', $individuals );
                        session()->put( 'titulo' );

                        session()->put( 'grafico_A_Nome_Grafico', $grafico_A_Nome_Grafico );
                        session()->put( 'grafico_A_Titulo', $grafico_A_Titulo );
                        session()->put( 'grafico_A_Titulo_Eixo_Y', $grafico_A_Titulo_Eixo_Y );
                        session()->put( 'grafico_A_Rotulos_Series_Eixo_X', $grafico_A_Rotulos_Series_Eixo_X );
                        session()->put( 'grafico_A_Serie_1_Legenda', $grafico_A_Serie_1_Legenda ); 
                        session()->put( 'grafico_A_Serie_1_Tipo_Grafico', $grafico_A_Serie_1_Tipo_Grafico ); 
                        session()->put( 'grafico_A_Serie_1_Dados', $grafico_A_Serie_1_Dados ); 
                    // final - dados dos gráficos

                    return view('callcenter.consulta.cpfdistintoportipocidadao.cpfdistintoportipocidadao', compact( 'titulo' ) );
                }
                else
                {
                    return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta ' );
                }
            }

    // problemas por tipo de cidadão
        public function acao_ctrl_problemasportipocidadao_parametros( Request $request )
            {
                $tipocidadao     = Tipocidadao::all();
                $cidadao         = Cidadao::all();

                return view('callcenter.consulta.problemasportipocidadao.view_problemasportipocidadao_parametros', compact( 'tipocidadao', 'cidadao' ));
            }

        public function acao_ctrl_problemasportipocidadao_resultado( Request $request )
            {
                $datainicial = $request->data_inicial;
                $datafinal = $request->data_final;
                $titulo = 'Consulta Problemas por Tipo de Cidadão';
                $titulo = $titulo . ': [ Período: ' . dataDiaSemana( $datainicial ) . ' a ' . dataDiaSemana( $datafinal ) . ' ]';
                $arrayInformacoes = collect( [ 'titulo', 'individuals' ] );

                $complementoWhere = '';
                if ( $request->tipocidadao_id == 0 )
                    { $tipocidadao_identificado = ' - [ Tipo de Cidadão: Todos ]'; }
                else
                    { $complementoWhere = ' (chamadas.tipocidadao_id = ' . $request->tipocidadao_id . ') AND ';
                    $tipocidadaoencontrado = Tipocidadao::find($request->tipocidadao_id);
                    $tipocidadao_identificado = ' - [ Tipo de Cidadão: ' . $tipocidadaoencontrado->descricao_tipocidadao . ' ]'; }

                if ( $request->tipocidadao_id != 0 ) 
                { 
                    $individuals = Chamada::select( 'tipocidadao_id', 'tipoproblema_id' )
                                        ->where( 'tipocidadao_id', '=', $request->tipocidadao_id )
                                        ->whereRaw( 'CAST(data_hora_inicio AS DATE) BETWEEN "' . $datainicial . '" AND "' . $datafinal . '"' )
                                        ->groupBy( 'tipocidadao_id' )
                                        ->groupBy( 'tipoproblema_id' )
                                        ->selectRaw( 'tipocidadao_id, tipoproblema_id, count(*) as contador' )
                                        ->orderBy( 'tipocidadao_id' )
                                        ->orderBy( 'contador', 'DESC' )->get();
                    $contadorTipocidadao = 0;
                    $individualsDetalhe = null;
                }
                else
                { 
                    $individuals = Chamada::select( 'tipocidadao_id', 'tipoproblema_id' )
                                        ->whereRaw( 'CAST(data_hora_inicio AS DATE) BETWEEN "' . $datainicial . '" AND "' . $datafinal . '"' )
                                        ->groupBy( 'tipocidadao_id' )
                                        ->groupBy( 'tipoproblema_id' )
                                        ->selectRaw( 'tipocidadao_id, tipoproblema_id, count(*) as contador' )
                                        ->orderBy( 'tipocidadao_id' )
                                        ->orderBy( 'contador', 'DESC' )->get();
                    $contadorTipocidadao = Tipocidadao::count();
                    $individualsDetalhe = Chamada::select( 'tipocidadao_id', 'tipoproblema_id' )
                                        ->whereRaw( 'CAST(data_hora_inicio AS DATE) BETWEEN "' . $datainicial . '" AND "' . $datafinal . '"' )
                                        ->groupBy( 'tipocidadao_id' )
                                        ->groupBy( 'tipoproblema_id' )
                                        ->selectRaw( 'tipocidadao_id, tipoproblema_id, count(*) as contador' )
                                        // ->orderBy( 'tipocidadao_id' )
                                        ->orderBy( 'contador', 'DESC' )->limit( $contadorTipocidadao * 2 )->get();
                }
                // dd( $contadorTipocidadao, $individualsDetalhe );
                $nTotalAtendimentos = 0;
                $individualsTotais = array();
                foreach ($individuals as $keyItem => $itemValue)
                {
                    $individuals[ $keyItem ]->item = $keyItem+1;
                    $individuals[ $keyItem ]->tipocidadao = $individuals[ $keyItem ]->tipocidadao->descricao;
                    $individuals[ $keyItem ]->tipoproblema = $individuals[ $keyItem ]->tipoproblema->descricao;
                    $nTotalAtendimentos = $nTotalAtendimentos + $individuals[ $keyItem ]->contador;
                    $rotuloAtual = $individuals[ $keyItem ]->tipocidadao . ' - ' . $individuals[ $keyItem ]->tipoproblema;
                    $rotuloGrupo = $individuals[ $keyItem ]->tipocidadao;
                    $individuals[ $keyItem ]->rotuloAtual = $rotuloAtual;
                    $individuals[ $keyItem ]->rotuloGrupo = $rotuloGrupo;
                    $rotulos1Grafico[] = $individuals[ $keyItem ]->rotuloAtual;
                    $valores1Grafico[] = $individuals[ $keyItem ]->contador;

                    if ( isset( $individualsTotais[ $individuals[ $keyItem ]->rotuloGrupo ] ) ) 
                    { $individualsTotais[ $individuals[ $keyItem ]->rotuloGrupo ] = $individualsTotais[ $individuals[ $keyItem ]->rotuloGrupo ] + $individuals[ $keyItem ]->contador; } 
                    else 
                    { $individualsTotais[ $individuals[ $keyItem ]->rotuloGrupo ] = $individuals[ $keyItem ]->contador; }
                }

                foreach ($individuals as $keyItem => $itemValue) 
                { 
                    $individuals[ $keyItem ]->percentual = ( $individuals[ $keyItem ]->contador / $individualsTotais[ $individuals[ $keyItem ]->rotuloGrupo ] ) * 100; 
                    $individuals[ $keyItem ]->totalGrupo = $individualsTotais[ $individuals[ $keyItem ]->rotuloGrupo ]; 
                }
                /*
                    foreach ($individuals as $keyItem => $itemValue)
                    { 
                        if ( $keyItem > 0 )
                        {
                            $registros[] = array();
                            if ( $individuals[ $keyItem ]->rotuloGrupo != $individuals[ $keyItem-1 ]->rotuloGrupo )
                            {
                                // $registros[ $keyItem+1 ]->item = $individuals[ $keyItem ]->item;
                                $registros[ $keyItem+1 ]->tipocidadao = $individuals[ $keyItem ]->tipocidadao;
                                $registros[ $keyItem+1 ]->tipoproblema = $individuals[ $keyItem ]->tipoproblema;
                                $registros[ $keyItem+1 ]->$nTotalAtendimentos = $individuals[ $keyItem ]->contador;
                                $registros[ $keyItem+1 ]->$rotuloAtual = $individuals[ $keyItem ]->rotuloAtual;
                                $registros[ $keyItem+1 ]->$rotuloGrupo = $individuals[ $keyItem ]->rotuloGrupo;
                                    }
                        }
                        // dd( $keyItem, $individuals[ $keyItem ]->item );
                        // $registros[]->item = $individuals[ $keyItem ]->item;
                        $registros[]->tipocidadao = $individuals[ $keyItem ]->tipocidadao;
                        $registros[]->tipoproblema = $individuals[ $keyItem ]->tipoproblema;
                        $registros[]->$nTotalAtendimentos = $individuals[ $keyItem ]->contador;
                        $registros[]->$rotuloAtual = $individuals[ $keyItem ]->rotuloAtual;
                        $registros[]->$rotuloGrupo = $individuals[ $keyItem ]->rotuloGrupo;
                    }
                */

                $tituloGrafico = 'Registros de Chamadas por Tipo de Cidadão';

                if ( isset( $individuals ) && count( $individuals ) > 0 )
                {
                    $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );
                    
                    if ( isset( $individualsDetalhe ) && count( $individualsDetalhe ) > 0 )
                    {
                        $arrayResultadoCalculo2 = calcularTotaisArrayComplexo( $individualsDetalhe, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );
                        $individualsDetalhe = $arrayResultadoCalculo2;

                        $grafico2tituloGrafico = 'Tipos de Problemas com Maiores Ocorrências';
                        $grafico2tituloSeries = 'Tipo de Cidadão - Tipo de Problema';
                        $grafico2eixoX = 'Qtde Atendimentos';
                        $grafico2eixoY = 'Tipos de Problemas com Maiores Qtde de Chamadas';

                        $grafico2textoEixoY = array();
                        $grafico2valoresEixoYCategoria1 = array();
                        foreach ($individualsDetalhe as $keyItem => $itemValue)
                        {
                            $individualsDetalhe[ $keyItem ]->percentual = ( $individualsDetalhe[ $keyItem ]->contador / $individualsDetalhe[ $keyItem ]->totalcontador ) * 100;
                            $individualsDetalhe[ $keyItem ]->tipocidadao = $individualsDetalhe[ $keyItem ]->tipocidadao->descricao;
                            $individualsDetalhe[ $keyItem ]->tipoproblema =  $individualsDetalhe[ $keyItem ]->tipoproblema->descricao;
                            
                            $grafico2textoEixoY[] = $individualsDetalhe[ $keyItem ]->tipocidadao . ' - ' . strtok( wordwrap( $individualsDetalhe[ $keyItem ]->tipoproblema, 8, "...\n" ), "\n" );
                            $grafico2valoresEixoYCategoria1[] = $individualsDetalhe[ $keyItem ]->contador;
                        }        
                    }

                    $individuals = $arrayResultadoCalculo;
                    foreach ( $individuals as $keyItem => $itemValue )
                    {
                        $percentual = ( $individuals[ $keyItem ]->contador / $individuals[ $keyItem ]->totalcontador ) * 100;
                        $individuals[ $keyItem ]->percentual = $percentual;
                        $mediaAtendimentoIndividual = ( $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador );
                        $individuals[ $keyItem ]->mediaAtendimentoIndividual = $mediaAtendimentoIndividual;
                        // $media = ( $individuals[ $keyItem ]->contador / $individuals[ $keyItem ]->diasuteis );
                        // $individuals[ $keyItem ]->media = $media;
                        $tempomedio = ( $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador );
                        $individuals[ $keyItem ]->tempomedio = $tempomedio;
                    }

                    $tituloGrafico = 'Tipos de Problemas com Maiores Ocorrências';
                    $tituloSeries = 'Tipo de Cidadão - Tipo de Problema';
                    $eixoX = 'Qtde Atendimentos';
                    $eixoY = 'Tipos de Problemas com Maiores Qtde de Chamadas';

                    $textoEixoY = array();
                    $valoresEixoYCategoria1 = array();
                    foreach ( $individuals as $keyItem => $itemValue )
                    {
                        $textoEixoY[] = $individuals[ $keyItem ]->tipocidadao . ' - ' . $individuals[ $keyItem ]->tipoproblema;
                        $valoresEixoYCategoria1[] = $individuals[ $keyItem ]->contador;
                        // $valoresEixoYCategoria2[] = $individuals[ $keyItem ]->diasuteis;
                    }

                    session()->forget( [ 'individuals', 'titulo', 'totalregistros', 'tituloGrafico', 'eixoX', 'textoEixoY', 'valoresEixoYCategoria1', 'tituloSeries' ] );
                    session()->forget( [ 'individualsDetalhe', 'grafico2tituloGrafico', 'grafico2eixoX', 'grafico2textoEixoY', 'grafico2valoresEixoYCategoria1', 'grafico2tituloSeries' ] );

                    $totalregistros = count( $individuals );
                    session()->put( 'totalregistros', $totalregistros );

                    session()->put( 'individuals', $individuals );
                    session()->put( 'titulo', $titulo );
                    session()->put( 'tituloGrafico', $tituloGrafico );
                    session()->put( 'eixoX', $eixoX );
                    session()->put( 'textoEixoY', $textoEixoY );
                    session()->put( 'valoresEixoYCategoria1', $valoresEixoYCategoria1 );
                    session()->put( 'tituloSeries', $tituloSeries );

                    if ( isset( $individualsDetalhe ) && count( $individualsDetalhe ) > 0 )
                    {
                        session()->put( 'individualsDetalhe', $individualsDetalhe );
                        session()->put( 'grafico2tituloGrafico', $grafico2tituloGrafico );
                        session()->put( 'grafico2eixoX', $grafico2eixoX );
                        session()->put( 'grafico2textoEixoY', $grafico2textoEixoY );
                        session()->put( 'grafico2valoresEixoYCategoria1', $grafico2valoresEixoYCategoria1 );
                        session()->put( 'grafico2tituloSeries', $grafico2tituloSeries );
                        }

                    return view('callcenter.consulta.problemasportipocidadao.view_problemasportipocidadao_resultado', compact( 'titulo' ) );
                }
                else
                {
                    return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta.' );
                }

                /*
                    session()->put('individuals', $individuals);
                    if ( count( $individuals ) > 0 )
                    {   $chart = Charts::multi('bar', 'google')->title( $tituloGrafico )->dimensions( 1024, 400 )->template( "material" )->dataset( 'Qtde Atendimentos', $valores1Grafico )->labels( $rotulos1Grafico )->responsive(false);
                        session()->put('chart', $chart);
                        session()->put('tituloGrafico', $tituloGrafico);
                        session()->put('rotulos1Grafico', $rotulos1Grafico); }

                    return view('callcenter.consulta.problemasportipocidadao.view_problemasportipocidadao_resultado', compact( 'nTotalAtendimentos', 'titulo' ) );
                */
            }

        public function acao_ctrl_problemasportipocidadao_imprimir( Request $request )
            {
                $titulo = $request->titulo;
                $individualsTotais = $request->individualsTotais;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.problemasportipocidadao.view_problemasportipocidadao_imprimir', compact( 'titulo', 'individualsTotais' ) );
                
                return $pdf->stream('JUCESE-produtividade-problemasportipocidadao.pdf');
            }

    // atendimentos por CPF
        public function acao_ctrl_atendimentosporcpf_parametros( Request $request )
            {
                $tipocidadao     = Tipocidadao::all();
                $cidadao         = Cidadao::all();

                return view('callcenter.consulta.atendimentosporcpf.view_atendimentosporcpf_parametros', compact( 'tipocidadao', 'cidadao' ));
            }

        public function acao_ctrl_atendimentosporcpf_resultado( Request $request )
            {
                $validaCPF = validaCPF( retirarMascaraCPF( $request['cpf'] ) );
                if ($validaCPF === false) { return redirect()->back()->with('msg', 'C.P.F. Inválido.'); } 

                $datainicial = $request->data_inicial; $datafinal = $request->data_final; $titulo = 'Consulta Atendimentos por C.P.F. ';
                $titulo = $titulo . ': [ Limite Registros: ' . $request->max_registros . ' ] - [ Período: ' . dataDiaSemana( $datainicial ) . ' a ' . dataDiaSemana( $datafinal ) . ' ]';
                $request->cpf != null ? $titulo = $titulo . ' - [ C.P.F.: ' . mask( retirarMascaraCPF($_REQUEST['cpf']), '###.###.###-##' ) . ' ]' : '';

                $cpf = retirarMascaraCPF($_REQUEST['cpf']);

                /* 
                    $individuals = Chamada::Join( 'cidadaos as cidadaos', 'chamadas.cidadao_id', '=', 'cidadaos.id' )
                                            ->whereRaw( 'CAST(data_hora_inicio AS DATE) BETWEEN "' . $datainicial . '" AND "' . $datafinal . '"' )
                                            ->where( 'cidadaos.cpf', '=', retirarMascaraCPF($request->cpf) )
                                            ->orderBy( 'data_hora_inicio', 'DESC' )
                                            ->limit( $request->max_registros )->get();
                */
                $individualsCPF = Cidadao::where( 'cpf', retirarMascaraCPF($request->cpf) )->get();
                
                if ( $individualsCPF != null && count( $individualsCPF ) > 0 ) 
                { 
                    if ( $individualsCPF[0]->id != null ) 
                    { 
                        $individuals = Chamada::whereRaw( 'CAST(data_hora_inicio AS DATE) BETWEEN "' . $datainicial . '" AND "' . $datafinal . '" AND cidadao_id = ' . $individualsCPF[0]->id )
                                                ->orderBy( 'data_hora_inicio', 'DESC' )
                                                ->limit( $request->max_registros )->get();
                    } 
                }

                $tituloGrafico = 'Registros de Chamadas por Tipo de Cidadão';

                if ( isset( $individuals ) && count( $individuals ) > 0 )
                {
                    foreach ($individuals as $keyItem => $itemValue) { $individuals[ $keyItem ]->item = $keyItem+1; }
                    foreach( $individuals as $individual ) { $individual->diferencadatas = strtotime( $individual->data_hora_fim ) - strtotime( $individual->data_hora_inicio ); }

                    $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );
                    
                    session()->forget( [ 'individuals', 'titulo', 'totalregistros' ] );

                    $totalregistros = count( $individuals );
                    session()->put( 'totalregistros', $totalregistros );

                    session()->put( 'individuals', $individuals );
                    session()->put( 'titulo', $titulo );

                    return view('callcenter.consulta.atendimentosporcpf.view_atendimentosporcpf_resultado', compact( 'titulo' ));
                }
                else
                {
                    return redirect()->back()->with( 'msg', 'Registro não encontrado para o C.P.F. ' . $_REQUEST['cpf'] . '.' );
                }
            }

        public function acao_ctrl_atendimentosporcpf_imprimir( Request $request )
            {
                $titulo = $request->titulo;
                $nTotalAtendimentos = $request->nTotalAtendimentos;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.atendimentosporcpf.view_atendimentosporcpf_imprimir', compact( 'titulo', 'nTotalAtendimentos' ) ); //->setPaper('a4', 'portrait');
                
                return $pdf->stream('JUCESE-produtividade-atendimentosporcpf.pdf');
            }

    // painel on-line
        public function painel( Request $request )
            {
                $titulo = 'Painel On-line';

                $dataatual = date( 'Y-m-d', strtotime( now() ) );
                // $dataatual = date( 'Y-m-d', strtotime( '2020-11-11' ) );

                session()->forget( [ 
                                    'individuals',
                                    'titulo', 
                                    'totalregistros', 
                                    'graficoAtitulo', 
                                    'graficoAEixoVerticalYValoresColuna1', 
                                    'graficoAEixoHorizontalXSerieDadosColuna1', 
                                    'graficoAEixoVerticalYValoresColuna2', 
                                    'graficoAEixoHorizontalXSerieDadosColuna2', 
                                    'graficoAEixoVerticalYValoresColuna3', 
                                    'graficoAEixoHorizontalXSerieDadosColuna3', 
                                    'graficoAEixoVerticalYValoresColuna4', 
                                    'graficoAEixoHorizontalXSerieDadosColuna4'
                                ] );

                $individuals = Chamada::whereDate( 'data_hora_inicio', '=', $dataatual )
                                        ->groupBy( 'user_id' )
                                        ->selectRaw( 'gruposuser_id_red, 
                                                    user_id, 
                                                    count(*) as contador, 
                                                    count(user_id_atendeu_red) as contadorUsuario_atendeu_red, 
                                                    count(user_id_red) as contadorPorUsuario, 
                                                    count(gruposuser_id_red) as contadorGruposuser' )
                                        ->orderBy( 'user_id' )->get();

                if ( count( $individuals ) > 0 )
                {
                    foreach ($individuals as $keyItem => $itemValue)
                    {
                        $individuals[ $keyItem ]->nome_usuario = $individuals[ $keyItem ]->usuariocadastro->name;
                        $individuals[ $keyItem ]->descricao_gruposuser = $individuals[ $keyItem ]->usuariocadastro->grupo->nome;
                    }

                    $individuals = $individuals->sortBy( 'nome_usuario' );

                    $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], 
                                                                                        [ 'contador', 0 ], 
                                                                                        [ 'contadorUsuario_atendeu_red', 0 ], 
                                                                                        [ 'contadorPorUsuario', 0 ], 
                                                                                        [ 'contadorGruposuser', 0 ] ] );
                    $individuals = $arrayResultadoCalculo;

                    // inicio - dados dos gráficos
                        $grafico_A_Nome_Grafico = 'grafico1';
                        $grafico_A_Titulo = 'Atendimentos';
                        $grafico_A_Titulo_Eixo_Y = 'Qtde Atendimentos';

                        $grafico_A_Serie_1_Legenda = 'Atendimentos Por Usuário';
                        $grafico_A_Serie_2_Legenda = 'Atendimentos Transferidos para Grupos de Usuários';
                        $grafico_A_Serie_3_Legenda = 'Atendimentos Transferidos para Usuários';
                        $grafico_A_Serie_4_Legenda = 'Atendimentos Transferidos e Resolvidos';
                        $grafico_A_Serie_5_Legenda = 'Média de Atendimentos';

                        $grafico_A_Serie_1_Tipo_Grafico = 'column';
                        $grafico_A_Serie_2_Tipo_Grafico = 'column';
                        $grafico_A_Serie_3_Tipo_Grafico = 'column';
                        $grafico_A_Serie_4_Tipo_Grafico = 'column';
                        $grafico_A_Serie_5_Tipo_Grafico = 'spline';

                        foreach ( $individuals as $keyItem => $itemValue )
                        {
                            $grafico_A_Rotulos_Series_Eixo_X[] = $individuals[ $keyItem ]->nome_usuario;
                            $grafico_A_Serie_1_Dados[] = $individuals[ $keyItem ]->contador;
                            $grafico_A_Serie_2_Dados[] = $individuals[ $keyItem ]->contadorGruposuser;
                            $grafico_A_Serie_3_Dados[] = $individuals[ $keyItem ]->contadorPorUsuario;
                            $grafico_A_Serie_4_Dados[] = $individuals[ $keyItem ]->contadorUsuario_atendeu_red;
                            $grafico_A_Serie_5_Dados[] = $individuals[ $keyItem ]->contador / 2;
                        }
                        
                        $totalregistros = count( $individuals );
                        session()->put( 'totalregistros', $totalregistros );
                        session()->put( 'individuals', $individuals );
                        session()->put( 'titulo' );

                        session()->put( 'grafico_A_Nome_Grafico', $grafico_A_Nome_Grafico );
                        session()->put( 'grafico_A_Titulo', $grafico_A_Titulo );
                        session()->put( 'grafico_A_Titulo_Eixo_Y', $grafico_A_Titulo_Eixo_Y );
                        session()->put( 'grafico_A_Rotulos_Series_Eixo_X', $grafico_A_Rotulos_Series_Eixo_X );
                        session()->put( 'grafico_A_Serie_1_Legenda', $grafico_A_Serie_1_Legenda ); 
                        session()->put( 'grafico_A_Serie_2_Legenda', $grafico_A_Serie_2_Legenda ); 
                        session()->put( 'grafico_A_Serie_3_Legenda', $grafico_A_Serie_3_Legenda ); 
                        session()->put( 'grafico_A_Serie_4_Legenda', $grafico_A_Serie_4_Legenda ); 
                        session()->put( 'grafico_A_Serie_5_Legenda', $grafico_A_Serie_5_Legenda );
                        session()->put( 'grafico_A_Serie_1_Tipo_Grafico', $grafico_A_Serie_1_Tipo_Grafico ); 
                        session()->put( 'grafico_A_Serie_2_Tipo_Grafico', $grafico_A_Serie_2_Tipo_Grafico ); 
                        session()->put( 'grafico_A_Serie_3_Tipo_Grafico', $grafico_A_Serie_3_Tipo_Grafico ); 
                        session()->put( 'grafico_A_Serie_4_Tipo_Grafico', $grafico_A_Serie_4_Tipo_Grafico ); 
                        session()->put( 'grafico_A_Serie_5_Tipo_Grafico', $grafico_A_Serie_5_Tipo_Grafico );
                        session()->put( 'grafico_A_Serie_1_Dados', $grafico_A_Serie_1_Dados ); 
                        session()->put( 'grafico_A_Serie_2_Dados', $grafico_A_Serie_2_Dados ); 
                        session()->put( 'grafico_A_Serie_3_Dados', $grafico_A_Serie_3_Dados ); 
                        session()->put( 'grafico_A_Serie_4_Dados', $grafico_A_Serie_4_Dados ); 
                        session()->put( 'grafico_A_Serie_5_Dados', $grafico_A_Serie_5_Dados );
                    // final - dados dos gráficos
                }

                return view('callcenter.consulta.painelonline.index', compact( 'titulo' ) );
            }

        public function imprimir_painelonline( Request $request )
            {
                $chart = $request->chart;
                $titulo = $request->titulo;
                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.painelonline.imprimir_painelonline', compact( 'titulo' ) ); //->setPaper('a4', 'portrait');

                return $pdf->stream('JUCESE-painelonline-imprimir.pdf');
            }

    // produtividade
        public function produtividade_parametros( Request $request )
            {
                $tipocidadao   = Chamada::selectRaw( 'DISTINCT tipocidadao_id' )->where( 'tipocidadao_id', '>', 0 )->get();

                return view('callcenter.consulta.produtividade.produtividade_parametros', compact( 'tipocidadao' ));
            }

        public function produtividade_parametros_aplicar( Request $request )
            {
                $datainicial = $request->data_inicial; $anomesinicial = substr($datainicial,0,7);
                $datafinal   = $request->data_final;   $anomesfinal   = substr($datafinal,0,7);
                $titulo = 'Produtividade: Consulta de Chamadas';
                $arrayInformacoes = collect( [ 'titulo', 'individuals' ] );

                $tipoResultado = array( 'Por Ano / Mês', 
                                        'Por Data / Hora', 
                                        'Por Dia', 
                                        'Por Hora', 
                                        'Por Data / Atendente', 
                                        'Por Atendente', 
                                        'Por Tipo de Problema', 
                                        'Problemas com ocorrências > 2', 
                                        'Por Tipo de Solução', 
                                        'Soluções com ocorrências > 2', 
                                        'Clientes com chamadas > 2', 
                                        'Atendimentos por protocolo agiliza', 
                                        'Por Tipo de Cidadão' );
                                        // , 'C.P.F.s distintos por Tipo de Cidadão' );

                $complementoWhere = '';
                $complementoLeftJoin = '';

                if ( $request->tipocidadao_id == 0 )
                    { $tipocidadao_identificado = ' - [ Tipo de Cidadão: Todos ]'; }
                else
                    { $complementoWhere = $complementoWhere . ' (chamadas.tipocidadao_id = ' . $request->tipocidadao_id . ')';
                    $tipocidadaoencontrado = Tipocidadao::find($request->tipocidadao_id);
                    $tipocidadao_identificado = ' - [ Tipo de Cidadão: ' . $tipocidadaoencontrado->descricao . ' ]'; }

                $complementoWhere = $complementoWhere <> '' ? $complementoWhere . ' AND ' : '';
                $complementoWhere = $complementoWhere . ' cidadao_id is not null ';

                $titulo = $titulo . ': ' . 
                          $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . 
                          ' [ Limite Registros: ' . 
                          $request->max_registros . 
                          ' ] - [ Período: ' . 
                          dataDiaSemana( $datainicial ) . 
                          ' a ' . dataDiaSemana( $datafinal ) . 
                          ' ]' . 
                          $tipocidadao_identificado; // . $tipogruposuser_identificado

                if     ( $request->has('id_tiporesultado') && $request->id_tiporesultado == '01' ) // produtividade: por ano / mes
                    {
                        $individuals = DB::connection('mysqlCallcenter')->
                                            select( 'SELECT YEAR(chamadas.data_hora_inicio) as ano, MONTH(chamadas.data_hora_inicio) as mes, COUNT(*) as contador,
                                                            (
                                                                SELECT count( DISTINCT DATE( chamadas2.data_hora_inicio) )
                                                                FROM jucese_callcenter.chamadas chamadas2
                                                                WHERE YEAR( chamadas2.data_hora_inicio ) = ano AND MONTH( chamadas2.data_hora_inicio ) = mes
                                                            ) as diasuteis,
                                                            sum( time_to_sec( timediff( chamadas.data_hora_fim, chamadas.data_hora_inicio ) ) ) as soma
                                                    FROM jucese_callcenter.chamadas chamadas ' . $complementoLeftJoin .
                                                    ' WHERE ' . $complementoWhere . ' AND LEFT(chamadas.data_hora_inicio,7) BETWEEN "' . $anomesinicial . '" AND "' . $anomesfinal . '"' .
                                                    ' GROUP BY YEAR(chamadas.data_hora_inicio), MONTH(chamadas.data_hora_inicio)
                                                    ORDER BY YEAR(chamadas.data_hora_inicio), MONTH(chamadas.data_hora_inicio) LIMIT ' . $request->max_registros );
                        
                        if ( count( $individuals ) > 0 )
                        {
                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'ano', 0 ], [ 'mes', 0 ], [ 'contador', 0 ], [ 'diasuteis', 0 ], [ 'soma', 0 ] ] );

                            $individuals = $arrayResultadoCalculo;
                            foreach ( $individuals as $keyItem => $itemValue )
                            {
                                $percentual = ( $individuals[ $keyItem ]->contador / $individuals[ $keyItem ]->totalcontador ) * 100;
                                $individuals[ $keyItem ]->percentual = $percentual;
                                $mediaAtendimentoIndividual = ( $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador );
                                $individuals[ $keyItem ]->mediaAtendimentoIndividual = $mediaAtendimentoIndividual;
                                $media = ( $individuals[ $keyItem ]->contador / $individuals[ $keyItem ]->diasuteis );
                                $individuals[ $keyItem ]->media = $media;
                                $tempomedio = ( $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador );
                                $individuals[ $keyItem ]->tempomedio = $tempomedio;
                            }

                            $grafico_A_Nome_Grafico = 'grafico1';
                            $grafico_A_Titulo = 'Atendimentos';
                            $grafico_A_Titulo_Eixo_Y = 'Qtde Atendimentos';
                            $grafico_A_Serie_1_Legenda = 'Qtde Atendimentos';
                            $grafico_A_Serie_2_Legenda = 'Média de Atendimentos / Dia';
                            $grafico_A_Serie_1_Tipo_Grafico = 'column';
                            $grafico_A_Serie_2_Tipo_Grafico = 'spline';
        
                            $grafico_B_Nome_Grafico = 'grafico2';
                            $grafico_B_Titulo = 'Tempo Médio de Atendimento';
                            $grafico_B_Titulo_Eixo_Y = 'Tempo';
                            $grafico_B_Serie_1_Legenda = 'Média';
                            $grafico_B_Serie_1_Tipo_Grafico = 'column';

                            foreach ( $individuals as $keyItem => $itemValue )
                            {
                                $grafico_A_Rotulos_Series_Eixo_X[] = meses( $individuals[ $keyItem ]->mes -1 ) . '/' . $individuals[ $keyItem ]->ano;
                                $grafico_A_Serie_1_Dados[] = $individuals[ $keyItem ]->contador;
                                $grafico_A_Serie_2_Dados[] = number_format( $individuals[ $keyItem ]->contador / $individuals[ $keyItem ]->diasuteis, 0, ',', '.' );

                                $grafico_B_Rotulos_Series_Eixo_X[] = meses( $individuals[ $keyItem ]->mes -1 ) . '/' . $individuals[ $keyItem ]->ano;
                                $grafico_B_Serie_1_Dados[] = $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador;
                            }
        
                            $totalregistros = count( $individuals );
                            session()->put( 'individuals', $individuals );
                            session()->put( 'totalregistros', $totalregistros );

                            session()->put( 'grafico_A_Nome_Grafico', $grafico_A_Nome_Grafico );
                            session()->put( 'grafico_A_Titulo', $grafico_A_Titulo );
                            session()->put( 'grafico_A_Titulo_Eixo_Y', $grafico_A_Titulo_Eixo_Y );
                            session()->put( 'grafico_A_Rotulos_Series_Eixo_X', $grafico_A_Rotulos_Series_Eixo_X );
                            session()->put( 'grafico_A_Serie_1_Legenda', $grafico_A_Serie_1_Legenda );
                            session()->put( 'grafico_A_Serie_2_Legenda', $grafico_A_Serie_2_Legenda );
                            session()->put( 'grafico_A_Serie_1_Tipo_Grafico', $grafico_A_Serie_1_Tipo_Grafico );
                            session()->put( 'grafico_A_Serie_2_Tipo_Grafico', $grafico_A_Serie_2_Tipo_Grafico );
                            session()->put( 'grafico_A_Serie_1_Dados', $grafico_A_Serie_1_Dados );
                            session()->put( 'grafico_A_Serie_2_Dados', $grafico_A_Serie_2_Dados );

                            session()->put( 'grafico_B_Nome_Grafico', $grafico_A_Nome_Grafico );
                            session()->put( 'grafico_B_Titulo', $grafico_A_Titulo );
                            session()->put( 'grafico_B_Titulo_Eixo_Y', $grafico_A_Titulo_Eixo_Y );
                            session()->put( 'grafico_B_Rotulos_Series_Eixo_X', $grafico_A_Rotulos_Series_Eixo_X );
                            session()->put( 'grafico_B_Serie_1_Legenda', $grafico_A_Serie_1_Legenda );
                            session()->put( 'grafico_B_Serie_1_Tipo_Grafico', $grafico_A_Serie_1_Tipo_Grafico );
                            session()->put( 'grafico_B_Serie_1_Dados', $grafico_A_Serie_1_Dados );

                            return view('callcenter.consulta.produtividade.poranomes', compact( 'titulo' ) );
                        }
                        else
                        {
                            return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta ' . $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . '.' );
                        }
                    }
                elseif ( $request->has('id_tiporesultado') && $request->id_tiporesultado == '02' ) // produtividade: por data / hora
                    {
                        $complementoqueryEstrutura = '';
                        $queryEstrutura = 'SELECT DATE(chamadas.data_hora_inicio) as datas, HOUR(chamadas.data_hora_inicio) as hora, COUNT(*) as contador,
                                        sum( time_to_sec( timediff(chamadas.data_hora_fim, chamadas.data_hora_inicio) ) ) as soma
                                        FROM jucese_callcenter.chamadas chamadas ' . $complementoLeftJoin .
                                        ' WHERE ' . $complementoWhere . ' AND (date_format(data_hora_inicio, "%Y-%m-%d") BETWEEN "' . $datainicial . '" AND "' . $datafinal . '")';

                        $request->gruposuser_id != 0 ? $queryEstrutura = $queryEstrutura . ' AND gruposuser.id = ' . $request->gruposuser_id : '';
                        $request->tipocidadao_id != 0 ? $queryEstrutura = $queryEstrutura . ' AND chamadas.tipocidadao_id = ' . $request->tipocidadao_id : '';

                        $queryEstrutura = $queryEstrutura . ' GROUP BY DATE(chamadas.data_hora_inicio), HOUR(chamadas.data_hora_inicio)' .
                                                            ' ORDER BY 1, 2' . ' LIMIT ' . $request->max_registros;

                        $individuals = DB::connection('mysqlCallcenter')->select( $queryEstrutura );

                        if ( count( $individuals ) > 0 ) 
                        {
                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ], [ 'soma', 0 ] ] );
                            $individuals = $arrayResultadoCalculo;

                            session()->forget( [ 'individuals', 'titulo', 'totalregistros', 'tituloGrafico', 'eixoX', 'textoEixoY', 'valoresEixoYCategoria1', 'tituloSeries' ] );

                            // inicio - dados dos gráficos
                                $grafico_A_Nome_Grafico = 'grafico1';
                                $grafico_A_Titulo = 'Atendimentos por Data / Horário';
                                $grafico_A_Titulo_Eixo_Y = 'Qtde Atendimentos';

                                $grafico_A_Serie_1_Legenda = 'Data / Horário';

                                $grafico_A_Serie_1_Tipo_Grafico = 'column';

                                $totaltempomedioatendimento = 0;
                                foreach ( $individuals as $keyItem => $itemValue )
                                {
                                    $individuals[ $keyItem ]->tempomedioatendimento = $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador;
                                    $totaltempomedioatendimento = $totaltempomedioatendimento + ( $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador );

                                    $grafico_A_Rotulos_Series_Eixo_X[] = datahoraBR( $individuals[ $keyItem ]->datas ) . ' - ' . $individuals[ $keyItem ]->hora . 'h as ' . $individuals[ $keyItem ]->hora . 'h59';
                                    $grafico_A_Serie_1_Dados[] = $individuals[ $keyItem ]->contador;
                                }
                                foreach ( $individuals as $keyItem => $itemValue )
                                {
                                    $individuals[ $keyItem ]->totaltempomedioatendimento = $totaltempomedioatendimento / $individuals[ $keyItem ]->totalitens;
                                }
                                
                                $totalregistros = count( $individuals );
                                session()->put( 'totalregistros', $totalregistros );
                                session()->put( 'individuals', $individuals );
                                session()->put( 'titulo' );

                                session()->put( 'grafico_A_Nome_Grafico', $grafico_A_Nome_Grafico );
                                session()->put( 'grafico_A_Titulo', $grafico_A_Titulo );
                                session()->put( 'grafico_A_Titulo_Eixo_Y', $grafico_A_Titulo_Eixo_Y );
                                session()->put( 'grafico_A_Rotulos_Series_Eixo_X', $grafico_A_Rotulos_Series_Eixo_X );
                                session()->put( 'grafico_A_Serie_1_Legenda', $grafico_A_Serie_1_Legenda ); 
                                session()->put( 'grafico_A_Serie_1_Tipo_Grafico', $grafico_A_Serie_1_Tipo_Grafico ); 
                                session()->put( 'grafico_A_Serie_1_Dados', $grafico_A_Serie_1_Dados ); 
                            // final - dados dos gráficos

                            return view('callcenter.consulta.produtividade.pordatahora', compact( 'titulo' ) );
                        }
                        else {
                            return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta ' . $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . '.' );
                        }
                    }
                elseif ( $request->has('id_tiporesultado') && $request->id_tiporesultado == '03' ) // produtividade: por dia
                    {
                        $individuals = DB::connection('mysqlCallcenter')->
                                            select( 'SELECT DATE(chamadas.data_hora_inicio) as dataprocessada, COUNT(*) as contador, SUM( time_to_sec( timediff(chamadas.data_hora_fim, chamadas.data_hora_inicio) ) ) as soma
                                                    FROM jucese_callcenter.chamadas chamadas ' . $complementoLeftJoin .
                                                    ' WHERE ' . $complementoWhere . ' AND (date_format(data_hora_inicio, "%Y-%m-%d") BETWEEN "' . $datainicial . '" AND "' . $datafinal . '")' .
                                                    ' GROUP BY DATE(data_hora_inicio)' . ' ORDER BY DATE(data_hora_inicio)' . ' LIMIT ' . $request->max_registros );

                        if ( count( $individuals ) > 0 )
                        {
                            $nTotalAtendimentos = 0;
                            foreach ($individuals as $keyItem => $itemValue)
                            { $nTotalAtendimentos = $nTotalAtendimentos + $individuals[ $keyItem ]->contador; $individuals[ $keyItem ]->item = $keyItem+1; }

                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ], [ 'soma', 0 ] ] );

                            $tituloGrafico = 'Atendimentos por Dia';
                            $tituloSeries = 'Data';
                            $eixoX = 'Qtde Atendimentos';
                            $eixoY = 'Registros de Chamadas por Dia';
                            
                            $textoEixoY = array();
                            $valoresEixoYCategoria1 = array();

                            $totaltempomedioatendimento = 0;
                            foreach ( $individuals as $keyItem => $itemValue )
                            {
                                $individuals[ $keyItem ]->tempomedioatendimento = $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador;
                                $totaltempomedioatendimento = $totaltempomedioatendimento + ( $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador );

                                $individuals[ $keyItem ]->percentualatendimento = ( $individuals[ $keyItem ]->contador / $individuals[ $keyItem ]->totalcontador ) * 100;

                                $textoEixoY[] = datahoraBR( $individuals[ $keyItem ]->dataprocessada );
                                $valoresEixoYCategoria1[] = $individuals[ $keyItem ]->contador;
                            }
                            foreach ( $individuals as $keyItem => $itemValue )
                            {
                                $individuals[ $keyItem ]->totaltempomedioatendimento = $totaltempomedioatendimento / $individuals[ $keyItem ]->totalitens;
                            }

                            session()->forget( [ 'individuals', 'titulo', 'totalregistros', 'tituloGrafico', 'eixoX', 'textoEixoY', 'valoresEixoYCategoria1', 'tituloSeries' ] );

                            $totalregistros = count( $individuals );
                            session()->put( 'totalregistros', $totalregistros );
        
                            session()->put( 'individuals', $individuals );
                            session()->put( 'titulo', $titulo );
                            session()->put( 'tituloGrafico', $tituloGrafico );
                            session()->put( 'eixoX', $eixoX );
                            session()->put( 'textoEixoY', $textoEixoY );
                            session()->put( 'valoresEixoYCategoria1', $valoresEixoYCategoria1 );
                            session()->put( 'tituloSeries', $tituloSeries );

                            /*
                                session()->put('individuals', $individuals);
                            */

                            return view('callcenter.consulta.produtividade.pordia', compact( 'titulo' ) );
                        }
                        else
                        {
                            return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta ' . $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . '.' );
                        }
                    }
                elseif ( $request->has('id_tiporesultado') && $request->id_tiporesultado == '04' ) // produtividade: por hora - ok
                    {
                        $individuals = DB::connection('mysqlCallcenter')->
                                            select( 'SELECT HOUR(chamadas.data_hora_inicio) as hora, 
                                                    COUNT(*) as contador, 
                                                    SUM( time_to_sec( timediff(chamadas.data_hora_fim, chamadas.data_hora_inicio) ) ) as soma
                                                    FROM jucese_callcenter.chamadas chamadas ' . $complementoLeftJoin .
                                                    ' WHERE ' . $complementoWhere . ' AND (date_format(data_hora_inicio, "%Y-%m-%d") BETWEEN "' . $datainicial . '" AND "' . $datafinal . '")' .
                                                    ' GROUP BY HOUR(chamadas.data_hora_inicio)' . ' ORDER BY 1 ASC LIMIT ' . $request->max_registros );

                        if ( count( $individuals ) > 0 )
                        {
                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ], [ 'soma', 0 ] ] );

                            session()->forget( [ 'individuals', 'titulo', 'totalregistros', 'tituloGrafico', 'eixoX', 'textoEixoY', 'valoresEixoYCategoria1', 'tituloSeries' ] );

                            // inicio - dados dos gráficos
                                $grafico_A_Nome_Grafico = 'grafico1';
                                $grafico_A_Titulo = 'Atendimentos por Horário';
                                $grafico_A_Titulo_Eixo_Y = 'Qtde Atendimentos';
                                $grafico_A_Serie_1_Legenda = 'Horário';
                                $grafico_A_Serie_1_Tipo_Grafico = 'column';

                                $totaltempomedioatendimento = 0;
                                foreach ( $individuals as $keyItem => $itemValue )
                                {
                                    $individuals[ $keyItem ]->tempomedioatendimento = $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador;
                                    $totaltempomedioatendimento = $totaltempomedioatendimento + ( $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador );

                                    $grafico_A_Rotulos_Series_Eixo_X[] = $individuals[ $keyItem ]->hora . 'h às ' . (string)($individuals[ $keyItem ]->hora + 1) . 'h';
                                    $grafico_A_Serie_1_Dados[] = $individuals[ $keyItem ]->contador;
                                }
                                foreach ( $individuals as $keyItem => $itemValue )
                                {
                                    $individuals[ $keyItem ]->totaltempomedioatendimento = $totaltempomedioatendimento / $individuals[ $keyItem ]->totalitens;
                                }
    
                                $totalregistros = count( $individuals );
                                session()->put( 'totalregistros', $totalregistros );
                                session()->put( 'individuals', $individuals );
                                session()->put( 'titulo' );

                                session()->put( 'grafico_A_Nome_Grafico', $grafico_A_Nome_Grafico );
                                session()->put( 'grafico_A_Titulo', $grafico_A_Titulo );
                                session()->put( 'grafico_A_Titulo_Eixo_Y', $grafico_A_Titulo_Eixo_Y );
                                session()->put( 'grafico_A_Rotulos_Series_Eixo_X', $grafico_A_Rotulos_Series_Eixo_X );
                                session()->put( 'grafico_A_Serie_1_Legenda', $grafico_A_Serie_1_Legenda ); 
                                session()->put( 'grafico_A_Serie_1_Tipo_Grafico', $grafico_A_Serie_1_Tipo_Grafico ); 
                                session()->put( 'grafico_A_Serie_1_Dados', $grafico_A_Serie_1_Dados ); 
                            // final - dados dos gráficos

                            return view('callcenter.consulta.produtividade.porhora', compact( 'titulo' ) );
                        }
                        else
                        {
                            return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta ' . $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . '.' );
                        }
                    }
                elseif ( $request->has('id_tiporesultado') && $request->id_tiporesultado == '05' ) // produtividade: por data / atendente DB::connection('mysqlCallcenter')->
                    {
                        $individuals = Chamada::select( 'user_id' )
                                                ->whereRaw( 'date_format(data_hora_inicio, "%Y-%m-%d") BETWEEN "' . $datainicial . '" AND "' . $datafinal . '"' )
                                                ->whereRaw( $complementoWhere )->groupBy( 'user_id' )->groupBy( 'data' )
                                                ->selectRaw( 'date_format(data_hora_inicio, "%Y-%m-%d") as data' )
                                                ->selectRaw( 'SUM( time_to_sec( timediff(data_hora_fim, data_hora_inicio) ) ) as soma, count(*) as contador' )
                                                ->orderBy( 'data' )->orderByDesc( 'contador' )->limit( $request->max_registros )->get();
                        
                        if ( count( $individuals ) > 0 )
                        {
                            foreach ($individuals as $keyItem => $itemValue)
                            { $individuals[ $keyItem ]->nomeusuario = isset( $individuals[ $keyItem ]->usuariocadastro->name ) ? $individuals[ $keyItem ]->usuariocadastro->name : '';
                            $individuals[ $keyItem ]->item = $keyItem+1; }

                            // $individuals = $individuals->sortBy( 'data' )->sortBy( 'nomeusuario' );

                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ], [ 'soma', 0 ] ] );
                            $individuals = $arrayResultadoCalculo;

                            $tituloGrafico = 'Atendimentos por Data / Atendente';
                            $tituloSeries = 'Data - Atendente';
                            $eixoX = 'Qtde Atendimentos';
                            $eixoY = 'Registros de Chamadas por Data / Atendente';
                            
                            $textoEixoY = array();
                            $valoresEixoYCategoria1 = array();

                            $totaltempomedioatendimento = 0;
                            foreach ( $individuals as $keyItem => $itemValue )
                            {
                                $individuals[ $keyItem ]->tempomedioatendimento = $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador;
                                $totaltempomedioatendimento = $totaltempomedioatendimento + ( $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador );

                                $textoEixoY[] = datahoraBR( $individuals[ $keyItem ]->data ) . ' - ' . $individuals[ $keyItem ]->usuariocadastro->name;
                                $valoresEixoYCategoria1[] = $individuals[ $keyItem ]->contador;
                            }
                            foreach ( $individuals as $keyItem => $itemValue )
                            {
                                $individuals[ $keyItem ]->totaltempomedioatendimento = $totaltempomedioatendimento / $individuals[ $keyItem ]->totalitens;
                            }

                            session()->forget( [ 'individuals', 'titulo', 'totalregistros', 'tituloGrafico', 'eixoX', 'textoEixoY', 'valoresEixoYCategoria1', 'tituloSeries' ] );

                            $totalregistros = count( $individuals );
                            session()->put( 'totalregistros', $totalregistros );
        
                            session()->put( 'individuals', $individuals );
                            session()->put( 'titulo', $titulo );
                            session()->put( 'tituloGrafico', $tituloGrafico );
                            session()->put( 'eixoX', $eixoX );
                            session()->put( 'textoEixoY', $textoEixoY );
                            session()->put( 'valoresEixoYCategoria1', $valoresEixoYCategoria1 );
                            session()->put( 'tituloSeries', $tituloSeries );

                            /*
                            session()->put('individuals', $individuals);
                            */

                            return view('callcenter.consulta.produtividade.pordataatendente', compact( 'titulo' ) );
                        }
                        else
                        {
                            return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta ' . $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . '.' );
                        }
                    }
                elseif ( $request->has('id_tiporesultado') && $request->id_tiporesultado == '06' ) // produtividade: por atendente
                    {
                        $individuals = Chamada::whereRaw( 'CAST(data_hora_inicio AS DATE) BETWEEN "' . $datainicial . '" AND "' . $datafinal . '"' )
                                                ->whereRaw( $complementoWhere )->groupBy( 'user_id' )
                                                ->selectRaw( 'user_id, SUM( time_to_sec( timediff(data_hora_fim, data_hora_inicio) ) ) as soma, count(*) as contador' )
                                                ->orderBy( 'user_id' )->limit( $request->max_registros )->get();

                        if ( count( $individuals ) > 0 )
                        {
                            // calcular o total de atendimentos
                            // $nTotalAtendimentos = 0;
                            foreach ($individuals as $keyItem => $itemValue) 
                            { 
                                // $nTotalAtendimentos = $nTotalAtendimentos + $individuals[ $keyItem ]->contador; 
                                $individuals[ $keyItem ]->nomeusuario = isset( $individuals[ $keyItem ]->usuariocadastro->name ) ? $individuals[ $keyItem ]->usuariocadastro->name : $individuals[ $keyItem ]->user_id;
                            }
                            $individuals = $individuals->sortBy( 'nomeusuario' );


                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ], [ 'soma', 0 ] ] );
                            $individuals = $arrayResultadoCalculo;

                            $totaltempomedioatendimento = 0;
                            foreach ($individuals as $keyItem => $itemValue) 
                            { 
                                $individuals[ $keyItem ]->tempomedioatendimento = $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador;
                                $totaltempomedioatendimento = $totaltempomedioatendimento + ( $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador );

                                $individuals[ $keyItem ]->percentualatendimento = ( $individuals[ $keyItem ]->contador / $individuals[ $keyItem ]->totalcontador ) * 100;

                                $rotulos1Grafico[] = $individuals[ $keyItem ]['nomeusuario']; 
                                $valores1Grafico[] = $individuals[ $keyItem ]['contador']; 
                            }
                            foreach ( $individuals as $keyItem => $itemValue )
                            {
                                $individuals[ $keyItem ]->totaltempomedioatendimento = $totaltempomedioatendimento / $individuals[ $keyItem ]->totalitens;
                            }

                            $tituloGrafico = 'Registros de Chamadas por Atendente';

                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ], [ 'soma', 0 ] ] );
                            $individuals = $arrayResultadoCalculo;

                            session()->forget( [ 'individuals', 'titulo', 'totalregistros', 'tituloGrafico', 'eixoX', 'textoEixoY', 'valoresEixoYCategoria1', 'tituloSeries' ] );

                            // inicio - dados dos gráficos
                                $grafico_A_Nome_Grafico = 'grafico1';
                                $grafico_A_Titulo = 'Atendimentos';
                                $grafico_A_Titulo_Eixo_Y = 'Qtde Atendimentos';
                                $grafico_A_Serie_1_Legenda = 'Atendentes';
                                $grafico_A_Serie_1_Tipo_Grafico = 'column';

                                foreach ( $individuals as $keyItem => $itemValue )
                                {
                                    $grafico_A_Rotulos_Series_Eixo_X[] = $individuals[ $keyItem ]->nomeusuario;
                                    $grafico_A_Serie_1_Dados[] = $individuals[ $keyItem ]->contador;
                                }
                                
                                $totalregistros = count( $individuals );
                                session()->put( 'totalregistros', $totalregistros );
                                session()->put( 'individuals', $individuals );
                                session()->put( 'titulo' );

                                session()->put( 'grafico_A_Nome_Grafico', $grafico_A_Nome_Grafico );
                                session()->put( 'grafico_A_Titulo', $grafico_A_Titulo );
                                session()->put( 'grafico_A_Titulo_Eixo_Y', $grafico_A_Titulo_Eixo_Y );
                                session()->put( 'grafico_A_Rotulos_Series_Eixo_X', $grafico_A_Rotulos_Series_Eixo_X );
                                session()->put( 'grafico_A_Serie_1_Legenda', $grafico_A_Serie_1_Legenda ); 
                                session()->put( 'grafico_A_Serie_1_Tipo_Grafico', $grafico_A_Serie_1_Tipo_Grafico ); 
                                session()->put( 'grafico_A_Serie_1_Dados', $grafico_A_Serie_1_Dados ); 
                            // final - dados dos gráficos
                            return view('callcenter.consulta.produtividade.poratendente', compact( 'titulo' ) );
                        }
                        else
                        {
                            return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta ' . $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . '.' );
                        }
                    }
                elseif ( $request->has('id_tiporesultado') && $request->id_tiporesultado == '07' ) // produtividade: por tipo de problema
                    {
                        $individuals = DB::connection('mysqlCallcenter')->
                                            select( 'SELECT tipoproblemas.descricao as descricao, count(*) as contador
                                                    FROM jucese_callcenter.chamadas chamadas ' . $complementoLeftJoin .
                                                    ' LEFT JOIN jucese_callcenter.tipoproblemas tipoproblemas ON chamadas.tipoproblema_id = tipoproblemas.id
                                                    WHERE ' . $complementoWhere . ' AND (date_format(data_hora_inicio, "%Y-%m-%d") BETWEEN "' . $datainicial . '" AND "' . $datafinal . '")' .
                                                    ' GROUP BY descricao ORDER BY 2 DESC' . ' LIMIT ' . $request->max_registros );

                        if ( count( $individuals ) > 0 )
                        {
                            $nTotalAtendimentos = 0;
                            foreach ( $individuals as $individual ) { $nTotalAtendimentos = $nTotalAtendimentos + $individual->contador; }

                            foreach ($individuals as $keyItem => $itemValue) { $individuals[ $keyItem ]->item = $keyItem+1; }

                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );
                            $individuals = $arrayResultadoCalculo;

                            $tituloGrafico = 'Atendimentos por Tipo de Problema';
                            $tituloSeries = 'Tipos de Problema';
                            $eixoX = 'Qtde Atendimentos';
                            $eixoY = 'Registros de Chamadas por Tipo de Problema';
                            
                            session()->forget( [ 'individuals', 'titulo', 'totalregistros', 'tituloGrafico', 'eixoX', 'textoEixoY', 'valoresEixoYCategoria1', 'tituloSeries' ] );

                            // inicio - dados dos gráficos
                                $grafico_A_Nome_Grafico = 'grafico1';
                                $grafico_A_Titulo = 'Atendimentos';
                                $grafico_A_Titulo_Eixo_Y = 'Qtde Atendimentos';
                                $grafico_A_Serie_1_Legenda = 'Tipo de Problema';
                                $grafico_A_Serie_1_Tipo_Grafico = 'column';

                                foreach ( $individuals as $keyItem => $itemValue )
                                {
                                    $individuals[ $keyItem ]->percentualatendimento = ( $individuals[ $keyItem ]->contador / $individuals[ $keyItem ]->totalcontador ) * 100;

                                    $grafico_A_Rotulos_Series_Eixo_X[] = $individuals[ $keyItem ]->descricao;
                                    $grafico_A_Serie_1_Dados[] = $individuals[ $keyItem ]->contador;
                                }
                                
                                $totalregistros = count( $individuals );
                                session()->put( 'totalregistros', $totalregistros );
                                session()->put( 'individuals', $individuals );
                                session()->put( 'titulo' );

                                session()->put( 'grafico_A_Nome_Grafico', $grafico_A_Nome_Grafico );
                                session()->put( 'grafico_A_Titulo', $grafico_A_Titulo );
                                session()->put( 'grafico_A_Titulo_Eixo_Y', $grafico_A_Titulo_Eixo_Y );
                                session()->put( 'grafico_A_Rotulos_Series_Eixo_X', $grafico_A_Rotulos_Series_Eixo_X );
                                session()->put( 'grafico_A_Serie_1_Legenda', $grafico_A_Serie_1_Legenda ); 
                                session()->put( 'grafico_A_Serie_1_Tipo_Grafico', $grafico_A_Serie_1_Tipo_Grafico ); 
                                session()->put( 'grafico_A_Serie_1_Dados', $grafico_A_Serie_1_Dados ); 
                            // final - dados dos gráficos

                            return view('callcenter.consulta.produtividade.portipoproblema', compact( 'titulo' ) );
                        }
                        else
                        {
                            return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta ' . $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . '.' );
                        }
                    }
                elseif ( $request->has('id_tiporesultado') && $request->id_tiporesultado == '08' ) // produtividade: problemas com ocorrências > 2
                    {
                        $individuals = DB::connection('mysqlCallcenter')->
                                            select( 'SELECT tipoproblemas.descricao as descricao, COUNT(*) as contador
                                                    FROM jucese_callcenter.chamadas chamadas ' . $complementoLeftJoin .
                                                    ' LEFT JOIN jucese_callcenter.tipoproblemas tipoproblemas ON chamadas.tipoproblema_id = tipoproblemas.id
                                                    WHERE ' . $complementoWhere . ' AND (date_format(data_hora_inicio, "%Y-%m-%d") BETWEEN "' . $datainicial . '" AND "' . $datafinal . '")' .
                                                    ' GROUP BY tipoproblemas.descricao HAVING contador > 2 ORDER BY 2 DESC' . 
                                                    ' LIMIT ' . $request->max_registros );

                        if ( count( $individuals ) > 0 )
                        {
                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );
                            $individuals = $arrayResultadoCalculo;

                            session()->forget( [ 'individuals', 'titulo', 'totalregistros', 'tituloGrafico', 'eixoX', 'textoEixoY', 'valoresEixoYCategoria1', 'tituloSeries' ] );

                            // inicio - dados dos gráficos
                                $grafico_A_Nome_Grafico = 'grafico1';
                                $grafico_A_Titulo = 'Problemas com qtde de ocorrências maiores que 2';
                                $grafico_A_Titulo_Eixo_Y = 'Qtde Atendimentos';
                                $grafico_A_Serie_1_Legenda = 'Tipo de Problema';
                                $grafico_A_Serie_1_Tipo_Grafico = 'column';

                                foreach ( $individuals as $keyItem => $itemValue )
                                {
                                    $grafico_A_Rotulos_Series_Eixo_X[] = $individuals[ $keyItem ]->descricao;
                                    $grafico_A_Serie_1_Dados[] = $individuals[ $keyItem ]->contador;
                                }
                                
                                $totalregistros = count( $individuals );
                                session()->put( 'totalregistros', $totalregistros );
                                session()->put( 'individuals', $individuals );
                                session()->put( 'titulo' );

                                session()->put( 'grafico_A_Nome_Grafico', $grafico_A_Nome_Grafico );
                                session()->put( 'grafico_A_Titulo', $grafico_A_Titulo );
                                session()->put( 'grafico_A_Titulo_Eixo_Y', $grafico_A_Titulo_Eixo_Y );
                                session()->put( 'grafico_A_Rotulos_Series_Eixo_X', $grafico_A_Rotulos_Series_Eixo_X );
                                session()->put( 'grafico_A_Serie_1_Legenda', $grafico_A_Serie_1_Legenda ); 
                                session()->put( 'grafico_A_Serie_1_Tipo_Grafico', $grafico_A_Serie_1_Tipo_Grafico ); 
                                session()->put( 'grafico_A_Serie_1_Dados', $grafico_A_Serie_1_Dados ); 
                            // final - dados dos gráficos

                            return view('callcenter.consulta.produtividade.problemascomocorrencias', compact( 'titulo' ) );
                        }
                        else
                        {
                            return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta ' . $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . '.' );
                        }
                    }
                elseif ( $request->has('id_tiporesultado') && $request->id_tiporesultado == '09' ) // produtividade: por tipo de solução
                    {
                        $individuals = DB::connection('mysqlCallcenter')->
                                            select( 'SELECT tiposolucaos.descricao as descricao, count(*) as contador
                                                    FROM jucese_callcenter.chamadas chamadas ' . $complementoLeftJoin .
                                                    ' LEFT JOIN jucese_callcenter.tiposolucaos tiposolucaos ON chamadas.tiposolucao_id = tiposolucaos.id
                                                    WHERE ' . $complementoWhere . ' AND (date_format(data_hora_inicio, "%Y-%m-%d") BETWEEN "' . $datainicial . '" AND "' . $datafinal . '")' .
                                                    ' GROUP BY descricao ORDER BY 2 DESC LIMIT ' . $request->max_registros );

                        if ( count( $individuals ) > 0 )
                        {
                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );
                            $individuals = $arrayResultadoCalculo;

                            session()->forget( [ 'individuals', 'titulo', 'totalregistros', 'tituloGrafico', 'eixoX', 'textoEixoY', 'valoresEixoYCategoria1', 'tituloSeries' ] );

                            // inicio - dados dos gráficos
                                $grafico_A_Nome_Grafico = 'grafico1';
                                $grafico_A_Titulo = 'Atendimentos';
                                $grafico_A_Titulo_Eixo_Y = 'Qtde Atendimentos';
                                $grafico_A_Serie_1_Legenda = 'Tipo de Solução';
                                $grafico_A_Serie_1_Tipo_Grafico = 'column';

                                foreach ( $individuals as $keyItem => $itemValue )
                                {
                                    $individuals[ $keyItem ]->percentualatendimento = ( $individuals[ $keyItem ]->contador / $individuals[ $keyItem ]->totalcontador ) * 100;

                                    $grafico_A_Rotulos_Series_Eixo_X[] = $individuals[ $keyItem ]->descricao;
                                    $grafico_A_Serie_1_Dados[] = $individuals[ $keyItem ]->contador;
                                }
                                
                                $totalregistros = count( $individuals );
                                session()->put( 'totalregistros', $totalregistros );
                                session()->put( 'individuals', $individuals );
                                session()->put( 'titulo' );

                                session()->put( 'grafico_A_Nome_Grafico', $grafico_A_Nome_Grafico );
                                session()->put( 'grafico_A_Titulo', $grafico_A_Titulo );
                                session()->put( 'grafico_A_Titulo_Eixo_Y', $grafico_A_Titulo_Eixo_Y );
                                session()->put( 'grafico_A_Rotulos_Series_Eixo_X', $grafico_A_Rotulos_Series_Eixo_X );
                                session()->put( 'grafico_A_Serie_1_Legenda', $grafico_A_Serie_1_Legenda ); 
                                session()->put( 'grafico_A_Serie_1_Tipo_Grafico', $grafico_A_Serie_1_Tipo_Grafico ); 
                                session()->put( 'grafico_A_Serie_1_Dados', $grafico_A_Serie_1_Dados ); 
                            // final - dados dos gráficos

                            return view('callcenter.consulta.produtividade.portiposolucao', compact( 'titulo' ) );
                        }
                        else
                        {
                            return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta ' . $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . '.' );
                        }
                    }
                elseif ( $request->has('id_tiporesultado') && $request->id_tiporesultado == '10' ) // produtividade: soluções com ocorrências > 2
                    {
                        $individuals = DB::connection('mysqlCallcenter')->
                                            select( 'SELECT tiposolucaos.descricao as descricao, COUNT(*) as contador
                                                    FROM jucese_callcenter.chamadas chamadas ' . $complementoLeftJoin .
                                                    ' LEFT JOIN jucese_callcenter.tiposolucaos tiposolucaos ON chamadas.tiposolucao_id = tiposolucaos.id
                                                    WHERE ' . $complementoWhere . ' AND (date_format(data_hora_inicio, "%Y-%m-%d") BETWEEN "' . $datainicial . '" AND "' . $datafinal . '")' .
                                                    ' GROUP BY tiposolucaos.descricao HAVING contador > 2 ORDER BY 2 DESC LIMIT ' . $request->max_registros );

                        if ( count( $individuals ) > 0 )
                        {
                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );
                            $individuals = $arrayResultadoCalculo;

                            session()->forget( [ 'individuals', 'titulo', 'totalregistros', 'tituloGrafico', 'eixoX', 'textoEixoY', 'valoresEixoYCategoria1', 'tituloSeries' ] );

                            // inicio - dados dos gráficos
                                $grafico_A_Nome_Grafico = 'grafico1';
                                $grafico_A_Titulo = 'Soluções com qtde de ocorrências maiores que 2';
                                $grafico_A_Titulo_Eixo_Y = 'Qtde Atendimentos';
                                $grafico_A_Serie_1_Legenda = 'Tipo de Solução';
                                $grafico_A_Serie_1_Tipo_Grafico = 'column';

                                foreach ( $individuals as $keyItem => $itemValue )
                                {
                                    $grafico_A_Rotulos_Series_Eixo_X[] = $individuals[ $keyItem ]->descricao;
                                    $grafico_A_Serie_1_Dados[] = $individuals[ $keyItem ]->contador;
                                }
                                
                                $totalregistros = count( $individuals );
                                session()->put( 'totalregistros', $totalregistros );
                                session()->put( 'individuals', $individuals );
                                session()->put( 'titulo' );

                                session()->put( 'grafico_A_Nome_Grafico', $grafico_A_Nome_Grafico );
                                session()->put( 'grafico_A_Titulo', $grafico_A_Titulo );
                                session()->put( 'grafico_A_Titulo_Eixo_Y', $grafico_A_Titulo_Eixo_Y );
                                session()->put( 'grafico_A_Rotulos_Series_Eixo_X', $grafico_A_Rotulos_Series_Eixo_X );
                                session()->put( 'grafico_A_Serie_1_Legenda', $grafico_A_Serie_1_Legenda ); 
                                session()->put( 'grafico_A_Serie_1_Tipo_Grafico', $grafico_A_Serie_1_Tipo_Grafico ); 
                                session()->put( 'grafico_A_Serie_1_Dados', $grafico_A_Serie_1_Dados ); 
                            // final - dados dos gráficos

                            return view('callcenter.consulta.produtividade.solucoescomocorrencias', compact( 'titulo' ) );
                        }
                        else
                        {
                            return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta ' . $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . '.' );
                        }
                    }
                elseif ( $request->has('id_tiporesultado') && $request->id_tiporesultado == '11' ) // produtividade: clientes com chamada > 2
                    {
                        $individuals = DB::connection('mysqlCallcenter')->
                                            select( 'SELECT cidadaos2.cpf as cpf, cidadaos2.nome as nome, tipocidadaos2.descricao as descricao, cidadaos2.fone_fixo as fone_fixo, cidadaos2.fone_celular as fone_celular, cidadaos2.fone_celular2, cidadaos2.email, 
                                                    COUNT(*) as contador, sum( time_to_sec( timediff(chamadas.data_hora_fim, chamadas.data_hora_inicio) ) ) as soma
                                                    FROM jucese_callcenter.chamadas chamadas ' . $complementoLeftJoin .
                                                    ' LEFT JOIN jucese_callcenter.cidadaos cidadaos2 ON chamadas.cidadao_id = cidadaos2.id
                                                    LEFT JOIN jucese_callcenter.tipocidadaos tipocidadaos2 ON chamadas.tipocidadao_id = tipocidadaos2.id
                                                    WHERE ' . $complementoWhere . ' AND (date_format(data_hora_inicio, "%Y-%m-%d") BETWEEN "' . $datainicial . '" AND "' . $datafinal . '")' .
                                                    ' GROUP BY cidadaos2.cpf, cidadaos2.nome, tipocidadaos2.descricao, cidadaos2.fone_fixo, cidadaos2.fone_celular, cidadaos2.fone_celular2, cidadaos2.email
                                                    HAVING count(*) > 2 ORDER BY 8 desc, 2 LIMIT ' . $request->max_registros );

                        if ( count( $individuals ) > 0 )
                        {

                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ], [ 'soma', 0 ] ] );
                            $individuals = $arrayResultadoCalculo;

                            $totaltempomedioatendimento = 0;
                            foreach ( $individuals as $keyItem => $itemValue )
                            {   
                                $individuals[ $keyItem ]->tempomedioatendimento = $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador;
                                $totaltempomedioatendimento = $totaltempomedioatendimento + ( $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador );
                                // $percentual = ( $individuals[ $keyItem ]->contador / $individuals[ $keyItem ]->totalcontador ) * 100;
                                // $individuals[ $keyItem ]->percentual = $percentual;
                                $mediaAtendimentoIndividual = ( $individuals[ $keyItem ]->soma / $individuals[ $keyItem ]->contador );
                                $individuals[ $keyItem ]->mediaAtendimentoIndividual = $mediaAtendimentoIndividual;
                            }
                            foreach ( $individuals as $keyItem => $itemValue )
                            {
                                $individuals[ $keyItem ]->totaltempomedioatendimento = $totaltempomedioatendimento / $individuals[ $keyItem ]->totalitens;
                            }

                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ], [ 'soma', 0 ] ] );
                            $individuals = $arrayResultadoCalculo;

                            session()->forget( [ 'individuals', 'titulo', 'totalregistros', 'tituloGrafico', 'eixoX', 'textoEixoY', 'valoresEixoYCategoria1', 'tituloSeries' ] );
                            
                            // inicio - dados dos gráficos
                                $grafico_A_Nome_Grafico = 'grafico1';
                                $grafico_A_Titulo = 'Clientes com qtde de ocorrências maiores que 2';
                                $grafico_A_Titulo_Eixo_Y = 'Qtde Atendimentos';
                                $grafico_A_Serie_1_Legenda = 'Cidadãos';
                                $grafico_A_Serie_1_Tipo_Grafico = 'column';

                                foreach ( $individuals as $keyItem => $itemValue )
                                {
                                    $grafico_A_Rotulos_Series_Eixo_X[] = $individuals[ $keyItem ]->nome;
                                    $grafico_A_Serie_1_Dados[] = $individuals[ $keyItem ]->contador;
                                }
                                
                                $totalregistros = count( $individuals );
                                session()->put( 'totalregistros', $totalregistros );
                                session()->put( 'individuals', $individuals );
                                session()->put( 'titulo' );

                                session()->put( 'grafico_A_Nome_Grafico', $grafico_A_Nome_Grafico );
                                session()->put( 'grafico_A_Titulo', $grafico_A_Titulo );
                                session()->put( 'grafico_A_Titulo_Eixo_Y', $grafico_A_Titulo_Eixo_Y );
                                session()->put( 'grafico_A_Rotulos_Series_Eixo_X', $grafico_A_Rotulos_Series_Eixo_X );
                                session()->put( 'grafico_A_Serie_1_Legenda', $grafico_A_Serie_1_Legenda ); 
                                session()->put( 'grafico_A_Serie_1_Tipo_Grafico', $grafico_A_Serie_1_Tipo_Grafico ); 
                                session()->put( 'grafico_A_Serie_1_Dados', $grafico_A_Serie_1_Dados ); 
                            // final - dados dos gráficos

                            return view('callcenter.consulta.produtividade.clientescomchamadas', compact( 'titulo' ) );
                        }
                        else
                        {
                            return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta ' . $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . '.' );
                        }
                    }
                elseif ( $request->has('id_tiporesultado') && $request->id_tiporesultado == '12' ) // produtividade: qtde de atendimentos por protocolo agiliza x cpf - ok
                    {
                        if ( $request->tipocidadao_id == 0 )
                        {
                            $protocolosAgiliza = Chamada::selectRaw( 'cidadao_id, id_agiliza, count(*) as contador, SUM( time_to_sec( timediff(data_hora_fim, data_hora_inicio) ) ) as soma, 
                                                                        min(data_hora_inicio) as datainicialprotocolo, max(data_hora_fim) as datafinalprotocolo' )
                                                            ->where( "id_agiliza", "!=", "" )->where( "id_agiliza", "!=", "NAO TEM" )->where( "cidadao_id", "!=", "" )
                                                            ->whereRaw( ' date_format(data_hora_inicio, "%Y-%m-%d") BETWEEN "' . $datainicial . '" AND "' . $datafinal . '"' )
                                                            ->groupBy( 'id_agiliza' )->orderByDesc( 'contador' )->limit( $request->max_registros )->get(); // ->whereBetween( 'data_hora_inicio', [ $datainicial, $datafinal ] )
                        }
                        else
                        {
                            $protocolosAgiliza = Chamada::selectRaw( 'cidadao_id, id_agiliza, count(*) as contador, SUM( time_to_sec( timediff(data_hora_fim, data_hora_inicio) ) ) as soma, 
                                                                        min(data_hora_inicio) as datainicialprotocolo, max(data_hora_fim) as datafinalprotocolo' )
                                                            ->where( "id_agiliza", "!=", "" )->where( "id_agiliza", "!=", "NAO TEM" )->where( "cidadao_id", "!=", "" )
                                                            ->where( 'tipocidadao_id', '=', $request->tipocidadao_id )
                                                            ->whereRaw( ' date_format(data_hora_inicio, "%Y-%m-%d") BETWEEN "' . $datainicial . '" AND "' . $datafinal . '"' )
                                                            ->groupBy( 'id_agiliza' )->orderByDesc( 'contador' )->limit( $request->max_registros )->get(); // ->whereBetween( 'data_hora_inicio', [ $datainicial, $datafinal ] )
                        }

                        if ( count( $protocolosAgiliza ) > 0 )
                        {
                                
                            foreach ( $protocolosAgiliza as $keyItem => $itemValue ) {
                                $dataInicialProtocoloAgiliza = $protocolosAgiliza[ $keyItem ]->datainicialprotocolo;
                                $dataFinalProtocoloAgiliza = $protocolosAgiliza[ $keyItem ]->datafinalprotocolo;

                                $rotulos1Grafico[] = $protocolosAgiliza[ $keyItem ]->id_agiliza;
                                $valores1Grafico[] = $protocolosAgiliza[ $keyItem ]->contador;

                                $protocolosAgilizaCidadao = Chamada::selectRaw( 'cidadao_id, id_agiliza, count(*) as contador, SUM( time_to_sec( timediff(data_hora_fim, data_hora_inicio) ) ) as soma' )
                                                ->where( "id_agiliza", $protocolosAgiliza[ $keyItem ]->id_agiliza )->where( "cidadao_id", "!=", "" )->groupBy( 'id_agiliza' )->groupBy( 'cidadao_id' )->orderByDesc( 'contador' )->limit( $request->max_registros )->get();

                                foreach ( $protocolosAgilizaCidadao as $keyItem2 => $itemValue2 ) {   
                                    $protocolosAgilizaCidadaoChamadas = Chamada::selectRaw( 'cidadao_id, user_id, id_agiliza, data_hora_inicio, data_hora_fim' )->where( "id_agiliza", $protocolosAgiliza[ $keyItem ]->id_agiliza )
                                                            ->where( "cidadao_id", $protocolosAgilizaCidadao[ $keyItem2 ]->cidadao_id )->orderBy( 'data_hora_inicio' )->get();

                                    if ( count( $protocolosAgilizaCidadaoChamadas ) > 0 ) {
                                        $datainicial = datahoraBR( $protocolosAgilizaCidadaoChamadas[0]->data_hora_inicio ); $datafinal = datahoraBR( $protocolosAgilizaCidadaoChamadas[0]->data_hora_fim );
                                        $usuariosatendeu = '';
                                        $arrayUsuariosAtendeu = array();
                                        foreach ( $protocolosAgilizaCidadaoChamadas as $keyItem3 => $itemValue3 ) {
                                            $datafinal = datahoraBR( $protocolosAgilizaCidadaoChamadas[ $keyItem3 ]->data_hora_fim );
                                            $nomeatual = $protocolosAgilizaCidadaoChamadas[ $keyItem3 ]->usuariocadastro->name;
                                            
                                            if ( array_search( $nomeatual, $arrayUsuariosAtendeu, true ) === false ) {
                                                $arrayUsuariosAtendeu[] = $nomeatual;
                                            }
                                        }
                                        sort( $arrayUsuariosAtendeu );

                                        foreach ( $arrayUsuariosAtendeu as $keyItem4 => $itemValue4 ) {
                                            if ( $keyItem4 == 0 ) {
                                                $usuariosatendeu = $arrayUsuariosAtendeu[ $keyItem4 ];
                                            }
                                            else {
                                                $usuariosatendeu = $usuariosatendeu . ', ' . $arrayUsuariosAtendeu[ $keyItem4 ];
                                            }
                                        }
                                    }

                                    $individuals[] = [ 'contadortotal' => isset( $protocolosAgiliza[ $keyItem ]->contador ) ? $protocolosAgiliza[ $keyItem ]->contador : 0, 
                                                    'contadorindividual' => isset( $protocolosAgilizaCidadao[ $keyItem2 ]->contador ) ? $protocolosAgilizaCidadao[ $keyItem2 ]->contador : 0, 
                                                    'id_agiliza' => isset( $protocolosAgilizaCidadao[ $keyItem2 ]->id_agiliza ) ? $protocolosAgilizaCidadao[ $keyItem2 ]->id_agiliza : '',
                                                    'nome' => isset( $protocolosAgilizaCidadao[ $keyItem2 ]->cidadao->nome ) ? $protocolosAgilizaCidadao[ $keyItem2 ]->cidadao->nome : '', 
                                                    'descricao_tipocidadao' => ( $protocolosAgilizaCidadao[ $keyItem2 ]->cidadao->tipocidadao_id > 0 ) ? $protocolosAgilizaCidadao[ $keyItem2 ]->cidadao->tipocidadao->descricao : '', 
                                                    'cpf' => isset( $protocolosAgilizaCidadao[ $keyItem2 ]->cidadao->cpf ) ? $protocolosAgilizaCidadao[ $keyItem2 ]->cidadao->cpf : '', 
                                                    'data_inicial' => $datainicial, 
                                                    'data_final' => $datafinal,
                                                    'tempoatendimento' => $protocolosAgilizaCidadao[ $keyItem2 ]->soma,
                                                    'tempoatendimentoprotocolo' => $protocolosAgiliza[ $keyItem ]->soma,
                                                    'tempomedioatendimentoprotocolo' => $protocolosAgiliza[ $keyItem ]->soma / $protocolosAgiliza[ $keyItem ]->contador,
                                                    'mediatempoatendimento' => $protocolosAgilizaCidadao[ $keyItem2 ]->soma / $protocolosAgilizaCidadao[ $keyItem2 ]->contador,
                                                    'usuariosatenderam' => $usuariosatendeu,
                                                    'dataInicialProtocoloAgiliza' => datahoraBR( $dataInicialProtocoloAgiliza ),
                                                    'dataFinalProtocoloAgiliza' => datahoraBR( $dataFinalProtocoloAgiliza )
                                                    ];
                                }
                            }

                            
                            // calculo totais
                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $protocolosAgiliza, [ [ 'itens', 0 ], [ 'contador', 0 ], [ 'soma', 0 ] ] );
                            $protocolosAgiliza = $arrayResultadoCalculo;

                            $percentualtotal = 0;
                            $nTotalMediaTempoAtendimento = 0;
                            $rotulos1Grafico = array();
                            $valores1Grafico = array();
                            
                            foreach ( $protocolosAgiliza as $keyItem => $itemValue ) {
                                $percentualtotal = $percentualtotal + $protocolosAgiliza[ $keyItem ]->percentualcontador;
                                $protocolosAgiliza[ $keyItem ]->id_agiliza = $protocolosAgiliza[ $keyItem ]->id_agiliza;
                                $protocolosAgiliza[ $keyItem ]->mediatempoatendimento = ( $protocolosAgiliza[ $keyItem ]->soma / $protocolosAgiliza[ $keyItem ]->contador );
                                $nTotalMediaTempoAtendimento = $nTotalMediaTempoAtendimento + $protocolosAgiliza[ $keyItem ]->mediatempoatendimento;

                                $rotulos1Grafico[] = $protocolosAgiliza[ $keyItem ]->id_agiliza;
                                $valores1Grafico[] = $protocolosAgiliza[ $keyItem ]->contador;
                            }
                            foreach ( $protocolosAgiliza as $keyItem => $itemValue ) {
                                $protocolosAgiliza[ $keyItem ]->nTotalMediaTempoAtendimento = $nTotalMediaTempoAtendimento / $protocolosAgiliza[ $keyItem ]->totalitens;
                                $protocolosAgiliza[ $keyItem ]->percentualtotal = $protocolosAgiliza[ $keyItem ]->percentual;
                            }

                            $individualsDetalhe = $individuals;
                            $individuals = $protocolosAgiliza;

                            $tituloGrafico = 'Protocolo Agiliza com Maiores Qtde de Registros de Chamadas';

                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ], [ 'soma', 0 ] ] );
                            $individuals = $arrayResultadoCalculo;

                            session()->forget( [ 'individuals', 'titulo', 'totalregistros', 'tituloGrafico', 'eixoX', 'textoEixoY', 'valoresEixoYCategoria1', 'tituloSeries' ] );
                            
                            // inicio - dados dos gráficos
                                $grafico_A_Nome_Grafico = 'grafico1';
                                $grafico_A_Titulo = 'Protocolo Agiliza com Maiores Qtde de Atendimentos';
                                $grafico_A_Titulo_Eixo_Y = 'Qtde Atendimentos';
                                $grafico_A_Serie_1_Legenda = 'Protocolos Agiliza';
                                $grafico_A_Serie_1_Tipo_Grafico = 'column';

                                foreach ( $individuals as $keyItem => $itemValue )
                                {
                                    $grafico_A_Rotulos_Series_Eixo_X[] = $individuals[ $keyItem ]->id_agiliza;
                                    $grafico_A_Serie_1_Dados[] = $individuals[ $keyItem ]->contador;
                                }
                                
                                $totalregistros = count( $individuals );
                                session()->put( 'totalregistros', $totalregistros );
                                session()->put( 'individuals', $individuals );
                                session()->put( 'individualsDetalhe', $individualsDetalhe );
                                session()->put( 'titulo' );

                                session()->put( 'grafico_A_Nome_Grafico', $grafico_A_Nome_Grafico );
                                session()->put( 'grafico_A_Titulo', $grafico_A_Titulo );
                                session()->put( 'grafico_A_Titulo_Eixo_Y', $grafico_A_Titulo_Eixo_Y );
                                session()->put( 'grafico_A_Rotulos_Series_Eixo_X', $grafico_A_Rotulos_Series_Eixo_X );
                                session()->put( 'grafico_A_Serie_1_Legenda', $grafico_A_Serie_1_Legenda ); 
                                session()->put( 'grafico_A_Serie_1_Tipo_Grafico', $grafico_A_Serie_1_Tipo_Grafico ); 
                                session()->put( 'grafico_A_Serie_1_Dados', $grafico_A_Serie_1_Dados ); 
                            // final - dados dos gráficos
                            // dd( $individuals, $individualsDetalhe );

                            return view('callcenter.consulta.produtividade.agilizaxcpf', compact( 'titulo' ) );
                        }
                        else
                        {
                            return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta ' . $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . '.' );
                        }
                    }
                elseif ( $request->has('id_tiporesultado') && $request->id_tiporesultado == '13' ) // produtividade: atendimentos por tipo de cidadão - ok
                    {
                        $individuals = Chamada::leftJoin( 'cidadaos as cidadaos', 'chamadas.cidadao_id', '=', 'cidadaos.id' )
                                                ->leftJoin( 'tipocidadaos as tipocidadaos', 'chamadas.tipocidadao_id', '=', 'tipocidadaos.id' )
                                                ->whereRaw( 'CAST(data_hora_inicio AS DATE) BETWEEN "' . $datainicial . '" AND "' . $datafinal . '"' )
                                                ->whereRaw( $complementoWhere )
                                                ->groupBy( 'chamadas.tipocidadao_id' )
                                                ->selectRaw( 'tipocidadaos.descricao as descricao_tipocidadao, count(*) as contador' )
                                                ->orderByDesc( 'contador' )
                                                ->limit( $request->max_registros )->get();

                        if ( count( $individuals ) > 0 )
                        {
                        $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );
                        $individuals = $arrayResultadoCalculo;

                        $percentualtotal = 0;
                        foreach ( $individuals as $keyItem => $itemValue )
                        {   
                            $percentualtotal = $percentualtotal + $individuals[ $keyItem ]->percentualcontador;
                        }
                        foreach ( $individuals as $keyItem => $itemValue )
                        {   
                            $individuals[ $keyItem ]->percentualtotal = $individuals[ $keyItem ]->percentual;
                        }

                        $tituloGrafico = 'Registros de Chamadas por Tipo de Cidadão';

                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );
                            $individuals = $arrayResultadoCalculo;

                            session()->forget( [ 'individuals', 'titulo', 'totalregistros', 'tituloGrafico', 'eixoX', 'textoEixoY', 'valoresEixoYCategoria1', 'tituloSeries' ] );

                            // inicio - dados dos gráficos
                                $grafico_A_Nome_Grafico = 'grafico1';
                                $grafico_A_Titulo = 'Atendimentos';
                                $grafico_A_Titulo_Eixo_Y = 'Qtde Atendimentos';
                                $grafico_A_Serie_1_Legenda = 'Tipo de Cidadão';
                                $grafico_A_Serie_1_Tipo_Grafico = 'column';

                                foreach ( $individuals as $keyItem => $itemValue )
                                {
                                    $grafico_A_Rotulos_Series_Eixo_X[] = $individuals[ $keyItem ]->descricao_tipocidadao;
                                    $grafico_A_Serie_1_Dados[] = $individuals[ $keyItem ]->contador;
                                }
                                
                                $totalregistros = count( $individuals );
                                session()->put( 'totalregistros', $totalregistros );
                                session()->put( 'individuals', $individuals );
                                session()->put( 'titulo' );

                                session()->put( 'grafico_A_Nome_Grafico', $grafico_A_Nome_Grafico );
                                session()->put( 'grafico_A_Titulo', $grafico_A_Titulo );
                                session()->put( 'grafico_A_Titulo_Eixo_Y', $grafico_A_Titulo_Eixo_Y );
                                session()->put( 'grafico_A_Rotulos_Series_Eixo_X', $grafico_A_Rotulos_Series_Eixo_X );
                                session()->put( 'grafico_A_Serie_1_Legenda', $grafico_A_Serie_1_Legenda ); 
                                session()->put( 'grafico_A_Serie_1_Tipo_Grafico', $grafico_A_Serie_1_Tipo_Grafico ); 
                                session()->put( 'grafico_A_Serie_1_Dados', $grafico_A_Serie_1_Dados ); 
                            // final - dados dos gráficos

                            return view('callcenter.consulta.produtividade.portipocidadao', compact( 'titulo' ) );
                        }
                        else
                        {
                            return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta ' . $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . '.' );
                        }
                    }
                elseif ( $request->has('id_tiporesultado') && $request->id_tiporesultado == '14' ) // produtividade: c.p.f.'s distintos por tipo de cidadão - ok
                    {
                        $individuals = Cidadao::selectRaw( 'tipocidadao_id, count(*) as contador' )
                                                ->leftJoin( 'tipocidadaos as tipocidadaos', 'tipocidadao_id', '=', 'tipocidadaos.id' )
                                                ->groupBy( 'tipocidadao_id' )
                                                ->orderByDesc( 'contador' )->limit( $request->max_registros )->get();

                        if ( count( $individuals ) > 0 )
                        {
                            $titulo = 'Produtividade: Consulta de Chamadas' . ': ' . $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . ' [ Limite Registros: ' . $request->max_registros . ' ]';

                            $percentualtotal = 0;
                            foreach ( $individuals as $keyItem => $itemValue )
                            {   
                                $percentualtotal = $percentualtotal + $individuals[ $keyItem ]->percentualcontador;
                                $individuals[ $keyItem ]->descricao_tipocidadao = $individuals[ $keyItem ]->tipocidadao->descricao;
                            }

                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );
                            $individuals = $arrayResultadoCalculo;

                            foreach ( $individuals as $keyItem => $itemValue )
                            {   
                                $individuals[ $keyItem ]->percentualtotal = $individuals[ $keyItem ]->percentual;
                            }

                            $tituloGrafico = 'Qtde C.P.F.s Distintos por Tipo de Cidadão';

                            $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individuals, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );
                            $individuals = $arrayResultadoCalculo;


                            session()->forget( [ 'individuals', 'titulo', 'totalregistros', 'tituloGrafico', 'eixoX', 'textoEixoY', 'valoresEixoYCategoria1', 'tituloSeries' ] );

                            // inicio - dados dos gráficos
                                $grafico_A_Nome_Grafico = 'grafico1';
                                $grafico_A_Titulo = 'C.P.F.s Distintos por Tipo de Cidadão';
                                $grafico_A_Titulo_Eixo_Y = 'Qtde C.P.F.s';
                                $grafico_A_Serie_1_Legenda = 'Tipo de Cidadão';
                                $grafico_A_Serie_1_Tipo_Grafico = 'column';

                                foreach ( $individuals as $keyItem => $itemValue )
                                {
                                    $grafico_A_Rotulos_Series_Eixo_X[] = $individuals[ $keyItem ]->descricao_tipocidadao;
                                    $grafico_A_Serie_1_Dados[] = $individuals[ $keyItem ]->contador;
                                }
                                
                                $totalregistros = count( $individuals );
                                session()->put( 'totalregistros', $totalregistros );
                                session()->put( 'individuals', $individuals );
                                session()->put( 'titulo' );

                                session()->put( 'grafico_A_Nome_Grafico', $grafico_A_Nome_Grafico );
                                session()->put( 'grafico_A_Titulo', $grafico_A_Titulo );
                                session()->put( 'grafico_A_Titulo_Eixo_Y', $grafico_A_Titulo_Eixo_Y );
                                session()->put( 'grafico_A_Rotulos_Series_Eixo_X', $grafico_A_Rotulos_Series_Eixo_X );
                                session()->put( 'grafico_A_Serie_1_Legenda', $grafico_A_Serie_1_Legenda ); 
                                session()->put( 'grafico_A_Serie_1_Tipo_Grafico', $grafico_A_Serie_1_Tipo_Grafico ); 
                                session()->put( 'grafico_A_Serie_1_Dados', $grafico_A_Serie_1_Dados ); 
                            // final - dados dos gráficos

                            return view('callcenter.consulta.produtividade.cpfdistintoportipocidadao', compact( 'titulo' ) );
                        }
                        else
                        {
                            return redirect()->back()->with( 'msg', 'Registro não encontrado para consulta ' . $tipoResultado[ ( (int)$request->id_tiporesultado )-1 ] . '.' );
                        }
                    }
            }
        
    // impressão de produtividade: por ano / mes ...
        public function imprimir_poranomes( Request $request )
            {
                $chart = $request->chart;
                $titulo = $request->titulo;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.produtividade.imprimir.imprimir_poranomes', compact( 'titulo', 'chart' ) ); //->setPaper('a4', 'portrait');

                return $pdf->stream('JUCESE-produtividade-poranomes.pdf');
            }

        public function imprimir_agilizaxcpf( Request $request )
            {
                $titulo = $request->titulo;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.produtividade.imprimir.imprimir_agilizaxcpf', compact( 'titulo' ) ); //->setPaper('a4', 'portrait');
                return $pdf->stream('JUCESE-produtividade-agilizaxcpf.pdf');
            }

        public function imprimir_clientescomchamadas( Request $request )
            {
                $titulo = $request->titulo;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.produtividade.imprimir.imprimir_clientescomchamadas', compact( 'titulo' ) ); //->setPaper('a4', 'portrait');
                return $pdf->stream('JUCESE-produtividade-clientescomchamadas.pdf');
            }

        public function imprimir_cpfdistintoportipocidadao( Request $request )
            {
                $titulo = $request->titulo;
                $nTotalAtendimentos = $request->nTotalAtendimentos;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.produtividade.imprimir.imprimir_cpfdistintoportipocidadao', compact( 'titulo', 'nTotalAtendimentos' ) ); //->setPaper('a4', 'portrait');
                return $pdf->stream('JUCESE-produtividade-cpfdistintoportipocidadao.pdf');
            }

        public function imprimir_poratendente( Request $request )
            {
                $titulo = $request->titulo;
                $nTotalAtendimentos = $request->nTotalAtendimentos;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.produtividade.imprimir.imprimir_poratendente', compact( 'titulo', 'nTotalAtendimentos' ) ); //->setPaper('a4', 'portrait');
                return $pdf->stream('JUCESE-produtividade-poratendente.pdf');
            }

        public function imprimir_pordataatendente( Request $request )
            {
                $titulo = $request->titulo;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.produtividade.imprimir.imprimir_pordataatendente', compact( 'titulo' ) ); //->setPaper('a4', 'portrait');
                return $pdf->stream('JUCESE-produtividade-pordataatendente.pdf');
            }

        public function imprimir_pordatahora( Request $request )
            {
                $titulo = $request->titulo;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.produtividade.imprimir.imprimir_pordatahora', compact( 'titulo' ) ); //->setPaper('a4', 'portrait');
                return $pdf->stream('JUCESE-produtividade-pordatahora.pdf');
            }

        public function imprimir_pordia( Request $request )
            {
                $titulo = $request->titulo;
                $nTotalAtendimentos = $request->nTotalAtendimentos;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.produtividade.imprimir.imprimir_pordia', compact( 'titulo', 'nTotalAtendimentos' ) ); //->setPaper('a4', 'portrait');
                return $pdf->stream('JUCESE-produtividade-pordia.pdf');
            }

        public function imprimir_porhora( Request $request )
            {
                $titulo = $request->titulo;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.produtividade.imprimir.imprimir_porhora', compact( 'titulo' ) ); //->setPaper('a4', 'portrait');
                return $pdf->stream('JUCESE-produtividade-porhora.pdf');
            }

        public function imprimir_portipocidadao( Request $request )
            {
                $titulo = $request->titulo;
                $nTotalAtendimentos = $request->nTotalAtendimentos;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.produtividade.imprimir.imprimir_portipocidadao', compact( 'titulo', 'nTotalAtendimentos' ) ); //->setPaper('a4', 'portrait');
                return $pdf->stream('JUCESE-produtividade-portipocidadao.pdf');
            }

        public function imprimir_portipoproblema( Request $request )
            {
                $titulo = $request->titulo;
                $nTotalAtendimentos = $request->nTotalAtendimentos;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.produtividade.imprimir.imprimir_portipoproblema', compact( 'titulo', 'nTotalAtendimentos' ) ); //->setPaper('a4', 'portrait');
                return $pdf->stream('JUCESE-produtividade-portipoproblema.pdf');
            }

        public function imprimir_portiposolucao( Request $request )
            {
                $titulo = $request->titulo;
                $nTotalAtendimentos = $request->nTotalAtendimentos;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.produtividade.imprimir.imprimir_portiposolucao', compact( 'titulo', 'nTotalAtendimentos' ) ); //->setPaper('a4', 'portrait');
                return $pdf->stream('JUCESE-produtividade-portiposolucao.pdf');
            }

        public function imprimir_problemascomocorrencias( Request $request )
            {
                $titulo = $request->titulo;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.produtividade.imprimir.imprimir_problemascomocorrencias', compact( 'titulo' ) ); //->setPaper('a4', 'portrait');
                return $pdf->stream('JUCESE-produtividade-problemascomocorrencias.pdf');
            }

        public function imprimir_solucoescomocorrencias( Request $request )
            {
                $titulo = $request->titulo;

                $pdf = \App::make('dompdf.wrapper');
                $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
                $pdf->loadView( 'callcenter.consulta.produtividade.imprimir.imprimir_solucoescomocorrencias', compact( 'titulo' ) ); //->setPaper('a4', 'portrait');
                return $pdf->stream('JUCESE-produtividade-solucoescomocorrencias.pdf');
            }

        public function chamadageralantigo( Request $request )
            {
                $tipoproblema = Tipoproblema::select( 'id', 'descricao' )
                                ->orderby( 'descricao', 'asc' )
                                ->whereIn( 'id', Chamada::select('tipoproblema_id')->get()->toArray() )
                                ->get();

                $tiposolucao  = Tiposolucao::select( 'id', 'descricao' )
                                ->orderby( 'descricao', 'asc' )
                                ->whereIn( 'id', Chamada::select('tiposolucao_id')->get()->toArray() )
                                ->get();

                $gruposuser  = Gruposuser::select( 'id', 'descricao' )
                                ->orderby( 'descricao', 'asc' )
                                ->whereIn( 'id', Chamada::select('gruposuser_id')->get()->toArray() )
                                ->get();

                $gruposuser = Gruposuser::all();

                $tipocidadao  = Tipocidadao::select( 'id', 'descricao' )
                                ->orderby( 'descricao', 'asc' )
                                ->whereIn( 'id', Chamada::select('tipocidadao_id')->get()->toArray() )
                                ->get();

                $usuario      = User::select( 'id', 'name' )
                                ->orderby( 'name', 'asc' )
                                ->whereIn( 'id', Chamada::select('user_id')->get()->toArray() )
                                ->get();

                $individuals = Chamada::orderby('id', 'desc')->paginate($this->itensPorPagina);

                return view('callcenter.consulta.chamadageral.index', compact('individuals', 'tipoproblema', 'tiposolucao', 'gruposuser', 'tipocidadao', 'usuario' ));
            }

        public function filtrarantigo( Request $request )
            {
                $tipoproblema = Tipoproblema::all();
                $tiposolucao = Tiposolucao::all();
                $gruposuser = Gruposuser::all();
                $tipocidadao = Tipocidadao::all();
                $usuario = User::all();

                $individuals = Chamada::where('nome', 'LIKE', '%' . $request->criterio . '%')->orderby('id', 'asc')->paginate($this->itensPorPagina);

                return view('callcenter.consulta.chamadageral.index', compact('individuals', 'tipoproblema', 'tiposolucao', 'gruposuser', 'tipocidadao', 'usuario' ));
            }

        public function produtividade2antigo( Request $request )
            {
                // $tipoproblema = Tipoproblema::all();
                // $tiposolucao = Tiposolucao::all();
                // $gruposuser = Gruposuser::all();
                $tipocidadao = Tipocidadao::all();
                // $usuario = User::all();

                // $usuario = User::whereIn('id', 'select distinct id from chamadas' );

                $tipoproblema = Tipoproblema::select('id','descricao')->orderby('descricao', 'asc')->whereIn('id', Chamada::select('tipoproblema_id')->get()->toArray())->get();
                $tiposolucao = Tiposolucao::select('id','descricao')->orderby('descricao', 'asc')->whereIn('id', Chamada::select('tiposolucao_id')->get()->toArray())->get();
                $gruposuser = Gruposuser::select('id','descricao')->orderby('descricao', 'asc')->whereIn('id', Chamada::select('gruposuser_id')->get()->toArray())->get();
                $gruposuser = Gruposuser::all();
                // $tipocidadao = Tipocidadao::select('tipocidadao_id','descricao')->whereIn('tipocidadao_id', Chamada::select('tipocidadao_id')->get()->toArray())->get();
                $usuario = User::select('id','name')->orderby('name', 'asc')->whereIn('id', Chamada::select('id')->get()->toArray())->get();

                // DB::table('user')->select('id','name')->whereNotIn('id', DB::table('curses')->select('id_user')->where('id_user', '=', $id)->get()->toArray())->get();


                // $individuals = Chamada::orderby('id', 'asc')->where('id', auth()->user()->id)->paginate($this->itensPorPagina);
                $individuals = Chamada::orderby('id', 'desc')->paginate($this->itensPorPagina);

                $retorno = $request->id_tiporesultado;
                // dd($request);
                if ( $request->has('id_tiporesultado') && $request->id_tiporesultado == '01' )
                    return view('callcenter.consulta.produtividade.pordatahora', compact('individuals', 'tipoproblema', 'tiposolucao', 'gruposuser', 'tipocidadao', 'usuario' ));
                else
                    return view('callcenter.consulta.produtividade.produtividade', compact('individuals', 'tipoproblema', 'tiposolucao', 'gruposuser', 'tipocidadao', 'usuario' ));
                    // dd( $request );

                //@elseif (  )
                    //return view('callcenter.consulta.produtividade.pordatahora', compact('individuals', 'tipoproblema', 'tiposolucao', 'gruposuser', 'tipocidadao', 'usuario' ));
                //@elseif (  )
                    //return view('callcenter.consulta.produtividade.pordatahora', compact('individuals', 'tipoproblema', 'tiposolucao', 'gruposuser', 'tipocidadao', 'usuario' ));
                //@elseif (  )
                    //return view('callcenter.consulta.produtividade.pordatahora', compact('individuals', 'tipoproblema', 'tiposolucao', 'gruposuser', 'tipocidadao', 'usuario' ));
                //@elseif (  )
                    //return view('callcenter.consulta.produtividade.pordatahora', compact('individuals', 'tipoproblema', 'tiposolucao', 'gruposuser', 'tipocidadao', 'usuario' ));
                //@elseif (  )
                    //return view('callcenter.consulta.produtividade.pordatahora', compact('individuals', 'tipoproblema', 'tiposolucao', 'gruposuser', 'tipocidadao', 'usuario' ));
                //@elseif (  )
                    //return view('callcenter.consulta.produtividade.pordatahora', compact('individuals', 'tipoproblema', 'tiposolucao', 'gruposuser', 'tipocidadao', 'usuario' ));
                //@elseif (  )
                    //return view('callcenter.consulta.produtividade.pordatahora', compact('individuals', 'tipoproblema', 'tiposolucao', 'gruposuser', 'tipocidadao', 'usuario' ));
                //@elseif (  )
                    //return view('callcenter.consulta.produtividade.pordatahora', compact('individuals', 'tipoproblema', 'tiposolucao', 'gruposuser', 'tipocidadao', 'usuario' ));
                //@elseif (  )
                    //return view('callcenter.consulta.produtividade.pordatahora', compact('individuals', 'tipoproblema', 'tiposolucao', 'gruposuser', 'tipocidadao', 'usuario' ));
                //@elseif (  )
                    //return view('callcenter.consulta.produtividade.pordatahora', compact('individuals', 'tipoproblema', 'tiposolucao', 'gruposuser', 'tipocidadao', 'usuario' ));
                //@elseif (  )

                // return view('callcenter.consulta.produtividade.index', compact('individuals', 'tipoproblema', 'tiposolucao', 'gruposuser', 'tipocidadao', 'usuario' ));
            }

    // Dashboard
        public function dashboard( Request $request )
            {
                // $datainicial = $request->data_inicial; $datafinal = $request->data_final; $titulo = 'Consulta Geral de Chamadas '; // $arrayInformacoes = collect( [ 'titulo', 'individuals' ] );

                // $titulo = $titulo . ': [ Limite Registros: ' . $request->max_registros . ' ] - [ Período: ' . dataDiaSemana( $datainicial ) . ' a ' . dataDiaSemana( $datafinal ) . ' ]';

                $usuarioatual = auth()->user()->id;
                $gruposuserLogado = auth()->user()->grupoid;

                // Problemas Recorrentes
                    $individualsProblemasRecorrentes = DB::connection('mysqlCallcenter')->select( 'SELECT tipoproblemas.descricao as descricao, count(*) as contador
                                                    FROM jucese_callcenter.chamadas chamadas ' . 
                                                    ' LEFT JOIN jucese_callcenter.tipoproblemas tipoproblemas ON chamadas.tipoproblema_id = tipoproblemas.id
                                                    GROUP BY descricao ORDER BY 2 DESC LIMIT 5' );

                    // inicio grafico
                        $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individualsProblemasRecorrentes, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );

                        $individualsProblemasRecorrentes = $arrayResultadoCalculo;
                        foreach ( $individualsProblemasRecorrentes as $keyItem => $itemValue )
                        {
                            $percentual = ( $individualsProblemasRecorrentes[ $keyItem ]->contador / $individualsProblemasRecorrentes[ $keyItem ]->totalcontador ) * 100;
                            $individualsProblemasRecorrentes[ $keyItem ]->percentual = $percentual;
                        }

                        $ProblemasRecorrentes_grafico_A_Nome_Grafico = 'graficoProblemasRecorrentes';
                        $ProblemasRecorrentes_grafico_A_Titulo = 'Problemas Recorrentes';
                        $ProblemasRecorrentes_grafico_A_Titulo_Eixo_Y = 'Qtde Atendimentos';
                        $ProblemasRecorrentes_grafico_A_Serie_1_Legenda = 'Tipos de Problemas';
                        $ProblemasRecorrentes_grafico_A_Serie_1_Tipo_Grafico = 'column';

                        foreach ( $individualsProblemasRecorrentes as $keyItem => $itemValue )
                        {
                            $ProblemasRecorrentes_grafico_A_Rotulos_Series_Eixo_X[] = $individualsProblemasRecorrentes[ $keyItem ]->descricao;
                            $ProblemasRecorrentes_grafico_A_Serie_1_Dados[] = $individualsProblemasRecorrentes[ $keyItem ]->contador;
                        }

                        $totalregistros = count( $individualsProblemasRecorrentes );
                        session()->put( 'individualsProblemasRecorrentes', $individualsProblemasRecorrentes );
                        session()->put( 'totalregistros', $totalregistros );

                        session()->put( 'ProblemasRecorrentes_grafico_A_Nome_Grafico', $ProblemasRecorrentes_grafico_A_Nome_Grafico );
                        session()->put( 'ProblemasRecorrentes_grafico_A_Titulo', $ProblemasRecorrentes_grafico_A_Titulo );
                        session()->put( 'ProblemasRecorrentes_grafico_A_Titulo_Eixo_Y', $ProblemasRecorrentes_grafico_A_Titulo_Eixo_Y );
                        session()->put( 'ProblemasRecorrentes_grafico_A_Rotulos_Series_Eixo_X', $ProblemasRecorrentes_grafico_A_Rotulos_Series_Eixo_X );
                        session()->put( 'ProblemasRecorrentes_grafico_A_Serie_1_Legenda', $ProblemasRecorrentes_grafico_A_Serie_1_Legenda );
                        session()->put( 'ProblemasRecorrentes_grafico_A_Serie_1_Tipo_Grafico', $ProblemasRecorrentes_grafico_A_Serie_1_Tipo_Grafico );
                        session()->put( 'ProblemasRecorrentes_grafico_A_Serie_1_Dados', $ProblemasRecorrentes_grafico_A_Serie_1_Dados );
                    // fim grafico

                // Soluções Recorrentes
                    $individualsSolucoesRecorrentes = DB::connection('mysqlCallcenter')->select( 'SELECT tiposolucaos.descricao as descricao, count(*) as contador
                                                    FROM jucese_callcenter.chamadas chamadas ' . 
                                                    ' LEFT JOIN jucese_callcenter.tiposolucaos tiposolucaos ON chamadas.tiposolucao_id = tiposolucaos.id
                                                    GROUP BY descricao ORDER BY 2 DESC LIMIT 5' );

                    // inicio grafico
                        $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individualsSolucoesRecorrentes, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );

                        $individualsSolucoesRecorrentes = $arrayResultadoCalculo;
                        foreach ( $individualsSolucoesRecorrentes as $keyItem => $itemValue )
                        {
                            $percentual = ( $individualsSolucoesRecorrentes[ $keyItem ]->contador / $individualsSolucoesRecorrentes[ $keyItem ]->totalcontador ) * 100;
                            $individualsSolucoesRecorrentes[ $keyItem ]->percentual = $percentual;
                        }

                        $SolucoesRecorrentes_grafico_A_Nome_Grafico = 'graficoSolucoesRecorrentes';
                        $SolucoesRecorrentes_grafico_A_Titulo = 'Soluções Recorrentes';
                        $SolucoesRecorrentes_grafico_A_Titulo_Eixo_Y = 'Qtde Atendimentos';
                        $SolucoesRecorrentes_grafico_A_Serie_1_Legenda = 'Tipos de Soluções';
                        $SolucoesRecorrentes_grafico_A_Serie_1_Tipo_Grafico = 'column';

                        foreach ( $individualsSolucoesRecorrentes as $keyItem => $itemValue )
                        {
                            $SolucoesRecorrentes_grafico_A_Rotulos_Series_Eixo_X[] = $individualsSolucoesRecorrentes[ $keyItem ]->descricao;
                            $SolucoesRecorrentes_grafico_A_Serie_1_Dados[] = $individualsSolucoesRecorrentes[ $keyItem ]->contador;
                        }

                        $totalregistros = count( $individualsSolucoesRecorrentes );
                        session()->put( 'individualsSolucoesRecorrentes', $individualsSolucoesRecorrentes );
                        session()->put( 'totalregistros', $totalregistros );

                        session()->put( 'SolucoesRecorrentes_grafico_A_Nome_Grafico', $SolucoesRecorrentes_grafico_A_Nome_Grafico );
                        session()->put( 'SolucoesRecorrentes_grafico_A_Titulo', $SolucoesRecorrentes_grafico_A_Titulo );
                        session()->put( 'SolucoesRecorrentes_grafico_A_Titulo_Eixo_Y', $SolucoesRecorrentes_grafico_A_Titulo_Eixo_Y );
                        session()->put( 'SolucoesRecorrentes_grafico_A_Rotulos_Series_Eixo_X', $SolucoesRecorrentes_grafico_A_Rotulos_Series_Eixo_X );
                        session()->put( 'SolucoesRecorrentes_grafico_A_Serie_1_Legenda', $SolucoesRecorrentes_grafico_A_Serie_1_Legenda );
                        session()->put( 'SolucoesRecorrentes_grafico_A_Serie_1_Tipo_Grafico', $SolucoesRecorrentes_grafico_A_Serie_1_Tipo_Grafico );
                        session()->put( 'SolucoesRecorrentes_grafico_A_Serie_1_Dados', $SolucoesRecorrentes_grafico_A_Serie_1_Dados );
                    // fim grafico

                // Produtividade Por Atendente

                    $individualsProdutividadeAtendentes = Chamada::select( 'user_id' )
                                                                ->groupBy( 'user_id' )
                                                                ->selectRaw( 'user_id, count(*) as contador' )
                                                                ->orderBy( 'contador', 'DESC' )
                                                                ->limit( 5 )->get();

                    foreach ( $individualsProdutividadeAtendentes as $keyItem => $itemValue )
                    {
                        $individualsProdutividadeAtendentes[ $keyItem ]->nomeusuario = isset( $individualsProdutividadeAtendentes[ $keyItem ]->usuariocadastro->name ) ? $individualsProdutividadeAtendentes[ $keyItem ]->usuariocadastro->name : $individualsProdutividadeAtendentes[ $keyItem ]->user_id;
                    }
                    // inicio grafico
                        $arrayResultadoCalculo = calcularTotaisArrayComplexo( $individualsProdutividadeAtendentes, [ [ 'itens', 0 ], [ 'contador', 0 ] ] );

                        $individualsProdutividadeAtendentes = $arrayResultadoCalculo;
                        foreach ( $individualsProdutividadeAtendentes as $keyItem => $itemValue )
                        {
                            $percentual = ( $individualsProdutividadeAtendentes[ $keyItem ]->contador / $individualsProdutividadeAtendentes[ $keyItem ]->totalcontador ) * 100;
                            $individualsProdutividadeAtendentes[ $keyItem ]->percentual = $percentual;
                        }

                        $ProdutividadeAtendentes_grafico_A_Nome_Grafico = 'graficoProdutividadeAtendentes';
                        $ProdutividadeAtendentes_grafico_A_Titulo = 'Produtividade Atendentes';
                        $ProdutividadeAtendentes_grafico_A_Titulo_Eixo_Y = 'Qtde Atendimentos';
                        $ProdutividadeAtendentes_grafico_A_Serie_1_Legenda = 'Atendentes';
                        $ProdutividadeAtendentes_grafico_A_Serie_1_Tipo_Grafico = 'column';

                        foreach ( $individualsProdutividadeAtendentes as $keyItem => $itemValue )
                        {
                            $ProdutividadeAtendentes_grafico_A_Rotulos_Series_Eixo_X[] = $individualsProdutividadeAtendentes[ $keyItem ]->nomeusuario;
                            $ProdutividadeAtendentes_grafico_A_Serie_1_Dados[] = $individualsProdutividadeAtendentes[ $keyItem ]->contador;
                        }

                        $totalregistros = count( $individualsProdutividadeAtendentes );
                        session()->put( 'individualsProdutividadeAtendentes', $individualsProdutividadeAtendentes );
                        session()->put( 'totalregistros', $totalregistros );

                        session()->put( 'ProdutividadeAtendentes_grafico_A_Nome_Grafico', $ProdutividadeAtendentes_grafico_A_Nome_Grafico );
                        session()->put( 'ProdutividadeAtendentes_grafico_A_Titulo', $ProdutividadeAtendentes_grafico_A_Titulo );
                        session()->put( 'ProdutividadeAtendentes_grafico_A_Titulo_Eixo_Y', $ProdutividadeAtendentes_grafico_A_Titulo_Eixo_Y );
                        session()->put( 'ProdutividadeAtendentes_grafico_A_Rotulos_Series_Eixo_X', $ProdutividadeAtendentes_grafico_A_Rotulos_Series_Eixo_X );
                        session()->put( 'ProdutividadeAtendentes_grafico_A_Serie_1_Legenda', $ProdutividadeAtendentes_grafico_A_Serie_1_Legenda );
                        session()->put( 'ProdutividadeAtendentes_grafico_A_Serie_1_Tipo_Grafico', $ProdutividadeAtendentes_grafico_A_Serie_1_Tipo_Grafico );
                        session()->put( 'ProdutividadeAtendentes_grafico_A_Serie_1_Dados', $ProdutividadeAtendentes_grafico_A_Serie_1_Dados );
                    // fim grafico

                // Horários de Pico
                    $queryHorarioPico = DB::connection('mysqlCallcenter')->select( 'SELECT HOUR(chamadas.data_hora_inicio) as hora, COUNT(*) as contador
                                                FROM jucese_callcenter.chamadas chamadas ' . 
                                                ' GROUP BY HOUR(chamadas.data_hora_inicio) HAVING contador > 0 ORDER BY 1 ASC LIMIT 10' );

                    $nTotalAtendimentos = 0;
                    $maiorValor = 0;
                    foreach ( $queryHorarioPico as $individual ) { $nTotalAtendimentos = $nTotalAtendimentos + $individual->contador; if ( $individual->contador > $maiorValor ) { $maiorValor = $individual->contador; } }

                    foreach ($queryHorarioPico as $keyItem => $itemValue)
                    {   $queryHorarioPico[ $keyItem ]->item = $keyItem+1;

                        if ( $queryHorarioPico[ $keyItem ]->contador == $maiorValor ) 
                        { $queryHorarioPico[ $keyItem ]->maiorValor = 'Sim'; } 
                        else 
                        { $queryHorarioPico[ $keyItem ]->maiorValor = 'Não'; }

                        $queryHorarioPico[ $keyItem ]->percentual = ( $queryHorarioPico[ $keyItem ]->contador / $nTotalAtendimentos ) * 100;
                        $tituloGrafico_queryHorarioPico = $queryHorarioPico[ $keyItem ]->hora . 'h às ' . (string)($queryHorarioPico[ $keyItem ]->hora + 1) . 'h';
                        $rotulos1Grafico_queryHorarioPico[] = $tituloGrafico_queryHorarioPico;
                        $valores1Grafico_queryHorarioPico[] = $queryHorarioPico[ $keyItem ]->contador;
                    }

                    $tituloGrafico_horariosPico = 'Registros de Chamadas por Hora';

                    session()->put('queryHorarioPico', $queryHorarioPico);
                    if ( count( $queryHorarioPico ) > 0 )
                    {   $chart_queryHorarioPico = Charts::multi('bar', 'google')->title( $tituloGrafico_queryHorarioPico )->dimensions( 512, 300 )->template( "material" )->dataset( 'Horários de Pico', $valores1Grafico_queryHorarioPico )->labels( $rotulos1Grafico_queryHorarioPico )->responsive(false);
                        session()->put('chart_queryHorarioPico', $chart_queryHorarioPico);
                        session()->put('tituloGrafico_queryHorarioPico', $tituloGrafico_queryHorarioPico);
                        session()->put('rotulos1Grafico_queryHorarioPico', $rotulos1Grafico_queryHorarioPico); }

                // Produtividade Por Dia
                    $queryPorDia = DB::connection('mysqlCallcenter')->select( 'SELECT DAY(chamadas.data_hora_inicio) as dia, COUNT(*) as contador
                                                                            FROM jucese_callcenter.chamadas chamadas 
                                                                            GROUP BY DAY(chamadas.data_hora_inicio) ORDER BY DAY(chamadas.data_hora_inicio) LIMIT 31' );

                    $nTotalAtendimentos = 0;
                    $maiorValor = 0;
                    foreach ( $queryPorDia as $individual ) { $nTotalAtendimentos = $nTotalAtendimentos + $individual->contador; if ( $individual->contador > $maiorValor ) { $maiorValor = $individual->contador; } }
                                                
                    foreach ($queryPorDia as $keyItem => $itemValue)
                    {
                        $queryPorDia[ $keyItem ]->item = $keyItem+1;

                        if ( $queryPorDia[ $keyItem ]->contador == $maiorValor ) 
                        { $queryPorDia[ $keyItem ]->maiorValor = 'Sim'; } 
                        else 
                        { $queryPorDia[ $keyItem ]->maiorValor = 'Não'; }

                        $queryPorDia[ $keyItem ]->percentual = ( $queryPorDia[ $keyItem ]->contador / $nTotalAtendimentos ) * 100;
                        $rotulos1Grafico_queryPorDia[] = $queryPorDia[ $keyItem ]->dia;
                        $valores1Grafico_queryPorDia[] = $queryPorDia[ $keyItem ]->contador;
                    }

                    $tituloGrafico_queryPorDia = 'Atendimentos por Dia';

                    session()->put('queryPorDia', $queryPorDia);
                    if ( count( $queryPorDia ) > 0 )
                    {   $chart_queryPorDia = Charts::multi('bar', 'google')->
                                                    title( $tituloGrafico_queryPorDia )->dimensions( 512, 300 )->template( "material" )->
                                                    dataset( 'Dia', $valores1Grafico_queryPorDia )->
                                                    labels( $rotulos1Grafico_queryPorDia )->responsive(false);

                        session()->put('chart_queryPorDia', $chart_queryPorDia);
                        session()->put('tituloGrafico_queryPorDia', $tituloGrafico_queryPorDia);
                        session()->put('rotulos1Grafico_queryPorDia', $rotulos1Grafico_queryPorDia); }

                // Projeções de Produtividade
                    $queryProjecaoDia = DB::connection('mysqlCallcenter')->select( 'SELECT DAY(chamadas.data_hora_inicio) as dia, COUNT(*) as contador
                                                FROM jucese_callcenter.chamadas chamadas GROUP BY DAY(chamadas.data_hora_inicio) ORDER BY DAY(chamadas.data_hora_inicio) LIMIT 31' );

                    $queryProjecaoSemana = DB::connection('mysqlCallcenter')->select( 'SELECT DAY(chamadas.data_hora_inicio) as dia, COUNT(*) as contador
                                                FROM jucese_callcenter.chamadas chamadas GROUP BY DAY(chamadas.data_hora_inicio) ORDER BY DAY(chamadas.data_hora_inicio) LIMIT 31' );

                    $queryProjecaoMes = DB::connection('mysqlCallcenter')->select( 'SELECT DAY(chamadas.data_hora_inicio) as dia, COUNT(*) as contador
                                                FROM jucese_callcenter.chamadas chamadas GROUP BY DAY(chamadas.data_hora_inicio) ORDER BY DAY(chamadas.data_hora_inicio) LIMIT 31' );

                    $nTotalAtendimentos = 0;
                    foreach ( $queryPorDia as $individual ) { $nTotalAtendimentos = $nTotalAtendimentos + $individual->contador; }
                                                
                    foreach ($queryPorDia as $keyItem => $itemValue)
                    {   $queryPorDia[ $keyItem ]->item = $keyItem+1;
                        $queryPorDia[ $keyItem ]->percentual = ( $queryPorDia[ $keyItem ]->contador / $nTotalAtendimentos ) * 100;
                        $rotulos1Grafico_queryPorDia[] = $queryPorDia[ $keyItem ]->dia;
                        $valores1Grafico_queryPorDia[] = $queryPorDia[ $keyItem ]->contador; }

                    $tituloGrafico_queryPorDia = 'Projeções de Produtividade';

                    session()->put('queryPorDia', $queryPorDia);
                    if ( count( $queryPorDia ) > 0 )
                    {   $chart_queryPorDia = Charts::multi('bar', 'google')->title( $tituloGrafico_queryPorDia )->dimensions( 512, 300 )->template( "material" )->dataset( 'Dia', $valores1Grafico_queryPorDia )->labels( $rotulos1Grafico_queryPorDia )->responsive(false);
                        session()->put('chart_queryPorDia', $chart_queryPorDia);
                        session()->put('tituloGrafico_queryPorDia', $tituloGrafico_queryPorDia);
                        session()->put('rotulos1Grafico_queryPorDia', $rotulos1Grafico_queryPorDia); }


                // Atendimentos Por Ano / Mes
                    $queryAno = DB::connection('mysqlCallcenter')->select( 'SELECT YEAR(chamadas.data_hora_inicio) as ano, COUNT(*) as contador,
                                                        (
                                                        SELECT count( DISTINCT DATE( chamadas2.data_hora_inicio) )
                                                        FROM jucese_callcenter.chamadas chamadas2
                                                        WHERE YEAR( chamadas2.data_hora_inicio ) = ano
                                                        ) as diasuteis,
                                                        sum( time_to_sec( timediff( chamadas.data_hora_fim, chamadas.data_hora_inicio ) ) ) as soma
                                                FROM jucese_callcenter.chamadas chamadas ' . 
                                                ' GROUP BY YEAR(chamadas.data_hora_inicio) ORDER BY YEAR(chamadas.data_hora_inicio) ASC' );
                    
                    $nTotalAtendimentos = 0;
                    $maiorValor = 0;
                    foreach ( $queryAno as $individual ) { $nTotalAtendimentos = $nTotalAtendimentos + $individual->contador; if ( $individual->contador > $maiorValor ) { $maiorValor = $individual->contador; } }
                    
                    foreach ($queryAno as $keyItem => $itemValue)
                    {
                        $queryAno[ $keyItem ]->percentual = ( $queryAno[ $keyItem ]->contador / $nTotalAtendimentos ) * 100;
                        
                        if ( $queryAno[ $keyItem ]->contador == $maiorValor ) { $queryAno[ $keyItem ]->maiorValor = 'Sim'; } 
                        else { $queryAno[ $keyItem ]->maiorValor = 'Não'; }

                        $queryAno[ $keyItem ]->item = $keyItem+1;
                        $rotulos1Grafico_queryAno[] = $queryAno[ $keyItem ]->ano;
                        $valores1Grafico_queryAno[] = $queryAno[ $keyItem ]->contador;
                    }

                    $tituloGrafico_queryAno = 'Atendimentos por Ano';
                    session()->put('queryAno', $queryAno);
                    if ( count( $queryAno ) > 0 )
                    {   $chart_queryAno = Charts::multi('bar', 'google')->title( $tituloGrafico_queryAno )->
                                                    dimensions( 512, 300 )->template( "material" )->
                                                    dataset( 'Ano', $valores1Grafico_queryAno )->
                                                    labels( $rotulos1Grafico_queryAno )->responsive(false);
                        session()->put('chart_queryAno', $chart_queryAno);
                        session()->put('tituloGrafico_queryAno', $tituloGrafico_queryAno);
                        session()->put('rotulos1Grafico_queryAno', $rotulos1Grafico_queryAno); }


                    // dd( count( session()->get( 'queryAno' ) ), $tituloGrafico_queryAno, $chart_queryAno, $valores1Grafico_queryAno, $rotulos1Grafico_queryAno, $queryAno, session()->get( 'queryAno' ) );


                return view('callcenter.dashboard.acoes.index', compact( 'gruposuserLogado' ));
            }


}
