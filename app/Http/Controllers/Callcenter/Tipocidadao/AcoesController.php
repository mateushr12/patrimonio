<?php

namespace App\Http\Controllers\Callcenter\Tipocidadao;

use App\ERP\Acoes;
use App\Callcenter\Tipocidadao;
use App\Callcenter\Chamada;
use App\Callcenter\Cidadao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AcoesController extends Controller
{
    public $itensPorPagina = 5;

    public function index( Request $request )
    {
        $totalRegistros = Tipocidadao::count();

        if ( isset( $_POST['itensPorPagina5'] ) ) { $this->itensPorPagina = 5; } elseif ( isset( $_POST['itensPorPagina10'] ) ) { $this->itensPorPagina = 10; } elseif ( isset( $_POST['itensPorPaginaTodos'] ) ) { $this->itensPorPagina = 100; }

        if ( $request->descricao === null )
        { $individuals = Tipocidadao::orderby('descricao', 'asc')->get(); } //->paginate($this->itensPorPagina); }
        else
        { $individuals = Tipocidadao::whereRaw( 'upper(descricao) LIKE "%' . strtoupper($request->descricao) . '%"' )->orderby('descricao', 'asc')->get(); } //->paginate($this->itensPorPagina); }

        $filtro = $request->descricao;
        $titulo = 'Tipo de Cidadao';

        foreach ($individuals as $keyItem => $itemValue) { $individuals[ $keyItem ]->excluivel = 'Sim'; $nregistrosChamada = Chamada::where( 'tipocidadao_id', '=', $individuals[ $keyItem ]->id )->count(); if ( $nregistrosChamada > 0 ) { $individuals[ $keyItem ]->excluivel = 'Não'; } }
        foreach ($individuals as $keyItem => $itemValue) { $nregistrosChamada = Cidadao::where( 'tipocidadao_id', '=', $individuals[ $keyItem ]->id )->count(); if ( $nregistrosChamada > 0 ) { $individuals[ $keyItem ]->excluivel = 'Não'; } }

        session()->put('individuals', $individuals);

        return view( 'callcenter.tipocidadao.acoes.index', compact( 'individuals', 'totalRegistros', 'titulo', 'filtro' ) );
    }

    public function adicionar()
    {
        return redirect()->route('tipocidadao.adicionar');
    }

    public function salvar(Request $req)
    {
        $dados = $req->all();
        Tipocidadao::create($dados);
        return redirect()->route('tipocidadao.index');
    }

    public function excluir(Request $req)
    {
        $dados = $req->all();
        Acoes::deleted($dados);
        return redirect()->route('tipocidadao.index');
    }

//////////////////////////////////////////////////////////////////////////////////
    public function deletar($chave)
    {
        $ac = Tipocidadao::find($chave);
        if (isset($ac)) { $ac->delete(); }

        return redirect()->route('tipocidadao.index');
    }

    public function editar($chave)
    {
        $acoessalvas = Tipocidadao::all();
        $individual = Tipocidadao::find($chave);
        return view('callcenter.tipocidadao.acoes.editar', compact('individual'));
    }

    public function atualizar(Request $req, $chave)
    {
        $dados = $req->all();
        Tipocidadao::find($chave)->update($dados);
        return redirect()->route('tipocidadao.index');
    }

    public function imprimir( Request $request )
    {
        $titulo = $request->titulo;

        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->loadView( 'callcenter.tipocidadao.acoes.imprimir', compact( 'titulo' ) );

        return $pdf->stream('JUCESE-tipocidadao.pdf');
    }

}
