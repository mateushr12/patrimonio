<?php

namespace App\Http\Controllers\Callcenter\Palavra;

use App\ERP\Acoes;
use App\Callcenter\Palavra;
use App\Callcenter\Chamada;
use App\Callcenter\Cidadao;
use App\Callcenter\Tipoproblema;
use App\Callcenter\Tiposolucao;
use App\Callcenter\Tipoatendimento;
use App\Callcenter\Tipocidadao;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AcoesController extends Controller
{
    public $itensPorPagina = 5;

    public function index( Request $request )
    {

        
        // atualizar tabela palavra com as palavras das tabelas do callcenter
            // chamada
        /*
            $individuals = Chamada::all(); $palavra[] = array(  "palavra" => '', "qtde" => 0, ); 
            echo 'Tabela: Chamada - ' . count( $individuals ) . '<br>'; foreach ( $individuals as $keyItem => $itemValue ) 
            { $texto = trim( $individuals[ $keyItem ]->complemento_problema ) . ' ' . trim( $individuals[ $keyItem ]->complemento_solucao ); $palavras[] = explode( " ", $texto ); } set_time_limit(600);
            foreach ($palavras as $keyItem => $itemValue) { foreach ($palavras[ $keyItem ] as $keyItem2 => $itemValue2) { $palavra_atual = $itemValue2; if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '(', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ')', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ',', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '-', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '?', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '>', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '<', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '"', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '.', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( "'", ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '*', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ':', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ';', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '/', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '\\', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ' ', '', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { if ( seTemLetra( $palavra_atual ) ) { if ( trim( $palavra_atual ) != '' ) { if ( !seTodasSaoLetras( $palavra_atual ) ) { $palavraEncontrada = Palavra::where( 'palavra', $palavra_atual )->get(); if (count( $palavraEncontrada ) == 0  ) { $palavra = new Palavra(); $palavra->palavra = $palavra_atual; $palavra->save(); } } } } } } }
            // cidadao
            $individuals = Cidadao::all(); $palavra[] = array(  "palavra" => '', "qtde" => 0, ); 
            echo 'Tabela: Cidadao - ' . count( $individuals ) . '<br>'; foreach ( $individuals as $keyItem => $itemValue ) 
            { $texto = trim( $individuals[ $keyItem ]->nome ); $palavras[] = explode( " ", $texto ); } set_time_limit(600);
            foreach ($palavras as $keyItem => $itemValue) { foreach ($palavras[ $keyItem ] as $keyItem2 => $itemValue2) { $palavra_atual = $itemValue2; if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '(', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ')', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ',', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '-', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '?', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '>', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '<', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '"', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '.', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( "'", ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '*', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ':', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ';', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '/', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '\\', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ' ', '', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { if ( seTemLetra( $palavra_atual ) ) { if ( trim( $palavra_atual ) != '' ) { if ( !seTodasSaoLetras( $palavra_atual ) ) { $palavraEncontrada = Palavra::where( 'palavra', $palavra_atual )->get(); if (count( $palavraEncontrada ) == 0  ) { $palavra = new Palavra(); $palavra->palavra = $palavra_atual; $palavra->save(); } } } } } } }
            
            // tipo problema
            $individuals = Tipoproblema::all(); $palavra[] = array(  "palavra" => '', "qtde" => 0, ); 
            echo 'Tabela: Tipoproblema - ' . count( $individuals ) . '<br>'; foreach ( $individuals as $keyItem => $itemValue ) 
            { $texto = trim( $individuals[ $keyItem ]->descricao ); $palavras[] = explode( " ", $texto ); } set_time_limit(600);
            foreach ($palavras as $keyItem => $itemValue) { foreach ($palavras[ $keyItem ] as $keyItem2 => $itemValue2) { $palavra_atual = $itemValue2; if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '(', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ')', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ',', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '-', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '?', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '>', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '<', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '"', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '.', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( "'", ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '*', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ':', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ';', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '/', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '\\', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ' ', '', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { if ( seTemLetra( $palavra_atual ) ) { if ( trim( $palavra_atual ) != '' ) { if ( !seTodasSaoLetras( $palavra_atual ) ) { $palavraEncontrada = Palavra::where( 'palavra', $palavra_atual )->get(); if (count( $palavraEncontrada ) == 0  ) { $palavra = new Palavra(); $palavra->palavra = $palavra_atual; $palavra->save(); } } } } } } }

            // tipo solucao
            $individuals = Tiposolucao::all(); $palavra[] = array(  "palavra" => '', "qtde" => 0, ); 
            echo 'Tabela: Tiposolucao - ' . count( $individuals ) . '<br>'; foreach ( $individuals as $keyItem => $itemValue ) 
            { $texto = trim( $individuals[ $keyItem ]->descricao ); $palavras[] = explode( " ", $texto ); } set_time_limit(600);
            foreach ($palavras as $keyItem => $itemValue) { foreach ($palavras[ $keyItem ] as $keyItem2 => $itemValue2) { $palavra_atual = $itemValue2; if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '(', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ')', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ',', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '-', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '?', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '>', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '<', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '"', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '.', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( "'", ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '*', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ':', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ';', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '/', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '\\', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ' ', '', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { if ( seTemLetra( $palavra_atual ) ) { if ( trim( $palavra_atual ) != '' ) { if ( !seTodasSaoLetras( $palavra_atual ) ) { $palavraEncontrada = Palavra::where( 'palavra', $palavra_atual )->get(); if (count( $palavraEncontrada ) == 0  ) { $palavra = new Palavra(); $palavra->palavra = $palavra_atual; $palavra->save(); } } } } } } }

            // tipo atendimento
            $individuals = Tipoatendimento::all(); $palavra[] = array(  "palavra" => '', "qtde" => 0, ); 
            echo 'Tabela: Tipoatendimento - ' . count( $individuals ) . '<br>'; foreach ( $individuals as $keyItem => $itemValue ) 
            { $texto = trim( $individuals[ $keyItem ]->descricao ); $palavras[] = explode( " ", $texto ); } set_time_limit(600);
            foreach ($palavras as $keyItem => $itemValue) { foreach ($palavras[ $keyItem ] as $keyItem2 => $itemValue2) { $palavra_atual = $itemValue2; if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '(', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ')', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ',', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '-', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '?', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '>', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '<', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '"', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '.', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( "'", ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '*', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ':', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ';', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '/', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '\\', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ' ', '', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { if ( seTemLetra( $palavra_atual ) ) { if ( trim( $palavra_atual ) != '' ) { if ( !seTodasSaoLetras( $palavra_atual ) ) { $palavraEncontrada = Palavra::where( 'palavra', $palavra_atual )->get(); if (count( $palavraEncontrada ) == 0  ) { $palavra = new Palavra(); $palavra->palavra = $palavra_atual; $palavra->save(); } } } } } } }

            // tipo cidadao
            $individuals = Tipocidadao::all(); $palavra[] = array(  "palavra" => '', "qtde" => 0, ); 
            echo 'Tabela: Tipocidadao - ' . count( $individuals ) . '<br>'; foreach ( $individuals as $keyItem => $itemValue ) 
            { $texto = trim( $individuals[ $keyItem ]->descricao ); $palavras[] = explode( " ", $texto ); } set_time_limit(600);
            foreach ($palavras as $keyItem => $itemValue) { foreach ($palavras[ $keyItem ] as $keyItem2 => $itemValue2) { $palavra_atual = $itemValue2; if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '(', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ')', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ',', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '-', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '?', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '>', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '<', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '"', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '.', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( "'", ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '*', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ':', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ';', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '/', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( '\\', ' ', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { isset( $palavra_atual ) ? $palavra_atual = str_replace( ' ', '', $palavra_atual ) : $palavra_atual = $palavra_atual; } if ( $palavra_atual != '' ) { if ( seTemLetra( $palavra_atual ) ) { if ( trim( $palavra_atual ) != '' ) { if ( !seTodasSaoLetras( $palavra_atual ) ) { $palavraEncontrada = Palavra::where( 'palavra', $palavra_atual )->get(); if (count( $palavraEncontrada ) == 0  ) { $palavra = new Palavra(); $palavra->palavra = $palavra_atual; $palavra->save(); } } } } } } }
        */

        if ( isset( $_POST['itensPorPagina5'] ) ) { $this->itensPorPagina = 5; } elseif ( isset( $_POST['itensPorPagina10'] ) ) { $this->itensPorPagina = 10; } elseif ( isset( $_POST['itensPorPaginaTodos'] ) ) { $this->itensPorPagina = 100; }

        if ( $request->palavra === null )
        { $individuals = Palavra::orderby('palavra', 'asc')->paginate($this->itensPorPagina); }
        else
        { $individuals = Palavra::whereRaw( 'upper(palavra) LIKE "%' . strtoupper($request->palavra) . '%"' )->orderby('palavra', 'asc')->paginate($this->itensPorPagina); }



        $filtro = $request->palavra;
        $titulo = 'Palavra';
        $arrayCaracterErrado  = array( 'Ã‡Ãƒ', 'ÃŠ', 'Ã“', 'Ãƒ', 'Ã' );
        $arrayCaracterCorreto = array( 'ÇÃ', 'Ê', 'Ó', 'Ã', 'Í' );
        $arrayCaracterErrado   = array( 'ABREVIAÃ§ÃµES', 'ACONSELHÃVEL', 'ADMINISTRAÃ§Ã£O', 'ADMINISTRAÃ‡ÃƒO', 'ADMINISTRACAO', 'ADMISSÃƒO', 'ADVOCATÃCIO', 'ADVOCATÃCIOS', 'ALCÃ‚NTARA', 'ALOÃSIO', 'ALTERAÃ‡ÃƒO', 'ALTEARÃ‡ÃƒO', 'ALTERAÃ§Ã£O', 'ALTERAÃ§ÃµES', 'ALVARÃ', 'ANÃLISE', 'APROVAÃ§Ã£O', 'APROVAÃ‡ÃƒO', 'ATUAÃ‡ÃƒO', 'ATUALIZAÃ§Ã£O', 'ATUALIZAÃ‡ÃƒO', 'ATUALIZAÃ‡Ã•', 'ATUALIZÃ‡ÃƒO', 'AUDIÃŠNCIA', 'AUTENTICAÃ§Ã£O', 'AUNTETICAÃ‡ÃƒO', 'AUSÃŠNCIA', 'BALANÃ‡O', 'CARTÃ“RIO', 'CERTIDÃ•ES', 'CONSTITUIÃ‡ÃƒO', 'CORREÃ‡ÃƒO', 'DOCUMENTAÃ‡ÃƒO', 'DIÃRIO', 'DIGITALIZAÃ§Ã£O', 'DÃšVIDAS', 'ELETRÃ”NICO', 'EMPRESÃRIO', 'ESCRITÃ“RIO', 'EXIGÃŠNCIA', 'EXIGÃªNCIA', 'EXTINÃ‡ÃƒO', 'IDENTIFICACAO', 'JURÃDICA', 'LIGAÃ‡ÃƒO', 'MIGRAÃ§Ã£O', 'MUDANÃ§A', 'NECESSÃRIA', 'NÃƒO', 'ORIENTAÃ‡ÃƒO', 'RECLAMAÃ‡Ã•ES', 'SAÃDA', 'SÃ“CIO', 'TRANSFERÃŠNCIA', 'TRANSFORMAÃ‡ÃƒO', 'TÃ‰CNICO', 'Ã“RGÃƒOS', 'ÃšLTIMO', 'ÃšLTIMA', 'ÃŠNICO' );
        $arrayCaracterCorreto  = array( 'ABREVIAÇÕES',   'ACONSELHÁVEL',  'ADMINISTRAÇÃO',   'ADMINISTRAÇÃO',   'ADMINISTRAÇÃO', 'ADMISSÃO',  'ADVOCATÍCIO',   'ADVOCATÍCIOS',  'ALCÂNTARA',  'ALOÍSIO',   'ALTERAÇÃO',   'ALTERAÇÃO',   'ALTERAÇÃO',   'ALTERAÇÕES',   'ALVARÁ',  'ANÁLISE',  'APROVAÇÃO',   'APROVAÇÃO',   'ATUAÇÃO',   'ATUALIZAÇÃO',   'ATUALIZAÇÃO',   'ATUALIZAÇÃO',  'ATUALIZAÇÃO',  'AUDIÊNCIA',  'AUTENTICAÇÃO',   'AUTENTICAÇÃO',   'AUSÊNCIA',  'BALANÇO',  'CARTÓRIO',  'CERTIDÕES',  'CONSTITUIÇÃO',   'CORREÇÃO',   'DOCUMENTAÇÃO',   'DÁRIO',   'DIGITALIZAÇÃO',   'DÚVIDAS',  'ELETRÔNICO',  'EMPRESÁRIO',   'ESCRITÓRIO',  'EXIGÊNCIA',  'EXIGÊNCIA',  'EXTINÇÃO',   'IDENTIFICAÇÃO', 'JURÍDICA',  'LIGAÇÃO',   'MIGRAÇÃO',   'MUDANÇA',  'NECESSÁRIA',   'NÃO', 'ORIENTAÇÃO',   'RECLAMAÇÕES',   'SAÍDA',   'SÓCIO',  'TRANSFERÊNCIA',  'TRANSFORMAÇÃO',   'TÉCNICO',  'ÓRGÃOS',   'ÚLTIMO',  'ÚLTIMA',  'ÚNICO' );
        foreach ($individuals as $keyItem => $itemValue) 
        { $individuals[ $keyItem ]->correcao = $individuals[ $keyItem ]->palavra; 
            if ( in_array( $individuals[ $keyItem ]->correcao, $arrayCaracterErrado ) ) 
            { $individuals[ $keyItem ]->sugestao = str_replace( $individuals[ $keyItem ]->correcao, $arrayCaracterCorreto[ array_search( $individuals[ $keyItem ]->correcao, $arrayCaracterErrado ) ], $individuals[ $keyItem ]->palavra ); } }




        $arrayPalavraErrada   = array( 'ABREVIAÃ§ÃµES', 'ACONSELHÃVEL', 'ADMINISTRAÃ§Ã£O', 'ADMINISTRAÃ‡ÃƒO', 'ADMINISTRACAO', 'ADMISSÃƒO', 'ADVOCATÃCIO', 'ADVOCATÃCIOS', 'ALCÃ‚NTARA', 'ALOÃSIO', 'ALTERAÃ‡ÃƒO', 'ALTEARÃ‡ÃƒO', 'ALTERAÃ§Ã£O', 'ALTERAÃ§ÃµES', 'ALVARÃ', 'ANÃLISE', 'APROVAÃ§Ã£O', 'APROVAÃ‡ÃƒO', 'ATUAÃ‡ÃƒO', 'ATUALIZAÃ§Ã£O', 'ATUALIZAÃ‡ÃƒO', 'ATUALIZAÃ‡Ã•', 'ATUALIZÃ‡ÃƒO', 'AUDIÃŠNCIA', 'AUTENTICAÃ§Ã£O', 'AUNTETICAÃ‡ÃƒO', 'AUSÃŠNCIA', 'BALANÃ‡O', 'CARTÃ“RIO', 'CERTIDÃ•ES', 'CONSTITUIÃ‡ÃƒO', 'CORREÃ‡ÃƒO', 'DOCUMENTAÃ‡ÃƒO', 'DIÃRIO', 'DIGITALIZAÃ§Ã£O', 'DÃšVIDAS', 'ELETRÃ”NICO', 'EMPRESÃRIO', 'ESCRITÃ“RIO', 'EXIGÃŠNCIA', 'EXIGÃªNCIA', 'EXTINÃ‡ÃƒO', 'IDENTIFICACAO', 'JURÃDICA', 'LIGAÃ‡ÃƒO', 'MIGRAÃ§Ã£O', 'MUDANÃ§A', 'NECESSÃRIA', 'NÃƒO', 'ORIENTAÃ‡ÃƒO', 'RECLAMAÃ‡Ã•ES', 'SAÃDA', 'SÃ“CIO', 'TRANSFERÃŠNCIA', 'TRANSFORMAÃ‡ÃƒO', 'TÃ‰CNICO', 'Ã“RGÃƒOS', 'ÃšLTIMO', 'ÃšLTIMA', 'ÃŠNICO' );
        $arrayPalavraCorreta  = array( 'ABREVIAÇÕES',   'ACONSELHÁVEL',  'ADMINISTRAÇÃO',   'ADMINISTRAÇÃO',   'ADMINISTRAÇÃO', 'ADMISSÃO',  'ADVOCATÍCIO',   'ADVOCATÍCIOS',  'ALCÂNTARA',  'ALOÍSIO',   'ALTERAÇÃO',   'ALTERAÇÃO',   'ALTERAÇÃO',   'ALTERAÇÕES',   'ALVARÁ',  'ANÁLISE',  'APROVAÇÃO',   'APROVAÇÃO',   'ATUAÇÃO',   'ATUALIZAÇÃO',   'ATUALIZAÇÃO',   'ATUALIZAÇÃO',  'ATUALIZAÇÃO',  'AUDIÊNCIA',  'AUTENTICAÇÃO',   'AUTENTICAÇÃO',   'AUSÊNCIA',  'BALANÇO',  'CARTÓRIO',  'CERTIDÕES',  'CONSTITUIÇÃO',   'CORREÇÃO',   'DOCUMENTAÇÃO',   'DÁRIO',   'DIGITALIZAÇÃO',   'DÚVIDAS',  'ELETRÔNICO',  'EMPRESÁRIO',   'ESCRITÓRIO',  'EXIGÊNCIA',  'EXIGÊNCIA',  'EXTINÇÃO',   'IDENTIFICAÇÃO', 'JURÍDICA',  'LIGAÇÃO',   'MIGRAÇÃO',   'MUDANÇA',  'NECESSÁRIA',   'NÃO', 'ORIENTAÇÃO',   'RECLAMAÇÕES',   'SAÍDA',   'SÓCIO',  'TRANSFERÊNCIA',  'TRANSFORMAÇÃO',   'TÉCNICO',  'ÓRGÃOS',   'ÚLTIMO',  'ÚLTIMA',  'ÚNICO' );
        $arrayTabelaCampo = array( [ [ 'jucese_callcenter.tipoproblemas', 'descricao' ], 
                                     [ 'jucese_callcenter.tiposolucaos', 'descricao' ], 
                                     [ 'jucese_callcenter.tipocidadaos', 'descricao' ], 
                                     [ 'jucese_callcenter.tipoatendimentos', 'descricao' ], 
                                     [ 'jucese_callcenter.cidadaos', 'nome' ], 
                                     [ 'jucese_callcenter.chamadas', 'complemento_problema' ], 
                                     [ 'jucese_callcenter.chamadas', 'complemento_solucao' ] ] );
        $arraySQL = array();
        foreach ( $arrayTabelaCampo[0] as $keyItem => $itemValue ) { foreach ( $arrayPalavraCorreta as $keyItem2 => $itemValue2 ) { $arraySQL[] = 'UPDATE ' . $itemValue[0] . ' SET '. $itemValue[1] . ' = replace( ' . $itemValue[1] . ', "' . $arrayPalavraErrada[ $keyItem2 ] . '", "' . $arrayPalavraCorreta[ $keyItem2 ] . '" ) WHERE ' . $itemValue[1] . ' like "%' . $arrayPalavraErrada[ $keyItem2 ] . '%";'; } }





        session()->put('individuals', $individuals);

        return view( 'callcenter.palavra.acoes.index', compact( 'arraySQL', 'individuals', 'titulo', 'filtro' ) );
    }

    public function adicionar()
    {
        return redirect()->route('palavra.adicionar');
    }

    public function salvar(Request $req)
    {
        $dados = $req->all();
        Palavra::create($dados);
        return redirect()->route('palavra.index');
    }

    public function excluir(Request $req)
    {
        $dados = $req->all();
        Acoes::deleted($dados);
        return redirect()->route('palavra.index');
    }

//////////////////////////////////////////////////////////////////////////////////
    public function deletar($chave)
    {
        $ac = Palavra::find($chave);
        if (isset($ac)) { $ac->delete(); }

        return redirect()->route('palavra.index');
    }

    public function editar($chave)
    {
        $acoessalvas = Palavra::all();
        $individual = Palavra::find($chave);
        return view('callcenter.palavra.acoes.editar', compact('individual'));
    }

    public function atualizar(Request $req, $chave)
    {
        $dados = $req->all();
        Palavra::find($chave)->update($dados);
        return redirect()->route('palavra.index');
    }

    public function imprimir( Request $request )
    {
        $titulo = $request->titulo;

        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->loadView( 'callcenter.palavra.acoes.imprimir', compact( 'titulo' ) );

        return $pdf->stream('JUCESE-palavra.pdf');
    }

}
