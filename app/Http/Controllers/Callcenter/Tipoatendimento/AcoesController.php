<?php

namespace App\Http\Controllers\Callcenter\Tipoatendimento;

use App\ERP\Acoes;
use App\Callcenter\Tipoatendimento;
use App\Callcenter\Chamada;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AcoesController extends Controller
{
    public $itensPorPagina = 5;

    public function index( Request $request )
    {
        $totalRegistros = Tipoatendimento::count();
        
        if ( isset( $_POST['itensPorPagina5'] ) ) { $this->itensPorPagina = 5; } elseif ( isset( $_POST['itensPorPagina10'] ) ) { $this->itensPorPagina = 10; } elseif ( isset( $_POST['itensPorPaginaTodos'] ) ) { $this->itensPorPagina = 100; }

        if ( $request->descricao === null )
        { $individuals = Tipoatendimento::orderby('descricao', 'asc')->get(); } //->paginate($this->itensPorPagina); }
        else
        { $individuals = Tipoatendimento::whereRaw( 'upper(descricao) LIKE "%' . strtoupper($request->descricao) . '%"' )->orderby('descricao', 'asc')->get(); } //->paginate($this->itensPorPagina); }

        $filtro = $request->descricao;
        $titulo = 'Tipo de Atendimento';

        foreach ($individuals as $keyItem => $itemValue) { $individuals[ $keyItem ]->excluivel = 'Sim'; $nregistrosChamada = Chamada::where( 'tipoatendimento_id', '=', $individuals[ $keyItem ]->id )->count(); if ( $nregistrosChamada > 0 ) { $individuals[ $keyItem ]->excluivel = 'Não'; } }

        session()->put('individuals', $individuals);

        return view( 'callcenter.tipoatendimento.acoes.index', compact( 'individuals', 'totalRegistros', 'titulo', 'filtro' ) );
    }

    public function adicionar()
    {
        return redirect()->route('tipoatendimento.adicionar');
    }

    public function salvar(Request $req)
    {
        $dados = $req->all();
        Tipoatendimento::create($dados);
        return redirect()->route('tipoatendimento.index');
    }

    public function excluir(Request $req)
    {
        $dados = $req->all();
        Acoes::deleted($dados);
        return redirect()->route('tipoatendimento.index');
    }

//////////////////////////////////////////////////////////////////////////////////
    public function deletar($chave)
    {
        $ac = Tipoatendimento::find($chave);
        if (isset($ac)) { $ac->delete(); }

        return redirect()->route('tipoatendimento.index');
    }

    public function editar($chave)
    {
        $acoessalvas = Tipoatendimento::all();
        $individual = Tipoatendimento::find($chave);
        return view('callcenter.tipoatendimento.acoes.editar', compact('individual'));
    }

    public function atualizar(Request $req, $chave)
    {
        $dados = $req->all();
        Tipoatendimento::find($chave)->update($dados);
        return redirect()->route('tipoatendimento.index');
    }

    public function imprimir( Request $request )
    {
        $titulo = $request->titulo;
        
        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
        $pdf->loadView( 'callcenter.tipoatendimento.acoes.imprimir', compact( 'titulo' ) ); //->setPaper('a4', 'portrait');

        return $pdf->stream('JUCESE-tipoatendimento.pdf');
    }
}
