<?php

namespace App\Http\Controllers\Callcenter\Cidadao;

use App\ERP\Acoes;
use App\Callcenter\Tipocidadao;
use App\Callcenter\Chamada;
use App\Callcenter\Cidadao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AcoesController extends Controller
{
    public $itensPorPagina = 5;

    public function index( Request $request )
    {
        if ( isset( $_POST['itensPorPagina5'] ) ) { $this->itensPorPagina = 5; } 
        elseif ( isset( $_POST['itensPorPagina10'] ) ) { $this->itensPorPagina = 10; } 
        elseif ( isset( $_POST['itensPorPaginaTodos'] ) ) { $this->itensPorPagina = 100; }

        // set_time_limit(300);
        // $tipocidadao = Tipocidadao::all();
        $tipocidadao = Cidadao::selectRaw( 'tipocidadao_id as tipocidadao_id, tipocidadao_id as id, count(*) as contador' )
                                    ->groupby( 'tipocidadao_id' )
                                    ->get();

        
        if ( !isset( $request->tipocidadao_id ) )
        {
            $minimo = 1000;
            $tipocidadao_id = 0;

            foreach ($tipocidadao as $keyItem => $itemValue) 
            {
                if ( $tipocidadao[ $keyItem ]->contador < $minimo )
                {
                    $minimo = $tipocidadao[ $keyItem ]->contador; 
                    $tipocidadao_id = $tipocidadao[ $keyItem ]->tipocidadao_id;
                }
            }
        }
        else
        {
            $tipocidadao_id = $request->tipocidadao_id;
        }

        // dd( !isset( $tipocidadao_id ), $minimo, $tipocidadao, $tipocidadao[0]->tipocidadao_id, $tipocidadao[0]->tipocidadao->descricao, $tipocidadao[0]->contador );
        
        $totalRegistros = Cidadao::count();

        if ( $tipocidadao_id == 0 )
        {
            $individuals = Cidadao::orderby('nome', 'asc')->get(); //->paginate($this->itensPorPagina);
        }
        else
        {
            $individuals = Cidadao::whereRaw( 'tipocidadao_id = "' . $tipocidadao_id . '"' )
                                    ->orderby('nome', 'asc')->get();
            /*
                $individuals = Cidadao::whereRaw( '( upper(nome) LIKE "%' . strtoupper($request->nome) . '%" ) OR 
                                                ( upper(cpf) LIKE "%' . strtoupper($request->nome) . '%" ) OR
                                                ( upper(fone_fixo) LIKE "%' . strtoupper($request->nome) . '%" ) OR
                                                ( upper(fone_celular) LIKE "%' . strtoupper($request->nome) . '%" ) OR
                                                ( upper(fone_celular2) LIKE "%' . strtoupper($request->nome) . '%" ) OR
                                                ( upper(email) LIKE "%' . strtoupper($request->nome) . '%" )' )
                                        ->orderby('nome', 'asc')
                                        ->paginate($this->itensPorPagina);
            */
        }
        
        $filtro = $request->nome;

        // dd( $request, $individuals );
        $titulo = 'Cidadao';

        foreach ($individuals as $keyItem => $itemValue) 
        { 
            $individuals[ $keyItem ]->excluivel = 'Sim'; 
            $nregistrosChamada = Chamada::where( 'cidadao_id', '=', 
            $individuals[ $keyItem ]->id )->count(); 
            if ( $nregistrosChamada > 0 ) 
            { 
                $individuals[ $keyItem ]->excluivel = 'Não'; 
            } 
        }

        $tipocidadao_id_escolhido = $tipocidadao_id;

        session()->put('individuals', $individuals);
        
        return view( 'callcenter.cidadao.acoes.index', compact( 'totalRegistros', 'titulo', 'filtro', 'tipocidadao', 'tipocidadao_id_escolhido' ) );
    }

    public function adicionar()
    {
        $tipocidadao = Tipocidadao::all();
        return redirect()->route('cidadao.adicionar');
    }

    public function salvar(Request $req)
    {
        $dados = $req->all();

        $validaCPF = validaCPF( retirarMascaraCPF( $dados['cpf'] ) ); if ($validaCPF === false) { return redirect()->back()->with('msg', 'C.P.F. Inválido.'); } 

        $encontracpf = Cidadao::where( 'cpf', '=', retirarMascaraCPF( $dados['cpf'] ) );
        
        if ( $encontracpf->count() > 0 ) { return redirect()->back()->with('msg', 'C.P.F. já cadastrado.'); }

        $dados['cpf'] = retirarMascaraCPF($dados['cpf']);
        $dados['fone_fixo'] = retirarMascaraTelefone( $dados['fone_fixo'] );
        $dados['fone_celular'] = retirarMascaraTelefone( $dados['fone_celular'] );
        $dados['fone_celular2'] = retirarMascaraTelefone( $dados['fone_celular2'] );

        Cidadao::create($dados);

        return redirect()->route('cidadao.index');
    }

    public function excluir(Request $req)
    {
        $dados = $req->all();
        Acoes::deleted($dados);
        return redirect()->route('cidadao.index');
    }

//////////////////////////////////////////////////////////////////////////////////
    public function deletar($chave)
    {
        $ac = Cidadao::find($chave);
        if (isset($ac)) { $ac->delete(); }

        return redirect()->route('cidadao.index');
    }

    public function editar($chave)
    {
        $acoessalvas = Cidadao::all();
        $tipocidadao = Tipocidadao::all();
        $individual = Cidadao::find($chave);
        return view('callcenter.cidadao.acoes.editar', compact('individual', 'tipocidadao'));
    }

    public function atualizar(Request $req, $chave)
    {
        $dados = $req->all();
        $validaCPF = validaCPF( retirarMascaraCPF( $dados['cpf'] ) ); if ($validaCPF === false) { return redirect()->back()->with('msg', 'C.P.F. Inválido.'); } 

        $dados['cpf'] = retirarMascaraCPF($dados['cpf']);
        $dados['fone_fixo'] = retirarMascaraTelefone( $dados['fone_fixo'] );
        $dados['fone_celular'] = retirarMascaraTelefone( $dados['fone_celular'] );
        $dados['fone_celular2'] = retirarMascaraTelefone( $dados['fone_celular2'] );

        Cidadao::find($chave)->update($dados);

        return redirect()->route('cidadao.index');
    }

    public function imprimir( Request $request )
    {
        set_time_limit(300);
        $titulo = $request->titulo;

        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->loadView( 'callcenter.cidadao.acoes.imprimir', compact( 'titulo' ) ); //->setPaper('a4', 'portrait');

        return $pdf->stream('JUCESE-cidadao.pdf');
    }

}
