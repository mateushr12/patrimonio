<?php

namespace App\Http\Controllers\Callcenter\Tipoproblema;

use App\ERP\Acoes;
use App\Callcenter\Tipoproblema;
use App\Callcenter\Chamada;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Datatables;

class AcoesController extends Controller
{
    public $itensPorPagina = 5;

    public function index( Request $request )
    {
        $totalRegistros = Tipoproblema::count();

        if ( isset( $_POST['itensPorPagina5'] ) ) { $this->itensPorPagina = 5; } elseif ( isset( $_POST['itensPorPagina10'] ) ) { $this->itensPorPagina = 10; } elseif ( isset( $_POST['itensPorPaginaTodos'] ) ) { $this->itensPorPagina = 100; }

        if ( $request->descricao === null )
        { $individuals = Tipoproblema::orderby('descricao', 'asc')->get(); } //->paginate($this->itensPorPagina); }
        else
        { $individuals = Tipoproblema::whereRaw( 'upper(descricao) LIKE "%' . strtoupper($request->descricao) . '%"' )->orderby('descricao', 'asc')->get(); } //->paginate($this->itensPorPagina); }

        $filtro = $request->descricao;
        $titulo = 'Tipos de Problemas';

        foreach ($individuals as $keyItem => $itemValue) { $individuals[ $keyItem ]->excluivel = 'Sim'; $nregistrosChamada = Chamada::where( 'tipoproblema_id', '=', $individuals[ $keyItem ]->id )->count(); if ( $nregistrosChamada > 0 ) { $individuals[ $keyItem ]->excluivel = 'Não'; } }

        /*
        $palavra[] = '';
        foreach ($individuals as $keyItem => $itemValue) { $texto = trim( $individuals[ $keyItem ]->descricao ); $palavras[] = explode( " ", $texto ); }
        foreach ($palavras as $keyItem => $itemValue) { foreach ($palavras[ $keyItem ] as $keyItem2 => $itemValue2) { if ( !in_array( $itemValue2, $palavra ) ) { $palavra[] = $itemValue2; } } }
        sort( $palavra );
        $file = fopen('tipoproblemas-textos.txt','w'); fwrite($file,  print_r($palavra, TRUE));
        dd( $texto, $palavra );
        */


        session()->put('individuals', $individuals);

        return view( 'callcenter.tipoproblema.acoes.index', compact( 'individuals', 'totalRegistros', 'titulo', 'filtro' ) );
    }

    public function adicionar()
    {
        return redirect()->route('tipoproblema.adicionar');
    }

    public function salvar(Request $req)
    {
        $dados = $req->all();
        Tipoproblema::create($dados);
        return redirect()->route('tipoproblema.index');
    }

    public function excluir(Request $req)
    {
        $dados = $req->all();
        Acoes::deleted($dados);
        return redirect()->route('tipoproblema.index');
    }

//////////////////////////////////////////////////////////////////////////////////
    public function deletar($chave)
    {
        $ac = Tipoproblema::find($chave);
        if (isset($ac)) { $ac->delete(); }

        return redirect()->route('tipoproblema.index');
    }

    public function editar($chave)
    {
        $acoessalvas = Tipoproblema::all();
        $individual = Tipoproblema::find($chave);
        return view('callcenter.tipoproblema.acoes.editar', compact('individual'));
    }

    public function atualizar(Request $req, $chave)
    {
        $dados = $req->all();
        Tipoproblema::find($chave)->update($dados);
        return redirect()->route('tipoproblema.index');
    }

    public function imprimir( Request $request )
    {
        $titulo = $request->titulo;

        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true)->setPaper('a4', 'portrait');
        $pdf->loadView( 'callcenter.tipoproblema.acoes.imprimir', compact( 'titulo' ) ); //->setPaper('a4', 'portrait');
        return $pdf->stream('JUCESE-tipoproblema.pdf');
    }
}
