<?php

namespace App\Http\Controllers\Patrimonio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Patrimonio\Material;
use App\Patrimonio\Conservacao;
use App\Patrimonio\Proprietario;
use App\Patrimonio\Setor;
use App\Patrimonio\TipoMaterial;
use App\Patrimonio\Log;
use App\User;

use Auth;

class MaterialController extends Controller
{
    //
    public function index()
    {
      $material = Material::all();
      $tpmaterial = TipoMaterial::all();
      $setor = Setor::all();
      $conservacao = Conservacao::all();
      $proprietario = Proprietario::all();
      $usuario = User::all();

      return view('patrimonio.material.index', compact('material','tpmaterial','setor','conservacao','proprietario','usuario'));
    }

    public function salvar(Request $req)
    {
      try
      {
        $dados = $req->all();
        $material = Material::create($dados);
        Log::create([
          'tabela' => 'material',
          'idLinha' => $material->id,
          'user' => Auth::user()->name,
          'acao' => 'salvar'
        ]);
        return redirect()->route('material');
      }
      catch(\Illuminate\Database\QueryException $e)
      {
        return redirect()->back()->with('erro', 'Não foi possivel cadastrar o material. ERRO:'.$e->getMessage());
      }
      catch(PDOException $e)
      {
        return redirect()->back()->with('erro', 'Algo errado aconteceu, tente novamente!');
      }
    }

    public function editar($id)
    {
        $registro = Material::find($id);
        $tpmaterial = TipoMaterial::all();
        $setor = Setor::all();
        $conservacao = Conservacao::all();
        $proprietario = Proprietario::all();
        $usuario = User::all();

        return view('patrimonio.material.editar' , compact('registro','tpmaterial','setor','conservacao','proprietario','usuario'));
    }

    public function atualizar(Request $req, $id)
    {
        $dados = $req->all();
        $atualizar = Material::find($id);
        $atualizar->update($dados);
        Log::create([
          'tabela' => 'material',
          'idLinha' => $atualizar->id,
          'user' => Auth::user()->name,
          'acao' => 'atualizar'
        ]);
        return redirect()->route('material');

    }

    public function ver($id)
    {
      $material = Material::find($id);
      return view('patrimonio.material.ver', compact('material'));
    }

    public function deletar($id)
    {
      try
      {
        $ac = Material::find($id);
        $ac->delete();
        Log::create([
            'tabela' => 'material',
            'idLinha' => $ac->id,
            'user' => Auth::user()->name,
            'acao' => 'deletar'
        ]);
        return redirect()->back();
      }
      catch(\Illuminate\Database\QueryException $e)
      {
        return redirect()->back()->with('erro', 'Não foi possivel deletar o material. ERRO:'.$e->getMessage());
      }
      catch(PDOException $e)
      {
        return redirect()->back()->with('erro', 'Algo errado aconteceu, tente novamente!');
      }
    }


    public function etiqueta()
    {
      $material = Material::all();

      return view('patrimonio.material.etiqueta', compact('material'));
    }


    public function geraretiqueta(Request $req)
    {
      $material = $req->input('material_id');
      if (isset($material)){
        foreach ($material as $key => $value) {
          $mt[] = Material::find($value);
        }
        return view('patrimonio.material.geraretiqueta' , compact('mt'));
      }else{
        return redirect()->back();
      }
    }

    public function devolver($id)
    {
      $atualizar = Material::find($id);
      $atualizar->devolvido = date('Y-m-d');
      $atualizar->save();
      Log::create([
          'tabela' => 'material',
          'idLinha' => $id,
          'user' => Auth::user()->name,
          'acao' => 'atualizar'
      ]);
      return redirect()->back();
    }

    /////////// RELATORIO /////////////////////////////////////////////////
    public function filtro()
    {
      $tpmaterial = TipoMaterial::all();
      $setor = Setor::all();
      $proprietario = Proprietario::all();
      $usuario = User::all();

      return view('patrimonio.material.relatorio.filtro', compact('tpmaterial','setor','proprietario','usuario'));
    }

    public function resultadoRel(Request $req)
    {
      $a = Material::where(function($query) use($req) {
        if ($req->filled(['id_tipo_material'])) {
          $query->where('id_tipo_material', $req->input('id_tipo_material'));
        }
        if ($req->filled(['id_proprietario'])) {
          $query->where('id_proprietario', $req->input('id_proprietario'));
        }
        if ($req->filled(['id_setor_atual'])) {
          $query->where('id_setor_atual', $req->input('id_setor_atual'));
        }
        if ($req->filled(['id_usuario'])) {
          $query->where('id_usuario', $req->input('id_usuario'));
        }
        if ($req->filled(['devolvido'])) {
          if ($req->input('devolvido') == 1){
              $query->whereNull('devolvido');
          }elseif($req->input('devolvido') == 2){
              $query->whereNotNull('devolvido');
          }
        }
      })->get();

      return view('patrimonio.material.relatorio.imprimir', compact('a'));

      // $acoes = $a
      //   ->leftJoin('mysqlPatrimonio.orcamento_suplementar', 'mysqlPatrimonio..id', '=', 'mysqlPatrimonio..codAcoes')
      //   ->leftJoin('mysqlPatrimonio.empenhos', 'mysqlPatrimonio.acoes.id', '=', 'mysqlPatrimonio.empenhos.acaoempenho')
      //   ->whereNull('empenhos.deleted_at')
      //   ->selectRaw('sum(empenhos.valorempenho) as empenhado, acoes.*, orcamento_suplementar.valor')
      //   ->groupBy('acoes.id', 'orcamento_suplementar.valor')
      //   ->get();
    }
    ///////////////////////////////////////////////////////////////////////

}
