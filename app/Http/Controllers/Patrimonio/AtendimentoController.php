<?php

namespace App\Http\Controllers\Patrimonio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Patrimonio\Atendimentos;
use App\Patrimonio\Status;
use App\Patrimonio\Categoria;
use App\Patrimonio\TipoAtendimento;
use App\Patrimonio\Log;
use App\Patrimonio\Gestor;
use App\Patrimonio\Grupo;
use Auth;
use App\User;

class AtendimentoController extends Controller
{
  public function index()
  {
        $id_user = Auth::user()->id;
        $atendimentos = Atendimentos::where('id_requerente', $id_user)->orWhere('atribuido_para', $id_user)->orderBy('created_at', 'desc')->paginate(20);
        $status = Status::all();
        return view('patrimonio.atendimento.index', compact('atendimentos', 'status'));
  }

  public function buscaAtendimento(Request $req)
  {
        $id_status = $req->input('id_status');
        $id_user = Auth::user()->id;
        if(empty($id_status)){
            $atendimentos = Atendimentos::where('id_requerente', $id_user)->orWhere('atribuido_para', $id_user)->orderBy('created_at', 'desc')->paginate(20);
            $status = Status::all();
        }else{
            $atendimentos = Atendimentos::where([['id_status', $id_status], ['id_requerente', $id_user]])->orWhere([['atribuido_para', $id_user], ['id_status', $id_status]])->orderBy('created_at', 'desc')->paginate(20);
            $status = Status::all();
        }
        return view('patrimonio.atendimento.index', compact('atendimentos', 'status'));
  }

  public function novoAtendimento()
  {
        $id_user = Auth::user()->id;
        $tipo = TipoAtendimento::all();
        $categoria = Categoria::all();
        return view('patrimonio.atendimento.novo', compact('tipo', 'categoria', 'id_user'));
  }

  public function salvarAtendimento (Request $req)
  {
    $dados = $req->all();
    $novo = Atendimentos::create($dados);
    Log::create([
            'tabela' => 'atendimentos',
            'idLinha' => $novo->id,
            'user' => Auth::user()->name,
            'acao' => 'novoAtendimento'
    ]);
    
    $id_user = Auth::user()->id;
    $atendimentos = Atendimentos::where('id_requerente', $id_user)->orWhere('atribuido_para', $id_user)->orderBy('created_at', 'desc')->paginate(20);
    $status = Status::all();
    return view('patrimonio.atendimento.index', compact('atendimentos', 'status'));
  } 


  public function verAtendimento($id)
  {
        $atendimentos = Atendimentos::find($id);
        return view('patrimonio.atendimento.visualizar', compact('atendimentos'));
  }

  public function atribuirAtendimento($id)
  {     
        $atendimentos = Atendimentos::find($id);
        $usuarios = User::all();
        return view('patrimonio.atendimento.atribuir', compact('atendimentos', 'usuarios'));
  }

  public function fecharAtendimento($id)
  {     
        $atendimentos = Atendimentos::find($id);
        $usuarios = User::all();
        return view('patrimonio.atendimento.fechar', compact('atendimentos', 'usuarios'));
  }

  public function atualizarAtendimento(Request $req, $id)
  {   
      $dados = $req->all();
      $atualizar = Atendimentos::find($id);
      $atualizar->update($dados);
      Log::create([
        'tabela' => 'Atendimentos',
        'idLinha' => $atualizar->id,
        'user' => Auth::user()->name,
        'acao' => 'atualizar'
      ]);
      return redirect()->route('atendimentos');

  }

  public function inserirGestor (Request $req)
  {

  }

  public function excluirGestor ($id)
  {

  }

  public function grupos()
  {
        $id_user = Auth::user()->id;
        $grupo = Grupo::all();
        return view('patrimonio.atendimento.gestor.grupos', compact('grupo'));
  }





}
