<?php

namespace App\Http\Controllers\Patrimonio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Patrimonio\Componente;
use App\Patrimonio\Material;
use App\Patrimonio\TipoMaterial;
use App\Patrimonio\Log;
use App\User;

use Auth;

class ComponenteController extends Controller
{
    //
    public function index($id)
    {
      $material = Material::find($id);
      $componente = Componente::where('id_material',$id);
      $tpmaterial = TipoMaterial::all();
      return view('patrimonio.material.componente', compact('material','componente','tpmaterial'));
    }

    public function salvar(Request $req)
    {
      try
      {
        $dados = $req->all();
        $componente = Componente::create($dados);
        Log::create([
          'tabela' => 'componente',
          'idLinha' => $componente->id,
          'user' => Auth::user()->name,
          'acao' => 'salvar'
        ]);
        return redirect()->route('componente');
      }
      catch(\Illuminate\Database\QueryException $e)
      {
        return redirect()->back()->with('erro', 'Não foi possivel cadastrar o material. ERRO:'.$e->getMessage());
      }
      catch(PDOException $e)
      {
        return redirect()->back()->with('erro', 'Algo errado aconteceu, tente novamente!');
      }
    }    

    public function ver($id)
    {
      $material = Material::find($id);
      return view('patrimonio.material.ver', compact('material'));
    }

    public function deletar($id)
    {
      try
      {
        $ac = Material::find($id);
        $ac->delete();
        Log::create([
            'tabela' => 'material',
            'idLinha' => $ac->id,
            'user' => Auth::user()->name,
            'acao' => 'deletar'
        ]);
        return redirect()->back();
      }
      catch(\Illuminate\Database\QueryException $e)
      {
        return redirect()->back()->with('erro', 'Não foi possivel deletar o material. ERRO:'.$e->getMessage());
      }
      catch(PDOException $e)
      {
        return redirect()->back()->with('erro', 'Algo errado aconteceu, tente novamente!');
      }
    }

}
