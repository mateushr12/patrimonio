<?php

namespace App\Http\Controllers\Patrimonio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Patrimonio\Setor;
use App\Patrimonio\Conservacao;
use App\Patrimonio\Proprietario;
use App\Patrimonio\TipoMaterial;

class OutrosCadastrosController extends Controller
{
    /////// SETOR //////////
    public function inicioSetor()
    {
      $setor = Setor::all();
      return view('patrimonio.outroscadastros.setor.index' , compact('setor'));
    }

    public function salvarSetor(Request $req)
    {
      try {

        $dados = $req->all();
        $existe = Setor::where('descricao', $dados['descricao'])->exists();
        if ($existe){
            return redirect()->back()->with('erro', 'Este setor já existe.');
        }else{
          Setor::create($dados);
          return redirect()->back()->with('msg', 'Setor salvo com sucesso.');
        }

      } catch (\Illuminate\Database\QueryException $e) {

        return redirect()->back()->with('erro', 'Não foi possivel cadastrar o setor.');

      }
    }

    public function deletarSetor($id)
    {
      $del = Setor::find($id);
      $del->delete();

      return redirect()->back()->with('delete', 'Registro deletado com sucesso');

    }
    ///////////////////////

    ///// CONSERVAÇÃO ////////
    public function inicioConservacao()
    {
      $conservacao = Conservacao::all();
      return view('patrimonio.outroscadastros.conservacao.index' , compact('conservacao'));
    }

    public function salvarConservacao(Request $req)
    {
      try {

        $dados = $req->all();
        Conservacao::create($dados);
        return redirect()->back()->with('msg', 'Registro salvo com sucesso.');

      } catch (\Illuminate\Database\QueryException $e) {

        return redirect()->back()->with('erro', 'Não foi possível cadastrar o registro.');

      }
    }

    public function deletarConservacao($id)
    {
      $del = Conservacao::find($id);
      $del->delete();

      return redirect()->back()->with('delete', 'Registro deletado com sucesso');
    }
    /////////////////////////

    //// PROPRIETARIO //////
    public function inicioProprietario()
    {
      $proprietario = Proprietario::all();
      return view('patrimonio.outroscadastros.proprietario.index' , compact('proprietario'));
    }

    public function salvarProprietario(Request $req)
    {
      try {

        $dados = $req->all();
        Proprietario::create($dados);
        return redirect()->back()->with('msg', 'Registro salvo com sucesso.');

      } catch (\Illuminate\Database\QueryException $e) {

        return redirect()->back()->with('erro', 'Não foi possível cadastrar o registro.');

      }
    }

    public function deletarProprietario($id)
    {
      $del = Proprietario::find($id);
      $del->delete();

      return redirect()->back()->with('delete', 'Registro deletado com sucesso');
    }
   ////////////////////////

   //// TIPO MATERIAL ////
   public function inicioTpMaterial()
   {
     $tpMaterial = TipoMaterial::all();
     return view('patrimonio.outroscadastros.tpMaterial.index' , compact('tpMaterial'));
   }

   public function salvarTpMaterial(Request $req)
   {
     try {

       $dados = $req->all();
       $existe = TipoMaterial::where('descricao', $dados['descricao'])->exists();
       if ($existe){
           return redirect()->back()->with('erro', 'Este tipo de material já existe.');
       }else{
         TipoMaterial::create($dados);
         return redirect()->back()->with('msg', 'Registro salvo com sucesso.');
       }

     } catch (\Illuminate\Database\QueryException $e) {

       return redirect()->back()->with('erro', 'Não foi possível cadastrar o registro.');

     }
   }

   public function deletarTpMaterial($id)
   {
     $del = TipoMaterial::find($id);
     $del->delete();

     return redirect()->back()->with('delete', 'Registro deletado com sucesso');
   }
   ///////////////////////
}
