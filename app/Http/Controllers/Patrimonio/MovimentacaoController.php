<?php

namespace App\Http\Controllers\Patrimonio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Patrimonio\Movimentacao;
use App\Patrimonio\Material;
use App\Patrimonio\Atendimentos;
use App\Patrimonio\Setor;
use App\Patrimonio\Log;
use App\User;
use Auth;

use Response;

class MovimentacaoController extends Controller
{
    //
    public function index()
    {
      $mov = Movimentacao::all();
      $material = Material::whereNull('devolvido')->get();
      $setor = Setor::all();
      $usuario = User::all();
      $atend = Atendimentos::where('id_categoria',1)->where('id_status',6)->get();

      return view('patrimonio.movimentacao.index', compact('mov','material','setor','usuario','atend'));
    }


    public function salvar(Request $req)
    {
      try
      {
        $dados = $req->all();
        //dd($dados);
        $mov = Movimentacao::create($dados);

        $atualizar = Material::find($dados['id_material']);
        $atualizar->id_setor_atual = $dados['id_setor_destino'];
        $atualizar->id_usuario = $dados['id_usuario_destino'];
        $atualizar->save();

        Log::create([
          'tabela' => 'movimentacao',
          'idLinha' => $mov->id,
          'user' => Auth::user()->name,
          'acao' => 'salvar'
        ]);
        return redirect()->route('movimentacao');
      }
      catch(\Illuminate\Database\QueryException $e)
      {
        return redirect()->back()->with('erro', 'Não foi possivel cadastrar a movimentação. ERRO:'.$e->getMessage());
      }
      catch(PDOException $e)
      {
        return redirect()->back()->with('erro', 'Algo errado aconteceu, tente novamente!');
      }
    }

    public function origem($id)
    {
      $sub = Material::where('id', $id)->get();
      foreach ($sub as $key => $value) {
        $vl['descricao'] = $value->setor->descricao;
        $vl['id'] = $value->id_setor_atual;
        $vl['usuario'] = isset($value->id_usuario) ? $value->usuario->name : 'Sem Usuário';
        $vl['idusuario'] = $value->id_usuario;
      }
      return Response::json($vl);
    }

    public function editar($id)
    {
        $registro = Movimentacao::find($id);
        $material = Material::all();
        $setor = Setor::all();
        $usuario = User::all();
        $atend = Atendimentos::where('id_categoria',1)->where('id_status',6)->get();

        return view('patrimonio.movimentacao.editar' , compact('registro','material','setor','usuario','atend'));
    }

    public function atualizar(Request $req, $id)
    {
        $dados = $req->all();
        $atualizar = Movimentacao::find($id);
        $atualizar->update($dados);
        Log::create([
          'tabela' => 'movimentacao',
          'idLinha' => $atualizar->id,
          'user' => Auth::user()->name,
          'acao' => 'atualizar'
        ]);
        return redirect()->route('movimentacao');

    }

    public function ver($id)
    {
      $mov = Movimentacao::find($id);
      return view('patrimonio.movimentacao.ver', compact('mov'));
    }

    public function deletar($id)
    {
      try
      {
        $ac = Movimentacao::find($id);
        $ac->delete();
        Log::create([
            'tabela' => 'movimentacao',
            'idLinha' => $ac->id,
            'user' => Auth::user()->name,
            'acao' => 'deletar'
        ]);
        return redirect()->back();
      }
      catch(\Illuminate\Database\QueryException $e)
      {
        return redirect()->back()->with('erro', 'Não foi possivel deletar a movimentação. ERRO:'.$e->getMessage());
      }
      catch(PDOException $e)
      {
        return redirect()->back()->with('erro', 'Algo errado aconteceu, tente novamente!');
      }
    }

    /////////// RELATORIO /////////////////////////////////////////////////
    public function filtro()
    {
      $material = Material::all();
      return view('patrimonio.movimentacao.relatorio.filtro', compact('material'));
    }

    public function resultadoRel(Request $req)
    {
      $a = Movimentacao::where(function($query) use($req) {
        if ($req->filled(['id_material'])) {
          $query->where('id_tipo_material', $req->input('id_tipo_material'));
        }
        if ($req->filled(['data_i'])) {
          $query->whereBetween('data', [$req->input('data_i'),$req->input('data_f')]);
        }
      })->get();

      return view('patrimonio.movimentacao.relatorio.imprimir', compact('a'));

      // $acoes = $a
      //   ->leftJoin('mysqlPatrimonio.orcamento_suplementar', 'mysqlPatrimonio..id', '=', 'mysqlPatrimonio..codAcoes')
      //   ->leftJoin('mysqlPatrimonio.empenhos', 'mysqlPatrimonio.acoes.id', '=', 'mysqlPatrimonio.empenhos.acaoempenho')
      //   ->whereNull('empenhos.deleted_at')
      //   ->selectRaw('sum(empenhos.valorempenho) as empenhado, acoes.*, orcamento_suplementar.valor')
      //   ->groupBy('acoes.id', 'orcamento_suplementar.valor')
      //   ->get();
    }
    ///////////////////////////////////////////////////////////////////////


}
