<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ERP\Suprimento;
use App\ERP\Acoes;
use App\ERP\Elemento;
use App\ERP\Empenho;
use App\ERP\Aditivos;
use App\ERP\OrcamentoInicial;
use App\ERP\Contrato;
use App\ERP\Fornecedores;
use App\ERP\ModalidadeLicitacao;
use App\ERP\AnexosDespesaSupri;
use App\ERP\AnexosSuprimentos;
use App\ERP\RefLegal;
use App\ERP\Ano;
use App\ERP\SuprimentoDespesa;
use App\ERP\OrcamentoSuplementar;
use App\ERP\Log;

use Auth;

class SuprimentosController extends Controller
{
  public function index()
  {
    $suprimentos = Suprimento::leftJoin('suprimento_despesa', function ($join) {
      $join->on('suprimentos.id', '=', 'suprimento_despesa.suprimento_id')->whereNull('suprimento_despesa.deleted_at');
      })
    ->selectRaw('sum(suprimento_despesa.valor) as usado, suprimentos.*')
    ->groupBy('suprimentos.id')
    ->get();
    //dd($suprimentos);
    //$suprimentos = Suprimento::all();
    $elementos = Elemento::all();
    $orcamento = OrcamentoInicial::where('ativo', 0)->max('id');
    $acoes = Acoes::where('CodOrcamento', $orcamento)->get();
    return view('erp.suprimentos.index', compact('suprimentos', 'elementos', 'acoes'));
  }


  public function salvar(Request $req)
  {
    $vl = str_replace(',', '.', str_replace('.', '', $req->input('valor')));
    $req->request->add([
      'valor' => $vl
    ]);
    $dados = $req->all();

    if (Suprimento::where('numero', '=', $req->input('numero'))->exists())
    {
      return redirect()->back()->with('erro', "Já existe um suprimento com esse número.");
    }else{
      $acao = Acoes::find($req->input('acao_id'));
      $acaosupl = OrcamentoSuplementar::where('codAcoes',$req->input('acao_id'))->get();
      foreach ($acaosupl as $key => $value) {
        $vlSpl = $value['valor'];
      }
      $vlSpl = isset($vlSpl) ? $vlSpl : 0.00;
      $vlAcao = $acao->ValorDestinado + $vlSpl;

      $vlEmpenhado = Empenho::where('acaoempenho',$req->input('acao_id'))->sum('valorempenho');
      $vlDisponivel = $vlAcao - $vlEmpenhado;
      $vlDisp = number_format($vlDisponivel,2,',','.');

      if ($vl > $vlDisponivel)
      {
        return redirect()->back()->with('erro', "Valor do suprimento maior que o valor disponível (R$ $vlDisp) na Ação.");
      }
      else
      {
        //dd($dados);
        $criar = Suprimento::create($dados);
        Log::create([
          'tabela' => 'suprimentos',
          'idLinha' => $criar->id,
          'user' => Auth::user()->name,
          'acao' => 'salvar'
        ]);
        return redirect()->route('suprimento');
      }
    }
  }

  public function empenho($id)
  {
    $suprimento = Suprimento::find($id);
    $acao = Acoes::find($suprimento->acao_id);
    $elemento = Elemento::find($suprimento->elemento_id);
    $contratos = Contrato::all();
    $modLs = ModalidadeLicitacao::all();
    $refs = RefLegal::all();
    return view('erp.suprimentos.empenho', compact('acao', 'elemento', 'contratos', 'modLs', 'refs', 'suprimento'));
  }


  public function editar($id)
  {
    // return view('erp.acoes.editar', compact('fontes', 'grupos', 'elementos', 'registro', 'acoessalvas', 'especificacao'));
  }

  public function atualizar(Request $req, $id)
  {
    // $dados = $req->all();
    // $findId = Acoes::find($id);
    // $findId->update($dados);
    // Log::create([
    //   'tabela' => 'acoes',
    //   'idLinha' => $findId->id,
    //   'user' => Auth::user()->id,
    //   'acao' => 'atualizar'
    // ]);
    // return redirect()->route('erp.acoes');
  }

  public function deletar($id)
  {
    $sp = Suprimento::find($id);

    if (isset($sp->empenho_id))
    {
      return redirect()->back()->with('erro', "Não é possível excluir, há um empenho cadastrado para esse suprimento.");

    }elseif (SuprimentoDespesa::where('suprimento_id',$id)->exists()){

      return redirect()->back()->with('erro', "Não é possível excluir, há despesas cadastradas para esse suprimento.");

    }elseif (AnexosSuprimentos::where('id_suprimento',$id)->exists()){

      return redirect()->back()->with('erro', "Não é possível excluir, há arquivos anexo.");

    }

    $sp->delete();

    Log::create([
      'tabela' => 'suprimentos',
      'idLinha' => $sp->id,
      'user' => Auth::user()->name,
      'acao' => 'deletar'
    ]);
    return redirect()->back()->with('ok', 'Suprimento deletado.');
  }

  public function imprimirSuprimento($id)
  {
    $s = Suprimento::find($id);
    $d = SuprimentoDespesa::where('suprimento_id', $id)->get();
    return view('erp.suprimentos.imprimir', compact('s','d'));
  }

  public function despesa($id)
  {
    $despesa = SuprimentoDespesa::where('suprimento_id', $id)->get();
    $suprimento = Suprimento::find($id);
    $fornecedor = Fornecedores::where('status', 1)->get();
    $vlsupri = Suprimento::where('id',$id)->sum('valor');
    $vldesp = SuprimentoDespesa::where('suprimento_id',$id)->sum('valor');
    $vldisp = ($vlsupri - $vldesp);
    return view('erp.suprimentos.despesa' , compact('despesa','suprimento','fornecedor','vldisp'));

  }

  public function salvarDespesa(Request $req, $id)
  {
    $req->merge([
      'data' => date('Y-m-d')
    ]);
    $dados = $req->all();

    $vlsupri = Suprimento::where('id',$id)->sum('valor');
    $vldesp = SuprimentoDespesa::where('suprimento_id',$id)->sum('valor');
    $vldisp = ($vlsupri - $vldesp);

    if ($req->input('valor') > $vldisp)
    {
      //dd('erro valor é maior que o valor a ser liquidado');
      return redirect()->back()->with('erro', "Valor é maior que o saldo disponível.");
    }elseif ($req->input('valor') == 0.00)
    {
      //dd('erro valor não pode der zero');
      return redirect()->back()->with('erro', "Valor não pode ser zero.");
    }else
    {
      $s = SuprimentoDespesa::create($dados);
      Log::create([
        'tabela' => 'suprimento_despesa',
        'idLinha' => $s->id,
        'user' => Auth::user()->name,
        'acao' => 'salvar'
      ]);
      return redirect()->back();
    }
  }

  public function deletarDespesa($id)
  {
    $sd = SuprimentoDespesa::find($id);
    if (AnexosDespesaSupri::where('id_despesa',$id)->exists())
    {
      return redirect()->back()->with('erro', "Não é possível excluir, há arquivos anexo.");
    }
    $sd->delete();
    Log::create([
      'tabela' => 'suprimento_despesa',
      'idLinha' => $sd->id,
      'user' => Auth::user()->name,
      'acao' => 'deletar'
    ]);
    return redirect()->back();
  }

  ////////////////////////////////////// ANEXOS ////////////////////////////////
  public function anexar($id)
  {
    $ids = $id;
    //$idsupl = (SuprimentoDespesa::find($id))->suprimento_id;
    $arquivo = AnexosSuprimentos::where('id_suprimento', $id)->get();
    return view('erp.suprimentos.anexar' , compact('arquivo','ids'));
  }


  public function anexarSalvar(Request $req)
  {
    $nomereq = $req->file('arquivo')->store('suprimentos');
    $supri = AnexosSuprimentos::create([
      'id_suprimento' => $req->input('id_suprimento'),
      'descricao' => $req->input('descricao'),
      'arquivo' => $nomereq
    ]);
    Log::create([
      'tabela' => 'anexos_suprimentos',
      'idLinha' => $supri->id,
      'user' => Auth::user()->name,
      'acao' => 'salvar'
    ]);
    return redirect()->back();
  }

  public function arquivo($id)
  {
    try {
      $arquivo = AnexosSuprimentos::find($id);
      $narq = $arquivo->arquivo;
      return response()->file(storage_path("app/public/$narq"));
    } catch (\Exception $e) {
      return view('notfound');
    }
  }

  public function deleteArquivo($id)
  {
    $arquivo = AnexosSuprimentos::find($id);
    $narq = $arquivo->arquivo;
    unlink(storage_path("app/public/$narq"));
    AnexosSuprimentos::find($id)->delete();
    Log::create([
      'tabela' => 'anexos_suprimentos',
      'idLinha' => $id,
      'user' => Auth::user()->name,
      'acao' => 'deletar'
    ]);
    return redirect()->back();
  }
  //////////////////////////////////////////////////////////////////////////////


  ////////////////////////////////////// ANEXOS DESPESAS ///////////////////////
  public function anexarDespesa($id)
  {
    $idespesa = $id;
    $idsupl = (SuprimentoDespesa::find($id))->suprimento_id;
    $arquivo = AnexosDespesaSupri::where('id_despesa', $id)->get();
    return view('erp.suprimentos.anexardespesa' , compact('arquivo','idespesa','idsupl'));
  }


  public function anexardepesaSalvar(Request $req)
  {
    $idespesa = $req->input('id');
    $nomereq = $req->file('arquivo')->store('suprimentos/despesas');
    $despesa = AnexosDespesaSupri::create([
      'id_despesa' => $req->input('id_despesa'),
      'arquivo' => $nomereq,
      'descricao' => $req->input('descricao')
    ]);
    Log::create([
      'tabela' => 'anexos_despesa',
      'idLinha' => $despesa->id,
      'user' => Auth::user()->name,
      'acao' => 'salvar'
    ]);
    return redirect()->back();
  }

  public function arquivoDespesa($id)
  {
    try {
      $arquivo = AnexosDespesaSupri::find($id);
      $narq = $arquivo->arquivo;
      return response()->file(storage_path("app/public/$narq"));
    } catch (\Exception $e) {
      return view('notfound');
    }
  }

  public function deletearquivoDel($id)
  {
    $arquivo = AnexosDespesaSupri::find($id);
    $narq = $arquivo->arquivo;
    unlink(storage_path("app/public/$narq"));
    AnexosDespesaSupri::find($id)->delete();
    Log::create([
      'tabela' => 'anexos_despesa',
      'idLinha' => $id,
      'user' => Auth::user()->name,
      'acao' => 'deletar'
    ]);
    return redirect()->back();
  }
  //////////////////////////////////////////////////////////////////////////////

  public function salvarEmpenho(Request $req, $id)
  {
    $dados = $req->all();
    if ($req->input('subelemento') === null){
      return redirect()->back()->with('erro', "Sub-elemento não pode ser vazio.");
    }else{
      $vlEmpenho = str_replace(',', '.', str_replace('.', '', $req->input('valorempenho')));

      $vlContratado = Empenho::where('licempenho',$req->input('licempenho'))->sum('valorempenho');
      $vlAditivo = Aditivos::where('id_contrato',$req->input('licempenho'))->sum('valor');
      $vlsuprimento = Suprimento::find($id)->valor;


      $req->merge([
        'valorempenho' => $vlEmpenho
      ]);
      $dados = $req->all();

      if (round($vlEmpenho,2) > round($vlsuprimento,2)){
        return redirect()->back()->with('erro', "Valor do empenho maior que o valor do suprimento ($vlsuprimento).");
      }else{
        $dt = explode('-', $req->input('dataempenho'));

        $ano = Ano::join('orcamento_inicial', function ($join) {
          $join->on('anos.id', '=', 'orcamento_inicial.ano');
        })
        ->join('acoes', function ($join) {
          $join->on('orcamento_inicial.id', '=', 'acoes.CodOrcamento');
        })
        ->where('acoes.id', $req->input('acaoempenho'))
        ->where('anos.ano', $dt[0])
        ->exists();

        if($ano == false){
          return redirect()->back()->with('erro', "Data do empenho diferente do ano da ação.");
        }
        ///////////////////////////////////////// Validar so contrato sem a cota /////////////////////////////////////////////////////////////////
        $banco = $req->input('bancoprecoempenho');
        if ($banco == 1){
          if ($req->input('licempenho') == ''){
            return redirect()->back()->with('erro', "Escolha uma licitacão");
          }
          $contrato = Contrato::find($req->input('licempenho'));
          $vlContrato = ($contrato->valorcontrato) +   $vlAditivo;
          $vlDisponivelContr = $vlContrato - $vlContratado;
          //$data = (isset($contrato->vigencia) ? $contrato->vigencia : $contrato->finalvigencia);

          if (round($vlEmpenho,2) > round($vlDisponivelContr,2)){
            return redirect()->back()->with('erro', "Valor do empenho maior que o valor do contrato disponível ($vlDisponivelContr).");
            // }elseif (date('Y-m-d') > $data){
            //   return redirect()->back()->with('erro', "Erro, contrato fora de vigência.");
          }else{
            $ep = Empenho::create($dados);
            Log::create([
              'tabela' => 'empenhos',
              'idLinha' => $ep->id,
              'user' => Auth::user()->name,
              'acao' => 'salvar'
            ]);
            $sup = Suprimento::find($id);
            $sup->update(['empenho_id' => $ep->id]);
            return redirect()->route('suprimento');
          }
        }else{
          $ep = Empenho::create($dados);
          Log::create([
            'tabela' => 'anexos_despesa',
            'idLinha' => $ep->id,
            'user' => Auth::user()->name,
            'acao' => 'salvar'
          ]);
          $sup = Suprimento::find($id);
          $sup->update(['empenho_id' => $ep->id]);
          return redirect()->route('suprimento');
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      }
    }
  }


}
