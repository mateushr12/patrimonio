<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\OrcamentoInicial;
use App\ERP\OrcamentoSuplementar;
use App\ERP\Acoes;
use App\ERP\Cota;
use App\ERP\Log;
use Auth;


class OrcamentoSuplementarController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $orcamento_suplementar = OrcamentoSuplementar::all();

    $orcamento = OrcamentoInicial::where('ativo', 0)->max('id');
    $acoes = Acoes::where('CodOrcamento', $orcamento)->get();

    return view('erp.orcamentosuplementar.index', compact('orcamento_suplementar', 'acoes'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $dados = $request->all();
    $acao = OrcamentoSuplementar::where('codAcoes', $request->input('codAcoes'))->exists();
    //dd($acao);
    if ($acao){
      return redirect()->back()->with('erro', "Já existe um suplemento para essa ação.");
    }else{
      $criar = OrcamentoSuplementar::create($dados);
      Log::create([
        'tabela' => 'orcamento_suplementar',
        'idLinha' => $criar->id,
        'user' => Auth::user()->name,
        'acao' => 'salvar'
      ]);
      return redirect()->route('orcamentosuplementar.index');
    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $orc_sup_edit = OrcamentoSuplementar::find($id);
    //$orc_inicial = OrcamentoInicial::all();
    $orcamento = OrcamentoInicial::whereRaw('id = (select max(id) from orcamento_inicial where ativo = 0)')->get();
    foreach ($orcamento as $key => $value) {
      $orc_inicial = $value['especificacao'];
    }
    return view('erp.orcamentosuplementar.editar', compact('orc_sup_edit', 'orc_inicial'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $dados = $request->all();
    $findId = OrcamentoSuplementar::find($id);
    $findId->update($dados);
    Log::create([
      'tabela' => 'orcamento_suplementar',
      'idLinha' => $findId->id,
      'user' => Auth::user()->name,
      'acao' => 'atualizar'
    ]);
    return redirect()->route('orcamentosuplementar.index');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $os = OrcamentoSuplementar::find($id);
    if (Cota::where('cotaacao',$os->codAcoes)->exists()){
      return redirect()->back()->with('erro', "Erro, existe uma cota para esse suplemento.");
    }else{
      //dd('Pode deletar');
      $os->delete();
      Log::create([
        'tabela' => 'orcamento_suplementar',
        'idLinha' => $os->id,
        'user' => Auth::user()->name,
        'acao' => 'deletar'
      ]);
      return redirect()->back();
    }
  }
}
