<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\Taxa;
use App\ERP\Empenho;
use App\ERP\Pagamento;
use App\ERP\Fornecedores;
use App\ERP\TaxaBanese;
use App\ERP\OrcamentoInicial;
use App\ERP\Log;
use Auth;


class GraficosController extends Controller
{

  public function tipoevento(){
    return view('erp.graficos.taxaevento.index');
  }

  public function empenhobuscar()
  {
    $orc = OrcamentoInicial::all();
    //dd($orc);
    return view('erp.graficos.empenhos.index', compact('orc'));
  }

  public function graficoempenho(Request $req)
  {
    $id = $req->input('or');
    $esp = OrcamentoInicial::find($id)->especificacao;

    $empenho = Empenho::leftjoin('acoes', function ($join) {
        $join->on('acoes.id', '=', 'empenhos.acaoempenho')->whereNull('empenhos.deleted_at');
    })->leftjoin('orcamento_inicial', function ($join) {
        $join->on('orcamento_inicial.id', '=', 'acoes.CodOrcamento');
    })
    ->where('orcamento_inicial.id', $id)
    ->groupBy(\DB::raw('date_format(dataempenho, "%m")'))
    ->selectRaw('date_format(dataempenho, "%m") as data, sum(valorempenho) as soma ')
    ->orderBy('data','asc')
    ->get();

    $pagamentos = Pagamento::selectRaw('date_format(datapag, "%m") as data, sum(valor_bruto) as soma ')
    ->leftjoin('empenhos', function ($join) {
        $join->on('pagamentos.id_empenho', '=', 'empenhos.id')->whereNull('empenhos.deleted_at');
    })->leftjoin('acoes', function ($join) {
        $join->on('acoes.id', '=', 'empenhos.acaoempenho');
    })->leftjoin('orcamento_inicial', function ($join) {
        $join->on('orcamento_inicial.id', '=', 'acoes.CodOrcamento');
    })
    ->where('orcamento_inicial.id', $id)
    ->groupBy(\DB::raw('date_format(datapag, "%m")'))
    ->orderBy('data','asc')
    ->get();

    //dd($pagamentos);
    if ( count($empenho) < 1 ){
      $dataEmp[] = null;
      $somaEmp[] = null;
    }

    if ( count($pagamentos) < 1 ){    
      $dataPg[] = null;
      $somaPg[] = null;
    }

    foreach ($empenho as $key => $value) {
      switch ($value['data']) {
        case '01':
          $dataEmp[] = 'JAN'; break;
        case '02':
          $dataEmp[] = 'FEV'; break;
        case '03':
          $dataEmp[] = 'MAR'; break;
        case '04':
          $dataEmp[] = 'ABR'; break;
        case '05':
          $dataEmp[] = 'MAI'; break;
        case '06':
          $dataEmp[] = 'JUN'; break;
        case '07':
          $dataEmp[] = 'JUL'; break;
        case '08':
          $dataEmp[] = 'AGO'; break;
        case '09':
          $dataEmp[] = 'SET'; break;
        case '10':
          $dataEmp[] = 'OUT'; break;
        case '11':
          $dataEmp[] = 'NOV'; break;
        case '12':
          $dataEmp[] = 'DEZ'; break;
      }
      $somaEmp[] = $value['soma'];
    }

    foreach ($pagamentos as $key => $value) {
      switch ($value['data']) {
        case '01':
          $dataPg[] = 'JAN'; break;
        case '02':
          $dataPg[] = 'FEV'; break;
        case '03':
          $dataPg[] = 'MAR'; break;
        case '04':
          $dataPg[] = 'ABR'; break;
        case '05':
          $dataPg[] = 'MAI'; break;
        case '06':
          $dataPg[] = 'JUN'; break;
        case '07':
          $dataPg[] = 'JUL'; break;
        case '08':
          $dataPg[] = 'AGO'; break;
        case '09':
          $dataPg[] = 'SET'; break;
        case '10':
          $dataPg[] = 'OUT'; break;
        case '11':
          $dataPg[] = 'NOV'; break;
        case '12':
          $dataPg[] = 'DEZ'; break;
      }
      $somaPg[] = $value['soma'];
    }

    // dd( $dataEmp, $somaEmp, $dataPg, $somaPg, $esp );
    return view('erp.graficos.empenhos.resultado', compact('dataEmp','somaEmp','dataPg','somaPg','esp'));
    //dd($empenho);
  }

  ////////////////////////////GRAFICO DE EMPENHOS ANOS DIFERENTES///////
  public function graficoempenhoAll()
  {

    $empenho = Empenho::selectRaw('date_format(dataempenho, "%m") as data,
    sum( case when date_format(dataempenho, "%Y") = "2019" then valorempenho else 0 end) as "2019",
    sum( case when date_format(dataempenho, "%Y") = "2020" then valorempenho else 0 end) as "2020",
    sum( case when date_format(dataempenho, "%Y") = "2021" then valorempenho else 0 end) as "2021",
    sum( case when date_format(dataempenho, "%Y") = "2022" then valorempenho else 0 end) as "2022",
    sum( case when date_format(dataempenho, "%Y") = "2023" then valorempenho else 0 end) as "2023" ')
    ->groupBy(\DB::raw('date_format(dataempenho, "%m")'))
    ->orderBy('data','asc')
    ->get();

    // $pagamentos = Pagamento::selectRaw('date_format(datapag, "%m") as data, sum(valor_bruto) as soma ')
    // ->leftjoin('empenhos', function ($join) {
    //     $join->on('pagamentos.id_empenho', '=', 'empenhos.id')->whereNull('empenhos.deleted_at');
    // })->leftjoin('acoes', function ($join) {
    //     $join->on('acoes.id', '=', 'empenhos.acaoempenho');
    // })->leftjoin('orcamento_inicial', function ($join) {
    //     $join->on('orcamento_inicial.id', '=', 'acoes.CodOrcamento');
    // })
    // ->where('orcamento_inicial.id', $id)
    // ->groupBy(\DB::raw('date_format(datapag, "%m")'))
    // ->orderBy('data','asc')
    // ->get();

    $pagamentos = Pagamento::selectRaw('date_format(datapag, "%m") as data,
    sum( case when date_format(datapag, "%Y") = "2019" then valor_bruto else 0 end) as "2019",
    sum( case when date_format(datapag, "%Y") = "2020" then valor_bruto else 0 end) as "2020",
    sum( case when date_format(datapag, "%Y") = "2021" then valor_bruto else 0 end) as "2021",
    sum( case when date_format(datapag, "%Y") = "2022" then valor_bruto else 0 end) as "2022",
    sum( case when date_format(datapag, "%Y") = "2023" then valor_bruto else 0 end) as "2023" ')
    ->groupBy(\DB::raw('date_format(datapag, "%m")'))
    ->orderBy('data','asc')
    ->get();

    if ( count($empenho) < 1 ){
      $somaEmp[] = null;
      $somaPg[] = null;
    }

    foreach ($empenho as $key => $value) {
      $somaEmp[0][] = $value['2019'];
      $somaEmp[1][] = $value['2020'];
      $somaEmp[2][] = $value['2021'];
      $somaEmp[3][] = $value['2022'];
      $somaEmp[4][] = $value['2023'];
    }

    foreach ($pagamentos as $key => $value) {
      $somaPg[0][] = $value['2019'];
      $somaPg[1][] = $value['2020'];
      $somaPg[2][] = $value['2021'];
      $somaPg[3][] = $value['2022'];
      $somaPg[4][] = $value['2023'];
    }

    //dd($somaEmp);

    return view('erp.graficos.empenhos.all', compact('somaEmp','somaPg'));
  }

  /////////////////////////////////////////////////////////////////////

  public function taxasmes()
  {
    $ano = TaxaBanese::selectRaw(' YEAR(data_pagamento) as ano')
    ->whereNotNull(\DB::raw('YEAR(data_pagamento)'))
    ->groupBy(\DB::raw('YEAR(data_pagamento)'))
    ->orderBy('ano')
    ->get();
    //dd($ano);
    return view('erp.graficos.taxa.index', compact('ano'));
  }

  public function graficotaxasmes(Request $req)
  {
    $ano = $req->input('ano');
    $taxa = TaxaBanese::selectRaw('date_format(data_pagamento, "%m") as data, sum(valor) as soma ')
    ->where(\DB::raw('YEAR(data_pagamento)'),$ano)
    ->groupBy(\DB::raw('date_format(data_pagamento, "%m")'))
    ->orderBy('data','asc')
    ->get();

    //dd($taxa);

    $serasa = TaxaBanese::selectRaw('date_format(data_pagamento, "%m") as data, sum(valor) as soma ')
    ->where('tipo','0000')
    ->where(\DB::raw('YEAR(data_pagamento)'),$ano)
    ->groupBy(\DB::raw('date_format(data_pagamento, "%m")'))
    ->orderBy('data','asc')
    ->get();

    $bv = TaxaBanese::selectRaw('date_format(data_pagamento, "%m") as data, sum(valor) as soma ')
    ->where('tipo','0001')
    ->where(\DB::raw('YEAR(data_pagamento)'),$ano)
    ->groupBy(\DB::raw('date_format(data_pagamento, "%m")'))
    ->orderBy('data','asc')
    ->get();

    foreach ($taxa as $key => $value) {
      switch ($value['data']) {
        case '01':
          $data[] = 'JAN'; break;
        case '02':
          $data[] = 'FEV'; break;
        case '03':
          $data[] = 'MAR'; break;
        case '04':
          $data[] = 'ABR'; break;
        case '05':
          $data[] = 'MAI'; break;
        case '06':
          $data[] = 'JUN'; break;
        case '07':
          $data[] = 'JUL'; break;
        case '08':
          $data[] = 'AGO'; break;
        case '09':
          $data[] = 'SET'; break;
        case '10':
          $data[] = 'OUT'; break;
        case '11':
          $data[] = 'NOV'; break;
        case '12':
          $data[] = 'DEZ'; break;
      }
      $soma[] = $value['soma'];
    }

    foreach ($serasa as $key => $value) {
      switch ($value['data']) {
        case '01':
          $dataS[] = 'JAN'; break;
        case '02':
          $dataS[] = 'FEV'; break;
        case '03':
          $dataS[] = 'MAR'; break;
        case '04':
          $dataS[] = 'ABR'; break;
        case '05':
          $dataS[] = 'MAI'; break;
        case '06':
          $dataS[] = 'JUN'; break;
        case '07':
          $dataS[] = 'JUL'; break;
        case '08':
          $dataS[] = 'AGO'; break;
        case '09':
          $dataS[] = 'SET'; break;
        case '10':
          $dataS[] = 'OUT'; break;
        case '11':
          $dataS[] = 'NOV'; break;
        case '12':
          $dataS[] = 'DEZ'; break;
      }
      $somaS[] = $value['soma'];
    }

    foreach ($bv as $key => $value) {
      switch ($value['data']) {
        case '01':
          $dataB[] = 'JAN'; break;
        case '02':
          $dataB[] = 'FEV'; break;
        case '03':
          $dataB[] = 'MAR'; break;
        case '04':
          $dataB[] = 'ABR'; break;
        case '05':
          $dataB[] = 'MAI'; break;
        case '06':
          $dataB[] = 'JUN'; break;
        case '07':
          $dataB[] = 'JUL'; break;
        case '08':
          $dataB[] = 'AGO'; break;
        case '09':
          $dataB[] = 'SET'; break;
        case '10':
          $dataB[] = 'OUT'; break;
        case '11':
          $dataB[] = 'NOV'; break;
        case '12':
          $dataB[] = 'DEZ'; break;
      }
      $somaB[] = $value['soma'];
    }

    if ( count($taxa) < 1 ){
      $soma[] = null;
      $data[] = null;
    }
    if ( count($serasa) < 1 ){
      $somaS[] = null;
      $dataS[] = null;
    }
    if ( count($bv) < 1 ){
      $somaB[] = null;
      $dataB[] = null;
    }

    $total = array_sum($soma);
    $totalS = array_sum($somaS);
    $totalB = array_sum($somaB);

    return view('erp.graficos.taxa.resultado',compact('data','soma','dataS','somaS','dataB','somaB', 'total', 'totalS', 'totalB', 'ano'));


  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public function graficotipoevento(Request $req){

    $a = Taxa::where(function($query) use($req) {
      if ($req->filled(['dt_paga_in']) and $req->filled(['dt_paga_fl'])) {
        $query->whereBetween('dt_pagamento', [ $req->input('dt_paga_in'), $req->input('dt_paga_fl') ]);
      }
      if ($req->filled(['dt_emissao_in']) and $req->filled(['dt_emissao_fl'])) {
        $query->whereBetween('dt_emissao', [ $req->input('dt_emissao_in'), $req->input('dt_emissao_fl') ]);
      }
    });

    $buscas = $a->select('nosso_numero','tipo_evento_redesim','vl_recebido','evento_redesim')
          ->distinct()
          ->orderBy('nosso_numero', 'asc')
          ->orderBy('tipo_evento_redesim')
          //->limit(400)
          ->get();

    if(count($buscas) > 0){
      for ($i=0; $i < count($buscas); $i++) {
        if ( isset($buscas[$i+1]['nosso_numero']) and $buscas[$i]['nosso_numero'] <> $buscas[$i+1]['nosso_numero']) {
          if ($buscas[$i]['tipo_evento_redesim'] == 'ALTERAÇÃO') {
            $busca[] = [
              'nosso_numero' => $buscas[$i]['nosso_numero'],
              'tipo_evento_redesim' => 'ALTERAÇÃO/CONSOLIDAÇÃO/PROCURAÇÃO',
              'vl_recebido' => $buscas[$i]['vl_recebido']
            ];
          }elseif ($buscas[$i]['tipo_evento_redesim'] == 'USO DA JUNTA COMERCIAL' or $buscas[$i]['tipo_evento_redesim'] == 'USO EXCLUSIVO DAS JUNTAS COMERCIAIS') {
            $busca[] = [
              'nosso_numero' => $buscas[$i]['nosso_numero'],
              'tipo_evento_redesim' => $buscas[$i]['tipo_evento_redesim'],
              'vl_recebido' => $buscas[$i]['vl_recebido'],
              'evento_redesim' => $buscas[$i]['evento_redesim']
            ];
          }else{
            $busca[] = [
              'nosso_numero' => $buscas[$i]['nosso_numero'],
              'tipo_evento_redesim' => $buscas[$i]['tipo_evento_redesim'],
              'vl_recebido' => $buscas[$i]['vl_recebido']
            ];
          }
        }else{
          next($buscas);
        }
      }

    foreach ($busca as $key => $val) {
      if ($val['tipo_evento_redesim'] == 'USO DA JUNTA COMERCIAL' or $val['tipo_evento_redesim'] == 'USO EXCLUSIVO DAS JUNTAS COMERCIAIS'){
        $naoR[$val['evento_redesim']][] = $val['vl_recebido'];
      }
    }

    foreach ($naoR as $key => $value) {
      $nr[] = array($key, count($value));
      //$nrtipo[] = $key;
      $nrsoma[] = array_sum($value);
      //$nrqtd[] = count($value);
    }
    /////
    uasort($nr, function ($a, $b) {
	     return $a[1] < $b[1];
    });
    ////
    //dd($nr);

    foreach ($busca as $ky => $vl) {
      if ($vl['tipo_evento_redesim'] == 'CERTIDÕES DAS JUNTAS COMERCIAIS'){
        $vlr['CERTIDÕES DAS JUNTAS COMERCIAIS'][] = $vl['vl_recebido'];
      }
      elseif ($vl['tipo_evento_redesim'] == 'ALTERAÇÃO/CONSOLIDAÇÃO/PROCURAÇÃO') {
        $vlr['ALTERAÇÃO*'][] = $vl['vl_recebido'];
      }
      elseif ($vl['tipo_evento_redesim'] == 'PEDIDO DE BAIXA') {
        $vlr['PEDIDO DE BAIXA'][] = $vl['vl_recebido'];
      }
      elseif ($vl['tipo_evento_redesim'] == 'INSCRIÇÃO DE EMPRESA') {
        $vlr['INSCRIÇÃO DE EMPRESA'][] = $vl['vl_recebido'];
      }
      elseif ($vl['tipo_evento_redesim'] == 'USO DA JUNTA COMERCIAL' or $vl['tipo_evento_redesim'] == 'USO EXCLUSIVO DAS JUNTAS COMERCIAIS') {
        $vlr['EVENTOS NÃO REDESIM'][] = $vl['vl_recebido'];
      }
      elseif ($vl['tipo_evento_redesim'] == 'LIVRO') {
        $vlr['LIVRO'][] = $vl['vl_recebido'];
      }
      elseif ($vl['tipo_evento_redesim'] == 'SITUAÇÕES ESPECIAIS') {
        $vlr['SITUAÇÕES ESPECIAIS'][] = $vl['vl_recebido'];
      }
      else
      {
        $vlr['OUTROS EVENTOS NÃO CADASTRADOS'][] = $vl['vl_recebido'];
      }
    }

    foreach ($vlr as $key => $value) {
      $taxa[] = array($key, count($value));
      $tipo[] = $key;
      //$qtd[] = count($value);
      $soma[] = array_sum($value);
    }
    //dd($taxa);
    // uasort($taxa, function ($a, $b) {
	  //    return $a[1] < $b[1];
    // });

  }else{
    $taxa[] = null;
    $tipo[] = null;
    $qtd[] = null;
    $soma[] = null;
    $nr[]= null;
    $nrsoma[] = null;
  }

    if ($req->filled(['dt_paga_in'])){
        $data =  \Carbon\Carbon::parse($req->input('dt_paga_in'))->format('d/m/Y').' até '.\Carbon\Carbon::parse($req->input('dt_paga_fl'))->format('d/m/Y').' (data de pagamento)';
    }elseif ($req->filled(['dt_emissao_in'])){
        $data =  \Carbon\Carbon::parse($req->input('dt_emissao_in'))->format('d/m/Y').' até '.\Carbon\Carbon::parse($req->input('dt_emissao_fl'))->format('d/m/Y').' (data de emissão)';
    }else{
        $data = 'Sem data específica';
    }

    return view('erp.graficos.taxaevento.resultado',compact('tipo', 'soma', 'taxa', 'data', 'nr', 'nrsoma'));
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////

  public function pagamentoForn()
  {
    $fornecedores = Fornecedores::where('status', 1)->get();
    return view('erp.graficos.pagamentoforn.index', compact('fornecedores'));
  }

  public function graficopagamentoForn(Request $req)
  {

    if ($req->input('fornecedor') == 'credor') {
      $nome = '(CREDOR) JUCESE';
      $a = Pagamento::leftjoin('empenhos', function ($join) {
          $join->on('pagamentos.id_empenho', '=', 'empenhos.id')
          ->whereNull('empenhos.deleted_at');
      })
      ->whereNull('empenhos.licempenho')
      ->groupBy(\DB::raw('date_format(pagamentos.datapag, "%m/%y")'))
      ->selectRaw('date_format(pagamentos.datapag, "%m/%y") as datapag, sum(pagamentos.valor_bruto) as valor_bruto ')
      ->orderBy('datapag')
      ->get();
    }else {
      $nome = Fornecedores::find($req->input('fornecedor'))->nome;
      $a = Pagamento::leftjoin('empenhos', function ($join) {
          $join->on('pagamentos.id_empenho', '=', 'empenhos.id')->whereNull('empenhos.deleted_at');
      })->leftjoin('contratos', function ($join) {
          $join->on('empenhos.licempenho', '=', 'contratos.id');
      })->leftjoin('fornecedores', function ($join) {
          $join->on('contratos.fornecedorcontratado', '=', 'fornecedores.id');
      })->where('fornecedorcontratado', '=', $req->input('fornecedor'))
      ->groupBy(\DB::raw('date_format(pagamentos.datapag, "%m/%y")'))
      ->selectRaw('date_format(pagamentos.datapag, "%m/%y") as datapag, sum(valor_bruto) as valor_bruto' )
      ->orderByRaw('date_format(pagamentos.datapag, "%m/%y")')
      ->get();
    }

    //dd($a);
    if (count($a) < 1) {
      $valores[] = null;
      $data[] = null;
    }

    foreach ($a as $key => $value) {
      $data[] = $value['datapag'];
      $valores[] = $value['valor_bruto'];
    }

    $soma = array_sum($valores);

    //dd($data,$valores);

    return view('erp.graficos.pagamentoforn.resultado',compact('data','valores', 'nome', 'soma'));

  }


}
