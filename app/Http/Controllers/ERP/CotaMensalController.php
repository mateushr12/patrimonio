<?php

namespace App\Http\Controllers\ERP;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\ERP\Cota;
use App\ERP\Acoes;
use App\ERP\Log;
use App\ERP\Empenho;
use Response;
use Auth;
///////////////////////////////////////////////////NÃO UTILIZADO////////////////////////////////////////////////////////
class CotaMensalController extends Controller
{
  public function index()
  {
    $cotaacoes = Acoes::all();
    $cotas = Cota::all();

    return view('erp.cota.index' , compact('cotaacoes','cotas'));
  }

  public function salvar(Request $req)
  {
    $idAcao = $req->input('cotaacao');
    $mes = $req->input('mesrefcota');
    $valorcota = $req->input('valorcota');

    if (Cota::where('cotaacao', $req->input('cotaacao'))->where('mesrefcota', $req->input('mesrefcota'))->exists())
    {
      return redirect()->back()->with('erro', 'Já exite uma cota para esta ação e mês.');
    }else{
      $a = Acoes::find($idAcao);
      if ($valorcota > $a->ValorDestinado){
        return redirect()->back()->with('erro', 'Valor da cota deve ser igual ou inferior ao valor da ação.');
      }else{
        $dados = $req->all();
        Cota::create($dados);
        return redirect()->route('cota.index');
      }
    }
  }

  public function editar($id)
  {
    $cotaacoes = Acoes::all();
    $registro = Cota::find($id);
    return view('erp.cota.editar' , compact('registro','cotaacoes'));
  }

  public function atualizar(Request $req, $id)
  {
    $registro = Cota::find($id);
    $findId = Acoes::find($registro->cotaacao);
    $valorcota = $req->input('valorcota');

    if ($valorcota > $findId->ValorDestinado){
      return redirect()->back()->with('erro', "Valor da cota deve ser igual ou inferior ao valor da ação (R$ $findId->ValorDestinado).");
    }else{
      $dados = $req->all();
      $registro->update($dados);
      return redirect()->route('cota.index');
      Log::create([
        'tabela' => 'cotas',
        'idLinha' => $registro->id,
        'user' => Auth::user()->id,
        'acao' => 'atualizar'
      ]);
    }
  }

  public function deletar($id)
  {

    $ac = Cota::find($id);
    if (Empenho::where('acaoempenho',$ac->cotaacao)->exists()) {
        //dd('Não é possivel deletar essa cota pôs há um empenho cadastrado.');
        return redirect()->back()->with('erro','Não é possivel deletar essa cota pôs há um empenho cadastrado.');
    }
    $ac->delete();
    Log::create([
      'tabela' => 'cotas',
      'idLinha' => $ac->id,
      'user' => Auth::user()->id,
      'acao' => 'deletar'
    ]);

    return redirect()->route('cota.index');
  }

  public function valor($id)
  {
    $acao = Acoes::where('id', $id)->get();
    foreach ($acao as $key => $value) {
      $valorD = $value['ValorDestinado'];
    }
    return Response::json($valorD);
  }


}
