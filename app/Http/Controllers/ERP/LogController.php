<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ERP\Log;
use App\ERP\Empenho;
use App\ERP\Fornecedores;

use DB;

class LogController extends Controller
{
  //
  public function index()
  {
    $tabela = Log::select('tabela')->groupBy('tabela')->get();
    $user = Log::select('user')->groupBy('user')->get();
    return view('erp.log.index', compact('tabela','user'));
  }

  public function resultado(Request $req)
  {
    $a = Log::where(function($query) use($req) {
      if ($req->filled(['tabela'])) {
        $query->where('tabela', $req->input('tabela'));
      }
      if ($req->filled(['user'])) {
        $query->where('user', $req->input('user'));
      }
      if ($req->filled(['data'])) {
        $dt = $req->input('data');
        $query->whereRaw("date_format(data, '%Y-%m-%d') = '$dt' ");
      }
      if ($req->filled(['acao'])) {
        $query->where('acao', $req->input('acao'));
      }
    })->orderBy('tabela');

    $log = $a->get();

    return view('erp.log.resultado', compact('log'));

  }

  public function tabelas(Request $req)
  {
    $tb = $req->input('table');
    $id = $req->input('id');
    $tabela = DB::table($tb)->find($id);

    return view('erp.log.tabela', compact('tabela','tb'));
  }



}
