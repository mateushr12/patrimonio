<?php

namespace App\Http\Controllers\ERP;

use App\ERP\Acoes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\Fonte;
use App\ERP\Grupo;
use App\ERP\Cota;
use App\ERP\Elemento;
use App\ERP\OrcamentoInicial;
use App\ERP\OrcamentoSuplementar;
use App\ERP\Log;
use Auth;

use App\Permissao;
use App\Gruposuser;
use App\Permissaogrupo;

class AcoesController extends Controller
{
    public function index()
    {
        $orcamento = OrcamentoInicial::whereRaw('id = (select max(id) from orcamento_inicial where ativo = 0)')->get();
        $especificacao = null;
        foreach ($orcamento as $key => $value) {
            $especificacao = $value['especificacao'];
        }

        $acoessalvas = Acoes::leftJoin('orcamento_suplementar', function ($join) {
            $join->on('acoes.id', '=', 'orcamento_suplementar.codAcoes');
          })->leftJoin('empenhos', function ($join) {
                $join->on('acoes.id', '=', 'empenhos.acaoempenho')->whereNull('empenhos.deleted_at');
                })
            ->selectRaw('sum(empenhos.valorempenho) as empenhado, acoes.*, orcamento_suplementar.valor')
            ->groupBy('acoes.id', 'orcamento_suplementar.valor')
            ->get();
        //dd($acoessalvas);
        // foreach ($acoessalvas as $key => $value) {
        //     $t[] = $value['empenhado'];
        // }
        // dd(array_sum($t));

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // $grupo = Auth::user()->grupoid;
        // $permGrupo = Permissao::join('permissao_grupo', function ($join) {
        //     $join->on('permissoes.id', '=', 'permissao_grupo.permissoes_id');
        //     })
        //     ->where('permissao_grupo.grupouser_id', '=', $grupo)
        //     ->select('permissoes.nome')->get();
        //
        // foreach ($permGrupo as $key => $value) {
        //   $nomes[] = $value['nome'];
        // }
        // $planejamento = array('erp/acoes','erp/orcamentoinicial','erp/orcamentosuplementar','erp/remanejamento');
        // $execucao = array('erp/empenho','erp/empenho/showcotas','erp/suprimentos');
        // $gestao = array('erp/contratos','erp/fornecedor');
        // $contasareceber = array('erp/taxasRetorno','erp/taxasRetorno/exibir','erp/taxas','erp/taxas/serasa');
        // $contaspagar = array('porgrupo', 'porelemento');
        // $relatorios = array('erp/relatorio/acao','erp/relatorio/contrato','erp/relatorio/pagamento');
        // $outroscadastros = array('erp/cadastro/elemento', '/erp/cadastro/modalidadelic', 'erp/cadastro/reflegal');
        //
        // $planejamento = count(array_intersect($nomes,$planejamento));
        // $execucao = count(array_intersect($nomes,$execucao));
        // $gestao = count(array_intersect($nomes,$gestao));
        // $contasareceber = count(array_intersect($nomes,$contasareceber));
        // $contaspagar = count(array_intersect($nomes,$contaspagar));
        // $relatorios = count(array_intersect($nomes,$relatorios));
        // $outroscadastros = count(array_intersect($nomes,$outroscadastros));
        //
        // \Session::put('permissoes', [
        //   'nomes' => $nomes,
        //   'plan' => $planejamento,
        //   'gestao' => $gestao,
        //   'exe' => $execucao,
        //   'contas' => $contasareceber,
        //   'contasp' => $contaspagar,
        //   'relatorio' => $relatorios,
        //   'outroscad' => $outroscadastros
        // ]);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $fontes = Fonte::all();
        $grupos = Grupo::all();
        $elementos = Elemento::all();

        return view('erp.acoes.index', compact('fontes', 'grupos', 'elementos', 'acoessalvas', 'especificacao'));
    }


    public function salvar(Request $req)
    {

      //$maxOrc = OrcamentoInicial::where('ativo', '0')->max('id');
      $orcamento = OrcamentoInicial::whereRaw('id = (select max(id) from orcamento_inicial where ativo = 0)')->get();
      foreach ($orcamento as $key => $value) {
        $id = $value['id'];
        $receita = $value['receita_orcada'];
      }

      $somaOrc = 0;
      $somaAc = Acoes::where('codOrcamento', $id)->sum('ValorDestinado');

      $vlD = str_replace(',', '.', str_replace('.', '', $req->input('ValorDestinado')));

      if ($vlD > ($receita+$somaOrc)){
        return redirect()->back()->with('erro', 'Valor da ação maior que o valor do orçamento + orçamento suplementar.');
      }

      if (($vlD + $somaAc) > ($receita+$somaOrc)){
        return redirect()->back()->with('erro', 'Soma das ações ultrapassa o valor do orçamento + orçamento suplementar.');
      }

      $req->request->add([
        'UniGestora' => 192011,
        'UniEspecificacao' => 'JUCESE',
        'CodOrcamento' => $id,
        'ValorDestinado' => $vlD,
        'ValorInicial' => $vlD,
        'CodFonte' => 33
      ]);

      $dados = $req->all();
      $criar = Acoes::create($dados);
      Log::create([
        'tabela' => 'acoes',
        'idLinha' => $criar->id,
        'user' => Auth::user()->name,
        'acao' => 'salvar'
      ]);
      return redirect()->route('erp.acoes');
    }


    public function editar($id)
    {

        $acoessalvas = Acoes::all();
        $orcamento = OrcamentoInicial::whereRaw('id = (select max(id) from orcamento_inicial where ativo = 0)')->get();
        foreach ($orcamento as $key => $value) {
            $especificacao = $value['especificacao'];
        }
        $registro = Acoes::find($id);
        $fontes = Fonte::all();
        $grupos = Grupo::all();
        $elementos = Elemento::all();
        return view('erp.acoes.editar', compact('fontes', 'grupos', 'elementos', 'registro', 'acoessalvas', 'especificacao'));
    }

    public function atualizar(Request $req, $id)
    {
        $dados = $req->all();
        $findId = Acoes::find($id);
        $findId->update($dados);
        Log::create([
          'tabela' => 'acoes',
          'idLinha' => $findId->id,
          'user' => Auth::user()->name,
          'acao' => 'atualizar'
        ]);
        return redirect()->route('erp.acoes');
    }

    public function deletar($id)
    {

        $ac = Acoes::find($id);
        if (isset($ac)){
            // if (Cota::where('cotaacao',$id)->exists()) {
            //     //dd('Não é possivel deletar essa ação pôs há uma cota mensal cadastrada.');
            //     return redirect()->back()->with('erro','Não é possivel deletar essa ação pôs há uma cota mensal cadastrada.');
            // }
            $ac->delete();
            OrcamentoSuplementar::where('codAcoes', $id)->delete();
            Log::create([
              'tabela' => 'acoes',
              'idLinha' => $ac->id,
              'user' => Auth::user()->name,
              'acao' => 'deletar'
            ]);
        }
        return redirect()->route('erp.acoes');
    }

    public function inicioRelatorio()
    {
      $orcamento = OrcamentoInicial::all();
      return view('erp.acoes.relatorio.buscar', compact('orcamento'));
    }

    public function resultRelatorio(Request $req)
    {
      $data = date('d/m/Y');
      $a = Acoes::where(function($query) use($req) {
        if ($req->filled(['CodAcao'])) {
          $query->where('CodAcao', $req->input('CodAcao'));
        }
        if ($req->filled(['CodGrupAcao'])) {
          $query->where('CodGrupAcao', $req->input('CodGrupAcao'));
        }
      });

      $acoes = $a->leftJoin('orcamento_suplementar', function ($join) {
          $join->on('acoes.id', '=', 'orcamento_suplementar.codAcoes');
        })->leftJoin('empenhos', function ($join) {
              $join->on('acoes.id', '=', 'empenhos.acaoempenho')->whereNull('empenhos.deleted_at');
              })
          ->selectRaw('sum(empenhos.valorempenho) as empenhado, acoes.*, orcamento_suplementar.valor')
          ->groupBy('acoes.id', 'orcamento_suplementar.valor')
          ->get();

      $t = [];
      $disp = [];
      $v = [];
      $vl = [];

      foreach ($acoes as $key => $value) {
        $t[] = $value['empenhado'];
        $disp[] = ($value['ValorDestinado'] + $value['valor']) - $value['empenhado'];
        $v[] = $value['valor'];
        $vl[] = $value['ValorDestinado'];
      }

      $somaEmp = array_sum($t);
      $somaDisp = array_sum($disp);
      $somaVl = array_sum($v);
      $somaVlD = array_sum($vl);

      return view('erp.acoes.relatorio.imprimir', compact('acoes','data','somaEmp','somaDisp','somaVl','somaVlD'));

    }

    public function empenhos($id)
    {
      $empenhos = Acoes::join('empenhos', function ($join){
        $join->on('acoes.id', '=', 'empenhos.acaoempenho')->whereNull('empenhos.deleted_at');
      })
      ->where('acoes.id',$id)
      ->select('empenhos.numero','empenhos.valorempenho','acoes.DescAcao','acoes.CodAcao')
      ->orderBy('empenhos.numero')
      ->get();

      //dd($empenhos);
      return view('erp.acoes.empenhos', compact('empenhos'));
    }

}
