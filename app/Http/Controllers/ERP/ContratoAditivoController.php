<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\Log;
use App\ERP\AnexosAditivo;
use App\ERP\Aditivos;
use App\ERP\Contrato;
use Auth;
use Illuminate\Support\Facades\Storage;


class ContratoAditivoController extends Controller
{

    public function index($id)
    {
        $aditivos = Aditivos::where('id_contrato', $id)->get();
        $contrato = $id;
        $con = Contrato::where('id', $id)->get();
        foreach ($con as $key => $value) {
          $n_contrato = $value['numerocontrato'];
        }
        return view('erp.contratos.aditivo.index' , compact('aditivos', 'contrato', 'n_contrato'));
    }

    public function salvar(Request $req)
    {
        if ($req->input('natureza') == 5 or $req->input('natureza') == 6 or $req->input('natureza') == 7){
            $req->merge([ 'valor' => $req->input('valor') * -1]);
        }
        $dados = $req->all();
        $contrato = Aditivos::create($dados);
        Log::create([
          'tabela' => 'aditivos',
          'idLinha' => $contrato->id,
          'user' => Auth::user()->name,
          'acao' => 'salvar'
        ]);
        return redirect()->back();
    }

    public function deletar($id)
    {
      $ad = Aditivos::find($id);
      $aa = AnexosAditivo::where('id_aditivo', $id)->exists();
      //dd($aa);
      if($aa){
        return redirect()->back()->with('erro', "Esse aditivo não pode ser deletado pôs há um anexo cadastrado para ele.");
      }else{
        $ad->delete();
        Log::create([
          'tabela' => 'aditivos',
          'idLinha' => $ad->id,
          'user' => Auth::user()->name,
          'acao' => 'deletar'
        ]);
        return redirect()->back();
      }

    }

    public function anexar($id)
    {
      $arquivo = AnexosAditivo::where('id_aditivo', $id)->get();
      $aditivo = $id;
      $adt = Aditivos::where('id', $id)->get();
      foreach ($adt as $key => $value) {
        $n_aditivo = $value['n_aditivo'];
      }
      return view('erp.contratos.aditivo.anexar' , compact('arquivo', 'aditivo', 'n_aditivo'));
    }


    public function anexarSalvar(Request $req)
    {
      $nomereq = $req->file('arquivo')->store('contratos/aditivos');
      $contrato = AnexosAditivo::create([
        'id_aditivo' => $req->input('id_aditivo'),
        'arquivo' => $nomereq,
        'tipoarquivo' => $req->input('tipoarquivo')
      ]);
      Log::create([
        'tabela' => 'anexos_aditivos',
        'idLinha' => $contrato->id,
        'user' => Auth::user()->name,
        'acao' => 'salvar'
      ]);
      return redirect()->back();
    }

    public function pdf($id)
    {
      try {

        $arquivo = AnexosAditivo::where('id', $id)->get();
        foreach ($arquivo as $key => $value) {
          $narq = $value['arquivo'];
        }
        return response()->file(storage_path("app/public/$narq"));

      } catch (\Exception $e) {

        return view('notfound');

      }
    }

    public function deletepdf($id)
    {
      $arquivo = AnexosAditivo::where('id', $id)->get();
      foreach ($arquivo as $key => $value) {
        $narq = $value['arquivo'];
      }
      unlink(storage_path("app/public/$narq"));
      AnexosAditivo::find($id)->delete();
      Log::create([
        'tabela' => 'anexos_aditivos',
        'idLinha' => $id,
        'user' => Auth::user()->name,
        'acao' => 'deletar'
      ]);
      return redirect()->back();
    }


}
