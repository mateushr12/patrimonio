<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ERP\Log;
use App\ERP\Caixas;
use App\ERP\CaixaTipos;
use App\ERP\CaixaConteudos;
use App\ERP\CaixaAnexo;
use App\ERP\Empenho;
use App\ERP\Fornecedores;

use Auth;

class CaixasController extends Controller
{
    public function caixas()
    {
      $caixas = Caixas::all();
      $tipo = CaixaTipos::all();
      return view('erp.caixas.index' , compact('caixas', 'tipo'));
    }

    public function novaCaixa(Request $req)
    {
      $dados = $req->all();
      $salvo = Caixas::create($dados);
      Log::create([
        'tabela' => 'caixas',
        'idLinha' => $salvo->id,
        'user' => Auth::user()->name,
        'acao' => 'salvar'
      ]);
      return redirect()->route('caixas');
    }

    public function deletarCaixa($id)
    {
      $tp = Caixas::find($id);
      if (CaixaConteudos::where('caixa_id',$id)->exists()){
        return redirect()->back()->with('erro','Não é possivel deletar, essa caixa não está vazia.');
      }else{
        $tp->delete();
        Log::create([
          'tabela' => 'caixas',
          'idLinha' => $id,
          'user' => Auth::user()->name,
          'acao' => 'deletar'
        ]);
        return redirect()->route('caixas');
      }
    }

    public function geraretiqueta(Request $r)
    {
      $caixas = $r->input('caixa_id');
      if (isset($caixas)){
        foreach ($caixas as $key => $value) {
          $cx[] = Caixas::find($value);
        }
        return view('erp.caixas.geraretiqueta' , compact('cx'));
      }else{
        return redirect()->back();
      }
    }

    public function imprimirConteudo($id)
    {
      $caixa = Caixas::find($id);
      $processos = CaixaConteudos::where('caixa_id',$id)->orderBy('data')->get();
      return view('erp.caixas.imprimir' , compact('caixa','processos'));
    }

    public function processos($id)
    {
      $caixa = Caixas::find($id);
      $processos = CaixaConteudos::where('caixa_id',$id)->orderBy('data')->get();

      return view('erp.caixas.processos' , compact('caixa','processos'));
    }

    public function salvarProcesso(Request $req)
    {
      $dados = $req->all();
      //dd($req->filled(['cpfCnpj']));
      //if (strlen($dados['cpfCnpj']) <> 18 or strlen($dados['cpfCnpj']) <> 14){
      //  return redirect()->back()->with('erro','Dados inválidos no campo CPF/CNPJ.');
      //}

      $salvo = CaixaConteudos::create($dados);
      Log::create([
        'tabela' => 'caixa_conteudos',
        'idLinha' => $salvo->id,
        'user' => Auth::user()->name,
        'acao' => 'salvar'
      ]);
      return redirect()->back();
    }

    public function deletarProcesso($id)
    {
      $tp = CaixaConteudos::find($id);
      $tp->delete();
      Log::create([
        'tabela' => 'caixa_conteudos',
        'idLinha' => $id,
        'user' => Auth::user()->name,
        'acao' => 'deletar'
      ]);
      return redirect()->back();
    }

    public function tipo()
    {
      $tipo = CaixaTipos::all();
      return view('erp.caixas.tipo' , compact('tipo'));
    }

    public function salvarTipo(Request $req)
    {
      $dados = $req->all();
      if ( CaixaTipos::where('descricao',$dados['descricao'])->exists() ){
        return redirect()->back()->with('erro','Esse setor de caixa já existe');
      }else{
        $salvo = CaixaTipos::create($dados);
        return redirect()->route('caixa.tipo');
      }
    }

    public function deletarTipo($id)
    {
      $tp = CaixaTipos::find($id);
      if (Caixas::where('tipo_id',$id)->exists()){
        return redirect()->back()->with('erro','Não é possivel deletar, há uma ou mais caixas cadastradas com esse tipo.');
      }else{
        $tp->delete();
        return redirect()->route('caixa.tipo');
      }
    }

    public function buscarProcesso()
    {
      $buscar = Caixas::leftJoin('caixa_conteudos', function ($join) {
        $join->on('caixas.id', '=', 'caixa_conteudos.caixa_id')->whereNull('caixa_conteudos.deleted_at');
        })
      ->leftJoin('caixa_tipos', function ($join) {
        $join->on('caixas.tipo_id', '=', 'caixa_tipos.id');
        })
      ->whereNull('caixas.deleted_at')
      ->selectRaw('caixa_conteudos.*, caixas.id, caixas.tipo_id')
      ->get();
      //dd($buscar);
      return view('erp.caixas.buscar' , compact('buscar'));
    }

    public function getAutocompleteData(Request $request)
    {
        if($request->has('term')){
            $re = Fornecedores::where('nome','like','%'.$request->input('term').'%')->get();
            return response()->json($re);
        }
    }

    /////////////////////////////////////////---ANEXO----/////////////////////////////////////////////
    public function anexar($id)
    {
      $ids = $id;
      $arquivo = CaixaAnexo::where('id_conteudos', $id)->get();
      $caixaid = CaixaConteudos::find($id)->caixa_id;
      return view('erp.caixas.anexar' , compact('arquivo','ids','caixaid'));
    }

    public function anexarSalvar(Request $req)
    {
      $nomereq = $req->file('arquivo')->store('caixas');
      $supri = CaixaAnexo::create([
        'id_conteudos' => $req->input('id_conteudos'),
        'descricao' => $req->input('descricao'),
        'arquivo' => $nomereq
      ]);
      Log::create([
        'tabela' => 'caixa_anexos',
        'idLinha' => $supri->id,
        'user' => Auth::user()->name,
        'acao' => 'salvar'
      ]);
      return redirect()->back();
    }

    public function arquivo($id)
    {
      try {
        $arquivo = CaixaAnexo::find($id);
        $narq = $arquivo->arquivo;
        return response()->file(storage_path("app/public/$narq"));
      } catch (\Exception $e) {
        return view('notfound');
      }
    }

    public function deleteArquivo($id)
    {
      $arquivo = CaixaAnexo::find($id);
      $narq = $arquivo->arquivo;
      unlink(storage_path("app/public/$narq"));
      CaixaAnexo::find($id)->delete();
      Log::create([
        'tabela' => 'caixa_anexos',
        'idLinha' => $id,
        'user' => Auth::user()->name,
        'acao' => 'deletar'
      ]);
      return redirect()->back();
    }
    //////////////////////////////////////////////////////////////////////////////




}
