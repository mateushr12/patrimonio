<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\OrcamentoInicial;
use App\ERP\OrcamentoSuplementar;
use App\ERP\Ano;
use App\ERP\Acoes;
use App\ERP\Log;
use Auth;

class OrcamentoInicialController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    //$orcamento_resultado = OrcamentoInicial::all();

    $orcamento_resultado = OrcamentoInicial::join('acoes', function ($join) {
      $join->on('acoes.CodOrcamento', '=', 'orcamento_inicial.id');
    })->join('orcamento_suplementar', function ($join) {
      $join->on('acoes.id', '=', 'orcamento_suplementar.codAcoes');
    })
    ->selectRaw('orcamento_inicial.*, (orcamento_inicial.receita_orcada + sum(orcamento_suplementar.valor)) as atual  ')
    ->groupBy('orcamento_inicial.id')
    ->get();

    //dd($orcamento_resultado);

    $anoAtual = (int) (date('Y'));
    $anos = Ano::where('ano', '>=', $anoAtual)->get();

    return view('erp.orcamentoinicial.index', compact('orcamento_resultado', 'anos'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    try
    {
      if ($request->input('receita_orcada') == '0.00'){
        return redirect()->back()->with('erro', 'Valor da receita no orçamento não pode ser 0.');
      }
      $dados = $request->all();
      $criar = OrcamentoInicial::create($dados);
      Log::create([
        'tabela' => 'orcamento_inicial',
        'idLinha' => $criar->id,
        'user' => Auth::user()->name,
        'acao' => 'salvar'
      ]);
      return redirect()->route('orcamentoinicial.index');
    }
    catch(\Illuminate\Database\QueryException $e)
    {
      return redirect()->back()->with('erro', 'Não foi possivel cadastrar o orçamento.');
    }
    catch(PDOException $e)
    {
      return redirect()->back()->with('erro', 'Algo errado aconteceu, tente novamente!');
    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //$anoAtual = (int) (date('Y'));
    //$anos = Ano::where('ano', '!=', $anoAtual)->get();
    $orcamento = OrcamentoInicial::find($id);
    $anos = Ano::all();
    return view('erp.orcamentoinicial.editar', compact('orcamento', 'anos'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $dados = $request->all();
    $somaAcoes = Acoes::where('CodOrcamento', $id)->sum('ValorDestinado');
    $somaOrc   = OrcamentoSuplementar::where('codOrcamento', $id)->sum('valor');
    if ($request->input('receita_orcada') < $somaAcoes-$somaOrc)
    {
      //dd('O valor da receita não pode ser menor que a soma das ações');
      return redirect()->back()->with( 'erro', 'O valor da receita não pode ser menor que a soma das ações');
    }
    $findId = OrcamentoInicial::find($id);
    $findId->update($dados);
    Log::create([
      'tabela' => 'orcamento_inicial',
      'idLinha' => $findId->id,
      'user' => Auth::user()->name,
      'acao' => 'atualizar'
    ]);
    return redirect()->route('orcamentoinicial.index');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }

  public function ver($id)
  {
    $orcamento_resultado = OrcamentoInicial::find($id);
    $totacao = Acoes::where('CodOrcamento' , $orcamento_resultado->id)->sum('ValorDestinado');

    $totalSupl = 0;

    $totalSupl = OrcamentoInicial::join('acoes', function ($join) {
      $join->on('acoes.CodOrcamento', '=', 'orcamento_inicial.id');
    })->join('orcamento_suplementar', function ($join) {
      $join->on('acoes.id', '=', 'orcamento_suplementar.codAcoes');
    })
    ->where('orcamento_inicial.id',$id)
    ->sum('orcamento_suplementar.valor');

    $totalOrc = $totalSupl+$orcamento_resultado->receita_orcada;

    return view('erp.orcamentoinicial.ver', compact('orcamento_resultado', 'totalOrc'));
  }
}
