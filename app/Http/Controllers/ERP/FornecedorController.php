<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\Fornecedores;
use App\ERP\Banco;
use App\ERP\Log;
use Auth;

class FornecedorController extends Controller
{
  public function index(){

      $fornecedores = Fornecedores::all();
      $bancos = Banco::all()->sortBy('CodigoBanco');
      return view('erp.fornecedor.index' , compact('fornecedores','bancos'));
  }

  public function salvar(Request $req)
  {
    try {
      $dados = $req->all();

      $fornecedor = Fornecedores::create($dados);
      Log::create([
        'tabela' => 'fornecedores',
        'idLinha' => $fornecedor->id,
        'user' => Auth::user()->name,
        'acao' => 'salvar'
      ]);
      return redirect()->route('fornecedor.index');

    } catch (\Illuminate\Database\QueryException $e) {

      return redirect()->back()->with('erro', 'Não foi possivel cadastrar o fornecedor.');

    }
  }

  public function editar($id)
  {
      $registro = Fornecedores::where('id', $id)->get();
      $bancos = Banco::all()->sortBy('CodigoBanco');
      foreach ($registro as $key => $value) {
        $tipo = $value['identificacao'];
      }
      if ($tipo == 4)
      {
          $registro = Fornecedores::find($id);
          return view('erp.fornecedor.cpf.editar' , compact('registro','bancos'));
      }else
      {
          $registro = Fornecedores::find($id);
          return view('erp.fornecedor.cnpj.editar' , compact('registro','bancos'));
      }

  }

  public function ver($id){

      $fornecedores = Fornecedores::find($id);
      return view('erp.fornecedor.ver' , compact('fornecedores'));
  }

  public function atualizar(Request $req, $id)
  {
     $dados = $req->all();
     $atualizar = Fornecedores::find($id);
     $atualizar->update($dados);
     Log::create([
       'tabela' => 'fornecedores',
       'idLinha' => $atualizar->id,
       'user' => Auth::user()->name,
       'acao' => 'atualizar'
     ]);

     return redirect()->route('fornecedor.index');
  }

}
