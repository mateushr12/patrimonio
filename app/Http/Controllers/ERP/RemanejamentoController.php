<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\Remanejamento;
use App\ERP\Acoes;
use App\ERP\OrcamentoInicial;
use App\ERP\Log;
use Auth;


class RemanejamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $remanejamento = Remanejamento::all();
        $orcamento = OrcamentoInicial::where('ativo', 0)->max('id');
        $acao = Acoes::where('CodOrcamento', $orcamento)->get();
        return view('erp.remanejamento.index', compact('remanejamento', 'acao'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $acaoDest = Acoes::where('id', $request->input('codAcao_destino'))->get();
        foreach ($acaoDest as $keyD => $valueD) {
            $vlOld = $valueD['ValorDestinado'];
        }

        $acao = Acoes::where('id', $request->input('codAcao_origem'))->get();
        foreach ($acao as $key => $value) {
            $vlDestinado = $value['ValorDestinado'];
        }

        if ( $request->input('codAcao_origem') == $request->input('codAcao_destino')){
            //dd('erro');
            return redirect()->back()->with('erro', 'Ação de origem deve ser diferente da ação de destino.');
        }elseif ($request->input('valor') > $vlDestinado){
            //dd('erro2');
            return redirect()->back()->with('erro', 'Valor é superior ao valor da ação de origem.');
        }
        $dados = $request->all();
        $criar = Remanejamento::create($dados);
        Log::create([
          'tabela' => 'remanejamento',
          'idLinha' => $criar->id,
          'user' => Auth::user()->name,
          'acao' => 'salvar'
        ]);
        Acoes::find($request->input('codAcao_origem'))->update([
            'ValorDestinado' => $vlDestinado - $request->input('valor')
        ]);
        Acoes::find($request->input('codAcao_destino'))->update([
            'ValorDestinado' => $vlOld + $request->input('valor')
        ]);

        return redirect()->route('remanejamento.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $remanejamento = Remanejamento::find($id);
        $acao = Acoes::all();
        return view('erp.remanejamento.editar', compact('remanejamento', 'acao'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dados = $request->all();
        $findId = Remanejamento::find($id);
        $findId->update($dados);
        Log::create([
          'tabela' => 'remanejamento',
          'idLinha' => $findId->id,
          'user' => Auth::user()->name,
          'acao' => 'atualizar'
        ]);
        return redirect()->route('remanejamento.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
