<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\Fornecedores;
use App\ERP\Elemento;
use App\ERP\Natureza;
use App\ERP\ModalidadeLicitacao;
use App\ERP\RefLegal;
use App\ERP\TipoRetencao;
use App\ERP\Retencao;
use App\ERP\Log;
use Auth;

class CadastroController extends Controller
{
  //Elementos
  public function indexEle(){

      $elemento = Elemento::paginate(10);
      return view('erp.cadastro.elementos.listar' , compact('elemento'));
  }

  public function salvarEle(Request $req)
  {
    try {

      $dados = $req->all();
      $elemento = Elemento::create($dados);
      Log::create([
        'tabela' => 'elementos',
        'idLinha' => $elemento->id,
        'user' => Auth::user()->name,
        'acao' => 'salvar'
      ]);
      return redirect()->route('elemento');

    } catch (\Illuminate\Database\QueryException $e) {

      return redirect()->back()->with('erro', 'Não foi possivel cadastrar o elemento.');

    }
  }

  public function deletarEle($id)
  {
    $ac = Elemento::find($id);
    $ac->delete();
    Log::create([
      'tabela' => 'elementos',
      'idLinha' => $ac->id,
      'user' => Auth::user()->name,
      'acao' => 'deletar'
    ]);

    return redirect()->route('elemento');
  }

  //Sub-elemento
  public function indexSubele($id)
  {
      $subelemento = Natureza::where('id_elemento',$id)->get();
      $idele = $id;
      return view('erp.cadastro.elementos.subelementos.inserir' , compact('subelemento','idele'));

  }

  public function salvarSubele(Request $req)
  {
    try {

      $dados = $req->all();
      $subelemento = Natureza::create($dados);
      Log::create([
        'tabela' => 'naturezas',
        'idLinha' => $subelemento->id,
        'user' => Auth::user()->name,
        'acao' => 'salvar'
      ]);
      return redirect()->route('subelemento', $subelemento->id_elemento);

    } catch (\Illuminate\Database\QueryException $e) {

      return redirect()->back()->with('erro', 'Não foi possivel cadastrar o subelemento.');

    }
  }

  public function deletarSubele($id)
  {
    $ac = Natureza::find($id);
    $ac->delete();
    Log::create([
      'tabela' => 'naturezas',
      'idLinha' => $ac->id,
      'user' => Auth::user()->name,
      'acao' => 'deletar'
    ]);

    return redirect()->back();
  }

//Modalidade de Licitação
  public function indexMod()
  {
      $modlic = ModalidadeLicitacao::all();
      return view('erp.cadastro.modalidadelic.inserir' , compact('modlic'));

  }

  public function salvarMod(Request $req)
  {
    try {

      $dados = $req->all();
      $modlic = ModalidadeLicitacao::create($dados);
      Log::create([
        'tabela' => 'modalidade_licitacao',
        'idLinha' => $modlic->id,
        'user' => Auth::user()->name,
        'acao' => 'salvar'
      ]);
      return redirect()->route('modalidadelic');

    } catch (\Illuminate\Database\QueryException $e) {

      return redirect()->back()->with('erro', 'Não foi possivel cadastrar a modalidade da licitação.');

    }
  }

  public function deletarMod($id)
  {
    $ac = ModalidadeLicitacao::find($id);
    $ac->delete();
    Log::create([
      'tabela' => 'modalidade_licitacao',
      'idLinha' => $ac->id,
      'user' => Auth::user()->name,
      'acao' => 'deletar'
    ]);

    return redirect()->route('modalidadelic');
  }

//Referencia Legal
  public function indexRefLegal()
  {
      $ref = RefLegal::all();
      return view('erp.cadastro.reflegal.inserir' , compact('ref'));

  }

  public function salvarRefLegal(Request $req)
  {
    try {

      $dados = $req->all();
      $ref = RefLegal::create($dados);
      Log::create([
        'tabela' => 'ref_legal',
        'idLinha' => $ref->id,
        'user' => Auth::user()->name,
        'acao' => 'salvar'
      ]);
      return redirect()->route('reflegal');

    } catch (\Illuminate\Database\QueryException $e) {

      return redirect()->back()->with('erro', 'Não foi possivel cadastrar a referência legal.');

    }
  }

  public function deletarRefLegal($id)
  {
    $ac = RefLegal::find($id);
    if(Empenho::where('reflegalempenho',$id)->exists()){
      return redirect()->back()->with('erro', 'Não é possível deletar.');
    }else{
      $ac->delete();
      Log::create([
        'tabela' => 'ref_legal',
        'idLinha' => $ac->id,
        'user' => Auth::user()->name,
        'acao' => 'deletar'
      ]);

      return redirect()->route('reflegal');
    }
  }

//Tipo retenção
public function indexTpRetencao()
{
    $tp = TipoRetencao::all();
    return view('erp.cadastro.tpretencao.inserir' , compact('tp'));

}

public function salvarTpRetencao(Request $req)
{
  try {

    $dados = $req->all();
    $tp = TipoRetencao::create($dados);
    Log::create([
      'tabela' => 'tipo_retencao',
      'idLinha' => $tp->id,
      'user' => Auth::user()->name,
      'acao' => 'salvar'
    ]);
    return redirect()->route('tpretencao');

  } catch (\Illuminate\Database\QueryException $e) {

    return redirect()->back()->with('erro', 'Não foi possível cadastrar .');

  }
}

public function deletarTpRetencao($id)
{
  $ac = TipoRetencao::find($id);
  if(Retencao::where('id_tipo',$id)->exists()){
    return redirect()->back()->with('erro', 'Não é possível deletar.');
  }else{
    $ac->delete();
    Log::create([
      'tabela' => 'tipo_retencao',
      'idLinha' => $ac->id,
      'user' => Auth::user()->name,
      'acao' => 'deletar'
    ]);

    return redirect()->route('tpretencao');
  }
}


}
