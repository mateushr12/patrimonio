<?php

namespace App\Http\Controllers\ERP;

use App\ERP\Contrato;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\Log;
use App\ERP\Fornecedores;
use App\ERP\Aditivos;
use App\ERP\Empenho;
use App\ERP\AnexosContrato;
use Auth;
use Illuminate\Support\Facades\Storage;
use DB;


class ContratoController extends Controller
{
    public function index()
    {
        $fornecedores = Fornecedores::where('status', 1)->get();
        $dt = date('Y-m-d');
        //$contratos = Contrato::all();
        $contratos = Contrato::leftJoin('aditivos', function ($join) {
          $join->on('contratos.id', '=', 'aditivos.id_contrato');
        })
        ->selectRaw('contratos.id, contratos.numerocontrato ,contratos.licitacao ,contratos.iniciovigencia ,contratos.finalvigencia ,contratos.fornecedorcontratado, contratos.empenho,
        contratos.valorcontrato, max(aditivos.vigencia_f) as vigencia')
        ->groupBy('contratos.id','contratos.numerocontrato' ,'contratos.licitacao' ,'contratos.iniciovigencia' ,'contratos.finalvigencia' ,'contratos.fornecedorcontratado', 'contratos.empenho',
        'contratos.valorcontrato')
        //->where('contratos.finalvigencia', '>', date('Y-m-d'))
        //->orWhere('aditivos.vigencia_f', '>', date('Y-m-d'))
        //->whereRaw('contratos.finalvigencia > now() or max(aditivos.vigencia_f) > now()')
        ->havingRaw('contratos.finalvigencia > now() or max(aditivos.vigencia_f) > now()')
        ->get();

        //dd($contratos);

        return view ('erp.contratos.index' , compact('contratos','fornecedores'));
    }

    public function encerrados()
    {
        $fornecedores = Fornecedores::where('status', 1)->get();
        $contratos = Contrato::leftJoin('aditivos', function ($join) {
          $join->on('contratos.id', '=', 'aditivos.id_contrato');
        })
        ->selectRaw('contratos.id, contratos.numerocontrato ,contratos.licitacao ,contratos.iniciovigencia ,contratos.finalvigencia ,contratos.fornecedorcontratado, contratos.empenho,
        contratos.valorcontrato, max(aditivos.vigencia_f) as vigencia')
        ->groupBy('contratos.id','contratos.numerocontrato' ,'contratos.licitacao' ,'contratos.iniciovigencia' ,'contratos.finalvigencia' ,'contratos.fornecedorcontratado', 'contratos.empenho',
        'contratos.valorcontrato')
        //->whereRaw('contratos.finalvigencia < now() and (max(aditivos.vigencia_f) is null or max(aditivos.vigencia_f) < now())')
        ->havingRaw('contratos.finalvigencia < now() and (max(aditivos.vigencia_f) is null or max(aditivos.vigencia_f) < now())')
        ->get();

        //dd($contratos);

        return view ('erp.contratos.encerrados' , compact('contratos','fornecedores'));
    }

    public function ver($id)
    {
        $contrato = Contrato::find($id);
        $aditivo =  Aditivos::where('id_contrato',$id)->get();
        return view ('erp.contratos.ver' , compact('contrato','aditivo'));
    }

    public function salvar(Request $req)
    {
      try
      {
        $dados = $req->all();
        if ($req->input('iniciovigencia') >= $req->input('finalvigencia')){
          return back()->with('erro', 'Data de inicio da vigência não pode ser igual nem maior que a data final da vigência');
        }
        else{
          $contrato = Contrato::create($dados);
          Log::create([
            'tabela' => 'contratos',
            'idLinha' => $contrato->id,
            'user' => Auth::user()->name,
            'acao' => 'salvar'
          ]);
          return redirect()->route('contratos.index');
        }
      }
      catch(\Illuminate\Database\QueryException $e)
      {
        return redirect()->back()->with('erro', 'Não foi possivel cadastrar o contrato. ERRO:'.$e->getMessage());
      }
      catch(PDOException $e)
      {
        return redirect()->back()->with('erro', 'Algo errado aconteceu, tente novamente!');
      }
    }

    public function editar($id)
    {
        $registro = Contrato::find($id);
        $fornecedores = Fornecedores::where('status', 1)->get();
        return view('erp.contratos.editar' , compact('registro', 'fornecedores'));
    }

    public function atualizar(Request $req, $id)
    {
        $dados = $req->all();
        $atualizar = Contrato::find($id);
        if ($req->input('iniciovigencia') >= $req->input('finalvigencia')){
          return back()->with('erro', 'Data de inicio da vigência não pode ser igual nem maior que a data final da vigência');
        }
        else{
          $atualizar->update($dados);
          Log::create([
            'tabela' => 'contratos',
            'idLinha' => $atualizar->id,
            'user' => Auth::user()->name,
            'acao' => 'atualizar'
          ]);
          return redirect()->route('contratos.index');
        }

    }

    public function deletar($id)
    {
      $ac = Contrato::find($id);
      $ad = Aditivos::where('id_contrato', $ac->id)->exists();
      $anx = AnexosContrato::where('id_contrato', $ac->id)->exists();
      $emp = Empenho::where('licempenho', $ac->id)->exists();

      if (isset($ac) && !$ad && !$anx){
        if ($emp){
          return back()->with('erro', 'Não é possível deletar esse contrato pôs ele está sendo utilizado em um ou mais empenhos.');
        }else{
          $ac->delete();
          Contrato::where('id', $id)->delete();
          Log::create([
            'tabela' => 'contratos',
            'idLinha' => $ac->id,
            'user' => Auth::user()->name,
            'acao' => 'deletar'
          ]);
          return redirect()->route('contratos.index');
        }
      }else{
        return back()->with('erro', 'Não é possível deletar esse contrato pôs ele contém aditivo ou anexos.');
      }
    }

    public function anexar($id)
    {
      $arquivo = AnexosContrato::where('id_contrato', $id)->get();
      $contrato = $id;
      return view('erp.contratos.anexar' , compact('arquivo', 'contrato'));
    }


    public function anexarSalvar(Request $req)
    {
      $idcontrato = $req->input('id');
      $nomereq = $req->file('arquivo')->store('contratos');
      $contrato = AnexosContrato::create([
        'id_contrato' => $req->input('id'),
        'arquivo' => $nomereq,
        'tipoarquivo' => $req->input('tipoarquivo')
      ]);
      Log::create([
        'tabela' => 'anexos_contrato',
        'idLinha' => $contrato->id,
        'user' => Auth::user()->name,
        'acao' => 'salvar'
      ]);
      return redirect()->back();
    }

    public function pdf($id)
    {

      try {

        $arquivo = AnexosContrato::where('id', $id)->get();
        foreach ($arquivo as $key => $value) {
          $narq = $value['arquivo'];
        }
        return response()->file(storage_path("app/public/$narq"));

      } catch (\Exception $e) {

        return view('notfound');

      }
    }

    public function deletepdf($id)
    {
      $arquivo = AnexosContrato::where('id', $id)->get();
      foreach ($arquivo as $key => $value) {
        $narq = $value['arquivo'];
      }
      unlink(storage_path("app/public/$narq"));
      AnexosContrato::find($id)->delete();
      Log::create([
        'tabela' => 'anexos_contrato',
        'idLinha' => $id,
        'user' => Auth::user()->name,
        'acao' => 'deletar'
      ]);
      return redirect()->back();
    }

    public function inicioRelatorio()
    {
      $fornecedores = Fornecedores::all();
      return view('erp.contratos.buscar', compact('fornecedores'));
    }

    public function resultRelatorio(Request $req)
    {
      $data = date('d/m/Y');
      // $a = Contrato::where(function($query) use($req) {
      //   if ($req->filled(['numerocontrato'])) {
      //     $query->where('numerocontrato', 'like', "%$req->input('numerocontrato')%");
      //   }
      //   if ($req->filled(['licitacao'])) {
      //     $query->where('licitacao', $req->input('licitacao'));
      //   }
      //   if ($req->filled(['empenho'])) {
      //     $query->where('empenho', $req->input('empenho'));
      //   }
      //   if ($req->filled(['fornecedorcontratado'])) {
      //     $query->where('fornecedorcontratado', $req->input('fornecedorcontratado'));
      //   }
      // });

      $e = Contrato::leftJoin('aditivos', function ($join) {
        $join->on('aditivos.id_contrato', '=', 'contratos.id');
      })
      ->selectRaw('sum(aditivos.valor) as total, contratos.*')
      ->groupBy('contratos.id')->toSql();

      $contrato = DB::table(DB::raw("($e) as a"))
      ->leftJoin('empenhos', function ($join) {
        $join->on('empenhos.licempenho', '=', 'a.id')->whereNull('empenhos.deleted_at');
      })
      ->leftJoin('fornecedores', function ($join) {
        $join->on('a.fornecedorcontratado', '=', 'fornecedores.id');
        // ->where('fornecedores.tatus',1);
      })
      ->selectRaw('a.*, sum(empenhos.valorempenho) as total, sum(a.total) as totalPg, fornecedores.nome, year(empenhos.dataempenho) as ano')
      ->where(function($query) use($req) {
        if ($req->filled(['numerocontrato'])) {
          $query->where('numerocontrato', 'like', '%'.$req->input('numerocontrato').'%');
        }
        if ($req->filled(['licitacao'])) {
          $query->where('licitacao', 'like', '%'.$req->input('licitacao').'%');
        }
        if ($req->filled(['empenho'])) {
          $query->where('empenho', 'like', '%'.$req->input('empenho').'%');
        }
        if ($req->filled(['fornecedorcontratado'])) {
          $query->where('fornecedorcontratado', $req->input('fornecedorcontratado'));
        }
      })
      ->groupBy(DB::raw('a.id, year(empenhos.dataempenho)'))
      ->get();

      //dd($contrato);
      if(count($contrato) > 0){
        for ($i=0; $i < count($contrato); $i++) {
          $totalC[] = (isset($contrato[$i-1]->licitacao) and $contrato[$i]->licitacao == $contrato[$i-1]->licitacao) ? '-' : ($contrato[$i]->valorcontrato + $contrato[$i]->totalPg);
        }
        $totalC = array_sum($totalC);
      }else{
        $totalC = null;
      }

      // $t = [];
      // $disp = [];
      // $v = [];
      // $vl = [];
      //
      // foreach ($acoes as $key => $value) {
      //   $t[] = $value['empenhado'];
      //   $disp[] = ($value['ValorDestinado'] + $value['valor']) - $value['empenhado'];
      //   $v[] = $value['valor'];
      //   $vl[] = $value['ValorDestinado'];
      // }
      //
      // $somaEmp = array_sum($t);
      // $somaDisp = array_sum($disp);
      // $somaVl = array_sum($v);
      // $somaVlD = array_sum($vl);

      return view('erp.contratos.imprimir', compact('contrato','data','totalC'));
    }

}
