<?php

namespace App\Http\Controllers\ERP;

use App\ERP\Taxa;
use App\ERP\TaxaBanese;
use App\ERP\ProcessaArquivo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use PDF;
use Auth;
use App\ERP\Log;



class TaxasController extends Controller
{
    public function index(){

        $taxas = Taxa::select('tipo_evento_redesim')->distinct()->get();
        return view('erp.taxas.index' , compact('taxas'));
    }


    public function resultado(Request $req){

      $data = date('d/m/Y');

      $r = $req->session()->put([
        'protocolo' => $req->input('protocolo'),
        'cpf_solicitante' => $req->input('cpf_solicitante'),
        'cnpj_solicitante' => $req->input('cnpj_solicitante'),
        'nosso_numero' => $req->input('nosso_numero'),
        'cd_documento' => $req->input('cd_documento'),
        'dt_emissao_in' => $req->input('dt_emissao_in'),
        'dt_emissao_fl' => $req->input('dt_emissao_fl'),
        'dt_paga_in' => $req->input('dt_paga_in'),
        'dt_paga_fl' => $req->input('dt_paga_fl'),
        'tipo_evento_redesim' => $req->input('tipo_evento_redesim')
      ]);

      return redirect()->route('taxas.sessao');
    }


    public function sessao(Request $req){

      $a = Taxa::where(function($query) use($req) {
        if ($req->session()->has('protocolo')) {
          $query->where('protocolo', 'like' ,'%'.trim($req->session()->get('protocolo')).'%');
        }
        if ($req->session()->has('cpf_solicitante')) {
          $query->where('cpf_solicitante', trim($req->session()->get('cpf_solicitante')));
        }
        if ($req->session()->has('cnpj_solicitante')) {
          $query->where('cnpj_solicitante', trim($req->session()->get('cnpj_solicitante')));
        }
        if ($req->session()->has('nosso_numero')) {
          $query->where('nosso_numero', 'like' ,'%'.trim($req->session()->get('nosso_numero')).'%');
        }
        if ($req->session()->has('cd_documento')) {
          $query->where('cd_documento', trim($req->session()->get('cd_documento')));
        }
        if ($req->session()->has('dt_emissao_in') and $req->session()->has('dt_emissao_fl')) {
          $query->whereBetween('dt_emissao', [$req->session()->get('dt_emissao_in'),$req->session()->get('dt_emissao_fl')]);
        }
        if ($req->session()->has('dt_paga_in') and $req->session()->has('dt_paga_fl')) {
          $query->whereBetween('dt_pagamento', [$req->session()->get('dt_paga_in'),$req->session()->get('dt_paga_fl')]);
        }
        if ($req->session()->has('tipo_evento_redesim')) {
          $query->where('tipo_evento_redesim', $req->session()->get('tipo_evento_redesim'));
        }
      });

      $buscas = $a->select('protocolo','cpf_solicitante','cnpj_solicitante','nosso_numero','cd_documento','dt_emissao','dt_vencimento','dt_pagamento','tipo_evento_redesim','vl_recebido')
          ->distinct()
          ->orderBy('nosso_numero', 'asc')
          ->get();

      if(count($buscas) > 0){
        for ($i=0; $i < count($buscas); $i++) {
          $busca[] = [
            'protocolo' => $buscas[$i]['protocolo'],
            'cpf_solicitante' => $buscas[$i]['cpf_solicitante'],
            'cnpj_solicitante' => $buscas[$i]['cnpj_solicitante'],
            'nosso_numero' => $buscas[$i]['nosso_numero'],
            'cd_documento' => $buscas[$i]['cd_documento'],
            'dt_emissao' => $buscas[$i]['dt_emissao'],
            'dt_vencimento' => $buscas[$i]['dt_vencimento'],
            'dt_pagamento' => $buscas[$i]['dt_pagamento'],
            'tipo_evento_redesim' => $buscas[$i]['tipo_evento_redesim'],
            'vl_recebido' => $buscas[$i]['vl_recebido'] = (isset($buscas[$i+1]['nosso_numero']) and $buscas[$i]['nosso_numero'] == $buscas[$i+1]['nosso_numero']) ? '-' : $buscas[$i]['vl_recebido']
          ];
        }

        foreach ($busca as $ky => $vl) {
          $valor[] = isset($vl['vl_recebido']) ? $vl['vl_recebido'] : [];
        }

        $somaRec = array_sum($valor);
        $registros = count($valor);

      }else{
        $busca = [];
        $somaRec = 0.00;
        $registros = 0;
      }

      $re = session()->put([
        'busca' => $busca,
        'somaRec' => $somaRec,
        'registros' => $registros
      ]);

      return view('erp.taxas.resultado', compact('busca','somaRec','registros'));

    }

    public function pdf()
    {
      set_time_limit(300);
      $busca = session()->get('busca');
      $somaRec = session()->get('somaRec');
      $registros = session()->get('registros');
      return view('erp.taxas.pdf', compact('busca','somaRec','registros'));

      // return PDF::loadView('erp.empenho.pdf', compact('empenho'))
      //             // Se quiser que fique no formato a4 retrato: ->setPaper('a4', 'landscape')
      //             ->download('empenho.pdf');

      // return PDF::loadView('erp.taxas.pdf', compact('busca','somaRec','registros'))
      // ->setPaper('a4', 'Landscape')
      // ->stream();

    }


    public function detalhes($nn){

      $eventos = Taxa::where('nosso_numero',$nn)->get();
      $resultado = Taxa::where('nosso_numero',$nn)->first();
      $data = date('d/m/Y');
      return view('erp.taxas.detalhe', compact('resultado','data', 'eventos'));

    }

    public function novaSerasa(){
      $serasas = TaxaBanese::where('tipo', '0000')
      ->orWhere('tipo', '0001')
      ->orderBy('data_pagamento')
      ->get();
      return view('erp.taxas.serasa', compact('serasas'));
    }

    public function deletarserasa($id){
      $serasa = TaxaBanese::find($id);
      $serasa->delete();
      return redirect()->back();
    }

    public function serasaBoleto(){
        return view('erp.taxas.boletos');
    }

    public function serasaBuscar(Request $r){

      $serasa = TaxaBanese::where('nosso_numero',$r->nn)->get();
      //dd($serasa);
      return view('erp.taxas.boletos', compact('serasa'));
    }

    public function serasaAtualizar(Request $r){
      $serasa = TaxaBanese::find($r->id);
      $dados = $r->all();
      $serasa->update($dados);
      return redirect('erp/taxas/serasaBoleto')->with('sucesso','Taxa atualizada com sucesso.');
    }

    public function salvarSerasa(Request $r){

      // $r->merge([
      //   'cpf_solicitante' => '-',
      //   'cnpj_solicitante' => '-',
      //   'cd_documento' => '-',
      //   'dt_emissao' => $r->input('dt_pagamento'),
      //   'dt_vencimento' => $r->input('dt_pagamento'),
      //   'pago' => 'Sim',
      //   'tipo_evento_redesim' => 'SERASA/BOA VISTA', //EVENTOS FORA DO B.I.
      //   'natureza_juridica' => '-',
      //   'evento_redesim' => '-',
      //   'porte' => '-',
      //   'cpf_cnpj_contador' => '-',
      //   'nome_contador' => '-',
      //   'vl_recebido' => str_replace(',', '.', str_replace('.', '', $r->input('vl_recebido')))
      // ]);
      $vlD = str_replace(',', '.', str_replace('.', '', $r->input('valor')));
      $r->merge([
        'id_taxa_retorno' => 3,
        'valor' => $vlD
      ]);
      $dados = $r->all();      
      $criar = ProcessaArquivo::create($dados);
      // Log::create([
      //   'tabela' => 'taxas',
      //   'idLinha' => $criar->id,
      //   'user' => Auth::user()->id,
      //   'acao' => 'salvar'
      // ]);
      return redirect()->back()->with('sucesso', 'Taxa cadastrada com sucesso.');

    }



}
