<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\Pagamento;
use App\ERP\Empenho;
use App\ERP\Liquidado;
use App\ERP\Retencao;
use App\ERP\Fornecedores;
use App\ERP\TipoRetencao;
use App\ERP\Log;
use Response;
use Form;
use Auth;
use PDF;

class PagamentoController extends Controller
{

    public function pagamento($id)
    {
      $pagamentos = Pagamento::where('id_empenho', $id)->get();

      // $pagamentos = Pagamento::leftJoin('retencoes', function ($join) {
      //   $join->on('pagamentos.id', '=', 'retencoes.id_pagamento');
      // })
      // ->where('id_empenho', $id)
      // ->selectRaw('sum(retencoes.valor) as totalretido, pagamentos.*')
      // ->groupBy('pagamentos.id')
      // ->get();

      $empenho = Empenho::find($id);
      return view('erp.empenho.pagamentos' , compact('pagamentos','empenho'));

    }

    public function salvarpagamento(Request $req, $id)
    {
      $dados = $req->all();

      $vempenhado = Empenho::where('id',$id)->sum('valorempenho');
      $vlpagamento = Pagamento::where('id_empenho',$id)->sum('valor_bruto');
      $vldisp = ($vempenhado - $vlpagamento);

      if ($req->input('valor_bruto') > $vldisp)
      {
        //dd('erro valor é maior que o valor a ser liquidado');
        return redirect()->back()->with('erro', "Valor é maior que o valor a ser pago.");
      }elseif ($req->input('valor_bruto') == 0.00)
      {
        //dd('erro valor não pode der zero');
        return redirect()->back()->with('erro', "Valor não pode ser zero.");
      }else
      {
        Pagamento::create($dados);
        return redirect()->back();
      }

    }

    public function deletarpag($id)
    {
      $pg = Pagamento::find($id);
      if (Retencao::where('id_pagamento',$id)->exists()){
        return redirect()->back()->with('erro', "Esse pagamento não pode ser deletado pôs há retenções cadastradas.");
      }else{
        //dd('Pode deletar');
        $pg->delete();
        Log::create([
          'tabela' => 'pagamentos',
          'idLinha' => $pg->id,
          'user' => Auth::user()->name,
          'acao' => 'deletar'
        ]);
        return redirect()->back();
      }
    }

    public function retencao($id)
    {
      //$pagamentos = Pagamento::where('id_empenho', $id)->get();
      $pagamento = Pagamento::find($id);
      $retencoes = Retencao::where('id_pagamento', $id)->get();
      $tipos = TipoRetencao::all();

      return view('erp.empenho.retencao' , compact('retencoes','pagamento','tipos'));
    }

    public function salvarretencao(Request $req, $id)
    {
      $dados = $req->all();

      $vlpagamento = Pagamento::where('id',$id)->sum('valor_bruto');
      $vlretido = Retencao::where('id_pagamento',$id)->sum('valor');
      $vldisp = ($vlpagamento - $vlretido);

      if ($req->input('valor') > $vldisp)
      {
        return redirect()->back()->with('erro', "Valor é maior que o valor do pagamento.");
      }elseif ($req->input('valor') == 0.00)
      {
        return redirect()->back()->with('erro', "Valor não pode ser zero.");
      }else
      {
        Retencao::create($dados);
        return redirect()->back();
      }
    }


    public function deletarret($id)
    {
      $ret = Retencao::find($id);
      $ret->delete();
      Log::create([
        'tabela' => 'retencoes',
        'idLinha' => $ret->id,
        'user' => Auth::user()->name,
        'acao' => 'deletar'
      ]);
      return redirect()->back();

    }


    public function pagamentoret($id)
    {
      $retencoes = Retencao::find($id);
      return view('erp.empenho.retencaopagar' , compact('retencoes'));

    }


    public function pagarret(Request $req, $id)
    {
      $dados = $req->all();
      $r = Retencao::find($id);
      if ($req->input('datapag') < $r->data){
        return redirect()->back()->with('erro', "Error na data de pagamento");
      }else{
        $r->update($dados);
        return redirect()->route('retencao',$r->id_pagamento);
      }

    }

    public function inicioRelatorio()
    {
      //$fornecedores = Fornecedores::all();

      $mes = Pagamento::selectRaw('month(datapag) as mes')
      ->groupBy(\DB::raw('month(datapag)'))
      ->orderBy(\DB::raw('month(datapag)'))
      ->get();

      $ano = Pagamento::selectRaw('year(datapag) as ano')
      ->groupBy(\DB::raw('year(datapag)'))
      ->orderBy(\DB::raw('year(datapag)'))
      ->get();

      return view('erp.empenho.buscarPagamento', compact('mes','ano'));
    }

    public function resultRelatorio(Request $req)
    {
      // $a = Pagamento::leftjoin('empenhos', function ($join) {
      //     $join->on('pagamentos.id_empenho', '=', 'empenhos.id')->whereNull('empenhos.deleted_at');
      // })->leftjoin('contratos', function ($join) {
      //     $join->on('empenhos.licempenho', '=', 'contratos.id');
      // })->leftjoin('fornecedores', function ($join) {
      //     $join->on('contratos.fornecedorcontratado', '=', 'fornecedores.id');
      // });
      // $data = date('d/m/Y');
      // $pagamentos = $a->where(function($query) use($req) {
      //   if ($req->filled(['ob'])) {
      //     $query->where('ob', 'like', '%'.$req->input('ob').'%');
      //   }
      //   if ($req->filled(['fornecedor'])) {
      //     $query->where('fornecedorcontratado', '=', $req->input('fornecedor'));
      //   }
      //   if ($req->filled(['datapag_i']) and $req->filled(['datapag_f'])) {
      //     $query->whereBetween('datapag', [ $req->input('datapag_i'), $req->input('datapag_f') ]);
      //   }
      // })
      // ->orderBy('fornecedorcontratado', 'asc')
      // ->get();

      $pagamentos = Pagamento::leftjoin('empenhos', function ($join) {
          $join->on('pagamentos.id_empenho', '=', 'empenhos.id')->whereNull('empenhos.deleted_at');
      })->leftjoin('contratos', function ($join) {
          $join->on('empenhos.licempenho', '=', 'contratos.id');
      })->leftjoin('fornecedores', function ($join) {
          $join->on('contratos.fornecedorcontratado', '=', 'fornecedores.id');
      })
      ->selectRaw(' "101000000" as fr, (
        case
        when empenhos.tipoempenho = 1 then empenhos.dataempenho
        when empenhos.tipoempenho = 1 then empenhos.dataempenho
        end ) as dtEmp,
        empenhos.nomecredor, fornecedores.nome , pagamentos.nf, pagamentos.vencimento, pagamentos.valor_bruto, pagamentos.ob, pagamentos.datapag, empenhos.obsempenho, empenhos.numero,
        empenhos.obsempenho  ')
      ->where(\DB::raw("MONTH(pagamentos.datapag)"), $req->input('mes'))
      ->where(\DB::raw("YEAR(pagamentos.datapag)"), $req->input('ano'))
      ->orderBy('pagamentos.datapag')
      ->orderBy('pagamentos.ob')
      ->get();

      //dd($a);
      $no = 0;

      switch ($req->input('mes')) {
        case 1: $m = 'JANEIRO'; break;
        case 2: $m = 'FEVEREIRO'; break;
        case 3: $m = 'MARÇO'; break;
        case 4: $m = 'ABRIL'; break;
        case 5: $m = 'MAIO'; break;
        case 6: $m = 'JUNHO'; break;
        case 7: $m = 'JULHO'; break;
        case 8: $m = 'AGOSTO'; break;
        case 9: $m = 'SETEMBRO'; break;
        case 10: $m = 'OUTUBRO'; break;
        case 11: $m = 'NOVEMBRO'; break;
        case 12: $m = 'DEZEMBRO'; break;
      }

      $data = $m.' / '.$req->input('ano');

      return view('erp.empenho.imprimirPagamento', compact('pagamentos','data','no'));

    }

    public function listCorrigir()
    {
      $pag = Pagamento::all();
      return view('erp.cadastro.corrigirPag.index', compact('pag'));
    }

    public function corrigirObPost(Request $r)
    {
      $atual = Pagamento::find($r->input('id'));
      $atual->update($r->all());
      return redirect()->back();
    }


}
