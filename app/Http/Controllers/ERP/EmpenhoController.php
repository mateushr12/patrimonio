<?php

namespace App\Http\Controllers\ERP;

use App\ERP\Empenho;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\Elemento;
use App\ERP\Natureza;
use App\ERP\OrcamentoInicial;
use App\ERP\Acoes;
use App\ERP\ModalidadeLicitacao;
use App\ERP\RefLegal;
use App\ERP\Contrato;
use App\ERP\Aditivos;
use App\ERP\Cota;
use App\ERP\Ano;
use App\ERP\OrcamentoSuplementar;
use App\ERP\Pagamento;
use App\ERP\Log;
use Response;
use DB;
use Form;
use Auth;
use PDF;

class EmpenhoController extends Controller
{
  //////////// CARREGA A PAGINA INICIAL DOS EMPENHOS ////////////////////////
  public function index($order=null, $par=null)
  {
    //$registros = Empenho::all();
    // $registros = Empenho::leftJoin('liquidados', function ($join) {
    //   $join->on('empenhos.id', '=', 'liquidados.id_empenho');
    // })
    // ->selectRaw('sum(liquidados.valor) as totaliquidado, empenhos.numero,
    // empenhos.dataempenho, empenhos.tipoempenho, empenhos.valorempenho,
    // empenhos.liquidado, empenhos.modempenho, empenhos.id')
    // ->groupBy('empenhos.id')
    // ->get();

    $registros = Empenho::leftJoin('pagamentos', function ($join) {
      $join->on('empenhos.id', '=', 'pagamentos.id_empenho');
    })
    ->selectRaw('sum(pagamentos.valor_bruto) as totapago, empenhos.numero,
    empenhos.dataempenho, empenhos.tipoempenho, empenhos.valorempenho,empenhos.licempenho,
    empenhos.liquidado, empenhos.modempenho, empenhos.id')
    ->groupBy('empenhos.id');
    //->get();

    if (isset($order)){
      $registros = $registros->orderBy($order, $par)->get();
    }else {
      $registros = $registros->get();
    }

    $elementos = Elemento::all();
    $orcamento = OrcamentoInicial::where('ativo', 0)->max('id');
    $refs = RefLegal::all();
    $modLs = ModalidadeLicitacao::all();
    $contratos = Contrato::all();
    $acoes = Acoes::where('CodOrcamento', $orcamento)->get();

    return view('erp.empenho.index' , compact('registros',  'elementos', 'acoes', 'refs', 'modLs', 'contratos'));
  }

  //////////////////////// SALVA NOVOS EMPENHOS /////////////////////////////////////////////
  public function salvar(Request $req)
  {
    $dados = $req->all();
    if ($req->input('subelemento') === null){
      return redirect()->back()->with('erro', "Sub-elemento não pode ser vazio.");
    }else{
      //$vlEmpenho = $req->input('valorempenho');
      $vlEmpenho = str_replace(',', '.', str_replace('.', '', $req->input('valorempenho')));

      $acao = Acoes::find($req->input('acaoempenho'));
      $acaosupl = OrcamentoSuplementar::where('codAcoes',$req->input('acaoempenho'))->get();
      foreach ($acaosupl as $key => $value) {
        $vlSpl = $value['valor'];
      }
      $vlSpl = isset($vlSpl) ? $vlSpl : 0.00;
      $vl = $acao->ValorDestinado + $vlSpl;

      $vlEmpenhado = Empenho::where('acaoempenho',$req->input('acaoempenho'))->sum('valorempenho');

      $vlContratado = Empenho::where('licempenho',$req->input('licempenho'))->sum('valorempenho');
      $vlAditivo = Aditivos::where('id_contrato',$req->input('licempenho'))->sum('valor');
      //$vlContratado = $vlContratado + ($vlAditivo);

      $vlDisponivel = $vl - $vlEmpenhado;

      $req->merge([
        'valorempenho' => $vlEmpenho
      ]);
      $dados = $req->all();

      if (round($vlEmpenho,2) > round($vlDisponivel,2)){
        return redirect()->back()->with('erro', "Valor do empenho maior que o valor disponível ($vlDisponivel).");
      }else{
        $dt = explode('-', $req->input('dataempenho'));

        $ano = Ano::join('orcamento_inicial', function ($join) {
          $join->on('anos.id', '=', 'orcamento_inicial.ano');
        })
        ->join('acoes', function ($join) {
          $join->on('orcamento_inicial.id', '=', 'acoes.CodOrcamento');
        })
        ->where('acoes.id', $req->input('acaoempenho'))
        ->where('anos.ano', $dt[0])
        ->exists();

        if($ano == false){
          return redirect()->back()->with('erro', "Data do empenho diferente do ano da ação.");
        }
        ///////////////////////////////////////// Validar so contrato sem a cota /////////////////////////////////////////////////////////////////
        $banco = $req->input('bancoprecoempenho');
        if ($banco == 1){
          if ($req->input('licempenho') == ''){
            return redirect()->back()->with('erro', "Escolha uma licitacão");
          }
          $contrato = Contrato::find($req->input('licempenho'));
          $vlContrato = ($contrato->valorcontrato) +   $vlAditivo;
          $vlDisponivelContr = $vlContrato - $vlContratado;
          //$data = (isset($contrato->vigencia) ? $contrato->vigencia : $contrato->finalvigencia);

          if (round($vlEmpenho,2) > round($vlDisponivelContr,2)){
            return redirect()->back()->with('erro', "Valor do empenho maior que o valor do contrato disponível ($vlDisponivelContr).");
            // }elseif (date('Y-m-d') > $data){
            //   return redirect()->back()->with('erro', "Erro, contrato fora de vigência.");
          }else{
            $e = Empenho::create($dados);
            Log::create([
              'tabela' => 'empenhos',
              'idLinha' => $e->id,
              'user' => Auth::user()->name,
              'acao' => 'salvar'
            ]);
            return redirect()->route('empenho.index');
          }
        }else{
          $e = Empenho::create($dados);
          Log::create([
            'tabela' => 'empenhos',
            'idLinha' => $e->id,
            'user' => Auth::user()->name,
            'acao' => 'salvar'
          ]);
          return redirect()->route('empenho.index');
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // $cota = Cota::where('cotaacao',$req->input('acaoempenho'))->where('mesrefcota', $dt[1]);
        // if ($cota->exists()){
        //   foreach ($cota->get() as $key => $value) {
        //     $vlCota = $value->valorcota;
        //   }
        //   if ($vlEmpenho > $vlCota){
        //     return redirect()->back()->with('erro', "Valor do empenho maior que o valor da cota ($vlCota), aumente o valor da cota.");
        //   }else{
        //     $banco = $req->input('bancoprecoempenho');
        //     if ($banco == 1){
        //       $contrato = Contrato::find($req->input('licempenho'));
        //       $vlContrato = ($contrato->valorcontrato) +   $vlAditivo;
        //       $vlDisponivelContr = $vlContrato - $vlContratado;
        //       //$data = (isset($contrato->vigencia) ? $contrato->vigencia : $contrato->finalvigencia);
        //
        //       if ($vlEmpenho > $vlDisponivelContr){
        //         return redirect()->back()->with('erro', "Valor do empenho maior que o valor do contrato disponível ($vlDisponivelContr).");
        //         // }elseif (date('Y-m-d') > $data){
        //         //   return redirect()->back()->with('erro', "Erro, contrato fora de vigência.");
        //       }else{
        //
        //         Empenho::create($dados);
        //         return redirect()->route('empenho.index');
        //       }
        //     }else{
        //       Empenho::create($dados);
        //       return redirect()->route('empenho.index');
        //     }
        //   }
        // }else{
        //   return redirect()->back()->with('erro', "Não há cota mensal cadastrada para essa ação no mês indicado, por favor cadastre uma cota.");
        // }
      }
    }
  }

  ////////////////////////////////// CARREGA PAGINA DE EDIÇÃO DE EMPENHOS /////////////////////////////////////////
  public function editar($id)
  {
    $empenho = Empenho::find($id);
    $empenhopai = Empenho::find($empenho->id_empenho);
    $modLs = ModalidadeLicitacao::all();
    $orcamento = OrcamentoInicial::where('ativo', 0)->max('id');
    $acoes = Acoes::where('CodOrcamento', $orcamento)->get();
    $elementos = Elemento::all();
    $contratos = Contrato::all();
    $refs = RefLegal::all();
    $subelemento = Natureza::find($empenho->subelemento);

    return view('erp.empenho.editar' , compact('empenho', 'modLs','orcamento','acoes','elementos','contratos','refs','subelemento', 'empenhopai'));
  }

  /////////////////////////////// CARREGAR IMPRIMIR EMPENHO //////////////////////////////////////////////
  public function pdf($id)
  {
    set_time_limit(300);
    $empenho = Empenho::find($id);
    $empenhopai = Empenho::find($empenho->id_empenho);
    $data = date('d/m/Y');
    return view('erp.empenho.pdf', compact('empenho','empenhopai','data'));
    // return PDF::loadView('erp.empenho.pdf', compact('empenho'))
    //             // Se quiser que fique no formato a4 retrato: ->setPaper('a4', 'landscape')
    //             ->download('empenho.pdf');
    //return PDF::loadView('erp.empenho.pdf', compact('empenho'))->stream();

  }

  ////////////////////////////// ATUALIZA O EMPENHO EDITADO ////////////////////////////////////
  public function atualizar(Request $req , $id)
  {
    $dados = $req->all();
    $atualizar = Empenho::find($id);
    $atualizar->update($dados);
    Log::create([
      'tabela' => 'empenhos',
      'idLinha' => $atualizar->id,
      'user' => Auth::user()->name,
      'acao' => 'atualizar'
    ]);
    return redirect()->route('empenho.index');
  }

  /////////////////// CARREGA A PAGINA DE CRIAÇÃO DE REFORÇO /////////////////////////////////////
  public function reforco($id)
  {
    $empenho = Empenho::find($id);

    $modLs = ModalidadeLicitacao::all();
    $orcamento = OrcamentoInicial::where('ativo', 0)->max('id');
    $acoes = Acoes::where('CodOrcamento', $orcamento)->get();
    $elementos = Elemento::all();
    $contratos = Contrato::all();
    $refs = RefLegal::all();
    $subelemento = Natureza::find($empenho->subelemento);

    return view('erp.empenho.reforco' , compact('empenho', 'modLs','orcamento','acoes','elementos','contratos','refs','subelemento'));
  }

  /////////////////////// SALVA O NOVO REFORÇO ///////////////////////////////////////////////
  public function newreforco(Request $r, $id)
  {

    $emp = Empenho::find($id);
    //$vla = $r->input('valorempenho');//valor do empenho
    $vla = str_replace(',', '.', str_replace('.', '', $r->input('valorempenho')));

    $acao = Acoes::find($emp->acaoempenho);
    $acaosupl = OrcamentoSuplementar::where('codAcoes',$emp->acaoempenho)->get();
    foreach ($acaosupl as $key => $value) {
      $vlSpl = $value['valor'];
    }
    $vlSpl = isset($vlSpl) ? $vlSpl : 0.00;
    $vl = $acao->ValorDestinado + $vlSpl;

    $vlEmpenhado = Empenho::where('acaoempenho',$emp->acaoempenho)->sum('valorempenho');

    $vlContratado = Empenho::where('licempenho',$r->input('licempenho'))->sum('valorempenho');
    $vlAditivo = Aditivos::where('id_contrato',$r->input('licempenho'))->sum('valor');
    //$vlContratado = $vlContratado + ($vlAditivo);

    $vlDisponivel = $vl - $vlEmpenhado;

    $r->merge([
      'id_empenho' => $id,
      'bancoprecoempenho' => $emp->bancoprecoempenho,
      'urgempenho' => $emp->urgempenho,
      'modempenho' => $emp->modempenho,
      'subelemento' => $emp->subelemento,
      'modlicempenho' => $emp->modlicempenho,
      'despempenho' => $emp->despempenho,
      'acaoempenho' => $emp->acaoempenho,
      'elemempenho' => $emp->elemempenho,
      'convempenho' => $emp->convempenho,
      'licempenho' => $emp->licempenho,
      'protoempenho' => $emp->protoempenho,
      'credorempenho' => $emp->credorempenho,
      'doccredor' => $emp->doccredor,
      'numerodoccredor' => $emp->numerodoccredor,
      'nomecredor' => $emp->nomecredor,
      'reflegalempenho' => $emp->reflegalempenho,
      'nuproempenho' => $emp->nuproempenho,
      'dt_pai' => $emp->dataempenho,
      'valorempenho' => $vla
    ]);
    $dados = $r->all();

    if (round($vla,2) > round($vlDisponivel,2)){
      return redirect()->back()->with('erro', "Valor do reforço maior que o valor disponível ($vlDisponivel).");
    }else{
      $dt = explode('-', $r->input('dataempenho'));
      $cota = Cota::where('cotaacao',$emp->acaoempenho)->where('mesrefcota', $dt[1])->get();

      $ano = Ano::join('orcamento_inicial', function ($join) {
        $join->on('anos.id', '=', 'orcamento_inicial.ano');
      })
      ->join('acoes', function ($join) {
        $join->on('orcamento_inicial.id', '=', 'acoes.CodOrcamento');
      })
      ->where('acoes.id', $emp->acaoempenho)
      ->where('anos.ano', $dt[0])
      ->exists();

      if($ano == false){
        return redirect()->back()->with('erro', "Data do empenho diferente do ano da ação.");
      }

      ///////////////////////////////////////////// So contrato sem cotas //////////////////////////////////////////////////////////////////////
      $banco = $emp->bancoprecoempenho;
      if ($banco == 1){
        if ($req->input('licempenho') == ''){
          return redirect()->back()->with('erro', "Escolha uma licitacão");
        }
        $contrato = Contrato::find($emp->licempenho);
        $vlContrato = ($contrato->valorcontrato) + $vlAditivo;
        $vlDisponivelContr = $vlContrato - $vlContratado;
        //$data = (isset($contrato->vigencia) ? $contrato->vigencia : $contrato->finalvigencia);
        if (round($vla,2) > round($vlDisponivelContr,2)){
          return redirect()->back()->with('erro', "Valor do reforço maior que o valor do contrato disponível ($vlDisponivelContr).");
          // }elseif (date('Y-m-d') > $data){
          //   return redirect()->back()->with('erro', "Contrato fora de vigência.");
        }else{
          $e = Empenho::create($dados);
          Log::create([
            'tabela' => 'empenhos',
            'idLinha' => $e->id,
            'user' => Auth::user()->name,
            'acao' => 'salvar'
          ]);
          return redirect()->route('empenho.index');
        }
      }else{
        $e = Empenho::create($dados);
        Log::create([
          'tabela' => 'empenhos',
          'idLinha' => $e->id,
          'user' => Auth::user()->name,
          'acao' => 'salvar'
        ]);
        return redirect()->route('empenho.index');
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      // foreach ($cota as $key => $value) {
      //   $vlCota = $value->valorcota;
      // }
      // if ($vla > $vlCota){
      //   return redirect()->back()->with('erro', "Valor do reforço maior que o valor da cota ($vlCota), aumente o valor da cota.");
      // }else{
      //   $banco = $emp->bancoprecoempenho;
      //   if ($banco == 1){
      //     $contrato = Contrato::find($emp->licempenho);
      //     $vlContrato = ($contrato->valorcontrato) + $vlAditivo;
      //     $vlDisponivelContr = $vlContrato - $vlContratado;
      //     //$data = (isset($contrato->vigencia) ? $contrato->vigencia : $contrato->finalvigencia);
      //
      //     if ($vla > $vlDisponivelContr){
      //       return redirect()->back()->with('erro', "Valor do reforço maior que o valor do contrato disponível ($vlDisponivelContr).");
      //       // }elseif (date('Y-m-d') > $data){
      //       //   return redirect()->back()->with('erro', "Contrato fora de vigência.");
      //     }else{
      //       Empenho::create($dados);
      //       return redirect()->route('empenho.index');
      //     }
      //   }else{
      //     Empenho::create($dados);
      //     return redirect()->route('empenho.index');
      //   }
      // }
    }

  }

  ////////////////////////////// DELETA OS EMPENHOS ///////////////////////////////////////////
  public function deletar($id)
  {
    $ac = Empenho::find($id);
    $tp = $ac->tipoempenho;

    if($tp == 1){
      if( Empenho::where('id_empenho',$id)->exists() ){
        return redirect()->back()->with('erro', "Esse empenho não pode ser deletado pôs há um reforço cadastrado para ele.");
      }else{
        if ( Pagamento::where('id_empenho', $id)->exists() ){
          return redirect()->back()->with('erro', "Esse empenho não pode ser deletado pôs já foi pago.");
        }else{
          $ac->delete();
          Log::create([
            'tabela' => 'empenhos',
            'idLinha' => $ac->id,
            'user' => Auth::user()->name,
            'acao' => 'deletar'
          ]);
          return redirect()->route('empenho.index');
        }
      }
    }else{
      if ( Pagamento::where('id_empenho', $id)->exists() ){
        return redirect()->back()->with('erro', "Esse empenho não pode ser deletado pôs já foi pago.");
      }else{
        $ac->delete();
        Log::create([
          'tabela' => 'empenhos',
          'idLinha' => $ac->id,
          'user' => Auth::user()->name,
          'acao' => 'deletar'
        ]);
        return redirect()->route('empenho.index');
      }
    }

  }

  // public function liquidar($id)
  // {
  //   $empenho = Empenho::find($id);
  //   return view('erp.empenho.liquidar' , compact('empenho'));
  // }

  // public function liquidarsalvar(Request $req , $id)
  // {
  //   $dados = $req->all();
  //   $atualizar = Empenho::find($id);
  //   if ($req->input('dt_liquidacao') < $atualizar->dataempenho){
  //     //dd('Data de liquidação deve ser posterior a data de empenho');
  //     return redirect()->back()->with('erro', "Data de liquidação deve ser posterior à data de empenho.");
  //   }else{
  //     //dd('ok');
  //     $atualizar->update($dados);
  //     return redirect()->route('empenho.index');
  //   }
  //
  // }


  public function valor($id)
  {
    $sub = Natureza::where('id_elemento', $id)->select('id','descricao','codnatureza')->get();
    return Response::json($sub);
  }

  public function ficha($id)
  {
    $sub = Acoes::where('id', $id)->get();
    foreach ($sub as $key => $value) {
      $grupo = $value->grupo->DescGrupo;
    }
    return Response::json($grupo);
  }

  public function porgrupo()
  {
    $orc = OrcamentoInicial::all();
    return view('erp.empenho.inicioporgrupo', compact('orc'));

  }

  public function porgrupoResult(Request $r)
  {
    $orc = $r->input('orc');
    //////////////////// select /////////////////////////////////////////////////////////////////////////////
    $e = Empenho::leftJoin('pagamentos', function ($join) {
      $join->on('empenhos.id', '=', 'pagamentos.id_empenho');
    })
    ->selectRaw('sum(pagamentos.valor_bruto) as total, empenhos.valorempenho, empenhos.id, empenhos.acaoempenho')
    ->groupBy('empenhos.id')->toSql();

    $contas = DB::table(DB::raw("($e) as a"))
    ->join('acoes', function ($join) {
      $join->on('acoes.id', '=', 'a.acaoempenho');
    })
    ->leftJoin('grupos', function ($join) {
      $join->on('grupos.id', '=', 'acoes.CodGrupAcao');
    })
    ->where('acoes.CodOrcamento',$orc)
    ->selectRaw('grupos.DescGrupo, sum(a.valorempenho) as total, sum(a.total) as totalPg')
    ->groupBy('grupos.DescGrupo')
    ->get();
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    $se = session()->put([
      'contas' => $contas
    ]);

    return view('erp.empenho.porgrupo' , compact('contas'));
  }

  public function imprimirPorgrupo()
  {
    $seconta = session()->get('contas');
    return view('erp.empenho.imprimirporgrupo', compact('seconta'));
  }

  public function porelementoResult(Request $r)
  {
    $orc = $r->input('orc');

    $e = Empenho::leftJoin('pagamentos', function ($join) {
      $join->on('empenhos.id', '=', 'pagamentos.id_empenho');
    })
    ->selectRaw('sum(pagamentos.valor_bruto) as total, empenhos.valorempenho, empenhos.id, empenhos.acaoempenho, empenhos.elemempenho, empenhos.subelemento')
    ->groupBy('empenhos.id')->toSql();

    $contas = DB::table(DB::raw("($e) as a"))
    ->join('acoes', function ($join) {
      $join->on('acoes.id', '=', 'a.acaoempenho');
    })
    ->join('elementos', function ($join) {
      $join->on('elementos.id', '=', 'a.elemempenho');
    })
    ->join('naturezas', function ($join) {
      $join->on('naturezas.id', '=', 'a.subelemento');
    })
    ->where('acoes.CodOrcamento',$orc)
    ->selectRaw('elementos.DescElemento, naturezas.descricao, elementos.CodElemento, naturezas.codnatureza, sum(a.valorempenho) as total, sum(a.total) as totalPg')
    ->orderBy('elementos.DescElemento', 'asc')
    ->groupBy('elementos.DescElemento', 'naturezas.descricao', 'elementos.CodElemento', 'naturezas.codnatureza')
    ->get();

    $seEl = session()->put([
      'contasE' => $contas
    ]);

    return view('erp.empenho.porelemento' , compact('contas'));
  }

  public function imprimirPorelem()
  {
    $seconta = session()->get('contasE');
    return view('erp.empenho.imprimirporelem', compact('seconta'));
  }

  public function porelemento()
  {
    $orc = OrcamentoInicial::all();
    return view('erp.empenho.inicioporelemento', compact('orc'));

  }

  public function showcotas()
  {
    $i = 0;
    $cotas = Empenho::selectRaw('acaoempenho, month(dataempenho) as mes, sum(valorempenho) as vl')
        ->groupBy(\DB::Raw('acaoempenho, month(dataempenho)'))
        ->get();
    foreach ($cotas as $key ) {
      $i = $key['vl']+$i;
    }
    $total = $i;
    return view('erp.empenho.showcotas', compact('cotas', 'total'));
  }

}
