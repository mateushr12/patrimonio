<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\ProcessaArquivo;
use App\ERP\TaxaRetorno;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\CssSelector\Parser\Reader;

class ProcessaArquivosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            //exibir geral
            $files = TaxaRetorno::all();
            return view('erp.processaarquivos.index', compact('files'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $processado = TaxaRetorno::where('status', '=', '0')->take(1)->get();

        //Existe arquivo para ser processado
            foreach ($processado as $key => $value){
                $id = $value['id'];
                $arquivo = $value['path'];
                $nomearquivo = $value['filename'];

                //$file = fopen('../storage/app/arquivosretorno/'.$arquivo, 'r');
                $file  = fopen(storage_path("app/public/arquivosretorno/$arquivo"),'r');

                while(! feof($file)) {
                    $line = fgets($file);
                    if(substr($line, 0, 1) == 'A'){

                        $DataPagamento = substr($line, 65, 4)."-".substr($line, 69, 2)."-".substr($line, 71, 2);

                    }
                    if(substr($line, 0, 1) == 'G'){

                        $NossoNumero = substr($line, 64, 17);
                        $ValorRecebido = substr($line, 81, 10);
                        $FormaArrecadacao = substr($line, 116, 1);

                    }

                    $request->request->add([
                        'nosso_numero' => $NossoNumero,
                        'valor' => $ValorRecebido,
                        'data_pagamento' => $DataPagamento,
                        'tipo' => $FormaArrecadacao,
                        'id_taxa_retorno' => $id
                      ]);
                    $dados = $request->all();
                    $files = ProcessaArquivo::create($dados);

                  }

                  fclose($file);

            }

            return view('erp.processaarquivos.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
