<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\TaxaRetorno;
use App\ERP\ProcessaArquivo;
use App\ERP\TaxaBanese;
use App\ERP\TipoArrecadacao;
use DateTime;
use DB;

class TaxaRetornosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //exibir geral
        $files = DB::table('file_entries')->orderBy('created_at', 'desc')->orderBy('filename', 'desc')->paginate(20);
        return view('erp.taxasretornos.index', compact('files'));
    }


    public function retorno(Request $req){

        $data = date('d/m/Y');

        $r = $req->session()->put([
          'nossonumero' => $req->input('nosso_numero'),
          'dtpagainicial' => $req->input('dt_paga_in'),
          'dtpagafinal' => $req->input('dt_paga_fl')
        ]);

        return redirect()->route('taxasRetorno.busca');
      }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){

    }


    public function exibir()
    {
        //exibir busca
        return view('erp.taxasretornos.exibir');
    }


    public function busca(Request $req)
    {
        $a = TaxaBanese::where(function($query) use($req) {
            if ($req->session()->has('nossonumero')) {
              $query->where('nosso_numero', 'like' ,'%'.trim($req->session()->get('nossonumero')).'%');
            }
            if ($req->session()->has('dtpagainicial') and $req->session()->has('dtpagafinal')) {
              $query->whereBetween('data_pagamento', [$req->session()->get('dtpagainicial'),$req->session()->get('dtpagafinal')]);
            }
        })->leftjoin('tipo_arrecadacao', 'tipo_arrecadacao.codigo', 'taxa_retornos.tipo');


        $buscas = $a->select('nosso_numero','valor','data_pagamento','tipo', 'tipo2', 'tipo3', 'descricao', 'valor_cobrado')
          ->distinct()
          ->orderBy('data_pagamento', 'asc')
          ->get();

        if(count($buscas) > 0){
            for ($i=0; $i < count($buscas); $i++) {
              $busca[] = [
                'nosso_numero' => $buscas[$i]['nosso_numero'],
                'data_pagamento' => $buscas[$i]['data_pagamento'],
                'tipo' => $buscas[$i]['tipo'],
                'tipo2' => $buscas[$i]['tipo2'],
                'tipo3' => $buscas[$i]['tipo3'],
                'descricao' => $buscas[$i]['descricao'],
                'valor_cobrado' => $buscas[$i]['valor_cobrado'],
                'valor' => $buscas[$i]['valor'] = (isset($buscas[$i+1]['nosso_numero']) and $buscas[$i]['nosso_numero'] == $buscas[$i+1]['nosso_numero']) ? '-' : $buscas[$i]['valor']
              ];
        }

        foreach ($busca as $ky => $vl) {
              $valor[] = isset($vl['valor']) ? $vl['valor'] : [];
        }

            $somaRec = array_sum($valor);
            $registros = count($valor);

        }else{
            $busca = [];
            $somaRec = 0.00;
            $registros = 0;
        }

        $re = session()->put([
            'busca' => $busca,
            'somaRec' => $somaRec,
            'registros' => $registros
        ]);



        return view('erp.taxasretornos.resultadoretorno', compact('busca','somaRec','registros'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //salvar
        $total = count($request->arqretorno);
        for ($i = 0; $i < $total; $i++){
          //dd($request->arqretorno[$i]->isValid());

        //Testa se o aruqivo enviado é valido
          if ($request->arqretorno[$i]->isValid()){

            $testenome = TaxaRetorno::where('filename', '=', $request->arqretorno[$i]->getClientOriginalName())->first();
            if($testenome === null){

              $nameFile = md5($request->arqretorno[$i]->getClientOriginalName()) . '.' . $request->arqretorno[$i]->extension();
              //dd($nameFile);
              $request->arqretorno[$i]->storeAs('arquivosretorno', $nameFile);
              $request->request->add([
                  'filename' => $request->arqretorno[$i]->getClientOriginalName(),
                  'mime' => $request->arqretorno[$i]->getClientMimeType(),
                  'path' => $nameFile,
                  'size' => $request->arqretorno[$i]->getClientSize(),
                  'status' => '0'
                ]);
              $dados = $request->all();
              $files = TaxaRetorno::create($dados);
              $testenome = null;
            }else{
              //dd($testenome);
              return redirect()->back()->with('erro', 'Já existe um arquivo de retorno cadastrado com esse nome.');
            }
          }
        }
        return redirect()->back();


    }



    public function processaArquivoSPC(Request $request, $id)
    {
        //

        $processado = TaxaRetorno::where('id', '=', $id)->get();


        $NossoNumero = null;
        $ValorRecebido = null;
        $FormaArrecadacao = null;
        $DataPagamento = null;

        //Existe arquivo para ser processado
            foreach ($processado as $key => $value){

                $arquivo = $value['path'];
                $nomearquivo = $value['filename'];

                //$file = fopen('../storage/app/arquivosretorno/'.$arquivo, 'r');
                $file  = fopen(storage_path("app/public/arquivosretorno/$arquivo"),'r');

                while(! feof($file)) {
                    $line = fgets($file);

                    //REGISTRO “A” – HEADER
                    if(substr($line, 0, 1) == 'A'){

                        $DataPagamento = substr($line, 65, 4)."-".substr($line, 69, 2)."-".substr($line, 71, 2);
                        if($DataPagamento == '0000-00-00'){
                          $DataPagamento = null;
                        }

                    }

                    //REGISTRO “G” – RETORNO DAS ARRECADAÇÕES IDENTIFICADAS COM CÓDIGO DE BARRAS
                    if(substr($line, 0, 1) == 'G'){

                       $NossoNumero = substr($line, 64, 17);
                       $ValorRecebido = substr($line, 81, 10);
                       $FormaArrecadacao = substr($line, 116, 1);

                       $request->request->add([
                        'nosso_numero' => $NossoNumero,
                        'valor' => $ValorRecebido,
                        'data_pagamento' => $DataPagamento,
                        'tipo' => $FormaArrecadacao,
                        'id_taxa_retorno' => $id
                        ]);
                        $dados = $request->all();
                        $files = ProcessaArquivo::create($dados);

                    }


                  }

                  fclose($file);
                  $atualiza = TaxaRetorno::find($id);
                  $atualiza->status = 1;
                  $atualiza->dataprocessamento = date('Y-m-d H:i:s');
                  $atualiza->save();

            }


            return redirect()->back();

    }


    public function processaArquivoCOB(Request $request, $id)
    {
        //

        $processado = TaxaRetorno::where('id', '=', $id)->get();


        $NossoNumero = null;
        $ValorRecebido = null;
        $FormaArrecadacao = null;
        $DataPagamento = null;

        //Existe arquivo para ser processado
            foreach ($processado as $key => $value){

                $arquivo = $value['path'];
                $nomearquivo = $value['filename'];

                //$file = fopen('../storage/app/public/arquivosretorno/'.$arquivo, 'r');
                $file  = fopen(storage_path("app/public/arquivosretorno/$arquivo"),'r');
                while(! feof($file)) {
                    $line = fgets($file);

                    //REGISTRO “T” – RETORNO DAS ARRECADAÇÕES IDENTIFICADAS
                    if(substr($line, 13, 1) == 'T'){

                        $NossoNumero = substr($line, 48, 9);
                        $FormaArrecadacao01 = substr($line, 213, 2);
                        $FormaArrecadacao02 = substr($line, 215, 2);
                        $FormaArrecadacao03 = substr($line, 217, 2);

                    }

                    //REGISTRO “U” – RETORNO DAS ARRECADAÇÕES IDENTIFICADAS
                    if(substr($line, 13, 1) == 'U'){

                        $ValorRecebido = substr($line, 80, 10);
                        $DataPagamento = substr($line, 149, 4)."-".substr($line, 147, 2)."-".substr($line, 145, 2);
                        if($DataPagamento == '0000-00-00'){
                          $DataPagamento = null;
                        }
                        $request->request->add([
                            'nosso_numero' => $NossoNumero,
                            'valor' => $ValorRecebido,
                            'data_pagamento' => $DataPagamento,
                            'tipo' => $FormaArrecadacao01,
                            'tipo2' => $FormaArrecadacao02,
                            'tipo3' => $FormaArrecadacao03,
                            'id_taxa_retorno' => $id
                        ]);
                        $dados = $request->all();
                        $files = ProcessaArquivo::create($dados);

                    }


                  }

                  fclose($file);
                    $atualiza = TaxaRetorno::find($id);
                    $atualiza->status = 1;
                    $atualiza->dataprocessamento = date('Y-m-d H:i:s');
                    $atualiza->save();
            }


            return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //exibir
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //editar
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //atualizar
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //apagar
    }


    public function buscarpordia()
    {
      $mesano = TaxaBanese::whereRaw('date_format(data_pagamento, "%m/%Y") is not null')
      ->groupBy(\DB::raw('date_format(data_pagamento, "%m/%Y")'))
      ->selectRaw('date_format(data_pagamento, "%m/%Y") as mesano')
      ->get();
      //dd($mesano);
      return view('erp.taxasretornos.buscarpordia', compact('mesano'));
    }

    public function resultadopordia(Request $r)
    {
        $ma = $r->input('mesano');

        $busca = TaxaBanese::whereRaw("date_format(data_pagamento, '%m/%Y') = '$ma' ")
        ->groupBy(\DB::raw('date_format(data_pagamento, "%d/%m/%Y")'))
        ->selectRaw('date_format(data_pagamento, "%d/%m/%Y") as dia, sum(valor) as soma')
        ->orderBy('dia', 'asc')
        ->get();

        $total = [];
        foreach ($busca as $key => $vl ) {
          $total[] = $vl['soma'];
        }
        $tt = array_sum($total);


        return view('erp.taxasretornos.resultadopordia', compact('busca', 'ma', 'tt'));

    }


    public function uploadFile(Request $request) {
        $file = Input::file('file');
        $filename = $file->getClientOriginalName();

        $path = hash( 'sha256', time());

        if(Storage::disk('uploads')->put($path.'/'.$filename,  File::get($file))) {
            $input['filename'] = $filename;
            $input['mime'] = $file->getClientMimeType();
            $input['path'] = $path;
            $input['size'] = $file->getClientSize();
            $file = FileEntry::create($input);

            return response()->json([
                'success' => true,
                'id' => $file->id
            ], 200);
        }
        return response()->json([
            'success' => false
        ], 500);
    }


}
