<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\Log;
use App\ERP\Liquidado;
use App\ERP\Empenho;
use App\ERP\Pagamento;
use Response;
use Form;
use Auth;

class LiquidarController extends Controller
{
    //
    public function liquidar($id)
    {
      $empenho = Empenho::find($id);
      $liquidados = Liquidado::where('id_empenho',$id)->get();
      $vliquidado = Liquidado::where('id_empenho',$id)->sum('valor');

      return view('erp.empenho.liquidar' , compact('empenho','vliquidado','liquidados'));

    }


    public function liquidarsalvar(Request $req , $id)
    {
      $req->merge(['id_empenho' => $id]);
      $dados = $req->all();
      $atualizar = Empenho::find($id);
      $vliquidado = Liquidado::where('id_empenho',$id)->sum('valor');

      if ($req->input('data') < $atualizar->dataempenho){
        //dd('Data de liquidação deve ser posterior a data de empenho');
        return redirect()->back()->with('erro', "Data de liquidação deve ser posterior à data de empenho.");
      }elseif ($req->input('valor') > ($atualizar->valorempenho - $vliquidado)) {
        //dd('Valor não permitido');
        return redirect()->back()->with('erro', "Valor não permitido.");
      }else{
        if ( ($req->input('valor') + $vliquidado) == $atualizar->valorempenho ){
          //dd(2);
          $atualizar->update(['liquidado' => 2]);
        }elseif ($req->input('valor') == 0.00) {
          //dd('Valor não pode ser zero.');
          return redirect()->back()->with('erro', "Valor não pode ser zero.");
        }else{
          //dd(3);
          $atualizar->update(['liquidado' => 3]);
        }
        Liquidado::create($dados);
        return redirect()->route('liquidar',$id);
      }

    }

    public function deletar($id)
    {
      $lq = Liquidado::find($id);

      if( Pagamento::where('id_empenho',$lq->id_empenho)->exists() ){
          //dd('Não pode deletar, ja pago');
          return redirect()->back()->with('erro', "Erro ao tentar deletar, empenho já pago.");
      }else{
          $lq->delete();
          Log::create([
            'tabela' => 'liquidados',
            'idLinha' => $lq->id,
            'user' => Auth::user()->id,
            'acao' => 'deletar'
          ]);
          return redirect()->back();
          //dd('Delete');
      }

    }


}
