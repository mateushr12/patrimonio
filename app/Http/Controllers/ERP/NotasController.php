<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\Taxas;
use App\ERP\Log;
use Auth;

class NotasController extends Controller
{

    public function inicio()
    {
      $tpevento = Taxas::select('tipo_evento_redesim')->distinct()->get();
      $natureza = Taxas::select('natureza_juridica')->distinct()->get();
      return view('erp.', compact('tpevento','natureza'));
    }

    public function resultado(Request $req)
    {
      $data = date('d/m/Y');

     $req->request->add([
        'protocolo' => '',
        'cpf_solicitante' => '',
        'cnpj_solicitante' => '',
        'nosso_numero' => '',
        'cd_documento' => '',
        'data_emissao_i' => '2019-12-01',
        'data_emissao_f' => '2019-12-02',
        'data_pag_i' => '2019-12-01',
        'data_pag_f' => '2019-12-03',
        'tipo_evento_redesim' => ''
      ]);


      $a = Taxas::where(function($query) use($req) {
        if ($req->filled(['protocolo'])) {
          $query->where('protocolo', 'like' ,'%'.trim($req->input('protocolo')).'%');
        }
        if ($req->filled(['cpf_solicitante'])) {
          $query->where('cpf_solicitante', trim($req->input('cpf_solicitante')));
        }
        if ($req->filled(['cnpj_solicitante'])) {
          $query->where('cnpj_solicitante', trim($req->input('cnpj_solicitante')));
        }
        if ($req->filled(['nosso_numero'])) {
          $query->where('nosso_numero', 'like' ,'%'.trim($req->input('nosso_numero')).'%');
        }
        if ($req->filled(['cd_documento'])) {
          $query->where('cd_documento', trim($req->input('cd_documento')));
        }
        if ($req->filled(['data_emissao_i','data_emissao_f'])) {
          $query->whereBetween('dt_emissao', [$req->input('data_emissao_i'),$req->input('data_emissao_f')]);
        }
        if ($req->filled(['data_pag_i','data_pag_f'])) {
          $query->whereBetween('dt_pagamento', [$req->input('data_pag_i'),$req->input('data_pag_f')]);
        }
        if ($req->filled(['tipo_evento_redesim'])) {
          $query->where('tipo_evento_redesim', $req->input('tipo_evento_redesim'));
        }
      });

      $acoes = $a->select('protocolo','cpf_solicitante','cnpj_solicitante','nosso_numero','cd_documento','dt_emissao','dt_vencimento','dt_pagamento','tipo_evento_redesim','vl_recebido')
          ->distinct()
          //->get();
          ->paginate(50);
      //dd($acoes);
      $v = [];
      foreach ($acoes as $key => $value) {
        $v[] = $value['vl_recebido'];
      }

      $somaRec = array_sum($v);
      $registros = count($v);

      dd($acoes, $registros, $somaRec);
      //dd($somaRec);
      //dd($registros);

      return view('erp.', compact('acoes','data','somaRec'));

    }

    public function detalhe($protocolo)
    {
      //set_time_limit(300);
      $eventos = Taxas::where('protocolo',$protocolo)->get();
      $nota = Taxas::where('protocolo',$protocolo)->first();
      $data = date('d/m/Y');
      return view('erp.', compact('nota','data', 'eventos'));
      // return PDF::loadView('erp.empenho.pdf', compact('empenho'))
      //             // Se quiser que fique no formato a4 retrato: ->setPaper('a4', 'landscape')
      //             ->download('empenho.pdf');
      //return PDF::loadView('erp.empenho.pdf', compact('empenho'))->stream();

    }

}
