<?php

namespace App\Http\Controllers\ERP;

use App\ERP\ContratoPresi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERP\Log;
use App\ERP\TipoTermo;
use App\ERP\AditivoPres;
use App\ERP\AnexosContratoPres;


use Auth;
use Illuminate\Support\Facades\Storage;
use DB;


class ContratoPresiController extends Controller
{
    public function index()
    {
        //$contratos = ContratoPresi::all();
        $tipo = TipoTermo::all();
        $contratos = ContratoPresi::leftJoin('aditivos_presidencia', function ($join) {
          $join->on('contratos_presidencia.id', '=', 'aditivos_presidencia.id_contrato_pres')->whereNull('aditivos_presidencia.deleted_at');
        })
        ->selectRaw('contratos_presidencia.id, contratos_presidencia.n_contrato ,contratos_presidencia.desricao ,contratos_presidencia.dt_inicio ,contratos_presidencia.dt_fim ,
        contratos_presidencia.tipo, contratos_presidencia.dt_publicacao_diario,
        contratos_presidencia.dt_assinatura, max(aditivos_presidencia.vigencia_f) as vigencia')
        ->groupBy('contratos_presidencia.id', 'contratos_presidencia.n_contrato' ,'contratos_presidencia.desricao' ,'contratos_presidencia.dt_inicio' ,'contratos_presidencia.dt_fim' ,
        'contratos_presidencia.tipo', 'contratos_presidencia.dt_publicacao_diario',
        'contratos_presidencia.dt_assinatura')
        ->havingRaw('contratos_presidencia.dt_fim > now() or max(aditivos_presidencia.vigencia_f) > now()')
        ->get();

        return view ('erp.contratosPresi.index' , compact('contratos','tipo'));
    }

    public function encerrados()
    {

        $tipo = TipoTermo::all();
        $contratos = ContratoPresi::leftJoin('aditivos_presidencia', function ($join) {
          $join->on('contratos_presidencia.id', '=', 'aditivos_presidencia.id_contrato_pres')->whereNull('aditivos_presidencia.deleted_at');
        })
        ->selectRaw('contratos_presidencia.id, contratos_presidencia.n_contrato ,contratos_presidencia.desricao ,contratos_presidencia.dt_inicio ,contratos_presidencia.dt_fim ,
        contratos_presidencia.tipo, contratos_presidencia.dt_publicacao_diario,
        contratos_presidencia.dt_assinatura, max(aditivos_presidencia.vigencia_f) as vigencia')
        ->groupBy('contratos_presidencia.id', 'contratos_presidencia.n_contrato' ,'contratos_presidencia.desricao' ,'contratos_presidencia.dt_inicio' ,'contratos_presidencia.dt_fim' ,
        'contratos_presidencia.tipo', 'contratos_presidencia.dt_publicacao_diario',
        'contratos_presidencia.dt_assinatura')        
        ->havingRaw('contratos_presidencia.dt_fim < now() and (max(aditivos_presidencia.vigencia_f) is null or max(aditivos_presidencia.vigencia_f) < now())')
        ->get();

        return view ('erp.contratosPresi.encerrados' , compact('contratos','tipo'));
    }

    public function ver($id)
    {
        $contrato = ContratoPresi::find($id);
        $aditivo =  AditivoPres::where('id_contrato_pres',$id)->get();
        return view ('erp.contratosPresi.ver' , compact('contrato','aditivo'));
    }

    public function salvar(Request $req)
    {
      try
      {
        $dados = $req->all();
        if ($req->input('dt_inicio') >= $req->input('dt_fim')){
          return back()->with('erro', 'Data de inicio da vigência não pode ser igual nem maior que a data final da vigência');
        }
        else{
          $contrato = ContratoPresi::create($dados);
          Log::create([
            'tabela' => 'contratos_presidencia',
            'idLinha' => $contrato->id,
            'user' => Auth::user()->name,
            'acao' => 'salvar'
          ]);
          return redirect()->route('contratosPres');
        }
      }
      catch(\Illuminate\Database\QueryException $e)
      {
        return redirect()->back()->with('erro', 'Não foi possivel cadastrar o contrato. ERRO:'.$e->getMessage());
      }
      catch(PDOException $e)
      {
        return redirect()->back()->with('erro', 'Algo errado aconteceu, tente novamente!');
      }
    }

    public function editar($id)
    {
        $registro = ContratoPresi::find($id);
        $tipo = TipoTermo::all();
        return view('erp.contratosPresi.editar' , compact('registro', 'tipo'));
    }

    public function atualizar(Request $req, $id)
    {
        $dados = $req->all();
        $atualizar = ContratoPresi::find($id);
        $atualizar->update($dados);
        Log::create([
          'tabela' => 'contratos_presidencia',
          'idLinha' => $atualizar->id,
          'user' => Auth::user()->name,
          'acao' => 'atualizar'
        ]);
        return redirect()->route('contratosPres');
    }

    public function deletar($id)
    {
      $ac = ContratoPresi::find($id);
      $ad = AditivoPres::where('id_contrato_pres', $ac->id)->exists();
      // $anx = AnexosContratoPres::where('id_contrato_pres', $ac->id)->exists();

      if (isset($ac) && !$ad ){
          $ac->delete();
          Log::create([
            'tabela' => 'contratos_presidencia',
            'idLinha' => $ac->id,
            'user' => Auth::user()->name,
            'acao' => 'deletar'
          ]);
          return redirect()->route('contratosPres');
      }else{
        return back()->with('erro', 'Não é possível deletar esse contrato pôs ele contém aditivo ou anexos.');
      }
    }

    ///////////////////////////// anexo ////////////////////////////////////////
    public function anexar($id)
    {
      $arquivo = AnexosContratoPres::where('id_contrato_pres', $id)->get();
      $contrato = $id;
      return view('erp.contratosPresi.anexar' , compact('arquivo', 'contrato'));
    }


    public function anexarSalvar(Request $req)
    {
      $idcontrato = $req->input('id');
      $nomereq = $req->file('arquivo')->store('contratosPresidencia');
      $contrato = AnexosContratoPres::create([
        'id_contrato_pres' => $req->input('id'),
        'descricao' => $req->input('descricao'),
        'arquivo' => $nomereq
      ]);
      Log::create([
        'tabela' => 'anexos_contratopres',
        'idLinha' => $contrato->id,
        'user' => Auth::user()->name,
        'acao' => 'salvar'
      ]);
      return redirect()->back();
    }

    public function pdf($id)
    {
      try {
        $arquivo = AnexosContratoPres::find($id);
        $narq = $arquivo->arquivo;
        return response()->file(storage_path("app/public/$narq"));

      } catch (\Exception $e) {
        return view('notfound');
      }
    }

    public function deletepdf($id)
    {
      $arquivo = AnexosContratoPres::find($id);
      $narq = $arquivo->arquivo;

      unlink(storage_path("app/public/$narq"));
      AnexosContratoPres::find($id)->delete();
      Log::create([
        'tabela' => 'anexos_contratopres',
        'idLinha' => $id,
        'user' => Auth::user()->name,
        'acao' => 'deletar'
      ]);
      return redirect()->back();
    }
    ///////////////////////////////////////////////////////////////////////////

    // public function inicioRelatorio()
    // {
    //   $fornecedores = Fornecedores::all();
    //   return view('erp.contratos.buscar', compact('fornecedores'));
    // }

    // public function resultRelatorio(Request $req)
    // {
    //   $data = date('d/m/Y');
    //   $e = Contrato::leftJoin('aditivos', function ($join) {
    //     $join->on('aditivos.id_contrato', '=', 'contratos.id');
    //   })
    //   ->selectRaw('sum(aditivos.valor) as total, contratos.*')
    //   ->groupBy('contratos.id')->toSql();
    //
    //   $contrato = DB::table(DB::raw("($e) as a"))
    //   ->leftJoin('empenhos', function ($join) {
    //     $join->on('empenhos.licempenho', '=', 'a.id')->whereNull('empenhos.deleted_at');
    //   })
    //   ->leftJoin('fornecedores', function ($join) {
    //     $join->on('a.fornecedorcontratado', '=', 'fornecedores.id');
    //   })
    //   ->selectRaw('a.*, sum(empenhos.valorempenho) as total, sum(a.total) as totalPg, fornecedores.nome, year(empenhos.dataempenho) as ano')
    //   ->where(function($query) use($req) {
    //     if ($req->filled(['numerocontrato'])) {
    //       $query->where('numerocontrato', 'like', '%'.$req->input('numerocontrato').'%');
    //     }
    //     if ($req->filled(['licitacao'])) {
    //       $query->where('licitacao', 'like', '%'.$req->input('licitacao').'%');
    //     }
    //     if ($req->filled(['empenho'])) {
    //       $query->where('empenho', 'like', '%'.$req->input('empenho').'%');
    //     }
    //     if ($req->filled(['fornecedorcontratado'])) {
    //       $query->where('fornecedorcontratado', $req->input('fornecedorcontratado'));
    //     }
    //   })
    //   ->groupBy(DB::raw('a.id, year(empenhos.dataempenho)'))
    //   ->get();
    //
    //   if(count($contrato) > 0){
    //     for ($i=0; $i < count($contrato); $i++) {
    //       $totalC[] = (isset($contrato[$i-1]->licitacao) and $contrato[$i]->licitacao == $contrato[$i-1]->licitacao) ? '-' : ($contrato[$i]->valorcontrato + $contrato[$i]->totalPg);
    //     }
    //     $totalC = array_sum($totalC);
    //   }else{
    //     $totalC = null;
    //   }
    //
    //   return view('erp.contratos.imprimir', compact('contrato','data','totalC'));
    // }

    ///////////////// tipo de termo ///////////////////////////////////////////
    public function termoIndex()
    {
        $termo = TipoTermo::all();
        return view ('erp.contratosPresi.tipoTermo.index' , compact('termo'));
    }

    public function termoSalvar(Request $req)
    {
      try
      {
        $dados = $req->all();
        $contrato = TipoTermo::create($dados);
        return redirect()->route('termo');
      }
      catch(\Illuminate\Database\QueryException $e)
      {
        return redirect()->back()->with('erro', 'Não foi possivel cadastrar o contrato. ERRO:'.$e->getMessage());
      }
      catch(PDOException $e)
      {
        return redirect()->back()->with('erro', 'Algo errado aconteceu, tente novamente!');
      }
    }

    public function termoDeletar($id)
    {
      $ac = TipoTermo::find($id);
      $cont = ContratoPresi::where('tipo', $id)->exists();

      if (isset($ac) && !$cont){
          $ac->delete();
          return redirect()->route('termo');
      }else{
        return back()->with('erro', 'Não é possível deletar esse tipo pôs ele está sendo usado em um contrato.');
      }

    }
    ///////////////////////////////////////////////////////////////////////////

    /////////////////////////// aditivo ////////////////////////////////////////
    public function aditivoIndex($id)
    {
        $aditivos = AditivoPres::where('id_contrato_pres', $id)->get();
        $contrato = $id;
        $con = ContratoPresi::where('id', $id)->get();
        foreach ($con as $key => $value) {
          $n_contrato = $value['n_contrato'];
        }
        return view('erp.contratosPresi.aditivo' , compact('aditivos', 'contrato', 'n_contrato'));
    }

    public function aditivoSalvar(Request $req)
    {
        $dados = $req->all();
        $contrato = AditivoPres::create($dados);
        Log::create([
          'tabela' => 'aditivos_presidencia',
          'idLinha' => $contrato->id,
          'user' => Auth::user()->name,
          'acao' => 'salvar'
        ]);
        return redirect()->back();
    }

    public function aditivoDeletar($id)
    {
        $ac = AditivoPres::find($id);
        $ac->delete();
        Log::create([
          'tabela' => 'aditivos_presidencia',
          'idLinha' => $id,
          'user' => Auth::user()->name,
          'acao' => 'deletar'
        ]);
        return redirect()->route('contratosPres');

    }
    ///////////////////////////////////////////////////////////////////////////



}
