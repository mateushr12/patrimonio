<?php
// ================================================================================================
function array_orderby()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($data as $key => $row)
                    $tmp[$key] = $row[$field];
                $args[$n] = $tmp;
                }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }
// ================================================================================================
function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;

        if ( isset( $val ) and $val != null )
        {
            for($i = 0; $i<=strlen($mask)-1; $i++)
            {
                if($mask[$i] == '#')
                {
                    if(isset($val[$k]))
                        $maskared .= $val[$k++];
                }
                else
                {
                    if(isset($mask[$i]))
                        $maskared .= $mask[$i];
                }
            }
        }
        return $maskared;
    }
// ================================================================================================
function dataBR($val)
    {
        // 2019-05-24 15:26:30
        // 0123456789012345678
        $dataformatada = '';
        $dataformatada = substr($val, 8, 2).'/'.substr($val, 5, 2).'/'.substr($val, 0, 4);
        return $dataformatada;
    }
// ================================================================================================
function dataInvertida($val)
    {
        // 2019-05-24 15:26:30
        // 24-05-2019 15:26:30
        // 0123456789012345678
        $dataformatada = '';
        $dataformatada = substr($val, 6, 4).'-'.substr($val, 3, 2).'-'.substr($val, 0, 2);
        return $dataformatada;
    }
// ================================================================================================
function horaBR($val)
    {
        // 2019-05-24 15:26:30
        // 0123456789012345678
        $dataformatada = '';
        $dataformatada = substr($val, 11, 2).':'.substr($val, 14, 2).':'.substr($val, 17, 2);
        return $dataformatada;
    }
// ================================================================================================
function novoProtocolo()
    {
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        $d = new DateTime( date('d-m-Y H:i:s.'.$micro, $t) );
        $d = $d->format("d-m-Y H:i:s.u");

        // 2019-05-24 15:26:30.069509
        // 24-05-2019 15:26:30.069509
        // 01234567890123456789012345

        // 20190524152630069509

        $dataformatada = '';
        $dataformatada = substr( $d, 6, 4 ).
            substr( $d, 3, 2 ).
            substr( $d, 0, 2 ).
            substr( $d, 11, 2 ).
            substr( $d, 14, 2 ).
            substr( $d, 17, 2 ).
            substr( $d, 20, 6 );
        // dd( 't: ' . $t, 'micro: ' . $micro, 'd: ' . $d, 'Data Formatada: ' . $dataformatada );
        return $dataformatada;
    }
// ================================================================================================
function dataDiaSemana($data)
    {
        $diasemana = array('Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sabado');
        $diasemanacurto = array('dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sab');

        $diasemana_numero = date('w', strtotime($data));

        return $diasemana[$diasemana_numero];

        // $limite = 86400; // 24 horas x 60 minutos x 60 segundos
        // $strDias = '';

        // if ( $segundosTotal > $limite )
        // {
        //     $strDias = (string)(int)( $segundosTotal / $limite ) . ' dia(s)';
        //     $segundosTotalAlemLimite = $segundosTotal % $limite;
        // }

        // if ( $strDias != '' )
        // {
        //     $retorno = $strDias . ' + ' . gmdate( "H:i:s", $segundosTotalAlemLimite );
        // }
        // elseif ( $strDias == '' )
        // {
        //     $retorno = gmdate( "H:i:s", $segundosTotal );
        // }

        // if ( $segundosTotal == 0 )
        // {
        //     return 'vazio';
        // }
        // else
        // {
        //     return $retorno;
        // }
    }
// ================================================================================================
function horaAnormal($segundosTotal)
    {
        $limite = 86400; // 24 horas x 60 minutos x 60 segundos
        $strDias = '';

        if ( $segundosTotal > $limite )
        {
            $strDias = (string)(int)( $segundosTotal / $limite ) . ' dia(s)';
            $segundosTotalAlemLimite = $segundosTotal % $limite;
        }

        if ( $strDias != '' )
        {
            $retorno = $strDias . ' + ' . gmdate( "H:i:s", $segundosTotalAlemLimite );
        }
        elseif ( $strDias == '' )
        {
            $retorno = gmdate( "H:i:s", $segundosTotal );
        }

        if ( $segundosTotal == 0 )
        {
            return 'vazio';
        }
        else
        {
            return $retorno;
        }
    }
// ================================================================================================
function retirarMascaraCPF($cpf)
    {
        $CPFSemMascara = $cpf;

        $CPFSemMascara = str_replace( '.', '', $CPFSemMascara );
        $CPFSemMascara = str_replace( '-', '', $CPFSemMascara );

        return $CPFSemMascara;
    }
// ================================================================================================
function contemLetra($texto)
    {
        $nContador = 1;
        $resultado = false;
        $array = array( 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'w', 'y', 'z' );
    
        for ( $i = 1; $i <= strlen( $texto ); $i++ )
        {
            if ( in_array( strtolower( substr( $texto, $i, 1 ) ), $array ) )
            {
                $resultado = true;
                return $resultado;
            }
        }
    
        return $resultado;
    }
// ================================================================================================
function seTemLetra( $texto )
    {
        $arrayLetra = array( 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'w', 'y', 'z',
                            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'W', 'Y', 'Z' );

        for ( $i = 1; $i <= strlen( $texto ); $i++ )
        {
            if ( substr( $texto, $i, 1 ) <> '' )
            {
                $letra = false;

                if ( in_array( substr( $texto, $i, 1 ), $arrayLetra ) )
                {
                    $letra = true;
                }

                if ( $letra == true )
                {
                    return true;
                }
            }
        }
    
        return false;
    }
// ================================================================================================
function retiraLetras( $texto )
    {
        $arrayLetra = array( 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'w', 'y', 'z',
                            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'W', 'Y', 'Z' );

        $novotexto = '';
        if ( $texto != '' )
        {
            for ( $i = 0; $i <= strlen( $texto )+1; $i++ )
            {
                if ( substr( $texto, $i, 1 ) != '' )
                {
                    if ( !in_array( substr( $texto, $i, 1 ), $arrayLetra ) )
                    {
                        $novotexto = $novotexto . substr( $texto, $i, 1 );
                        // dd( $texto, substr( $texto, $i, 1 ), strlen( $texto ) );
                    }
                }
            }
        }
    
        return $novotexto;
    }
// ================================================================================================
function seTodasSaoLetras( $texto )
    {
        $arrayLetra = array( 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'w', 'y', 'z',
                            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'W', 'Y', 'Z' );

        $todasSaoLetras = true;

        for ( $i = 1; $i <= strlen( $texto ); $i++ )
        {
            if ( substr( $texto, $i, 1 ) <> '' )
            {
                if ( !in_array( substr( $texto, $i, 1 ), $arrayLetra ) )
                {
                    $todasSaoLetras = false;
                }

                if ( $todasSaoLetras == false )
                {
                    return false;
                }
            }
        }
    
        return true;
    }
// ================================================================================================
function retirarMascaraTelefone($numero)
    {
        $TelefoneSemMascara = $numero;

        $TelefoneSemMascara = str_replace( '(', '', $TelefoneSemMascara );
        $TelefoneSemMascara = str_replace( ')', '', $TelefoneSemMascara );
        $TelefoneSemMascara = str_replace( ' ', '', $TelefoneSemMascara );
        $TelefoneSemMascara = str_replace( '-', '', $TelefoneSemMascara );

        return $TelefoneSemMascara;
    }
// ================================================================================================
function datahoraBR($val)
    {
        // 2019-05-24 15:26:30
        // 0123456789012345678
        $dataformatada = '';
        if ( substr($val, 11, 2) == '' )
            $dataformatada = substr($val, 8, 2).'/'.substr($val, 5, 2).'/'.substr($val, 0, 4);
        else
            $dataformatada = substr($val, 8, 2).'/'.substr($val, 5, 2).'/'.substr($val, 0, 4).'  '.substr($val, 11, 2).':'.substr($val, 14, 2).':'.substr($val, 17, 2);

        if ( substr($val, 8, 2) == '' )
            return '';
        else
            return $dataformatada;
    }
// ================================================================================================
function validaCPF($cpf) 
    {
                // Extrai somente os números
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );
        
        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }
        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                return false;
            }
        }
        return true;
    }
// ================================================================================================
function meses( $numeroMes )
    {
        $Meses = [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ];
        return $Meses[ $numeroMes ];
    }
// ================================================================================================
function calcularTotaisArrayComplexo( $array, $arrayCampos )
    {
        $arrayCamposInformados = $arrayCampos;
        $arrayCamposInformadosTemp = $arrayCampos;
        $arrayInformada = $array;
        $nTotalItens = 0;

        foreach ( $arrayInformada as $keyItem => $itemValue )
        { 
            $arrayInformada[ $keyItem ]->item = $keyItem+1; 

            $nTotalItens = $nTotalItens + 1; 

            foreach ( $arrayCamposInformados as $keyItem2 => $itemValue2 )
            { 
                if ( $keyItem2 > 0 )
                {
                    $nome1 = $arrayCamposInformados[$keyItem2][0];
                    $nome2 = $arrayInformada[$keyItem]->$nome1;
                    $arrayCamposInformados[ $keyItem2 ][1] = $arrayCamposInformados[ $keyItem2 ][1] + $nome2;
                }
            }
        }

        $arrayCamposInformados[0][1] = $nTotalItens;

        foreach ( $arrayInformada as $keyItem => $itemValue )
        { 
            $arrayInformada[ $keyItem ]->totalitens = $nTotalItens;
            foreach ( $arrayCamposInformados as $keyItem2 => $itemValue2 )
            { 
                if ( $keyItem2 > 0 )
                {
                    $nome1 = $arrayCamposInformados[$keyItem2][0];
                    $nome2 = $arrayInformada[$keyItem]->$nome1;
                    $nome3 = 'total' . $arrayCamposInformados[$keyItem2][0];
                    $arrayInformada[ $keyItem ]->$nome3 = $arrayCamposInformados[ $keyItem2 ][1];
                    $nome4 = 'percentual' . $arrayCamposInformados[$keyItem2][0];
                    if ( $arrayCamposInformados[ $keyItem2 ][1] > 0 )
                    {
                        $arrayInformada[ $keyItem ]->$nome4 = ( $nome2 / $arrayCamposInformados[ $keyItem2 ][1] ) * 100;
                    }
                    else
                    {
                        $arrayInformada[ $keyItem ]->$nome4 = 0;
                    }
                }
            }
        }

        foreach ( $arrayInformada as $keyItem => $itemValue )
        { 
        }

        return $arrayInformada;
    }
// ================================================================================================

