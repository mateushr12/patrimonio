<?php

namespace App\Patrimonio;

use Illuminate\Database\Eloquent\Model;

class Setor extends Model
{
    //
    protected $fillable = [
      'descricao', 'tp_setor'
    ];

    protected $table = 'setor';

    protected $connection = 'mysqlPatrimonio';

    public $timestamps = false;
}
