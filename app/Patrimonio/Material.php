<?php

namespace App\Patrimonio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Material extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
      'descricao', 'n_serie', 'patrimonio', 'obs', 'id_conservacao', 'id_proprietario', 'id_setor_atual', 'id_tipo_material', 'id_usuario'
    ];

    protected $table = 'material';

    protected $connection = 'mysqlPatrimonio';

    public function conservacao()
    {
        return $this->hasOne('App\Patrimonio\Conservacao', 'id', 'id_conservacao');
    }

    public function proprietario()
    {
        return $this->hasOne('App\Patrimonio\Proprietario', 'id', 'id_proprietario');
    }

    public function setor()
    {
        return $this->hasOne('App\Patrimonio\Setor', 'id', 'id_setor_atual');
    }

    public function tipoMaterial()
    {
        return $this->hasOne('App\Patrimonio\TipoMaterial', 'id', 'id_tipo_material');
    }

    public function usuario()
    {
        return $this->hasOne('App\User', 'id', 'id_usuario');
    }
}
