<?php

namespace App\Patrimonio;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
     //
     protected $fillable = [
        'idLinha', 'nome_user', 'acao', 'tabela'
      ];
  
      protected $table = 'log';
      protected $connection = 'mysqlPatrimonio';
  
     
}
