<?php

namespace App\Patrimonio;

use Illuminate\Database\Eloquent\Model;

class TipoMaterial extends Model
{
    //
    protected $fillable = [
      'descricao'
    ];

    protected $table = 'tipo_material';

    protected $connection = 'mysqlPatrimonio';

    public $timestamps = false;
}
