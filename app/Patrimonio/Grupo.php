<?php

namespace App\Patrimonio;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    protected $table = 'grupo';

    protected $fillable = [
        'id',
        'grupo'
    ];
    
    protected $primaryKey = 'id';
           
    protected $connection = 'mysqlPatrimonio';
}
