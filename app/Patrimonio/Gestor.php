<?php

namespace App\Patrimonio;

use Illuminate\Database\Eloquent\Model;

class Gestor extends Model
{
    protected $table = 'gestor';

    protected $fillable = [
        'id',
        'id_gestor',
        'descricao'
    ];

    protected $guarded = [
        'id',
        'created_at',
        'update_at',
        'deleted_at'
    ];

    protected $primaryKey = 'id';

    public function grupouser()
    {
        return $this->hasOne('App\Gruposuser', 'id', 'id_gestor');
    }

    public function grupos()
    {
        return $this->hasOne('App\Patrimonio\Grupo', 'id', 'id_grupo');
    }
        
    protected $connection = 'mysqlPatrimonio';

}
