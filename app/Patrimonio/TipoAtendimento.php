<?php

namespace App\Patrimonio;

use Illuminate\Database\Eloquent\Model;

class TipoAtendimento extends Model
{
    protected $table = 'tp_atendimento';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'descricao'
    ];

    
    protected $connection = 'mysqlPatrimonio';
}
