<?php

namespace App\Patrimonio;

use Illuminate\Database\Eloquent\Model;

class Atendimentos extends Model
{
    protected $table = 'atendimento';

    protected $fillable = [
        'id_tipo',
        'id_categoria',
        'id_status',
        'id_requerente',
        'atribuido_para',
        'autorizado_por',
        'titulo',
        'descricao',
        'urgencia',
        'dt_abertura',
        'dt_fechamento',
        'dt_andamento',
        'solucao',
        'solucionado_por',
        'fechado_por'
    ];

    protected $guarded = [
        'id',
        'created_at',
        'update_at',
        'deleted_at'
    ];

    protected $primaryKey = 'id';

    public function status()
    {
        return $this->hasOne('App\Patrimonio\Status', 'id', 'id_status');
    }

    public function categoria()
    {
        return $this->hasOne('App\Patrimonio\Categoria', 'id', 'id_categoria');
    }

    public function tipo()
    {
        return $this->hasOne('App\Patrimonio\TipoAtendimento', 'id', 'id_tipo');
    }

    public function requerente()
    {
        return $this->hasOne('App\User', 'id', 'id_requerente');
        
    }

    public function atribuido()
    {
        return $this->hasOne('App\User', 'id', 'atribuido_para');
        
    }

    public function autorizado()
    {
        return $this->hasOne('App\User', 'id', 'autorizado_por');
        
    }

    public function solucionado()
    {
        return $this->hasOne('App\User', 'id', 'solucionado_por');
        
    }

    public function fechado()
    {
        return $this->hasOne('App\User', 'id', 'fechado_por');
        
    }

    
    protected $connection = 'mysqlPatrimonio';
}
