<?php

namespace App\Patrimonio;

use Illuminate\Database\Eloquent\Model;

class Componente extends Model
{
    //
    protected $fillable = [
      'obs', 'id_tipo_material', 'id_material'
    ];

    protected $table = 'componente';

    protected $connection = 'mysqlPatrimonio';


    public function tipoMaterial()
    {
        return $this->hasOne('App\Patrimonio\TipoMaterial', 'id', 'id_tipo_material');
    }

    public function material()
    {
        return $this->hasOne('App\Patrimonio\Material', 'id', 'id_material');
    }
    
}
