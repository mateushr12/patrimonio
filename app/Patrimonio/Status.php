<?php

namespace App\Patrimonio;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status_atendimento';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'descricao'
    ];

    
    protected $connection = 'mysqlPatrimonio';
}
