<?php

namespace App\Patrimonio;

use Illuminate\Database\Eloquent\Model;

class Conservacao extends Model
{
  protected $fillable = [
    'descricao'
  ];

  protected $table = 'conservacao';

  protected $connection = 'mysqlPatrimonio';

  public $timestamps = false;
}
