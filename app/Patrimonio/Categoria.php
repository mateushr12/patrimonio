<?php

namespace App\Patrimonio;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categoria';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'descricao'
    ];

    
    protected $connection = 'mysqlPatrimonio';
}
