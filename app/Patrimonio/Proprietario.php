<?php

namespace App\Patrimonio;

use Illuminate\Database\Eloquent\Model;

class Proprietario extends Model
{
    //
    protected $fillable = [
      'descricao'
    ];

    protected $table = 'proprietario';

    protected $connection = 'mysqlPatrimonio';

    public $timestamps = false;
}
