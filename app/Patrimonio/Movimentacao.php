<?php

namespace App\Patrimonio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movimentacao extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
      'data', 'id_material', 'id_setor_origem', 'id_setor_destino', 'id_usuario_origem', 'id_usuario_destino', 'atendimento_id', 'obs'
    ];

    protected $table = 'movimentacao';

    protected $connection = 'mysqlPatrimonio';

    public function material()
    {
        return $this->hasOne('App\Patrimonio\Material', 'id', 'id_material');
    }

    public function setorO()
    {
        return $this->hasOne('App\Patrimonio\Setor', 'id', 'id_setor_origem');
    }

    public function setorD()
    {
        return $this->hasOne('App\Patrimonio\Setor', 'id', 'id_setor_destino');
    }

    public function atendimento()
    {
        return $this->hasOne('App\Patrimonio\Atendimentos', 'id', 'atendimento_id');
    }

    public function usuarioO()
    {
        return $this->hasOne('App\User', 'id', 'id_usuario_origem');
    }

    public function usuarioD()
    {
        return $this->hasOne('App\User', 'id', 'id_usuario_destino');
    }
}
