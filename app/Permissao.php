<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Permissaogrupo;

class Permissao extends Model
{
    protected $table = 'permissoes';
    protected $fillable = array('nome', 'descricao');
    public $timestamps = false;
    protected $connection = 'mysqlAutenticacao';

}
