<?php

namespace App\Callcenter;

use Illuminate\Database\Eloquent\Model;

class Tipoproblema extends Model
{
    protected $fillable = [
        'descricao',
        'orientacao_cliente',
        'user_id'
    ];

    protected $guarded = [
        'id',
        'created_at',
        'update_at'
    ];

    protected $primaryKey = 'id';

    public function usuariocadastro()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }


    public function scopeSearch( $query, $request = null )
    {
        if ($request === null) {
            $request = request();
        }

        $termos = $request->only('descricao');

        foreach ($termos as $nome => $valor) {
            if ($valor) { 
                $query->where($nome, 'LIKE', '%' . $valor . '%');
            }
        }

        $iguais = $request->only('descricao');

        foreach ($iguais as $nome => $valor) {
            if ($valor) { 
                $query->where($nome, '=', $valor);
            }
        }
        
        return $query;
    }


    protected $connection = 'mysqlCallcenter';
}
