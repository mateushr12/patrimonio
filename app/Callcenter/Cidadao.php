<?php

namespace App\Callcenter;

use Illuminate\Database\Eloquent\Model;

class Cidadao extends Model
{
    protected $fillable = [
        'cpf',
        'nome',
        'fone_fixo',
        'fone_celular',
        'fone_celular2',
        'email',
        'receber_informativo',
        'tipocidadao_id',
        'user_id'
    ];

    protected $guarded = [
        'id',
        'created_at',
        'update_at'
    ];

    protected $primaryKey = 'id';

    public function tipocidadao()
    {
        return $this->hasOne('App\Callcenter\Tipocidadao', 'id', 'tipocidadao_id');
    }

    public function usuariocadastro()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    protected $connection = 'mysqlCallcenter';
}
