<?php

namespace App\Callcenter;

use Illuminate\Database\Eloquent\Model;

class Palavra extends Model
{
    protected $fillable = [
        'palavra',
        'correcao'
    ];

    protected $guarded = [
        'id',
        'created_at',
        'update_at'
    ];

    protected $primaryKey = 'id';

    protected $connection = 'mysqlCallcenter';
}
