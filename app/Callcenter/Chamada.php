<?php

namespace App\Callcenter;

use Illuminate\Database\Eloquent\Model;

class Chamada extends Model
{
    protected $fillable = [
        'id',
        'protocolo',
        'data_hora_inicio',
        'data_hora_fim',
        'id_agiliza',
        'tipoproblema_id',
        'tiposolucao_id',
        'complemento_problema',
        'complemento_solucao',
        'tipoatendimento_id',
        'data_hora_red_finalizado',
        'id_ref',
        'user_id',
        'user_id_red',
        'user_id_atendeu_red',
        'gruposuser_id_red',
        'gruposuser_id',
        'cidadao_id',
        'tipocidadao_id',
        'id_chamada_ref_red'
    ];

    protected $guarded = [
        'id',
        'created_at',
        'update_at'
    ];

    protected $primaryKey = 'id';

    protected $appends = [
        'diferencahoras'
    ];

    public function calculaDiferenca($DataEvento){
        $hoje = new DateTime();
        $diferenca = $hoje->diff($DataEvento);
        return $diferenca->d;
    }
    public function getDiferencahorasAttribute()
    {
        $diferencahoras = '';
        $diferencahoras = '';
        return $diferencahoras;
    }

    public function chamadaredirecionada()
    {
        return $this->hasOne('App\Callcenter\Chamada', 'id', 'id_chamada_ref_red');
    }

    public function usuariocadastro()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function usuariored()
    {
        return $this->hasOne('App\User', 'id', 'user_id_red');
    }
    public function usuarioatendeu()
    {
        return $this->hasOne('App\User', 'id', 'user_id_atendeu_red');
    }
    public function grupousuariored()
    {
        return $this->hasOne('App\Gruposuser', 'id', 'gruposuser_id_red');
    }
    public function cidadao()
    {
        return $this->hasOne('App\Callcenter\Cidadao', 'id', 'cidadao_id');
    }
    public function tipocidadao()
    {
        return $this->hasOne('App\Callcenter\Tipocidadao', 'id', 'tipocidadao_id');
    }
    public function tipoproblema()
    {
        return $this->hasOne('App\Callcenter\Tipoproblema', 'id', 'tipoproblema_id');
    }
    public function tiposolucao()
    {
        return $this->hasOne('App\Callcenter\Tiposolucao', 'id', 'tiposolucao_id');
    }
    public function tipoatendimento()
    {
        return $this->hasOne('App\Callcenter\Tipoatendimento', 'id', 'tipoatendimento_id');
    }

    
    public function getDateAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function gruposuser()
    {
        return $this->hasOne('App\Gruposuser', 'id', 'gruposuser_id');
    }

    public function search( Array $data )
    {
        $procura = $this->where( function( $query ) use ( $data ) {
            if ( isset($data['id']))
                $query->where('id', $data['id']);
            if ( isset($data['date']))
                $query->where('data_hora_inicio', $data['data_hora_inicio']);
        })->toSql(); dd($procura, $data);
    }

    protected $connection = 'mysqlCallcenter';
}
