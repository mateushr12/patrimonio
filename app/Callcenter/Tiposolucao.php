<?php

namespace App\Callcenter;

use Illuminate\Database\Eloquent\Model;

class Tiposolucao extends Model
{
    protected $fillable = [
        'descricao',
        'orientacao_cliente',
        'user_id'
    ];

    protected $guarded = [
        'id',
        'created_at',
        'update_at'
    ];

    protected $primaryKey = 'id';

    public function usuariocadastro()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    protected $connection = 'mysqlCallcenter';
}
