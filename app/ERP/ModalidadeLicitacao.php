<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModalidadeLicitacao extends Model
{
    //
    protected $table = 'modalidade_licitacao';
    use SoftDeletes;
    protected $fillable=[
        'descricao'
    ];
}
