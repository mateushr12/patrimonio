<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    //
    protected $fillable = [
      'tabela', 'idLinha', 'user', 'data', 'acao'
    ];

    protected $table = 'log';

    public $timestamps = false;
}
