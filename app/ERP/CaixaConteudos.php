<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaixaConteudos extends Model
{
    use SoftDeletes;

    protected $table = 'caixa_conteudos';

    protected $fillable=[
        'nome','cpfCnpj','conteudo','data','processo','caixa_id'
    ];

    public function caixa()
    {
        return $this->hasOne('App\ERP\Caixas', 'id', 'caixa_id');
    }



}
