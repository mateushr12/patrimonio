<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;

class Pagamento extends Model
{
    //
    protected $fillable = [
      'id_empenho','valor_bruto','datapag','ob','numero','nf','vencimento'
    ];

    protected $table = 'pagamentos';

    public function empenho()
    {
        return $this->hasOne('App\ERP\Empenho', 'id', 'id_empenho');
    }


}
