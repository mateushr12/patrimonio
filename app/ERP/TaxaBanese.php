<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;

class TaxaBanese extends Model
{
    protected $table = 'taxa_retornos';
    protected $fillable = ['nosso_numero', 'valor', 'data_pagamento', 'tipo', 'tipo2', 'tipo3'];


    public function TipoArrecadacao()
    {
        return $this->hasOne('App\ERP\TipoArrecadacao', 'codigo', 'tipo');
    }



}
