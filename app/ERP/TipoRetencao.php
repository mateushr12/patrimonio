<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoRetencao extends Model
{
    //
    protected $table = 'tipo_retencao';
    use SoftDeletes;
    protected $fillable=[
        'descricao'
    ];
}
