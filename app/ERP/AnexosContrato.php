<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnexosContrato extends Model
{
    //
    protected $table = 'anexos_contrato';
    use SoftDeletes;
    protected $fillable=[
        'id_contrato', 'arquivo', 'tipoarquivo'
    ];
}
