<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contrato extends Model
{
    use SoftDeletes;

    protected $fillable=[
        'bancodepreco','licitacao','empenho','fornecedorcontratado','numerocontrato','iniciovigencia',
        'finalvigencia','valorcontrato','dataassinatura','anulacaocontrato','motivoanulacao','periodoaquisitivo', 'origem'
    ];

    public function fornecedor()
    {
        return $this->hasOne('App\ERP\Fornecedores', 'id', 'fornecedorcontratado');
    }

}
