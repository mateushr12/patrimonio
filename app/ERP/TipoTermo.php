<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;


class TipoTermo extends Model
{
    protected $table = 'tipo_termo';

    protected $fillable=[
        'descricao'
    ];

    public $timestamps = false;

}
