<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AditivoPres extends Model
{
    use SoftDeletes;

    protected $table = 'aditivos_presidencia';

    protected $fillable=[
        'id_contrato_pres','descricao','vigencia_i','vigencia_f','assinatura','anulacao',
        'tipo'
    ];

    public function contrato()
    {
        return $this->hasOne('App\ERP\ContratoPresi', 'id', 'id_contrato_pres');
    }

}
