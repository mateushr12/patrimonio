<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fornecedores extends Model
{
    //
    protected $table = 'fornecedores';
    use SoftDeletes;

    protected $fillable=[
        'identificacao', 'numeroident', 'nome', 'representante', 'cpfrepres', 'tel', 'email', 'cnae', 'especificacao', 'situacao', 'situacaocadastro',
        'status', 'id_banco', 'conta', 'dig_conta', 'agencia', 'dig_agencia', 'tp_conta', 'no_titular'
    ];

    public function banco()
    {
        return $this->hasOne('App\ERP\Banco', 'id', 'id_banco');
    }
}
