<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContratoPresi extends Model
{
    use SoftDeletes;

    protected $table = 'contratos_presidencia';

    protected $fillable=[
        'n_contrato','desricao','dt_inicio','dt_fim','publicado_site','dt_publicacao_diario',
        'gestor','parecer','dt_assinatura','obs','tipo','dt_anulacao'
    ];

    public function termo()
    {
        return $this->hasOne('App\ERP\TipoTermo', 'id', 'tipo');
    }

}
