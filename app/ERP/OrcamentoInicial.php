<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;

class OrcamentoInicial extends Model
{

    protected $table = 'orcamento_inicial';
    protected $primaryKey = 'id';
    protected $fillable = ['especificacao', 'receita_orcada', 'ano', 'ativo'];
    //public $timestamps = false;

    public function ano()
    {
        return $this->hasOne('App\ERP\Ano', 'id', 'ano');
    }

}
