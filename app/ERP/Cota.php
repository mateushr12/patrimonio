<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Cota extends Model
{
    use SoftDeletes;
    protected $fillable =[
        'cotaacao','mesrefcota','valorcota'
    ];

    public function acoes()
    {
        return $this->hasOne('App\ERP\Acoes', 'id', 'cotaacao');
    }


}
