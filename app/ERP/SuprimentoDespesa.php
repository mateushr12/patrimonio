<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SuprimentoDespesa extends Model
{
    use SoftDeletes;

    protected $fillable=[
        'suprimento_id','fornecedor_id','valor','nf','n_cheque','objeto','data'
    ];

    protected $table = 'suprimento_despesa';

    public function supri()
    {
        return $this->hasOne('App\ERP\Suprimento', 'id', 'suprimento_id');
    }

    public function forn()
    {
        return $this->hasOne('App\ERP\Fornecedores', 'id', 'fornecedor_id');
    }

}
