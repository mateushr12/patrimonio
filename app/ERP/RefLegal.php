<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RefLegal extends Model
{
    //
    protected $table = 'ref_legal';
    use SoftDeletes;
    protected $fillable=[
        'descricao'
    ];
}
