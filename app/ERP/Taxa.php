<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;


class Taxa extends Model
{



    protected $fillable=[
        'protocolo','cpf_solicitante','cnpj_solicitante','nosso_numero','cd_documento',
        'dt_emissao','dt_vencimento','dt_pagamento','pago','tipo_evento_redesim','natureza_juridica',
        'evento_redesim','porte','cpf_cnpj_contador','nome_contador','vl_recebido'
    ];

    public $timestamps = false;
}
