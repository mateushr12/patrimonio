<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;

class Liquidado extends Model
{
    //
    protected $fillable = [
        'id_empenho','valor','data'
    ];
}
