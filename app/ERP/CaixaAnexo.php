<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaixaAnexo extends Model
{
    //
    protected $table = 'caixa_anexos';
    use SoftDeletes;
    protected $fillable=[
        'id_conteudos','descricao', 'arquivo'
    ];
}
