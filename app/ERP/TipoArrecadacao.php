<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;

class TipoArrecadacao extends Model
{
    protected $table = 'tipo_arrecadacao';
    protected $fillable = ['descricao', 'codigo', 'valor_cobrado'];
}
