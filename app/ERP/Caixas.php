<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Caixas extends Model
{
    use SoftDeletes;

    protected $fillable=[
        'obs','tipo_id'
    ];

    public function tipo()
    {
        return $this->hasOne('App\ERP\CaixaTipos', 'id', 'tipo_id');
    }

}
