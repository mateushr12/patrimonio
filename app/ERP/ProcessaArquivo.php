<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;

class ProcessaArquivo extends Model
{
    protected $table = 'taxa_retornos';
    protected $fillable = ['nosso_numero', 'valor', 'data_pagamento', 'tipo', 'tipo2', 'tipo3', 'id_taxa_retorno'];

    public function TaxaRetorno()
    {
        return $this->hasOne('App\ERP\TaxaRetorno', 'id', 'id_taxa_retorno');
    }
}
