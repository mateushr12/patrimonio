<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnexosContratoPres extends Model
{
    //
    protected $table = 'anexos_contratopres';
    use SoftDeletes;
    protected $fillable=[
        'id_contrato_pres', 'descricao', 'arquivo'
    ];
}
