<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnexosDespesaSupri extends Model
{
    //
    protected $table = 'anexos_despesas';
    use SoftDeletes;
    protected $fillable=[
        'id_despesa', 'arquivo', 'descricao'
    ];
}
