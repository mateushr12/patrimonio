<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Acoes extends Model
{

    use SoftDeletes;

    protected $fillable=[
        'UniGestora', 'UniEspecificacao', 'CodFonte', 'CodGrupAcao', 'CodAcao', 'DescAcao', 'CodElemento', 'ValorDestinado', 'CodOrcamento', 'ValorInicial'
    ];

    public function fonte()
    {
        return $this->hasOne('App\ERP\Fonte', 'CodFonte', 'CodFonte');
    }

    public function grupo()
    {
        return $this->hasOne('App\ERP\Grupo', 'CodGrupo', 'CodGrupAcao');
    }

    public function elemento()
    {
        return $this->hasOne('App\ERP\Elemento', 'CodElemento', 'CodElemento');
    }

    public function orcamento()
    {
        return $this->hasOne('App\ERP\OrcamentoInicial', 'id', 'CodOrcamento');
    }

}
