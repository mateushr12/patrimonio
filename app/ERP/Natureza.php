<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Natureza extends Model
{
    //
    use SoftDeletes;
    protected $fillable =[
        'id_elemento', 'codnatureza', 'descricao'
    ];

    public function elemento()
    {
        return $this->hasOne('App\ERP\Elemento', 'id', 'id_elemento');
    }
}
