<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Elemento extends Model
{
    //
    use SoftDeletes;
    protected $fillable=[
        'CodElemento', 'DescElemento'
    ];
}
