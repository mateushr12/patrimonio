<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;


class CaixaTipos extends Model
{
    protected $table = 'caixa_tipos';

    protected $fillable=[
        'descricao'
    ];

    public $timestamps = false;

}
