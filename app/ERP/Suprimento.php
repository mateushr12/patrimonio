<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Suprimento extends Model
{
    use SoftDeletes;

    protected $fillable=[
        'numero','valor','data','empenho_id','acao_id','elemento_id'
    ];

    public function emp()
    {
        return $this->hasOne('App\ERP\Empenho', 'id', 'empenho_id');
    }

    public function acaoId()
    {
        return $this->hasOne('App\ERP\Acoes', 'id', 'acao_id');
    }

    public function elem()
    {
        return $this->hasOne('App\ERP\Elemento', 'id', 'elemento_id');
    }

}
