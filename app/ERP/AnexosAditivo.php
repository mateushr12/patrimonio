<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnexosAditivo extends Model
{
    //
    protected $table = 'anexos_aditivos';
    use SoftDeletes;
    protected $fillable=[
        'id_aditivo', 'arquivo', 'tipoarquivo'
    ];
}
