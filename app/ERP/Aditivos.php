<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Aditivos extends Model
{
    //
    protected $table = 'aditivos';
    use SoftDeletes;
    protected $fillable=[
        'id_contrato', 'n_aditivo', 'vigencia_i', 'vigencia_f', 'valor', 'assinatura', 'anulacao', 'motivo', 'periodo', 'tipo', 'natureza'
    ];
}
