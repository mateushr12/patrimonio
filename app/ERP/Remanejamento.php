<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;

class Remanejamento extends Model
{
    protected $table = 'remanejamento';
    protected $primaryKey = 'id';
    protected $fillable = ['codAcao_origem', 'codAcao_destino', 'valor', 'data', 'obs'];
    protected $dates = ['data'];
    //public $timestamps = false;


    public function acoes()
    {
        return $this->hasOne('App\ERP\Acoes', 'id', 'DescAcao');
    }
}
