<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;

class Retencao extends Model
{
    //
    protected $fillable = [
      'id_pagamento','id_tipo','valor','data','datapag','ob','obs','pago'
    ];

    protected $table = 'retencoes';

    public function pagamento()
    {
        return $this->hasOne('App\ERP\Pagamento', 'id', 'id_pagamento');
    }

    public function tipo()
    {
        return $this->hasOne('App\ERP\TipoRetencao', 'id', 'id_tipo');
    }
}
