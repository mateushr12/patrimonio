<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;

class TaxaRetorno extends Model
{
    protected $table = 'file_entries';
    protected $fillable = ['filename', 'mime', 'path', 'size', 'dataprocessamento', 'status'];

    public function TaxaRetorno()
    {
        return $this->hasOne('App\ERP\TaxaRetorno', 'id', 'id_taxa_retorno');
    }
}
