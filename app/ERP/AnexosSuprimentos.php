<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnexosSuprimentos extends Model
{
    //
    protected $table = 'anexos_suprimentos';
    use SoftDeletes;
    protected $fillable=[
        'id_suprimento','descricao', 'arquivo'
    ];
}
