<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;

class OrcamentoSuplementar extends Model
{
    protected $table = 'orcamento_suplementar';
    protected $primaryKey = 'id';
    protected $fillable = ['valor', 'detalhes', 'codAcoes'];
    //public $timestamps = false;

    public function acoes()
    {
        return $this->hasOne('App\ERP\Acoes', 'id', 'codAcoes');
    }
}
