<?php

namespace App\ERP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empenho extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'numero','tipoempenho','id_empenho','bancoprecoempenho','dataempenho','urgempenho','modempenho','modlicempenho',
        'despempenho','acaoempenho','elemempenho','subelemento','fichaempenho','convempenho','valorempenho',
        'licempenho','protoempenho','credorempenho','doccredor','numerodoccredor',
        'nomecredor','reflegalempenho','nuproempenho','obsempenho','dt_pai','liquidado'
    ];

    public function subelementos()
    {
        return $this->hasOne('App\ERP\Natureza', 'id', 'subelemento');
    }

    public function reflegal()
    {
        return $this->hasOne('App\ERP\RefLegal', 'id', 'reflegalempenho');
    }

    public function acao()
    {
        return $this->hasOne('App\ERP\Acoes', 'id', 'acaoempenho');
    }

    public function contrato()
    {
        return $this->hasOne('App\ERP\Contrato', 'id', 'licempenho');
    }

    public function modalidadelic()
    {
        return $this->hasOne('App\ERP\ModalidadeLicitacao', 'id', 'modlicempenho');
    }

    public function elemento()
    {
        return $this->hasOne('App\ERP\Elemento', 'id', 'elemempenho');
    }


}
