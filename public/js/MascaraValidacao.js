// JavaScript Document
//adiciona mascara de cnpj
// ================================================================================================
        function buscaInfoCidadao(cpf_cidadao)
        {
                var cpf = $("#cpf_cidadao");

                if ($("#cpf_cidadao").val().trim() != "")
                {
                        $("#nome_cidadao").val() = "";
                }
        }
// ================================================================================================
        function mascaraData(data)
        {
                if(mascaraInteiro(data)==false){
                        event.returnValue = false;
                }
                return formataCampo(data, '00/00/0000', event);
        }
// ================================================================================================
        function mascaraCep(cep)
        {
                if(mascaraInteiro(cep)==false)
                {
                        event.returnValue = false;
                }
                return formataCampo(cep, '00.000-000', event);
        }
// ================================================================================================
        function mascaraCNPJ(cnpj)
        {
                if(mascaraInteiro(cnpj)==false){
                        event.returnValue = false;
                }
                return formataCampo(cnpj, '00.000.000/0000-00', event);
        }
// ================================================================================================
//adiciona mascara ao Fone
// ================================================================================================
        function mascaraFone(fone)
        {
        if(mascaraInteiro(fone)==false){
                event.returnValue = false;
        }
        return formataCampo(fone, '(00) 0000-0000', event);
        }
// ================================================================================================
        function mascaraFoneFixo(fone)
        {
        if(mascaraInteiro(fone)==false){
                event.returnValue = false;
        }
        return formataCampo(fone, '(00) 0000-0000', event);
        }
// ================================================================================================
        function mascaraFoneCelular(fone)
        {
        if(mascaraInteiro(fone)==false){
                event.returnValue = false;
        }
        return formataCampo(fone, '(00) 00000-0000', event);
        }
// ================================================================================================
function mascaraCPF(cpf)
{
    if(mascaraInteiro(cpf)==false)
    {
        event.returnValue = false;
    }
    return formataCampo(cpf, '000.000.000-00', event);
}
// ================================================================================================
// valida numero inteiro com mascara
        function mascaraInteiro()
        {
                if (event.keyCode < 48 || event.keyCode > 57)
                {
                        event.returnValue = false;
                        return false;
                }
        return true;
        }
// ================================================================================================
        function mascaraRG(rg)
        {
                if((rg)==false)
                {
                        event.returnValue = false;
                }
                return formataCampo(rg, '00.000.000-0', event);
        }
// ================================================================================================
// adiciona mascara ao telefone
        function mascaraTelefone(tel)
        {
                if(mascaraInteiro(tel)==false){
                        event.returnValue = false;
                }
                return formataCampo(tel, '(00) 0000-0000', event);
        }
// ================================================================================================
        function validaCep(cep)
        {
                exp = /\d{2}\.\d{3}\-\d{3}/
                if(!exp.test(cep.value))
                        alert('Numero de Cep Invalido!');
        }
// ================================================================================================
        function validarCPF(Objcpf)
        {
                var cpf = Objcpf.value;
                exp = /\.|\-/g
                cpf = cpf.toString().replace( exp, "" );
                var digitoDigitado = eval(cpf.charAt(9)+cpf.charAt(10));
                var soma1=0, soma2=0;
                var vlr =11;

                for(i=0;i<9;i++)
                {
                        soma1+=eval(cpf.charAt(i)*(vlr-1));
                        soma2+=eval(cpf.charAt(i)*vlr);
                        vlr--;
                }
                soma1 = (((soma1*10)%11)==10 ? 0:((soma1*10)%11));
                soma2=(((soma2+(2*soma1))*10)%11);

                var digitoGerado=(soma1*10)+soma2;

                /*
                if(digitoGerado!=digitoDigitado)
                {
                        alert('CPF Invalido!');
                }
                */
        }

        function ValidarCPF2(Objcpf)
        {
                var cpf = Objcpf.value;
                var vl = true
                exp = /\.|\-/g
                cpf = cpf.toString().replace( exp, "" );
                if (cpf.length != 11 ||
		                cpf == "00000000000" ||
		                cpf == "11111111111" ||
		                cpf == "22222222222" ||
		                cpf == "33333333333" ||
		                cpf == "44444444444" ||
		                cpf == "55555555555" ||
		                cpf == "66666666666" ||
		                cpf == "77777777777" ||
		                cpf == "88888888888" ||
		                cpf == "99999999999")
                {
                  vl = false
                }
                var digitoDigitado = eval(cpf.charAt(9)+cpf.charAt(10));
                var soma1=0, soma2=0;
                var vlr =11;

                for(i=0;i<9;i++)
                {
                        soma1+=eval(cpf.charAt(i)*(vlr-1));
                        soma2+=eval(cpf.charAt(i)*vlr);
                        vlr--;
                }
                soma1 = (((soma1*10)%11)==10 ? 0:((soma1*10)%11));
                soma2=(((soma2+(2*soma1))*10)%11);

                var digitoGerado=(soma1*10)+soma2;
                if(digitoGerado!=digitoDigitado || vl==false)
                {
                        Objcpf.value='';
                        alert('CPF Inválido!');
                }
        }

// ================================================================================================
        function validarCNPJ(ObjCnpj)
        {
                var cnpj = ObjCnpj.value;
                var valida = new Array(6,5,4,3,2,9,8,7,6,5,4,3,2);
                var dig1= new Number;
                var dig2= new Number;

                exp = /\.|\-|\//g
                cnpj = cnpj.toString().replace( exp, "" );

                if (cnpj == "00000000000000"){
                  ObjCnpj.value='';
                  alert('CNPJ Inválido!');
                }
                
                var digito = new Number(eval(cnpj.charAt(12)+cnpj.charAt(13)));

                for(i = 0; i<valida.length; i++)
                {
                        dig1 += (i>0? (cnpj.charAt(i-1)*valida[i]):0);
                        dig2 += cnpj.charAt(i)*valida[i];
                }
                dig1 = (((dig1%11)<2)? 0:(11-(dig1%11)));
                dig2 = (((dig2%11)<2)? 0:(11-(dig2%11)));

                if(((dig1*10)+dig2) != digito)
                {
                  ObjCnpj.value='';
                  alert('CNPJ Inválido!');
                }

        }
// ================================================================================================
        function validaData(data)
        {
                exp = /\d{2}\/\d{2}\/\d{4}/
                if(!exp.test(data.value))
                        alert('Data Invalida!');
        }
// ================================================================================================
        function validaEmail($email)
        {
                if ( ( ! isset( $email ) || ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) && !$erro )
                {
                $erro = 'Envie um email válido.';
                }
        }
// ================================================================================================
// valida telefone
        function validaTelefone(tel)
        {
                exp = /\(\d{2}\)\ \d{4}\-\d{4}/
                if(!exp.test(tel.value))
                        alert('Numero de Telefone Invalido!');
        }
// ================================================================================================
        function formataCampo(campo, Mascara, evento)
        {
                var boleanoMascara;

                var Digitato = evento.keyCode;
                exp = /\-|\.|\/|\(|\)| /g
                campoSoNumeros = campo.value.toString().replace( exp, "" );

                var posicaoCampo = 0;
                var NovoValorCampo="";
                var TamanhoMascara = campoSoNumeros.length;;

                if (Digitato != 8)
                { // backspace
                        for(i=0; i<= TamanhoMascara; i++)
                        {
                                boleanoMascara  = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".") || (Mascara.charAt(i) == "/"))
                                boleanoMascara  = boleanoMascara || ((Mascara.charAt(i) == "(") || (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " "))

                                if (boleanoMascara)
                                {
                                        NovoValorCampo += Mascara.charAt(i);
                                        TamanhoMascara++;
                                }
                                else
                                {
                                        NovoValorCampo += campoSoNumeros.charAt(posicaoCampo);
                                        posicaoCampo++;
                                }
                        }
                        campo.value = NovoValorCampo;
                        return true;
                }
                else
                {
                        return true;
                }
        }
// ================================================================================================
        function mostraDialogo(mensagem, tipo, tempo)
        {
                alert('Cheguei');
                // se houver outro alert desse sendo exibido, cancela essa requisição
                if($("#message").is(":visible"))
                {
                        return false;
                }

                // se não setar o tempo, o padrão é 3 segundos
                if(!tempo)
                {
                        var tempo = 3000;
                }

                // se não setar o tipo, o padrão é alert-info
                if(!tipo)
                {
                        var tipo = "info";
                }

                // monta o css da mensagem para que fique flutuando na frente de todos elementos da página
                var cssMessage = "display: block; position: fixed; top: 0; left: 20%; right: 20%; width: 60%; padding-top: 10px; z-index: 9999";
                var cssInner = "margin: 0 auto; box-shadow: 1px 1px 5px black;";

                // monta o html da mensagem com Bootstrap
                var dialogo = "";
                dialogo += '<div id="message" style="'+cssMessage+'">';
                dialogo += '    <div class="alert alert-'+tipo+' alert-dismissable" style="'+cssInner+'">';
                dialogo += '    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>';
                dialogo +=          mensagem;
                dialogo += '    </div>';
                dialogo += '</div>';

                // adiciona ao body a mensagem com o efeito de fade
                $("body").append(dialogo);
                $("#message").hide();
                $("#message").fadeIn(200);

                // contador de tempo para a mensagem sumir
                setTimeout(function()
                {
                        $('#message').fadeOut(300, function()
                        {
                                $(this).remove();
                        });
                }, tempo); // milliseconds
        }


// ================================================================================================
//     FUNCOES PARA CEP
// ================================================================================================
        function limpa_formulário_cep()
        {
        //Limpa valores do formulário de cep.
        document.getElementById('descricao_problema').value=("");
        }
// ================================================================================================
        function meu_callback(conteudo)
        {
        if (!("erro" in conteudo))
        {
                //Atualiza os campos com os valores.
                var valor1 = (conteudo.logradouro).split(" ");
                var valor2 = (conteudo.bairro).split(" ");
                var valor3 = (conteudo.localidade).split(" ");
                var valor4 = (conteudo.uf).split(" ");
                document.getElementById('descricao_problema').value=valor1+valor2+valor3+valor4;
        } //end if.
        else
        {
                //CEP não Encontrado.
                limpa_formulário_cep();
                alert("CEP não encontrado.");
        }
        }
// ================================================================================================
        function pesquisaCep(valor)
        {
                //Nova variável "cep" somente com dígitos.
                var cep = valor.replace(/\D/g, '');
                //Verifica se campo cep possui valor informado.
                if (cep != "")
                {
                        //Expressão regular para validar o CEP.
                        var validacep = /^[0-9]{8}$/;

                        //Valida o formato do CEP.
                        if(validacep.test(cep))
                        {
                                //Preenche os campos com "..." enquanto consulta webservice.
                                document.getElementById('descricao_problema').value="...";
                                //Cria um elemento javascript.
                                var script = document.createElement('script');
                                //Sincroniza com o callback.
                                script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';
                                //Insere script no documento e carrega o conteúdo.
                                document.body.appendChild(script);
                        } //end if.
                        else
                        {
                                //cep é inválido.
                                limpa_formulário_cep();
                                alert("Formato de CEP inválido.");
                        }
                } //end if.
                else
                {
                        //cep sem valor, limpa formulário.
                        limpa_formulário_cep();
                }
        }
// ================================================================================================
    // $pesquisaCidadao = $this->select( "SELECT distinct cpf_cidadao, nome_cidadao, fone_celular, email_cidadao FROM chamada" );
    // document.getElementById('nome_cidadao').asstring = $cpf_cidadao;
    // document.getElementById('fone_celular').value = $pesquisaCidadao['fone_celular'];
    // document.getElementById('email_cidadao').value = $pesquisaCidadao['email_cidadao'];

    // return formataCampo(cpf, '000.000.000-00', event);
