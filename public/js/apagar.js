$(document).ready(function(){
	$('a[data-confirm]').click(function(ev){
		var href = $(this).attr('href');
		if(!$('#confirm-delete').length){
			
			$('body').append('<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header bg-danger text-white">EXCLUIR ITEM<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body">Tem certeza que deseja excluir o item selecionado?</div><div class="modal-footer"><a class="btn btn-danger text-white" id="dataConfirmOK">Apagar</a><button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button></div></div></div></div>');
			
		}
		$('#dataConfirmOK').attr('href', href);
		$('#confirm-delete').modal({show:true});
		return false;
	
	});
	
});