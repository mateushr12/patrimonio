$(document).ready(function(){

    $(".MaskCPF").mask('999.999.999-99');

    $("#identificacao").on('change', function(){
        if($("#identificacao").val() == 1){
            $("#numeroident").mask("99.999.999/9999-99")
        }else if($("#identificacao").val() == 2){
            $("#numeroident").mask("999.999.999.999.999")
        }else if($("#identificacao").val() == 3){
            $("#numeroident").mask("999999999-9")
        }else if($("#identificacao").val() == 4){
            $("#numeroident").mask("999.999.999-99")
        }

    });

    $(".precoempenho").hide();

    $("#bancoprecoempenho").change(function(){
        
        var tst = $("option:selected").attr('value')
        if(tst == 1){
            $("#preconao").hide();
            $("#precosim").show();
        }else if(tst == 2){
            $("#precosim").hide();
            $("#preconao").show();
        }else{
            $("#precosim").hide();
            $("#preconao").hide();
        }
    }).trigger( "change" );



})
