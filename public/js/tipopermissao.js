$(document).ready(function(){

    $("#rotas").hide()
    $("#manual").hide()

    $("#tppermissao").on('change', function(){
        if($("#tppermissao").val() == 1){
            $("#rotas").show()
            $("#manual").hide()
        }else if($("#tppermissao").val() == 2){
            $("#rotas").hide()
            $("#manual").show()
        }else{
            $("#rotas").hide()
            $("#manual").hide()            
        }
    })
})
