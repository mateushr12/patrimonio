$(document).ready(function(){

    $("#empenho").hide()
    $("#manual").hide()

    $("#tpprocesso").on('change', function(){
        if($("#tpprocesso").val() == 1){
            $("#empenho").show()
            $("#manual").hide()
        }else if($("#tpprocesso").val() == 2){
            $("#empenho").hide()
            $("#manual").show()
        }else{
            $("#empenho").hide()
            $("#manual").hide()
        }
    })
})
