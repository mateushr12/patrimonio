$(document).ready(function() {

  $("#cotaacao").on('change', function(){
    var idacao = $("#cotaacao").val();
    $.get('/erp/cota/acao/'+idacao, function (valor){
        $("#valordisponivel").val(valor);
    });
  });


  $('#elemempenho').on('change', function () {
      var id_estado = $('#elemempenho').val();
      $.get('/erp/empenho/elemento/'+id_estado, function (elem){
          $('select[name=subelemento]').empty();
          $.each(elem, function (key, value) {
              $('select[name=subelemento]').append('<option value=' + value.id + '>' + value.codnatureza +'-'+ value.descricao + '</option>');
          });
      });
  });

  $("#acaoempenho").on('change', function(){
    var idacao = $("#acaoempenho").val();
    $.get('/erp/empenho/ficha/'+idacao, function (valor){
        $("#fichaempenho").val(valor);
    });
  });

//funçao que preenche automagicamente o campo setor de origem da movimentaçao de materiais (Patrimonio)
  $("#material").on('change', function(){
    var idacao = $("#material").val();
    $.get('/patrimonio/movimentacao/origem/'+idacao, function (valor){
        $("#setorOrigem").val(valor.id);
        $("#setorOrigemShow").val(valor.descricao);
        $("#usuarioOrigem").val(valor.idusuario);
        $("#usuarioOrigemShow").val(valor.usuario);
    });
  });



})
