@include('layout_admin._includes.topo_print_callcenter')
    <table class="table table-bordered table-hover" style="font-size:80%; margin: auto;">
        <caption></caption>
        <thead>
            <tr class="bg-light">
                <th>ID</th>
                <th>Descrição<br>
                    Usuário (cadastro/alteração) / Criação / Modificação</th>
            </tr>
        </thead>

        <tbody>
            @if(session()->has('individuals'))
                @foreach( session()->get( 'individuals' ) as $key => $individual )
                    <tr>
                        <td style="text-align: center;">{{ isset( session()->get( 'individuals' )[ $key ]->id ) ? session()->get( 'individuals' )[ $key ]->id : '' }}</td>
                        <td>{{ isset( session()->get( 'individuals' )[ $key ]->descricao ) ? session()->get( 'individuals' )[ $key ]->descricao : '' }}<br>
                            <strong>Usuário: </strong>{{ isset( session()->get( 'individuals' )[ $key ]->usuariocadastro->name ) ? session()->get( 'individuals' )[ $key ]->usuariocadastro->name : '' }} ({{ isset( session()->get( 'individuals' )[ $key ]->usuariocadastro->grupo->nome ) ? session()->get( 'individuals' )[ $key ]->usuariocadastro->grupo->nome : '' }} ) | 
                            <strong>Criação: </strong>{{ isset( session()->get( 'individuals' )[ $key ]->created_at ) ? datahoraBR( session()->get( 'individuals' )[ $key ]->created_at ) : '' }} | 
                            <strong>Modificação: </strong>{{ isset( session()->get( 'individuals' )[ $key ]->updated_at ) ? datahoraBR( session()->get( 'individuals' )[ $key ]->updated_at ) : '' }}
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
@include('layout_admin._includes.footer_print_callcenter')
