<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="copyright">
                    <p>JUCESE - 2019. Todos os direitos reservados.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END PAGE CONTAINER-->
</div>

</div>

<!-- Jquery JS-->
<script src="<?php echo asset('vendor/jquery-3.2.1.min.js')?>"></script>
<!-- Bootstrap JS-->
<script src="<?php echo asset('vendor/bootstrap-4.1/popper.min.js')?>"></script>
<script src="<?php echo asset('vendor/bootstrap-4.1/bootstrap.min.js')?>"></script>
<!-- Vendor JS       -->
<script src="<?php echo asset('vendor/slick/slick.min.js')?>"></script>
<script src="<?php echo asset('vendor/wow/wow.min.js')?>"></script>
<script src="<?php echo asset('vendor/animsition/animsition.min.js')?>"></script>
<script src="<?php echo asset('vendor/bootstrap-progressbar/bootstrap-progressbar.min.js ')?>"></script>
<script src="<?php echo asset('vendor/counter-up/jquery.waypoints.min.js')?>"></script>
<script src="<?php echo asset('vendor/counter-up/jquery.counterup.min.js')?>"></script>
<script src="<?php echo asset('vendor/circle-progress/circle-progress.min.js')?>"></script>
<script src="<?php echo asset('vendor/perfect-scrollbar/perfect-scrollbar.js')?>"></script>
<script src="<?php echo asset('vendor/chartjs/Chart.bundle.min.js')?>"></script>
<script src="<?php echo asset('vendor/select2/select2.min.js')?>"></script>
<script src="<?php echo asset('vendor/vector-map/jquery.vmap.js')?>"></script>
<script src="<?php echo asset('vendor/vector-map/jquery.vmap.min.js')?>"></script>
<script src="<?php echo asset('vendor/vector-map/jquery.vmap.sampledata.js')?>"></script>
<script src="<?php echo asset('vendor/vector-map/jquery.vmap.world.js')?>"></script>

<!-- Main JS-->
<script src="<?php echo asset('js/main.js')?>"></script>
<script src="<?php echo asset('js/MascaraValidacao.js')?>"></script>
<script src="<?php echo asset('js/jquery.validate.min.js')?>" type="text/javascript"></script>
<script src="<?php echo asset('js/validacao.js ')?>" type="text/javascript"></script>
<script src="<?php echo asset('js/apagar.js ')?>" type="text/javascript"></script>
<script src="<?php echo asset('js/jquery.mask.js')?>"></script>
<script src="<?php echo asset('js/jquery.rg.js')?>"></script>
<script src="<?php echo asset('js/tipoanexo.js')?>"></script>
<script src="<?php echo asset('js/valoracao.js')?>"></script>

</body>
</html>
<!-- end document-->
