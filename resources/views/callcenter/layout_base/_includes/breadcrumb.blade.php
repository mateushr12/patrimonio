<!-- BREADCRUMB-->
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">Você esta aqui:</span>
                            <ul class="list-unstyled list-inline au-breadcrumb__list">
                                <li class="list-inline-item active">
                                    <a href="{{url("/admin")}}">Principal</a>
                                </li>
                                <li class="list-inline-item seprate">
                                    <span>/</span>
                                </li>
                                <li class="list-inline-item">@yield('breadcrumb')</li>
                            </ul>
                        </div>
                       <!-- <button class="au-btn au-btn-icon au-btn--green"><i class="zmdi zmdi-plus"></i>add item</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</br>
<!-- END BREADCRUMB-->
