<body class="animsition">
    <div class="page-wrapper">

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar2">
            <div class="logo">
                <a href="/welcome">
                    <img src="<?php echo asset('images/logo-white.png')?>" alt="JUCESE" />
                </a>
            </div>

            <div class="menu-sidebar2__content js-scrollbar1">
                <div class="account2">

                    <div class="image img-cir img-120">
                        @if ( file_exists( 'images/fotouser/'.auth()->user()->email.'.jpg' ) )
                            <img src="<?php echo asset('images/fotouser/'.auth()->user()->email.'.jpg') ?>" alt="" />
                        @else
                            <img src="<?php echo asset('images/fotouser/default-avatar.png')?>" alt="" />
                        @endif
                    </div>

                    <h4 class="name">{{ \Auth::user()->name }}</h4>
                    <h4 class="nivel">{{ \Auth::user()->grupo->nome }}</h4>
                    {{-- \Auth::user()->permissoes1->grupouser_id --}}
                    <a href="{{ route('sair') }}">Sair</a>
                </div>

                <nav class="navbar-sidebar2">
                    <ul class="list-unstyled navbar__list">

                            <div class="container-fluid">
                                <div class="collapse navbar-collapse">
                                    <ul class="nav navbar-nav">
                                        @isset($menu)
                                        @foreach ($menus as $key => $item)
                                            @if ($item['parent'] != 0)
                                                @break
                                            @endif
                                            @include('partials.menu-item', ['item' => $item])
                                        @endforeach
                                        @endisset
                                    </ul>
                                </div>
                            </div>

                        <li><a href="{{ route('chamada.index') }}"><i class="fas fa-phone"></i>Chamada</a></li>
                        <li><a href="{{ route('tipoproblema.index') }}"><i class="fa fa-bug"></i>Tipo de Problema</a></li>
                        <li><a href="{{ route('tiposolucao.index') }}"><i class="fa fa-lightbulb"></i>Tipo de Solução</a></li>
                        <li><a href="{{ route('tipocidadao.index') }}"><i class="fa fa-male"></i>Tipo de Cidadão</a></li>
                        <li><a href="{{ route('cidadao.index') }}"><i class="fa fa-male"></i>Cidadão</a></li>

                        <li><a href="{{ route('consulta.chamadageral') }}"><i class="fas fa-phone"></i>Consulta Chamadas</a></li>
                        <li><a href="{{ route('consulta.produtividade') }}"><i class="fas fa-chart-bar"></i>Consulta Produtividade</a></li>
                        <li><a href="{{ route('consulta.painelonline') }}"><i class="fas fa-bolt"></i>Painel On-Line</a></li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

    <!-- PAGE CONTAINER-->
    <div class="page-container2">
