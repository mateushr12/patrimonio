<meta HTTP-EQUIV='refresh' CONTENT='10;URL='" . $_SERVER['PHP_SELF'] . ">

@extends('layout_admin.principal')
@section ('breadcrumb','Callcenter / Chamada')

@section('conteudo')

    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-10">
                                        {{ $titulo }}
                                    </div>

                                    @if (in_array('callcenter/chamada/acoes/pesquisarcpf', Session::get('permissoes.nomes')))
                                        <div class="col-2">
                                            <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small"><i class="zmdi zmdi-plus"></i><a style="color: white;" href="{{ route ('chamada.pesquisarcpf') }}">Chamada</a></button>
                                        </div>
                                    @else
                                    @endif
                                </div>
                            </div>

                            <div class="card-body card-block">

                                @if ($errors->any()) <div class="alert alert-danger"> <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul> </div> @endif
                                @if (session('msg')) <div class="alert alert-danger alert-dismissible fade show" role="alert"> {{session('msg')}} <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> @endif

                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light">
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Item</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Protocolo / Cidadão</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Atendimento: Problema / Solução / Agiliza</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Ação</td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach( $individuals as $individual )
                                            <tr>
                                                <td class="small text-left" style="vertical-align: middle;">{{ $individual->item }}</td>
                                                <td class="small text-left" style="vertical-align: middle;">
                                                        <strong>Protocolo:                  </strong> {{ isset( $individual->protocolo ) ? mask( $individual->protocolo, '#### #### #### #### ####' ) : '' }}
                                                    <br><strong>Ocorrência:                 </strong> {{ isset( $individual->data_hora_inicio ) ? dataBR( $individual->data_hora_inicio ) : '' }} das {{ isset( $individual->data_hora_inicio ) ? horaBR( $individual->data_hora_inicio ) : '' }} às {{ isset( $individual->data_hora_fim ) ? horaBR( $individual->data_hora_fim ) : '' }} (duração: {{ isset( $individual->diferencadatas ) ? horaAnormal( $individual->diferencadatas ) : '' }})
                                                    <br><strong>C.P.F.:                     </strong> {{ isset( $individual->cidadao->cpf ) ? mask( $individual->cidadao->cpf, '###.###.###-##' ) : '' }}
                                                    <br><strong>Tipo Cidadão:               </strong> {{ isset( $individual->tipocidadao->descricao ) ? $individual->tipocidadao->descricao : '' }}
                                                    <br><strong>Nome:                       </strong> {{ isset( $individual->cidadao->nome ) ? $individual->cidadao->nome : '' }}
                                                    <br><strong>Fone Fixo:                  </strong> {{ isset( $individual->cidadao->fone_fixo ) ? mask( $individual->cidadao->fone_fixo, '(##) ####-####' ) : '' }} | 
                                                        <strong>Celular:                    </strong> {{ isset( $individual->cidadao->fone_celular ) ? mask( $individual->cidadao->fone_celular, '(##) #####-####' ) : '' }} | 
                                                        <strong>Celular2:                   </strong> {{ isset( $individual->cidadao->fone_celular2 ) ? mask( $individual->cidadao->fone_celular2, '(##) #####-####' ) : '' }} 
                                                    <br><strong>Email:                      </strong> {{ isset( $individual->cidadao->email ) ? $individual->cidadao->email : '' }} 
                                                    <br><strong>Deseja Receber Informativo: </strong> @if ( $individual->cidadao_id ) @if ( $individual->cidadao->receber_informativo === 1 ) {{ 'Sim' }} @elseif ( $individual->cidadao->receber_informativo === 0 ) {{ 'Não' }} @else {{ 'Não Informado' }} @endif @endif
                                                    <br><strong>Tipo de Atendimento:        </strong> {{ isset( $individual->tipoatendimento->descricao ) ? $individual->tipoatendimento->descricao : '' }}
{{--
                                                    <br>@if ( $individual->id_chamada_ref_red > 0 ) <strong>Atendimento vinculado de: </strong>
                                                                                                    <strong class="text-success">{{ $individual->chamadaredirecionada->usuariocadastro->name }} em 
                                                                                                                     {{ datahoraBR( $individual->chamadaredirecionada->data_hora_inicio ) }}</strong> @endif
--}}
                                                </td>

                                                <td class="small text-left" style="vertical-align: middle;">
                                                        <strong>Tipo de Problema:                     </strong> {{ isset($individual->tipoproblema->descricao) ? $individual->tipoproblema->descricao : '' }}
                                                    <br><strong>Complemento do Problema:              </strong> {{ isset($individual->complemento_problema) ? $individual->complemento_problema : '' }} 
                                                    <br><strong>Tipo de Solução:                      </strong> {{ isset($individual->tiposolucao->descricao) ? $individual->tiposolucao->descricao : '' }}
                                                    <br><strong>Complemento da Solução:               </strong> {{ isset($individual->complemento_solucao) ? $individual->complemento_solucao : '' }} 
                                                    <br><strong>Protocolo Agiliza:                    </strong> {{ isset($individual->id_agiliza) ? $individual->id_agiliza : '' }}
                                                    <br><strong>Usuário que atendeu:                  </strong> {{ isset($individual->usuariocadastro->name) ? $individual->usuariocadastro->name : '' }} ({{ isset($individual->usuariocadastro->grupo->nome) ? $individual->usuariocadastro->grupo->nome : '' }})
                                                    <br><strong>Redirecionado para Grupo:             </strong> {{ isset($individual->grupousuariored->nome) ? $individual->grupousuariored->nome : '' }}
                                                    <br><strong>Redirecionado para Usuário:           </strong> {{ isset($individual->usuariored->name) ? $individual->usuariored->name : '' }} ({{ isset($individual->usuariored->grupo->nome) ? $individual->usuariored->grupo->nome : '' }})
                                                    <br><strong>Data/Hora Solução redirecionamento:   </strong> @if ( isset($individual->data_hora_red_finalizado) ) <strong class="text-success">{{ datahoraBR( $individual->data_hora_red_finalizado ) }}</strong> @elseif ( !$individual->data_hora_red_finalizado && !$individual->user_id_atendeu_red && ( $individual->gruposuser_id_red || $individual->user_id_red ) ) <strong class="text-danger"> - Pendente -</strong> @endif
                                                    <br><strong>Usuário que atendeu redirecionamento: </strong> @if ( isset($individual->usuarioatendeu->name) ) <strong class="text-success">{{ $individual->usuarioatendeu->name }} ({{ $individual->usuarioatendeu->grupo->nome }})</strong> @endif
                                                </td>

                                                @if ( ( $individual->user_id_atendeu_red == NULL ) && ( $individual->user_id_red == $usuarioatual || $individual->gruposuser_id_red == $gruposuserLogado ) )
                                                    <td class="text-center" style="vertical-align: middle;">
                                                        <div class="table-data-feature">
                                                            <a href='{{route("chamada.cadastroredirecionado", $individual["id"])}}'><button type="button"  class="item"  title="Editar Chamada Pendente"  data-toggle="modal" data-target="#EditarModal" style="background-color: yellow" data-whatever="{{ route ('chamada.cadastroredirecionado', $individual->id) }}"><i class="zmdi zmdi-edit"></i></button></a>
                                                        </div>
                                                    </td>
                                                @else
                                                    <td class="text-center" style="vertical-align: middle;">
                                                        <div class="table-data-feature">
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="row d-flex justify-content-center">
                                    {{-- $individuals->links() --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- modal form NOVA AÇÃO-->
    <div class="modal fade" id="FormModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="FormModalLabel">Cadastro de Chamada</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body card-block">
                            <form class="" action="{{ route('chamada.pesquisarcpf') }}" method="post">
                            {{ csrf_field() }}
                            {{-- @include('/callcenter/chamada.acoes/_form') --}}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">
                        Salvar
                    </button>
                    <button type="reset" class="btn btn-warning" data-dismiss="modal">
                        Cancelar
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end modal form NOVA AÇÃO -->
@endsection
