@extends('layout_admin.principal')
@section ('breadcrumb','Callcenter / Chamada')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <strong>Cadastro de Chamada - Nova Chamada</strong>
                            </div>

                            <div class="card-body card-block">

                                @if ($errors->any()) <div class="alert alert-danger"> <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul> </div> @endif
                                @if (session('msg')) <div class="alert alert-danger alert-dismissible fade show" role="alert"> {{session('msg')}} <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> @endif

                                @if ( $chamadasAgiliza != '' )
                                    @if ( $chamadasAgiliza->count() > 0 )
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <strong style="color: red">Ocorrências sobre o Protocolo Agiliza Informado: {{ $agilizaInformado }} - {{ $chamadasAgiliza->count() }} ocorrência(s) ( por data de ocorrência )</strong>
                                                    </div>

                                                    <div class="card-body card-block">
                                                        <table class="table table-bordered table-hover" style="border-color: red;">
                                                            <thead>
                                                                <tr class="bg-light">
                                                                    <td class="info small text-center font-weight-bold" style="vertical-align: middle; border-color: red;">Cidadão</td>
                                                                    <td class="info small text-center font-weight-bold" style="vertical-align: middle; border-color: red;">Data Atendimento</td>
                                                                    <td class="info small text-center font-weight-bold" style="vertical-align: middle; border-color: red;">Atendente</td>
                                                                    <td class="info small text-center font-weight-bold" style="vertical-align: middle; border-color: red;">Problema</td>
                                                                    <td class="info small text-center font-weight-bold" style="vertical-align: middle; border-color: red;">Solução</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach( $chamadasAgiliza as $chamadasAgilizaIndividual )
                                                                    <tr class="bg-light">
                                                                        <td class="small text-center" style="vertical-align: middle; border-color: red;">
                                                                            {{ isset( $chamadasAgilizaIndividual->cidadao->nome ) ? $chamadasAgilizaIndividual->cidadao->nome : '' }}
                                                                        </td>
                                                                        <td class="small text-center" style="vertical-align: middle; border-color: red;">
                                                                            {{ isset( $chamadasAgilizaIndividual->data_hora_inicio ) ? datahoraBR( $chamadasAgilizaIndividual->data_hora_inicio ) : '' }}
                                                                        </td>
                                                                        <td class="small text-center" style="vertical-align: middle; border-color: red;">
                                                                            {{ isset( $chamadasAgilizaIndividual->usuariocadastro->name ) ? $chamadasAgilizaIndividual->usuariocadastro->name : '' }}
                                                                        </td>
                                                                        <td class="small text-left" style="vertical-align: middle; border-color: red;">
                                                                            <strong>Problema: </strong>{{ isset( $chamadasAgilizaIndividual->tipoproblema->descricao ) ? $chamadasAgilizaIndividual->tipoproblema->descricao : '' }}<br>
                                                                            <strong>Complemento: </strong>{{ isset( $chamadasAgilizaIndividual->complemento_problema ) ? $chamadasAgilizaIndividual->complemento_problema : '' }}
                                                                        </td>
                                                                        <td class="small text-left" style="vertical-align: middle; border-color: red;">
                                                                            <strong>Solução: </strong>{{ isset( $chamadasAgilizaIndividual->tiposolucao->descricao ) ? $chamadasAgilizaIndividual->tiposolucao->descricao : '' }}<br>
                                                                            <strong>Complemento: </strong>{{ isset( $chamadasAgilizaIndividual->complemento_solucao ) ? $chamadasAgilizaIndividual->complemento_solucao : '' }}
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif

                                <form class="" action="{{ route('chamada.salvar') }}" method="post">
                                    {{ csrf_field() }}


                                    @include('/callcenter/chamada.acoes/_formnovo')

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary" >Salvar</button>
                                        <button type="reset"  class="btn btn-warning" ><a href="/callcenter/chamada/acoes">Cancelar</a></button>
                                    </div>
                                </form>
                                <div>
                                    <strong style="color: red">*</strong> informação obrigatória
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
