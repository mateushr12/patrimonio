@extends('layout_admin.principal')
@section ('breadcrumb','Callcenter / Chamada')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <strong>Pesquisa de C.P.F. para Cadastro de Nova Chamada</strong>
                            </div>

                            <div class="card-body card-block">

                                @if ($errors->any()) <div class="alert alert-danger"> <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul> </div> @endif
                                @if (session('msg')) <div class="alert alert-danger alert-dismissible fade show" role="alert"> {{session('msg')}} <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> @endif

                                <form class="" action="{{route( 'chamada.adicionar', 'cpf' )}}" method="get">
                                    {{ csrf_field() }}

                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="cpf">C.P.F. Cidadão</label>
                                            <input type="text" class="form-control" id="cpf" name="cpf" maxlength="14" placeholder="" value="" OnKeyPress="mascaraCPF(cpf);" 
                                                title="C.P.F. do Cidadão" data-toggle="tooltip" data-placement="top" required autofocus>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="id_agiliza">Protocolo Agiliza</label>
                                            <input type="text" class="form-control" id="id_agiliza" name="id_agiliza" placeholder="" value="" autocomplete="off" maxlength="13" onkeyup="this.value = this.value.toUpperCase();"  
                                                title="Número do Protocolo Agiliza" data-toggle="tooltip" data-placement="top" autofocus>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary" >Pesquisar</button>
                                        <button type="reset"  class="btn btn-warning" ><a href="{{route( 'chamada.index' )}}">Cancelar</a></button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
