@extends('layout_admin.principal')
@section ('breadcrumb','Callcenter / Chamada')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <strong>Cadastro de Chamada - Nova Chamada com C.P.F. Encontrado</strong>
                            </div>

                            <div class="card-body card-block">

                                @if ($errors->any()) <div class="alert alert-danger"> <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul> </div> @endif
                                @if (session('msg')) <div class="alert alert-danger alert-dismissible fade show" role="alert"> {{session('msg')}} <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> @endif

                                @if ( $chamadasAgiliza != '' )
                                    @if ( $chamadasAgiliza->count()  > 0 )
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <strong style="color: red">Ocorrências sobre o Protocolo Agiliza Informado: {{ $agilizaInformado }} - {{ $chamadasAgiliza->count() }} ocorrência(s) ( por data de ocorrência )</strong>
                                                    </div>

                                                    <div class="card-body card-block">
                                                        <table class="table table-bordered table-hover" style="border-color: red;">
                                                            <thead>
                                                                <tr class="bg-light">
                                                                    <td class="info small text-center font-weight-bold" style="vertical-align: middle; border-color: red;">Cidadão</td>
                                                                    <td class="info small text-center font-weight-bold" style="vertical-align: middle; border-color: red;">Data Atendimento</td>
                                                                    <td class="info small text-center font-weight-bold" style="vertical-align: middle; border-color: red;">Atendente</td>
                                                                    <td class="info small text-center font-weight-bold" style="vertical-align: middle; border-color: red;">Problema</td>
                                                                    <td class="info small text-center font-weight-bold" style="vertical-align: middle; border-color: red;">Solução</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach( $chamadasAgiliza as $chamadasAgilizaIndividual )
                                                                    <tr class="bg-light">
                                                                        <td class="small text-center" style="vertical-align: middle; border-color: red;">
                                                                            {{ $chamadasAgilizaIndividual->cidadao_nome }}
                                                                        </td>
                                                                        <td class="small text-center" style="vertical-align: middle; border-color: red;">
                                                                            {{ isset( $chamadasAgilizaIndividual->data_hora_inicio ) ? datahoraBR( $chamadasAgilizaIndividual->data_hora_inicio ) : '' }}
                                                                        </td>
                                                                        <td class="small text-center" style="vertical-align: middle; border-color: red;">
                                                                            {{ $chamadasAgilizaIndividual->usuariocadastro_name }}
                                                                        </td>
                                                                        <td class="small text-left" style="vertical-align: middle; border-color: red;">
                                                                            <strong>Problema: </strong>{{ $chamadasAgilizaIndividual->tipoproblema_descricao }}<br>
                                                                            <strong>Complemento: </strong>{{ isset( $chamadasAgilizaIndividual->complemento_problema ) ? $chamadasAgilizaIndividual->complemento_problema : '' }}
                                                                        </td>
                                                                        <td class="small text-left" style="vertical-align: middle; border-color: red;">
                                                                            <strong>Solução: </strong>{{ $chamadasAgilizaIndividual->tiposolucao_descricao }}<br>
                                                                            <strong>Complemento: </strong>{{ isset( $chamadasAgilizaIndividual->complemento_solucao ) ? $chamadasAgilizaIndividual->complemento_solucao : '' }}
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <br>
                                                                    </td>
                                                                </tr>
                                                            </tfoot>                                                
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif

                                <form class="" action="{{ route('chamada.salvar') }}" method="post">
                                    {{ csrf_field() }}

                                    @include('callcenter.chamada.acoes._formcpfencontrado')

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary" >Salvar</button>
                                        <button type="reset"  class="btn btn-warning" ><a href="/callcenter/chamada/acoes">Cancelar</a></button>
                                    </div>
                                </form>
                                <div>
                                    <strong style="color: red">*</strong> informação obrigatória
                                </div>

                                <div class="card-body card-block">

                                    <div class="row">
                                        <div class="col-8">
                                            <strong>3 Últimos registros referente ao C.P.F.: {{ mask( $cpfinformado, '###.###.###-##' ) }} ( a partir dos últimos protocolos )</strong>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <table class="table table-bordered table-hover">
                                            <tr class="bg-light">
                                                <td class="small text-left">
                                                       <strong>Tipo Cidadão:               </strong> {{ isset( $ultimaschamadas[0]->tipocidadao_descricao ) ? $ultimaschamadas[0]->tipocidadao_descricao : '' }}
                                                     | <strong>Nome:                       </strong> {{ isset( $ultimaschamadas[0]->cidadao_nome ) ? $ultimaschamadas[0]->cidadao_nome : '' }}
                                                     | <strong>Fone Fixo:                  </strong> {{ isset( $ultimaschamadas[0]->cidadao_fone_fixo ) ? mask( $ultimaschamadas[0]->cidadao_fone_fixo, '(##) ####-####' ) : '' }}
                                                     | <strong>Celular:                    </strong> {{ isset( $ultimaschamadas[0]->cidadao_fone_celular ) ? mask( $ultimaschamadas[0]->cidadao_fone_celular, '(##) #####-####' ) : '' }}
                                                     | <strong>Celular2:                   </strong> {{ isset( $ultimaschamadas[0]->cidadao_fone_celular2 ) ? mask( $ultimaschamadas[0]->cidadao_fone_celular2, '(##) #####-####' ) : '' }}
                                                     | <strong>Email:                      </strong> {{ isset( $ultimaschamadas[0]->cidadao_email ) ? $ultimaschamadas[0]->cidadao_email : '' }}
                                                     | <strong>Deseja Receber Informativo: </strong> @if ( isset( $ultimaschamadas[0]->cidadao_receber_informativo ) ) @if ( $ultimaschamadas[0]->cidadao_receber_informativo === 1 ) {{ 'Sim' }} @elseif ( $ultimaschamadas[0]->cidadao->receber_informativo === 0 ) {{ 'Não' }} @else {{ 'Não Informado' }} @endif @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class="row">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr class="bg-light">
                                                    <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Protocolo / Cidadão</td>
                                                    <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Atendimento: Problema / Solução / Agiliza</td>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach( $ultimaschamadas as $individual )
                                                    <tr>
                                                        <td class="small text-left" style="vertical-align: middle;">
                                                                <strong>Protocolo:                  </strong> {{ isset( $individual->protocolo ) ? mask( $individual->protocolo, '#### #### #### #### ####' ) : '' }}
                                                            <br><strong>Ocorrência:                 </strong> {{ isset( $individual->data_hora_inicio ) ? dataBR( $individual->data_hora_inicio ) : '' }} das {{ isset( $individual->data_hora_inicio ) ? horaBR( $individual->data_hora_inicio ) : '' }} às {{ isset( $individual->data_hora_fim ) ? horaBR( $individual->data_hora_fim ) : '' }} (duração: {{ isset( $individual->diferencadatas ) ? horaAnormal( $individual->diferencadatas ) : '' }} )
                                                            <br><strong>Tipo de Atendimento:        </strong> {{ isset( $individual->tipoatendimento_descricao ) ? $individual->tipoatendimento_descricao : '' }}
                                                            <br><strong>Tipo de Problema:           </strong> {{ isset( $individual->tipoproblema_descricao ) ? $individual->tipoproblema_descricao : '' }}
                                                            <br><strong>Complemento do Problema:    </strong> {{ isset( $individual->complemento_problema ) ? $individual->complemento_problema : '' }} 
                                                            <br><strong>Tipo de Solução:            </strong> {{ isset( $individual->tiposolucao_descricao ) ? $individual->tiposolucao_descricao : '' }}
                                                            <br><strong>Complemento da Solução:     </strong> {{ isset( $individual->complemento_solucao ) ? $individual->complemento_solucao : '' }} 
                                                        </td>

                                                        <td class="small text-left" style="vertical-align: middle;">
                                                            <br><strong>Protocolo Agiliza:                    </strong> {{ isset($individual->id_agiliza ) ? $individual->id_agiliza : '' }}
                                                            <br><strong>Usuário que atendeu:                  </strong> {{ $individual->usuariocadastro_name }} {{ $individual->usuariocadastro_grupo_nome != '' ? '(' . $individual->usuariocadastro_grupo_nome . ')' : '' }}
                                                            <br><strong>Redirecionado para Grupo:             </strong> {{ $individual->grupousuariored_grupo_nome }}
                                                            <br><strong>Redirecionado para Usuário:           </strong> {{ $individual->usuariored_name }} {{ $individual->usuariored_grupo_nome != '' ? '(' . $individual->usuariored_grupo_nome . ')' : '' }}
                                                            <br><strong>Data/Hora Solução redirecionamento:   </strong> @if ( isset($individual->data_hora_red_finalizado ) ) <strong class="text-success">{{ datahoraBR( $individual->data_hora_red_finalizado ) }}</strong> @elseif ( !$individual->data_hora_red_finalizado && !$individual->user_id_atendeu_red && ( $individual->gruposuser_id_red || $individual->user_id_red ) ) <strong class="text-danger"> - Pendente -</strong> @endif
                                                            <br><strong>Usuário que atendeu redirecionamento: </strong> @if ( isset($individual->user_id_atendeu_red ) ) <strong class="text-success">{{ $individual->usuarioatendeu_name }} ({{ $individual->usuarioatendeu_grupo_nome }})</strong> @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
