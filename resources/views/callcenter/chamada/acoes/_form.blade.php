@foreach( $individuals as $individual )

<div class="row">
    <div class="form-group col-md-3">
        <label for="cpf">C.P.F. Cidadão</label><strong style="color: red">*</strong>
        <input type="text" class="form-control" id="cpf" name="cpf" maxlength="14" placeholder="" value="" OnKeyPress="mascaraCPF(cpf);" value="{{ isset( $cpfinformado ) ? mask( $cpfinformado, '###.###.###-##' ) : '' }}" 
               title="C.P.F. do Cidadão" data-toggle="tooltip" data-placement="top" required>
    </div>

    <div class="form-group col-md-3">
        <label for="id_agiliza">Protocolo Agiliza</label>
        <input type="text" class="form-control" id="id_agiliza" name="id_agiliza" placeholder=""
               value="{{ isset( $individual->id_agiliza ) ? $individual->id_agiliza : '' }}" autocomplete="off" maxlength="13" onkeyup="this.value = this.value.toUpperCase();"  
               title="Número do Protocolo Agiliza" data-toggle="tooltip" data-placement="top" autofocus>
    </div>

    <div class="form-group col-md-6">
        <label for="nome">Nome do Cidadão</label><strong style="color: red">*</strong>
        <input type="text" class="form-control" id="nome" name="nome" placeholder="" value="{{ isset( $individual->nome ) ? $individual->nome : '' }}" 
        autocomplete="off" maxlength="255" onkeyup="this.value = this.value.toUpperCase();"  
        title="Nome do Cidadão" data-toggle="tooltip" data-placement="top" required>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-2">
        <label for="fone_fixo">Telefone Fixo</label>
        <input type="text" class="form-control" id="fone_fixo" name="fone_fixo" placeholder="" value="{{ isset( $individual->fone_fixo ) ?  mask( $individual->fone_fixo, '(##) ####-####' ) : '' }}" 
        autocomplete="off" maxlength="14" OnKeyPress="mascaraFoneFixo(fone_fixo);" 
        title="Número do Telefone Fixo do Cidadão" data-toggle="tooltip" data-placement="top">
    </div>
    <div class="form-group col-md-2">
        <label for="fone_celular">Telefone Celular</label>
        <input type="text" class="form-control" id="fone_celular" name="fone_celular" placeholder="" value="{{ isset( $individual->fone_celular ) ? mask( $individual->fone_celular, '(##) #####-####' ) : '' }}" 
        autocomplete="off" maxlength="15" OnKeyPress="mascaraFoneCelular(fone_celular);" 
        title="Número do Telefone Celular do Cidadão" data-toggle="tooltip" data-placement="top">
    </div>
    <div class="form-group col-md-2">
        <label for="fone_celular2">Telefone Celular 2</label>
        <input type="text" class="form-control" id="fone_celular2" name="fone_celular2" placeholder="" value="{{ isset( $individual->fone_celular2 ) ? mask( $individual->fone_celular2, '(##) #####-####' ) : '' }}" 
        autocomplete="off" maxlength="15" OnKeyPress="mascaraFoneCelular(fone_celular2);" 
        title="Número de outro Telefone Celular do Cidadão" data-toggle="tooltip" data-placement="top">
    </div>

    <div class="form-group col-md-6">
        <label for="email">E-mail do Cidadão</label><strong style="color: red">*</strong>
        <input type="email" class="form-control" id="email" name="email" placeholder="" value="{{ isset( $individual->email ) ? $individual->email : '' }}" 
        autocomplete="off" maxlength="255" onkeyup="minuscula(this)" 
        title="E-mail do Cidadão" data-toggle="tooltip" data-placement="top" required>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-6">
        <label for="FonteAcao">Tipo de Problema</label><strong style="color: red">*</strong>
        <select class="form-control" id="tipoproblema_id" name="tipoproblema_id" autocomplete="off" 
               title="Tipo de Problema relatado pelo cidadão" data-toggle="tooltip" data-placement="top" required>
            <option value="">Selecione o Tipo de Problema</option>
            @foreach($tipoproblema as $tipo)
                <option {{ $individual->tipoproblema_id == $tipo->tipoproblema_id ? 'selected' : '' }} value="{{ $tipo->tipoproblema_id }}">{{ $tipo->tipoproblema_id }} - {{$tipo->descricao}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-sm-6">
        <label for="FonteAcao">Tipo de Solução</label><strong style="color: red">*</strong>
        <select class="form-control" id="tiposolucao_id" name="tiposolucao_id" autocomplete="off" 
               title="Tipo de Solução escolhido pelo atendente da chamada" data-toggle="tooltip" data-placement="top" required>
            <option value="">Selecione o Tipo de Solução</option>
            @foreach($tiposolucao as $tipo)
                <option {{-- $individual->tiposolucao_id == $tipo->tiposolucao_id ? 'selected' : '' --}} value="{{ $tipo->tiposolucao_id }}">{{ $tipo->tiposolucao_id }} - {{$tipo->descricao}}</option>
            @endforeach
        </select>
    </div>

</div>

<div class="row">
    <div class="form-group col-md-6">
        <label for="complemento_problema">Complemento do Problema</label>
        <input type="text" class="form-control" id="complemento_problema" name="complemento_problema" placeholder=""
               value="{{ isset( $individual->complemento_problema ) ? $individual->complemento_problema : '' }}" autocomplete="off" maxlength="255" onkeyup="this.value = this.value.toUpperCase();" 
               title="Complemento do Tipo de Problema relatado pelo cidadão" data-toggle="tooltip" data-placement="top">
    </div>
    
    <div class="form-group col-md-6">
        <label for="complemento_solucao">Complemento da Solução</label>
        <input type="text" class="form-control" id="complemento_solucao" name="complemento_solucao" placeholder=""
               value="{{ isset( $individual->complemento_solucao ) ? $individual->complemento_solucao : '' }}" autocomplete="off" maxlength="255" onkeyup="this.value = this.value.toUpperCase();" 
               title="Complemento do Tipo de Solução escolhido pelo atendente da chamada" data-toggle="tooltip" data-placement="top">
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-3">
        <label for="gruposuser_id_red">Redirecionar Chamada para grupo</label>
        <select class="form-control" id="gruposuser_id_red" name="gruposuser_id_red" autocomplete="off" 
               title="Indique o grupo para qual esta chamada será redirecionada" data-toggle="tooltip" data-placement="top">
            <option value="">Selecione o Grupo</option>
            @foreach($gruposuserCombo as $tipo)
                <option value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->nome}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-sm-3">
        <label for="user_id_red">Redirecionar Chamada para usuário</label>
        <select class="form-control" id="user_id_red" name="user_id_red" autocomplete="off" 
               title="Indique o usuário para o qual esta chamada será redirecionada" data-toggle="tooltip" data-placement="top">
            <option value="">Selecione o Usuário</option>
            @foreach($usuariosgrupoCombo as $tipo)
                <option value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-sm-6">
        <label for="FonteAcao">Tipo de Cidadão</label><strong style="color: red">*</strong>
        <select class="form-control" id="tipocidadao_id" name="tipocidadao_id" autocomplete="off" 
               title="Tipo de Cidadão informado" data-toggle="tooltip" data-placement="top" required>
            <option value="">Selecione o Tipo de Cidadão</option>  {{-- disabled="disabled" --}}
            @foreach($tipocidadao as $tipo)
                @if ( isset( $individual->tipocidadao_id ) ) { <option {{ $individual->tipocidadao_id == $tipo->tipocidadao_id ? 'selected' : '' }} value="{{ $tipo->tipocidadao_id }}">{{ $tipo->tipocidadao_id }} - {{$tipo->descricao}}</option> }
                @else { <option value="{{ $tipo->tipocidadao_id }}">{{ $tipo->tipocidadao_id }} - {{$tipo->descricao}}</option> } @endif
            @endforeach
        </select>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-6">
        <label for="FonteAcao">Deseja receber informativo JUCESE por e-mail</label><strong style="color: red">*</strong>
        <select class="form-control" id="receber_informativo" name="receber_informativo" 
               title="Informe se o cidadão deseja receber informativo JUCESE por e-mail" data-toggle="tooltip" data-placement="top" required>
            <option value="">Selecione a opção</option>
            <option value="0">NÃO</option>
            <option value="1">SIM</option>
        </select>
    </div>

    <div class="form-group col-sm-6">
        <label for="FonteAcao">Tipo de Atendimento</label><strong style="color: red">*</strong>
        <select class="form-control" id="tipoatendimento_id" name="tipoatendimento_id" autocomplete="off" 
               title="Tipo de Atendimento realizado pelo atendente" data-toggle="tooltip" data-placement="top" required>
            <option value="">Selecione o Tipo de Atendimento</option>
            @foreach($tipoatendimento as $tipo)
                <option {{-- $individuals->tipoatendimento_id == $tipo->tipoatendimento_id ? 'selected' : '' --}} value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->descricao}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="row">
    <input type="hidden" name="protocolo" value="0" />
    <input type="hidden" name="user_idOLD" value="{{ $individual->user_id }}" />
    <input type="hidden" name="idOLD" value = {{ $individual->id }} />
    <input type="hidden" name="data_hora_inicioOLD" value = {{ $individual->data_hora_inicio }} />
    <input type="hidden" name="hora_inicioOLD" value = {{ date('H:i:s') }} />
    <input type="hidden" name="data_hora_fimOLD" value = {{ $individual->data_hora_fim }} />
    <input type="hidden" name="gruposuser_user_idOLD" value = {{ $individual->gruposuser_id }} />
    <input type="hidden" name="data_hora_red_finalizadoOLD" value = {{ $individual->data_hora_red_finalizado }} />
    {{-- <input type="hidden" name="user_id_atendeu_redOLD" value = {{ $individual->user_id_atendeu_red }} /> --}}
    <input type="hidden" name="cpfOLD" value="{{ $individual->cpf }}" />
    <input type="hidden" name="tipocidadao_user_idOLD" value="{{ $individual->tipocidadao_id }}" />
    <input type="hidden" name="fone_fixoOLD" value="{{ $individual->fone_fixo }}" />
    <input type="hidden" name="fone_celularOLD" value="{{ $individual->fone_celular }}" />
    <input type="hidden" name="fone_celular2OLD" value="{{ $individual->fone_celular2 }}" />
    <input type="hidden" name="nomeOLD" value="{{ $individual->nome }}" />
    <input type="hidden" name="emailOLD" value="{{ $individual->email }}" />
    <input type="hidden" name="id_agilizaOLD" value="{{ $individual->id_agiliza }}" />
    <input type="hidden" name="tipoproblema_user_idOLD" value="{{ $individual->tipoproblema_id }}" />
    <input type="hidden" name="complemento_problemaOLD" value="{{ $individual->complemento_problema }}" />
    <input type="hidden" name="tiposolucao_user_idOLD" value="{{ $individual->tiposolucao_id }}" />
    <input type="hidden" name="complemento_solucaoOLD" value="{{ $individual->complemento_solucao }}" />
    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}" />
    <input type="hidden" name="id" value = '' />
    <input type="hidden" name="data_hora_inicio" value = {{ $datainicial }} />
    <input type="hidden" name="hora_inicio" value = {{ date('H:i:s') }} />
    <input type="hidden" name="data_hora_fim" value = {{-- $individual->data_hora_fim --}} />
    <input type="hidden" name="data_hora_red_finalizado" />
    <input type="hidden" name="user_id_atendeu_red" value="{{ auth()->user()->id }}" />
</div>

@endforeach

{{-- @endforeach --}}
