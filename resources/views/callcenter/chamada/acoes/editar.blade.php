@extends('layout_admin.principal')
@section ('breadcrumb','Callcenter / Chamada')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <strong>Editar Chamada</strong>{{ $complementotitulo }}
                            </div>
                            <div class="card-body card-block">

                                @if ($errors->any()) <div class="alert alert-danger"> <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul> </div> @endif
                                @if (session('msg')) <div class="alert alert-danger alert-dismissible fade show" role="alert"> {{session('msg')}} <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> @endif

                                <form class="" action="{{ route( '/callcenter/chamada.acoes/atualizar', $chave ) }}" method="post">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="_method" value="put">

                                    @include('/callcenter/chamada.acoes/_form')


                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Atualizar</button>
                                        <button type="reset"  class="btn btn-warning" ><a href="/callcenter/chamada/acoes">Cancelar</a></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
