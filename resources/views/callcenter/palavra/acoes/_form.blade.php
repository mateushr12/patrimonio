<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="descricao_tipocidadao">Descrição do Tipo de Cidadão</label><strong style="color: red">*</strong>
            <input type="text" class="form-control" id="descricao" name="descricao" placeholder=""
                   value="{{isset($individual->descricao) ? $individual->descricao : ''}}" autocomplete="off" maxlength="255" onkeyup="this.value = this.value.toUpperCase();" 
               title="Descrição do Tipo de Cidadão" data-toggle="tooltip" data-placement="top" required autofocus>
        </div>
        <input type="hidden" name="user_id" value="{{auth()->user()->id}}" />
        {{--
        <div class="form-group">
            <label for="id">Usuário Logado</label>
            <input type="text" class="form-control" id="id" name="id" placeholder="Usuário Logado"
                   value="{{ auth()->user()->id }} - {{ auth()->user()->name }}" autocomplete="off" required>
        </div>
        --}}
    </div>
</div>
