@extends('layout_admin.principal')
@section ('breadcrumb','Callcenter / Palavra')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                   <div class="col-3"><strong>Lista de Palavras</strong></div>
                                    
                                    <div class="col-6">
                                        <form class="form-inline" action="{{ route( 'palavra.index' ) }}" method="post">
                                            {{ csrf_field() }}
                                            <div class="form-group"><input type="hidden" name="_method" value="get"></div>

                                            <div class="form-group">
                                                <input type="text" class="form-control" id="palavra" name="palavra" value="{{ isset($filtro) ? $filtro : '' }}" autocomplete="off" maxlength="255" onkeyup="" 
                                                        title="Informe todo / parte do texto que deseja procurar ( % no meio do texto = qualquer texto ). Será pesquisado em descrição" data-toggle="tooltip" data-placement="top">
                                                <button type="submit" class="btn btn-primary">Buscar</button>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="col-3"><button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#FormModal"><i class="zmdi zmdi-plus"></i>Nova Palavra</button></div>
                                </div>
                                
                                @if ( count( session()->get( 'individuals' ) ) > 0 )
                                    <div class="row">
                                        <div class="col-2"><a href="{{ route( 'palavra.imprimir', compact( 'titulo' ) ) }}"><button type="button" class="item" title="Imprimir" data-toggle="tooltip" data-placement="top"><i class="zmdi zmdi-print"></i> Imprimir</button></a></div>
                                    </div>
                                @endif
                            </div>

                            <div class="row">
                                <textarea class="col-12" rows="10">@foreach( $arraySQL as $key => $arraySQLIndividual ) {{ $arraySQLIndividual . "\n" }} @endforeach</textarea>
                            </div>

                            <div class="card-body card-block">

                                @if ($errors->any()) <div class="alert alert-danger"> <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul> </div> @endif
                                @if (session('msg')) <div class="alert alert-danger alert-dismissible fade show" role="alert"> {{session('msg')}} <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> @endif

                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light">
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">ID</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Palavra</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Correção</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Sugestão</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle; width: 80px;">Ação</td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach( session()->get( 'individuals' ) as $key => $individual )
                                            <tr>
                                                <td class="small text-center" style="vertical-align: middle;">{{ $individual->id }}</td>
                                                <td class="small text-left" style="vertical-align: middle;">{{ $individual->palavra }}</td>
                                                <td class="small text-left" style="vertical-align: middle;">{{ isset( $individual->correcao ) ? $individual->correcao : '' }}</td>
                                                <td class="small text-left" style="vertical-align: middle;">{{ isset( $individual->sugestao ) ? $individual->sugestao : '' }}</td>

                                                <td class="text-center" style="vertical-align: middle;">
                                                    <div class="table-data-feature">
                                                        @if ( in_array('callcenter/palavra/acoes/editar/{id}', Session::get('permissoes.nomes')) )
                                                            <a href="{{ route('palavra.editar', $individual->id) }}"><button type="button" class="item" title="Editar" data-toggle="tooltip" data-placement="top"><i class="zmdi zmdi-edit"></i></button></a>
                                                            &nbsp;
                                                        @else @endif
                                                        @if ( (in_array('callcenter/palavra/acoes/deletar/{id}', Session::get('permissoes.nomes')) ) && ( $individual->excluivel == 'Sim' ) )
                                                            <a href="{{ route('palavra.deletar', $individual->id) }}" data-confirm="Tem certeza que deseja excluir o item selecionado?"><button type="button" class="item" title="Deletar"data-toggle="tooltip" data-placement="top"><i class="zmdi zmdi-delete"></i></button></a>
                                                        @else @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>

                                </table>

                                <div class="row d-flex justify-content-center">
                                    {{ session()->get( 'individuals' )->links() }}

                                    <form class="form-inline" action="{{ route( 'palavra.index' ) }}" method="post"><div class="form-group"><input type="hidden" name="_method" value="get" id="itensPorPagina"></div>          Exibir:     <button type="submit" class="btn btn-primary" name="itensPorPagina5" id="itensPorPagina5">5</button></form>
                                    <form class="form-inline" action="{{ route( 'palavra.index' ) }}" method="post"><div class="form-group"><input type="hidden" name="_method" value="get"></div>     <button type="submit" class="btn btn-primary" name="itensPorPagina10" id="itensPorPagina10">10</button></form>
                                    <form class="form-inline" action="{{ route( 'palavra.index' ) }}" method="post"><div class="form-group"><input type="hidden" name="_method" value="get"></div>     <button type="submit" class="btn btn-primary" name="itensPorPaginaTodos" id="itensPorPaginaTodos">100</button></form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- modal form NOVA AÇÃO-->
    <div class="modal fade" id="FormModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="FormModalLabel">Cadastro de Palavra</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body card-block">
                            <form class="" action="{{route('palavra.salvar')}}" method="post">
                            {{ csrf_field() }}
                            @include('callcenter.palavra.acoes.novo')
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">
                        Salvar
                    </button>
                    <button type="reset" class="btn btn-warning" data-dismiss="modal">
                        Cancelar
                    </button>
                </div>
                <div style="text-align: right;">
                    <strong style="color: red">*</strong> informação obrigatória
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end modal form NOVA AÇÃO -->
@endsection
