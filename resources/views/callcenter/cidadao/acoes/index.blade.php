@extends('layout_admin.principal')
@section ('breadcrumb','Callcenter / Cidadão')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                   <div class="col-5"><strong>Lista de Cidadãos ( {{ number_format( count( session()->get( 'individuals' ) ), 0, ',', '.' ) }} registros existentes )</strong></div>
                                    
                                    <div class="col-5">
                                        <form class="form-inline" action="{{ route( 'cidadao.index' ) }}" method="post">
                                            {{ csrf_field() }}
                                            <div class="form-group"><input type="hidden" name="_method" value="get"></div>

                                            {{--
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="nome" name="nome" value="{{ isset($filtro) ? $filtro : '' }}" autocomplete="off" maxlength="255" onkeyup="" 
                                                        title="Informe todo / parte do texto que deseja procurar ( % no meio do texto = qualquer texto ). Será pesquisado em nome, e-mail, telefones e C.P.F." data-toggle="tooltip" data-placement="top">
                                                <button type="submit" class="btn btn-primary">Buscar</button>
                                            </div>
                                            --}}
                                            <div class="">
                                                <form class="form-inline" action="{{ route( 'cidadao.index' ) }}" method="post">
                                                {{ csrf_field() }}
                                                    <div class="form-group">
                                                        <select class="form-control" id="tipocidadao_id" name="tipocidadao_id" 
                                                            title="Tipo de Cidadão" data-toggle="tooltip" data-placement="top">
                                                            <option value = 0>Todos</option>
                                                            @foreach($tipocidadao as $keyitem => $tipo)
                                                                @if ( isset( $tipocidadao_id_escolhido ) && $tipocidadao_id_escolhido == $tipocidadao[ $keyitem ]->id ) 
                                                                { 
                                                                    <option {{ $tipocidadao_id = $tipocidadao[ $keyitem ]->id ? 'selected' : '' }} value="{{ $tipocidadao[ $keyitem ]->id }}">{{ $tipocidadao[ $keyitem ]->id }} - {{ $tipocidadao[ $keyitem ]->tipocidadao->descricao }} - {{ $tipocidadao[ $keyitem ]->contador }} registros</option> 
                                                                }
                                                                @else 
                                                                { 
                                                                    <option value="{{ $tipocidadao[ $keyitem ]->id }}">{{ $tipocidadao[ $keyitem ]->id }} - {{ $tipocidadao[ $keyitem ]->tipocidadao->descricao }} - {{ $tipocidadao[ $keyitem ]->contador }} registros</option> 
                                                                } 
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                        <button type="submit" class="small btn btn-primary">Aplicar Filtro</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="col-2"><button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#FormModal"><i class="zmdi zmdi-plus"></i>Novo Cidadão</button></div>
                                </div>

                                @if ( count( session()->get( 'individuals' ) ) > 0 )
                                    <div class="row col-12">
                                        <div class="form-group"><input type="hidden" name="_method" value="get"></div>

                                        <div class="col-4"><a target="_blank" href="{{ route( 'cidadao.imprimir', compact( 'titulo' ) ) }}"><button type="button" class="item" title="Imprimir" data-toggle="tooltip" data-placement="top"><i class="zmdi zmdi-print"></i> Imprimir</button></a></div>
                                    </div>
                                @endif
                            </div>

                            <div class="card-body card-block">

                                @if ($errors->any()) <div class="alert alert-danger"> <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul> </div> @endif
                                @if (session('msg')) <div class="alert alert-danger alert-dismissible fade show" role="alert"> {{session('msg')}} <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> @endif

                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light">
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">ID</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Nome</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Telefones</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Recebe Informativo JUCESE?</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Tipo de Cidadão</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Criação/Modificação</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Usuário (cadastro/alteração)</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle; width: 80px;">Ação</td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach( session()->get( 'individuals' ) as $key => $individual )
                                            <tr>
                                                <td class="small text-center" style="vertical-align: middle;">{{ isset( $individual->id ) ? $individual->id : '' }}</td>
                                                <td class="small text-left" style="vertical-align: middle;">
                                                    <strong>Nome: </strong>{{ isset( $individual->nome ) ? $individual->nome : '' }}<br>
                                                    <strong>C.P.F.: </strong>{{ isset( $individual->cpf ) ? mask( $individual->cpf, '###.###.###-##' ) : '' }}<br>
                                                    <strong>E-mail: </strong>{{ isset( $individual->email ) ? $individual->email : '' }}
                                                </td>
                                                <td class="small text-left" style="vertical-align: middle;">
                                                    <strong>Fone Fixo: </strong>{{ isset( $individual->fone_fixo ) ? mask( $individual->fone_fixo, '(##) ####-####' ) : '' }}<br>
                                                    <strong>Fone Celular: </strong>{{ isset( $individual->fone_celular ) ? mask( $individual->fone_celular, '(##) #####-####' ) : '' }}<br>
                                                    <strong>Fone Celular 2: </strong>{{ isset( $individual->fone_celular2 ) ? mask( $individual->fone_celular2, '(##) #####-####' ) : '' }}
                                                </td>
                                                
                                                @if ( $individual->receber_informativo == 0 )
                                                    <td class="small text-center" style="vertical-align: middle;">Não</td>
                                                @elseif ( $individual->receber_informativo == 1 )
                                                    <td class="small text-center" style="vertical-align: middle;">Sim</td>
                                                @else
                                                    <td class="small text-center" style="vertical-align: middle;">Não Informado</td>
                                                @endif

                                                <td class="small text-center" style="vertical-align: middle;">{{ isset( $individual->tipocidadao->descricao ) ? $individual->tipocidadao->descricao : '' }}</td>

                                                <td class="small text-left" style="vertical-align: middle;">
                                                    <strong>Criação: </strong>{{ isset( $individual->created_at ) ? datahoraBR( $individual->created_at ) : '' }} <br>
                                                    <strong>Modificação: </strong>{{ isset( $individual->updated_at ) ? datahoraBR( $individual->updated_at ) : '' }}
                                                </td>
                                                <td class="small text-center" style="vertical-align: middle;">
                                                    {{ isset( $individual->user->name ) ? $individual->user->name : '' }} ({{ isset( $individual->user->grupo->nome ) ? $individual->user->grupo->nome : '' }})
                                                </td>

                                                <td class="text-center" style="vertical-align: middle;">
                                                    <div class="table-data-feature">
                                                        @if ( in_array('callcenter/cidadao/acoes/editar/{id}', Session::get('permissoes.nomes')) )
                                                            <a href="{{ route('cidadao.editar', $individual->id) }}"><button type="button" class="item" title="Editar" data-toggle="tooltip" data-placement="top"><i class="zmdi zmdi-edit"></i></button></a>
                                                            &nbsp;
                                                        @else @endif
                                                        @if ( (in_array('callcenter/cidadao/acoes/deletar/{id}', Session::get('permissoes.nomes')) ) && ( $individual->excluivel == 'Sim' ) )
                                                            <a href="{{ route('cidadao.deletar', $individual->id) }}" data-confirm="Tem certeza que deseja excluir o item selecionado?"><button type="button" class="item" title="Deletar"data-toggle="tooltip" data-placement="top"><i class="zmdi zmdi-delete"></i></button></a>
                                                        @else @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="row d-flex justify-content-center">
                                    {{--
                                        {{ session()->get( 'individuals' )->links() }}

                                    <form class="form-inline" action="{{ route( 'cidadao.index' ) }}" method="post"><div class="form-group"><input type="hidden" name="_method" value="get" id="itensPorPagina"></div>          Exibir:     <button type="submit" class="btn btn-primary" name="itensPorPagina5" id="itensPorPagina5">5</button></form>
                                    <form class="form-inline" action="{{ route( 'cidadao.index' ) }}" method="post"><div class="form-group"><input type="hidden" name="_method" value="get"></div>     <button type="submit" class="btn btn-primary" name="itensPorPagina10" id="itensPorPagina10">10</button></form>
                                    <form class="form-inline" action="{{ route( 'cidadao.index' ) }}" method="post"><div class="form-group"><input type="hidden" name="_method" value="get"></div>     <button type="submit" class="btn btn-primary" name="itensPorPaginaTodos" id="itensPorPaginaTodos">100</button></form>
                                    --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- modal form NOVA AÇÃO-->
    <div class="modal fade" id="FormModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="FormModalLabel">Cadastro de Cidadão</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body card-block">
                            <form class="" action="{{route('cidadao.salvar')}}" method="post">
                                {{ csrf_field() }}
                                @include('callcenter.cidadao.acoes.novo')
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">
                        Salvar
                    </button>
                    <button type="reset" class="btn btn-warning" data-dismiss="modal">
                        Cancelar
                    </button>
                </div>
                <div style="text-align: right;">
                    <strong style="color: red">*</strong> informação obrigatória
                </div>
            </div>
        </div>
    </div>
    <!-- end modal form NOVA AÇÃO -->
@endsection
