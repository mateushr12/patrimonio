@include('layout_admin._includes.topo_print_callcenter')
    <table class="table table-bordered table-hover">
        <caption></caption>
        <thead>
            <tr class="bg-light">
                <th style="text-align: center;">ID</th>
                <th style="text-align: left;">Cidadão</th>
            </tr>
        </thead>

        <tbody>
        @if(session()->has('individuals'))
            @foreach( session()->get( 'individuals' ) as $key => $individual )
                <tr>
                    <td style="text-align: center;">{{ isset( session()->get( 'individuals' )[ $key ]->id ) ? session()->get( 'individuals' )[ $key ]->id : '' }}</td>
                    <td><strong>Nome: </strong>{{ isset( session()->get( 'individuals' )[ $key ]->nome ) ? session()->get( 'individuals' )[ $key ]->nome : '' }}<br>
                        <strong>C.P.F.: </strong>{{ isset( $individual->cpf ) ? mask( $individual->cpf, '###.###.###-##' ) : '' }} | 
                        <strong>E-mail: </strong>{{ isset( $individual->email ) ? $individual->email : '' }}<br>

                        <strong>Fone Fixo: </strong>{{ isset( $individual->fone_fixo ) ? mask( $individual->fone_fixo, '(##) ####-####' ) : '' }} | 
                        <strong>Fone Celular: </strong>{{ isset( $individual->fone_celular ) ? mask( $individual->fone_celular, '(##) #####-####' ) : '' }} | 
                        <strong>Fone Celular 2: </strong>{{ isset( $individual->fone_celular2 ) ? mask( $individual->fone_celular2, '(##) #####-####' ) : '' }}<br>

                        <strong>Tipo de Cidadão: </strong>{{ isset( $individual->tipocidadao->descricao ) ? $individual->tipocidadao->descricao : '' }}<br>

                        <strong>Usuário: </strong>{{ isset( session()->get( 'individuals' )[ $key ]->usuariocadastro->name ) ? session()->get( 'individuals' )[ $key ]->usuariocadastro->name : '' }} ({{ isset( session()->get( 'individuals' )[ $key ]->usuariocadastro->grupo->nome ) ? session()->get( 'individuals' )[ $key ]->usuariocadastro->grupo->nome : '' }} ) | 
                        <strong>Criação: </strong>{{ isset( session()->get( 'individuals' )[ $key ]->created_at ) ? datahoraBR( session()->get( 'individuals' )[ $key ]->created_at ) : '' }} | 
                        <strong>Modificação: </strong>{{ isset( session()->get( 'individuals' )[ $key ]->updated_at ) ? datahoraBR( session()->get( 'individuals' )[ $key ]->updated_at ) : '' }}

                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</section>
@include('layout_admin._includes.footer_print_callcenter')
