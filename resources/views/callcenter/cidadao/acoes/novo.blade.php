<div class="row">
    <div class="form-group col-md-3">
        <label for="cpf">C.P.F. Cidadão</label><strong style="color: red">*</strong>
        <input type="text"
               class="form-control"
               id="cpf"
               name="cpf"
               placeholder=""
               value=""
               maxlength="14"
               OnKeyPress="mascaraCPF(cpf);" 
               title="C.P.F. do Cidadão" data-toggle="tooltip" data-placement="top"
               required>
               {{-- onkeypress="return event.charCode >= 48 && event.charCode <= 57" required> --}}
    </div>
    
    <div class="form-group col-md-9">
        <label for="nome">Nome do Cidadão</label><strong style="color: red">*</strong>
        <input type="text" class="form-control" id="nome" name="nome" placeholder="" value="" autocomplete="off" maxlength="255" onkeyup="this.value = this.value.toUpperCase();" 
               title="Nome do cidadão" data-toggle="tooltip" data-placement="top" required autofocus>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-12">
        <label for="email">E-mail do Cidadão</label><strong style="color: red">*</strong>
        <input type="email" class="form-control" id="email" name="email" placeholder="" value="" autocomplete="off" maxlength="255" onkeyup="minuscula(this)" 
               title="E-mail do cidadão" data-toggle="tooltip" data-placement="top" required>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        <label for="fone_fixo">Telefone Fixo</label>
        <input type="text" class="form-control" id="fone_fixo" name="fone_fixo" placeholder="" value="" autocomplete="off" maxlength="14" OnKeyPress="mascaraFoneFixo(fone_fixo);" 
               title="Número do Telefone Fixo do Cidadão" data-toggle="tooltip" data-placement="top">
    </div>
    
    <div class="form-group col-md-4">
        <label for="fone_celular">Telefone Celular</label>
        <input type="text" class="form-control" id="fone_celular" name="fone_celular" placeholder="" value="" autocomplete="off" maxlength="15" OnKeyPress="mascaraFoneCelular(fone_celular);" 
               title="Número do Telefone Celular do Cidadão" data-toggle="tooltip" data-placement="top">
    </div>
    
    <div class="form-group col-md-4">
        <label for="fone_celular2">Telefone Celular 2</label>
        <input type="text" class="form-control" id="fone_celular2" name="fone_celular2" placeholder="" value="" autocomplete="off" maxlength="15" OnKeyPress="mascaraFoneCelular(fone_celular2);" 
               title="Número de outro Telefone Celular do Cidadão" data-toggle="tooltip" data-placement="top">
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-6">
        <label for="FonteAcao">Tipo de Cidadão:</label><strong style="color: red">*</strong>

        <select class="form-control" id="tipocidadao_id" name="tipocidadao_id" autocomplete="off" 
               title="Tipo de cidadão" data-toggle="tooltip" data-placement="top" 
               title="Tipo de Cidadão informado" data-toggle="tooltip" data-placement="top" required>
            <option value="">Selecione o Tipo de Cidadão</option>
            @foreach($tipocidadao as $tipo)
                <option value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->descricao}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-sm-6">
        <label for="FonteAcao">Deseja receber informativo JUCESE por e-mail:</label><strong style="color: red">*</strong>

        <select class="form-control" name="receber_informativo" 
               title="Informe se o cidadão deseja receber informativo JUCESE por e-mail" data-toggle="tooltip" data-placement="top" required>
            <option value="">Selecione a opção</option>
            <option value="0">NÃO</option>
            <option value="1">SIM</option>
        </select>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <input type="hidden" name="user_id" value="{{auth()->user()->id}}" />

        {{--
            <div class="form-group">
                <label for="id">Usuário Logado</label>
                <input type="text" class="form-control" id="id" name="id" placeholder="Usuário Logado"
                    value="{{ auth()->user()->id }} - {{ auth()->user()->name }}" autocomplete="off" required>
            </div>
        --}}
    </div>
</div>
