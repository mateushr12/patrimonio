<div class="row">
    <div class="form-group col-md-3">
        <label for="cpf">C.P.F. Cidadão</label><strong style="color: red">*</strong>
        <input type="text"
               class="form-control"
               id="cpf"
               name="cpf"
               placeholder=""
               value="{{ isset( $individual->cpf ) ? mask( $individual->cpf, '###.###.###-##' ) : '' }}"
               maxlength="14"
               OnKeyPress="mascaraCPF(cpf);" 
               title="C.P.F. do Cidadão" data-toggle="tooltip" data-placement="top"
               required>
    </div>
    <div class="form-group col-md-9">
        <label for="nome">Nome do Cidadão</label><strong style="color: red">*</strong>
        <input type="text" class="form-control" id="nome" name="nome" placeholder=""
               value="{{ isset( $individual->nome ) ? $individual->nome : '' }}"
               autocomplete="off" maxlength="255" onkeyup="this.value = this.value.toUpperCase();"  
               title="Nome do Cidadão" data-toggle="tooltip" data-placement="top" required autofocus>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-8">
        <label for="email">E-mail do Cidadão</label><strong style="color: red">*</strong>
        <input type="email" class="form-control" id="email" name="email" placeholder=""
               value="{{ isset( $individual->email ) ? $individual->email : '' }}" autocomplete="off" maxlength="255" onkeyup="minuscula(this)" 
               title="E-mail do Cidadão" data-toggle="tooltip" data-placement="top" required>
    </div>
    <div class="form-group col-sm-4">
        <label for="FonteAcao">Tipo de Cidadão:</label><strong style="color: red">*</strong>
        <select class="form-control" id="tipocidadao_id" name="tipocidadao_id" autocomplete="off" 
               title="Tipo de Cidadão informado" data-toggle="tooltip" data-placement="top" required>
            <option value="">Selecione o Tipo de Cidadão</option>  {{-- disabled="disabled" --}}
            @foreach($tipocidadao as $tipo)
                @if ( isset( $individual->tipocidadao_id ) ) { <option {{ $individual->tipocidadao_id == $tipo->id ? 'selected' : '' }} value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->descricao}}</option> }
                @else { <option value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->descricao}}</option> } @endif
            @endforeach
        </select>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-3">
        <label for="fone_fixo">Telefone Fixo</label>
        <input type="text" class="form-control" id="fone_fixo" name="fone_fixo" placeholder=""
               value="{{ isset( $individual->fone_fixo ) ?  mask( $individual->fone_fixo, '(##) ####-####' ) : '' }}" autocomplete="off" maxlength="14" OnKeyPress="mascaraFoneFixo(fone_fixo);" 
               title="Número do Telefone Fixo do Cidadão" data-toggle="tooltip" data-placement="top">
    </div>
    <div class="form-group col-md-3">
        <label for="fone_celular">Telefone Celular</label>
        <input type="text" class="form-control" id="fone_celular" name="fone_celular" placeholder=""
               value="{{ isset( $individual->fone_celular ) ? mask( $individual->fone_celular, '(##) #####-####' ) : '' }}" autocomplete="off" maxlength="15" OnKeyPress="mascaraFoneCelular(fone_celular);" 
               title="Número do Telefone Celular do Cidadão" data-toggle="tooltip" data-placement="top">
    </div>
    <div class="form-group col-md-3">
        <label for="fone_celular2">Telefone Celular 2</label>
        <input type="text" class="form-control" id="fone_celular2" name="fone_celular2" placeholder=""
               value="{{ isset( $individual->fone_celular2 ) ? mask( $individual->fone_celular2, '(##) #####-####' ) : '' }}" autocomplete="off" maxlength="15" OnKeyPress="mascaraFoneCelular(fone_celular2);" 
               title="Número de outro Telefone Celular do Cidadão" data-toggle="tooltip" data-placement="top">
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-6"><strong style="color: red">*</strong>
        <label for="FonteAcao">Deseja receber informativo JUCESE por e-mail:</label>
        <select class="form-control" name="receber_informativo" 
               title="Informe se o cidadão deseja receber informativo JUCESE por e-mail" data-toggle="tooltip" data-placement="top" required>
            <option value="">Selecione a opção</option>
            <option {{ $individual->receber_informativo == 0 ? 'selected' : '' }} value="{{ 0 }}">{{ 0 }} - {{ "Não" }}</option>''
            <option {{ $individual->receber_informativo == 1 ? 'selected' : '' }} value="{{ 1 }}">{{ 1 }} - {{ "Sim" }}</option>''
        </select>
    </div>
</div>

<div class="row">
    <input type="hidden" name="user_id" value="{{auth()->user()->id}}" />
</div>
