@extends('layout_admin.principal')
@section ('breadcrumb','Callcenter / Tipo de Solução')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <strong>Lista de Tipos de Solução</strong>
                            </div>

                            <div class="card-body card-block">

                                <form class="" action="{{route('tiposolucao')}}" method="post">
                                    {{ csrf_field() }}
                                    @include('callcenter.tiposolucao.acoes._form')

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary" >Salvar</button>
                                        <button type="reset"  class="btn btn-warning" >Cancelar</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
