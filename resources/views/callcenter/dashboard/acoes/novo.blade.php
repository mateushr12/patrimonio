<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="descricao">Descrição do Tipo de Solução</label>
            <input type="text" class="form-control" id="descricao" name="descricao" placeholder=""
                   value="" autocomplete="off" maxlength="255" onkeyup="this.value = this.value.toUpperCase();" 
               title="Descrição do Tipo de Solução" data-toggle="tooltip" data-placement="top" required>
        </div>
        <input type="hidden" name="user_id" value="{{auth()->user()->id}}" />
        {{--
        <div class="form-group">
            <label for="id">Usuário Logado</label>
            <input type="text" class="form-control" id="id" name="id" placeholder="Usuário Logado"
                   value="{{ auth()->user()->id }} - {{ auth()->user()->name }}" autocomplete="off" required>
        </div>
        --}}
    </div>
</div>
