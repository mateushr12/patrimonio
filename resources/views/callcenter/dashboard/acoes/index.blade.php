@extends('layout_admin.principal')
@section ('breadcrumb', 'Callcenter / Dashboard')

@section('conteudo')


<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div><h2>Dashboard</h2></div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-12">
                                    <ul class="small nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" id="pills-operacional-tab" data-toggle="pill" href="#pills-operacional" role="tab" aria-controls="pills-operacional" aria-selected="true">Operacional</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-tatico-tab"      data-toggle="pill" href="#pills-tatico"      role="tab" aria-controls="pills-tatico"      aria-selected="false">Tático</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-estrategico-tab" data-toggle="pill" href="#pills-estrategico" role="tab" aria-controls="pills-estrategico" aria-selected="false">Estratégico</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="tab-content">

                                <div class="tab-pane fade show active" id="pills-operacional" role="tabpanel" aria-labelledby="pills-operacional-tab">
                                    <ul class="small nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
                                        <li class="nav-item"><a class="nav-link active" id="pills-operacional-tab-sub1" data-toggle="pill" href="#pills-operacional-sub1" role="tab" aria-controls="pills-operacional-sub1" aria-selected="true">Problemas Recorrentes</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-operacional-tab-sub2" data-toggle="pill" href="#pills-operacional-sub2" role="tab" aria-controls="pills-operacional-sub2" aria-selected="false">Soluções Recorrentes</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-operacional-tab-sub3" data-toggle="pill" href="#pills-operacional-sub3" role="tab" aria-controls="pills-operacional-sub3" aria-selected="false">Produtividade Atendentes</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-operacional-tab-sub4" data-toggle="pill" href="#pills-operacional-sub4" role="tab" aria-controls="pills-operacional-sub4" aria-selected="false">Operacional Sub 4</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-operacional-tab-sub5" data-toggle="pill" href="#pills-operacional-sub5" role="tab" aria-controls="pills-operacional-sub5" aria-selected="false">Operacional Sub 5</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-operacional-tab-sub6" data-toggle="pill" href="#pills-operacional-sub6" role="tab" aria-controls="pills-operacional-sub6" aria-selected="false">Operacional Sub 6</a></li>
                                    </ul>
                                </div>

                                <div class="tab-pane fade show" id="pills-tatico" role="tabpanel" aria-labelledby="pills-tatico-tab">
                                    
                                    Texto correspondente ao módulo Tático.
                                    
                                    <ul class="small nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
                                        <li class="nav-item"><a class="nav-link       " id="pills-tatico-tab-sub1" data-toggle="pill" href="#pills-tatico-sub1" role="tab" aria-controls="pills-tatico-sub1" aria-selected="true">Tático Sub 1</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-tatico-tab-sub2" data-toggle="pill" href="#pills-tatico-sub2" role="tab" aria-controls="pills-tatico-sub2" aria-selected="false">Tático Sub 2</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-tatico-tab-sub3" data-toggle="pill" href="#pills-tatico-sub3" role="tab" aria-controls="pills-tatico-sub3" aria-selected="false">Tático Sub 3</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-tatico-tab-sub4" data-toggle="pill" href="#pills-tatico-sub4" role="tab" aria-controls="pills-tatico-sub4" aria-selected="false">Tático Sub 4</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-tatico-tab-sub5" data-toggle="pill" href="#pills-tatico-sub5" role="tab" aria-controls="pills-tatico-sub5" aria-selected="false">Tático Sub 5</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-tatico-tab-sub6" data-toggle="pill" href="#pills-tatico-sub6" role="tab" aria-controls="pills-tatico-sub6" aria-selected="false">Tático Sub 6</a></li>
                                    </ul>
                                </div>

                                <div class="tab-pane fade" id="pills-estrategico" role="tabpanel" aria-labelledby="pills-estrategico-tab">
                                    
                                    Texto correspondente ao módulo Estratégico.
                                    
                                    <ul class="small nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
                                        <li class="nav-item"><a class="nav-link       " id="pills-estrategico-tab-sub1" data-toggle="pill" href="#pills-estrategico-sub1" role="tab" aria-controls="pills-estrategico-sub1" aria-selected="true">Estratégico Sub 1</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-estrategico-tab-sub2" data-toggle="pill" href="#pills-estrategico-sub2" role="tab" aria-controls="pills-estrategico-sub2" aria-selected="false">Estratégico Sub 2</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-estrategico-tab-sub3" data-toggle="pill" href="#pills-estrategico-sub3" role="tab" aria-controls="pills-estrategico-sub3" aria-selected="false">Estratégico Sub 3</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-estrategico-tab-sub4" data-toggle="pill" href="#pills-estrategico-sub4" role="tab" aria-controls="pills-estrategico-sub4" aria-selected="false">Estratégico Sub 4</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-estrategico-tab-sub5" data-toggle="pill" href="#pills-estrategico-sub5" role="tab" aria-controls="pills-estrategico-sub5" aria-selected="false">Estratégico Sub 5</a></li>
                                        <li class="nav-item"><a class="nav-link"        id="pills-estrategico-tab-sub6" data-toggle="pill" href="#pills-estrategico-sub6" role="tab" aria-controls="pills-estrategico-sub6" aria-selected="false">Estratégico Sub 6</a></li>
                                    </ul>
                                </div>

                            </div>

                            <div class="tab-content">

                                <div class="tab-pane fade show active" id="pills-operacional-sub1" role="tabpanel" aria-labelledby="pills-operacional-tab-sub1">{{-- Operacional / Problemas Recorrentes --}}

                                    <div class="row col-12">{{-- Problemas Recorrentes --}}
                                        <div class="card col-6">{{-- Gráfico --}}
                                            <div class="card-body card-block">
                                                <div id="graficoProblemasRecorrentes" style="min-width: 300px; height: 400px; margin: 0 auto; border: 1px solid #ccc"></div>
                                            </div>
                                        </div>

                                        <div class="card col-6">
                                            <div class="card-body card-block">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr class="bg-light">
                                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;" title="" data-toggle="tooltip">Problema</td>
                                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;" title="" data-toggle="tooltip">%</td>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @foreach( session()->get( 'individualsProblemasRecorrentes' ) as $individual )
                                                            <tr>
                                                                <td class="small text-center" style="vertical-align: middle;" title="" data-toggle="tooltip">{{ isset( $individual->descricao ) ? $individual->descricao : ''  }}</td>
                                                                <td class="small text-center" style="vertical-align: middle;" title="" data-toggle="tooltip">{{ isset( $individual->percentual ) ? number_format( $individual->percentual, 0, ',', '.' ) : '' }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane fade show"        id="pills-operacional-sub2" role="tabpanel" aria-labelledby="pills-operacional-tab-sub2">{{-- Operacional / Soluções Recorrentes --}}

                                    <div class="row col-12">{{-- Soluções Recorrentes --}}
                                        <div class="card col-6">{{-- Gráfico --}}
                                            <div class="card-body card-block">
                                                <div id="graficoSolucoesRecorrentes" style="min-width: 300px; height: 400px; margin: 0 auto; border: 1px solid #ccc"></div>
                                            </div>
                                        </div>

                                        <div class="card col-6">
                                            <div class="card-body card-block">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr class="bg-light">
                                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;" title="" data-toggle="tooltip">Solução</td>
                                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;" title="" data-toggle="tooltip">%</td>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @foreach( session()->get( 'individualsSolucoesRecorrentes' ) as $individual )
                                                            <tr>
                                                                <td class="small text-center" style="vertical-align: middle;" title="" data-toggle="tooltip">{{ isset( $individual->descricao ) ? $individual->descricao : ''  }}</td>
                                                                <td class="small text-center" style="vertical-align: middle;" title="" data-toggle="tooltip">{{ isset( $individual->percentual ) ? number_format( $individual->percentual, 0, ',', '.' ) : '' }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane fade show"        id="pills-operacional-sub3" role="tabpanel" aria-labelledby="pills-operacional-tab-sub3">{{-- Operacional / produtividade dos atendentes --}}

                                    <div class="row col-12">{{-- Produtividade Atendentes --}}
                                        <div class="card col-6">{{-- Gráfico --}}
                                            <div class="card-body card-block">
                                                <div id="graficoProdutividadeAtendentes" style="min-width: 300px; height: 400px; margin: 0 auto; border: 1px solid #ccc"></div>
                                            </div>
                                        </div>

                                        <div class="card col-6">
                                            <div class="card-body card-block">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr class="bg-light">
                                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;" title="" data-toggle="tooltip">Atendente</td>
                                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;" title="" data-toggle="tooltip">%</td>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @foreach( session()->get( 'individualsProdutividadeAtendentes' ) as $individual )
                                                            <tr>
                                                                <td class="small text-center" style="vertical-align: middle;" title="" data-toggle="tooltip">{{ isset( $individual->nomeusuario ) ? $individual->nomeusuario : ''  }}</td>
                                                                <td class="small text-center" style="vertical-align: middle;" title="" data-toggle="tooltip">{{ isset( $individual->percentual ) ? number_format( $individual->percentual, 0, ',', '.' ) : '' }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane fade show"        id="pills-operacional-sub4" role="tabpanel" aria-labelledby="pills-operacional-tab-sub4">{{-- Operacional --}}
                                    Texto Operaciona -sub4
                                </div>
                                <div class="tab-pane fade show"        id="pills-operacional-sub5" role="tabpanel" aria-labelledby="pills-operacional-tab-sub5">{{-- Operacional --}}
                                    Texto Operaciona -sub5
                                </div>
                                <div class="tab-pane fade show"        id="pills-operacional-sub6" role="tabpanel" aria-labelledby="pills-operacional-tab-sub6">{{-- Operacional --}}
                                    Texto Operaciona -sub6
                                </div>


                                <div class="tab-pane fade show       " id="pills-tatico-sub1" role="tabpanel" aria-labelledby="pills-tatico-tab-sub1">{{-- Tático --}}
                                    Texto Tático -sub1
                                </div>
                                <div class="tab-pane fade show"        id="pills-tatico-sub2" role="tabpanel" aria-labelledby="pills-tatico-tab-sub2">{{-- Tático --}}
                                    Texto Tático -sub2
                                </div>
                                <div class="tab-pane fade show"        id="pills-tatico-sub3" role="tabpanel" aria-labelledby="pills-tatico-tab-sub3">{{-- Tático --}}
                                    Texto Tático -sub3
                                </div>
                                <div class="tab-pane fade show"        id="pills-tatico-sub4" role="tabpanel" aria-labelledby="pills-tatico-tab-sub4">{{-- Tático --}}
                                    Texto Tático -sub4
                                </div>
                                <div class="tab-pane fade show"        id="pills-tatico-sub5" role="tabpanel" aria-labelledby="pills-tatico-tab-sub5">{{-- Tático --}}
                                    Texto Tático -sub5
                                </div>
                                <div class="tab-pane fade show"        id="pills-tatico-sub6" role="tabpanel" aria-labelledby="pills-tatico-tab-sub6">{{-- Tático --}}
                                    Texto Tático -sub6
                                </div>


                                <div class="tab-pane fade show       " id="pills-estrategico-sub1" role="tabpanel" aria-labelledby="pills-estrategico-tab-sub1">{{-- Estratégico --}}
                                    Texto Estratégico -sub1
                                </div>
                                <div class="tab-pane fade show"        id="pills-estrategico-sub2" role="tabpanel" aria-labelledby="pills-estrategico-tab-sub2">{{-- Estratégico --}}
                                    Texto Estratégico -sub2
                                </div>
                                <div class="tab-pane fade show"        id="pills-estrategico-sub3" role="tabpanel" aria-labelledby="pills-estrategico-tab-sub3">{{-- Estratégico --}}
                                    Texto Estratégico -sub3
                                </div>
                                <div class="tab-pane fade show"        id="pills-estrategico-sub4" role="tabpanel" aria-labelledby="pills-estrategico-tab-sub4">{{-- Estratégico --}}
                                    Texto Estratégico -sub4
                                </div>
                                <div class="tab-pane fade show"        id="pills-estrategico-sub5" role="tabpanel" aria-labelledby="pills-estrategico-tab-sub5">{{-- Estratégico --}}
                                    Texto Estratégico -sub5
                                </div>
                                <div class="tab-pane fade show"        id="pills-estrategico-sub6" role="tabpanel" aria-labelledby="pills-estrategico-tab-sub6">{{-- Estratégico --}}
                                    Texto Estratégico -sub6
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    // inicio graficoProblemasRecorrentes
        $(function() {
            var chart;
            var categories = <?php echo json_encode( session()->get( 'ProblemasRecorrentes_grafico_A_Rotulos_Series_Eixo_X' ) ) ?>;

            $(document).ready(function() {
                chart = new Highcharts.Chart({
                    exporting: { enabled: false },
                    credits:{ enabled: false },
                    chart: { renderTo: {{ session()->get( 'ProblemasRecorrentes_grafico_A_Nome_Grafico' ) }}, type: 'column', options3d: { enabled: true, alpha: 10, beta: 5, depth: 60 }, options3d: { enabled: true, alpha: 10, beta: 5, depth: 60 } },
                    title: { text: '<b>{{ session()->get( 'ProblemasRecorrentes_grafico_A_Titulo' ) }}</b>' },
                    xAxis: { categories: categories, labels: { enabled: true } },
                    yAxis: { min: 0, title: { text: '{{ session()->get( 'ProblemasRecorrentes_grafico_A_Titulo_Eixo_Y' ) }}' } },
                    plotOptions: {
                        column: {
                            depth: 60
                        },
                        series: {
                            dataLabels: {
                                formatter: function() {
                                return '' + new Intl.NumberFormat('de-DE').format(this.y); },
                                enabled: true,
                                // rotation: -90,
                                align: 'right', color: 'black', shadow: false,
                                x: -10,
                                style: { "fontSize": "12px", "textShadow": "0px" }
                            },
                        pointPadding: 0.1, groupPadding: 0 } },
                    tooltip: { formatter: function() { return this.x + ': ' + new Intl.NumberFormat('de-DE').format(this.y); } },
                    series: [
                                {
                                    type: '{{ session()->get( 'ProblemasRecorrentes_grafico_A_Serie_1_Tipo_Grafico' ) }}',
                                    name: '{{ session()->get( 'ProblemasRecorrentes_grafico_A_Serie_1_Legenda' ) }}',
                                    data: <?php echo json_encode( session()->get( 'ProblemasRecorrentes_grafico_A_Serie_1_Dados' ), JSON_NUMERIC_CHECK ) ?>
                                }
                    ]
                });
            });
        });
    // final graficoProblemasRecorrentes
</script>
<script>
    // inicio graficoSolucoesRecorrentes
        $(function() {
            var chart;
            var categories = <?php echo json_encode( session()->get( 'SolucoesRecorrentes_grafico_A_grafico_A_Rotulos_Series_Eixo_X' ) ) ?>;

            $(document).ready(function() {
                chart = new Highcharts.Chart({
                    exporting: { enabled: false },
                    credits:{ enabled: false },
                    chart: { renderTo: {{ session()->get( 'SolucoesRecorrentes_grafico_A_grafico_A_Nome_Grafico' ) }}, type: 'column', options3d: { enabled: true, alpha: 10, beta: 5, depth: 60 }, options3d: { enabled: true, alpha: 10, beta: 5, depth: 60 } },
                    title: { text: '<b>{{ session()->get( 'SolucoesRecorrentes_grafico_A_grafico_A_Titulo' ) }}</b>' },
                    xAxis: { categories: categories, labels: { enabled: true } },
                    yAxis: { min: 0, title: { text: '{{ session()->get( 'SolucoesRecorrentes_grafico_A_grafico_A_Titulo_Eixo_Y' ) }}' } },
                    plotOptions: {
                        column: {
                            depth: 60
                        },
                        series: {
                            dataLabels: {
                                formatter: function() {
                                return '' + new Intl.NumberFormat('de-DE').format(this.y); },
                                enabled: true,
                                // rotation: -90,
                                align: 'right', color: 'black', shadow: false,
                                x: -10,
                                style: { "fontSize": "12px", "textShadow": "0px" }
                            },
                        pointPadding: 0.1, groupPadding: 0 } },
                    tooltip: { formatter: function() { return this.x + ': ' + new Intl.NumberFormat('de-DE').format(this.y); } },
                    series: [
                                {
                                    type: '{{ session()->get( 'SolucoesRecorrentes_grafico_A_grafico_A_Serie_1_Tipo_Grafico' ) }}',
                                    name: '{{ session()->get( 'SolucoesRecorrentes_grafico_A_grafico_A_Serie_1_Legenda' ) }}',
                                    data: <?php echo json_encode( session()->get( 'SolucoesRecorrentes_grafico_A_grafico_A_Serie_1_Dados' ), JSON_NUMERIC_CHECK ) ?>
                                }
                    ]
                });
            });
        });
    // final graficoSolucoesRecorrentes    
</script>
<script>
    // inicio graficoProdutividadeAtendentes
        $(function() {
            var chart;
            var categories = <?php echo json_encode( session()->get( 'ProdutividadeAtendentes_grafico_A_Rotulos_Series_Eixo_X' ) ) ?>;

            $(document).ready(function() {
                chart = new Highcharts.Chart({
                    exporting: { enabled: false },
                    credits:{ enabled: false },
                    chart: { renderTo: {{ session()->get( 'ProdutividadeAtendentes_grafico_A_Nome_Grafico' ) }}, type: 'column', options3d: { enabled: true, alpha: 10, beta: 5, depth: 60 }, options3d: { enabled: true, alpha: 10, beta: 5, depth: 60 } },
                    title: { text: '<b>{{ session()->get( 'ProdutividadeAtendentes_grafico_A_Titulo' ) }}</b>' },
                    xAxis: { categories: categories, labels: { enabled: true } },
                    yAxis: { min: 0, title: { text: '{{ session()->get( 'ProdutividadeAtendentes_grafico_A_Titulo_Eixo_Y' ) }}' } },
                    plotOptions: {
                        column: {
                            depth: 60
                        },
                        series: {
                            dataLabels: {
                                formatter: function() {
                                return '' + new Intl.NumberFormat('de-DE').format(this.y); },
                                enabled: true,
                                // rotation: -90,
                                align: 'right', color: 'black', shadow: false,
                                x: -10,
                                style: { "fontSize": "12px", "textShadow": "0px" }
                            },
                        pointPadding: 0.1, groupPadding: 0 } },
                    tooltip: { formatter: function() { return this.x + ': ' + new Intl.NumberFormat('de-DE').format(this.y); } },
                    series: [
                                {
                                    type: '{{ session()->get( 'ProdutividadeAtendentes_grafico_A_Serie_1_Tipo_Grafico' ) }}',
                                    name: '{{ session()->get( 'ProdutividadeAtendentes_grafico_A_Serie_1_Legenda' ) }}',
                                    data: <?php echo json_encode( session()->get( 'ProdutividadeAtendentes_grafico_A_Serie_1_Dados' ), JSON_NUMERIC_CHECK ) ?>
                                }
                    ]
                });
            });
        });
    // final graficoProdutividadeAtendentes
<script>

    ///////////////////////////////////////////////////////////////////////////////
    $(function() {
        var chart;
        var categories = <?php echo json_encode( session()->get( 'individuals' ) ) ?>;
        $(document).ready(function() {
            chart = new Highcharts.Chart({
                exporting: {
                    enabled: false
                },
                credits:{
                    enabled: false
                },
                chart: {
                    renderTo: 'grafico2',
                    type: 'column'
                },
                colors: ['#78ff41'],
                title: {
                    text: '<b>Gráfico 2</b>'
                },
                xAxis: {
                    categories: categories,
                    labels: {
                    enabled: true
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                    text: 'Total Pago (R$)'
                    }
                },
                plotOptions: {
                    series: {
                    dataLabels: {
                        formatter: function() {
                        return 'R$ ' + new Intl.NumberFormat('de-DE').format(this.y);
                        },
                        enabled: true,
                        align: 'right',
                        color: 'black',
                        shadow: false,
                        x: -10,
                        style: {
                        "fontSize": "12px",
                        "textShadow": "0px"
                        }
                    },
                    pointPadding: 0.1,
                    groupPadding: 0
                    }
                },

                tooltip: {
                    formatter: function() {
                    return this.x + ': R$ ' + new Intl.NumberFormat('de-DE').format(this.y);
                    }
                },
                series: [{
                    type: 'column',
                    name: 'Total de empenhos pagos',
                    data: <?php echo json_encode( session()->get( 'individuals' ), JSON_NUMERIC_CHECK ) ?>,
                    stacking: 'normal'
                },
                {
                    type: 'column',
                    name: 'Total de empenhos pagos',
                    data: <?php echo json_encode( session()->get( 'individuals' ), JSON_NUMERIC_CHECK ) ?>,
                    stacking: 'normal'
                }]
            });
        });
    });
    ////////////////////////////////////////////////
    {{--
        function dataAt(){
            var data = new Date(),
                dia  = data.getDate().toString().padStart(2, '0'),
                mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
                ano  = data.getFullYear();
            return dia+"/"+mes+"/"+ano;
        }

        document.getElementById("export-pdf").onclick = function() {
            var svg = document.getElementById("grafico1").querySelector("svg");
            var pdf = new jsPDF('l', 'pt', 'a4');
            var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"

            svg2pdf(svg, pdf, {
                xOffset: 40,
                yOffset: 180,
                scale: 0.69
            });

            pdf.setFontSize(18)
            pdf.setTextColor(40)
            pdf.setFontType("bold")
            pdf.text('GRÁFICOS DE EMPENHOS', 320, 130)
            pdf.setFontSize(10)
            pdf.text('Data: ' + dataAt(), 398, 145)

            var img = new Image();
            img.src = base64Img;
            pdf.addImage(img, 'JPEG', 40, 15, 765, 86)
            //Rodape
            var str = 'Pág. ' + pdf.internal.getNumberOfPages()
            pdf.setFontSize(9)
            var pageSize = pdf.internal.pageSize
            var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
            pdf.text(str, 780, pageHeight - 35)
            pdf.setFontType("bold");
            pdf.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', 328, pageHeight - 40)
            pdf.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br',  340, pageHeight - 30)
            ///

            pdf.addPage("a4", "l");
            //pdf.addImage(base64Img, 'JPEG', 40, 15, 765, 86)
            var svg1 = document.getElementById("grafico2").querySelector("svg");
            svg2pdf(svg1, pdf, {
            xOffset: 40,
            yOffset: 150,
            scale: 0.69
            });
            pdf.addImage(img, 'JPEG', 40, 15, 765, 86)
            //Rodape
            var str = 'Pág. ' + pdf.internal.getNumberOfPages()
            pdf.setFontSize(9)
            var pageSize = pdf.internal.pageSize
            var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
            pdf.text(str, 780, pageHeight - 35)
            pdf.setFontType("bold");
            pdf.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', 328, pageHeight - 40)
            pdf.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br',  340, pageHeight - 30)
            ///

            pdf.save("graficosEmpenhos.pdf");
        };


        $(document).ready(function(){
            $('#imprimirPDF2').click(function() {
                var doc = new jsPDF('portrait', 'pt', 'a4')
                var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"
                var img = new Image()
                img.src = base64Img
                var hoje = new Date()
                doc.autoTable({
                html: '#tabela',
                styles: {
                    lineColor: '#ccc',
                    lineWidth: 1,
                    fontSize: 9
                },
                headStyles: { fillColor: '#424647' },
                didDrawPage: function (data) {
                    // Cabeçalho
                    if (base64Img) {
                    doc.addImage(img, 'JPEG', 40, 15, 515, 66)
                    }
                    doc.setFontSize(14)
                    doc.setTextColor(40)
                    doc.setFontType("bold")
                    doc.text('RELATÓRIO DE AÇÕES', data.settings.margin.left + 0, 100)
                    doc.setFontSize(9)
                    doc.text('Data: ' + dataAt(), data.settings.margin.left + 445, 100)

                    // Rodape
                    var str = 'Pág. ' + doc.internal.getNumberOfPages()
                    doc.setFontSize(9)
                    var pageSize = doc.internal.pageSize
                    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                    doc.text(str, data.settings.margin.left + 480, pageHeight - 25)
                    doc.setFontType("bold");
                    doc.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', data.settings.margin.left + 128, pageHeight - 30)
                    doc.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br', data.settings.margin.left + 145, pageHeight - 20)
                },
                margin: {top: 108}
                });
                doc.save("RelatorioAcao-"+ dataAt() +".pdf");
            });
        });
    --}}
<script>
@endsection
