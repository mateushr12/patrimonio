@extends('layout_admin.principal')
@section ('breadcrumb','Callcenter / Tipo de Solução')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-10">
                                        <strong>Editar Tipo de Solução</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">

                                @if ($errors->any()) <div class="alert alert-danger"> <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul> </div> @endif
                                @if (session('msg')) <div class="alert alert-danger alert-dismissible fade show" role="alert"> {{session('msg')}} <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> @endif

                                <form class="" action="{{route('tiposolucao.atualizar',$individual->id)}}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="put">
                                    @include('callcenter.tiposolucao.acoes._form')
                                    <button type="submit" class="btn btn-primary">Atualizar</button>
                                </form>
                                <div style="text-align: left;">
                                    <strong style="color: red">*</strong> informação obrigatória
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
