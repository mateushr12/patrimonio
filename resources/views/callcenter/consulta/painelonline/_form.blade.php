<div class="row">
    <div class="form-group col-sm-6">
        <label for="FonteAcao">Grupo / Nivel:</label>
        <select class="form-control" id="gruposuser_id" name="gruposuser_id" required>
            <option value="0">Todos</option>
            @foreach($gruposuser as $tipo)
                <option value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->descricao}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-sm-6">
        <label for="FonteAcao">Tipo de Cidadão:</label>
        <select class="form-control" id="tipocidadao_id" name="tipocidadao_id" 
               title="Tipo de Cidadão informado" data-toggle="tooltip" data-placement="top" required>
            <option value="0">Todos</option>
            @foreach($tipocidadao as $tipo)
                <option value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->descricao}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6">
        <label for="FonteAcao">Período:</label>
        <select class="form-control" id="id_periodo" name="id_periodo" required>
            <option value="1">Hoje</option>
            <option value="2">3 Dias</option>
            <option value="3">5 Dias</option>
            <option value="4">1 Semana</option>
            <option value="5">Mês Atual</option>
            <option value="6">3 Ultimos Meses</option>
            <option value="7">Semestre Atual</option>
            <option value="8">Ano Atual</option>
            <option value="9">Período Específico</option>
        </select>
        </select>
    </div>
    <div class="form-group col-sm-6">
        <label for="FonteAcao">Tipo de Resultado:</label>
        <select class="form-control" id="id_tiporesultado" name="id_tiporesultado" 
               title="Tipo de Resultado desejado" data-toggle="tooltip" data-placement="top" required>
            {{-- <option value="">Selecione o Tipo de Resultado</option> --}}
            <option value="01">Por Data / Hora</option>
            <option value="02">Por Dia</option>
            <option value="03">Por Hora</option>
            <option value="04">Por Data / Atendente</option>
            <option value="05">Por Atendente</option>
            <option value="06">Por Tipo de Problema</option>
            <option value="07">Problemas com ocorrências > 2</option>
            <option value="08">Por Tipo de Solução</option>
            <option value="09">Soluções com ocorrências > 2</option>
            <option value="15">Clientes com chamadas > 2</option>
            <option value="11">Agiliza x C.P.F.</option>
            <option value="12">Por Tipo de Cidadão</option>
        </select>
    </div>
</div>
<div class="row">
    <input type="hidden" name="user_id" value="{{auth()->user()->id}}" />
    {{--
    <div class="form-group col-md-12">
        <label for="id">Usuário Logado</label>
        <input type="text" class="form-control" id="id" name="id" placeholder="Usuário Logado"
               value="{{ auth()->user()->id }} - {{ auth()->user()->name }}" autocomplete="off" required>
    </div>
    --}}
</div>
