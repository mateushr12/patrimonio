<script>
    #container {
    height: 400px;
    }

    .highcharts-figure, .highcharts-data-table table {
    min-width: 310px;
    max-width: 800px;
    margin: 1em auto;
    }

    #datatable {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #EBEBEB;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
    }
    #datatable caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
    }
    #datatable th {
        font-weight: 600;
    padding: 0.5em;
    }
    #datatable td, #datatable th, #datatable caption {
    padding: 0.5em;
    }
    #datatable thead tr, #datatable tr:nth-child(even) {
    background: #f8f8f8;
    }
    #datatable tr:hover {
    background: #f1f7ff;
    }
</script>
<meta HTTP-EQUIV='refresh' CONTENT='30;URL='" . $_SERVER['PHP_SELF'] . ">

@extends('layout_admin.principal')
@section ('breadcrumb','Callcenter / Consulta')


@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        {{-- <strong>{{ $titulo }}</strong> --}}
                                        Painel On-Line ( atualização automática a cada 30 segundos )   
                                        {{ ( session()->get( 'totalregistros' ) > 0 ) ? ( '( ' . session()->get( 'totalregistros' ) . ' registros )' ) : '' }} 
                                           Data: <strong>{{ datahoraBR( date( 'Y-m-d', strtotime( now() ) ) ) }}</strong>
                                        ( {{ dataDiaSemana( date( 'Y-m-d', strtotime( now() ) ) ) }} )
                                    </div>
                                </div>
                            </div>

                            <div class="card-body card-block">

                                @if ($errors->any()) <div class="alert alert-danger"> <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul> </div> @endif
                                @if (session('msg')) <div class="alert alert-danger alert-dismissible fade show" role="alert"> {{session('msg')}} <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> @endif

                                <div class="row">
                                    <div class="col-4">
                                        @if ( ( session()->has( 'individuals' ) ) && ( count( session()->get( 'individuals' ) ) > 0 ) )
                                        <a target="_blank" href="{{ route( 'consulta.imprimir_painelonline', compact( 'titulo' ) ) }}"><button type="button" class="item" title="Imprimir" data-toggle="tooltip" data-placement="top"><i class="zmdi zmdi-print"></i> Imprimir</button></a>
                                        @endif
                                    </div>
                                </div>

                                <table id="datatable" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light">
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Usuário</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Qtde Atendimentos</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Qtde Atendimentos Transferidos p/ Grupos</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Qtde Atendimentos Transferidos p/ Usuários</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Qtde Atendimentos Resolvidos</td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if ( session()->has( 'individuals' ) )
                                            @foreach( session()->get( 'individuals' ) as $individual )
                                                <tr>
                                                    <td class="small text-center" style="vertical-align: middle;"><strong>{{ isset( $individual->nome_usuario ) ? $individual->nome_usuario : '' }}</strong><br>
                                                                                                                ( {{ isset( $individual->descricao_gruposuser ) ? $individual->descricao_gruposuser : '' }} )</td>
                                                    <td class="small text-center" style="vertical-align: middle;">{{ number_format( $individual->contador, 0, ',', '.' ) }}</td>
                                                    @if ( $individual->contadorGruposuser > 0 )
                                                        <td class="small text-center text-danger font-weight-bold" style="vertical-align: middle; color: red;">
                                                            {{ number_format( $individual->contadorGruposuser, 0, ',', '.' ) }}
                                                        </td>
                                                    @else
                                                        <td class="small text-center" style="vertical-align: middle;">
                                                            {{ number_format( $individual->contadorGruposuser, 0, ',', '.' ) }}
                                                        </td>
                                                    @endif
                                                    @if ( $individual->contadorPorUsuario > 0 )
                                                        <td class="small text-center text-danger font-weight-bold" style="vertical-align: middle; color: red;">
                                                            {{ number_format( $individual->contadorPorUsuario, 0, ',', '.' ) }}
                                                        </td>
                                                    @else
                                                        <td class="small text-center" style="vertical-align: middle;">
                                                            {{ number_format( $individual->contadorPorUsuario, 0, ',', '.' ) }}
                                                        </td>
                                                    @endif

                                                    @if ( $individual->contadorUsuario_atendeu_red > 0 )
                                                        <td class="small text-center text-danger font-weight-bold" style="vertical-align: middle; color: red;">
                                                            {{ number_format( $individual->contadorUsuario_atendeu_red, 0, ',', '.' ) }}
                                                        </td>
                                                    @else
                                                        <td class="small text-center" style="vertical-align: middle;">
                                                            {{ number_format( $individual->contadorUsuario_atendeu_red, 0, ',', '.' ) }}
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            @if ( ( session()->has( 'individuals' ) ) && ( count( session()->get( 'individuals' ) ) > 0 ) )
                                                <td colspan="1" class="text-center" style="vertical-align: middle;"><strong>TOTAIS</strong></td>
                                                <td class="text-center" style="vertical-align: middle;"><strong>{{ number_format( session()->get( 'individuals' )[0]->totalcontador, 0, ',', '.' ) }}</strong></td>
                                                <td class="text-center" style="vertical-align: middle;"><strong>{{ number_format( session()->get( 'individuals' )[0]->totalcontadorGruposuser, 0, ',', '.' ) }}</strong></td>
                                                <td class="text-center" style="vertical-align: middle;"><strong>{{ number_format( session()->get( 'individuals' )[0]->totalcontadorPorUsuario, 0, ',', '.' ) }}</strong></td>
                                                <td class="text-center" style="vertical-align: middle;"><strong>{{ number_format( session()->get( 'individuals' )[0]->totalcontadorUsuario_atendeu_red, 0, ',', '.' ) }}</strong></td>
                                            @endif
                                        </tr>                                        
                                    </tfoot>
                                </table>

                                <div class="row d-flex justify-content-center">
                                    {{-- $individuals->links() --}}
                                </div>

                                @if ( session()->has( 'individuals' ) )
                                    <div class="card-body card-block">
                                        <div id="grafico1" style="min-width: 300px; height: 400px; margin: 0 auto; border: 1px solid #ccc"></div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--
        <figure class="highcharts-figure">
            <div id="container"></div>
        </figure>        
        --}}
    </section>

<script>
    $(function() {
        var chart;
        var categories = <?php echo json_encode( session()->get( 'grafico_A_Rotulos_Series_Eixo_X' ) ) ?>;

        $(document).ready(function() {
            chart = new Highcharts.Chart({
                exporting: { enabled: false },
                credits:{ enabled: false },
                chart: { renderTo: {{ session()->get( 'grafico_A_Nome_Grafico' ) }}, type: 'column', options3d: { enabled: true, alpha: 10, beta: 5, depth: 60 }, options3d: { enabled: true, alpha: 10, beta: 5, depth: 60 } },
                title: { text: '<b>{{ session()->get( 'grafico_A_Titulo' ) }}</b>' },
                xAxis: { categories: categories, labels: { enabled: true } },
                yAxis: { min: 0, title: { text: '{{ session()->get( 'grafico_A_Titulo_Eixo_Y' ) }}' } },
                plotOptions: {
                    column: {
                        depth: 60
                    },
                    series: {
                        dataLabels: {
                            formatter: function() {
                            return '' + new Intl.NumberFormat('de-DE').format(this.y); },
                            enabled: true,
                            // rotation: -90,
                            align: 'right', color: 'black', shadow: false,
                            x: -10,
                            style: { "fontSize": "12px", "textShadow": "0px" }
                        },
                    pointPadding: 0.1, groupPadding: 0 } },
                tooltip: { formatter: function() { return this.x + ': ' + new Intl.NumberFormat('de-DE').format(this.y); } },
                series: [
                            {
                                type: '{{ session()->get( 'grafico_A_Serie_1_Tipo_Grafico' ) }}',
                                name: '{{ session()->get( 'grafico_A_Serie_1_Legenda' ) }}',
                                data: <?php echo json_encode( session()->get( 'grafico_A_Serie_1_Dados' ), JSON_NUMERIC_CHECK ) ?>
                            }
                            ,
                            {
                                type: '{{ session()->get( 'grafico_A_Serie_2_Tipo_Grafico' ) }}',
                                name: '{{ session()->get( 'grafico_A_Serie_2_Legenda' ) }}',
                                data: <?php echo json_encode( session()->get( 'grafico_A_Serie_2_Dados' ), JSON_NUMERIC_CHECK ) ?>
                            }
                            ,
                            {
                                type: '{{ session()->get( 'grafico_A_Serie_3_Tipo_Grafico' ) }}',
                                name: '{{ session()->get( 'grafico_A_Serie_3_Legenda' ) }}',
                                data: <?php echo json_encode( session()->get( 'grafico_A_Serie_3_Dados' ), JSON_NUMERIC_CHECK ) ?>
                            }
                            ,
                            {
                                type: '{{ session()->get( 'grafico_A_Serie_4_Tipo_Grafico' ) }}',
                                name: '{{ session()->get( 'grafico_A_Serie_4_Legenda' ) }}',
                                data: <?php echo json_encode( session()->get( 'grafico_A_Serie_4_Dados' ), JSON_NUMERIC_CHECK ) ?>
                            },
                            {
                                type: '{{ session()->get( 'grafico_A_Serie_5_Tipo_Grafico' ) }}',
                                name: '{{ session()->get( 'grafico_A_Serie_5_Legenda' ) }}',
                                data: <?php echo json_encode( session()->get( 'grafico_A_Serie_5_Dados' ), JSON_NUMERIC_CHECK ) ?>
                            }
                ]
            });
        });
    });
    // final grafico 1
    ///////////////////////////////////////////////////////////////////////////////
    $(function() {
        var chart;
        var categories = <?php echo json_encode( session()->get( 'individuals' ) ) ?>;
        $(document).ready(function() {
            chart = new Highcharts.Chart({
                exporting: {
                    enabled: false
                },
                credits:{
                    enabled: false
                },
                chart: {
                    renderTo: 'grafico2',
                    type: 'column'
                },
                colors: ['#78ff41'],
                title: {
                    text: '<b>Gráfico 2</b>'
                },
                xAxis: {
                    categories: categories,
                    labels: {
                    enabled: true
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                    text: 'Total Pago (R$)'
                    }
                },
                plotOptions: {
                    series: {
                    dataLabels: {
                        formatter: function() {
                        return 'R$ ' + new Intl.NumberFormat('de-DE').format(this.y);
                        },
                        enabled: true,
                        align: 'right',
                        color: 'black',
                        shadow: false,
                        x: -10,
                        style: {
                        "fontSize": "12px",
                        "textShadow": "0px"
                        }
                    },
                    pointPadding: 0.1,
                    groupPadding: 0
                    }
                },

                tooltip: {
                    formatter: function() {
                    return this.x + ': R$ ' + new Intl.NumberFormat('de-DE').format(this.y);
                    }
                },
                series: [{
                    type: 'column',
                    name: 'Total de empenhos pagos',
                    data: <?php echo json_encode( session()->get( 'individuals' ), JSON_NUMERIC_CHECK ) ?>,
                    stacking: 'normal'
                },
                {
                    type: 'column',
                    name: 'Total de empenhos pagos',
                    data: <?php echo json_encode( session()->get( 'individuals' ), JSON_NUMERIC_CHECK ) ?>,
                    stacking: 'normal'
                }]
            });
        });
    });

    ////////////////////////////////////////////////
    function dataAt(){
        var data = new Date(),
            dia  = data.getDate().toString().padStart(2, '0'),
            mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
            ano  = data.getFullYear();
        return dia+"/"+mes+"/"+ano;
    }

    document.getElementById("export-pdf").onclick = function() {
        var svg = document.getElementById("grafico1").querySelector("svg");
        var pdf = new jsPDF('l', 'pt', 'a4');
        var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"

        svg2pdf(svg, pdf, {
            xOffset: 40,
            yOffset: 180,
            scale: 0.69
        });

        pdf.setFontSize(18)
        pdf.setTextColor(40)
        pdf.setFontType("bold")
        pdf.text('GRÁFICOS DE EMPENHOS', 320, 130)
        pdf.setFontSize(10)
        pdf.text('Data: ' + dataAt(), 398, 145)

        var img = new Image();
        img.src = base64Img;
        pdf.addImage(img, 'JPEG', 40, 15, 765, 86)
        //Rodape
        var str = 'Pág. ' + pdf.internal.getNumberOfPages()
        pdf.setFontSize(9)
        var pageSize = pdf.internal.pageSize
        var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
        pdf.text(str, 780, pageHeight - 35)
        pdf.setFontType("bold");
        pdf.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', 328, pageHeight - 40)
        pdf.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br',  340, pageHeight - 30)
        ///

        pdf.addPage("a4", "l");
        //pdf.addImage(base64Img, 'JPEG', 40, 15, 765, 86)
        var svg1 = document.getElementById("grafico2").querySelector("svg");
        svg2pdf(svg1, pdf, {
        xOffset: 40,
        yOffset: 150,
        scale: 0.69
        });
        pdf.addImage(img, 'JPEG', 40, 15, 765, 86)
        //Rodape
        var str = 'Pág. ' + pdf.internal.getNumberOfPages()
        pdf.setFontSize(9)
        var pageSize = pdf.internal.pageSize
        var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
        pdf.text(str, 780, pageHeight - 35)
        pdf.setFontType("bold");
        pdf.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', 328, pageHeight - 40)
        pdf.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br',  340, pageHeight - 30)
        ///

        pdf.save("graficosEmpenhos.pdf");
    };


    $(document).ready(function(){
        $('#imprimirPDF2').click(function() {
            var doc = new jsPDF('portrait', 'pt', 'a4')
            var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"
            var img = new Image()
            img.src = base64Img
            var hoje = new Date()
            doc.autoTable({
            html: '#tabela',
            styles: {
                lineColor: '#ccc',
                lineWidth: 1,
                fontSize: 9
            },
            headStyles: { fillColor: '#424647' },
            didDrawPage: function (data) {
                // Cabeçalho
                if (base64Img) {
                doc.addImage(img, 'JPEG', 40, 15, 515, 66)
                }
                doc.setFontSize(14)
                doc.setTextColor(40)
                doc.setFontType("bold")
                doc.text('RELATÓRIO DE AÇÕES', data.settings.margin.left + 0, 100)
                doc.setFontSize(9)
                doc.text('Data: ' + dataAt(), data.settings.margin.left + 445, 100)

                // Rodape
                var str = 'Pág. ' + doc.internal.getNumberOfPages()
                doc.setFontSize(9)
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left + 480, pageHeight - 25)
                doc.setFontType("bold");
                doc.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', data.settings.margin.left + 128, pageHeight - 30)
                doc.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br', data.settings.margin.left + 145, pageHeight - 20)
            },
            margin: {top: 108}
            });
            doc.save("RelatorioAcao-"+ dataAt() +".pdf");
        });
    });        
</script>


{{--
    <script>
        Highcharts.chart('container', {
        data: {
            table: 'datatable'
        },
        chart: {
            type: 'column'
        },
        title: {
            text: '<b>{{ session()->get( 'grafico1tituloGrafico' ) }}</b>'
        },
        yAxis: {
            allowDecimals: false,
            title: {
            text: 'Qtde de Atendimentos'
            }
        },
        tooltip: {
            formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
            }
        }
        });
    </script>

    <script>
        Highcharts.chart('container', {
            data: {
                table: 'tabela1'
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Data extracted from a HTML table in the page'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                text: 'Units'
                }
            },
            tooltip: {
                formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' ' + this.point.name.toLowerCase();
                }
            }
        });
    </script>
--}}

@endsection
