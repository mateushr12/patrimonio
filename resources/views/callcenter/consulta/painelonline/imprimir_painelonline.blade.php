@include('layout_admin._includes.topo_print_callcenter')
    <table class="table table-bordered table-hover" style="font-size:80%; margin: auto;">
        <caption></caption>
        <thead>
            <tr class="bg-light">
                <th>Usuário</th>
                <th>Qtde Atendimentos</th>
                <th>Qtde Atendimentos transferidos p/ grupos</th>
                <th>Qtde Atendimentos transferidos p/ usuários</th>
                <th>Qtde Atendimentos resolvidos</th>
            </tr>
        </thead>

        <tbody>
            @if(session()->has('individuals'))
                @foreach( session()->get( 'individuals' ) as $individual )
                    <tr>
                        <td style="text-align: center;"><strong>{{ isset( $individual->nome_usuario ) ? $individual->nome_usuario : '' }}</strong><br>
                                                                                            ( {{ isset( $individual->descricao_gruposuser ) ? $individual->descricao_gruposuser : '' }} )</td>
                        <td style="text-align: center;">{{ number_format( $individual->contador, 0, ',', '.' ) }}</td>
                        <td style="text-align: center;">{{ number_format( $individual->contadorGruposuser, 0, ',', '.' ) }}</td>
                        <td style="text-align: center;">{{ number_format( $individual->contadorPorUsuario, 0, ',', '.' ) }}</td>
                        <td style="text-align: center;">{{ number_format( $individual->contadorUsuario_atendeu_red, 0, ',', '.' ) }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>

        <tfoot>
            <tr>
                <td colspan="1" style="text-align: center;">TOTAIS</td>
                <td style="text-align: center;">{{ number_format( session()->get( 'individuals' )[0]->totalcontador, 0, ',', '.' ) }}</td>
                <td style="text-align: center;">{{ number_format( session()->get( 'individuals' )[0]->totalcontadorUsuario_atendeu_red, 0, ',', '.' ) }}</td>
                <td style="text-align: center;">{{ number_format( session()->get( 'individuals' )[0]->totalcontadorPorUsuario, 0, ',', '.' ) }}</td>
                <td style="text-align: center;">{{ number_format( session()->get( 'individuals' )[0]->totalcontadorGruposuser, 0, ',', '.' ) }}</td>
            </tr>                                        
        </tfoot>
    </table>
</section>

@include('layout_admin._includes.footer_print_callcenter')