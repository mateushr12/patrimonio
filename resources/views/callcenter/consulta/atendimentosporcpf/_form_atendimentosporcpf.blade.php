<script>$("#cpf").mask("000.000.000-00");</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#data_final').change(function() {
            if ( $('#data_final').getTime() < $('#data_inicial').getTime() ) {
                document.getElementById("data_inicial").value = $('#data_final').value;
            }
        });
    });
</script> 

<div class="row">
    <div class="form-group col-md-3">
        <label for="data_inicial">Data Inicial</label>
        <input type="date" class="form-control" id="data_inicial" name="data_inicial" value={{ now() }} 
               title="Data inicial do período desejado" data-toggle="tooltip" data-placement="top" autofocus>
    </div>

    <div class="form-group col-md-3">
        <label for="data_final">Data Final</label>
        <input type="date" class="form-control" id="data_final" name="data_final" value={{ now() }} 
               title="Data Final do período desejado" data-toggle="tooltip" data-placement="top">
    </div>

    <div class="form-group col-md-3">
        <label for="data_final">C.P.F. Cidadão</label>
        <input type="text" class="form-control" id="cpf" name="cpf" maxlength="14" placeholder="" value="" OnKeyPress="mascaraCPF(cpf);" value="" 
               title="C.P.F. do Cidadão" data-toggle="tooltip" data-placement="top" required>
    </div>

    <div class="form-group col-md-3">
        <label for="max_registros">Máx. Registros</label>
        <input type="number" class="form-control" id="max_registros" name="max_registros" value="15" 
               title="Número máximo de registros a ser retornado na consulta" data-toggle="tooltip" data-placement="top">
    </div>
</div>

