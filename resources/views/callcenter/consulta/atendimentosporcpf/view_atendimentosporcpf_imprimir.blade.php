@include('layout_admin._includes.topo_print_callcenter')

@if(session()->has('individuals'))
    <table>
        <tr>
            <td>
                @foreach( session()->get( 'individuals' ) as $individual )
                    <strong>Tipo Cidadão:               </strong> {{ isset( $individual->tipocidadao->descricao ) ? $individual->tipocidadao->descricao : '' }}
                     | <strong>Nome:                       </strong> {{ isset( $individual->cidadao->nome ) ? $individual->cidadao->nome : '' }}
                     | <strong>Fone Fixo:                  </strong> {{ isset( $individual->cidadao->fone_fixo ) ? mask( $individual->cidadao->fone_fixo, '(##) ####-####' ) : '' }}
                     | <strong>Celular:                    </strong> {{ isset( $individual->cidadao->fone_celular ) ? mask( $individual->cidadao->fone_celular, '(##) #####-####' ) : '' }}
                     | <strong>Celular2:                   </strong> {{ isset( $individual->cidadao->fone_celular2 ) ? mask( $individual->cidadao->fone_celular2, '(##) #####-####' ) : '' }}
                     | <strong>Email:                      </strong> {{ isset( $individual->cidadao->email ) ? $individual->cidadao->email : '' }}
                     | <strong>Deseja Receber Informativo: </strong> @if ( isset( $individual->cidadao->receber_informativo ) ) @if ( $individual->cidadao->receber_informativo === 1 ) {{ 'Sim' }} @elseif ( $individual->cidadao->receber_informativo === 0 ) {{ 'Não' }} @else {{ 'Não Informado' }} @endif @endif
                    @break
                @endforeach
            </td>
        </tr>
    </table>
@endif

<table class="table table-bordered" style="font-size:87%;">
        <caption></caption>
        <thead>
            <tr class="bg-light">
                <th>Protocolo / Cidadão</th>
                <th>Atendimento: Problema / Solução / Agiliza</th>
            </tr>
        </thead>

        <tbody>
        @if(session()->has('individuals'))
            @foreach( session()->get( 'individuals' ) as $key => $individual )
                <tr>
                    <td class="small text-center" style="vertical-align: middle;">
                            <strong>Protocolo: </strong> {{ isset( session()->get( 'individuals' )[ $key ]->protocolo ) ? mask( session()->get( 'individuals' )[ $key ]->protocolo, '#### #### #### #### ####' ) : '' }}
                        <br><strong>Ocorrência: </strong>
                            {{ isset( session()->get( 'individuals' )[ $key ]->data_hora_inicio ) ? dataBR( session()->get( 'individuals' )[ $key ]->data_hora_inicio ) : '' }} das
                            {{ isset( session()->get( 'individuals' )[ $key ]->data_hora_inicio ) ? horaBR( session()->get( 'individuals' )[ $key ]->data_hora_inicio ) : '' }} às
                            {{ isset( session()->get( 'individuals' )[ $key ]->data_hora_fim ) ? horaBR( session()->get( 'individuals' )[ $key ]->data_hora_fim ) : '' }}
                            (duração: {{ isset( session()->get( 'individuals' )[ $key ]->diferencadatas ) ? horaAnormal( session()->get( 'individuals' )[ $key ]->diferencadatas ) : '' }})

                        <br><strong>Tipo Cidadão: </strong>{{ isset( session()->get( 'individuals' )[ $key ]->tipocidadao->descricao ) ? session()->get( 'individuals' )[ $key ]->tipocidadao->descricao : '' }}
                        <br><strong>Tipo de Atendimento: </strong>{{ isset( session()->get( 'individuals' )[ $key ]->tipoatendimento->descricao ) ? session()->get( 'individuals' )[ $key ]->tipoatendimento->descricao : '' }}
                        <br><strong>Tipo de Problema: </strong>{{ isset( session()->get( 'individuals' )[ $key ]->tipoproblema->descricao ) ? session()->get( 'individuals' )[ $key ]->tipoproblema->descricao : '' }}
                        <br><strong>Complemento do Problema: </strong>{{ isset( session()->get( 'individuals' )[ $key ]->complemento_problema ) ? session()->get( 'individuals' )[ $key ]->complemento_problema : '' }} 
                        <br><strong>Usuário que atendeu: </strong> {{ isset( session()->get( 'individuals' )[ $key ]->usuariocadastro->name ) ? session()->get( 'individuals' )[ $key ]->usuariocadastro->name : '' }} {{ isset( session()->get( 'individuals' )[ $key ]->usuariocadastro->grupo->nome ) ? (session()->get( 'individuals' )[ $key ]->usuariocadastro->grupo->nome) : '' }}
                    </td>

                    <td class="small text-left" style="vertical-align: middle;">
                            <strong>Tipo de Solução: </strong> {{ isset( session()->get( 'individuals' )[ $key ]->tiposolucao->descricao ) ? session()->get( 'individuals' )[ $key ]->tiposolucao->descricao : '' }}
                        <br><strong>Complemento da Solução: </strong>{{ isset( session()->get( 'individuals' )[ $key ]->complemento_solucao ) ? session()->get( 'individuals' )[ $key ]->complemento_solucao : '' }} 
                        <br><strong>Protocolo Agiliza: </strong>{{ isset( session()->get( 'individuals' )[ $key ]->id_agiliza ) ? session()->get( 'individuals' )[ $key ]->id_agiliza : '' }}
                        <br><strong>Redirecionado para Grupo: </strong> {{ isset( session()->get( 'individuals' )[ $key ]->grupousuariored->name ) ? session()->get( 'individuals' )[ $key ]->grupousuariored->name : '' }}
                        <br><strong>Redirecionado para Usuário: </strong> {{ isset( session()->get( 'individuals' )[ $key ]->usuariored->name ) ? session()->get( 'individuals' )[ $key ]->usuariored->name : '' }} {{ isset( session()->get( 'individuals' )[ $key ]->usuariored->grupo->nome ) ? (session()->get( 'individuals' )[ $key ]->usuariored->grupo->nome) : '' }}
                        <br><strong>Data/Hora Solução redirecionamento: </strong> {{ isset( session()->get( 'individuals' )[ $key ]->data_hora_red_finalizado ) ? datahoraBR( session()->get( 'individuals' )[ $key ]->data_hora_red_finalizado ) : '' }} @if ( !isset( $individual['data_hora_red_finalizado'] ) && isset( $individual['gruposuser_id_red'] ) )<strong class="text-danger"> - Pendente -</strong> @endif
                        <br>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</section>
@include('layout_admin._includes.footer_print_callcenter')
