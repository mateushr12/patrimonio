<div class="row">
    <div class="form-group col-md-3">
        <label for="data_inicial">Data Inicial</label>
        <input type="date" class="form-control" id="data_inicial" name="data_inicial" value={{ now() }} 
               title="Data inicial do período desejado" data-toggle="tooltip" data-placement="top" autofocus>
    </div>

    <div class="form-group col-md-3">
        <label for="data_final">Data Final</label>
        <input type="date" class="form-control" id="data_final" name="data_final" value={{ now() }} 
               title="Data Final do período desejado" data-toggle="tooltip" data-placement="top">
    </div>

    <div class="form-group col-sm-4">
        <label for="FonteAcao">Tipo de Cidadão:</label>
        <select class="form-control" id="tipocidadao_id" name="tipocidadao_id" 
               title="Tipo de Cidadão" data-toggle="tooltip" data-placement="top">
            <option value="">Todos os Tipos de Cidadãos</option>
            @foreach($tipocidadao as $tipo)
                <option value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->descricao}}</option>
            @endforeach
        </select>
    </div>
</div>
