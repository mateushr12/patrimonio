@include('layout_admin._includes.topo_print_callcenter')
    <table class="table table-bordered" style="font-size:87%;">
        <caption></caption>
        <thead>
            <tr class="bg-light">
                <th>Tipo de Cidadão</th>
                <th>Tipo de Problema</th>
                <th>Qtde</th>
                <th>%</th>
            </tr>
        </thead>

        <tbody>
        @if(session()->has('individuals'))
            @foreach( session()->get( 'individuals' ) as $key => $individual )
                <tr>
                    <td style="text-align: center;">{{ isset( session()->get( 'individuals' )[ $key ]->tipocidadao ) ? session()->get( 'individuals' )[ $key ]->tipocidadao : '' }}</td>
                    <td style="text-align: center;">{{ isset( session()->get( 'individuals' )[ $key ]->tipoproblema ) ? session()->get( 'individuals' )[ $key ]->tipoproblema : '' }}</td>
                    <td style="text-align: center;">{{ isset( session()->get( 'individuals' )[ $key ]->contador ) ? number_format( session()->get( 'individuals' )[ $key ]->contador, 0, ',', '.' ) : '' }}</td>
                    <td style="text-align: center;">{{ isset( session()->get( 'individuals' )[ $key ]->percentual ) ? number_format( session()->get( 'individuals' )[ $key ]->percentual, 2, ',','') : '' }}</td>
                </tr>
            @endforeach
        @endif
        </tbody>

        <tfoot>
            <tr>
                <td colspan="2" style="text-align: center;"><strong>TOTAL</strong></td>
                <td style="text-align: center;"><strong>{{ number_format( session()->get( 'individuals' )[0]->totalcontador, 0, ',', '.' ) }}</strong></td>
                <td></td>
            </tr>
        </tfoot>
    </table>
@include('layout_admin._includes.footer_print_callcenter')
