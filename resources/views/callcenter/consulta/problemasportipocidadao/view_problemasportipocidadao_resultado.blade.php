@extends('layout_admin.principal')
@section ('breadcrumb','Callcenter / Consulta')
@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        {{-- <strong>{{ $titulo }}</strong> --}}
                                        {{ $titulo }}
                                    </div>
                                </div>
                            </div>

                            <div class="card-body card-block">

                                @if ($errors->any()) <div class="alert alert-danger"> <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul> </div> @endif
                                @if (session('msg')) <div class="alert alert-danger alert-dismissible fade show" role="alert"> {{session('msg')}} <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> @endif

                                <div class="row">
                                    <div class="col-4">
                                        <a href="javascript:history.go(-1)"><button type="submit" class="au-btn-filter"><i class="zmdi zmdi-mail-reply"></i>Retornar</button></a>
                                        <a href="{{ route( 'consulta.rota_problemasportipocidadao_parametros' ) }}"><button type="submit" class="au-btn-filter"><i class="zmdi zmdi-filter-list"></i>Nova Consulta</button></a>
                                        @if ( count( session()->get( 'individuals' ) ) > 0 )
                                        <a target="_blank" href="{{ route( 'consulta.rota_problemasportipocidadao_imprimir', compact( 'titulo' ) ) }}"><button type="button" class="item" title="Imprimir" data-toggle="tooltip" data-placement="top"><i class="zmdi zmdi-print"></i> Imprimir</button></a>
                                        @endif
                                    </div>
                                    @if ( session()->get( 'individualsDetalhe' ) != null )
                                        <div class="col-8">
                                            <strong>Resultado da Pesquisa - Maiores Ocorrências entre os Tipos de Cidadãos - {{ count( session()->get( 'individualsDetalhe' ) ) }} registro(s)</strong>
                                        </div>
                                    @else
                                        <div class="col-8">
                                            <strong>Resultado da Pesquisa - {{ session()->get( 'totalregistros' ) }} registro(s)</strong>
                                        </div>
                                    @endif
                                </div>

                                @if ( session()->get( 'individualsDetalhe' ) != null )

                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr class="bg-light">
                                                <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Tipo de Cidadão</td>
                                                <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Tipo de Problema</td>
                                                <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Qtde Atendimentos</td>
                                                <td class="info small text-center font-weight-bold" style="vertical-align: middle;">%</td>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach( session()->get( 'individualsDetalhe' ) as $key => $individualDetalhe )
                                                <tr>
                                                    <td class="small text-center" style="vertical-align: middle;">{{ isset( session()->get( 'individualsDetalhe' )[ $key ]->tipocidadao ) ? session()->get( 'individualsDetalhe' )[ $key ]->tipocidadao : '' }}</td>
                                                    <td class="small text-center" style="vertical-align: middle;">{{ isset( session()->get( 'individualsDetalhe' )[ $key ]->tipoproblema ) ? session()->get( 'individualsDetalhe' )[ $key ]->tipoproblema : '' }}</td>
                                                    <td class="small text-center" style="vertical-align: middle;">{{ isset( session()->get( 'individualsDetalhe' )[ $key ]->contador ) ? number_format( session()->get( 'individualsDetalhe' )[ $key ]->contador, 0, ',', '.' ) : '' }}</td>
                                                    <td class="small text-center" style="vertical-align: middle;">{{ isset( session()->get( 'individualsDetalhe' )[ $key ]->percentual ) ? number_format( session()->get( 'individualsDetalhe' )[ $key ]->percentual, 2, ',','') : '' }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2" class="small text-center" style="vertical-align: middle;"><strong>TOTAL</strong></td>
                                                <td class="small text-center" style="vertical-align: middle;"><strong>{{ number_format( session()->get( 'individualsDetalhe' )[0]->totalcontador, 0, ',', '.' ) }}</strong></td>
                                                <td></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    
                                    <div class="row d-flex justify-content-center">
                                        {{-- $individuals->links() --}}
                                    </div>
                                    <div class="row d-flex justify-content-center">
                                        <div class="row">
                                            <div id="idChart" class="col-12">
                                            {{--
                                                {!! count(session()->get( 'individualsDetalhe' )) > 0 ? session()->get( 'chart' )->html() : '' !!}
                                            --}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-body card-block">
                                        <div id="grafico2" style="min-width: 300px; height: 400px; margin: 0 auto; border: 1px solid #ccc"></div>
                                        <!--
                                        <br>
                                        <div id="grafico2" style="min-width: 300px; height: 500px; margin: 0 auto; border: 1px solid #ccc"></div>
                                        -->
                                    </div>

                                    <div class="col-8">
                                        <strong>Resultado da Pesquisa - {{ session()->get( 'totalregistros' ) }} registro(s)</strong>
                                    </div>
                                @endif

                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light">
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Tipo de Cidadão</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Tipo de Problema</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Qtde</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">%</td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach( session()->get( 'individuals' ) as $key => $individual )
                                            <tr>
                                                <td class="small text-center" style="vertical-align: middle;">{{ isset( session()->get( 'individuals' )[ $key ]->tipocidadao ) ? session()->get( 'individuals' )[ $key ]->tipocidadao : '' }}</td>
                                                <td class="small text-center" style="vertical-align: middle;">{{ isset( session()->get( 'individuals' )[ $key ]->tipoproblema ) ? session()->get( 'individuals' )[ $key ]->tipoproblema : '' }}</td>
                                                <td class="small text-center" style="vertical-align: middle;">{{ isset( session()->get( 'individuals' )[ $key ]->contador ) ? number_format( session()->get( 'individuals' )[ $key ]->contador, 0, ',', '.' ) : '' }}</td>
                                                <td class="small text-center" style="vertical-align: middle;">{{ isset( session()->get( 'individuals' )[ $key ]->percentual ) ? number_format( session()->get( 'individuals' )[ $key ]->percentual, 2, ',','') : '' }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2" class="text-center" style="vertical-align: middle;"><strong>TOTAL</strong></td>
                                            <td class="text-center" style="vertical-align: middle;"><strong>{{ number_format( session()->get( 'individuals' )[0]->totalcontador, 0, ',', '.' ) }}</strong></td>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                </table>

                                <div class="row d-flex justify-content-center">
                                    {{-- $individuals->links() --}}
                                </div>

                                <div class="card-body card-block">
                                    <div id="grafico1" style="min-width: 300px; height: 400px; margin: 0 auto; border: 1px solid #ccc"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- Gráfico 2 --}}
    <script>
        $(function() {
            var chart;
            var categories = <?php echo json_encode( session()->get( 'grafico2textoEixoY' ) ) ?>;

            $(document).ready(function() {
                chart = new Highcharts.Chart({
                    exporting: { enabled: false },
                    credits:{ enabled: false },
                    chart: { renderTo: 'grafico2', type: 'column', options3d: { enabled: true, alpha: 10, beta: 5, depth: 60 } },
                    title: { text: '<b>{{ session()->get( 'grafico2tituloGrafico' ) }}</b>' },
                    xAxis: { categories: categories, labels: { enabled: true } },
                    yAxis: { min: 0, title: { text: '{{ session()->get( 'grafico2eixoX' ) }}' } },
                    plotOptions: {
                        series: {
                        dataLabels: {
                            formatter: function() {
                            return '' + new Intl.NumberFormat('de-DE').format(this.y); },
                            enabled: true,
                            // rotation: -90,
                            align: 'right', color: 'black', shadow: false,
                            x: -10,
                            style: { "fontSize": "12px", "textShadow": "0px" }
                        },
                        pointPadding: 0.1, groupPadding: 0 } },
                    tooltip: { formatter: function() { return this.x + ': ' + new Intl.NumberFormat('de-DE').format(this.y); } },
                    series: [{
                        type: 'column',
                        name: '{{ session()->get( 'grafico2tituloSeries' ) }}',
                        data: <?php echo json_encode( session()->get( 'grafico2valoresEixoYCategoria1' ), JSON_NUMERIC_CHECK ) ?>,
                        stacking: 'normal'
                    }]
                });
            });
        });
        // final grafico 1
     
    </script>


    {{-- Gráfico 1 --}}
    <script>
        $(function() {
            var chart;
            var categories = <?php echo json_encode( session()->get( 'textoEixoY' ) ) ?>;

            $(document).ready(function() {
                chart = new Highcharts.Chart({
                    exporting: { enabled: false },
                    credits:{ enabled: false },
                    chart: { renderTo: 'grafico1', type: 'column', options3d: { enabled: true, alpha: 10, beta: 5, depth: 60 } },
                    title: { text: '<b>{{ session()->get( 'tituloGrafico' ) }}</b>' },
                    xAxis: { categories: categories, labels: { enabled: true } },
                    yAxis: { min: 0, title: { text: '{{ session()->get( 'eixoX' ) }}' } },
                    plotOptions: {
                        series: {
                        dataLabels: {
                            formatter: function() {
                            return '' + new Intl.NumberFormat('de-DE').format(this.y); },
                            enabled: true,
                            // rotation: -90,
                            align: 'right', color: 'black', shadow: false,
                            x: -10,
                            style: { "fontSize": "12px", "textShadow": "0px" }
                        },
                        pointPadding: 0.1, groupPadding: 0 } },
                    tooltip: { formatter: function() { return this.x + ': ' + new Intl.NumberFormat('de-DE').format(this.y); } },
                    series: [{
                        type: 'column',
                        name: '{{ session()->get( 'tituloSeries' ) }}',
                        data: <?php echo json_encode( session()->get( 'valoresEixoYCategoria1' ), JSON_NUMERIC_CHECK ) ?>,
                        stacking: 'normal'
                    }]
                });
            });
        });
    
    </script>
@endsection
