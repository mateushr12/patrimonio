<div class="row">
    <div class="form-group col-sm-3">
        <label for="FonteAcao">Período:</label>
        <select class="form-control" id="id_periodo" name="id_periodo" required>
            <option value="1">Hoje</option>
            <option value="2">3 Dias</option>
            <option value="3">5 Dias</option>
            <option value="4">1 Semana</option>
            <option value="5">Mês Atual</option>
            <option value="6">3 Ultimos Meses</option>
            <option value="7">Semestre Atual</option>
            <option value="8">Ano Atual</option>
            <option value="9">Período Específico</option>
        </select>
        </select>
    </div>
    <div class="form-group col-sm-3">
        <label for="FonteAcao">Usuário:</label>
        <select class="form-control" id="id" name="id" required>
            <option value="">Selecione o Usuário</option>
            @foreach($usuarioredirecionado as $tipo)
                <option value="{{ $tipo->id }}">{{ $tipo->user_id_red }} - {{$tipo->usuariored->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-sm-3">
        <label for="FonteAcao">Chamada redirecionado para:</label>
        <select class="form-control" id="gruposuser_id_red" name="gruposuser_id_red" 
               title="Chamadas redirecionadas para um grupo específico" data-toggle="tooltip" data-placement="top" required>
            <option value="">Selecione o Nível</option>
            @foreach($gruporedirecionado as $tipo)
                <option value="{{ $tipo->id }}">{{ $tipo->gruposuser_id_red }} - {{$tipo->grupousuariored->nome}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="id_agiliza">Protocolo Agiliza</label>
        <input type="text" class="form-control" id="id_agiliza" name="id_agiliza" placeholder=""
               value="" autocomplete="off" required>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-3">
        <label for="FonteAcao">Tipo de Cidadão:</label>
        <select class="form-control" id="tipocidadao_id" name="tipocidadao_id" 
               title="Tipo de Cidadão informado" data-toggle="tooltip" data-placement="top" required>
            <option value="">Selecione o Tipo de Cidadão</option>
            @foreach($tipocidadao as $tipo)
                <option value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->descricao}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="cpf">C.P.F. Cidadão</label>
        <input type="text" class="form-control" id="cpf" name="cpf" placeholder=""
               value="" autocomplete="off" 
               title="C.P.F. do Cidadão" data-toggle="tooltip" data-placement="top" required>
    </div>
    <div class="form-group col-md-3">
        <label for="email">E-mail do Cidadão</label>
        <input type="text" class="form-control" id="email" name="email" placeholder=""
               value="" autocomplete="off" required>
    </div>
    <div class="form-group col-md-3">
        <label for="nome">Nome do Cidadão</label>
        <input type="text" class="form-control" id="nome" name="nome" placeholder=""
               value="" autocomplete="off" required>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-3">
        <label for="FonteAcao">Tipo de Problema:</label>
        <select class="form-control" id="tipoproblema_id" name="tipoproblema_id" required>
            <option value="">Selecione o Tipo de Problema</option>
            @foreach($tipoproblema as $tipo)
                <option value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->descricao}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="complemento_problema">Complemento do Problema</label>
        <input type="text" class="form-control" id="cpf" name="complemento_problema" placeholder=""
               value="" autocomplete="off" required>
    </div>
    <div class="form-group col-sm-3">
        <label for="FonteAcao">Tipo de Solução:</label>
        <select class="form-control" id="tiposolucao_id" name="tiposolucao_id" required>
            <option value="">Selecione o Tipo de Solução</option>
            @foreach($tiposolucao as $tipo)
                <option value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->descricao}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="complemento_solucao">Complemento da Solução</label>
        <input type="text" class="form-control" id="complemento_solucao" name="complemento_solucao" placeholder=""
               value="" autocomplete="off" required>
    </div>
</div>

<div class="row">
</div>
<div class="row">
    <input type="hidden" name="user_id" value="{{auth()->user()->id}}" />
    {{--
    <div class="form-group col-md-12">
        <label for="id">Usuário Logado</label>
        <input type="text" class="form-control" id="id" name="id" placeholder="Usuário Logado"
               value="{{ auth()->user()->id }} - {{ auth()->user()->name }}" autocomplete="off" required>
    </div>
    --}}
</div>
