@extends('layout_admin.principal')
@section ('breadcrumb','Callcenter / Consulta')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Consulta - Lista de Chamadas - Geral</strong>
                                    </div>
                                    {{--
                                    <div class="col-2"><button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#FormModal"><i class="zmdi zmdi-plus"></i>Nova Chamada</button></div>
                                    --}}
                                </div>
                            </div>

                            <div class="card-body card-block">

                                @if ($errors->any()) <div class="alert alert-danger"> <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul> </div> @endif
                                @if (session('msg')) <div class="alert alert-danger alert-dismissible fade show" role="alert"> {{session('msg')}} <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> @endif

                                {{--
                                <div class="table-data__tool">
                                --}}
                                    {{-- {{ route('callcenter.chamada.acoes') }}searchHistoric
                                        {{ route('callcenter.consulta.acoes.filtrar') }}
                                    --}}
                                    <form action="" method="POST">
                                        {{ csrf_field() }}

                                        <div class="table-data__tool-left">
                                            @include('callcenter.consulta.chamadageral._formConsultaChamadaGeral')
                                        </div>

                                        <div class="table-data__tool-left">
                                            {{--
                                            <div class="rs-select2--light rs-select2--md">
                                                <select class="js-select2" name="coluna">
                                                    <option selected="selected">Todas Colunas</option>
                                                    <option value="">Option 1</option>
                                                    <option value="">Option 2</option>
                                                </select>
                                                <div class="dropDownSelect2"></div>
                                            </div>
                                            --}}
                                            <div class="rs-select2--light rs-select2--md">
                                                <select class="js-select2" name="periodo">
                                                    <option selected="selected">Período</option>
                                                    <option value="">Hoje</option>
                                                    <option value="">3 Dias</option>
                                                    <option value="">1 Semana</option>
                                                    <option value="">Mês Atual</option>
                                                    <option value="">Semestre Atual</option>
                                                    <option value="">Ano Atual</option>
                                                </select>
                                                <div class="dropDownSelect2"></div>
                                            </div>
                                            <button type="submit" class="au-btn-filter">
                                                <i class="zmdi zmdi-filter-list"></i>Pesquisar
                                            </button>
                                            {{--
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="criterio" placeholder="Buscar por ...">
                                                <div class="input-group-append">
                                                    <button class="btn btn-success">Aplicar Filtro</button>
                                                </div>
                                            </div>
                                            --}}
                                        </div>
                                        {{--
                                        <div class="table-data__tool-right">
                                            <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                                <select class="js-select2" name="type">
                                                    <option selected="selected">Exportar</option>
                                                    <option value="">PDF</option>
                                                    <option value="">Planilha</option>
                                                </select>
                                                <div class="dropDownSelect2"></div>
                                            </div>
                                        </div>
                                        --}}
                                    </form>

                                {{--
                                </div>
                                --}}

                                <div class="card-body card-block">
                                    <div class="row">
                                        <div class="col-12">
                                            <strong>Resultado da Pesquisa - {{ session()->get( 'totalregistros' ) }} registro(s)</strong>
                                        </div>
                                    </div>
                                </div>

                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light">
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Protocolo</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Tempo</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Cidadão</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Contato</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Problema</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Protocolo Agiliza</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Grupo / Nível</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Criação / Modificação</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Usuário</td>
                                            {{--
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle; width: 80px;">Ação</td>
                                            --}}
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach( $individuals as $individual )
                                            <tr>
                                                <td class="small text-center" style="vertical-align: middle;">{{ $individual->id }}</td>
                                                <td class="small text-center" style="vertical-align: middle;">
                                                    {{ dataBR( $individual->data_hora_inicio ) }} <br>
                                                    {{ horaBR( $individual->data_hora_inicio ) }} a {{ horaBR( $individual->data_hora_fim ) }} <br>
                                                    <strong>Tempo: </strong>{{ gmdate( "H:i:s", $individual->Diferencadatas ) }}</td>
                                                <td class="small text-left" style="vertical-align: middle;">
                                                    <strong>C.P.F.: </strong>{{ mask( $individual->cpf, '###.###.###-##' ) }} <br>
                                                    <strong>Nome: </strong>{{ $individual->nome }}</td>
                                                <td class="small text-left" style="vertical-align: middle;">
                                                    <strong>Fone Fixo: </strong>{{ mask( $individual->fone_fixo, '(##) ####-####' ) }} <br>
                                                    <strong>Celular: </strong>{{ mask( $individual->fone_celular, '(##) #####-####' ) }} <br>
                                                    <strong>Celular2: </strong>{{ mask( $individual->fone_celular2, '(##) #####-####' ) }} <br>
                                                    <strong>Email: </strong>{{ $individual->email }}</td>
                                                <td class="small text-left" style="vertical-align: middle;">
                                                    <strong>Tipo de Problema: </strong>
                                                    @if ( $individual->tipoproblema_id )
                                                        {{ $individual->tipoproblema->descricao }} <br>
                                                    @else
                                                        - <br>
                                                    @endif
                                                    <strong>Complemento: </strong>{{ $individual->complemento_problema }} <br>
                                                    <strong>Tipo de Solução: </strong>
                                                    @if ( $individual->tiposolucao_id )
                                                        {{ $individual->tiposolucao->descricao }} <br>
                                                    @else
                                                        - <br>
                                                    @endif
                                                    <strong>Complemento: </strong>{{ $individual->complemento_solucao }}</td>
                                                <td class="small text-center" style="vertical-align: middle;">
                                                    @if ( $individual->id_agiliza )
                                                        {{ $individual->id_agiliza }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="small text-center" style="vertical-align: middle;">
                                                    @if ( $individual->gruposuser_id )
                                                        {{ $individual->gruposuser->nome }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="small text-left" style="vertical-align: middle;">
                                                    <strong>Criação: </strong>{{ isset( $individual->created_at ) ? $individual->created_at->format('d/m/Y H:i:s') : '' }} <br>
                                                    <strong>Modificação: </strong>{{ isset( $individual->updated_at ) ? $individual->updated_at->format('d/m/Y H:i:s') : '' }}
                                                </td>
                                                <td class="small text-center" style="vertical-align: middle;">
                                                    @if ( $individual->user_id )
                                                        {{ $individual->user->name }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                {{--
                                                <td class="text-center" style="vertical-align: middle;">
                                                    <div class="table-data-feature">
                                                        <a href="{{route('callcenter.chamada.acoes.editar', $individual->id)}}">
                                                            <button type="button"  class="item"  title="Editar"  data-toggle="modal"
                                                                    data-target="#EditarModal" style="background-color: yellow"
                                                                    data-whatever="{{ route ('callcenter.chamada.acoes.editar', $individual->id) }}">
                                                                <i class="zmdi zmdi-edit"></i>
                                                            </button>
                                                        </a>
                                                        &nbsp;
                                                        <a href="{{route('callcenter.chamada.acoes.deletar', $individual->id)}}" data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                            <button type="button"  class="item"  title="Deletar" style="background-color: red">
                                                                <i class="zmdi zmdi-delete" style="color: white"></i>
                                                            </button>
                                                        </a>
                                                    </div>
                                                </td>
                                                --}}
                                            </tr>
                                        @endforeach
                                    </tbody>

                                    {{--
                                    <tfoot>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                    --}}

                                </table>

                                <div class="row d-flex justify-content-center">
                                    {{ $individuals->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
