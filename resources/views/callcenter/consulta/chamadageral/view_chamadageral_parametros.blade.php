@extends('layout_admin.principal')
@section ('breadcrumb','Callcenter / Consulta')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Consulta - Chamada Geral - Opções de Pesquisa</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">

                                @if ($errors->any()) <div class="alert alert-danger"> <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul> </div> @endif
                                @if (session('msg')) <div class="alert alert-danger alert-dismissible fade show" role="alert"> {{session('msg')}} <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> @endif

                                <form class="" action="{{route('consulta.rota_chamadageral_resultado')}}" method="post">
                                    {{ csrf_field() }}

                                    <div class="table-data__tool-left">
                                        @include('callcenter.consulta.chamadageral._form_parametros_chamadageral')
                                    </div>

                                    <div class="table-data__tool-left">
                                        <button type="submit" class="au-btn-filter">
                                            <i class="zmdi zmdi-filter-list" 
                                                title="Inicia a pesquisa de acordo com os critérios escolhidos" data-toggle="tooltip" data-placement="top"></i>
                                                <strong title="Inicia a pesquisa de acordo com os critérios escolhidos" data-toggle="tooltip" data-placement="top">Pesquisar</strong>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
