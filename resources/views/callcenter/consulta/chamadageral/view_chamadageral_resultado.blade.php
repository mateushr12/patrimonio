@extends('layout_admin.principal')
@section ('breadcrumb','Callcenter / Consulta')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        {{-- <strong>{{ $titulo }}</strong> --}}
                                        {{ $titulo }}
                                    </div>
                                </div>
                            </div>

                            <div class="card-body card-block">

                                @if ($errors->any()) <div class="alert alert-danger"> <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul> </div> @endif
                                @if (session('msg')) <div class="alert alert-danger alert-dismissible fade show" role="alert"> {{session('msg')}} <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> @endif

                                <div class="row">
                                    <div class="col-4">
                                        <a href="javascript:history.go(-1)"><button type="submit" class="au-btn-filter"><i class="zmdi zmdi-mail-reply"></i>Retornar</button></a>
                                        <a href="{{ route( 'consulta.rota_chamadageral_parametros' ) }}"><button type="submit" class="au-btn-filter"><i class="zmdi zmdi-filter-list"></i>Nova Consulta</button></a>
                                        @if ( count( session()->get( 'individuals' ) ) > 0 )
                                        <a target="_blank" href="{{ route( 'consulta.rota_chamadageral_imprimir', compact( 'titulo' ) ) }}"><button type="button" class="item" title="Imprimir" data-toggle="tooltip" data-placement="top"><i class="zmdi zmdi-print"></i> Imprimir</button></a>
                                        @endif
                                    </div>
                                    <div class="col-8">
                                        <strong>Resultado da Pesquisa - {{ session()->get( 'totalregistros' ) }} registro(s)</strong>
                                    </div>
                                </div>

                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light">
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Protocolo / Cidadão</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Atendimento: Problema / Solução / Agiliza</td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach( session()->get( 'individuals' ) as $key => $individual )
                                            <tr>
                                                <td class="small text-left" style="vertical-align: middle;">
                                                        <strong>Protocolo:                  </strong> {{ isset( session()->get( 'individuals' )[ $key ]->protocolo ) ? mask( session()->get( 'individuals' )[ $key ]->protocolo, '#### #### #### #### ####' ) : '' }}
                                                    <br><strong>Ocorrência:                 </strong> {{ isset( session()->get( 'individuals' )[ $key ]->data_hora_inicio ) ? dataBR( session()->get( 'individuals' )[ $key ]->data_hora_inicio ) : '' }} das {{ isset( session()->get( 'individuals' )[ $key ]->data_hora_inicio ) ? horaBR( session()->get( 'individuals' )[ $key ]->data_hora_inicio ) : '' }} às {{ isset( session()->get( 'individuals' )[ $key ]->data_hora_fim ) ? horaBR( session()->get( 'individuals' )[ $key ]->data_hora_fim ) : '' }} (duração: {{ isset( session()->get( 'individuals' )[ $key ]->diferencadatas ) ? horaAnormal( session()->get( 'individuals' )[ $key ]->diferencadatas ) : '' }} )
                                                    <br><strong>C.P.F.:                     </strong> {{ isset( session()->get( 'individuals' )[ $key ]->cpf ) ? mask( session()->get( 'individuals' )[ $key ]->cpf, '###.###.###-##' ) : '' }}
                                                    <br><strong>Tipo Cidadão:               </strong> {{ isset( session()->get( 'individuals' )[ $key ]->tipocidadao->descricao ) ? session()->get( 'individuals' )[ $key ]->tipocidadao->descricao : '' }}
                                                    <br><strong>Nome:                       </strong> {{ isset( session()->get( 'individuals' )[ $key ]->nome ) ? session()->get( 'individuals' )[ $key ]->nome : '' }}
                                                    <br><strong>Fone Fixo:                  </strong> {{ isset( session()->get( 'individuals' )[ $key ]->fone_fixo ) ? mask( session()->get( 'individuals' )[ $key ]->fone_fixo, '(##) ####-####' ) : '' }} | 
                                                        <strong>Celular:                    </strong> {{ isset( session()->get( 'individuals' )[ $key ]->fone_celular ) ? mask( session()->get( 'individuals' )[ $key ]->fone_celular, '(##) #####-####' ) : '' }} | 
                                                        <strong>Celular2:                   </strong> {{ isset( session()->get( 'individuals' )[ $key ]->fone_celular2 ) ? mask( session()->get( 'individuals' )[ $key ]->fone_celular2, '(##) #####-####' ) : '' }}
                                                    <br><strong>Email:                      </strong> {{ isset( session()->get( 'individuals' )[ $key ]->email ) ? session()->get( 'individuals' )[ $key ]->email : '' }}
                                                    <br><strong>Deseja Receber Informativo: </strong> @if ( session()->get( 'individuals' )[ $key ]->cidadao_id ) @if ( session()->get( 'individuals' )[ $key ]->receber_informativo === 1 ) {{ 'Sim' }} @elseif ( session()->get( 'individuals' )[ $key ]->receber_informativo === 0 ) {{ 'Não' }} @else {{ 'Não Informado' }} @endif @endif
                                                    <br><strong>Tipo de Atendimento:        </strong> @if ( session()->get( 'individuals' )[ $key ]->tipoatendimento_id ) {{ session()->get( 'individuals' )[ $key ]->tipoatendimento->descricao }} @endif
                                                </td>

                                                <td class="small text-left" style="vertical-align: middle;">
                                                        <strong>Tipo de Problema:                     </strong> {{ isset(session()->get( 'individuals' )[ $key ]->tipoproblema->descricao) ? session()->get( 'individuals' )[ $key ]->tipoproblema->descricao : '' }}
                                                    <br><strong>Complemento do Problema:              </strong> {{ isset(session()->get( 'individuals' )[ $key ]->complemento_problema) ? session()->get( 'individuals' )[ $key ]->complemento_problema : '' }} 
                                                    <br><strong>Tipo de Solução:                      </strong> {{ isset(session()->get( 'individuals' )[ $key ]->tiposolucao->descricao) ? session()->get( 'individuals' )[ $key ]->tiposolucao->descricao : '' }}
                                                    <br><strong>Complemento da Solução:               </strong> {{ isset(session()->get( 'individuals' )[ $key ]->complemento_solucao) ? session()->get( 'individuals' )[ $key ]->complemento_solucao : ''  }} 
                                                    <br><strong>Protocolo Agiliza:                    </strong> {{ isset(session()->get( 'individuals' )[ $key ]->id_agiliza) ? session()->get( 'individuals' )[ $key ]->id_agiliza : '' }}
                                                    <br><strong>Usuário que atendeu:                  </strong> {{ isset(session()->get( 'individuals' )[ $key ]->usuariocadastro->name) ? session()->get( 'individuals' )[ $key ]->usuariocadastro->name : '' }} ({{ isset(session()->get( 'individuals' )[ $key ]->usuariocadastro->grupo->nome) ? session()->get( 'individuals' )[ $key ]->usuariocadastro->grupo->nome : '' }})
                                                    <br><strong>Redirecionado para Grupo:             </strong> {{ isset(session()->get( 'individuals' )[ $key ]->grupousuariored->nome) ? session()->get( 'individuals' )[ $key ]->grupousuariored->nome : '' }}
                                                    <br><strong>Redirecionado para Usuário:           </strong> {{ isset(session()->get( 'individuals' )[ $key ]->usuariored->name) ? session()->get( 'individuals' )[ $key ]->usuariored->name : '' }} ({{ isset(session()->get( 'individuals' )[ $key ]->usuariored->grupo->nome) ? session()->get( 'individuals' )[ $key ]->usuariored->grupo->nome : '' }})
                                                    <br><strong>Data/Hora Solução redirecionamento:   </strong> @if ( isset(session()->get( 'individuals' )[ $key ]->data_hora_red_finalizado) ) <strong class="text-success">{{ datahoraBR( session()->get( 'individuals' )[ $key ]->data_hora_red_finalizado ) }}</strong> @elseif ( !session()->get( 'individuals' )[ $key ]->data_hora_red_finalizado && !session()->get( 'individuals' )[ $key ]->user_id_atendeu_red && ( session()->get( 'individuals' )[ $key ]->gruposuser_id_red || session()->get( 'individuals' )[ $key ]->user_id_red ) ) <strong class="text-danger"> - Pendente -</strong> @endif
                                                    <br><strong>Usuário que atendeu redirecionamento: </strong> @if ( isset(session()->get( 'individuals' )[ $key ]->usuarioatendeu->name) ) <strong class="text-success">{{ session()->get( 'individuals' )[ $key ]->usuarioatendeu->name }} ({{ session()->get( 'individuals' )[ $key ]->usuarioatendeu->grupo->nome }})</strong> @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>

                                    </table>
                                <div class="row d-flex justify-content-center">
                                    {{-- session()->get( 'individuals' )[ $key ]->links() --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
