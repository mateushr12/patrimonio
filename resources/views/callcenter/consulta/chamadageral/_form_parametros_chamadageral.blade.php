<div class="row">
    <div class="form-group col-md-3">
        <label for="data_inicial">Data Inicial</label>
        <input type="date" class="form-control" id="data_inicial" name="data_inicial" value={{ now() }} 
               title="Data inicial do período desejado" data-toggle="tooltip" data-placement="top" autofocus>
    </div>
    <div class="form-group col-md-3">
        <label for="data_final">Data Final</label>
        <input type="date" class="form-control" id="data_final" name="data_final" value={{ now() }} 
               title="Data Final do período desejado" data-toggle="tooltip" data-placement="top">
    </div>
    <div class="form-group col-sm-3">
        <label for="gruposuser_id_red">Chamada Redirecionada para o grupo:</label>
        <select class="form-control" id="gruposuser_id_red" name="gruposuser_id_red" autocomplete="off" 
               title="Chamadas redirecionadas para um grupo específico" data-toggle="tooltip" data-placement="top">
            <option value="">Selecione o Grupo</option>
            @foreach($gruporedirecionado as $tipo)
                <option value="{{ $tipo->gruposuser_id_red }}">{{ isset( $tipo->gruposuser_id_red ) ? $tipo->gruposuser_id_red : '' }} - {{ isset( $tipo->grupousuariored->nome ) ? $tipo->grupousuariored->nome : '' }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-sm-3">
        <label for="user_id_red">Chamada Redirecionada para o usuário:</label>
        <select class="form-control" id="user_id_red" name="user_id_red" autocomplete="off" 
               title="Chamadas redirecionadas para um usuário específico" data-toggle="tooltip" data-placement="top">
            <option value="">Selecione o Usuário</option>
            @foreach($usuarioredirecionado as $tipo)
                <option value="{{ $tipo->user_id_red }}">{{ isset( $tipo->user_id_red ) ? $tipo->user_id_red : '' }} - {{ isset( $tipo->usuariored->name ) ? $tipo->usuariored->name : '' }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-3">
        <label for="tipocidadao_id">Tipo de Cidadão:</label>
        <select class="form-control" id="tipocidadao_id" name="tipocidadao_id" 
               title="Tipo de Cidadão" data-toggle="tooltip" data-placement="top">
            <option value="">Todos os Cidadãos</option>
            @foreach($tipocidadao as $tipo)
                <option value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->descricao}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="cpf">C.P.F. Cidadão</label>
        <input type="text" class="form-control" id="cpf" name="cpf" placeholder=""
               value="" autocomplete="off" 
               title="C.P.F. do Cidadão" data-toggle="tooltip" data-placement="top">
    </div>
    <div class="form-group col-md-3">
        <label for="email">E-mail do Cidadão</label>
        <input type="text" class="form-control" id="email" name="email" placeholder=""
               value="" autocomplete="off" 
               title="E-mail do Cidadão" data-toggle="tooltip" data-placement="top">
    </div>
    <div class="form-group col-md-3">
        <label for="nome">Nome do Cidadão</label>
        <input type="text" class="form-control" id="nome" name="nome" placeholder=""
               value="" autocomplete="off"  
                title="Nome do Cidadão" data-toggle="tooltip" data-placement="top">
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-3">
        <label for="tipoproblema_id">Tipo de Problema:</label>
        <select class="form-control" id="tipoproblema_id" name="tipoproblema_id" 
               title="Tipo de Problema relatado pelo cidadão" data-toggle="tooltip" data-placement="top">
            <option value="">Todos os Tipos de Problema</option>
            @foreach($tipoproblema as $tipo)
                <option value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->descricao}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="complemento_problema">Complemento do Problema</label>
        <input type="text" class="form-control" id="complemento_problema" name="complemento_problema" placeholder=""
               value="" autocomplete="off" 
               title="Complemento do Tipo de Problema relatado pelo cidadão" data-toggle="tooltip" data-placement="top">
    </div>
    <div class="form-group col-sm-3">
        <label for="tiposolucao_id">Tipo de Solução:</label>
        <select class="form-control" id="tiposolucao_id" name="tiposolucao_id" 
               title="Tipo de Solução escolhido pelo atendente da chamada" data-toggle="tooltip" data-placement="top">
            <option value="">Todos os Tipos de Solução</option>
            @foreach($tiposolucao as $tipo)
                <option value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->descricao}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="complemento_solucao">Complemento da Solução</label>
        <input type="text" class="form-control" id="complemento_solucao" name="complemento_solucao" placeholder=""
               value="" autocomplete="off" 
               title="Complemento do Tipo de Solução escolhido pelo atendente da chamada" data-toggle="tooltip" data-placement="top">
    </div>
</div>

<div class="row">
    <div class="form-group col-md-3">
        <label for="id_agiliza">Protocolo Agiliza</label>
        <input type="text" class="form-control" id="id_agiliza" name="id_agiliza" placeholder="" maxlength="13"
               value="" autocomplete="off"  
               title="Número do Protocolo Agiliza" data-toggle="tooltip" data-placement="top">
    </div>
    {{--
    <div class="form-group col-sm-3">
        <label for="receber_informativo">Recebe Informativo JUCESE?:</label>
        <select class="form-control" id="receber_informativo" name="receber_informativo" 
               title="Informe se o cidadão deseja receber informativo JUCESE por e-mail" data-toggle="tooltip" data-placement="top">
            <option value="">Selecione a opção</option>
            <option value="0">NÃO</option>
            <option value="1">SIM</option>
        </select>
    </div>
    --}}
    <div class="form-group col-sm-3">
        <label for="tipoatendimento_id">Tipo de Atendimento:</label>
        <select class="form-control" id="tipoatendimento_id" name="tipoatendimento_id" autocomplete="off" 
               title="Tipo de Atendimento realizado pelo atendente" data-toggle="tooltip" data-placement="top">
            <option value="">Selecione o Tipo de Atendimento</option>
            @foreach($tipoatendimento as $tipo)
                <option {{-- $individuals->tipoatendimento_id == $tipo->tipoatendimento_id ? 'selected' : '' --}} value="{{ $tipo->id }}">{{ $tipo->id }} - {{$tipo->descricao}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-sm-3">
        <label for="user_id">Usuário Atendimento:</label>
        <select class="form-control" id="user_id" name="user_id" 
               title="Usuário que atendeu a chamada" data-toggle="tooltip" data-placement="top">
            <option value="">Todos os Usuários</option>
            @foreach($usuarioatendimento as $tipo)
                <option value="{{ $tipo->user_id }}">{{ isset( $tipo->user_id ) ? $tipo->user_id : '' }} - {{ isset( $tipo->usuariocadastro->name ) ? $tipo->usuariocadastro->name : '' }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-sm-3">
        <label for="max_registros">Máx. Registros</label>
        <input type="number" class="form-control" id="max_registros" name="max_registros" value="15" 
               title="Número máximo de registros a ser retornado na consulta" data-toggle="tooltip" data-placement="top">
    </div>
</div>

<div class="row">
    {{--
    <input type="hidden" name="user_id" value="{{auth()->user()->id}}" />
    <div class="form-group col-md-12">
        <label for="id">Usuário Logado</label>
        <input type="text" class="form-control" id="id" name="id" placeholder="Usuário Logado"
               value="{{ auth()->user()->id }} - {{ auth()->user()->name }}" autocomplete="off">
    </div>
    --}}
</div>
