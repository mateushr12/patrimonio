@include('layout_admin._includes.topo_print_callcenter')
<table class="table table-bordered" style="font-size:87%;">
        <caption></caption>
        <thead>
            <tr class="bg-light">
                <th>Protocolo / Cidadão</th>
                <th>Atendimento: Problema / Solução / Agiliza</th>
            </tr>
        </thead>

        <tbody>
        @if(session()->has('individuals'))
            @foreach( session()->get( 'individuals' ) as $key => $individual )
                <tr>
                    <td class="small text-center" style="vertical-align: middle;">
                            <strong>Protocolo:                  </strong> {{ isset( session()->get( 'individuals' )[ $key ]->protocolo ) ? mask( session()->get( 'individuals' )[ $key ]->protocolo, '#### #### #### #### ####' ) : '' }}
                        <br><strong>Ocorrência:                 </strong> {{ isset( session()->get( 'individuals' )[ $key ]->data_hora_inicio ) ? dataBR( session()->get( 'individuals' )[ $key ]->data_hora_inicio ) : '' }} das {{ isset( session()->get( 'individuals' )[ $key ]->data_hora_inicio ) ? horaBR( session()->get( 'individuals' )[ $key ]->data_hora_inicio ) : '' }} às {{ isset( session()->get( 'individuals' )[ $key ]->data_hora_fim ) ? horaBR( session()->get( 'individuals' )[ $key ]->data_hora_fim ) : '' }} (duração: {{ isset( session()->get( 'individuals' )[ $key ]->diferencadatas ) ? horaAnormal( session()->get( 'individuals' )[ $key ]->diferencadatas ) : '' }} )
                        <br><strong>C.P.F.:                     </strong> @if ( session()->get( 'individuals' )[ $key ]->cidadao_id ) {{ mask( session()->get( 'individuals' )[ $key ]->cidadao->cpf, '###.###.###-##' ) }} @endif 
                        <br><strong>Tipo Cidadão:               </strong> @if ( session()->get( 'individuals' )[ $key ]->tipocidadao_id ) {{ session()->get( 'individuals' )[ $key ]->tipocidadao->descricao }} @endif
                        <br><strong>Nome:                       </strong> @if ( session()->get( 'individuals' )[ $key ]->cidadao_id ) {{ session()->get( 'individuals' )[ $key ]->cidadao->nome }} @endif
                        <br><strong>Fone Fixo:                  </strong> @if ( session()->get( 'individuals' )[ $key ]->cidadao_id ) {{ mask( session()->get( 'individuals' )[ $key ]->cidadao->fone_fixo, '(##) ####-####' ) }} | @endif
                            <strong>Celular:                    </strong> @if ( session()->get( 'individuals' )[ $key ]->cidadao_id ) {{ mask( session()->get( 'individuals' )[ $key ]->cidadao->fone_celular, '(##) #####-####' ) }} | @endif
                            <strong>Celular2:                   </strong> @if ( session()->get( 'individuals' )[ $key ]->cidadao_id ) {{ mask( session()->get( 'individuals' )[ $key ]->cidadao->fone_celular2, '(##) #####-####' ) }} @endif
                        <br><strong>Email:                      </strong> @if ( session()->get( 'individuals' )[ $key ]->cidadao_id ) {{ session()->get( 'individuals' )[ $key ]->cidadao->email }} @endif
                        <br><strong>Deseja Receber Informativo: </strong> @if ( session()->get( 'individuals' )[ $key ]->cidadao_id ) @if ( session()->get( 'individuals' )[ $key ]->cidadao->receber_informativo === 1 ) {{ 'Sim' }} @elseif ( session()->get( 'individuals' )[ $key ]->cidadao->receber_informativo === 0 ) {{ 'Não' }} @else {{ 'Não Informado' }} @endif @endif
                        <br><strong>Tipo de Atendimento:        </strong> @if ( session()->get( 'individuals' )[ $key ]->tipoatendimento_id ) {{ session()->get( 'individuals' )[ $key ]->tipoatendimento->descricao }} @endif
                    </td>

                    <td class="small text-left" style="vertical-align: middle;">
                            <strong>Tipo de Problema:                     </strong> @if ( session()->get( 'individuals' )[ $key ]->tipoproblema_id ) {{ isset(session()->get( 'individuals' )[ $key ]->tipoproblema->descricao) ? session()->get( 'individuals' )[ $key ]->tipoproblema->descricao : '' }} @else - @endif
                        <br><strong>Complemento do Problema:              </strong> {{ isset(session()->get( 'individuals' )[ $key ]->complemento_problema) ? session()->get( 'individuals' )[ $key ]->complemento_problema : '' }} 
                        <br><strong>Tipo de Solução:                      </strong> @if ( isset(session()->get( 'individuals' )[ $key ]->tiposolucao_id) ) {{ isset(session()->get( 'individuals' )[ $key ]->tiposolucao->descricao) ? session()->get( 'individuals' )[ $key ]->tiposolucao->descricao : '' }} @else - @endif
                        <br><strong>Complemento da Solução:               </strong> {{ isset(session()->get( 'individuals' )[ $key ]->complemento_solucao) ? session()->get( 'individuals' )[ $key ]->complemento_solucao : ''  }} 
                        <br><strong>Protocolo Agiliza:                    </strong> {{ isset(session()->get( 'individuals' )[ $key ]->id_agiliza) ? session()->get( 'individuals' )[ $key ]->id_agiliza : '' }}
                        <br><strong>Usuário que atendeu:                  </strong> {{ isset(session()->get( 'individuals' )[ $key ]->usuariocadastro->name) ? session()->get( 'individuals' )[ $key ]->usuariocadastro->name : '' }} {{ isset(session()->get( 'individuals' )[ $key ]->usuariocadastro->grupo->nome) ? (session()->get( 'individuals' )[ $key ]->usuariocadastro->grupo->nome) : '' }}
                        <br><strong>Redirecionado para Grupo:             </strong> {{ isset(session()->get( 'individuals' )[ $key ]->grupousuariored->nome) ? session()->get( 'individuals' )[ $key ]->grupousuariored->nome : '' }}
                        <br><strong>Redirecionado para Usuário:           </strong> {{ isset(session()->get( 'individuals' )[ $key ]->usuariored->name) ? session()->get( 'individuals' )[ $key ]->usuariored->name : '' }} {{ isset(session()->get( 'individuals' )[ $key ]->usuariored->grupo->nome) ? (session()->get( 'individuals' )[ $key ]->usuariored->grupo->nome) : '' }}
                        <br><strong>Data/Hora Solução redirecionamento:   </strong> @if ( isset(session()->get( 'individuals' )[ $key ]->data_hora_red_finalizado) ) <strong class="text-success">{{ datahoraBR( session()->get( 'individuals' )[ $key ]->data_hora_red_finalizado ) }}</strong> @elseif ( !session()->get( 'individuals' )[ $key ]->data_hora_red_finalizado && !session()->get( 'individuals' )[ $key ]->user_id_atendeu_red && ( session()->get( 'individuals' )[ $key ]->gruposuser_id_red || session()->get( 'individuals' )[ $key ]->user_id_red ) ) <strong class="text-danger"> - Pendente -</strong> @endif
                        <br><strong>Usuário que atendeu redirecionamento: </strong> @if ( isset(session()->get( 'individuals' )[ $key ]->usuarioatendeu->name) ) <strong class="text-success">{{ session()->get( 'individuals' )[ $key ]->usuarioatendeu->name }} ({{ session()->get( 'individuals' )[ $key ]->usuarioatendeu->grupo->nome }})</strong> @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</section>
@include('layout_admin._includes.footer_print_callcenter')

