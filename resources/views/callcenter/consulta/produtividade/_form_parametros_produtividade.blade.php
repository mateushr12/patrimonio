<div class="row">
    {{--
        <div class="form-group col-sm-3">
            <label for="FonteAcao">Período:</label>
            <select class="form-control" id="id_periodo" name="id_periodo">
                <option value="1">Hoje</option>
                <option value="2">3 Dias</option>
                <option value="3">5 Dias</option>
                <option value="4">1 Semana</option>
                <option value="5">Mês Atual</option>
                <option value="6">3 Ultimos Meses</option>
                <option value="7">Semestre Atual</option>
                <option value="8">Ano Atual</option>
                <option value="9">Período Específico</option>
            </select>
            </select>
        </div>
        <div class="row">
            <label class="col-md-3">Período Específico</label>
        </div>
    --}}

    <div class="form-group col-md-3">
        <label for="data_inicial">Data Inicial</label>
        <input type="date" class="form-control" id="data_inicial" name="data_inicial" value={{ now() }} 
               title="Data inicial do período desejado" data-toggle="tooltip" data-placement="top" autofocus>
    </div>
    <div class="form-group col-md-3">
        <label for="data_final">Data Final</label>
        <input type="date" class="form-control" id="data_final" name="data_final" value={{ now() }} 
               title="Data Final do período desejado" data-toggle="tooltip" data-placement="top">
    </div>
{{--
    <div class="form-group col-sm-3">
        <label for="FonteAcao">Grupo / Nivel Atendimento:</label>
        <select class="form-control" id="gruposuser_id" name="gruposuser_id" 
               title="Grupo que atendeu as chamadas" data-toggle="tooltip" data-placement="top">
            <option value="0">Todos</option>
            @foreach($grupoatendimento as $tipo)
                <option value="{{ $tipo->gruposuser_id }}">{{ $tipo->gruposuser_id }} - {{$tipo->gruposuser_id}}</option>
            @endforeach
        </select>
    </div>
--}}
    <div class="form-group col-sm-3">
        <label for="FonteAcao">Tipo de Cidadão:</label>
        <select class="form-control" id="tipocidadao_id" name="tipocidadao_id" 
               title="Tipo de Cidadão" data-toggle="tooltip" data-placement="top">
            <option value="0">Todos</option>
            @foreach($tipocidadao as $tipo)
                <option value="{{ $tipo->tipocidadao_id }}">{{ $tipo->tipocidadao_id }} - {{$tipo->tipocidadao->descricao}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-3">
        <label for="FonteAcao">Tipo de Resultado:</label>
        <select class="form-control" id="id_tiporesultado" name="id_tiporesultado" 
               title="Tipo de Resultado desejado" data-toggle="tooltip" data-placement="top">
            {{-- <option value="">Selecione o Tipo de Resultado</option> --}}
            <option value="1">Por Ano / Mês</option>
            <option value="2">Por Data / Hora</option>
            <option value="3">Por Dia</option>
            <option value="4">Por Hora</option>
            <option value="5">Por Data / Atendente</option>
            <option value="6">Por Atendente</option>
            <option value="7">Por Tipo de Problema</option>
            <option value="8">Problemas com ocorrências > 2</option>
            <option value="9">Por Tipo de Solução</option>
            <option value="10">Soluções com ocorrências > 2</option>
            <option value="11">Clientes com chamadas > 2</option>
            <option value="12">Atendimentos por protocolo agiliza</option>
            <option value="13">Por Tipo de Cidadão</option>
            {{--
            <option value="14">C.P.F.'s distintos por Tipo de Cidadão</option>
            --}}
        </select>
    </div>
    <div class="form-group col-sm-3">
        <label for="max_registros">Máx. Registros</label>
        <input type="number" class="form-control" id="max_registros" name="max_registros" value="15" 
               title="Número máximo de registros a ser retornado na consulta" data-toggle="tooltip" data-placement="top">
    </div>
</div>

<div class="row">
    <input type="hidden" name="user_id" value="{{auth()->user()->id}}" />
    {{--
    <div class="form-group col-md-12">
        <label for="id">Usuário Logado</label>
        <input type="text" class="form-control" id="id" name="id" placeholder="Usuário Logado"
               value="{{ auth()->user()->id }} - {{ auth()->user()->name }}" autocomplete="off">
    </div>
    --}}
</div>
