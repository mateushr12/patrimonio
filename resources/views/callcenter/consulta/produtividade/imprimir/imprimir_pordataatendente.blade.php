@include('layout_admin._includes.topo_print_callcenter')
    <table class="table table-bordered table-hover" style="font-size:100%; margin: auto;">
        <caption></caption>
        <thead>
            <tr class="bg-light">
                <th>Data</th>
                <th>Usuário</th>
                <th>Qtde Atendimentos</th>
                <th>Tempo Total</th>
                <th>Tempo Médio Atendimento</th>
            </tr>
        </thead>

        <tbody>
            @if(session()->has('individuals'))
                @foreach( session()->get( 'individuals' ) as $key => $individual )
                    <tr>
                        <td style="text-align: center;">{{ dataDiaSemana( session()->get( 'individuals' )[ $key ]->data ) }}</td>
                        <td style="text-align: center;">{{ session()->get( 'individuals' )[ $key ]->usuariocadastro->name }}</td>
                        <td style="text-align: center;">{{ number_format( session()->get( 'individuals' )[ $key ]->contador, 0, ',', '.' ) }}</td>
                        <td style="text-align: center;">{{ horaAnormal( session()->get( 'individuals' )[ $key ]->soma ) }}</td>
                        <td style="text-align: center;">{{ gmdate( "H:i:s", ( session()->get( 'individuals' )[ $key ]->soma / session()->get( 'individuals' )[ $key ]->contador ) ) }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>

        <tfoot>
            <tr>
                <td colspan="2" style="text-align: center;"><strong>TOTAIS</strong></td>
                <td style="text-align: center;"><strong>{{ number_format( session()->get( 'individuals' )[0]->totalcontador, 0, ',', '.' ) }}</strong></td>
                <td style="text-align: center;"><strong>{{ horaAnormal( session()->get( 'individuals' )[0]->totalsoma ) }}</strong></td>
                <td style="text-align: center;"><strong>{{ horaAnormal( session()->get( 'individuals' )[0]->totaltempomedioatendimento ) }}</strong></td>
            </tr>
        </tfoot>
    </table>
</section>
@include('layout_admin._includes.footer_print_callcenter')