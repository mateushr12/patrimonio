@include('layout_admin._includes.topo_print_callcenter')
    <table class="table table-bordered table-hover" style="font-size:100%; margin: auto;">
        <caption></caption>
        <thead>
            <tr class="bg-light">
                <th>Data</th>
                <th>Qtde Atendimentos</th>
                <th>% Atendimentos</th>
                <th>Tempo Total</th>
                <th>Tempo Médio Atendimento</th>
            </tr>
        </thead>

        <tbody>
            @if(session()->has('individuals'))
                @foreach( session()->get( 'individuals' ) as $keyItem => $individual )
                    <tr>
                        <td style="text-align: center;">{{ isset( session()->get( 'individuals' )[$keyItem]->dataprocessada ) ? dataDiaSemana( session()->get( 'individuals' )[$keyItem]->dataprocessada ) : '' }}</td>
                        <td style="text-align: center;">{{ isset( session()->get( 'individuals' )[$keyItem]->contador ) ? number_format( session()->get( 'individuals' )[$keyItem]->contador, 0, ',', '.' ) : '' }}</td>
                        <td style="text-align: center;">{{ number_format( session()->get( 'individuals' )[$keyItem]->percentualatendimento, 2, ',', '.' ) }}</td>
                        <td style="text-align: center;">{{ isset( session()->get( 'individuals' )[$keyItem]->soma ) ? horaAnormal( session()->get( 'individuals' )[$keyItem]->soma ) : '' }}</td>
                        <td style="text-align: center;">{{ isset( session()->get( 'individuals' )[$keyItem]->contador ) ? gmdate( "H:i:s", ( session()->get( 'individuals' )[$keyItem]->soma / session()->get( 'individuals' )[$keyItem]->contador ) ) : '' }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>

        <tfoot>
            <tr>
                <td colspan="1" style="text-align: center;"><strong>TOTAIS</strong></td>
                <td style="text-align: center;"><strong>{{ number_format( session()->get( 'individuals' )[0]->totalcontador, 0, ',', '.' ) }}</strong></td>
                <td></td>
                <td style="text-align: center;"><strong>{{ horaAnormal( session()->get( 'individuals' )[0]->totalsoma ) }}</strong></td>
                <td style="text-align: center;"><strong>{{ horaAnormal( session()->get( 'individuals' )[0]->totaltempomedioatendimento ) }}</strong></td>
            </tr>        

        </tfoot>
    </table>
</section>
@include('layout_admin._includes.footer_print_callcenter')