@include('layout_admin._includes.topo_print_callcenter')
    <table class="table table-bordered table-hover" style="font-size:100%; margin: auto;">
        <caption></caption>
        <thead>
            <tr class="bg-light">
                <th>Hora</th>
                <th>Qtde Atendimentos</th>
                <th>Tempo Total</th>
                <th>Tempo Médio Atendimento</th>
            </tr>
        </thead>

        <tbody>
            @if(session()->has('individuals'))
                @foreach( session()->get( 'individuals' ) as $key => $individual )
                    <tr>
                        <td style="text-align: center;">{{ session()->get( 'individuals' )[ $key ]->hora }}h às {{ session()->get( 'individuals' )[ $key ]->hora+1 }}h</td>
                        <td style="text-align: center;">{{ number_format( session()->get( 'individuals' )[ $key ]->contador, 0, ',', '.' ) }}</td>
                        <td style="text-align: center;">{{ horaAnormal( session()->get( 'individuals' )[ $key ]->soma ) }}</td>
                        <td style="text-align: center;">{{ gmdate( "H:i:s", session()->get( 'individuals' )[ $key ]->soma / session()->get( 'individuals' )[ $key ]->contador ) }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>

        <tfoot>
        <tr>
                <td colspan="1" style="text-align: center;"><strong>TOTAIS</strong></td>
                <td style="text-align: center;"><strong>{{ number_format( session()->get( 'individuals' )[0]->totalcontador, 0, ',', '.' ) }}</strong></td>
                <td style="text-align: center;"><strong>{{ horaAnormal( session()->get( 'individuals' )[0]->totalsoma ) }}</strong></td>
                <td style="text-align: center;"><strong>{{ horaAnormal( session()->get( 'individuals' )[0]->totaltempomedioatendimento ) }}</strong></td>
            </tr>
        </tfoot>
    </table>
</section>

<script>
    $(function() {
        var chart;
        var categories = <?php echo json_encode( session()->get( 'grafico_A_Rotulos_Series_Eixo_X' ) ) ?>;

        $(document).ready(function() {
            chart = new Highcharts.Chart({
                exporting: { enabled: false },
                credits:{ enabled: false },
                chart: { renderTo: {{ session()->get( 'grafico_A_Nome_Grafico' ) }}, type: 'column', options3d: { enabled: true, alpha: 10, beta: 5, depth: 60 } },
                title: { text: '<b>{{ session()->get( 'grafico_A_Titulo' ) }}</b>' },
                xAxis: { categories: categories, labels: { enabled: true } },
                yAxis: { min: 0, title: { text: '{{ session()->get( 'grafico_A_Titulo_Eixo_Y' ) }}' } },
                plotOptions: {
                    series: {
                        dataLabels: {
                            formatter: function() {
                            return '' + new Intl.NumberFormat('de-DE').format(this.y); },
                            enabled: true,
                            // rotation: -90,
                            align: 'right', color: 'black', shadow: false,
                            x: -10,
                            style: { "fontSize": "12px", "textShadow": "0px" }
                        },
                    pointPadding: 0.1, groupPadding: 0 } },
                tooltip: { formatter: function() { return this.x + ': ' + new Intl.NumberFormat('de-DE').format(this.y); } },
                series: [
                            {
                                type: '{{ session()->get( 'grafico_A_Serie_1_Tipo_Grafico' ) }}',
                                name: '{{ session()->get( 'grafico_A_Serie_1_Legenda' ) }}',
                                data: <?php echo json_encode( session()->get( 'grafico_A_Serie_1_Dados' ), JSON_NUMERIC_CHECK ) ?>
                            }
                ]
            });
        });
    });
    // final grafico 1
    ///////////////////////////////////////////////////////////////////////////////
    $(function() {
        var chart;
        var categories = <?php echo json_encode( session()->get( 'individuals' ) ) ?>;
        $(document).ready(function() {
            chart = new Highcharts.Chart({
                exporting: {
                    enabled: false
                },
                credits:{
                    enabled: false
                },
                chart: {
                    renderTo: 'grafico2',
                    type: 'column'
                },
                colors: ['#78ff41'],
                title: {
                    text: '<b>Gráfico 2</b>'
                },
                xAxis: {
                    categories: categories,
                    labels: {
                    enabled: true
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                    text: 'Total Pago (R$)'
                    }
                },
                plotOptions: {
                    series: {
                    dataLabels: {
                        formatter: function() {
                        return 'R$ ' + new Intl.NumberFormat('de-DE').format(this.y);
                        },
                        enabled: true,
                        align: 'right',
                        color: 'black',
                        shadow: false,
                        x: -10,
                        style: {
                        "fontSize": "12px",
                        "textShadow": "0px"
                        }
                    },
                    pointPadding: 0.1,
                    groupPadding: 0
                    }
                },

                tooltip: {
                    formatter: function() {
                    return this.x + ': R$ ' + new Intl.NumberFormat('de-DE').format(this.y);
                    }
                },
                series: [{
                    type: 'column',
                    name: 'Total de empenhos pagos',
                    data: <?php echo json_encode( session()->get( 'individuals' ), JSON_NUMERIC_CHECK ) ?>,
                    stacking: 'normal'
                },
                {
                    type: 'column',
                    name: 'Total de empenhos pagos',
                    data: <?php echo json_encode( session()->get( 'individuals' ), JSON_NUMERIC_CHECK ) ?>,
                    stacking: 'normal'
                }]
            });
        });
    });

    ////////////////////////////////////////////////
    function dataAt(){
        var data = new Date(),
            dia  = data.getDate().toString().padStart(2, '0'),
            mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
            ano  = data.getFullYear();
        return dia+"/"+mes+"/"+ano;
    }

    document.getElementById("export-pdf").onclick = function() {
        var svg = document.getElementById("grafico1").querySelector("svg");
        var pdf = new jsPDF('l', 'pt', 'a4');
        var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"

        svg2pdf(svg, pdf, {
            xOffset: 40,
            yOffset: 180,
            scale: 0.69
        });

        pdf.setFontSize(18)
        pdf.setTextColor(40)
        pdf.setFontType("bold")
        pdf.text('GRÁFICOS DE EMPENHOS', 320, 130)
        pdf.setFontSize(10)
        pdf.text('Data: ' + dataAt(), 398, 145)

        var img = new Image();
        img.src = base64Img;
        pdf.addImage(img, 'JPEG', 40, 15, 765, 86)
        //Rodape
        var str = 'Pág. ' + pdf.internal.getNumberOfPages()
        pdf.setFontSize(9)
        var pageSize = pdf.internal.pageSize
        var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
        pdf.text(str, 780, pageHeight - 35)
        pdf.setFontType("bold");
        pdf.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', 328, pageHeight - 40)
        pdf.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br',  340, pageHeight - 30)
        ///

        pdf.addPage("a4", "l");
        //pdf.addImage(base64Img, 'JPEG', 40, 15, 765, 86)
        var svg1 = document.getElementById("grafico2").querySelector("svg");
        svg2pdf(svg1, pdf, {
        xOffset: 40,
        yOffset: 150,
        scale: 0.69
        });
        pdf.addImage(img, 'JPEG', 40, 15, 765, 86)
        //Rodape
        var str = 'Pág. ' + pdf.internal.getNumberOfPages()
        pdf.setFontSize(9)
        var pageSize = pdf.internal.pageSize
        var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
        pdf.text(str, 780, pageHeight - 35)
        pdf.setFontType("bold");
        pdf.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', 328, pageHeight - 40)
        pdf.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br',  340, pageHeight - 30)
        ///

        pdf.save("graficosEmpenhos.pdf");
    };


    $(document).ready(function(){
        $('#imprimirPDF2').click(function() {
            var doc = new jsPDF('portrait', 'pt', 'a4')
            var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"
            var img = new Image()
            img.src = base64Img
            var hoje = new Date()
            doc.autoTable({
            html: '#tabela',
            styles: {
                lineColor: '#ccc',
                lineWidth: 1,
                fontSize: 9
            },
            headStyles: { fillColor: '#424647' },
            didDrawPage: function (data) {
                // Cabeçalho
                if (base64Img) {
                doc.addImage(img, 'JPEG', 40, 15, 515, 66)
                }
                doc.setFontSize(14)
                doc.setTextColor(40)
                doc.setFontType("bold")
                doc.text('RELATÓRIO DE AÇÕES', data.settings.margin.left + 0, 100)
                doc.setFontSize(9)
                doc.text('Data: ' + dataAt(), data.settings.margin.left + 445, 100)

                // Rodape
                var str = 'Pág. ' + doc.internal.getNumberOfPages()
                doc.setFontSize(9)
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left + 480, pageHeight - 25)
                doc.setFontType("bold");
                doc.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', data.settings.margin.left + 128, pageHeight - 30)
                doc.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br', data.settings.margin.left + 145, pageHeight - 20)
            },
            margin: {top: 108}
            });
            doc.save("RelatorioAcao-"+ dataAt() +".pdf");
        });
    });       
</script>

@include('layout_admin._includes.footer_print_callcenter')