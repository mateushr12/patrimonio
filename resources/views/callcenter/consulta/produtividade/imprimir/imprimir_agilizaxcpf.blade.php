@include('layout_admin._includes.topo_print_callcenter')
    <table class="table table-bordered" style="font-size:100%;">
        <caption></caption>
        <thead>
            <tr class="bg-light">
                <td style="text-align: center;">Protocolo Agiliza</td>
                <td style="text-align: center;">Qtde Atendimentos</td>
                <td style="text-align: center;">Tempo Total Atendimento</td>
                <td style="text-align: center;">Tempo Médio Atendimento</td>
            </tr>
        </thead>

        <tbody>
            @if(session()->has('individuals'))
                @foreach( session()->get( 'individuals' ) as $keyItem => $itemValue )
                    <tr>
                        <td style="vertical-align: middle; text-align: center;">{{ isset( session()->get( 'individuals' )[ $keyItem ]['id_agiliza'] ) ? session()->get( 'individuals' )[ $keyItem ]['id_agiliza'] : '' }}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ isset( session()->get( 'individuals' )[ $keyItem ]['contador'] ) ? number_format( session()->get( 'individuals' )[ $keyItem ]['contador'], 0, ',', '.' ) : '' }}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ isset( session()->get( 'individuals' )[ $keyItem ]['soma'] ) ? horaAnormal( session()->get( 'individuals' )[ $keyItem ]['soma'] ) : '' }}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ isset( session()->get( 'individuals' )[ $keyItem ]['mediatempoatendimento'] ) ? horaAnormal( session()->get( 'individuals' )[ $keyItem ]['mediatempoatendimento'] ) : '' }}</td>
                     </tr>
                @endforeach
            @endif
        </tbody>

        <tfoot>
            <tr>
                <td style="text-align: center;" colspan="1">TOTAIS</td>
                <td style="text-align: center;">{{ number_format( session()->get( 'individuals' )[0]['totalcontador'], 0, ',', '.' ) }}</td>
                <td style="text-align: center;">{{ isset( session()->get( 'individuals' )[0]->totalsoma ) ? horaAnormal( session()->get( 'individuals' )[0]->totalsoma ) : '' }}</td>
                <td style="text-align: center;">{{ isset( session()->get( 'individuals' )[0]->nTotalMediaTempoAtendimento ) ? horaAnormal( session()->get( 'individuals' )[0]->nTotalMediaTempoAtendimento ) : '' }}</td>
            </tr>
        </tfoot>
    </table>
</section>
@include('layout_admin._includes.footer_print_callcenter')
