@include('layout_admin._includes.topo_print_callcenter')
    <table class="table table-bordered table-hover" style="font-size:100%; margin: auto;">
        <caption></caption>
        <thead>
            <tr class="bg-light">
                <th>Tipo Cidadão</th>
                <th>Qtde C.P.F.'s Distintos</th>
                <th>% Relativo</th>
            </tr>
        </thead>

        <tbody>
            @if(session()->has('individuals'))
                @foreach( session()->get( 'individuals' ) as $key => $individual )
                    <tr>
                        <td style="text-align: center;">{{ session()->get( 'individuals' )[ $key ]->descricao_tipocidadao }}</td>
                        <td style="text-align: center;">{{ number_format( session()->get( 'individuals' )[ $key ]->contador, 0, ',', '.' ) }}</td>
                        <td style="text-align: center;">{{ number_format( ( (session()->get( 'individuals' )[ $key ]->contador / session()->get( 'individuals' )[ $key ]->totalcontador) * 100), 2, ',', '' ) }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>

        <tfoot>
            <tr>
                <td colspan="1" style="text-align: center;"><strong>TOTAIS</strong></td>
                <td style="text-align: center;"><strong>{{ number_format( session()->get( 'individuals' )[0]->totalcontador, 0, ',', '.' ) }}</strong></td>
                <td></td>
            </tr>
        </tfoot>
    </table>
</section>
@include('layout_admin._includes.footer_print_callcenter')