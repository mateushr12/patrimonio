@include('layout_admin._includes.topo_print_callcenter')
    <table class="table table-bordered table-hover" style="font-size:80%; margin: auto;">
        <caption></caption>
        <thead>
            <tr class="bg-light">
                <th>Ano</th>
                <th>Mes</th>
                <th>Qtde Dias Úteis</th>
                <th>Qtde Atendimentos</th>
                <th>Média Atendimentos / Dia</th>
                <th>Tempo Total</th>
                <th>Tempo Médio Atendimento</th>
            </tr>
        </thead>

        <tbody>
            @if(session()->has('individuals'))
                @foreach( session()->get( 'individuals' ) as $key => $individual )
                    <tr>
                        <td style="text-align: center;">{{ session()->get( 'individuals' )[ $key ]->ano }}</td>
                        <td style="text-align: center;">{{ meses( session()->get( 'individuals' )[ $key ]->mes -1 ) }}</td>
                        <td style="text-align: center;">{{ session()->get( 'individuals' )[ $key ]->diasuteis }}</td>
                        <td style="text-align: center;">{{ number_format( session()->get( 'individuals' )[ $key ]->contador, 0, ',', '.' ) }}</td>
                        <td style="text-align: center;">{{ number_format( ( session()->get( 'individuals' )[ $key ]->contador / session()->get( 'individuals' )[ $key ]->diasuteis ), 2, ',', '' ) }}</td>
                        <td style="text-align: center;">{{ horaAnormal( session()->get( 'individuals' )[ $key ]->soma ) }}</td>
                        <td style="text-align: center;">{{ gmdate( "H:i:s", ( session()->get( 'individuals' )[ $key ]->soma / session()->get( 'individuals' )[ $key ]->contador ) ) }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>

        <tfoot>
            <tr>
                <td colspan="2" style="text-align: center;"><strong>TOTAIS</strong></td>
                <td style="text-align: center;"><strong>{{ session()->get( 'individuals' )[0]->totaldiasuteis }}</strong></td>
                <td style="text-align: center;"><strong>{{ number_format( session()->get( 'individuals' )[0]->totalcontador, 0, ',', '.' ) }}</strong></td>
                <td style="text-align: center;"><strong>{{ number_format( session()->get( 'individuals' )[0]->totalcontador / session()->get( 'individuals' )[0]->totaldiasuteis, 2, ',', '' ) }}</strong></td>
                <td style="text-align: center;"><strong>{{ horaAnormal( session()->get( 'individuals' )[0]->totalsoma ) }}</strong></td>
                <td style="text-align: center;"><strong>{{ horaAnormal( session()->get( 'individuals' )[0]->totalsoma / session()->get( 'individuals' )[0]->totalcontador ) }}</strong></td>
            </tr>
        </tfoot>
    </table>

</section>

<script>
    $(function() {
        var chart;
        var categories = <?php echo json_encode( session()->get( 'textoEixoY' ) ) ?>;

        $(document).ready(function() {
            chart = new Highcharts.Chart({
                exporting: { enabled: false },
                credits:{ enabled: false },
                chart: { renderTo: 'grafico1Print', type: 'column', options3d: { enabled: true, alpha: 10, beta: 5, depth: 60 } },
                title: { text: '<b>{{ session()->get( 'tituloGrafico' ) }}</b>' },
                xAxis: { categories: categories, labels: { enabled: true } },
                yAxis: { min: 0, title: { text: '{{ session()->get( 'eixoX' ) }}' } },
                plotOptions: {
                    series: {
                    dataLabels: {
                        formatter: function() {
                        return '' + new Intl.NumberFormat('de-DE').format(this.y); },
                        enabled: true,
                        // rotation: -90,
                        align: 'right', color: 'black', shadow: false,
                        x: -10,
                        style: { "fontSize": "12px", "textShadow": "0px" }
                    },
                    pointPadding: 0.1, groupPadding: 0 } },
                tooltip: { formatter: function() { return this.x + ': ' + new Intl.NumberFormat('de-DE').format(this.y); } },
                series: [{
                    type: 'column',
                    name: '{{ session()->get( 'tituloSeries' ) }}',
                    data: <?php echo json_encode( session()->get( 'valoresEixoYCategoria1' ), JSON_NUMERIC_CHECK ) ?>,
                    stacking: 'normal'
                }]
            });
        });
    });
       
</script>

@include('layout_admin._includes.footer_print_callcenter')