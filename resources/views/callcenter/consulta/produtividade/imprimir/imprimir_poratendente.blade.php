@include('layout_admin._includes.topo_print_callcenter')
    <table class="table table-bordered table-hover" style="font-size:90%; margin: auto;">
        <caption></caption>
        <thead>
            <tr class="bg-light">
                <th>Usuário</th>
                <th>Qtde Atendimentos</th>
                <th>% Atendimentos</th>
                <th>Tempo Total</th>
                <th>Tempo Médio Atendimento</th>
            </tr>
        </thead>
        <tbody>
            @if(session()->has('individuals'))
                @foreach( session()->get( 'individuals' ) as $individual )
                    <tr>
                        <td style="text-align: center;">{{ isset( $individual['nomeusuario'] ) ? $individual['nomeusuario'] : '' }}</td>
                        <td style="text-align: center;">{{ isset( $individual['contador'] ) ? number_format( $individual['contador'], 0, ',', '.' ) : '' }}</td>
                        <td style="text-align: center;">{{ number_format( $individual['percentualatendimento'], 2, ',', '' ) }}</td>
                        <td style="text-align: center;">{{ horaAnormal( $individual['soma'] ) }}</td>
                        <td style="text-align: center;">{{ gmdate( "H:i:s", $individual['soma'] / $individual['contador'] ) }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>

        <tfoot>
            <tr>
                <td colspan="1" style="text-align: center;"><strong>TOTAIS</strong></td>
                <td style="text-align: center;"><strong>{{ number_format( session()->get( 'individuals' )[0]->totalcontador, 0, ',', '.' ) }}</strong></td>
                <td></td>
                <td style="text-align: center;"><strong>{{ horaAnormal( session()->get( 'individuals' )[0]->totalsoma ) }}</strong></td>
                <td style="text-align: center;"><strong>{{ horaAnormal( session()->get( 'individuals' )[0]->totaltempomedioatendimento ) }}</strong></td>
            </tr>
        </tfoot>
    </table>
</section>
@include('layout_admin._includes.footer_print_callcenter')