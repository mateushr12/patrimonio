@include('layout_admin._includes.topo_print_callcenter')
    <table class="table table-bordered table-hover" style="font-size:70%; margin: auto;">
        <caption></caption>
        <thead>
            <tr class="bg-light">
                <th>C.P.F.</th>
                <th>Tipo Cidadão</th>
                <th>Nome</th>
                <th>Contato</th>
                <th>Qtde Atendimentos</th>
                <th>Tempo Total</th>
                <th>Tempo Médio Atendimento</th>
            </tr>
        </thead>

        <tbody>
            @if(session()->has('individuals'))
                @foreach( session()->get( 'individuals' ) as $key => $individual )
                    <tr>
                        <td style="text-align: center;">{{ mask( session()->get( 'individuals' )[ $key ]->cpf, '###.###.###-##' ) }}</td>

                        <td style="text-align: center;">{{ session()->get( 'individuals' )[ $key ]->descricao }}</td>
                        <td style="text-align: center;">{{ session()->get( 'individuals' )[ $key ]->nome }}</td>
                        <td style="text-align: left;">Fone Fixo: {{ isset($individual->fone_fixo) ? mask( $individual->fone_fixo, '(##) ####-####' ) : '' }} <br>
                                                        Fone Celular: {{ isset($individual->fone_celular) ? mask( $individual->fone_celular, '(##) #####-####' ) : '' }} <br>
                                                        Fone Celular 2: {{ isset($individual->fone_celular2) ? mask( $individual->fone_celular2, '(##) #####-####' ) : '' }} <br>
                                                        E-mail: {{ session()->get( 'individuals' )[ $key ]->email }}</td>
                        
                        <td style="text-align: center;">{{ number_format( session()->get( 'individuals' )[ $key ]->contador, 0, ',', '.' ) }}</td>
                        <td style="text-align: center;">{{ horaAnormal( session()->get( 'individuals' )[ $key ]->soma ) }}</td>
                        <td style="text-align: center;">{{ gmdate( "H:i:s", session()->get( 'individuals' )[ $key ]->soma / number_format( session()->get( 'individuals' )[ $key ]->contador, 0, ',', '.' ) ) }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>

        <tfoot>
            <tr>
                <td colspan="4" style="text-align: center;"><strong>TOTAIS</strong></td>
                <td style="text-align: center;"><strong>{{ number_format( session()->get( 'individuals' )[0]->totalcontador, 0, ',', '.' ) }}</strong></td>
                <td style="text-align: center;"><strong>{{ horaAnormal( session()->get( 'individuals' )[0]->totalsoma ) }}</strong></td>
                <td style="text-align: center;"><strong>{{ horaAnormal( session()->get( 'individuals' )[0]->totaltempomedioatendimento ) }}</strong></td>
            </tr>

        </tfoot>
    </table>
</section>
@include('layout_admin._includes.footer_print_callcenter')