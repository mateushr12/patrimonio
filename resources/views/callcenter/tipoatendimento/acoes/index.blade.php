@extends('layout_admin.principal')
@section ('breadcrumb','Callcenter / Tipo de Atendimento')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-6"><strong>Lista de Tipos de Atendimento ( {{ number_format( $totalRegistros, 0, ',', '.' ) }} registros existentes )</strong></div>
                                    
                                    {{--
                                    <div class="col-4">
                                        <form class="form-inline" action="{{ route( 'tipoatendimento.index' ) }}" method="post">
                                            {{ csrf_field() }}
                                            <div class="form-group"><input type="hidden" name="_method" value="get"></div>

                                            <div class="form-group">
                                                <input type="text" class="form-control" id="descricao" name="descricao" value="{{ isset($filtro) ? $filtro : '' }}" autocomplete="off" maxlength="255" onkeyup="" 
                                                        title="Informe todo / parte do texto que deseja procurar ( % no meio do texto = qualquer texto ). Será pesquisado em descrição" data-toggle="tooltip" data-placement="top">
                                                <button type="submit" class="btn btn-primary">Buscar</button>
                                            </div>
                                        </form>
                                    </div>
                                    --}}

                                    <div class="col-6"><button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#FormModal"><i class="zmdi zmdi-plus"></i>Novo Tipo de Atendimento</button></div>
                                </div>
                                
                                @if ( count( session()->get( 'individuals' ) ) > 0 )
                                    <div class="row">
                                        <div class="col-2"><a target="_blank" href="{{ route( 'tipoatendimento.imprimir', compact( 'titulo' ) ) }}"><button type="button" class="item" title="Imprimir" data-toggle="tooltip" data-placement="top"><i class="zmdi zmdi-print"></i> Imprimir</button></a></div>
                                    </div>
                                @endif
                            </div>

                            <div class="card-body card-block">

                                @if ($errors->any()) <div class="alert alert-danger"> <ul> @foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach </ul> </div> @endif
                                @if (session('msg')) <div class="alert alert-danger alert-dismissible fade show" role="alert"> {{session('msg')}} <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> @endif

                                {{--
                                    <div class="table-data__tool">
                                        <div class="table-data__tool-left">
                                            <div class="rs-select2--light rs-select2--md">
                                                <select class="js-select2" name="property">
                                                    <option selected="selected">Todas Colunas</option>
                                                    <option value="">Option 1</option>
                                                    <option value="">Option 2</option>
                                                </select>
                                                <div class="dropDownSelect2"></div>
                                            </div>
                                            <div class="rs-select2--light rs-select2--md">
                                                <select class="js-select2" name="time">
                                                    <option selected="selected">Período</option>
                                                    <option value="">Hoje</option>
                                                    <option value="">3 Dias</option>
                                                    <option value="">1 Semana</option>
                                                    <option value="">Mês Atual</option>
                                                    <option value="">Semestre Atual</option>
                                                    <option value="">Ano Atual</option>
                                                </select>
                                                <div class="dropDownSelect2"></div>
                                            </div>
                                            <button class="au-btn-filter">
                                                <i class="zmdi zmdi-filter-list"></i>filtros
                                            </button>
                                        </div>
                                        <div class="table-data__tool-right">
                                            <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                                <select class="js-select2" name="type">
                                                    <option selected="selected">Exportar</option>
                                                    <option value="">PDF</option>
                                                    <option value="">Planilha</option>
                                                </select>
                                                <div class="dropDownSelect2"></div>
                                            </div>
                                        </div>
                                    </div>
                                --}}

                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="bg-light">
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">ID</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Descrição</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Criação/Modificação</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle;">Usuário (cadastro/alteração)</td>
                                            <td class="info small text-center font-weight-bold" style="vertical-align: middle; width: 80px;">Ação</td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach( session()->get( 'individuals' ) as $key => $individual )
                                            <tr>
                                                <td class="small text-center" style="vertical-align: middle;">{{ $individual->id }}</td>
                                                <td class="small text-left" style="vertical-align: middle;">{{ $individual->descricao }}</td>
                                                <td class="small text-left" style="vertical-align: middle;">
                                                    <strong>Criação: </strong>{{ isset( $individual->created_at ) ? datahoraBR( $individual->created_at ) : '' }} <br>
                                                    <strong>Modificação: </strong>{{ isset( $individual->updated_at ) ? datahoraBR( $individual->updated_at ) : '' }}
                                                </td>
                                                <td class="small text-center" style="vertical-align: middle;">
                                                    @if ( $individual->user_id && $individual->user ) {{ $individual->user->name }} ({{ $individual->user->grupo->nome }}) @else - @endif
                                                </td>

                                                <td class="text-center" style="vertical-align: middle;">
                                                    <div class="table-data-feature">
                                                        @if ( in_array('callcenter/tipoatendimento/acoes/editar/{id}', Session::get('permissoes.nomes')) )
                                                            <a href="{{ route('tipoatendimento.editar', $individual->id) }}"><button type="button" class="item" title="Editar" data-toggle="tooltip" data-placement="top"><i class="zmdi zmdi-edit"></i></button></a>
                                                            &nbsp;
                                                        @else @endif
                                                        @if ( (in_array('callcenter/tipoatendimento/acoes/deletar/{id}', Session::get('permissoes.nomes')) ) && ( $individual->excluivel == 'Sim' ) )
                                                            <a href="{{ route('tipoatendimento.deletar', $individual->id) }}" data-confirm="Tem certeza que deseja excluir o item selecionado?"><button type="button" class="item" title="Deletar"data-toggle="tooltip" data-placement="top"><i class="zmdi zmdi-delete"></i></button></a>
                                                        @else @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>

                                    {{--
                                    <tfoot>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                    --}}

                                </table>

                                <div class="row d-flex justify-content-center">
                                    {{--
                                    {{ $individuals->links() }}

                                    <form class="form-inline" action="{{ route( 'tipoatendimento.index' ) }}" method="post"><div class="form-group"><input type="hidden" name="_method" value="get" id="itensPorPagina"></div>          Exibir:     <button type="submit" class="btn btn-primary" name="itensPorPagina5" id="itensPorPagina5">5</button></form>
                                    <form class="form-inline" action="{{ route( 'tipoatendimento.index' ) }}" method="post"><div class="form-group"><input type="hidden" name="_method" value="get"></div>     <button type="submit" class="btn btn-primary" name="itensPorPagina10" id="itensPorPagina10">10</button></form>
                                    <form class="form-inline" action="{{ route( 'tipoatendimento.index' ) }}" method="post"><div class="form-group"><input type="hidden" name="_method" value="get"></div>     <button type="submit" class="btn btn-primary" name="itensPorPaginaTodos" id="itensPorPaginaTodos">100</button></form>
                                    --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- modal form NOVA AÇÃO-->
    <div class="modal fade" id="FormModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="FormModalLabel">Cadastro de Tipo de Atendimento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body card-block">
                            <form class="" action="{{route('tipoatendimento.salvar')}}" method="post">
                                {{ csrf_field() }}
                                @include('callcenter.tipoatendimento.acoes.novo')
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">
                        Salvar
                    </button>
                    <button type="reset" class="btn btn-warning" data-dismiss="modal">
                        Cancelar
                    </button>
                </div>
                <div style="text-align: right;">
                    <strong style="color: red">*</strong> informação obrigatória
                </div>
            </div>
        </div>
    </div>
    <!-- end modal form NOVA AÇÃO -->
@endsection
