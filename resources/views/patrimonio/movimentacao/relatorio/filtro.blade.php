@extends('layout_admin.principal')
@section ('breadcrumb','Patrimônio / Relatório Material')

@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-10">
                                    <strong>Relatório Movimentação</strong>
                                </div>
                            </div>
                        </div>
                        <div class="card-body card-block">
                            <form class="" action="{{route('movimentacao.relatorio.resultado')}}" method="post" target="_blank">
                                {{ csrf_field() }}
                                <div class="row">
                                  <div class="form-group col-sm-12">
                                      <label for="fornecedorcontratado">Material:</label>
                                      <select class="form-control" name="id_tipo_material" >
                                          <option value="">Selecione ...</option>
                                          @foreach($material as $key)
                                          <option value="{{$key->id }}">
                                              {{ $key->id }} - {{ $key->descricao }}
                                          </option>
                                          @endforeach
                                      </select>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="form-group col-sm-12">
                                    <label for="fornecedorcontratado">Data de Movimentação:</label>
                                    <div class="row">
                                      <div class="form-group col-sm-6">
                                          <input type="date" class="form-control" name="data_i" />
                                      </div>
                                      <div class="form-group col-sm-6">
                                          <input type="date" class="form-control" name="data_f" />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Buscar</button>
                                <button type="reset" class="btn btn-warning">Limpar</button>
                            </form>
                            <br><a href="{{ route('movimentacao') }}">Voltar<a/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>


<script>
    function k(i) {
                        var v = i.value.replace(/\D/g,'');
                        v = (v/100).toFixed(2) + '';
                        v = v.replace(".", ".");
                        v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                        v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                        i.value = v;
                    }
</script>


@endsection
