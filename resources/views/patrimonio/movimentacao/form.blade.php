<div class="tab-content">

        <div class="row">
            <div class="col-6 form-group">
                <label for="">Data<span id="ob">*</span></label>
                <input type="date" name="data" id="" class="form-control"
                value="{{isset($registro->data) ? $registro->data : ''}}" {{isset($registro->data) ? 'disabled' : ''}} required>
            </div>
            <div class="col-6 form-group">
                <label for="">Material<span id="ob">*</span></label>
                <select name="id_material" id="material" class="form-control" required
                  {{isset($registro->id_material) ? 'disabled' : ''}}>
                    <option value="">Selecione ...</option>
                    @foreach($material as $a)
                    <option {{isset($registro->id_material) && $registro->id_material == $a->id ? 'selected' : ''}}
                        value="{{ isset($registro->id_material) ? $registro->id_material : $a->id }}">
                        {{ $a->id }} - {{$a->descricao}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-6 form-group">
                <label for="">Setor de Origem</label>
                <input type="text" id="setorOrigemShow" class="form-control"
                value="{{isset($registro->id_setor_origem) ? $registro->setorO->descricao : ''}}" readonly >
                <input type="hidden" name="id_setor_origem" id="setorOrigem" value="{{isset($registro->id_setor_origem) ? $registro->id_setor_origem : ''}}" >
            </div>
            <div class="col-6 form-group">
                <label for="">Setor de Destino<span id="ob">*</span></label>
                <select name="id_setor_destino" id="" class="form-control" required
                  {{isset($registro->id_setor_destino) ? 'disabled' : ''}}>
                    <option value="">Selecione ...</option>
                    @foreach($setor as $a)
                    <option {{isset($registro->id_setor_destino) && $registro->id_setor_destino == $a->id ? 'selected' : ''}}
                        value="{{ isset($registro->id_setor_destino) ? $registro->id_setor_destino : $a->id }}">
                        {{$a->descricao}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-6 form-group">
              <label for="">Usuário de Origem</label>
              <input type="text" id="usuarioOrigemShow" class="form-control"
              value="{{isset($registro->id_usuario_origem) ? $registro->usuarioO->descricao : 'SEM USUÁRIO'}}" readonly >
              <input type="hidden" name="id_usuario_origem" id="usuarioOrigem" value="{{isset($registro->id_usuario_origem) ? $registro->id_usuario_origem : ''}}" >
            </div>
            <div class="col-6 form-group">
                <label for="">Usuário de Destino</label>
                <select name="id_usuario_destino" id="" class="form-control"
                  {{isset($registro->id) ? 'disabled' : ''}}>
                    <option value="">Sem Usuário</option>
                    @foreach($usuario as $a)
                    <option {{isset($registro->id_usuario_destino) && $registro->id_usuario_destino == $a->id ? 'selected' : ''}}
                        value="{{ isset($registro->id_usuario_destino) ? $registro->id_usuario_destino : $a->id }}">
                        {{$a->name}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-12 form-group">
                <label for="">Atendimento</label>
                <select name="atendimento_id" id="" class="form-control" >
                    <option value="">Selecione ...</option>
                    @foreach($atend as $a)
                    <option {{isset($registro->atendimento_id) && $registro->atendimento_id == $a->id ? 'selected' : ''}}
                        value="{{ isset($registro->atendimento_id) ? $registro->atendimento_id : $a->id }}">
                        {{$a->id}} - {{$a->titulo}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-12 form-group">
                <label for="">Obs</label>
                <textarea name="obs" class="form-control" id="" >{{isset($registro->obs) ? $registro->obs : ''}}</textarea>
            </div>
        </div>

</div>
