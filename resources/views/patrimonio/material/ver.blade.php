<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="JUCESE - Junta Comercial do Estado de Sergipe">

    <!-- Title Page-->
    <title>ERP - JUCESE</title>

    <link href="<?php echo asset('vendor/bootstrap-4.1/bootstrap.min.css')?>" rel="stylesheet" media="all">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@3.5.9/dist/jspdf.plugin.autotable.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

    <style>
      @media print {
        .btn {
          display: none !important;
        }
      }
      #btnPDF{
        background: #2579e6;
        color: white;
        font-weight: bold;
        width: 120px;
      }
    </style>

</head>

<body>


<div class="container">
        <div class="card-body card-block">
          <button type="button" class="btn" id="btnPDF">Gerar PDF</button><br>
          <img src="../../../images/gov-jucese-top-relatorio.jpg" hidden>
                <table style="width: 900px">
                  <thead >
                    <tr>
                      <td style=" font-size: 22px;" ><strong>Material</strong><p></p></td>
                      <td style="font-size: 14px; float: right;" >Data {{ date('d/m/Y') }}</td>
                    </tr>
                  </thead>
                <tbody>
                <tr><td colspan="2">
                <table class="table table-data2" id="tabela" border="1">
                    <tbody>
                        <tr>
                          <td ><strong >Descrição:</strong><br> {{ $material->descricao }}</td>
                          <td ><strong >Tipo de Material:</strong><br>{{ $material->tipoMaterial->descricao }}</td>
                          <td ><strong >Devolvido:</strong><br>{{ isset($material->devolvido) ? \Carbon\Carbon::parse($material->devolvido)->format('d/m/Y') : '-' }}</td>
                        </tr>
                        <tr>
                          <td ><strong >Patrimônio:</strong><br> {{ $material->patrimonio }}</td>
                          <td ><strong >Proprietário:</strong><br>{{ $material->proprietario->descricao }}</td>
                          <td ><strong >Estado de Conservação:</strong><br>{{ $material->conservacao->descricao }}</td>
                        </tr>
                        <tr>
                          <td ><strong >Nº Serie:</strong><br> {{ $material->n_serie }}</td>
                          <td ><strong >Usuário Atual:</strong><br>{{ isset($key->id_usuario) ? $key->usuario->name : 'SEM USUÁRIO' }}</td>
                          <td ><strong >Setor Atual:</strong><br>{{ $material->setor->descricao }}</td>
                        </tr>
                        <tr>
                          <td colspan="3"><strong >Obs:</strong><br>{{ $material->obs }}</td>
                        </tr>
                    </tbody>
                </table>
                </td></tr>
              </tbody>
                </table>
        </div>


</div>
</body>

<script>

function dataAt(){
    var data = new Date(),
        dia  = data.getDate().toString().padStart(2, '0'),
        mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
        ano  = data.getFullYear();
    return dia+"/"+mes+"/"+ano;
}

$(document).ready(function(){
  $('#btnPDF').click(function() {
    var numero = "{{ $material->id }}"
    var doc = new jsPDF('portrait', 'pt', 'a4')
    var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"
    var img = new Image()
    img.src = base64Img
    var hoje = new Date()
    doc.autoTable({
      html: '#tabela',
      styles: {
        lineColor: '#ccc',
        lineWidth: 1,
        fontSize: 10
      },
      headStyles: { fillColor: "#424647", fontSize: 11 },
      didParseCell: function (data, cell) {
        if (data.row.index == 7) {
          data.cell.styles.fontStyle = 'bold';
          data.cell.styles.halign = 'center';
          data.cell.styles.fillColor = '#424647';
          data.cell.styles.textColor = 'white';
        }else if (data.row.index == 8) {
          data.cell.styles.fontStyle = 'bold';
          data.cell.styles.halign = 'center';
          data.cell.styles.textColor = 'black';
        }
      },
      didDrawPage: function (data) {
      ///////////////////// Cabeçalho ///////////////////////////////////////////////
        if (base64Img) {
          doc.addImage(img, 'JPEG', 40, 15, 515, 66)
        }
        doc.setFontSize(14)
        doc.setTextColor(40)
        doc.setFontType("bold")
        doc.text('Material', data.settings.margin.left + 0, 100)
        doc.setFontSize(9)
        doc.text('Data: ' + dataAt(), data.settings.margin.left + 445, 100)


        ///////////////////////////// Rodape ////////////////////////////////////////
        var str = 'Pág. ' + doc.internal.getNumberOfPages()
        doc.setFontSize(9)
        var pageSize = doc.internal.pageSize
        var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
        doc.text(str, data.settings.margin.left + 480, pageHeight - 25)
        doc.setFontType("bold");
        doc.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', data.settings.margin.left + 128, pageHeight - 30)
        doc.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br', data.settings.margin.left + 145, pageHeight - 20)
      },
      margin: {top: 108}
    });
    doc.save("Material"+ numero +"_"+ dataAt() +".pdf");
  });
});

</script>

</html>
