@extends('layout_admin.principal')
@section ('breadcrumb','Patrimônio / Materiais')
@section('conteudo')
  <style>
  #ob{
    color: red;
  }
  </style>
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Etiquetas Materiais/Equipamentos</strong>
                                </div>
                                {{-- <div class="col-3 text-right">
                                    <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small"
                                            data-toggle="modal" data-target="#FormModal">
                                        <i class="zmdi zmdi-plus"></i>Adcionar
                                    </button>
                                </div> --}}
                            </div>
                        </div>

                        {{-- @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif --}}

                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="table-responsive table-responsive-data2">
                                        <table class="table" id="tabela">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Patrimônio</th>
                                                    <th>Descrição</th>
                                                    <th>Tipo de Material</th>
                                                    {{-- <th>Nº de Série</th>
                                                    <th>Setor Atual</th>
                                                    <th>Usuário Atual</th>
                                                    <th>Ações</th> --}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                              <form class="" action="{{ route('material.geraretiqueta') }}" method="post">
                                                  {{ csrf_field() }}
                                                @foreach ($material as $key)
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="material_id[]" value="{{$key->id}}" >
                                                    </td>
                                                    <td>{{$key->patrimonio }}</td>
                                                    <td>{{$key->descricao }}</td>
                                                    <td>{{$key->tipoMaterial->descricao }}</td>
                                                    {{-- <td>{{$key->n_serie }}</td>
                                                    <td>{{$key->setor->descricao }}</td>
                                                    <td>{{ isset($key->id_usuario) ? $key->usuario->name : 'Sem Usuário' }}</td> --}}
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <button type="submit" class="btn btn-primary" formtarget="_blank">Gerar Etiqueta</button>
                                      </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>

$(document).ready( function () {
  $('#tabel').DataTable({
    paging: false,

    language: {
      search: "Buscar:",
      zeroRecords: "Nenhum registro encontrado.",
      info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      infoEmpty: "Mostrando 0 até 0 de 0 registros",
      lengthMenu: "Mostrar _MENU_ entradas",
      infoFiltered: "(filtrado de _MAX_ registros)",
      paginate: {
        first: "Primeiro",
        previous: "Anterior",
        next: "Proximo",
        last: "Ultimo"
      },
      emptyTable: "Nenhum registro encontrado."
    }
  });
});


function k(i) {
  var v = i.value.replace(/\D/g,'');
  v = (v/100).toFixed(2) + '';
  v = v.replace(".", ".");
  v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
  v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
  i.value = v;
}

function data(j) {
  var d2 = j.value
  var d1 = document.getElementById('inicio')
  d1 = d1.value
  if (d1 > d2){
    alert('Inicio da Vigência tem que ser menor que o Fim da Vigência.')
    $("#inicio").val("")
    $("#fim").val("")
  }else if(d1 == ''){
    alert('Escolha uma data.')
    $("#fim").val("")
  }
}

function vl(){
  var vlr = document.getElementById('valorr')
  vlr = vlr.value
  if (vlr == 0.00){
    alert('Valor não pode ser zero.')
    $("#valorr").val("")
  }
}



</script>
@endsection
