<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="JUCESE - Junta Comercial do Estado de Sergipe">

    <!-- Title Page-->
    <title>ERP - JUCESE</title>

    <link href="<?php echo asset('vendor/bootstrap-4.1/bootstrap.min.css')?>" rel="stylesheet" media="all">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@3.5.9/dist/jspdf.plugin.autotable.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script> --}}

    <script src="<?php echo asset('js/qrcode.min.js')?>"></script>

    <style>
      @media print {
        .btn {
          display: none !important;
        }
      }
      #btnPDF{
        background: #2579e6;
        color: white;
        font-weight: bold;
        width: 300px;
      }
      #btnPDF:hover{
        background: #0439f1
      }

    </style>


</head>

<body>

  <div class="container">
    <div class="row" >
      <div class="col-12 form-group" style="margin-top: 10px">
        <div class="table-responsive table-responsive-data2">
          <button type="button" class="btn btn-block" id="btnPDF" onclick="window.print()">Imprimir</button><br>
          @foreach ($mt as $key)
            <table class="table table-bordered" style="width: 700px; font-size: 20px">
              <thead >
                <tr><td colspan="3">
                  <img src="<?php echo asset('images/gov-jucese-top-relatorio.png') ?>"  width="750px;"><br>
                </td></tr>
              </thead>
              <tbody>
                <tr>
                  <td ><strong>Nº Material:</strong> {{ $key->id }}</td>
                  <td ><strong>Patrimônio:</strong> {{ $key->patrimonio }}</td>
                  <td rowspan="2" style="width: 50px">
                    <div id="{{ $key->id }}"></div>
                  </td>
                  <script>
                    var id = "{{ $key->id }}";
                    var url = "{{ route('caixa.processo', ['id' => $key->id]) }}"
                    var qrcode = new QRCode(id, {
                      width: 120,
                      height: 120
                    });
                    qrcode.makeCode(url);
                  </script>
                </tr>
                <tr><td colspan="2"><strong>Descrição:</strong> {{ $key->descricao }}<br>{{ isset($key->devolvido) ? '*DEVOLVIDO '.$key->proprietario->descricao : '' }}</td></tr>
              </tbody>
            </table><br>
          @endforeach
        </div>
      </div>
    </div>
  </div>



</body>
</html>
