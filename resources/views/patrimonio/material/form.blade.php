<div class="tab-content">

        <div class="row">
            <div class="col-6 form-group">
                <label for="">Descrição<span id="ob">*</span></label>
                <input type="text" name="descricao" id="" class="form-control"
                value="{{isset($registro->descricao) ? $registro->descricao : ''}}" required>
            </div>
            <div class="col-6 form-group">
                <label for="">Tipo de Material<span id="ob">*</span></label>
                <select name="id_tipo_material" id="" class="form-control" required
                  {{isset($registro->id_tipo_material) ? 'disabled' : ''}}>
                    <option value="">Selecione ...</option>
                    @foreach($tpmaterial as $a)
                    <option {{isset($registro->id_tipo_material) && $registro->id_tipo_material == $a->id ? 'selected' : ''}}
                        value="{{ isset($registro->id_tipo_material) ? $registro->id_tipo_material : $a->id }}">
                        {{$a->descricao}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-4 form-group">
                  <label for="">Patrimônio<span id="ob">*</span></label>
                  <input type="text" name="patrimonio" id="" class="form-control" value="{{isset($registro->patrimonio) ? $registro->patrimonio : ''}}"
                  {{isset($registro->patrimonio) ? 'disabled' : ''}} required>
            </div>
            <div class="col-4 form-group">
                  <label for="">Proprietário<span id="ob">*</span></label>
                  <select class="form-control" name="id_proprietario" id="" required>
                      <option value="">Selecione ...</option>
                      @foreach($proprietario as $b)
                      <option {{isset($registro->id_proprietario) && $registro->id_proprietario == $b->id ? 'selected' : ''}}
                          value="{{ $b->id }}">
                          {{$b->descricao}}
                      </option>
                      @endforeach
                  </select>
            </div>
            <div class="col-4 form-group">
                  <label for="">Estado de Conservação<span id="ob">*</span></label>
                  <select class="form-control" name="id_conservacao" id="" required>
                      <option value="">Selecione ...</option>
                      @foreach($conservacao as $c)
                      <option {{isset($registro->id_conservacao) && $registro->id_conservacao == $c->id ? 'selected' : ''}}
                          value="{{ $c->id }}">
                          {{$c->descricao}}
                      </option>
                      @endforeach
                  </select>
            </div>
        </div>
        <div class="row">
            <div class="col-4 form-group">
                  <label for="">Nº Serie</label>
                  <input type="text" name="n_serie" id="" class="form-control" value="{{isset($registro->n_serie) ? $registro->n_serie : ''}}"
                  {{isset($registro->n_serie) ? 'disabled' : ''}} >
            </div>
            <div class="col-4 form-group">
                  <label for="">Usuário Atual</label>
                  <select class="form-control" name="id_usuario" id="" {{isset($registro->id) ? 'disabled' : ''}} >
                      <option value="">Sem Usuário</option>
                      @foreach($usuario as $e)
                      <option {{isset($registro->id_usuario) && $registro->id_usuario == $e->id ? 'selected' : ''}}
                          value="{{ $e->id }}">
                          {{$e->name}}
                      </option>
                      @endforeach
                  </select>
            </div>
            <div class="col-4 form-group">
                  <label for="">Setor Atual<span id="ob">*</span></label>
                  <select class="form-control" name="id_setor_atual" id="" {{isset($registro->id_setor_atual) ? 'disabled' : ''}} required>
                      <option value="">Selecione ...</option>
                      @foreach($setor as $d)
                      <option {{isset($registro->id_setor_atual) && $registro->id_setor_atual == $d->id ? 'selected' : ''}}
                          value="{{ isset($registro->id_setor_atual) ? $registro->id_setor_atual : $d->id }}">
                          {{$d->descricao}}
                      </option>
                      @endforeach
                  </select>
            </div>
        </div>
        <div class="row">
            <div class="col-12 form-group">
                <label for="">Obs</label>
                <textarea name="obs" class="form-control" id="" >{{isset($registro->obs) ? $registro->obs : ''}}</textarea>
            </div>
        </div>

</div>
