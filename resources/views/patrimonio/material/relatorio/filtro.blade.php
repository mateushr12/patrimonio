@extends('layout_admin.principal')
@section ('breadcrumb','Patrimônio / Relatório Material')

@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-10">
                                    <strong>Relatório Material</strong>
                                </div>
                            </div>
                        </div>
                        <div class="card-body card-block">
                            <form class="" action="{{route('material.relatorio.resultado')}}" method="post" target="_blank">
                                {{ csrf_field() }}
                                <div class="row">
                                  <div class="form-group col-sm-4">
                                      <label for="fornecedorcontratado">Tipo de Material:</label>
                                      <select class="form-control" name="id_tipo_material" >
                                          <option value="">Selecione ...</option>
                                          @foreach($tpmaterial as $key)
                                          <option value="{{$key->id }}">
                                              {{ $key->descricao }}
                                          </option>
                                          @endforeach
                                      </select>
                                  </div>
                                  <div class="form-group col-sm-4">
                                      <label for="fornecedorcontratado">Proprietário:</label>
                                      <select class="form-control" name="id_proprietario" >
                                          <option value="">Selecione ...</option>
                                          @foreach($proprietario as $key)
                                          <option value="{{$key->id }}">
                                              {{ $key->descricao }}
                                          </option>
                                          @endforeach
                                      </select>
                                  </div>
                                  <div class="form-group col-sm-4">
                                      <label for="fornecedorcontratado">Devolvidos:</label>
                                      <select class="form-control" name="devolvido" >
                                          <option value="">Selecione ...</option>
                                          <option value="1">Não</option>
                                          <option value="2">Sim</option>
                                      </select>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="form-group col-sm-6">
                                      <label for="fornecedorcontratado">Usuário:</label>
                                      <select class="form-control" name="id_usuario" >
                                          <option value="">Selecione ...</option>
                                          @foreach($usuario as $key)
                                          <option value="{{$key->id }}">
                                              {{ $key->name }}
                                          </option>
                                          @endforeach
                                      </select>
                                  </div>
                                  <div class="form-group col-sm-6">
                                      <label for="fornecedorcontratado">Setor:</label>
                                      <select class="form-control" name="id_setor_atual" >
                                          <option value="">Selecione ...</option>
                                          @foreach($setor as $key)
                                          <option value="{{$key->id }}">
                                              {{ $key->descricao }}
                                          </option>
                                          @endforeach
                                      </select>
                                  </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Buscar</button>
                                <button type="reset" class="btn btn-warning">Limpar</button>
                            </form>
                            <br><a href="{{ route('material') }}">Voltar<a/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>


<script>
    function k(i) {
                        var v = i.value.replace(/\D/g,'');
                        v = (v/100).toFixed(2) + '';
                        v = v.replace(".", ".");
                        v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                        v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                        i.value = v;
                    }
</script>


@endsection
