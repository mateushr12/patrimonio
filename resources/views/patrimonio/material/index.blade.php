@extends('layout_admin.principal')
@section ('breadcrumb','Patrimônio / Materiais')
@section('conteudo')
  <style>
  #ob{
    color: red;
  }
  </style>
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                  <div class="card">
                      <div class="card-header">
                        <strong>Outros Cadastros</strong>
                      </div>
                      <div class="card-body">
                        <a href="{{ route('conservacao') }}" class="btn btn-primary"><i class="fas fa-band-aid"></i> Estado de Conservação</a>&nbsp
                        <a href="{{ route('proprietario') }}" class="btn btn-primary"><i class="fas fa-parking"></i> Proprietário</a>&nbsp
                        <a href="{{ route('setor') }}" class="btn btn-primary"><i class=" fas fa-door-closed"></i> Setor</a>&nbsp
                        <a href="{{ route('tpMaterial') }}" class="btn btn-primary"><i class="fas fa-sitemap"></i> Tipo de Material</a>
                      </div>
                  </div>
              </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-8">
                                    <strong>Materiais/Equipamentos</strong>
                                </div>
                                <div class="col-4 text-right">
                                    <a class="btn btn-primary" href="{{ route('material.relatorio') }}"><i class="zmdi zmdi-receipt"></i> RELATÓRIO</a>
                                    <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small"
                                            data-toggle="modal" data-target="#FormModal">
                                        <i class="zmdi zmdi-plus"></i>NOVO
                                    </button>
                                </div>
                            </div>
                        </div>

                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="table-responsive table-responsive-data2">
                                        <table class="table" id="tabel">
                                            <thead>
                                                <tr>
                                                    <th>Patrimônio</th>
                                                    <th>Descrição</th>
                                                    <th>Tipo de Material</th>
                                                    <th>Setor Atual</th>
                                                    <th>Usuário Atual</th>
                                                    <th>Devolvido</th>
                                                    <th>Ações</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($material as $key)
                                                  <tr>
                                                    <td>{{$key->patrimonio }}</td>
                                                    <td>{{$key->descricao }}</td>
                                                    <td>{{$key->tipoMaterial->descricao }}</td>
                                                    <td>{{$key->setor->descricao }}</td>
                                                    <td>{{ isset($key->id_usuario) ? $key->usuario->name : 'Sem Usuário' }}</td>
                                                    <td>
                                                      @if (isset($key->devolvido))
                                                        <span class="badge badge-danger">SIM</span>
                                                      @else
                                                        -
                                                      @endif
                                                    </td>
                                                    <td>
                                                        <div class="table-data-feature">
                                                          <a href="{{ route('material.ver', $key->id) }}" target="_blank">
                                                              <button type="button" class="item" title="Ver Material"
                                                              data-toggle="tooltip" data-placement="top" >
                                                                  <i class="zmdi zmdi-eye"></i>
                                                              </button>
                                                          </a>&nbsp;
                                                          @if ($key->devolvido == null)
                                                          <a href="{{ route('material.devolver', $key->id) }}" >
                                                              <button type="button" class="item" title="Devolver para o Proprietário"
                                                              data-toggle="tooltip" data-placement="top" >
                                                                  <i class="zmdi zmdi-replay"></i>
                                                              </button>
                                                          </a>&nbsp;
                                                          @else
                                                          @endif
                                                          @if ($key->devolvido == null)
                                                          {{-- @if (in_array('erp/contratos/editar/{id}', Session::get('permissoes.nomes'))) --}}
                                                            <a href="{{route('material.editar' , $key->id)}}">
                                                                <button type="button" class="item" title="Editar"
                                                                data-toggle="tooltip" data-placement="top" title=""
                                                                    data-original-title="Editar"
                                                                >
                                                                    <i class="zmdi zmdi-edit"></i>
                                                                </button>
                                                            </a>&nbsp;
                                                          @else
                                                          @endif
                                                          {{-- @else
                                                          @endif
                                                          @if (in_array('erp/contratos/deletar/{id}', Session::get('permissoes.nomes'))) --}}
                                                            <a href="{{ route ('material.deletar', $key->id) }}"
                                                                data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                                <button type="button" class="item" title="Deletar"
                                                                data-toggle="tooltip" data-placement="top" title=""
                                                                    data-original-title="Deletar"
                                                                >
                                                                    <i class="zmdi zmdi-delete"></i>
                                                                </button>
                                                            </a>&nbsp;
                                                          {{-- @else
                                                          @endif--}}
                                                          @if ($key->devolvido == null)
                                                          {{-- @if (in_array('erp/contratos/anexar/{id}', Session::get('permissoes.nomes'))) --}}
                                                            <a href="{{ route ('componente', $key->id) }}">
                                                              <button type="button" class="item" title="Componente"
                                                              data-toggle="tooltip" data-placement="top" title=""
                                                              data-original-title="Componente"
                                                              >
                                                                  <i class="zmdi zmdi-devices"></i>
                                                              </button>
                                                            </a>&nbsp;
                                                          {{-- @else
                                                          @endif --}}
                                                        @else
                                                        @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="FormModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="FormModalLabel">Cadastro de Materiais</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form class="" action="{{route('material.salvar')}}" method="post">
                            {{ csrf_field() }}
                            @include('patrimonio.material.form')
                    </div>
                </div>
                <span id="ob">*</span> campo obrigatório
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Salvar
                </button>
                <button class="btn btn-warning" type="reset"  >
                    Limpar
                </button>
            </div>
        </div>
        </form>
    </div>
</div>

<script>

$(document).ready( function () {
  $('#tabel').DataTable({
    paging: false,

    language: {
      search: "Buscar:",
      zeroRecords: "Nenhum registro encontrado.",
      info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      infoEmpty: "Mostrando 0 até 0 de 0 registros",
      lengthMenu: "Mostrar _MENU_ entradas",
      infoFiltered: "(filtrado de _MAX_ registros)",
      paginate: {
        first: "Primeiro",
        previous: "Anterior",
        next: "Proximo",
        last: "Ultimo"
      },
      emptyTable: "Nenhum registro encontrado."
    }
  });
});


function k(i) {
  var v = i.value.replace(/\D/g,'');
  v = (v/100).toFixed(2) + '';
  v = v.replace(".", ".");
  v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
  v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
  i.value = v;
}

function data(j) {
  var d2 = j.value
  var d1 = document.getElementById('inicio')
  d1 = d1.value
  if (d1 > d2){
    alert('Inicio da Vigência tem que ser menor que o Fim da Vigência.')
    $("#inicio").val("")
    $("#fim").val("")
  }else if(d1 == ''){
    alert('Escolha uma data.')
    $("#fim").val("")
  }
}

function vl(){
  var vlr = document.getElementById('valorr')
  vlr = vlr.value
  if (vlr == 0.00){
    alert('Valor não pode ser zero.')
    $("#valorr").val("")
  }
}



</script>
@endsection
