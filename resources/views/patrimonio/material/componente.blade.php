@extends('layout_admin.principal')
@section ('breadcrumb','Patrimônio / Material / Componentes')
@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Componentes</strong>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body card-block">
                            <form class="" action="{{route('componente.salvar', $material->id)}}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                  <div class="col-12 form-group">
                                    <label for="numero">Componente*</label>
                                    <select class="form-control" name="id_tipo_material" id="" required>
                                        <option value="">Selecione ...</option>
                                        @foreach($tpmaterial as $b)
                                        <option value="{{ $b->id }}">{{$b->descricao}}</option>
                                        @endforeach
                                    </select>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-12 form-group">
                                    <label for="numero">Obs</label>
                                    <textarea name="obs" class="form-control" id="" ></textarea>
                                  </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Cadastrar</button>
                            </form>
                            <hr>
                            <div class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                    <tr>
                                        <th>Componente</th>
                                        <th>Obs</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($componente as $key)
                                    <tr>
                                      <td>{{ $key->tipoMaterial->descricao }}</td>
                                      <td>{{ $key->obs }}</td>
                                      <td>
                                          <div class="table-data-feature">
                                              {{-- {{route('empenho.deletar' , $liquidado->id)}} --}}
                                              <a href="{{ route('liquidar.deletar',$key->id) }}"
                                                  data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                  <button type="button" class="item" title="Deletar"
                                                  data-toggle="tooltip" data-placement="top" title=""
                                                      data-original-title="Deletar"
                                                  >
                                                      <i class="zmdi zmdi-delete"></i>
                                                  </button>
                                              </a>
                                          </div>
                                      </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function k(i) {
                var v = i.value.replace(/\D/g,'');
                v = (v/100).toFixed(2) + '';
                v = v.replace(".", ".");
                v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                i.value = v;
            }
</script>
@endsection
