@extends('layout_admin.principal')
@section ('breadcrumb','Atendimentos')

@section('conteudo')
<section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                    @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Filtro Atendimentos</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">
                                <form action="{{route('atendimentos.busca')}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-4 form-group">
                                            <label for="status"><strong>Buscar por Status:</strong></label>
                                            <select class="form-control" name="id_status" >
                                                <option value="">STATUS</option>
                                                @foreach($status as $key)
                                                <option value="{{$key->id }}">
                                                    {{ $key->descricao }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 form-group ">
                                            <button type="submit" class="btn btn-primary">Buscar</button>
                                            <button type="reset" class="btn btn-warning">Limpar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Atendimentos</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">
                            <div class="table-responsive table-responsive-data2">
                                @if($atendimentos->count())
                                <table class="table" id="tabel">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Titulo</th>
                                        <th>Status</th>
                                        <th>Última Atualização</th>
                                        <th>Data Abertura</th>
                                        <th>Prioridade</th>
                                        <th>Requerente</th>
                                        <th>Atribuido Para</th>
                                        <th>Autorizado Por</th>
                                        <th>Categoria</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($atendimentos as $atendimento)
                                    <tr>
                                    <td>{{ $atendimento->id }}</td>
                                    <td><a href="{{route('atendimentos.ver' , $atendimento->id)}}">{{ $atendimento->titulo }}</a></td>
                                    <td>
                                        @if($atendimento->status->id == 1)
                                        <span class="badge badge-primary">{{ $atendimento->status->descricao }}</span>
                                        @elseif ($atendimento->status->id == 2)
                                        <span class="badge badge-secondary">{{ $atendimento->status->descricao }}</span>
                                        @elseif ($atendimento->status->id == 3)
                                        <span class="badge badge-success">{{ $atendimento->status->descricao }}</span>
                                        @elseif ($atendimento->status->id == 4)
                                        <span class="badge badge-info">{{ $atendimento->status->descricao }}</span>
                                        @elseif ($atendimento->status->id == 5)
                                        <span class="badge badge-warning">{{ $atendimento->status->descricao }}</span>
                                        @elseif ($atendimento->status->id == 6)
                                        <span class="badge badge-dark">{{ $atendimento->status->descricao }}</span>
                                        @endif
                                    </td>
                                    <td>{{\Carbon\Carbon::parse($atendimento->dt_andamento)->format('d/m/Y')}}</td>
                                    <td>{{\Carbon\Carbon::parse($atendimento->dt_abertura)->format('d/m/Y')}}</td>                                       
                                    <td>
                                        @if ($atendimento->urgencia == 1)
                                        <span class="badge badge-warning">NÃO</span>
                                        @else
                                        <span class="badge badge-danger">SIM</span>
                                        @endif
                                    </td>
                                    <td>{{ $atendimento->requerente->name }}</td>
                                    <td>
                                        @if(empty($atendimento->atribuido->name))
                                        -
                                        @else
                                        {{ $atendimento->atribuido->name }}
                                        @endif
                                    </td>
                                    <td>
                                        @if(empty($atendimento->autorizado->name))
                                        -
                                        @else
                                        {{ $atendimento->autorizado->name }}
                                        @endif
                                    </td>
                                    <td>{{ $atendimento->categoria->descricao }}</td>
                                    <td>
                                        @if (in_array('erp/patrimonio/atribuirAtendimento/{id}', Session::get('permissoes.nomes')))
                                            @if($atendimento->status->id == 1)
                                                <a href="{{route('atendimentos.atribuir' , $atendimento->id)}}">  
                                                    <button type="button" class="btn btn-outline-info btn-sm" title="Atribuir">
                                                    <i class="fas fa-people-carry"></i>
                                                    </button>
                                                </a>
                                            @endif
                                        @endif

                                        @if($atendimento->status->id == 4 and $atendimento->id_requerente == Auth::user()->id)
                                            <a href="{{route('atendimentos.fechar' , $atendimento->id)}}">  
                                                <button type="button" class="btn btn-outline-dark btn-sm" title="Fechar">
                                                <i class="fas fa-spell-check"></i>
                                                </button>
                                            </a>                                       
                                        @endif
                                    </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                </table>
                                @else
                                Não Possui Atendimentos
                                @endif
                            </div>
                            <div class="card-footer">
                                {{ $atendimentos->links() }}
                            </div>
                        </div>
                        </div>

                    </div>
                </div>
            </div>
    </section>
@endsection
