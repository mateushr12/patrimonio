@extends('layout_admin.principal')
@section ('breadcrumb','Novo Atendimento')

@section('conteudo')
<section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                    @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Novo Atendimento</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">
                                <form action="{{route('atendimentos.salvar')}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-12 form-group">
                                            <label for="status"><strong>Tipo:</strong></label>
                                            <select class="form-control" name="id_tipo" id="id_tipo" onChange="tipo()" required>
                                                <option value="">---</option>
                                                @foreach($tipo as $key)
                                                <option value="{{ $key->id }}">
                                                    {{ $key->descricao }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                   
                                    <div class="row">
                                        <div class="col-12 form-group">
                                            <label for="status"><strong>Categoria:</strong></label>
                                            
                                            <select class="form-control"  id="requisicao" onChange="requi()"  style="display: none">
                                                <option value=""> -- SELECIONE --</option>
                                                <option value="1">Mudança</option>
                                                <option value="2">Almoxarifado</option>
                                            </select>

                                            <select class="form-control"  id="suporte" onChange="supo()" style="display: none">
                                                <option value=""> -- SELECIONE --</option>
                                                <option value="3">Suporte Técnico</option>
                                            </select>

                                            <input type="hidden" class="form-control" id="id_categoria" name="id_categoria">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 form-group">
                                            <label for="status"><strong>Prioridade:</strong></label>
                                            <select class="form-control" name="urgencia" required >
                                                <option value="1">Não</option>
                                                <option value="2">Sim</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 form-group">
                                            <label for="status"><strong>Titulo:</strong></label>
                                            <input type="text" class="form-control" id="titulo" name="titulo" required >
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 form-group">
                                            <label for="status"><strong>Descrição:</strong></label>
                                            <textarea class="form-control" name="descricao" id="descricao" style="width: 100%; height: 150px;" required></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 form-group ">
                                            <input type="hidden" name="id_requerente" id="id_requerente" value="{{$id_user}}">
                                            <input type="hidden" name="dt_abertura" id="dt_abertura" value="{{  now()->toDateTimeString() }}">
                                            <input type="hidden" name="dt_andamento" id="dt_andamento" value="{{  now()->toDateTimeString() }}">
                                            <button type="submit" class="btn btn-primary">Salvar</button>
                                            <button type="reset" class="btn btn-warning">Limpar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
   
    <script type="text/javascript">
        function tipo() {
            var select = document.getElementById('id_tipo');
            var option = select.options[select.selectedIndex];

            if(option.value == 1){
                document.getElementById('requisicao').style.display = 'block';
                document.getElementById('suporte').style.display = 'none';
            } else if(option.value == 2) {
                document.getElementById('requisicao').style.display = 'none';
                document.getElementById('suporte').style.display = 'block';
            } else {
                document.getElementById('requisicao').style.display = 'none';
                document.getElementById('suporte').style.display = 'none';
                document.getElementById('id_categoria').value = '';
            }
        }
        tipo();
    </script>
    <script type="text/javascript">
        function requi() {
            var select = document.getElementById('requisicao');
            var option = select.options[select.selectedIndex];
            
            document.getElementById('id_categoria').value = option.value;
        }

        requi();
    </script>
    <script type="text/javascript">
        function supo() {
            var select = document.getElementById('suporte');
            var option = select.options[select.selectedIndex];
            
            document.getElementById('id_categoria').value = option.value;
        }

        supo();
    </script>
    
@endsection