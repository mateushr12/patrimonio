@extends('layout_admin.principal')
@section ('breadcrumb','Fechar Atendimento')

@section('conteudo')
<section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        @if (session('erro'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{session('erro')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Fechar Atendimento</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label for="titulo"><strong>Titulo:</strong></label>
                                            <input type="text" class="form-control" id="titulo" value="{{ $atendimentos->titulo }}" readonly>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="requerente"><strong>Requerente:</strong></label>
                                            <input type="text" class="form-control" id="requerente" value="{{ $atendimentos->requerente->name }}" readonly>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="atribuido"><strong>Atribuido:</strong></label>
                                            @if(empty($atendimento->atribuido->name))
                                            <input type="text" class="form-control" id="atribuido" value="---" readonly>
                                            @else
                                            <input type="text" class="form-control" id="atribuido" value="{{ $atendimento->atribuido->name }}" readonly>
                                            @endif  
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="autorizado"><strong>Autorizado:</strong></label>
                                            @if(empty($atendimento->autorizado->name))
                                            <input type="text" class="form-control" id="autorizado" value="---" readonly>
                                            @else
                                            <input type="text" class="form-control" id="autorizado" value="{{ $atendimento->autorizado->name }}" readonly>
                                            @endif  
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label for="dataAbertura"><strong>Data de Abertura:</strong></label>
                                            <input type="text" class="form-control" id="dataAbertura" value="{{\Carbon\Carbon::parse($atendimentos->dt_abertura)->format('d/m/Y')}}" readonly>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="tipo"><strong>Tipo:</strong></label>
                                            <input type="text" class="form-control" id="tipo" value="{{ $atendimentos->tipo->descricao}}" readonly>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="categoria"><strong>Categoria:</strong></label>
                                            <input type="text" class="form-control" id="categoria" value="{{ $atendimentos->categoria->descricao}}" readonly>
                                        </div>
                                        <div class="form-group col-md-3">
                                                <label for="prioridade"><strong>Prioridade:</strong></label>
                                                @if ($atendimentos->urgencia == 1)
                                                    <div class="alert alert-warning" role="alert">NÃO</div>
                                                @else
                                                    <div class="alert alert-danger" role="alert">SIM</div>
                                                @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="descricao"><strong>Descrição:</strong></label>
                                        <textarea class="form-control" id="descricao" disabled>{{ $atendimentos->descricao }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="solucao"><strong>Solução:</strong></label>
                                        <textarea class="form-control" id="solucao" disabled>{{ $atendimentos->solucao }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="solucao_por"><strong>Solucionado Por:</strong></label>
                                        <input type="text" class="form-control" id="solucionado_por" value="{{ $atendimentos->solucionado->name }}" readonly>
                                    </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-12">
                                <strong>Aprovar Solução do Atendimento</strong>
                            </div>
                        </div>
                    </div>
                    <div class="card-body card-block">
                        <div class="form-row">
                            <div class="col-12">
                                <form class="" action="{{route('atendimentos.atualizar', $atendimentos->id)}}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="put">
                                    <input type="hidden" name="fechado_por" value="{{Auth::user()->id}}">
                                    <input type="hidden" name="dt_andamento" value="{{now()->toDateTimeString()}}">
                                    <div class="form-group">
                                        <label for="solucao"><strong>Solução:</strong></label>
                                        <select class="form-control" name="id_status" required>
                                            <option value="">-- Escolha --</option>
                                            <option value="6">SIM</option>
                                            <option value="5">NÃO</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Salvar</button>
                                    </div>
                                </form>
                            </div>
                        </div> 
                    </div>
                </div>
                  
            </div>
        </div>
</section>    
@endsection