@extends('layout_admin.principal')
@section ('breadcrumb','Log Patrimônio')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Buscar Log</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">
                                <form action="{{route('log.resultado')}}" method="post" >
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-6 form-group">
                                          <label for="tabela"><strong>Tabela:</strong></label>
                                          <select name="tabela" class="form-control" >
                                            <option value="">Selecione uma opção</option>
                                            @foreach($tabela as $key)
                                            <option value="{{ $key->tabela }}">{{ $key->tabela }}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                        <div class="col-6 form-group">
                                            <label for="user"><strong>Usuario:</strong></label>
                                            <select name="user" class="form-control" >
                                              <option value="">Selecione uma opção</option>
                                              @foreach($user as $key)
                                              <option value="{{ $key->user }}">{{ $key->user }}</option>
                                              @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 form-group">
                                            <label for="data"><strong>Data:</strong></label>
                                            <input type="date" name="data" id="data" class="form-control">
                                        </div>
                                        <div class="col-6 form-group">
                                            <label for="tipo_evento_redesim"><strong>Ação:</strong></label>
                                            <select name="acao" id="acao" class="form-control">
                                                <option value="">Selecione um evento</option>
                                                <option value="salvar">Salvar</option>
                                                <option value="deletar">Deletar</option>
                                                <option value="atualizar">Atualizar</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 form-group ">
                                            <button type="submit" class="btn btn-primary">Buscar</button>
                                            <button type="reset" class="btn btn-warning">Limpar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
