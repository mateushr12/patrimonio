@extends('layout_admin.principal')
@section ('breadcrumb','Log Patrimônio')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-10">
                                      <h3>Log Tabela</h3>
                                    </div>
                                    {{-- <div class="col-2">
                                        <a class="au-btn au-btn-icon au-btn--green au-btn--small"
                                            href="{{route('taxas.pdf')}}" target="_blank">
                                            <i class="zmdi zmdi-print"></i>Imprimir</a>
                                    </div> --}}
                                </div>
                            </div>
                            <div class="card-body card-block">
                                @if($tabela != null)
                                  <h4>{{ $tb }}</h4>
                                  <hr>
                                  @foreach ($tabela as $key => $value)
                                    <p><strong>{{ $key }}:</strong> {{ $value }}</p>
                                  @endforeach
                                @else
                                  Linha não encontrada.
                                @endif
                        </div>
                    </div>
                </div>
            </div>
    </section>


@endsection
