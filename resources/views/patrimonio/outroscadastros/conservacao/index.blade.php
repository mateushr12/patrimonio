@extends('layout_admin.principal')
@section ('breadcrumb','Patrimônio / Estado de Conservação')

@section('conteudo')
  <section>
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            @if (session('erro'))
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{session('erro')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            @endif
            <div class="card">
              <div class="card-header">
                <strong>Novo Estado de Conservação</strong>
              </div>
              <div class="card-body card-block">
                <form class="" action="{{ route('conservacao.salvar') }}" method="post">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="form-group col-sm-8">
                      <label for="grupo">Estado:</label>
                      <input type="text" class="form-control" id="descricao" name="descricao" onkeyup="this.value = this.value.toUpperCase();" required />                      
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                    <a href="{{route('material')}}" class="btn btn-warning">Voltar</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          @if (session('sucesso'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              {{session('msg')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @elseif (session('delete'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
              {{session('delete')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
          <div class="col-12">
            {{-- <h3>Estado de Conservação</h3> --}}
            <div class="card">
              <table class="table table-borderless table-data3">
                <thead>
                  <tr>
                    <th>Descrição</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($conservacao as $key)
                    <tr>
                      <td>{{$key->descricao}}</td>
                      <td>
                        <div class="table-data-feature">
                          <a href="{{ route('conservacao.deletar', $key->id) }}"
                            data-confirm="Tem certeza que deseja excluir o item selecionado?">
                            <button type="button" class="item" title="Deletar">
                              <i class="zmdi zmdi-delete"></i>
                            </button>
                          </a>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script>
  // function myFunction(i) {
  //
  //     //var idK = element.id;
  //     var c = confirm('Tem certeza que deseja deletar?');
  //     if( c == true){
  //         var a = "/autorizacao/grupos/excluir/"
  //         var b = i
  //         window.location.href = a.concat(b)
  //     }
  // }
</script>




@endsection
