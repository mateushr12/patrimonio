@include('layout_admin._includes.topo')
<body class="animsition">
<section>
    {{-- <div class="section__content section__content--p30"> --}}

    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">
                                <img src="images/icon/logo.png" alt="CoolAdmin">
                            </a>
                        </div>
                        <div class="login-form">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if (session('msg'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{session('msg')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                                <form class="" action="/login" method="post">
                                    {{-- bcrypt('123456') --}}
                                    {{csrf_field()}}
                                    @include('login._form')
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Logar</button>
                                        <button type="reset" class="btn btn-warning">Limpar</button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- </div> --}}
</section>

@include('layout_admin._includes.footer_login')
