<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="JUCESE - Junta Comercial do Estado de Sergipe">
    <meta name="author" content="Equipe de TI JUCESE">
    <meta name="keywords" content="jucese, sergipe, erp">

    <!-- Title Page-->
    <title>ERP - JUCESE</title>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
    <!-- Fontfaces CSS-->
    <link href="<?php echo asset('css/font-face.css')?>" rel="stylesheet" media="all">
    <link href="<?php echo asset('vendor/fontawesome-free-5.11.2/css/all.min.css')?>" rel="stylesheet" media="all">
    <link href="<?php echo asset('vendor/mdi-font/css/material-design-iconic-font.min.css')?>" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->

    <link href="<?php echo asset('vendor/bootstrap-4.1/bootstrap.min.css')?>" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo asset('vendor/animsition/animsition.min.css')?> " rel="stylesheet" media="all">
    <link href="<?php echo asset('vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')?> " rel="stylesheet" media="all">
    <link href="<?php echo asset('vendor/wow/animate.css')?> " rel="stylesheet" media="all">
    <link href="<?php echo asset('vendor/css-hamburgers/hamburgers.min.css')?> " rel="stylesheet" media="all">
    <link href="<?php echo asset('vendor/slick/slick.css')?> " rel="stylesheet" media="all">
    <link href="<?php echo asset('vendor/select2/select2.min.css')?> " rel="stylesheet" media="all">
    <link href="<?php echo asset('vendor/perfect-scrollbar/perfect-scrollbar.css')?> " rel="stylesheet" media="all">
    <link href="<?php echo asset('vendor/vector-map/jqvmap.min.css')?> " rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo asset('css/theme.css')?>" rel="stylesheet" media="all">
    <link href="<?php echo asset('css/jquery-steps.css')?>" rel="stylesheet" media="all">
    

    <script src="<?php echo asset('vendor/jquery-3.2.1.min.js')?>"></script>


    <!-- Highcharts -->
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>

    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <script src="https://code.highcharts.com/modules/offline-exporting.js"></script>
    <script src="https://code.highcharts.com/8.0.0/lib/jspdf.js"></script>
    <script src="https://code.highcharts.com/8.0.0/lib/svg2pdf.js"></script>
    <script src="https://code.highcharts.com/8.0.0/lib/rgbcolor.js"></script>

</head>
