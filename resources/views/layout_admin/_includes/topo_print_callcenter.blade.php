
<!DOCTYPE html>

<html lang="pt-br">

<head>
    <!-- CSS para impressão -->
    <link rel="stylesheet" type="text/css" href="/print.css" media="print" />

    <style>
        #pageCounter {
          counter-reset: pageTotal;
        }
        #pageCounter span {
            counter-increment: pageTotal; 
        }
        #pageNumbers {
            counter-reset: currentPage;
        }
        #pageNumbers div:before { 
            counter-increment: currentPage; 
            content: "Page " counter(currentPage) " of "; 
        }
        #pageNumbers div:after { 
            content: counter(pageTotal); 
        }
        .page-break { 
            page-break-before: always; 
        }
        #header, #footer {
            position: fixed;
            left: 0;
            right: 0;
            color: #aaa;
            font-size: 0.9em;
        }
        #section {
        }
        #header {
            top: 0;
        }
        #footer {
            bottom: 40;
        }
        .page-number:before {
            content: "Página " counter(page) " de " counter(pageTotal);
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 5px;
        }
        
        table { page-break-inside: auto }
        tr    { page-break-inside: avoid; page-break-after:auto }
        thead { display:table-header-group; }
        tfoot { display:table-footer-group; }

        @page { 
            size: auto;
        }
        body {
            margin-top: 80;
            margin-bottom: 50;
        }
    </style>
</head>


<body>
    <div id="header" style="">
        <img src="images/gov-jucese-top-relatorio.png" width="700" border: none;>
    </div>

    <div id="footer" style="">
        <img src="images/gov-jucese-footer-relatorio.png" width="700" height="70" border: none;>
        {{-- <div class="page-number" style="text-align: right;"></div> --}}
    </div>

    <section>
        <div class="container">
            <h3 style="text-align: center;">E.R.P. - SISTEMA CALLCENTER</h3>
            <h4 style="text-align: center;">{{ $titulo }}</h4>
            <h5 style="text-align: center;">Data de Emissão: {{ datahoraBR( date( 'Y-m-d', strtotime( now() ) ) ) }} ( {{ session()->get( 'totalregistros' ) }} registros )</h5>


