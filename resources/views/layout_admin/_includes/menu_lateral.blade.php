<body class="animsition">
  <div class="page-wrapper">

    <!-- MENU SIDEBAR-->
    <aside class="menu-sidebar2">
      <div class="logo">
        <a href="#">
          <img src="<?php echo asset('images/logo-white.png')?>" alt="JUCESE" />
        </a>
      </div>

      <div class="menu-sidebar2__content js-scrollbar1">
        <div class="account2">

          <div class="image img-cir img-120">
              @if ( file_exists( 'images/fotouser/'.auth()->user()->email.'.jpg' ) )
                  <img src="<?php echo asset('images/fotouser/'.auth()->user()->email.'.jpg') ?>" alt="" />
              @else
                  <img src="<?php echo asset('images/fotouser/default-avatar.png')?>" alt="" />
              @endif
          </div>

          <!--<h3>BEM VINDO</h3>-->
          <h5 class="name">{{ \Auth::user()->name }}</h5>
          <h5 class="nivel">{{ \Auth::user()->grupo->nome }}</h5>
          <a href="{{ route('sair') }}">SAIR</a>
          </div>

          <nav class="navbar-sidebar2">
            <ul class="list-unstyled navbar__list">
              @if (Session::get('permissoes.patrimonio') != 0)
                <li class="active has-sub" >
                  <a class="js-arrow" href="#" >
                    <i class="fas fa-fire-extinguisher"></i>Suporte e Patrimônio
                    <span class="arrow"><i class="fas fa-angle-down"></i></span>
                  </a>
                  <ul class="list-unstyled navbar__sub-list js-sub-list" >
                    <li>
                      @if (in_array('erp/patrimonio/atendimentos', Session::get('permissoes.nomes')))
                        <a href="{{route('atendimentos')}}">
                          <i class="fas fa-clipboard-list"></i>Atendimentos
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('erp/patrimonio/novoAtendimento', Session::get('permissoes.nomes')))
                        <a href="{{route('atendimentos.novo')}}">
                          <i class="fas fa-first-aid"></i>Novo Atendimento
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('patrimonio/material', Session::get('permissoes.nomes')))
                        <a href="{{route('material')}}">
                          <i class="fas fa-list"></i>Material
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('patrimonio/material/etiqueta', Session::get('permissoes.nomes')))
                        <a href="{{route('material.etiqueta')}}">
                          <i class="fas fa-qrcode"></i>Etiqueta Material
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('patrimonio/movimentacao', Session::get('permissoes.nomes')))
                        <a href="{{route('movimentacao')}}">
                          <i class="fas fa-luggage-cart"></i>Movimentação
                        </a>
                      @else
                      @endif
                    </li>
                  </ul>
                </li>
              @else
              @endif



            @if (Session::get('permissoes.callcenter_chamada') != 0)
                <li class="active has-sub" >
                  <a class="js-arrow" href="#" >
                    <i class="fas fa-phone"></i>CALLCENTER - Chamada
                    <span class="arrow"><i class="fas fa-angle-down"></i></span>
                  </a>
                  <ul class="list-unstyled navbar__sub-list js-sub-list" >
                    <li>
                      @if (in_array('callcenter/item_menu/chamada', Session::get('permissoes.nomes')))
                        <a href="{{route('chamada.index')}}">
                          <i class="fas fa-headset"></i>Chamada
                        </a>
                      @else
                      @endif
                    </li>
                  </ul>
                </li>
              @else
              @endif

              @if (Session::get('permissoes.callcenter_tabelas') != 0)
                <li class="active has-sub" >
                  <a class="js-arrow" href="#" >
                    <i class="fas fa-table"></i>CALLCENTER - Tabelas
                    <span class="arrow"><i class="fas fa-angle-down"></i></span>
                  </a>
                  <ul class="list-unstyled navbar__sub-list js-sub-list" >
                    <li>
                      @if (in_array('callcenter/item_menu/cidadao', Session::get('permissoes.nomes')))
                        <a href="{{route('cidadao.index')}}">
                          <i class="fas fa-male"></i>Cidadão
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('callcenter/item_menu/tipoatendimento', Session::get('permissoes.nomes')))
                        <a href="{{route('tipoatendimento.index')}}">
                          <i class="fas fa-person-booth"></i>Tipo de Atendimento
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('callcenter/item_menu/tipocidadao', Session::get('permissoes.nomes')))
                        <a href="{{route('tipocidadao.index')}}">
                          <i class="fas fa-walking"></i>Tipo de Cidadão
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('callcenter/item_menu/tipoproblema', Session::get('permissoes.nomes')))
                        <a href="{{route('tipoproblema.index')}}">
                          <i class="fas fa-bug"></i>Tipo de Problema
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('callcenter/item_menu/tiposolucao', Session::get('permissoes.nomes')))
                        <a href="{{route('tiposolucao.index')}}">
                          <i class="fas fa-lightbulb"></i>Tipo de Solução
                        </a>
                      @else
                      @endif
                    </li>
                  </ul>
                </li>
              @else
              @endif


              @if (Session::get('permissoes.callcenter_consultas') != 0)
                <li class="active has-sub" >
                  <a class="js-arrow" href="#" >
                    <i class="fas fa-search"></i>CALLCENTER - Consultas
                    <span class="arrow"><i class="fas fa-angle-down"></i></span>
                  </a>
                  <ul class="list-unstyled navbar__sub-list js-sub-list" >
                    <li>
                      @if (in_array('callcenter/item_menu/consulta/atendimentosporcpf', Session::get('permissoes.nomes')))
                        <a href="{{route('consulta.rota_atendimentosporcpf_parametros')}}">
                          <i class="fas fa-barcode"></i>Atendimentos p/ C.P.F.
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('callcenter/item_menu/consulta/cpfdistintoportipocidadao', Session::get('permissoes.nomes')))
                        <a href="{{ route('consulta.cpfdistintoportipocidadao') }}">
                          <i class="fas fa-barcode"></i>C.P.F.'s Distintos por Tipo de Cidadão
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('callcenter/item_menu/consulta/chamadageral', Session::get('permissoes.nomes')))
                        <a href="{{route('consulta.rota_chamadageral_parametros')}}">
                          <i class="fas fa-folder-open"></i>Consulta - Geral
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('callcenter/item_menu/consulta/problemasporcidadao', Session::get('permissoes.nomes')))
                        <a href="{{route('consulta.rota_problemasportipocidadao_parametros')}}">
                          <i class="fas fa-hiking"></i>Problemas p/ Tipo de Cidadão
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('callcenter/item_menu/consulta/produtividade', Session::get('permissoes.nomes')))
                        <a href="{{route('consulta.produtividade')}}">
                          <i class="fas fa-chart-bar"></i>Produtividade
                        </a>
                      @else
                      @endif
                    </li>
                  </ul>
                </li>
              @else
              @endif

              @if (Session::get('permissoes.callcenter_consultas') != 0 )
                <li class="active has-sub" >
                  <a class="js-arrow" href="#" >
                    <i class="fab fa-line"></i>CALLCENTER - Painel Online
                    <span class="arrow"><i class="fas fa-angle-down"></i></span>
                  </a>
                  <ul class="list-unstyled navbar__sub-list js-sub-list" >
                    <li>
                      @if (in_array('callcenter/item_menu/consulta/dashboard', Session::get('permissoes.nomes')))
                        <a href="{{route('consulta.dashboard')}}">
                          <i class="fab fa-line"></i>Dashboard
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('callcenter/item_menu/consulta/painelonline', Session::get('permissoes.nomes')))
                        <a href="{{route('consulta.painelonline')}}">
                          <i class="fab fa-line"></i>Painel OnLine
                        </a>
                      @else
                      @endif
                    </li>
                  </ul>
                </li>
              @else
              @endif

              @if (Session::get('permissoes.plan') != 0)
                <li class="active has-sub" >
                  <a class="js-arrow" href="#" >
                    <i class="fas fa-atlas"></i>Planejamento
                    <span class="arrow"><i class="fas fa-angle-down"></i></span>
                  </a>
                  <ul class="list-unstyled navbar__sub-list js-sub-list" >
                    <li>
                      @if (in_array('erp/orcamentoinicial', Session::get('permissoes.nomes')))
                        <a href="{{route('orcamentoinicial.index')}}">
                          <i class="fas fa-money-check-alt"></i>Orçamento Inicial
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('erp/acoes', Session::get('permissoes.nomes')))
                        <a href="{{route('erp.acoes')}}">
                          <i class="fas fa-boxes"></i>Ações
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('erp/orcamentosuplementar', Session::get('permissoes.nomes')))
                        <a href="{{route('orcamentosuplementar.index')}}">
                          <i class="fas fa-comments-dollar"></i>Suplementar
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('erp/remanejamento', Session::get('permissoes.nomes')))
                        <a href="{{route('remanejamento.index')}}">
                          <i class="fas fa-list-alt"></i>Remanejamento
                        </a>
                      @else
                      @endif
                    </li>
                  </ul>
                </li>
              @else
              @endif
              @if (Session::get('permissoes.gestao') != 0)
                <li class="active has-sub">
                  <a class="js-arrow" href="#">
                    <i class="fas fa-book-open"></i>Gestão
                    <span class="arrow"><i class="fas fa-angle-down"></i></span>
                  </a>
                  <ul class="list-unstyled navbar__sub-list js-sub-list">
                    <li>
                      @if (in_array('erp/contratos', Session::get('permissoes.nomes')))
                      <a href="{{route('contratos.index')}}">
                        <i class="fas fa-file-contract"></i>Contratos
                      </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('erp/fornecedor', Session::get('permissoes.nomes')))
                      <a href="{{route('fornecedor.index')}}">
                        <i class="far fa-address-card"></i>Fornecedores
                      </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('erp/contratosPres', Session::get('permissoes.nomes')))
                      <a href="{{route('contratosPres')}}">
                        <i class="fas fa-file-powerpoint"></i>Contratos Presidência
                      </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('erp/contratosPres/termo', Session::get('permissoes.nomes')))
                      <a href="{{route('termo')}}">
                        <i class="fas fa-clipboard-list"></i>Tipos de Termo
                      </a>
                      @else
                      @endif
                    </li>
                  </ul>
                </li>
              @else
              @endif
              @if (Session::get('permissoes.exe') != 0)
                <li class="active has-sub">
                  <a class="js-arrow" href="#">
                    <i class="fas fa-money-check-alt"></i>Execução
                    <span class="arrow"><i class="fas fa-angle-down"></i></span>
                  </a>
                  <ul class="list-unstyled navbar__sub-list js-sub-list">
                    <li>
                      @if (in_array('erp/empenho', Session::get('permissoes.nomes')))
                      <a href="{{route('empenho.index')}}">
                        <i class="fas fa-file-invoice-dollar"></i>Empenho
                      </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('erp/empenho/showcotas', Session::get('permissoes.nomes')))
                      <a href="{{route('empenho.cota')}}">
                        <i class="far fa-money-bill-alt"></i>Cotas Mensais
                      </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('erp/suprimentos', Session::get('permissoes.nomes')))
                      <a href="{{route('suprimento')}}">
                        <i class="fab fa-buffer"></i>Suprimentos de Fundos
                      </a>
                      @else
                      @endif
                    </li>
                  </ul>
                </li>
              @else
              @endif
              @if (Session::get('permissoes.contas') != 0)
                <li class="active has-sub">
                  <a class="js-arrow" href="#">
                    <i class="fas fa-chart-bar"></i>Contas a Receber (Taxas)
                    <span class="arrow"><i class="fas fa-angle-down"></i></span>
                  </a>
                  <ul class="list-unstyled navbar__sub-list js-sub-list">
                    <li>
                      @if (in_array('erp/taxas', Session::get('permissoes.nomes')))
                      <a href="{{route('taxas')}}">
                        <i class="fas fa-search-dollar"></i>Relatório Taxas Sigfacil
                      </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('erp/taxas/serasa', Session::get('permissoes.nomes')))
                      <a href="{{route('taxas.inicio.serasa')}}">
                        <i class="fas fa-money-bill-alt"></i>Cadastrar Taxas SERASA/BOA VISTA
                      </a>
                    @else
                    @endif
                    </li>
                    <li>
                      @if (in_array('erp/taxas/serasaBoleto', Session::get('permissoes.nomes')))
                      <a href="{{route('taxas.serasa.boleto')}}">
                        <i class="fas fa-money-bill-wave"></i>Boletos SERASA/BOA VISTA
                      </a>
                    @else
                    @endif
                    </li>
                    <li>
                      @if (in_array('erp/taxasRetorno', Session::get('permissoes.nomes')))
                      <a href="{{route('taxasRetorno.index')}}">
                        <i class="fas fa-money-check-alt"></i>Taxas Retorno Banese
                      </a>
                    @else
                    @endif
                    </li>
                    <li>
                      @if (in_array('erp/taxasRetorno/exibir', Session::get('permissoes.nomes')))
                      <a href="{{route('taxasRetorno.exibir')}}">
                        <i class="fas fa-file-invoice-dollar"></i>Relatório Taxas Banese
                      </a>
                    @else
                    @endif
                    </li>
                    <li>
                    @if (in_array('erp/taxaRetorno/buscarpordia', Session::get('permissoes.nomes')))
                      <a href="{{route('taxasRetorno.buscarpordia')}}">
                        <i class="far fa-calendar-minus"></i>Relatório Taxas Banese Por Dia
                      </a>
                    @else
                    @endif
                    </li>
                  </ul>
                </li>
              @else
              @endif
              @if (Session::get('permissoes.contasp') != 0)
                <li class="active has-sub">
                  <a class="js-arrow" href="#">
                    <i class="fas fa-donate"></i>Contas a Pagar
                    <span class="arrow"><i class="fas fa-angle-down"></i></span>
                  </a>
                  <ul class="list-unstyled navbar__sub-list js-sub-list">
                    <li>
                      @if (in_array('porelemento', Session::get('permissoes.nomes')))
                      <a href="{{ route('contasporelemento') }}">
                        <i class="far fa-sticky-note"></i>Elemento de Despesa
                      </a>
                    @else
                    @endif
                    </li>
                    <li>
                      @if (in_array('porgrupo', Session::get('permissoes.nomes')))
                      <a href="{{ route('contasporgrupo') }}">
                        <i class="far fa-list-alt"></i>Grupo de Despesa
                      </a>
                    @else
                    @endif
                    </li>
                  </ul>
                </li>
              @else
              @endif
              @if (Session::get('permissoes.relatorio') != 0)
                <li class="active has-sub">
                  <a class="js-arrow" href="#">
                    <i class="fas fa-bars"></i>Relatórios
                    <span class="arrow"><i class="fas fa-angle-down"></i></span>
                  </a>
                  <ul class="list-unstyled navbar__sub-list js-sub-list">
                    <li>
                      @if (in_array('erp/relatorio/acao', Session::get('permissoes.nomes')))
                      <a href="{{ route('acao.relatorio.buscar') }}">
                        <i class="fas fa-boxes"></i>Ações
                      </a>
                    @else
                    @endif
                    </li>
                    <li>
                      @if (in_array('erp/relatorio/contrato', Session::get('permissoes.nomes')))
                      <a href="{{ route('contrato.relatorio.buscar') }}">
                        <i class="fas fa-file-contract"></i>Contratos
                      </a>
                    @else
                    @endif
                    </li>
                    <li>
                      @if (in_array('erp/relatorio/pagamento', Session::get('permissoes.nomes')))
                      <a href="{{ route('pagamento.relatorio.buscar') }}">
                        <i class="fas fa-dollar-sign"></i>Pagamentos
                      </a>
                    @else
                    @endif
                    </li>
                  </ul>
                </li>
              @else
              @endif
              @if (Session::get('permissoes.graficos') != 0)
              <li class="active has-sub">
                <a class="js-arrow" href="#">
                  <i class="fas fa-chart-pie"></i>Gráficos
                  <span class="arrow"><i class="fas fa-angle-down"></i></span>
                </a>
                <ul class="list-unstyled navbar__sub-list js-sub-list">
                  <li>
                    <a href="{{ route('graficos.eventos') }}">
                      <i class="fas fa-chart-line"></i>Taxas por evento
                    </a>
                  </li>
                  <li>
                    <a href="{{ route('graficos.empenho') }}">
                      <i class="fas fa-chart-area"></i>Empenhos
                    </a>
                  </li>
                  <li>
                    <a href="{{ route('graficos.empenho.all') }}">
                      <i class="fas fa-poll"></i>Empenhos Por Ano
                    </a>
                  </li>
                  <li>
                    <a href="{{ route('graficos.taxames') }}">
                      <i class="fas fa-chart-bar"></i>Taxas recebidas por mês
                    </a>
                  </li>
                  <li>
                    <a href="{{ route('graficos.pagForn') }}">
                      <i class="fas fa-signal"></i>Pagamentos por Fornecedor
                    </a>
                  </li>
                </ul>
              </li>
            @else
            @endif
              {{-- //////////////////// Caixas /////////////// --}}
              @if (Session::get('permissoes.caixas') != 0)
              <li class="active has-sub">
                <a class="js-arrow" href="#">
                  <i class="fas fa-archive"></i>Gerenciar Arquivos
                  <span class="arrow"><i class="fas fa-angle-down"></i></span>
                </a>
                <ul class="list-unstyled navbar__sub-list js-sub-list">
                  <li>
                    @if (in_array('erp/caixas', Session::get('permissoes.nomes')))
                    <a href="{{ route('caixas') }}">
                      <i class="fas fa-boxes"></i>Caixas
                    </a>
                  @else
                  @endif
                  </li>
                  <li>
                    @if (in_array('erp/caixas/buscar', Session::get('permissoes.nomes')))
                    <a href="{{ route('buscar') }}">
                      <i class="fas fa-box-open"></i>Buscar Conteúdo
                    </a>
                  @else
                  @endif
                  </li>
                  <li>
                    @if (in_array('erp/caixas/tipo', Session::get('permissoes.nomes')))
                    <a href="{{ route('caixa.tipo') }}">
                      <i class="fas fa-box"></i>Setores
                    </a>
                  @else
                  @endif
                  </li>
                </ul>
              </li>
              @else
              @endif
              {{-- /////////////////////////////////////////// --}}
              @if (Session::get('permissoes.outroscad') != 0)
                <li class="active has-sub">
                  <a class="js-arrow" href="#">
                    <i class="fas fa-file-invoice"></i>Outros Cadastros
                    <span class="arrow"><i class="fas fa-angle-down"></i></span>
                  </a>
                  <ul class="list-unstyled navbar__sub-list js-sub-list">
                    <li>
                      @if (in_array('erp/cadastro/elemento', Session::get('permissoes.nomes')))
                        <a href="{{ route('elemento') }}">
                          <i class="fab fa-elementor"></i>Elemento e Subelementos de Despesa
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('erp/cadastro/modalidadelic', Session::get('permissoes.nomes')))
                        <a href="{{ route('modalidadelic') }}">
                          <i class="far fa-list-alt"></i>Modalidade da Licitação
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('erp/cadastro/reflegal', Session::get('permissoes.nomes')))
                        <a href="{{ route('reflegal') }}">
                          <i class="fas fa-balance-scale"></i>Referência Legal
                        </a>
                      @else
                      @endif
                    </li>
                    <li>
                      @if (in_array('erp/cadastro/listCorrigir', Session::get('permissoes.nomes')))
                        <a href="{{ route('listCorrigir') }}">
                          <i class="fas fa-edit"></i>Corrigir Pagamento
                        </a>
                      @else
                      @endif
                    </li>
                    {{-- <li>
                      <a href="{{ route('tpretencao') }}">
                        <i class="fas fa-cut"></i>Tipos de Retenções
                      </a>
                    </li> --}}
                  </ul>
                </li>
              @else
              @endif
            </ul>
            </li>
          </ul>
          </nav>

      </div>
    </aside>
    <!-- END MENU SIDEBAR-->

    <!-- PAGE CONTAINER-->
    <div class="page-container2">
