@extends('layout_admin.principal')
@section ('breadcrumb','Orçamento Suplementar')

@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-10">
                                    <strong>Orçamento Suplementar</strong>
                                </div>
                                <div class="col-2">
                                    <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small"
                                        data-toggle="modal" data-target="#FormModal">
                                        <i class="zmdi zmdi-plus"></i>Adicionar
                                    </button>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body card-block">
                            <div class="table-responsive table-responsive-data2">

                                <table class="table table-data2">
                                    <thead>
                                        <tr>
                                            {{-- <th>
                                                <label class="au-checkbox">
                                                    <input type="checkbox">
                                                    <span class="au-checkmark"></span>
                                                </label>
                                            </th> --}}
                                            <th>id</th>
                                            <th>Valor</th>
                                            <th>Ação</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($orcamento_suplementar as $orcamentos)
                                        <tr class="tr-shadow">
                                            {{-- <td>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </td> --}}
                                            <td>{{$orcamentos->id}}</td>
                                            <td>{{ number_format($orcamentos->valor,2,',','.') }}</td>
                                            <td>{{$orcamentos->Acoes->DescAcao}}</td>
                                            <td>
                                                <div class="table-data-feature">
                                                  @if (in_array('erp/orcamentosuplementar/{id}/edit', Session::get('permissoes.nomes')))
                                                    <a href="{{route('orcamentosuplementar.edit', $orcamentos->id)}}">
                                                        <button type="button" class="item" title="Editar">
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </button>
                                                    </a>&nbsp;
                                                  @else
                                                  @endif
                                                  @if (in_array('erp/orcamentosuplementar/{id}/delete', Session::get('permissoes.nomes')))
                                                    <a href="{{ route ('orcamentosuplementar.delete', $orcamentos->id) }}"
                                                        data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                        <button type="button" class="item" title="Deletar"
                                                        data-toggle="tooltip" data-placement="top" title=""
                                                            data-original-title="Deletar"
                                                        >
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>
                                                    </a>
                                                  @else
                                                  @endif
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="spacer"></tr>
                                        @empty
                                        <tr>
                                            Nenhum Orçamento Cadastrado.
                                        </tr>

                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>


<!-- modal form ORÇAMENTO-->
<div class="modal fade" id="FormModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="FormModalLabel">Cadastro de Orçamento Suplementar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form class="" action="{{route('orcamentosuplementar.store')}}" method="post">
                            {{ csrf_field() }}
                            @include('erp.orcamentosuplementar._form')
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" >
                    Salvar
                </button>
                <button type="reset" class="btn btn-warning" data-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
        </form>
    </div>
</div>
<!-- end modal form ORÇAMENTO -->

<script>
    function k(i) {
                    var v = i.value.replace(/\D/g,'');
                    v = (v/100).toFixed(2) + '';
                    v = v.replace(".", ".");
                    v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                    v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                    i.value = v;
                }
</script>


@endsection
