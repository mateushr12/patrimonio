<div class="row">
    <div class="form-group col-sm-12">
        <label for="ValorSuplementar">Valor a Suplementar <strong>(R$)</strong>:</label>
        <input type="text" class="form-control InputValor Monetario" onkeyup="k(this);"  name="valor"
            placeholder="0.00" value="{{isset($orc_sup_edit->valor) ? $orc_sup_edit->valor : ''}}" autocomplete="off"
            required />
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-12">
        <label for="OrcamentoInicial">Ação:</label>
        {{-- <strong>{{isset($orc_inicial) ? $orc_inicial : 'Não há nenhum orçamento inicial cadastrado.'}}</strong> --}}
        {{-- <input type="hidden" class="form-control" id="codOrcamento" name="codOrcamento" --}}
        {{-- value="{{isset($orc_inicial) ? $orcid : ''}}" /> --}}
        @if (isset($acoes))
          <select class="form-control" id="codAcoes" name="codAcoes" required>
            <option value="">SELECIONE A AÇÃO</option>
            @foreach($acoes as $acao)
            <option value="{{ $acao->id }}">{{$acao->CodAcao}} - {{$acao->DescAcao}}</option>
            @endforeach
          </select>
        @else
          <span class="form-control" readonly>{{ $orc_sup_edit->acoes->DescAcao }}</span>
        @endif
    </div>
</div>
{{-- <div class="row">
    <div class="form-group col-sm-12">
        <label for="Detalhes">Detalhes:</label>
        <textarea class="form-control" id="detalhes" name="detalhes"
        required>{{isset($orc_sup_edit->detalhes) ? $orc_sup_edit->detalhes : ''}}</textarea>
        <input type="text" class="form-control" id="detalhes" name="detalhes" placeholder=""
            value="{{isset($orc_sup_edit->detalhes) ? $orc_sup_edit->detalhes : ''}}" autocomplete="off" required />
    </div>
</div> --}}
