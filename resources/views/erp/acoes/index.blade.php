@extends('layout_admin.principal')
@section ('breadcrumb','Ações')

@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-10">
                                    <strong>Ações</strong>
                                </div>
                                <div class="col-2">
                                    <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small"
                                        data-toggle="modal" data-target="#FormModal">
                                        <i class="zmdi zmdi-plus"></i>Adicionar
                                    </button>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body card-block">
                            <div class="table-responsive table-responsive-data2">
                                @if(count($acoessalvas) > 0)
                                <table class="table table-data2" id="tabela">
                                    <thead>
                                        <tr>
                                            <th>Ação</th>
                                            <th>Orçamento</th>
                                            <th>Natureza de Despesa</th>
                                            <th>Valor (R$)</th>
                                            <th>Valor Suplementado (R$)</th>
                                            <th>Valor Disponivel (R$)</th>
                                            <th>Valor Empenhado (R$)</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($acoessalvas as $acaosalva)
                                        <tr >
                                            <td>{{ $acaosalva->CodAcao }} - {{$acaosalva->DescAcao}}</td>
                                            <td>{{$acaosalva->Orcamento->especificacao}}</td>
                                            <td>{{$acaosalva->CodGrupAcao}} - {{$acaosalva->grupo->DescGrupo}}</td>
                                            <td>{{ number_format($acaosalva->ValorDestinado,2,',','.') }}</td>
                                            <td>{{ isset($acaosalva->valor) ? number_format($acaosalva->valor,2,',','.') : '0,00' }}</td>
                                            <td>{{ number_format((($acaosalva->ValorDestinado + $acaosalva->valor) - $acaosalva->empenhado),2,',','.') }}</td>
                                            <td><a href="{{ route('erp.acoes.empenhos',$acaosalva->id) }}" target="_blank">{{ number_format($acaosalva->empenhado,2,',','.') }}</a></td>
                                            <td>
                                                <div class="table-data-feature">
                                                  @if (in_array('erp/acoes/editar/{id}', Session::get('permissoes.nomes')))
                                                    <a href="{{route('erp.acoes.editar', $acaosalva->id)}}">
                                                        <button type="button" class="item" title="Editar"
                                                        data-toggle="tooltip" data-placement="top"
                                                        >
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </button>
                                                    </a>&nbsp;
                                                  @else
                                                  @endif
                                                  @if (in_array('erp/acoes/deletar/{id}', Session::get('permissoes.nomes')))
                                                    <a href="{{ route ('erp.acoes.deletar', $acaosalva->id) }}"
                                                        data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                        <button type="button" class="item" title="Deletar"
                                                        data-toggle="tooltip" data-placement="top"
                                                        >
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>
                                                    </a>
                                                  @else
                                                  @endif
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                Nenhuma ação cadastrada.
                                @endif
                            </div>
                            <!-- END DATA TABLE -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- modal form NOVA AÇÃO-->
<div class="modal fade" id="FormModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="FormModalLabel">Cadastro de Ações</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form class="" action="{{route('erp.acoes.salvar')}}" method="post">
                            {{ csrf_field() }}
                            @include('erp.acoes._form')
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" {{isset($especificacao) ? '' : 'disabled' }}>
                    Salvar
                </button>
                <button type="reset" class="btn btn-warning" data-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
        </form>
    </div>
</div>
<!-- end modal form NOVA AÇÃO -->


<div class="modal fade" id="VlModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="FormModalLabel">Valor Suplementado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <ul>
                            <li>valor: 12000</li>
                            <li>valor: 12020</li>
                        {{-- @foreach($vlSupl as $vl)
                        <li>strong>Valor:</strong> R$ {{ $vl->valor }}</li>
                        @endforeach --}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>

$(document).ready( function () {
  $('#tabela').DataTable({
    paging: false,

    language: {
      search: "Buscar:",
      zeroRecords: "Nenhum registro encontrado.",
      info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      infoEmpty: "Mostrando 0 até 0 de 0 registros",
      lengthMenu: "Mostrar _MENU_ entradas",
      infoFiltered: "(filtrado de _MAX_ registros)",
      paginate: {
        first: "Primeiro",
        previous: "Anterior",
        next: "Proximo",
        last: "Ultimo"
      },
      emptyTable: "Nenhum registro encontrado."
    }
  });
});

    function k(i) {
                        var v = i.value.replace(/\D/g,'');
                        v = (v/100).toFixed(2) + '';
                        v = v.replace(".", ",");
                        v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                        v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                        i.value = v;
                    }
</script>

@endsection
