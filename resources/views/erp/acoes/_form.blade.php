<div class="row">
    <div class="form-group col-sm-3">
        <label for="UniGestora">Unidade Gestora:</label>
        <input type="text" class="form-control" id="UniGestora" name="UniGestora" placeholder="" value="192011"
            autocomplete="off" disabled />
    </div>
    <div class="form-group col-sm-9">
        <label for="UniEspecificacao">Especificação:</label>
        <input type="text" class="form-control" id="UniEspecificacao" name="UniEspecificacao" placeholder=""
            value="JUCESE" autocomplete="off" disabled />
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-12">
        <label for="UniGestora">Orçamento Inicial (Atual):</label>
        <input type="text" class="form-control" id="Orcamento" name="Orcamento" placeholder=""
        value="{{isset($especificacao) ? $especificacao : 'Por favor, cadastre ou ative um orçamento.'}}"
            autocomplete="off" disabled />
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-4">
        <label for="FonteAcao">Fontes:</label>
        <select class="form-control" id="CodFonte" name="CodFonte" disabled>
            <option value="33">RECURSO PRÓPRIO</option>
            {{-- <option value="">SELECIONE A FONTE</option>
            @foreach($fontes as $fonte)
            <option {{isset($registro->CodFonte) ? 'selected' : ''}}
                value="{{ isset($registro->CodFonte) ? $registro->CodFonte : $fonte->CodFonte }}">
                {{ $fonte->CodFonte }} - {{$fonte->DescFonte}}
            </option>
            @endforeach --}}
        </select>
    </div>
    <div class="form-group col-sm-8">
        <label for="CatGrupAcao">Categoria e Grupo:</label>
        <select class="form-control" id="CodGrupAcao" name="CodGrupAcao" {{isset($registro->CodGrupAcao) ? 'disabled' : ''}} required>
            <option value="">SELECIONE A CATEGORIA E GRUPO</option>
            @foreach($grupos as $grupo)
            <option {{isset($registro->CodGrupAcao) ? 'selected' : ''}}
                value="{{$grupo->CodGrupo }}">
                {{ $grupo->CodGrupo }} - {{$grupo->DescGrupo}}
            </option>
            @endforeach
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-2">
        <label for="CodAcao">Ação:</label>
        <input type="text" class="form-control" id="CodAcao"
            onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="CodAcao" placeholder=""
            value="{{isset($registro->CodAcao) ? $registro->CodAcao : ''}}" autocomplete="off" required />
    </div>
    <div class="form-group col-sm-10">
        <label for="DescAcao">Descrição da ação:</label>
        <input type="text" class="form-control" id="DescAcao" name="DescAcao"
        onkeyup="this.value = this.value.toUpperCase();" placeholder=""
        value="{{isset($registro->DescAcao) ? $registro->DescAcao : ''}}" autocomplete="off" required />
    </div>
</div>
<div class="row">
    {{-- <div class="form-group col-sm-8">
        <label for="DescElemento">Elemento:</label>
        <select class="form-control" id="CodElemento" name="CodElemento" required>
            <option value="">SELECIONE O ELEMENTO</option>
            @foreach($elementos as $elemento)
            <option {{isset($registro->CodElemento) ? 'selected' : ''}}
                value="{{isset($registro->CodElemento) ? $registro->CodElemento : $elemento->CodElemento}}">
                {{ $elemento->CodElemento }} - {{$elemento->DescElemento}}
            </option>
            @endforeach
        </select>
    </div> --}}
    <div class="form-group col-sm-12">
        <label for="ValorDestinado">Valor destinado<strong> ( R$ )</strong>:</label>
        <input type="text" class="form-control InputValor Monetario" onkeyup="k(this);" id="ValorDestinado"
            {{isset($registro->ValorDestinado) ? 'disabled' : ''}}
            name="ValorDestinado" placeholder="0.00"
            value="{{isset($registro->ValorDestinado) ? number_format($registro->ValorDestinado,2,',','.') : ''}}" autocomplete="off" required />
    </div>
</div>
