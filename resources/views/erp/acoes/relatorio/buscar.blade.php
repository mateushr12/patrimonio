@extends('layout_admin.principal')
@section ('breadcrumb','Ações')

@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-10">
                                    <strong>Buscar Ações</strong>
                                </div>
                            </div>
                        </div>
                        <div class="card-body card-block">
                            <form class="" action="{{route('acao.relatorio.resultado')}}" method="post" target="_blank">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <label for="CodAcao">Codigo da Ação:</label>
                                        <input type="text" class="form-control" name="CodAcao" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <label for="CodOrcamento">Orçamento:</label>
                                        <select class="form-control" name="CodOrcamento" >
                                            <option value="">SELECIONE O ORÇAMENTO</option>
                                            @foreach($orcamento as $key)
                                            <option value="{{$key->id }}">
                                                {{ $key->especificacao }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Buscar</button>
                                <button type="reset" class="btn btn-warning">Limpar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>


<script>
    function k(i) {
                        var v = i.value.replace(/\D/g,'');
                        v = (v/100).toFixed(2) + '';
                        v = v.replace(".", ".");
                        v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                        v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                        i.value = v;
                    }
</script>


@endsection
