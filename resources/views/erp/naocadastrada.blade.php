@extends('layout_admin.principal')
@section ('breadcrumb','Permissão não cadastrada')
@section('conteudo')
  <section>
          <div class="flex-center position-ref full-height">
              <div class="content">
                  <div class="title m-b-md">
                      <h4>PERMISSÃO NÃO CADASTRADA, ENTRE EM CONTATO COM O ADMINISTRADOR.</h4>
                  </div>
              </div>
          </div>
  </section>

  <style>
  .flex-center {
      align-items: center;
      display: flex;
      justify-content: center;
  }

  .content {
      color: #f0f0f0;
      font-family: 'Nunito', sans-serif;
      font-weight: 200;
      text-align: center;
  }
  </style>

@endsection
