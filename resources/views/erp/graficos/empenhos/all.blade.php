@extends('layout_admin.principal')
@section ('breadcrumb','Gráficos')

@section('conteudo')
  <style>
    /* g.highcharts-exporting-group{ display: none;}
    .highcharts-credits{ display: none;} */
  </style>

  <script src="https://code.highcharts.com/modules/offline-exporting.js"></script>
  <script src="https://code.highcharts.com/8.0.0/lib/jspdf.js"></script>
  <script src="https://code.highcharts.com/8.0.0/lib/svg2pdf.js"></script>
  <script src="https://code.highcharts.com/8.0.0/lib/rgbcolor.js"></script>

    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-9">
                                      <h4>Total empenhado e pago por mês</h4>
                                    </div>
                                    <div class="col-3">
      	                               <button id="export-pdf" class="btn btn-primary">Exportar para PDF</button>
                                     </div>
                                </div>
                            </div>
                            <img src="../../images/gov-jucese-top-relatorio.jpg" hidden>
                            <div class="card-body card-block">
                              <div id="graficoEmp" style="min-width: 300px; height: 400px; margin: 0 auto; border: 1px solid #ccc"></div>
                              <br>
                              <div id="graficoPg" style="min-width: 300px; height: 500px; margin: 0 auto; border: 1px solid #ccc"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <script>



    $(function() {
      var chart;

      $(document).ready(function() {
        chart = new Highcharts.Chart({
          exporting: {
            enabled: false
          },
          credits:{
            enabled: false
          },
          chart: {
            renderTo: 'graficoEmp',
            type: 'column'
          },
          title: {
            text: '<b>Total Empenhado</b>'
          },
          xAxis: {
            categories: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
            labels: {
              enabled: true
            }
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Total Empenhado (R$)'
            }
          },
          plotOptions: {
            column: {
              pointPadding: 0,
              borderWidth: 0
            }
          },
          tooltip: {
            formatter: function() {
              return this.x + ': R$ ' + new Intl.NumberFormat('de-DE').format(this.y);
            }
          },
          series: [{
            type: 'column',
            name: '2019',
            data: <?php echo json_encode($somaEmp[0], JSON_NUMERIC_CHECK) ?>,
          },
          {
            type: 'column',
            name: '2020',
            data: <?php echo json_encode($somaEmp[1], JSON_NUMERIC_CHECK) ?>,
          },
          {
            type: 'column',
            name: '2021',
            data: <?php echo json_encode($somaEmp[2], JSON_NUMERIC_CHECK) ?>,
          },
          {
            type: 'column',
            name: '2022',
            data: <?php echo json_encode($somaEmp[3], JSON_NUMERIC_CHECK) ?>,
          }
        ]
        });
      });
    });
///////////////////////////////////////////////////////////////////////////////
    $(function() {
      var chart;

      $(document).ready(function() {
        chart = new Highcharts.Chart({
          exporting: {
            enabled: false
          },
          credits:{
            enabled: false
          },
          chart: {
            renderTo: 'graficoPg',
            type: 'column'
          },
          title: {
            text: '<b>Total de Empenhos Pagos</b>'
          },
          xAxis: {
            categories: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
            labels: {
              enabled: true
            }
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Total Pago (R$)'
            }
          },
          plotOptions: {
            column: {
              pointPadding: 0,
              borderWidth: 0
            }
          },

          tooltip: {
            formatter: function() {
              return this.x + ': R$ ' + new Intl.NumberFormat('de-DE').format(this.y);
            }
          },
          series: [{
            type: 'column',
            name: '2019',
            data: <?php echo json_encode($somaPg[0], JSON_NUMERIC_CHECK) ?>
          },
          {
            type: 'column',
            name: '2020',
            data: <?php echo json_encode($somaPg[1], JSON_NUMERIC_CHECK) ?>
          },
          {
            type: 'column',
            name: '2021',
            data: <?php echo json_encode($somaPg[2], JSON_NUMERIC_CHECK) ?>
          },
          {
            type: 'column',
            name: '2022',
            data: <?php echo json_encode($somaPg[3], JSON_NUMERIC_CHECK) ?>
          }
        ]
        });
      });

    });

    ////////////////////////////////////////////////
    function dataAt(){
        var data = new Date(),
            dia  = data.getDate().toString().padStart(2, '0'),
            mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
            ano  = data.getFullYear();
        return dia+"/"+mes+"/"+ano;
    }

    document.getElementById("export-pdf").onclick = function() {
    var svg = document.getElementById("graficoEmp").querySelector("svg");
    var pdf = new jsPDF('l', 'pt', 'a4');
    var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"

    svg2pdf(svg, pdf, {
      xOffset: 40,
      yOffset: 180,
      scale: 0.69
    });

    pdf.setFontSize(18)
    pdf.setTextColor(40)
    pdf.setFontType("bold")
    pdf.text('GRÁFICOS DE EMPENHOS', 320, 130)
    pdf.setFontSize(10)
    pdf.text('Data: ' + dataAt(), 398, 145)

    var img = new Image();
    img.src = base64Img;
    pdf.addImage(img, 'JPEG', 40, 15, 765, 86)
    //Rodape
    var str = 'Pág. ' + pdf.internal.getNumberOfPages()
    pdf.setFontSize(9)
    var pageSize = pdf.internal.pageSize
    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
    pdf.text(str, 780, pageHeight - 35)
    pdf.setFontType("bold");
    pdf.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', 328, pageHeight - 40)
    pdf.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br',  340, pageHeight - 30)
    ///

    pdf.addPage("a4", "l");
    //pdf.addImage(base64Img, 'JPEG', 40, 15, 765, 86)
    var svg1 = document.getElementById("graficoPg").querySelector("svg");
    svg2pdf(svg1, pdf, {
      xOffset: 40,
      yOffset: 150,
      scale: 0.69
    });
    pdf.addImage(img, 'JPEG', 40, 15, 765, 86)
    //Rodape
    var str = 'Pág. ' + pdf.internal.getNumberOfPages()
    pdf.setFontSize(9)
    var pageSize = pdf.internal.pageSize
    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
    pdf.text(str, 780, pageHeight - 35)
    pdf.setFontType("bold");
    pdf.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', 328, pageHeight - 40)
    pdf.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br',  340, pageHeight - 30)
    ///

    pdf.save("graficosEmpenhos.pdf");
    };

    </script>


@endsection
