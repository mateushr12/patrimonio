@extends('layout_admin.principal')
@section ('breadcrumb','Gráficos')

@section('conteudo')
    <style>
      /* g.highcharts-exporting-group{ display: none;}
      .highcharts-credits{ display: none;} */
    </style>

    <script src="https://code.highcharts.com/modules/offline-exporting.js"></script>
    <script src="https://code.highcharts.com/8.0.0/lib/jspdf.js"></script>
    <script src="https://code.highcharts.com/8.0.0/lib/svg2pdf.js"></script>
    <script src="https://code.highcharts.com/8.0.0/lib/rgbcolor.js"></script>

    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-9">
                                      <h4>Resultado: {{ $data }}</h4>
                                    </div>
                                    <div class="col-3">
      	                               <button id="export-pdf" class="btn btn-primary">Exportar para PDF</button>
                                     </div>
                                </div>
                            </div>
                            <img src="../../images/gov-jucese-top-relatorio.jpg" hidden>
                            <div class="card-body card-block">
                                <div id="pizza" style="min-width: 300px; height: 400px; margin: 0 auto; border: 1px solid #ccc"></div>
                                <br>
                                {{-- <div id="pizzateste" style="min-width: 300px; height: 400px; margin: 0 auto; border: 1px solid #ccc"></div>
                                <br> --}}
                                <div id="graficoteste" style="min-width: 300px; height: 500px; margin: 0 auto; border: 1px solid #ccc"></div>
                                <br>
                                <div id="grafico" style="min-width: 300px; height: 500px; margin: 0 auto; border: 1px solid #ccc"></div>
                                <br>
                                <div id="container" style="min-width: 300px; height: 500px; margin: 0 auto; border: 1px solid #ccc"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <script>

    $(function() {
      var chart;
      var categories = <?php echo json_encode($tipo) ?>;
      $(document).ready(function() {
        chart = new Highcharts.Chart({
          exporting: {
            enabled: false
          },
          credits:{
            enabled: false
          },
          chart: {
            renderTo: 'container',
            type: 'column'
          },
          title: {
            text: '<b>Total de taxas pagas por Ato</b>'
          },
          xAxis: {
            categories: categories,
            labels: {
              enabled: true
            }
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Total de taxas pagas (R$)'
            }
          },
          plotOptions: {
            series: {
              dataLabels: {
                formatter: function() {
                  return 'R$ ' + new Intl.NumberFormat('de-DE').format(this.y);
                },
                enabled: true,
                align: 'right',
                color: 'black',
                shadow: false,
                x: -10,
                style: {
                  "fontSize": "15px",
                  "textShadow": "0px"
                }
              },
              pointPadding: 0.1,
              groupPadding: 0
            }
          },

          tooltip: {
            formatter: function() {
              return this.x + ': R$ ' + new Intl.NumberFormat('de-DE').format(this.y);
            }
          },
          series: [{
            type: 'column',
            name: 'Total de taxas pagas (R$)',
            data: <?php echo json_encode($soma, JSON_NUMERIC_CHECK) ?>,
            stacking: 'normal'
          }]
        });
      });
    });
///////////////////////////////////////////////////////////////////////////////

$(function() {
  var chart;
  var categories = <?php echo json_encode(array_column($nr,0)) ?>;
  $(document).ready(function() {
    chart = new Highcharts.Chart({
      exporting: {
        enabled: false
      },
      credits:{
        enabled: false
      },
      chart: {
        renderTo: 'graficoteste',
        type: 'bar'
      },
      title: {
        text: '<b>Qtd. de taxas por eventos NÃO REDESIM</b>'
      },
      colors: ['#c3792c'],
      xAxis: {
        categories: categories,
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Qtd. de eventos não REDESIM'
        }
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
          dataLabels: {
            formatter: function() {
              return new Intl.NumberFormat('de-DE').format(this.y);
            },
            enabled: true,
            align: 'right',
            color: 'black',
            shadow: false,
            x: -10,
            style: {
              "fontSize": "15px",
              "textShadow": "0px"
            }
          },
          pointPadding: 0.1,
          groupPadding: 0
        }
      },

      tooltip: {
        formatter: function() {
          return this.x + ': ' + new Intl.NumberFormat('de-DE').format(this.y);
        }
      },
      series: [{
        type: 'column',
        name: 'Total de eventos não REDESIM',
        data: <?php echo json_encode(array_column($nr,1), JSON_NUMERIC_CHECK) ?>,
        stacking: 'normal'
      }]
    });
  });
});

///////////////////////////////////////////////////////////////////////////////
    $(function() {
      var chart;
      var categories = <?php echo json_encode(array_column($taxa,0)) ?>;
      $(document).ready(function() {
        chart = new Highcharts.Chart({
          exporting: {
            enabled: false
          },
          credits:{
            enabled: false
          },
          chart: {
            renderTo: 'grafico',
            type: 'column'
          },
          title: {
            text: '<b>Quantidade de taxas por Ato</b>'
          },
          colors: ['#78ff41'],
          xAxis: {
            categories: categories,
            labels: {
              enabled: true
            }
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Qtd. de taxas'
            }
          },
          plotOptions: {
            series: {
              dataLabels: {
                formatter: function() {
                  return new Intl.NumberFormat('de-DE').format(this.y);
                },
                enabled: true,
                align: 'right',
                color: 'black',
                shadow: false,
                x: -10,
                style: {
                  "fontSize": "15px",
                  "textShadow": "0px"
                }
              },
              pointPadding: 0.1,
              groupPadding: 0
            }
          },

          tooltip: {
            formatter: function() {
              return this.x + ': ' + this.y;
            }
          },
          series: [{
            type: 'column',
            name: 'Quantidade de taxas',
            data: <?php echo json_encode(array_column($taxa,1), JSON_NUMERIC_CHECK) ?>,
            stacking: 'normal'
          }]
        });
      });

    });
    ///////////////////////////////////////////////////////////////////////////

    $(function() {
      var chart;
      $(document).ready(function() {
        chart = new Highcharts.Chart({
          exporting: {
            enabled: false
          },
          credits:{
            enabled: false
          },
          chart: {
            renderTo: 'pizza',
            type: 'pie'
          },
          title: {
            text: '<b>Quantidade de Taxas Por Atos</b>'
          },
          subtitle: {
            text: '<b>* O Ato do tipo ALTERAÇÃO pode conter os eventos: Procuração, Consolidação, Emancipação, Rerratificação </b>'
          },
          plotOptions: {
            pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  style: {
                    "fontSize": "15px"
                  }
              }
            }
          },

          tooltip: {
            formatter: function() {
              return  this.y;
            }
          },
          series: [{
            // type: 'column',

            data: <?php echo json_encode($taxa, JSON_NUMERIC_CHECK) ?>,
            // stacking: 'normal'
          }]
        });
      });

    });
    ////////////////////////////////////////////////////////////////////////////////
    $(function() {
      var chart;
      $(document).ready(function() {
        chart = new Highcharts.Chart({
          exporting: {
            enabled: false
          },
          credits:{
            enabled: false
          },
          chart: {
            renderTo: 'pizzateste',
            type: 'pie'
          },
          title: {
            text: '<b>Quantidade de Taxas Por Eventos Não Redesim</b>'
          },
          plotOptions: {
            pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  style: {
                    "fontSize": "15px"
                  }
              }
            }
          },

          tooltip: {
            formatter: function() {
              return  this.y;
            }
          },
          series: [{
            // type: 'column',

            data: <?php echo json_encode($nr, JSON_NUMERIC_CHECK) ?>,
            // stacking: 'normal'
          }]
        });
      });

    });

    ////////////////////////////////////////////////
    function dataAt(){
        var data = new Date(),
            dia  = data.getDate().toString().padStart(2, '0'),
            mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
            ano  = data.getFullYear();
        return dia+"/"+mes+"/"+ano;
    }

    document.getElementById("export-pdf").onclick = function() {
    var svg = document.getElementById("pizza").querySelector("svg");
    var pdf = new jsPDF('l', 'pt', 'a4');
    var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"

    svg2pdf(svg, pdf, {
      xOffset: 75,
      yOffset: 180,
      scale: 0.69
    });

    pdf.setFontSize(18)
    pdf.setTextColor(40)
    pdf.setFontType("bold")
    pdf.text('GRÁFICOS DE TAXAS POR EVENTO', 280, 130)
    pdf.setFontSize(10)
    pdf.text('Data: ' + dataAt(), 390, 145)
    var img = new Image()
    img.src = base64Img
    pdf.addImage(img, 'JPEG', 40, 15, 765, 86)
    //Rodape
    var str = 'Pág. ' + pdf.internal.getNumberOfPages()
    pdf.setFontSize(9)
    var pageSize = pdf.internal.pageSize
    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
    pdf.text(str, 780, pageHeight - 35)
    pdf.setFontType("bold");
    pdf.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', 328, pageHeight - 40)
    pdf.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br',  340, pageHeight - 30)

    //////////////
    pdf.addPage("a4", "l");
    //pdf.addImage(base64Img, 'JPEG', 40, 15, 765, 86)
    var svg1 = document.getElementById("graficoteste").querySelector("svg");
    svg2pdf(svg1, pdf, {
      xOffset: 45,
      yOffset: 150,
      scale: 0.69
    });
    pdf.addImage(img, 'JPEG', 40, 15, 765, 86)
    //Rodape
    var str = 'Pág. ' + pdf.internal.getNumberOfPages()
    pdf.setFontSize(9)
    var pageSize = pdf.internal.pageSize
    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
    pdf.text(str, 780, pageHeight - 35)
    pdf.setFontType("bold");
    pdf.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', 328, pageHeight - 40)
    pdf.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br',  340, pageHeight - 30)
    /////////////////

    //////////////
    pdf.addPage("a4", "l");
    //pdf.addImage(base64Img, 'JPEG', 40, 15, 765, 86)
    var svg1 = document.getElementById("grafico").querySelector("svg");
    svg2pdf(svg1, pdf, {
      xOffset: 45,
      yOffset: 150,
      scale: 0.69
    });
    pdf.addImage(img, 'JPEG', 40, 15, 765, 86)
    //Rodape
    var str = 'Pág. ' + pdf.internal.getNumberOfPages()
    pdf.setFontSize(9)
    var pageSize = pdf.internal.pageSize
    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
    pdf.text(str, 780, pageHeight - 35)
    pdf.setFontType("bold");
    pdf.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', 328, pageHeight - 40)
    pdf.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br',  340, pageHeight - 30)
    /////////////////

    //////////////
    pdf.addPage("a4", "l");
    //pdf.addImage(base64Img, 'JPEG', 40, 15, 765, 86)
    var svg1 = document.getElementById("container").querySelector("svg");
    svg2pdf(svg1, pdf, {
      xOffset: 45,
      yOffset: 150,
      scale: 0.69
    });
    pdf.addImage(img, 'JPEG', 40, 15, 765, 86)
    //Rodape
    var str = 'Pág. ' + pdf.internal.getNumberOfPages()
    pdf.setFontSize(9)
    var pageSize = pdf.internal.pageSize
    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
    pdf.text(str, 780, pageHeight - 35)
    pdf.setFontType("bold");
    pdf.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', 328, pageHeight - 40)
    pdf.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br',  340, pageHeight - 30)
    /////////////////

    pdf.save("graficosTaxaEventos.pdf");
    };

    </script>

@endsection
