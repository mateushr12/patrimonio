@extends('layout_admin.principal')
@section ('breadcrumb','Gráficos')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-10">
                                      <h4>Resultado: {{ $data }}</h4>
                                    </div>
                                    {{-- <div class="col-2">
                                        <a class="au-btn au-btn-icon au-btn--green au-btn--small"
                                            href="#" target="_blank">
                                            <i class="zmdi zmdi-print"></i>Imprimir</a>
                                    </div> --}}
                                </div>
                            </div>
                            {{-- style=" height: 800px; overflow-y: scroll; --}}
                            <div class="card-body card-block">
                                <div id="even_grafico">
                                  @piechart('eve', 'even_grafico')
                                </div>
                                <div id="barra1">
                                  @barchart('evet', 'barra1')
                                </div>
                                <div id="barra">
                                  @barchart('eve', 'barra')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>


@endsection
