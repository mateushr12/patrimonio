@extends('layout_admin.principal')
@section ('breadcrumb','Gráficos')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Gráficos das Taxas Recebidas</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">
                                <form action="{{route('graficos.taxasmes.resultado')}}" method="get" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-12 form-group">
                                                    <label for="ano">Ano:</label>
                                                    <select name="ano" class="form-control"  required>
                                                      <option value="">Selecione uma opção</option>
                                                      @foreach($ano as $key)
                                                      <option value="{{ $key->ano }}">{{ $key->ano }}</option>
                                                      @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 form-group ">
                                            <button type="submit" class="btn btn-primary">Gerar</button>
                                            <button type="reset" class="btn btn-warning">Limpar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
