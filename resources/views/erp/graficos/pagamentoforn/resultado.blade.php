@extends('layout_admin.principal')
@section ('breadcrumb','Gráficos')

@section('conteudo')
  <script src="https://code.highcharts.com/modules/offline-exporting.js"></script>
  <script src="https://code.highcharts.com/8.0.0/lib/jspdf.js"></script>
  <script src="https://code.highcharts.com/8.0.0/lib/svg2pdf.js"></script>
  <script src="https://code.highcharts.com/8.0.0/lib/rgbcolor.js"></script>
    <style>
      /* g.highcharts-exporting-group{ display: none;}
      .highcharts-credits{ display: none;} */
    </style>
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-9">
                                      <h4>Pagamentos Por Fornecedor</h4>
                                    </div>
                                    <div class="col-3">
                                       <button id="export-pdf" class="btn btn-primary">Exportar para PDF</button>
                                     </div>
                                </div>
                            </div>
                            <img src="../../images/gov-jucese-top-relatorio.jpg" hidden>
                            <div class="card-body card-block">
                                <div id="grafico" style="min-width: 300px; height: 400px; margin: 0 auto; border: 1px solid #ccc"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <script>

    $(function() {
      var chart;
      var categories = <?php echo json_encode($data) ?>;
      $(document).ready(function() {
        chart = new Highcharts.Chart({
          exporting: {
            enabled: false
          },
          credits:{
            enabled: false
          },
          chart: {
            renderTo: 'grafico',
            type: 'column'
          },
          title: {
            text: '<b>Fornecedor/Credor: </b>{{ $nome }}'
          },
          xAxis: {
            categories: categories,
            labels: {
              enabled: true
            }
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Total pago (R$)'
            }
          },
          plotOptions: {
            series: {
              dataLabels: {
                formatter: function() {
                  return 'R$ ' + new Intl.NumberFormat('de-DE').format(this.y);
                },
                enabled: true,
                align: 'right',
                color: 'black',
                shadow: false,
                x: -10,
                style: {
                  "fontSize": "12px",
                  "textShadow": "0px"
                }
              },
              pointPadding: 0.1,
              groupPadding: 0
            }
          },
          tooltip: {
            formatter: function() {
              return this.x + ': R$ ' + new Intl.NumberFormat('de-DE').format(this.y);
            }
          },
          series: [{
            type: 'column',
            name: 'SOMA: R$ ' + new Intl.NumberFormat('de-DE').format(<?php echo json_encode($soma) ?>),
            data: <?php echo json_encode($valores, JSON_NUMERIC_CHECK) ?>,
            stacking: 'normal'
          }]
        });
      });
    });
///////////////////////////////////////////////////////////////////////////////
function dataAt(){
    var data = new Date(),
        dia  = data.getDate().toString().padStart(2, '0'),
        mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
        ano  = data.getFullYear();
    return dia+"/"+mes+"/"+ano;
}

document.getElementById("export-pdf").onclick = function() {
var svg = document.getElementById("grafico").querySelector("svg");
var pdf = new jsPDF('l', 'pt', 'a4');
var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"

svg2pdf(svg, pdf, {
  xOffset: 20,
  yOffset: 190,
  scale: 0.69
});

pdf.setFontSize(18)
pdf.setTextColor(40)
pdf.setFontType("bold")
pdf.text('GRÁFICO DE FORNECEDOR', 320, 130)
pdf.setFontSize(10)
pdf.text('Data: ' + dataAt(), 398, 145)

var img = new Image()
img.src = base64Img
pdf.addImage(img, 'JPEG', 40, 15, 765, 86)
//Rodape
var str = 'Pág. ' + pdf.internal.getNumberOfPages()
pdf.setFontSize(9)
var pageSize = pdf.internal.pageSize
var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
pdf.text(str, 780, pageHeight - 35)
pdf.setFontType("bold");
pdf.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', 328, pageHeight - 40)
pdf.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br',  340, pageHeight - 30)
///


pdf.save("graficosFornecedores.pdf");
};


    </script>

@endsection
