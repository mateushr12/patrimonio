@extends('layout_admin.principal')
@section ('breadcrumb','Gráficos')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Gráficos das Pagamentos por Fornecedor</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">
                                <form action="{{route('graficos.pagForn.resultado')}}" method="get" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-12 form-group">
                                                    <label for="fornecedor">Fornecedor:</label>
                                                    <select name="fornecedor" class="form-control" required >
                                                      <option value="">Selecione uma opção</option>
                                                      <option value="credor">(Credor) JUCESE</option>
                                                      @foreach($fornecedores as $key)
                                                      <option value="{{ $key->id }}">{{ $key->nome }}</option>
                                                      @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 form-group ">
                                            <button type="submit" class="btn btn-primary">Gerar</button>
                                            <button type="reset" class="btn btn-warning">Limpar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
