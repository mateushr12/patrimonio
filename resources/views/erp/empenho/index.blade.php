@extends('layout_admin.principal')
@section ('breadcrumb','Empenho')
@section('conteudo')

<style>
  #ob{
    color: red;
  }
</style>

<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6">
                                    <strong>Empenhos</strong>
                                </div>
                                <div class="col-6 text-right">
                                    <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small"
                                            data-toggle="modal" data-target="#FormCota">
                                        <i class="zmdi zmdi-plus"></i>Adicionar
                                    </button>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="table-responsive-sm table-responsive-data2">
                                      {{-- class="table table-data2" --}}
                                      <table class="table " id="tabela">
                                          <thead >
                                              <tr >
                                                  <th >Nº Empenho</th>
                                                  <th>Data do Empenho</th>
                                                  <th>Tipo</th>
                                                  <th>Contrato</th>
                                                  <th>Valor (R$)</th>
                                                  <th>Valor à pagar (R$)</th>
                                                  <th >Valor Pago (R$)</th>
                                                  <th ></th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              @foreach ($registros as $registro)
                                              <tr>
                                                <td>{{ $registro->numero }}</td>
                                                <td>{{\Carbon\Carbon::parse($registro->dataempenho)->format('d/m/Y')}}</td>
                                                <td>
                                                  @if ($registro->tipoempenho == 1)
                                                    Empenho
                                                  @else
                                                    Reforço
                                                  @endif
                                                </td>
                                                <td>
                                                  @if (isset($registro->licempenho))
                                                    <a href="{{ route('contratos.ver', $registro->licempenho) }}" target="_blank">
                                                      {{ $registro->contrato->numerocontrato }} - {{ $registro->contrato->fornecedor->nome }}
                                                    </a>
                                                  @else
                                                    -
                                                  @endif
                                                </td>
                                                <td>{{ number_format($registro->valorempenho,2,',','.') }}</td>
                                                <td>{{ number_format(($registro->valorempenho - $registro->totapago),2,',','.') }}</td>
                                                <td>{{ number_format($registro->totapago,2,',','.') }}</td>
                                                <td>
                                                    <div class="table-data-feature">
                                                        <a href="{{ route('empenho.pdf', $registro->id) }}" target="_blank">
                                                            <button type="button" class="item" title="Imprimir"
                                                            data-toggle="tooltip" data-placement="top" >
                                                                <i class="zmdi zmdi-eye"></i>
                                                            </button>
                                                        </a>&nbsp;
                                                      @if (in_array('erp/empenho/editar/{id}', Session::get('permissoes.nomes')))
                                                        <a href="{{route('empenho.editar' , $registro->id)}}">
                                                            <button type="button" class="item" title="Editar"
                                                            data-toggle="tooltip" data-placement="top" >
                                                                <i class="zmdi zmdi-edit"></i>
                                                            </button>
                                                        </a>&nbsp;
                                                      @else
                                                      @endif
                                                      @if (in_array('erp/empenho/deletar/{id}', Session::get('permissoes.nomes')))
                                                        <a href="{{route('empenho.deletar' , $registro->id)}}"
                                                            data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                            <button type="button" class="item" title="Deletar"
                                                            data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="Deletar">
                                                                <i class="zmdi zmdi-delete"></i>
                                                            </button>
                                                        </a>&nbsp;
                                                      @else
                                                      @endif
                                                        {{-- @if ($registro->liquidado == 1 or $registro->liquidado == 3) --}}
                                                        {{-- <a href="{{route('liquidar' , $registro->id)}}">
                                                            <button type="button" class="item" title="Liquidar"
                                                            data-toggle="tooltip" data-placement="top" >
                                                                <i class="zmdi zmdi-badge-check"></i>
                                                            </button>
                                                        </a>
                                                        &nbsp; --}}
                                                        {{-- @else
                                                        @endif --}}
                                                        @if ($registro->tipoempenho == 1 && $registro->modempenho != 1)
                                                          @if (in_array('erp/empenho/reforco/{id}', Session::get('permissoes.nomes')))
                                                            <a href="{{route('empenho.reforco' , $registro->id)}}">
                                                                <button type="button" class="item" title="Reforço"
                                                                data-toggle="tooltip" data-placement="top" title="">
                                                                    <i class="zmdi zmdi-collection-plus"></i>
                                                                </button>
                                                            </a>
                                                          @else
                                                          @endif
                                                        @else
                                                        @endif
                                                        &nbsp;
                                                      @if (in_array('erp/empenho/pagamento/{id}', Session::get('permissoes.nomes')))
                                                        <a href="{{route('pagamento' , $registro->id)}}">
                                                            <button type="button" class="item" title="Pagar"
                                                            data-toggle="tooltip" data-placement="top" >
                                                                <i class="zmdi zmdi-money-box"></i>
                                                            </button>
                                                        </a>
                                                      @else
                                                      @endif
                                                    </div>
                                                </td>
                                              </tr>
                                              @endforeach
                                          </tbody>
                                      </table>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="FormCota" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="FormModalLabel">Cadastro de Empenho</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form class="" action="{{route('empenho.salvar')}}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="tipoempenho" value="1" />
                            @include('erp.empenho.form')
                    </div>
                </div>
                <span id="ob">*</span> campo obrigatório
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Salvar
                </button>
                <button class="btn btn-warning" type="reset" >
                    Limpar
                </button>
            </div>
        </div>
        </form>
    </div>
</div>




<script>

    $(document).ready( function () {
      $('#tabela').DataTable({
        paging: false,
        scrollY: 600,
        language: {
          search: "Buscar:",
          zeroRecords: "Nenhum registro encontrado.",
          info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          infoEmpty: "Mostrando 0 até 0 de 0 registros",
          lengthMenu: "Mostrar _MENU_ entradas",
          infoFiltered: "(filtrado de _MAX_ registros)",
          paginate: {
            first: "Primeiro",
            previous: "Anterior",
            next: "Proximo",
            last: "Ultimo"
          },
          emptyTable: "Nenhum registro encontrado."
        }
      });
    });

    function k(i) {
                var v = i.value.replace(/\D/g,'');
                v = (v/100).toFixed(2) + '';
                v = v.replace(".", ",");
                v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                i.value = v;
            }


</script>

@endsection
