@extends('layout_admin.principal')
@section ('breadcrumb','Contas à Pagar')
@section('conteudo')
  <section>
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-6">
                    <strong>Contas a Pagar por Elemento/Subelemento</strong>
                  </div>
                  <div class="col-6 text-right">
                    <a class="au-btn au-btn-icon au-btn--green au-btn--small"
                    href="{{ route('imprimirporelem') }}" target="_blank">
                    <i class="zmdi zmdi-plus"></i>Imprimir</a>
                  </div>
                </div>
              </div>
              @if (session('erro'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  {{session('erro')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              @endif
              <div class="card-body">
                <div class="row">
                  <div class="col-12">
                    <div class="table-responsive table-responsive-data2">
                      <table class="table table-data2" id="tabela">
                        <thead>
                          <tr>
                            <th>Elemento</th>
                            <th>Subelemento</th>
                            <th>Total Empenhado (R$)</th>
                            <th>Total Pago (R$)</th>
                            <th>Valor à pagar (R$)</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tbody>
                            @foreach ($contas as $conta)
                              <tr>
                                <td>{{ $conta->CodElemento }} - {{ $conta->DescElemento }}</td>
                                <td>{{ $conta->codnatureza }} - {{ $conta->descricao }}</td>
                                <td>{{ number_format($conta->total,2,',','.') }}</td>
                                <td>{{ number_format($conta->totalPg,2,',','.') }}</td>
                                <td>{{ number_format(($conta->total - $conta->totalPg),2,',','.') }}</td>
                                <span hidden>{{ $stotal[] = $conta->total  }}</span>
                                <span hidden>{{ $stotalPg[] = $conta->totalPg  }}</span>
                                <span hidden>{{ $sapagar[] = ($conta->total - $conta->totalPg) }}</span>
                              </tr>
                            @endforeach
                            <tr>
                              <td colspan="2"><strong>TOTAL</strong></td>
                              <td>{{ number_format(array_sum($stotal),2,',','.') }}</td>
                              <td>{{ number_format(array_sum($stotalPg),2,',','.') }}</td>
                              <td>{{ number_format(array_sum($sapagar),2,',','.') }}</td>
                            </tr>
                          </tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script>
  function k(i) {
    var v = i.value.replace(/\D/g,'');
    v = (v/100).toFixed(2) + '';
    v = v.replace(".", ",");
    v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
    v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
    i.value = v;
  }
</script>

@endsection
