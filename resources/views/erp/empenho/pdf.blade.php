<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="JUCESE - Junta Comercial do Estado de Sergipe">

    <!-- Title Page-->
    <title>ERP - JUCESE</title>

    <link href="<?php echo asset('vendor/bootstrap-4.1/bootstrap.min.css')?>" rel="stylesheet" media="all">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@3.5.9/dist/jspdf.plugin.autotable.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

    <style>
      @media print {
        .btn {
          display: none !important;
        }
      }
      #btnPDF{
        background: #2579e6;
        color: white;
        font-weight: bold;
        width: 150px;
        margin-top: 5px;
      }
      #btnPDF:hover{
        background: #0439f1
      }
    </style>


</head>

<body>

<div class="container">

  <button type="button" class="btn" id="btnPDF">Gerar PDF</button><br>
  <img src="../../../images/gov-jucese-top-relatorio.jpg" hidden>
  <div class="table-responsive table-responsive-data2">
      <table style="width: 940px">
        <thead >
          <tr>
            <td style=" font-size: 24px;" ><strong>NOTA DE EMPENHO</strong><p></p></td>
            <td style="font-size: 14px; float: right;">Data: {{ date('d/m/Y') }}</td>
          </tr>
        </thead>
        <tbody>
        <tr><td colspan="2">

          <table class="table table-data2" id="tabela" border="1">
              <tbody>
                <tr>
                  <td><strong>Nº Empenho:</strong><br>{{ $empenho->numero }}</td>
                  <td><strong>Banco de Preço:</strong><br> @if ($empenho->bancoprecoempenho == 1) Sim @elseif ($empenho->bancoprecoempenho == 2) Não @endif</td>
                  <td><strong>Data do Empenho:</strong><br> {{ \Carbon\Carbon::parse($empenho->dataempenho)->format('d/m/Y') }}</td>
                  <td><strong>Urgência:</strong><br> @if ($empenho->urgempenho == 1) Sim @elseif ($empenho->urgempenho == 2) Não @endif</td>
                </tr>
                @if (isset($empenho->tipoempenho) && $empenho->tipoempenho == 2)
                  <tr>
                    <td colspan="2"><strong>Tipo de Empenho:</strong><br> Reforço</td>
                    <td ><strong>Data Empenho Pai:</strong><br> {{ \Carbon\Carbon::parse($empenho->dt_pai)->format('d/m/Y') }}</td>
                    <td ><strong>Nº Empenho Pai:</strong><br> {{ $empenhopai->numero }}</td>
                  </tr>
                @else
                @endif
                <tr>
                  <td colspan="2">
                    <strong>Modalidade:</strong><br> @if ($empenho->modempenho == 1) Ordinário @elseif ($empenho->modempenho == 2) Estimativo @elseif ($empenho->modempenho == 3) Global @endif
                  </td>
                  <td><strong>Modalidade de Licitação:</strong><br> {{ $empenho->modalidadelic->descricao }} </td>
                  <td><strong>Tipo de despesa:</strong><br>
                    @switch($empenho->despempenho)
                      @case(1) 1 - NORMAL @break
                      @case(2) 2 - REPASSE FINANCEIRO @break
                      @case(3) 3 - SUPRIMENTO INDIVIDUAL @break
                      @case(4) 4 - SUPRIMENTO INSTITUCIONAL @break
                      @case(5) 5 - RETITUIÇÕES DE TRIBUTOS @break
                      @case(6) 6 - TRANSFERÊNCIAS VOLUNTÁRIAS @break
                      @case(7) 7 - FOLHA DE PAGAMENTO @break
                    @endswitch
                  </td>
                </tr>
                <tr>
                  <td colspan="2"><strong>Célula Orçamentária:</strong> <br> {{ $empenho->acao->CodAcao }} - {{ $empenho->acao->DescAcao }}</td>
                  <td colspan="2"><strong>Ficha Financeira:</strong><br> {{ $empenho->fichaempenho }}</td>
                </tr>
                <tr>
                  <td colspan="2"><strong>Elemento de Despesa:</strong><br> {{ $empenho->elemento->CodElemento }} - {{ $empenho->elemento->DescElemento }}</td>
                  <td colspan="2"><strong>Sub-Elemento de Despesa:</strong><br> {{ $empenho->subelementos->codnatureza }} - {{ $empenho->subelementos->descricao }}</td>
                </tr>
                <tr>
                  <td colspan="2"><strong>Convênio:</strong><br> {{$empenho->convempenho}}</td>
                  <td colspan="2"><strong>Valor da Solicitação:</strong><br>R$ {{ number_format($empenho->valorempenho,2,',','.') }}</td>
                </tr>
                <tr>
                  <td colspan="2"><strong>Licitação:</strong><br> {{isset($empenho->contrato->licitacao) ? $empenho->contrato->licitacao : '-'}}</td>
                  <td colspan="2"><strong>Origem do Protocolo:</strong><br>
                    @switch($empenho->protoempenho)
                      @case(1) i-Gesp @break
                      @case(2) Outros @break
                      @default -
                    @endswitch
                  </td>
                </tr>
                <tr>
                  <td colspan="2"><strong>Credor:</strong><br>
                    @if ($empenho->bancoprecoempenho == 1)
                      -
                    @else
                      @switch($empenho->credorempenho)
                        @case(1) Individual @break
                        @case(2) Coletivo @break
                        @default -
                      @endswitch
                    @endif
                  </td>
                  <td><strong>Documento:</strong><br>
                    @switch($empenho->doccredor)
                      @case(1) CPF @break
                      @case(2) CNPJ @break
                      @case(3) IG @break
                      @case(4) UX x GESTÃO @break
                      @default -
                    @endswitch
                  </td>
                  <td><strong>Nº Documento:</strong><br> {{$empenho->numerodoccredor}}</td>
                </tr>
                <tr>
                  <td colspan="4"><strong>Nome do Credor:</strong><br> {{$empenho->nomecredor}}</td>
                </tr>
                <tr>
                  <td colspan="2"><strong>Ref. Legal:</strong><br> {{$empenho->reflegal->descricao}}</td>
                  <td colspan="2"><strong>Nº do Protocolo:</strong><br> {{$empenho->nuproempenho}}</td>
                </tr>
                <tr>
                  <td colspan="4"><strong>Obs. do Solicitante:</strong> <br> {{$empenho->obsempenho}}</td>
                </tr>
              </tbody>
            </table>

        </td></tr>
      </tbody>
    </table>

  </div>

</div>


</body>
</html>

<script type="text/javascript">

function dataAt(){
    var data = new Date(),
        dia  = data.getDate().toString().padStart(2, '0'),
        mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
        ano  = data.getFullYear();
    return dia+"/"+mes+"/"+ano;
}

$(document).ready(function(){
  $('#btnPDF').click(function() {
    var numero = "{{ $empenho->numero }}"
    var doc = new jsPDF('portrait', 'pt', 'a4')
    var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"
    var img = new Image()
    img.src = base64Img
    var hoje = new Date()
    doc.autoTable({
      html: '#tabela',
      // columnStyles: {
      //      1: { cellWidth: 90 },
      //      3: { cellWidth: 90 }
      // },
      styles: {
        lineColor: '#ccc',
        lineWidth: 1,
        fontSize: 10
      },
      headStyles: { fillColor: "#424647", fontSize: 11 },
      didDrawPage: function (data) {
        /////////////////////// CABEÇALHO ////////////////////////////////////////////////////////
        doc.setFontSize(15)
        doc.setTextColor(40)
        doc.setFontType("bold")
        doc.text('NOTA DE EMPENHO', data.settings.margin.left + 0, 100)
        doc.setFontSize(9)
        doc.text('Data: ' + dataAt(), data.settings.margin.left + 445, 100)
        if (base64Img) {
          doc.addImage(img, 'JPEG', 40, 15, 515, 65)
        }
        // doc.text('Relatório de Pagamento por Fornecedor', data.settings.margin.left + 0, 125)

        //////////////////////// RODAPE ///////////////////////////////////////////////////////
        var str = 'Pág. ' + doc.internal.getNumberOfPages()
        doc.setFontSize(9)
        var pageSize = doc.internal.pageSize
        var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
        doc.text(str, data.settings.margin.left + 480, pageHeight - 25)
        doc.setFontType("bold");
        doc.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', data.settings.margin.left + 128, pageHeight - 30)
        doc.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br', data.settings.margin.left + 145, pageHeight - 20)
      },
      margin: {top: 108}
    });
    doc.save("Empenho_"+ numero +"_"+ dataAt() +".pdf");
  });
});


     //window.onload = function() { window.print(); }
</script>
