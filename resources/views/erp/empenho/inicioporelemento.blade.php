@extends('layout_admin.principal')
@section ('breadcrumb','Contas à Pagar')
@section('conteudo')
  <section>
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-9">
                    <strong>Contas à Pagar por Elemento/Subelemento</strong>
                  </div>
                </div>
              </div>
              @if (session('erro'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  {{session('erro')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              @endif
              <div class="card-body card-block">
                <form class="" action="{{route('porelemento')}}" method="post">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="col-6 form-group">
                      <label for="orc">Orçamento:</label>
                      <select name="orc" class="form-control" required>
                        <option value="">Selecione</option>
                        @foreach($orc as $key)
                          <option value="{{$key->id }}">{{$key->especificacao}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary">Buscar</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script>
  function k(i) {
    var v = i.value.replace(/\D/g,'');
    v = (v/100).toFixed(2) + '';
    v = v.replace(".", ".");
    v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
    v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
    i.value = v;
  }
</script>
@endsection
