@extends('layout_admin.principal')
@section ('breadcrumb','Relatório Pagamentos')

@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-10">
                                    <strong>Buscar Pagamento</strong>
                                </div>
                            </div>
                        </div>
                        <div class="card-body card-block">
                            <form class="" action="{{route('pagamento.relatorio.resultado')}}" method="post" target="_blank">
                                {{ csrf_field() }}
                                {{-- <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for=""><strong>OB:</strong></label>
                                        <input type="text" class="form-control" name="ob" />
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for=""><strong>Fornecedor:</strong></label>
                                        <select name="fornecedor" class="form-control"  >
                                          <option value="">Selecione uma opção</option>
                                          @foreach($fornecedores as $key)
                                          <option value="{{ $key->id }}">{{ $key->nome }}</option>
                                          @endforeach
                                        </select>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <p><strong>Data de Pagamento:</strong></p>
                                        <div class="row">
                                            <div class="col-6 form-group">
                                                <label for="dt_i">Início:</label>
                                                <input type="date" name="datapag_i"  class="form-control">
                                            </div>
                                            <div class="col-6 form-group">
                                                <label for="dt_f">Final:</label>
                                                <input type="date" name="datapag_f"  class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="row">
                                    <div class="col-12">
                                        <p><strong>Periodo:</strong></p>
                                        <div class="row">
                                            <div class="col-6 form-group">
                                                <label for="mes">Mês:</label>
                                                <select name="mes" class="form-control"  >
                                                  <option value="">Selecione uma opção</option>
                                                  @foreach ($mes as $key )
                                                  <option value="{{ $key->mes }}">{{ $key->mes }}</option>
                                                  @endforeach
                                                </select>
                                            </div>
                                            <div class="col-6 form-group">
                                                <label for="ano">Ano:</label>
                                                <select name="ano" class="form-control"  >
                                                  <option value="">Selecione uma opção</option>
                                                  @foreach ($ano as $key )
                                                  <option value="{{ $key->ano }}">{{ $key->ano }}</option>
                                                  @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Buscar</button>
                                <button type="reset" class="btn btn-warning">Limpar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>


<script>
    function k(i) {
                        var v = i.value.replace(/\D/g,'');
                        v = (v/100).toFixed(2) + '';
                        v = v.replace(".", ".");
                        v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                        v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                        i.value = v;
                    }
</script>


@endsection
