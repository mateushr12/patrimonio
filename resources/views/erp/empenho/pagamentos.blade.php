@extends('layout_admin.principal')
@section ('breadcrumb','Pagamento')
@section('conteudo')
<style>
    #ob{
      color: red;
    }
</style>
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Pagamento</strong>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body card-block">
                            <form class="" action="{{route('pagamento.salvar', $empenho->id)}}" method="post">
                                {{ csrf_field() }}
                                {{-- <input type="hidden" name="_method" value="put"> --}}
                                <input type="hidden" name="id_empenho" value="{{ $empenho->id }}">
                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="numero">Nº Empenho</label>
                                    <input type="text" class="form-control" readonly value="{{ $empenho->numero }}" >
                                  </div>
                                  <div class="col-6 form-group">
                                    <label for="numero">Nº Pagamento<span id="ob">*</span></label>
                                    <input type="text" name="numero" onkeyup="this.value = this.value.toUpperCase();" required class="form-control" >
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="numero">Data Pagamento<span id="ob">*</span></label>
                                    <input type="date" name="datapag" required class="form-control" >
                                  </div>
                                  <div class="col-6 form-group">
                                    <label for="numero">Data Vencimento<span id="ob">*</span></label>
                                    <input type="date" name="vencimento" required class="form-control" >
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-4 form-group">
                                    <label for="numero">Valor<span id="ob">*</span></label>
                                    <input type="text" class="form-control" onkeyup="k(this);" placeholder="0.00" name="valor_bruto" required >
                                  </div>
                                  <div class="col-4 form-group">
                                    <label for="numero">O.B.<span id="ob">*</span></label>
                                    <input type="text" class="form-control" name="ob" required >
                                  </div>
                                  <div class="col-4 form-group">
                                    <label for="datapag">N.F./Fatura<span id="ob">*</span></label>
                                    <input type="text" class="form-control" name="nf" required >
                                  </div>
                                </div>
                                <span id="ob">*</span> campo obrigatório<br>
                                <button type="submit" class="btn btn-primary">Pagar</button>
                            </form>

                            <hr>
                            <div class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                    <tr>
                                        <th>O.B.</th>
                                        <th>Nº</th>
                                        <th>N.F./Fatura</th>
                                        <th>Valor (R$)</th>
                                        <th>Data Pag.</th>
                                        <th>Data Venc.</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pagamentos as $pagamento)
                                    <tr>
                                      <td>{{ $pagamento->ob }}</td>
                                      <td>{{ $pagamento->numero }}</td>
                                      <td>{{ $pagamento->nf }}</td>
                                      <td>{{ number_format($pagamento->valor_bruto,2,',','.') }}</td>
                                      <td>{{\Carbon\Carbon::parse($pagamento->datapag)->format('d/m/Y')}}</td>
                                      <td>{{ isset($pagamento->vencimento) ? (\Carbon\Carbon::parse($pagamento->vencimento)->format('d/m/Y')) : '-' }}</td>
                                      <td>
                                          <div class="table-data-feature">
                                              {{-- {{route('empenho.deletar' , $liquidado->id)}} --}}
                                              <a href="{{ route('pagamento.deletar',$pagamento->id) }}"
                                                  data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                  <button type="button" class="item" title="Deletar"
                                                  data-toggle="tooltip" data-placement="top" title=""
                                                      data-original-title="Deletar"
                                                  >
                                                      <i class="zmdi zmdi-delete"></i>
                                                  </button>
                                              </a>&nbsp;
                                              {{-- <a href="{{ route('retencao',$pagamento->id) }}">
                                                  <button type="button" class="item" title="Retenção"
                                                  data-toggle="tooltip" data-placement="top" title=""
                                                      data-original-title="Retenção"
                                                  >
                                                      <i class="zmdi zmdi-scissors"></i>
                                                  </button>
                                              </a> --}}
                                          </div>
                                      </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function k(i) {
                var v = i.value.replace(/\D/g,'');
                v = (v/100).toFixed(2) + '';
                v = v.replace(".", ".");
                v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                i.value = v;
            }
</script>
@endsection
