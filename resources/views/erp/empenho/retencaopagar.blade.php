@extends('layout_admin.principal')
@section ('breadcrumb','Retenção')
@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Pagar Retenção</strong>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body card-block">
                            <form class="" action="{{route('retencao.pagar', $retencoes->id)}}" method="post">
                                {{ csrf_field() }}
                                {{-- <input type="hidden" name="_method" value="put"> --}}
                                <input type="hidden" name="pago" value="2">
                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="numero">Valor</label>
                                    <input type="text" class="form-control" disabled value="{{ $retencoes->valor }}">
                                  </div>
                                  <div class="col-6 form-group">
                                    <label for="numero">Data de Pagamento*</label>
                                    <input type="date" class="form-control" name="datapag" required >
                                  </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Pagar</button>
                            </form>
                            <a href="{{ route('retencao',$retencoes->id_pagamento) }}" >Voltar a retenção</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function k(i) {
                var v = i.value.replace(/\D/g,'');
                v = (v/100).toFixed(2) + '';
                v = v.replace(".", ".");
                v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                i.value = v;
            }
</script>
@endsection
