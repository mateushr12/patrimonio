<!DOCTYPE html>
<html lang="pt-br">
<head>
  <!-- Required meta tags-->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="JUCESE - Junta Comercial do Estado de Sergipe">

  <!-- Title Page-->
  <title>ERP - JUCESE</title>

  <link href="<?php echo asset('vendor/bootstrap-4.1/bootstrap.min.css')?>" rel="stylesheet" media="all">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"></script>
  <script src="https://unpkg.com/jspdf-autotable@3.5.9/dist/jspdf.plugin.autotable.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

  <style>
    @media print {
      .btn {
        display: none !important;
      }
    }
    #btnPDF{
      background: #2579e6;
      color: white;
      font-weight: bold;
      width: 315px;
    }
  </style>
</head>

<body>

    <div class="card" style="width: 100%">
      <div class="card-body">
        <button type="button" class="btn" id="btnPDF">Gerar PDF</button><br>
        <img src="../../images/gov-jucese-top-relatorio.jpg" hidden>
        <table >
          <thead >
            <tr>
              <td style=" font-size: 28px;" ><strong>ORDEM CRONOLÓGICA DE PAGAMENTOS</strong><p></p></td>
              <td style="font-size: 15px;">{{ $data }}</td>
            </tr>
          </thead>
          <tbody>
            <tr><td colspan="2">
              <table class="table table-data2" id="tabela">
                <thead>
                  <tr>
                    <th>Nº ORDEM</th>
                    <th>F.R.</th>
                    <th>EMPENHO</th>
                    <th>DT. EMPENHO</th>
                    <th>CREDOR</th>
                    <th>NF / FATURA</th>
                    <th>VENCIMENTO</th>
                    <th>VALOR</th>
                    <th>O.B.</th>
                    <th>DT. PAGAMENTO</th>
                    <th>OBS.</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($pagamentos as $key)
                    <tr class="tr-shadow">
                      <td>{{ $no+1 }}</td>
                      <span hidden>{{ $no = $no + 1 }}</span>
                      <td>{{ $key->fr }}</td>
                      <td>{{ $key->numero }}</td>
                      <td>{{  \Carbon\Carbon::parse($key->dtEmp)->format('d/m/Y') }}</td>
                      @if (isset($key->nomecredor))
                        <td>{{ $key->nomecredor }}</td>
                        {{-- <td>JUNTA COMERCIAL DO ESTADO DE SERGIPE</td> --}}
                      @else
                        <td>{{ $key->nome }}</td>
                      @endif
                      <td>{{ $key->nf }}</td>
                      <td>
                        @if (isset($key->vencimento))
                          {{ \Carbon\Carbon::parse($key->vencimento)->format('d/m/Y') }}
                        @else
                          -
                        @endif
                      </td>
                      <td>{{ number_format(($key->valor_bruto),2,',','.') }}</td>
                      <td>{{ $key->ob }}</td>
                      <td>{{ \Carbon\Carbon::parse($key->datapag)->format('d/m/Y') }}</td>
                      <td>{{$key->obsempenho}}</td>
                      <span hidden>{{ $total[] = $key->valor_bruto }}</span>
                    </tr>
                    <tr class="spacer"></tr>
                  @endforeach
                  <tr>
                    <td colspan="7"><strong>TOTAL</strong></td>
                    <td><strong>R$ {{ number_format((isset($total) ? array_sum($total) : 0),2,',','.') }}</strong></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            </td></tr>
          </tbody>
        </table>
      </div>
    </div>


</body>

<script>

function dataAt(){
    var data = new Date(),
        dia  = data.getDate().toString().padStart(2, '0'),
        mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
        ano  = data.getFullYear();
    return dia+"/"+mes+"/"+ano;
}

$(document).ready(function(){
  $('#btnPDF').click(function() {
    var doc = new jsPDF('landscape', 'pt', 'a4')
    var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"
    var img = new Image()
    img.src = base64Img
    var hoje = new Date()
    var dt = "{{ $data }}"
    doc.autoTable({
      html: '#tabela',
      // columnStyles: {
      //   1: { cellWidth: 90 },
      //   3: { cellWidth: 90 }
      // },
      styles: {
        lineColor: '#ccc',
        lineWidth: 1,
        fontSize: 8
      },
      headStyles: { fillColor: 0 },
      didDrawPage: function (data) {
        // Cabeçalho
        if (base64Img) {
          doc.addImage(img, 'JPEG', 40, 15, 765, 76)
        }
        doc.setFontSize(14)
        doc.setTextColor(40)
        doc.setFontType("bold")
        doc.text('ORDEM CRONOLÓGICA DE PAGAMENTOS', data.settings.margin.left + 0, 109)
        doc.setFontSize(9)
        doc.text(dt, data.settings.margin.left + 692, 109)
        // doc.text('Relatório de Pagamento por Fornecedor', data.settings.margin.left + 0, 125)

        // Rodape
        var str = 'Pág. ' + doc.internal.getNumberOfPages()
        doc.setFontSize(9)
        var pageSize = doc.internal.pageSize
        var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
        doc.text(str, data.settings.margin.left + 730, pageHeight - 20)
        doc.setFontType("bold");
        doc.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', data.settings.margin.left + 250, pageHeight - 25)
        doc.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br', data.settings.margin.left + 263, pageHeight - 15)
      },
      margin: {top: 118}
    });
    doc.save("RelatorioPagamentos-"+ dataAt() +".pdf");
  });
});

// window.onload = function() { window.print(); }
</script>

</html>
