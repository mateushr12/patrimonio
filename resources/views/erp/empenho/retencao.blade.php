@extends('layout_admin.principal')
@section ('breadcrumb','Retenção')
@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Retenção</strong>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body card-block">
                            <form class="" action="{{route('retencao.salvar', $pagamento->id)}}" method="post">
                                {{ csrf_field() }}
                                {{-- <input type="hidden" name="_method" value="put"> --}}
                                <input type="hidden" name="id_pagamento" value="{{ $pagamento->id }}">
                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="numero">Nº Pagamento</label>
                                    <input type="text" class="form-control" readonly value="{{ $pagamento->numero }}" >
                                  </div>
                                  <div class="col-6 form-group">
                                    <label for="numero">Valor Bruto</label>
                                    <input type="text" readonly value="{{ $pagamento->valor_bruto }}" class="form-control" >
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="numero">Valor*</label>
                                    <input type="text" class="form-control" onkeyup="k(this);" placeholder="0.00" name="valor" required >
                                  </div>
                                  <div class="col-6 form-group">
                                    <label for="numero">O.B.*</label>
                                    <input type="text" class="form-control" name="ob" required >
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="numero">Data*</label>
                                    <input type="date" class="form-control" name="data" required >
                                  </div>
                                  <div class="col-6 form-group">
                                    <label for="numero">Tipo de retencão*</label>
                                    <select name="id_tipo" class="form-control" required>
                                      <option value="">Selecione</option>
                                      @foreach($tipos as $tipo)
                                      <option value="{{$tipo->id }}">{{$tipo->descricao}}</option>
                                      @endforeach
                                    </select>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-12 form-group">
                                    <label for="numero">OBS.</label>
                                    <input type="text" class="form-control" name="obs" >
                                  </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Reter</button>
                            </form>
                            <a href="{{ route('pagamento',$pagamento->id_empenho) }}" >Voltar ao pagamento</a>
                            <hr>
                            <div class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                    <tr>
                                        <th>Tipo</th>
                                        <th>Valor (R$)</th>
                                        <th>Data</th>
                                        <th>O.B.</th>
                                        <th>Pago</th>
                                        <th>Data de Pagamento</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($retencoes as $retencao)
                                    <tr>
                                      <td>{{ $retencao->tipo->descricao }}</td>
                                      <td>{{ $retencao->valor }}</td>
                                      <td>{{ \Carbon\Carbon::parse($retencao->data)->format('d/m/Y') }}</td>
                                      <td>{{ $retencao->ob }}</td>
                                      <td>
                                        @if ($retencao->pago == 1)
                                          À pagar
                                        @else
                                          Pago
                                        @endif
                                      </td>
                                      <td>{{ isset($retencao->datapag) ? \Carbon\Carbon::parse($retencao->datapag)->format('d/m/Y') : '-' }}</td>
                                      <td>
                                          <div class="table-data-feature">                                              
                                              @if ($retencao->pago == 1)
                                              <a href="{{route('retencao.pagamento' , $retencao->id)}}">
                                                  <button type="button" class="item" title="Pagar Retenção"
                                                  data-toggle="tooltip" data-placement="top" title=""
                                                      data-original-title="Pagar Retenção"
                                                  >
                                                      <i class="zmdi zmdi-money"></i>
                                                  </button>
                                              </a>
                                              @else
                                              @endif
                                              &nbsp;
                                              <a href="{{route('retencao.deletar' , $retencao->id)}}"
                                                  data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                  <button type="button" class="item" title="Deletar"
                                                  data-toggle="tooltip" data-placement="top" title=""
                                                      data-original-title="Deletar"
                                                  >
                                                      <i class="zmdi zmdi-delete"></i>
                                                  </button>
                                              </a>
                                          </div>
                                      </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function k(i) {
                var v = i.value.replace(/\D/g,'');
                v = (v/100).toFixed(2) + '';
                v = v.replace(".", ".");
                v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                i.value = v;
            }
</script>
@endsection
