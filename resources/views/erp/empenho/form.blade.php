<div class="row">
  <div class="col-3 form-group">
    <label for="numero">Nº Empenho:<span id="ob">*</span></label>
    <input type="text" name="numero" id="numero" required class="form-control"
    onkeyup="this.value = this.value.toUpperCase();"
    {{-- {{ isset($empenho->numero) ? 'disabled' : '' }} --}}
    value="{{ isset($empenho->numero) ? $empenho->numero : '' }}" >
  </div>
  <div class="col-3 form-group">
    <label for="bancoprecoempenho">Banco de Preço:<span id="ob">*</span></label>
    <select name="bancoprecoempenho" id="bancoprecoempenho" class="form-control" {{isset($empenho->bancoprecoempenho) ? 'disabled' : ''}} required>
      <option value="">Selecione uma opção</option>
      <option value="1" @if(isset($empenho->bancoprecoempenho) && $empenho->bancoprecoempenho == 1):
        selected
      @endif>Sim</option>
      <option value="2" @if(isset($empenho->bancoprecoempenho) && $empenho->bancoprecoempenho == 2):
        selected
      @endif>Não</option>
    </select>
  </div>
  <div class="col-4 form-group">
    <label for="dataempenho">Data do Empenho:<span id="ob">*</span></label>
    <input type="date" name="dataempenho" id="dataempenho" class="form-control"
    {{isset($empenho->dataempenho) ? 'disabled' : ''}}
    value="{{isset($empenho->dataempenho) ? $empenho->dataempenho : ''}}" required>
  </div>
  <div class="col-2 form-group">
    <label for="urgempenho">Urgência:<span id="ob">*</span></label>
    <select name="urgempenho" id="urgempenho" class="form-control" required>
      <option value="1" @if(isset($empenho->urgempenho) && $empenho->urgempenho == 1):
        selected
      @endif>Sim</option>
      <option value="2"  @if(isset($empenho->urgempenho) && $empenho->urgempenho == 2):
        selected
      @endif>Não</option>
    </select>
  </div>
</div>

@if (isset($empenho->tipoempenho) && $empenho->tipoempenho == 2)
  <div class="row">
    <div class="col-6 form-group">
      <label for="numero">Tipo Empenho:</label>
      <input type="text" readonly value="Reforço" class="form-control">
    </div>
    <div class="col-6 form-group">
      <label for="numero">Nº Empenho Pai:</label>
      <input type="text" readonly value="{{ $empenhopai->numero }}" class="form-control">
    </div>
  </div>
@else
@endif
<hr>

<div class="row">
  <div class="col-4 form-group">
    <label for="modempenho">Modalidade:<span id="ob">*</span></label>
    <select name="modempenho" id="modempenho" class="form-control" required>
      <option value="">Selecione uma opção</option>
      <option value="1" @if(isset($empenho->modempenho) && $empenho->modempenho == 1):
        selected
      @endif>Ordinário</option>
      <option value="2" @if(isset($empenho->modempenho) && $empenho->modempenho == 2):
        selected
      @endif>Estimativo</option>
      <option value="3" @if(isset($empenho->modempenho) && $empenho->modempenho == 3):
        selected
      @endif>Global</option>
    </select>
  </div>
  <div class="col-4 form-group">
    <label for="modlicempenho">Modalidade de Licitação:<span id="ob">*</span></label>
    <select name="modlicempenho" id="modlicempenho" class="form-control" required>
      <option value="">Selecione uma opção</option>
      @foreach($modLs as $modL)
        <option
        {{isset($empenho->modlicempenho) && $empenho->modlicempenho == $modL->id ? 'selected' : ''}}
        value="{{$modL->id }}">
        {{$modL->descricao}}
      </option>
    @endforeach
  </select>
</div>
<div class="col-4 form-group">
  <label for="despempenho">Tipo de despesa:<span id="ob">*</span></label>
  <select name="despempenho" id="despempenho" class="form-control" required>
    <option value="">Selecione uma opção</option>
    <option value="1" @if(isset($empenho->despempenho) && $empenho->despempenho == 1):
      selected
    @endif>1 - NORMAL</option>
    <option value="2" @if(isset($empenho->despempenho) && $empenho->despempenho == 2):
      selected
    @endif>2 - REPASSE FINANCEIRO</option>
    <option value="3" @if(isset($empenho->despempenho) && $empenho->despempenho == 3):
      selected
    @endif>3 - SUPRIMENTO INDIVIDUAL</option>
    <option value="4" @if(isset($empenho->despempenho) && $empenho->despempenho == 4):
      selected
    @endif>4 - SUPRIMENTO INSTITUCIONAL</option>
    <option value="5" @if(isset($empenho->despempenho) && $empenho->despempenho == 5):
      selected
    @endif>5 - RETITUIÇÕES DE TRIBUTOS</option>
    <option value="6" @if(isset($empenho->despempenho) && $empenho->despempenho == 6):
      selected
    @endif>6 - TRANSFERÊNCIAS VOLUNTÁRIAS</option>
    <option value="7" @if(isset($empenho->despempenho) && $empenho->despempenho == 7):
      selected
    @endif>7 - FOLHA DE PAGAMENTO</option>
  </select>
</div>
</div>
<hr>

<div class="row">
  <div class="col-12 form-group">
    <label for="acaoempenho">Célula Orçamentária:<span id="ob">*</span></label>
    <select name="acaoempenho" id="acaoempenho" class="form-control" required
    {{isset($empenho->acaoempenho) ? 'disabled' : ''}} >
    <option value="">Selecione uma ação</option>
    @foreach($acoes as $acao)
      <option
      {{isset($empenho->acaoempenho) && $empenho->acaoempenho == $acao->id ? 'selected' : ''}}
      value="{{$acao->id }}">
      {{$acao->CodAcao}} - {{$acao->DescAcao}}
    </option>
  @endforeach
</select>
</div>
</div>

<div class="row">
  <div class="col-6 form-group">
    <label for="elemempenho">Elemento de Despesa:<span id="ob">*</span></label>
    <select name="elemempenho" id="elemempenho" class="form-control"
    {{isset($empenho->elemempenho) ? 'disabled' : ''}}
    required>
    <option value="">Selecione uma ação</option>
    @foreach($elementos as $elemento)
      <option {{isset($empenho->elemempenho) && $empenho->elemempenho == $elemento->id ? 'selected' : ''}}
        value="{{$elemento->id }}">
        {{$elemento->CodElemento}} - {{$elemento->DescElemento}}
      </option>
    @endforeach
  </select>
</div>

@if(isset($empenho->subelemento))
  <div class="col-6 form-group">
    <label for="subelem">Sub-Elemento de Despesa:<span id="ob">*</span></label>
    <input type="hidden" name="subelemento" id="subelemento" readonly class="form-control" value="{{ $subelemento->id }}">
    <input type="text" readonly class="form-control" value="{{ $subelemento->codnatureza }} - {{ $subelemento->descricao }}">
  </div>
@else
  <div class="col-6 form-group">
    <label for="subelem">Sub-Elemento de Despesa:<span id="ob">*</span></label>
    {!! Form::select('subelemento', [], ['required'] ) !!}
  </div>
@endif
</div>

<div class="row">
  <div class="col-12 form-group">
    <label for="fichaempenho">Ficha Financeira:</label>
    <input type="text" name="fichaempenho" id="fichaempenho" readonly class="form-control" value="{{isset($empenho->fichaempenho) ? $empenho->fichaempenho : ''}}">
  </div>
</div>
<div class="row">
  <div class="col-6 form-group">
    <label for="convempenho">Convênio:</label>
    <input type="text" name="convempenho" id="convempenho" class="form-control" value="{{isset($empenho->convempenho) ? $empenho->convempenho : ''}}">
  </div>
  <div class="col-6 form-group">
    <label for="valorempenho">Valor da Solicitação (R$):<span id="ob">*</span></label>
    <input type="text" name="valorempenho" id="valorempenho" class="form-control" onkeyup="k(this);" required
    {{isset($empenho->valorempenho) ? 'disabled' : ''}}
    value="{{isset($empenho->valorempenho) ? $empenho->valorempenho : ''}}">
  </div>
</div>
<hr>
<!--{ Tela de banco de preço | SIM}-->
<div class="precoempenho" id="precosim">
  <div class="row">
    <div class="col-12 form-group">
      <label for="licempenho">Licitação:<span id="ob">*</span></label>
      <select name="licempenho" id="licempenho" class="form-control"
      {{isset($empenho->licempenho) ? 'disabled' : ''}}
      >
      <option value="">Selecione uma opção</option>
      @foreach($contratos as $contrato)
        <option
        {{isset($empenho->licempenho) && $empenho->licempenho == $contrato->id ? 'selected' : ''}}
        value="{{$contrato->id }}">
        {{$contrato->numerocontrato}} - {{$contrato->fornecedor->nome}}
      </option>
    @endforeach
  </select>
</div>
</div>
<div class="row">
  <div class="col-12 form-group">
    <label for="protoempenho">Origem do protocolo:</label>
    <select name="protoempenho" id="protoempenho" class="form-control">
      <option value="">Selecione uma opção</option>
      <option value="1" @if(isset($empenho->protoempenho) && $empenho->protoempenho == 1):
        selected
      @endif>i-Gesp</option>
      <option value="2" @if(isset($empenho->protoempenho) && $empenho->protoempenho == 2):
        selected
      @endif>Outros</option>
    </select>
  </div>
</div>
</div>
<!--{ Fim da Tela de banco de preço | SIM}-->
<!--{ Tela de banco de preço | Não}-->
<div class="precoempenho" id="preconao">
  <div class="row">
    <div class="col-4 form-group">
      <label for="credorempenho">Credor:<span id="ob">*</span></label>
      <select name="credorempenho" id="credorempenho" class="form-control">
        <option value="1" @if(isset($empenho->credorempenho) && $empenho->credorempenho == 1):
          selected
        @endif>Individual</option>
        <option value="2" @if(isset($empenho->credorempenho) && $empenho->credorempenho == 2):
          selected
        @endif>Coletivo</option>
      </select>
    </div>
    <div class="col-4 form-group">
      <label for="doccredor">Documento</label>
      <select name="doccredor" id="doccredor" class="form-control">
        <option value="">Selecione uma opção</option>
        <option value="1" @if(isset($empenho->doccredor) && $empenho->doccredor == 1):
          selected
        @endif>CPF</option>
        <option value="2" @if(isset($empenho->doccredor) && $empenho->doccredor == 2):
          selected
        @endif>CNPJ</option>
        <option value="3" @if(isset($empenho->doccredor) && $empenho->doccredor == 3):
          selected
        @endif>IG</option>
        <option value="4" @if(isset($empenho->doccredor) && $empenho->doccredor == 4):
          selected
        @endif>UXxGESTÃO</option>
      </select>
    </div>
    <div class="col-4 form-group">
      <label for="numerodoccredor">Nº Documento:</label>
      <input type="text" name="numerodoccredor" id="numerodoccredor" class="form-control" value="{{isset($empenho->numerodoccredor) ? $empenho->numerodoccredor : ''}}">
    </div>
  </div>
  <div class="row">
    <div class="col-12 form-group">
      <label for="nomecredor">Nome do credor:<span id="ob">*</span></label>
      <input type="text" name="nomecredor" id="nomecredor" class="form-control" value="{{isset($empenho->nomecredor) ? $empenho->nomecredor : ''}}">
    </div>
  </div>
</div>
<!--{ Fim da Tela de banco de preço | Não}-->
<div class="row">
  <div class="col-8 form-group">
    <label for="reflegalempenho">Referência Legal:<span id="ob">*</span></label>
    <select name="reflegalempenho" id="reflegalempenho" class="form-control">
      <option value="">Selecione uma opção</option>
      @foreach($refs as $ref)
        <option
        {{isset($empenho->reflegalempenho) && $empenho->reflegalempenho == $ref->id ? 'selected' : ''}}
        value="{{$ref->id }}">
        {{$ref->descricao}}
      </option>
    @endforeach
  </select>
</div>
<div class="col-4 form-group">
  <label for="nuproempenho">Número do protocolo:</label>
  <input type="text" name="nuproempenho" id="nuproempenho" class="form-control" value="{{isset($empenho->nuproempenho) ? $empenho->nuproempenho : ''}}">
</div>
</div>
<hr>

<div class="row">
  <div class="col-12 form-group">
    <label for="obsempenho">Observação do Solicitante:</label>
    <input type="text" name="obsempenho" id="obsempenho" class="form-control" value="{{isset($empenho->obsempenho) ? $empenho->obsempenho : ''}}">
  </div>
</div>
