@extends('layout_admin.principal')
@section ('breadcrumb','Reforços')
@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Reforço Empenho Cadastrado</strong>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body card-block">
                            <form class="" action="{{route('empenho.newreforco', $empenho->id)}}" method="post">
                                {{ csrf_field() }}
                                {{-- <input type="hidden" name="_method" value="put"> --}}
                                <input type="hidden" name="tipoempenho" value="2">
                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="numero">Nº Reforço:*</label>
                                    <input type="text" class="form-control" name="numero" id="numero" required
                                    onkeyup="this.value = this.value.toUpperCase();"
                                    {{-- {{ isset($empenho->numero) ? 'disabled' : '' }} --}}
                                    {{-- value="{{ isset($empenho->numero) ? $empenho->numero : '' }}"  --}}
                                    >
                                  </div>
                                  <div class="col-6 form-group">
                                    <label for="numero">Empenho a Reforçar:</label>
                                    <input type="text" readonly class="form-control"
                                    value="{{ $empenho->numero }}" >
                                  </div>
                                </div>
                                <hr>

                                <div class="row">
                                  <div class="col-4 form-group">
                                    <label for="bancoprecoempenho">Banco de Preço:</label>
                                    <select name="bancoprecoempenho" id="bancoprecoempenho" class="form-control" readonly >
                                      <option value="">Selecione uma opção</option>
                                      <option value="1" @if($empenho->bancoprecoempenho == 1):
                                        selected
                                      @endif>Sim</option>
                                      <option value="2" @if($empenho->bancoprecoempenho == 2):
                                        selected
                                      @endif>Não</option>
                                    </select>
                                  </div>
                                  <div class="col-4 form-group">
                                    <label for="dataempenho">Data do Empenho:*</label>
                                    <input type="date" name="dataempenho" id="dataempenho" class="form-control" required
                                    value="" >
                                  </div>
                                  <div class="col-4 form-group">
                                    <label for="urgempenho">Urgência:</label>
                                    <select name="urgempenho" id="urgempenho" class="form-control" readonly>
                                      <option value="1" @if($empenho->urgempenho == 1):
                                        selected
                                      @endif>Sim</option>
                                      <option value="2"  @if($empenho->urgempenho == 2):
                                        selected
                                      @endif>Não</option>
                                    </select>
                                  </div>
                                </div>
                                <hr>

                                <div class="row">
                                  <div class="col-4 form-group">
                                    <label for="modempenho">Modalidade:</label>
                                    <select name="modempenho" id="modempenho" class="form-control" readonly>
                                      {{-- <option value="">Selecione uma opção</option> --}}
                                      <option value="1" @if($empenho->modlicempenho == 1):
                                        selected
                                      @endif>Ordinário</option>
                                      <option value="2" @if($empenho->modlicempenho == 2):
                                        selected
                                      @endif>Estimativo</option>
                                      <option value="3" @if($empenho->modlicempenho == 3):
                                        selected
                                      @endif>Global</option>
                                    </select>
                                  </div>
                                  <div class="col-4 form-group">
                                    <label for="modlicempenho">Modalidade de Licitação:</label>
                                    <select name="modlicempenho" id="modlicempenho" class="form-control" readonly>
                                      <option value="">Selecione uma opção</option>
                                      @foreach($modLs as $modL)
                                        <option
                                        {{$empenho->modlicempenho == $modL->id ? 'selected' : ''}}
                                        value="{{$modL->id }}">
                                        {{$modL->descricao}}
                                      </option>
                                    @endforeach
                                  </select>
                                </div>
                                <div class="col-4 form-group">
                                  <label for="despempenho">Tipo de despesa:</label>
                                  <select name="despempenho" id="despempenho" class="form-control" readonly>
                                    <option value="">Selecione uma opção</option>
                                    <option value="1" @if($empenho->despempenho == 1):
                                      selected
                                    @endif>1 - NORMAL</option>
                                    <option value="2" @if($empenho->despempenho == 2):
                                      selected
                                    @endif>2 - REPASSE FINANCEIRO</option>
                                    <option value="3" @if($empenho->despempenho == 3):
                                      selected
                                    @endif>3 - SUPRIMENTO INDIVIDUAL</option>
                                    <option value="4" @if($empenho->despempenho == 4):
                                      selected
                                    @endif>4 - SUPRIMENTO INSTITUCIONAL</option>
                                    <option value="5" @if($empenho->despempenho == 5):
                                      selected
                                    @endif>5 - RETITUIÇÕES DE TRIBUTOS</option>
                                    <option value="6" @if($empenho->despempenho == 6):
                                      selected
                                    @endif>6 - TRANSFERÊNCIAS VOLUNTÁRIAS</option>
                                    <option value="7" @if($empenho->despempenho == 7):
                                      selected
                                    @endif>7 - FOLHA DE PAGAMENTO</option>
                                  </select>
                                </div>
                                </div>
                                <hr>

                                <div class="row">
                                  <div class="col-12 form-group">
                                    <label for="acaoempenho">Célula Orçamentária:</label>
                                    <select name="acaoempenho" id="acaoempenho" class="form-control" required
                                    readonly >
                                    <option value="">Selecione uma ação</option>
                                    @foreach($acoes as $acao)
                                      <option
                                      {{$empenho->acaoempenho == $acao->id ? 'selected' : ''}}
                                      value="{{$acao->id }}">
                                      {{$acao->CodAcao}} - {{$acao->DescAcao}}
                                    </option>
                                  @endforeach
                                </select>
                                </div>
                                </div>

                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="elemempenho">Elemento de Despesa:</label>
                                    <select name="elemempenho" id="elemempenho" class="form-control"
                                    {{-- disabled --}} readonly
                                    >
                                    <option value="">Selecione uma ação</option>
                                    @foreach($elementos as $elemento)
                                      <option {{$empenho->elemempenho == $elemento->id ? 'selected' : ''}}
                                        value="{{$elemento->id}}">
                                        {{$elemento->CodElemento}} - {{$elemento->DescElemento}}
                                      </option>
                                    @endforeach
                                  </select>
                                  </div>
                                    <div class="col-6 form-group">
                                      <label for="subelem">Sub-Elemento de Despesa:</label>
                                      <input type="text" name="subelemento" id="subelemento" readonly class="form-control" value="{{ $subelemento->descricao }}">
                                    </div>
                                </div>

                                <div class="row">
                                  <div class="col-12 form-group">
                                    <label for="fichaempenho">Ficha Financeira:</label>
                                    <input type="text" name="fichaempenho" id="fichaempenho" readonly class="form-control" value="{{$empenho->fichaempenho}}">
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="convempenho">Convênio:</label>
                                    <input type="text" name="convempenho" id="convempenho" readonly class="form-control" value="{{$empenho->convempenho}}">
                                  </div>
                                  <div class="col-6 form-group">
                                    <label for="valorempenho">Valor da Solicitação (R$):*</label>
                                    <input type="text" name="valorempenho" id="valorempenho" class="form-control" onkeyup="k(this);" required
                                    value="" >
                                  </div>
                                </div>
                                <hr>
                                <!--{ Tela de banco de preço | SIM}-->
                                <div class="precoempenho" id="precosim">
                                  <div class="row">
                                    <div class="col-12 form-group">
                                      <label for="licempenho">Licitação:</label>
                                      <select name="licempenho" id="licempenho" class="form-control"
                                      readonly
                                      >
                                      <option value="">Selecione uma opção</option>
                                      @foreach($contratos as $contrato)
                                        <option
                                        {{$empenho->licempenho == $contrato->id ? 'selected' : ''}}
                                        value="{{$contrato->id }}">
                                        {{$contrato->numerocontrato}} - {{$contrato->fornecedor->nome}}
                                      </option>
                                    @endforeach
                                  </select>
                                </div>
                                </div>
                                <div class="row">
                                  <div class="col-12 form-group">
                                    <label for="protoempenho">Origem do protocolo:</label>
                                    <select name="protoempenho" id="protoempenho" class="form-control" readonly>
                                      <option value="">Selecione uma opção</option>
                                      <option value="1" @if($empenho->protoempenho == 1):
                                        selected
                                      @endif>i-Gesp</option>
                                      <option value="2" @if($empenho->protoempenho == 2):
                                        selected
                                      @endif>Outros</option>
                                    </select>
                                  </div>
                                </div>
                                </div>
                                <!--{ Fim da Tela de banco de preço | SIM}-->
                                <!--{ Tela de banco de preço | Não}-->
                                <div class="precoempenho" id="preconao">
                                  <div class="row">
                                    <div class="col-4 form-group">
                                      <label for="credorempenho">Credor:</label>
                                      <select name="credorempenho" id="credorempenho" class="form-control" readonly>
                                        <option value="1" @if($empenho->credorempenho == 1):
                                          selected
                                        @endif>Individual</option>
                                        <option value="2" @if($empenho->credorempenho == 2):
                                          selected
                                        @endif>Coletivo</option>
                                      </select>
                                    </div>
                                    <div class="col-4 form-group">
                                      <label for="doccredor">Documento</label>
                                      <select name="doccredor" id="doccredor" class="form-control" readonly>
                                        <option value="">Selecione uma opção</option>
                                        <option value="1" @if($empenho->doccredor == 1):
                                          selected
                                        @endif>CPF</option>
                                        <option value="2" @if($empenho->doccredor == 2):
                                          selected
                                        @endif>CNPJ</option>
                                        <option value="3" @if($empenho->doccredor == 3):
                                          selected
                                        @endif>IG</option>
                                        <option value="4" @if($empenho->doccredor == 4):
                                          selected
                                        @endif>UXxGESTÃO</option>
                                      </select>
                                    </div>
                                    <div class="col-4 form-group">
                                      <label for="numerodoccredor">Nº Documento:</label>
                                      <input type="text" name="numerodoccredor" id="numerodoccredor" readonly class="form-control" value="{{$empenho->numerodoccredor}}">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-12 form-group">
                                      <label for="nomecredor">Nome do credor:</label>
                                      <input type="text" name="nomecredor" id="nomecredor" readonly class="form-control" value="{{$empenho->nomecredor}}">
                                    </div>
                                  </div>
                                </div>
                                <!--{ Fim da Tela de banco de preço | Não}-->
                                <div class="row">
                                  <div class="col-8 form-group">
                                    <label for="reflegalempenho">Referência Legal:</label>
                                    <select name="reflegalempenho" id="reflegalempenho" class="form-control" readonly>
                                      <option value="">Selecione uma opção</option>
                                      @foreach($refs as $ref)
                                        <option
                                        {{$empenho->reflegalempenho == $ref->id ? 'selected' : ''}}
                                        value="{{$ref->id }}">
                                        {{$ref->descricao}}
                                      </option>
                                    @endforeach
                                  </select>
                                </div>
                                <div class="col-4 form-group">
                                  <label for="nuproempenho">Número do protocolo:</label>
                                  <input type="text" name="nuproempenho" id="nuproempenho" readonly class="form-control" value="{{$empenho->nuproempenho}}">
                                </div>
                                </div>
                                <hr>

                                <div class="row">
                                  <div class="col-12 form-group">
                                    <label for="obsempenho">Observação do Solicitante:</label>
                                    <input type="text" name="obsempenho" id="obsempenho" class="form-control" value="">
                                  </div>
                                </div>

                                <button type="submit" class="btn btn-primary">Salvar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function k(i) {
                var v = i.value.replace(/\D/g,'');
                v = (v/100).toFixed(2) + '';
                v = v.replace(".", ",");
                v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                i.value = v;
            }
</script>
@endsection
