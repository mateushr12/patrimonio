@extends('layout_admin.principal')
@section ('breadcrumb','Liquidar')
@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Liquidar Empenho</strong>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body card-block">
                            <form class="" action="{{route('liquidar.salvar', $empenho->id)}}" method="post">
                                {{ csrf_field() }}
                                {{-- <input type="hidden" name="_method" value="put"> --}}
                                {{-- <input type="hidden" name="liquidado" value="2"> --}}
                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="numero">Nº Empenho</label>
                                    <input type="text" class="form-control" readonly value="{{ $empenho->numero }}" >
                                  </div>
                                  <div class="col-6 form-group">
                                    <label for="numero">Valor à liquidar (R$)</label>
                                    <input type="text" class="form-control" readonly value="{{ number_format(($empenho->valorempenho - $vliquidado),2) }}" >
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="numero">Valor*</label>
                                    <input type="text" class="form-control" onkeyup="k(this);" placeholder="0.00" name="valor" required >
                                  </div>
                                  <div class="col-6 form-group">
                                    <label for="numero">Data de Liquidação*</label>
                                    <input type="date" class="form-control" name="data" id="data" required >
                                  </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Liquidar</button>
                            </form>
                            <hr>
                            <div class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                    <tr>
                                        <th>Data de Liquidação</th>
                                        <th>Valor (R$)</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($liquidados as $liquidado)
                                    <tr>
                                      <td>{{\Carbon\Carbon::parse($liquidado->data)->format('d/m/Y')}}</td>
                                      <td>{{ $liquidado->valor }}</td>
                                      <td>
                                          <div class="table-data-feature">
                                              {{-- {{route('empenho.deletar' , $liquidado->id)}} --}}
                                              <a href="{{ route('liquidar.deletar',$liquidado->id) }}"
                                                  data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                  <button type="button" class="item" title="Deletar"
                                                  data-toggle="tooltip" data-placement="top" title=""
                                                      data-original-title="Deletar"
                                                  >
                                                      <i class="zmdi zmdi-delete"></i>
                                                  </button>
                                              </a>
                                          </div>
                                      </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function k(i) {
                var v = i.value.replace(/\D/g,'');
                v = (v/100).toFixed(2) + '';
                v = v.replace(".", ".");
                v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                i.value = v;
            }
</script>
@endsection
