@extends('layout_admin.principal')
@section ('breadcrumb','Cota Mensal')
@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6">
                                    <strong>Cota Mensal</strong>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="table-responsive table-responsive-data2">
                                      <table class="table table-data2">
                                          <thead>
                                              <tr>
                                                  <th>Ação</th>
                                                  <th>Mês de Ref.</th>
                                                  <th>Valor (R$)</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              @foreach ($cotas as $cota)
                                                  <tr>
                                                        <td>{{ $cota->acao->CodAcao }} - {{ $cota->acao->DescAcao }}</td>
                                                        <td>
                                                            @switch($cota['mes'])
                                                                @case(1)
                                                                    Janeiro
                                                                    @break
                                                                @case(2)
                                                                    Fevereiro
                                                                    @break
                                                                @case(3)
                                                                    Março
                                                                    @break
                                                                @case(4)
                                                                    Abril
                                                                    @break
                                                                @case(5)
                                                                    Maio
                                                                    @break
                                                                @case(6)
                                                                    Junho
                                                                    @break
                                                                @case(7)
                                                                    Julho
                                                                    @break
                                                                @case(8)
                                                                    Agosto
                                                                    @break
                                                                @case(9)
                                                                    Setembro
                                                                    @break
                                                                @case(10)
                                                                    Outubro
                                                                    @break
                                                                @case(11)
                                                                    Novembro
                                                                    @break
                                                                @case(12)
                                                                    Dezembro
                                                                    @break
                                                                @default
                                                            @endswitch
                                                        </td>
                                                        <td>{{ number_format($cota['vl'],2,',','.') }}</td>
                                                  </tr>
                                              @endforeach
                                              <tr><td colspan="2"><strong>Total</strong></td><td><strong>{{ number_format($total,2,',','.') }}</strong></td></tr>
                                          </tbody>
                                      </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- <div class="modal fade" id="FormCota" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="FormModalLabel">Cadastro de Cota Mensal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form class="" action="{{route('cota.salvar')}}" method="post">
                            {{ csrf_field() }}
                            @include('erp.cota.form')
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Salvar
                </button>
                <button class="btn btn-warning" type="reset" >
                    Limpar
                </button>
            </div>
        </div>
        </form>
    </div>
</div> --}}

<script>

    function k(i) {
                        var v = i.value.replace(/\D/g,'');
                        v = (v/100).toFixed(2) + '';
                        v = v.replace(".", ".");
                        v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                        v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                        i.value = v;
                    }


</script>

@endsection
