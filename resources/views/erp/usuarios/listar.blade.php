@extends('layout_admin.principal')
@section ('breadcrumb','Usuario')

@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                @if (session('sucesso'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{session('sucesso')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif (session('delete'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{session('delete')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="col-12">
                    <h3>Usuários</h3>
                    <div class="table-data__tool">
                        <div class="table-data__tool-left"></div>
                        <div class="table-data__tool-right">
                            <a class="au-btn au-btn-icon au-btn--green au-btn--small"
                                href="{{route('usuarios.cadastrar')}}">
                                <i class="zmdi zmdi-plus"></i>Novo Usuário</a>
                        </div>
                    </div>
                    <div class="card">
                        <table class="table table-borderless table-data3">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>Grupo</th>
                                    <th>Ativo</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $key)
                                <tr>
                                    <td>{{$key->name}}</td>
                                    <td>{{$key->email}}</td>
                                    <td>{{$key->grupo->nome}}</td>
                                    <td>
                                        @if ($key->ativo == 1)
                                        <span style="color: green;">Sim</span>
                                        @else
                                        <span style="color: red;">Não</span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="table-data-feature">
                                            <button class="item" data-toggle="tooltip" data-placement="top" title=""
                                                data-original-title="Editar" onclick="return editar({{$key->id}})">
                                                <i class="zmdi zmdi-edit"></i>
                                            </button>
                                            <button class="item" data-toggle="tooltip" data-placement="top" title=""
                                                data-original-title="Redefinir senha"
                                                onclick="return redefinirSenha({{$key->id}})">
                                                <i class="zmdi zmdi-refresh"></i>
                                            </button>
                                            <button class="item" data-toggle="tooltip" data-placement="top" title=""
                                                data-original-title="Ativar/Inativar"
                                                onclick="return myFunction({{$key->id}})">
                                                <i class="zmdi zmdi-stop"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function myFunction(i) {
            //var idK = element.id;
            var c = confirm('Tem certeza que deseja ativar/inativar?');
            if( c == true){
                var a = "/usuarios/inativarAtivar/"
                var b = i
                window.location.href = a.concat(b)
            }
        }


    function editar(i) {

            var a = "/usuarios/editar/"
            var b = i
            window.location.href = a.concat(b)

        }

    function redefinirSenha(i) {

            var c = confirm('Tem certeza que deseja alterar a senha do usuário? A senha será redefinida para 123456.');
            if( c == true){
                var a = "/usuarios/alterarSenha/"
                var b = i
                window.location.href = a.concat(b)
            }
        }

</script>

@endsection
