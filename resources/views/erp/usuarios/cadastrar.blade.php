@extends('layout_admin.principal')
@section ('breadcrumb','Usuario')

@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                @if (session('sucesso'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{session('sucesso')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif (session('erro'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{session('erro')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Cadastro de Usuário</strong>
                        </div>
                        <div class="card-body card-block">
                            <form class="" action="{{route('usuarios.salvar')}}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="Nome">Nome:</label>
                                        <input type="text" class="form-control" id="Nome" name="nome" required />
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="Email">Email:</label>
                                        <input type="email" class="form-control" id="Email" name="Email" required />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="Grupouser">Grupo:</label>
                                        <select class="form-control" id="Grupouser" name="grupouser" required>
                                            <option value="">SELECIONE O GRUPO</option>
                                            @foreach($grupo as $fonte)
                                            <option value="{{ $fonte->id }}">
                                                {{$fonte->nome}}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="Password">Senha:</label>
                                        <input type="password" class="form-control" id="Password" name="Password"
                                            required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Cadastrar Usuario</button>
                                    <button type="reset" class="btn btn-warning">Limpar</button>
                                </div>
                            </form>
                            <a href="{{route('usuarios.listar')}}">Listar Usuarios</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
