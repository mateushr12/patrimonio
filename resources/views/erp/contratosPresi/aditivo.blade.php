@extends('layout_admin.principal')
@section ('breadcrumb','Contratos Presidência / Aditivo')
@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Cadastro de Aditivo - Contrato nº {{ $n_contrato }}</strong>
                                </div>
                                <div class="col-3 text-right">
                                    <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small"
                                            data-toggle="modal" data-target="#FormModal">
                                        <i class="zmdi zmdi-plus"></i>Adcionar
                                    </button>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body">
                          <div class="row">
                              <div class="col-12">
                                  <div class="table-responsive table-responsive-data2">
                                      <table class="table table-data2">
                                          <thead>
                                              <tr>
                                                  <th>Aditivo</th>
                                                  <th>Tipo</th>
                                                  <th>Data de Assinatura</th>
                                                  <th>Vigência Inicio</th>
                                                  <th>Vigência Final</th>
                                                  <th>&nbsp;</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              @foreach ($aditivos as $aditivo)
                                              <tr>
                                                  <td>{{ $aditivo->descricao }}</td>
                                                  <td>
                                                  @switch($aditivo->tipo)
                                                      @case(1)
                                                          Aditivo
                                                          @break
                                                      @case(2)
                                                          Apostilamento
                                                          @break
                                                      @case(3)
                                                          Rerratificação
                                                          @break
                                                      @default
                                                  @endswitch
                                                  </td>
                                                  <td>{{\Carbon\Carbon::parse($aditivo->assinatura)->format('d/m/Y')}}</td>
                                                  <td>
                                                      {{\Carbon\Carbon::parse($aditivo->vigencia_i)->format('d/m/Y')}}
                                                  </td>
                                                  <td>
                                                      {{\Carbon\Carbon::parse($aditivo->vigencia_f)->format('d/m/Y')}}
                                                  </td>
                                                  <td>
                                                      <div class="table-data-feature">
                                                          {{-- <a >
                                                              <button type="button" class="item" title="Editar"
                                                              data-toggle="tooltip" data-placement="top" title=""
                                                                  data-original-title="Editar"
                                                              >
                                                                  <i class="zmdi zmdi-edit"></i>
                                                              </button>
                                                          </a>&nbsp; --}}
                                                          <a href="{{ route('contratoPres.aditivo.deletar', $aditivo->id) }}"
                                                              data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                              <button type="button" class="item" title="Deletar"
                                                              data-toggle="tooltip" data-placement="top" title=""
                                                                  data-original-title="Deletar"
                                                              >
                                                                  <i class="zmdi zmdi-delete"></i>
                                                              </button>
                                                          </a>&nbsp;
                                                      </div>
                                                  </td>
                                              </tr>
                                              @endforeach
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="FormModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="FormModalLabel">Cadastro de Aditivo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form class="" action="{{ route('contratoPres.aditivo.salvar') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id_contrato_pres" value="{{ $contrato }}" />
                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="">Aditivo:*</label>
                                    <input type="text" class="form-control" name="descricao" id="" value="" required>
                                  </div>
                                  <div class="col-6 form-group">
                                      <label for="">Tipo de aditivo:*</label>
                                      <select class="form-control" name="tipo" id="">
                                          <option value="">Selecione </option>
                                          <option value="1">Aditivo</option>
                                          <option value="2">Apostilamento</option>
                                          <option value="3">Rerratificação</option>
                                      </select>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-6 form-group" id="">
                                    <label for="">Dt. Assinatuara:*</label>
                                    <input type="date" class="form-control" name="assinatura" value="" required>
                                  </div>
                                  <div class="col-6 form-group" id="">
                                    <label for="">Dt. Anulação:</label>
                                    <input type="date" class="form-control" name="anulacao" value="" >
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-6 form-group" id="">
                                    <label for="">Inicio Vigência:*</label>
                                    <input type="date" class="form-control" name="vigencia_i" value="" required>
                                  </div>
                                  <div class="col-6 form-group" id="">
                                    <label for="">Fim Vigência:*</label>
                                    <input type="date" class="form-control" name="vigencia_f" value="" required>
                                  </div>
                                </div>

                                {{-- <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Cadastrar Aditivo</button>
                                </div>
                        </form> --}}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Salvar
                </button>
                <button class="btn btn-warning" type="reset" >
                    Limpar
                </button>
            </div>
        </div>
        </form>
    </div>
</div>

<script>
    function k(i) {
                        var v = i.value.replace(/\D/g,'');
                        v = (v/100).toFixed(2) + '';
                        v = v.replace(".", ".");
                        v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                        v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                        i.value = v;
                    }
</script>

@endsection
