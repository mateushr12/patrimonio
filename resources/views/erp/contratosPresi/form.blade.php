<div class="tab-content">
    <div class="tab-pane fade show active"  id="DadosContrato" role="tabpanel" aria-labelledby="nav-profile-tab">
        <div class="row">
            <div class="col-4 form-group">
                <label for="">Nº do Contrato<span id="ob">*</span></label>
                <input type="text" name="n_contrato" id="" class="form-control" {{isset($registro->n_contrato) ? 'disabled' : ''}}
                value="{{isset($registro->n_contrato) ? $registro->n_contrato : ''}}" required>
            </div>
            <div class="col-4 form-group">
                <label for="">Integrantes<span id="ob">*</span></label>
                <input type="text" name="desricao" id="" onkeyup="this.value = this.value.toUpperCase();" class="form-control" {{isset($registro->desricao) ? 'disabled' : ''}}
                value="{{isset($registro->desricao) ? $registro->desricao : ''}}" required>
            </div>
            <div class="col-4 form-group">
                <label for="">Tipo de Termo<span id="ob">*</span></label>
                <select class="form-control" name="tipo" required>
                    <option value="">Selecione ...</option>
                    @foreach ($tipo as $key)
                      <option {{isset($registro->tipo) && $registro->tipo == $key->id ? 'selected' : ''}}
                          value="{{ isset($registro->tipo) ? $registro->tipo : $key->id }}">
                          {{$key->descricao}}
                      </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-6 form-group">
                <label for="">Início da Vigência<span id="ob">*</span></label>
                <input type="date" name="dt_inicio" id="inicio" class="form-control" {{isset($registro->dt_inicio) ? 'disabled' : ''}}
                value="{{isset($registro->dt_inicio) ? $registro->dt_inicio : ''}}" required>
            </div>
            <div class="col-6 form-group">
                <label for="">Final da Vigência<span id="ob">*</span></label>
                <input type="date" name="dt_fim" id="fim" onblur="data(this);" class="form-control" {{isset($registro->dt_fim) ? 'disabled' : ''}}
                value="{{isset($registro->dt_fim) ? $registro->dt_fim : ''}}" required>
            </div>
        </div>

        <div class="row">
            <div class="col-6 form-group">
                <label for="">Data de Assinatura<span id="ob">*</span></label>
                <input type="date" name="dt_assinatura" id="inicio" class="form-control" {{isset($registro->dt_assinatura) ? 'disabled' : ''}}
                value="{{isset($registro->dt_assinatura) ? $registro->dt_assinatura : ''}}" required>
            </div>
            <div class="col-6 form-group">
                <label for="">Data de Publicação no Diário</label>
                <input type="date" name="dt_publicacao_diario" id="fim" onblur="data(this);" class="form-control" value="{{isset($registro->dt_publicacao_diario) ? $registro->dt_publicacao_diario : ''}}" >
            </div>
        </div>

        <div class="row">
            <div class="col-4 form-group">
                <label for="">Gestor</label>
                <input type="text" name="gestor" onkeyup="this.value = this.value.toUpperCase();" class="form-control" value="{{isset($registro->gestor) ? $registro->gestor : ''}}" >
            </div>
            <div class="col-4 form-group">
                <label for="">Parecer</label>
                <select class="form-control" name="parecer" >
                    <option value="">Selecione ...</option>
                    <option value="1"  @if(isset($registro->parecer) && $registro->parecer == 1):
                        selected
                    @endif>Sim</option>
                    <option value="2" @if(isset($registro->parecer) && $registro->parecer == 2):
                        selected
                    @endif>Não</option>
                </select>
            </div>
            <div class="col-4 form-group">
                <label for="">Publicado Site<span id="ob">*</span></label>
                <select class="form-control" name="publicado_site" id="" required>
                    <option value="">Selecione ...</option>
                    <option value="1"  @if(isset($registro->publicado_site) && $registro->publicado_site == 1):
                        selected
                    @endif>Sim</option>
                    <option value="2" @if(isset($registro->publicado_site) && $registro->publicado_site == 2):
                        selected
                    @endif>Não</option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-12 form-group">
                <label for="">Obs</label>
                <input type="text" name="obs" id="" class="form-control" value="{{isset($registro->obs) ? $registro->obs : ''}}">
            </div>
        </div>

        <div class="row">
          <div class="col-4 form-group">
              <label for="">Anulação do Contrato</label>
              <input type="date" name="dt_anulacao" id="" class="form-control" value="{{isset($registro->dt_anulacao) ? $registro->dt_anulacao : ''}}">
          </div>
        </div>
    </div>
</div>
