<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="JUCESE - Junta Comercial do Estado de Sergipe">

    <!-- Title Page-->
    <title>ERP - JUCESE</title>

    <link href="<?php echo asset('vendor/bootstrap-4.1/bootstrap.min.css')?>" rel="stylesheet" media="all">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@3.5.9/dist/jspdf.plugin.autotable.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

    <style>
      @media print {
        .btn {
          display: none !important;
        }
      }
      #btnPDF{
        background: #2579e6;
        color: white;
        font-weight: bold;
        width: 120px;
      }
    </style>

</head>

<body>


<div class="container">
        <div class="card-body card-block">
          <button type="button" class="btn" id="btnPDF">Gerar PDF</button><br>
                <table style="width: 900px">
                  <thead >
                    <tr>
                      <td style=" font-size: 22px;" ><strong>Contrato Presidência</strong><p></p></td>
                      <td style="font-size: 14px; float: right;" >Data {{ date('d/m/Y') }}</td>
                    </tr>
                  </thead>
                <tbody>
                <tr><td colspan="2">
                <table class="table table-data2" id="tabela" border="1">
                    <tbody>
                        <tr>
                          <td colspan="1"><strong >Nº Contrato:</strong><br> {{ $contrato->n_contrato }}</td>
                          <td colspan="1"><strong >Data Da Assinatura:</strong><br> {{ \Carbon\Carbon::parse($contrato->dt_assinatura)->format('d/m/Y') }}</td>
                          <td colspan="1"><strong >Publicado no Site:</strong><br> @if ($contrato->publicado_site == 1) Sim @else Não @endif</td>
                          <td colspan="2"><strong >Tipo:</strong><br>
                            {{ $contrato->termo->descricao }}
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2"><strong >Período de Vigência:</strong><br> {{ \Carbon\Carbon::parse($contrato->dt_inicio)->format('d/m/Y') }} a {{ \Carbon\Carbon::parse($contrato->dt_fim)->format('d/m/Y') }}</td>
                          <td colspan="2"><strong >Data Publicação no Diário:</strong><br>
                            @if (isset($contrato->dt_publicacao_diario))
                              {{ \Carbon\Carbon::parse($contrato->dt_publicacao_diario)->format('d/m/Y') }}
                            @else
                              -
                            @endif
                          </td>
                          <td colspan="1"><strong >Data Anulação Contrato:</strong><br>
                            @if (isset($contrato->dt_anulacao))
                              {{ \Carbon\Carbon::parse($contrato->dt_anulacao)->format('d/m/Y') }}
                            @else
                              -
                            @endif
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2"><strong>Integrantes:</strong><br> {{ $contrato->desricao }}</td>
                          <td colspan="2"><strong >Gestor:</strong><br> {{ $contrato->gestor }}</td>
                          <td colspan="1"><strong >Parecer:</strong><br> @if ($contrato->parecer == 1) Sim @elseif ($contrato->parecer == 2) Não @else - @endif </td>
                        </tr>
                        <tr><td colspan="5"><strong>Obs:</strong><br> {{ $contrato->obs }}</td></tr>
                        <tr><th colspan="5" style="text-align: center">ADITIVOS</th></tr>
                        <tr>
                          <th>Aditivo</th>
                          <th>Vigência</th>
                          <th>Assinatura</th>
                          <th>Anulação</th>
                          <th>Tipo</th>
                        </tr>
                        @foreach ($aditivo as $key)
                        <tr>
                          <td>{{ $key->descricao }}</td>
                          <td>
                            @if (isset($key->vigencia_i))
                              {{ \Carbon\Carbon::parse($key->vigencia_i)->format('d/m/Y') }} a {{ \Carbon\Carbon::parse($key->vigencia_f)->format('d/m/Y') }}
                            @else
                              -
                            @endif
                          </td>
                          <td>@if (isset($key->assinatura)) {{ \Carbon\Carbon::parse($key->assinatura)->format('d/m/Y') }} @else - @endif</td>
                          <td>@if (isset($key->anulacao)) {{ \Carbon\Carbon::parse($key->anulacao)->format('d/m/Y') }} @else - @endif</td>
                          <td>
                            @switch($key->tipo)
                                @case(1) Aditivo @break
                                @case(2) Apostilamento @break
                                @case(3) Rerratificação @break
                            @endswitch
                          </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </td></tr>
              </tbody>
                </table>
        </div>


</div>
</body>

<script>

function dataAt(){
    var data = new Date(),
        dia  = data.getDate().toString().padStart(2, '0'),
        mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
        ano  = data.getFullYear();
    return dia+"/"+mes+"/"+ano;
}

$(document).ready(function(){
  $('#btnPDF').click(function() {
    var numero = "{{ $contrato->n_contrato }}"
    var doc = new jsPDF('portrait', 'pt', 'a4')
    var base64Img = "{{ asset('images/gov-jucese-top-relatorio.png') }}"
    var hoje = new Date()
    doc.autoTable({
      html: '#tabela',
      styles: {
        lineColor: '#ccc',
        lineWidth: 1,
        fontSize: 10
      },
      headStyles: { fillColor: "#424647", fontSize: 11 },
      didParseCell: function (data, cell) {
        if (data.row.index == 4) {
          data.cell.styles.fontStyle = 'bold';
          data.cell.styles.halign = 'center';
          data.cell.styles.fillColor = '#424647';
          data.cell.styles.textColor = 'white';
        }else if (data.row.index == 5) {
          data.cell.styles.fontStyle = 'bold';
          data.cell.styles.halign = 'center';
          data.cell.styles.textColor = 'black';
        }
      },
      didDrawPage: function (data) {
      ///////////////////// Cabeçalho ///////////////////////////////////////////////
        if (base64Img) {
          doc.addImage(base64Img, 'JPEG', 40, 15, 515, 66)
        }
        doc.setFontSize(14)
        doc.setTextColor(40)
        doc.setFontType("bold")
        doc.text('Contrato Presidência', data.settings.margin.left + 0, 100)
        doc.setFontSize(9)
        doc.text('Data: ' + dataAt(), data.settings.margin.left + 445, 100)


        ///////////////////////////// Rodape ////////////////////////////////////////
        var str = 'Pág. ' + doc.internal.getNumberOfPages()
        doc.setFontSize(9)
        var pageSize = doc.internal.pageSize
        var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
        doc.text(str, data.settings.margin.left + 480, pageHeight - 25)
        doc.setFontType("bold");
        doc.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', data.settings.margin.left + 128, pageHeight - 30)
        doc.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br', data.settings.margin.left + 145, pageHeight - 20)
      },
      margin: {top: 108}
    });
    doc.save("Contrato_pres"+ numero +"_"+ dataAt() +".pdf");
  });
});

</script>

</html>
