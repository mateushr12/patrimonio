@extends('layout_admin.principal')
@section ('breadcrumb','Suprimento Depesas Anexo')
@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Despesas Suprimento - Anexar arquivos</strong>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body card-block">
                            <form class="" action="{{route('suprimento.despesa.anexarsalvar')}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="id_despesa" value="{{ $idespesa }}" />
                                <div class="row">
                                  <div class="col-5 form-group">
                                    <label for="">Descrição:*</label>
                                    <input type="text" name="descricao" class="form-control"
                                    onkeyup="this.value = this.value.toUpperCase();" required>
                                  </div>
                                  <div class="col-7">
                                    <label for="">Carregar arquivo: (PDF)</label>
                                    <input type="file" name="arquivo" id="" class="form-control" required>
                                  </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Salvar</button>
                            </form><br>
                            <a href="{{ route('suprimento.despesa',$idsupl) }}">Voltar</a>
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-borderless table-data3">
                                    <thead>
                                        <tr>
                                            <th>Descrição</th>
                                            <th>Arquivo</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($arquivo as $arq)
                                        <tr>
                                            <td>{{ $arq->descricao }}</td>
                                            <td><a href="{{ route('suprimento.despesa.arquivo', $arq->id) }}" target="_blank">{{ $arq->arquivo }}</a></td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <a
                                                    href="{{ route ('suprimento.despesa.anexardelete', $arq->id) }}"
                                                        data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                        <button type="button" class="item" title="Deletar"
                                                        data-toggle="tooltip" data-placement="top" title=""
                                                            data-original-title="Deletar"
                                                        >
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
