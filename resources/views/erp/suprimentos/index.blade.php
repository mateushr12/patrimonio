@extends('layout_admin.principal')
@section ('breadcrumb','Suprimentos de Fundo')
@section('conteudo')
  <style>
      #ob{
        color: red;
      }
  </style>
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6">
                                    <strong>Suprimentos de Fundo</strong>
                                </div>
                                <div class="col-6 text-right">
                                    <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small"
                                            data-toggle="modal" data-target="#FormCota">
                                        <i class="zmdi zmdi-plus"></i>Adicionar
                                    </button>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @elseif (session('ok'))
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                            {{session('ok')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="table-responsive-md table-responsive-data2">
                                      <table class="table table-data2">
                                          <thead>
                                              <tr>
                                                  <th>Nº Requisição</th>
                                                  <th>Data</th>
                                                  <th>Valor (R$)</th>
                                                  <th>Empenho</th>
                                                  <th>Elemento</th>
                                                  <th>Saldo disponível</th>
                                                  <th>&nbsp;</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              @foreach ($suprimentos as $suprimento)
                                              <tr>
                                                <td>{{ $suprimento->numero }}</td>
                                                <td>{{\Carbon\Carbon::parse($suprimento->data)->format('d/m/Y')}}</td>
                                                <td>{{ number_format($suprimento->valor,2,',','.') }}</td>
                                                <td>{{ isset($suprimento->empenho_id) ? $suprimento->emp->numero : '-' }}</td>
                                                <td>{{ $suprimento->elem->DescElemento }}</td>
                                                <td>{{ number_format(($suprimento->valor - $suprimento->usado),2,',','.')}}</td>
                                                <td>
                                                  <div class="table-data-feature">
                                                    <a href="{{ route('suprimento.imprimir', $suprimento->id) }}" target="_blank">
                                                        <button type="button" class="item" title="Imprimir"
                                                        data-toggle="tooltip" data-placement="top" >
                                                            <i class="zmdi zmdi-eye"></i>
                                                        </button>
                                                    </a>&nbsp;
                                                  @if (is_null($suprimento->empenho_id))
                                                    <a href="{{route('suprimento.empenho' , $suprimento->id)}}">
                                                        <button type="button" class="item" title="Empenho"
                                                        data-toggle="tooltip" data-placement="top" >
                                                            <i class="zmdi zmdi-explicit"></i>
                                                        </button>
                                                    </a>&nbsp;
                                                  @else
                                                  @endif
                                                    <a href="{{ route('suprimento.despesa', $suprimento->id) }}" >
                                                        <button type="button" class="item" title="Despesas"
                                                        data-toggle="tooltip" data-placement="top" >
                                                            <i class="zmdi zmdi-money-box"></i>
                                                        </button>
                                                    </a>&nbsp;
                                                    {{-- @if (in_array('erp/empenho/deletar/{id}', Session::get('permissoes.nomes'))) --}}
                                                    <a href="{{ route('suprimento.anexar',$suprimento->id) }}">
                                                        <button type="button" class="item" title="Anexar Arquivos"
                                                        data-toggle="tooltip" data-placement="top" title=""
                                                            data-original-title="Anexar Arquivos"
                                                        >
                                                            <i class="zmdi zmdi-attachment-alt"></i>
                                                        </button>
                                                    </a>&nbsp;
                                                    {{-- @else
                                                    @endif --}}
                                                    @if (in_array('erp/suprimentos/deletar/{id}', Session::get('permissoes.nomes')))
                                                    <a href="{{route('suprimento.deletar' , $suprimento->id)}}"
                                                      data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                      <button type="button" class="item" title="Deletar"
                                                      data-toggle="tooltip" data-placement="top"
                                                      data-original-title="Deletar">
                                                        <i class="zmdi zmdi-delete"></i>
                                                      </button>
                                                    </a>&nbsp;
                                                    @else
                                                    @endif
                                                  </div>
                                                    {{-- <div class="table-data-feature">
                                                        <a href="{{ route('empenho.pdf', $suprimento->id) }}" target="_blank">
                                                            <button type="button" class="item" title="Imprimir"
                                                            data-toggle="tooltip" data-placement="top" >
                                                                <i class="zmdi zmdi-eye"></i>
                                                            </button>
                                                        </a>&nbsp;
                                                      @if (in_array('erp/empenho/editar/{id}', Session::get('permissoes.nomes')))
                                                        <a href="{{route('empenho.editar' , $suprimento->id)}}">
                                                            <button type="button" class="item" title="Editar"
                                                            data-toggle="tooltip" data-placement="top" >
                                                                <i class="zmdi zmdi-edit"></i>
                                                            </button>
                                                        </a>&nbsp;
                                                      @else
                                                      @endif
                                                      @if (in_array('erp/empenho/deletar/{id}', Session::get('permissoes.nomes')))
                                                        <a href="{{route('empenho.deletar' , $suprimento->id)}}"
                                                            data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                            <button type="button" class="item" title="Deletar"
                                                            data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="Deletar">
                                                                <i class="zmdi zmdi-delete"></i>
                                                            </button>
                                                        </a>&nbsp;
                                                      @else
                                                      @endif
                                                      @if ($registro->tipoempenho == 1 && $registro->modempenho != 1)
                                                        @if (in_array('erp/empenho/reforco/{id}', Session::get('permissoes.nomes')))
                                                          <a href="{{route('empenho.reforco' , $registro->id)}}">
                                                              <button type="button" class="item" title="Reforço"
                                                              data-toggle="tooltip" data-placement="top" title="">
                                                                  <i class="zmdi zmdi-collection-plus"></i>
                                                              </button>
                                                          </a>
                                                        @else
                                                        @endif
                                                      @else
                                                      @endif
                                                      &nbsp;
                                                      @if (in_array('erp/empenho/pagamento/{id}', Session::get('permissoes.nomes')))
                                                        <a href="{{route('pagamento' , $registro->id)}}">
                                                            <button type="button" class="item" title="Pagar"
                                                            data-toggle="tooltip" data-placement="top" >
                                                                <i class="zmdi zmdi-money-box"></i>
                                                            </button>
                                                        </a>
                                                      @else
                                                      @endif
                                                    </div> --}}
                                                </td>
                                              </tr>
                                              @endforeach
                                          </tbody>
                                      </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="FormCota" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="FormModalLabel">Cadastro de Suprimento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form class="" action="{{route('suprimento.salvar')}}" method="post">
                            {{ csrf_field() }}
                            @include('erp.suprimentos.form')
                    </div>
                </div>
                <span id="ob">*</span> campo obrigatório
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Salvar
                </button>
                <button class="btn btn-warning" type="reset" >
                    Limpar
                </button>
            </div>
        </div>
        </form>
    </div>
</div>




<script>
    function k(i) {
                var v = i.value.replace(/\D/g,'');
                v = (v/100).toFixed(2) + '';
                v = v.replace(".", ",");
                v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                i.value = v;
            }
</script>

@endsection
