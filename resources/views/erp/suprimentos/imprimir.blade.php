<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="JUCESE - Junta Comercial do Estado de Sergipe">

    <!-- Title Page-->
    <title>ERP - JUCESE</title>

    <link href="<?php echo asset('vendor/bootstrap-4.1/bootstrap.min.css')?>" rel="stylesheet" media="all">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@3.5.9/dist/jspdf.plugin.autotable.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

    <style>
      @media print {
        .btn {
          display: none !important;
        }
      }
      #btnPDF{
        background: #2579e6;
        color: white;
        font-weight: bold;
        width: 150px;
      }
    </style>

</head>

<body>


<div class="container">

        <div class="card-body card-block">
          <button type="button" class="btn" id="btnPDF">Gerar PDF</button><br>
          <img src="../../../images/gov-jucese-top-relatorio.jpg" hidden>
            <div class="table-responsive table-responsive-data2">
                <table style="width: 900px">
                  <thead >
                    <tr>
                      <td style=" font-size: 22px;" ><strong>Requisição de Suprimento de Fundo</strong><p></p></td>
                      <td style="font-size: 14px; float: right;" >Data {{ date('d/m/Y') }}</td>
                    </tr>
                  </thead>
                <tbody>
                <tr><td colspan="2">
                <table class="table table-data2" id="tabela" border="1">
                    <tbody>
                        <tr>
                          <td colspan="2"><strong >Nº Requisição:</strong><br> {{ $s->numero }}</td>
                          <td colspan="3"><strong >Data De Requisição:</strong><br> {{ \Carbon\Carbon::parse($s->data)->format('d/m/Y') }}</td>
                        </tr>
                        <tr>
                          <td colspan="2"><strong >Empenho:</strong><br> @if (isset($s->empenho_id)) {{ $s->emp->numero }} @else - @endif</td>
                          <td colspan="3"><strong >Valor:</strong><br>R$ {{ number_format($s->valor,2,',','.') }}</td>
                        </tr>
                        <tr><td colspan="5"><strong>Ação:</strong><br> {{ $s->acaoId->DescAcao }}</td></tr>
                        <tr><td colspan="5"><strong>Elemento:</strong><br> {{ $s->elem->DescElemento }}</td></tr>
                        <tr><th colspan="5" style="text-align: center">DEPESAS</th></tr>
                        <tr>
                          <th>Fornecedor</th>
                          <th>Objeto</th>
                          <th>N.F.</th>
                          <th>Nº Cheque</th>
                          <th>Valor (R$)</th>
                        </tr>
                        @foreach ($d as $key)
                        <tr>
                          <td>{{ $key->forn->nome }}</td>
                          <td>{{ $key->objeto }}</td>
                          <td>{{ $key->nf }}</td>
                          <td>{{ $key->n_cheque }}</td>
                          <td>{{ number_format($key->valor,2,',','.') }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </td></tr>
              </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
</body>

<script>

function dataAt(){
    var data = new Date(),
        dia  = data.getDate().toString().padStart(2, '0'),
        mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
        ano  = data.getFullYear();
    return dia+"/"+mes+"/"+ano;
}

$(document).ready(function(){
  $('#btnPDF').click(function() {
    var numero = "{{ $s->numero }}"
    var doc = new jsPDF('portrait', 'pt', 'a4')
    var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"
    var img = new Image()
    img.src = base64Img
    var hoje = new Date()
    doc.autoTable({
      html: '#tabela',
      //  columnStyles: {
      //    0: { cellWidth: 180 },
      //    1: { cellWidth: 170 }
      // },
      styles: {
        lineColor: '#ccc',
        lineWidth: 1,
        fontSize: 10
      },
      headStyles: { fillColor: "#424647", fontSize: 11 },
      didParseCell: function (data, cell) {
        if (data.row.index == 4) {
          data.cell.styles.fontStyle = 'bold';
          data.cell.styles.halign = 'center';
          data.cell.styles.fillColor = '#424647';
          data.cell.styles.textColor = 'white';
        }else if (data.row.index == 5) {
          data.cell.styles.fontStyle = 'bold';
          data.cell.styles.halign = 'center';
          data.cell.styles.textColor = 'black';
        }
      },
      didDrawPage: function (data) {
      ///////////////////// Cabeçalho ///////////////////////////////////////////////
        if (base64Img) {
          doc.addImage(img, 'JPEG', 40, 15, 515, 66)
        }
        doc.setFontSize(14)
        doc.setTextColor(40)
        doc.setFontType("bold")
        doc.text('Requisição de Suprimento de Fundo', data.settings.margin.left + 0, 100)
        doc.setFontSize(9)
        doc.text('Data: ' + dataAt(), data.settings.margin.left + 445, 100)


        ///////////////////////////// Rodape ////////////////////////////////////////
        var str = 'Pág. ' + doc.internal.getNumberOfPages()
        doc.setFontSize(9)
        var pageSize = doc.internal.pageSize
        var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
        doc.text(str, data.settings.margin.left + 480, pageHeight - 25)
        doc.setFontType("bold");
        doc.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', data.settings.margin.left + 128, pageHeight - 30)
        doc.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br', data.settings.margin.left + 145, pageHeight - 20)
      },
      margin: {top: 108}
    });
    // doc.autoTable({
    //   html: '#tabela2'
    // })
    doc.save("SuprFundos_"+ numero +"_"+ dataAt() +".pdf");
  });
});

</script>

</html>
