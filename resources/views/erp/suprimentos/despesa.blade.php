@extends('layout_admin.principal')
@section ('breadcrumb','Despesas')
@section('conteudo')
  <style>
      #ob{
        color: red;
      }
  </style>
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Despesa de suprimento</strong>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body card-block">
                            <form class="" action="{{ route('suprimento.despesa.salvar', $suprimento->id) }}" method="post">
                                {{ csrf_field() }}
                                {{-- route('', $suprimento->id) --}}
                                {{-- <input type="hidden" name="_method" value="put"> --}}
                                <input type="hidden" name="suprimento_id" value="{{ $suprimento->id }}">
                                {{-- onkeyup="this.value = this.value.toUpperCase();" --}}
                                <div class="row">
                                  <div class="col-4 form-group">
                                    <label for="supr">Suprimento</label>
                                    <input type="text" class="form-control" readonly value="{{ $suprimento->numero }}" >
                                  </div>
                                  <div class="col-4 form-group">
                                    <label for="vl">Valor<span id="ob">*</span></label>
                                    <input type="text" class="form-control" onkeyup="k(this);" placeholder="0.00" name="valor" required >
                                  </div>
                                  <div class="col-4 form-group">
                                    <label for="data">Saldo</label>
                                    <input type="text" class="form-control" value="{{ number_format($vldisp,2,',','.') }}" readonly>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="nf">N.F.</label>
                                    <input type="text" class="form-control" name="nf" >
                                  </div>
                                  <div class="col-6 form-group">
                                    <label for="nf">Nº Cheque</label>
                                    <input type="text" class="form-control" name="n_cheque" >
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-12 form-group">
                                    <label for="forn">Fornecedor<span id="ob">*</span></label>
                                    <select name="fornecedor_id" id="forn" class="form-control" >
                                      <option value="">Selecione uma opção</option>
                                      @foreach($fornecedor as $key)
                                      <option value="{{$key->id }}">
                                        {{$key->nome}}
                                      </option>
                                      @endforeach
                                    </select>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-12 form-group">
                                    <label for="ob">Objeto<span id="ob">*</span></label>
                                    <input type="text" class="form-control" name="objeto"
                                    onkeyup="this.value = this.value.toUpperCase();"
                                    required >
                                  </div>
                                </div>
                                <span id="ob">*</span> campo obrigatório<br>
                                <button type="submit" class="btn btn-primary">Cadastrar</button>
                            </form>

                            <hr>
                            <div class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Fornecedor</th>
                                        <th>Objeto</th>
                                        <th>Valor (R$)</th>
                                        <th>N.F.</th>
                                        <th>Nº Cheque</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($despesa as $key)
                                    <tr>
                                      <td>{{ $key->id }}</td>
                                      <td>{{ $key->forn->nome }}</td>
                                      <td>{{ $key->objeto }}</td>
                                      <td>{{ $key->valor }}</td>
                                      <td>{{ $key->nf }}</td>
                                      <td>{{ $key->n_cheque }}</td>
                                      {{-- <td>{{\Carbon\Carbon::parse($key->data)->format('d/m/Y')}}</td> --}}
                                      <td>
                                          <div class="table-data-feature">
                                              <a href="{{route('suprimento.despesa.deletar' , $key->id)}}"
                                                  data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                  <button type="button" class="item" title="Deletar"
                                                  data-toggle="tooltip" data-placement="top" title=""
                                                      data-original-title="Deletar"
                                                  >
                                                      <i class="zmdi zmdi-delete"></i>
                                                  </button>
                                              </a>&nbsp;
                                              <a href="{{ route('suprimento.despesa.anexar',$key->id) }}">
                                                  <button type="button" class="item" title="Anexar Arquivos"
                                                  data-toggle="tooltip" data-placement="top" title=""
                                                      data-original-title="Anexar Arquivos"
                                                  >
                                                      <i class="zmdi zmdi-attachment-alt"></i>
                                                  </button>
                                              </a>
                                          </div>
                                      </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function k(i) {
                var v = i.value.replace(/\D/g,'');
                v = (v/100).toFixed(2) + '';
                v = v.replace(".", ".");
                v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                i.value = v;
            }
</script>
@endsection
