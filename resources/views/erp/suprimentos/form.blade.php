<div class="row">
  <div class="col-4 form-group">
    <label for="numero">Nº Requisição:<span id="ob">*</span></label>
    <input type="text" name="numero" id="numero" required class="form-control"
    onkeyup="this.value = this.value.toUpperCase();"
    value="{{ isset($supri->numero) ? $supri->numero : '' }}" >
  </div>
  <div class="col-4 form-group">
    <label for="data">Data:<span id="ob">*</span></label>
    <input type="date" name="data" id="data" class="form-control"
    {{isset($supri->data) ? 'disabled' : ''}}
    value="{{isset($supri->data) ? $supri->data : ''}}" required>
  </div>
  <div class="col-4 form-group">
    <label for="valorS">Valor da Solicitação (R$):<span id="ob">*</span></label>
    <input type="text" name="valor" id="valorS" class="form-control" onkeyup="k(this);" required
    {{isset($supri->valor) ? 'disabled' : ''}}
    value="{{isset($supri->valor) ? $supri->valor : ''}}">
  </div>
</div>

<div class="row">
  <div class="col-12 form-group">
    <label for="elem">Elemento de Despesa:<span id="ob">*</span></label>
    <select name="elemento_id" id="elem" class="form-control"
    {{isset($supri->elemento_id) ? 'disabled' : ''}}
    required>
    <option value="">Selecione um elemento</option>
    @foreach($elementos as $elemento)
      <option {{isset($supri->elemento_id) && $supri->elemento_id == $elemento->id ? 'selected' : ''}}
        value="{{$elemento->id }}">
        {{$elemento->CodElemento}} - {{$elemento->DescElemento}}
      </option>
    @endforeach
  </select>
</div>
</div>

<div class="row">
  <div class="col-12 form-group">
    <label for="acao">Açao/Projeto/Atividade:<span id="ob">*</span></label>
    <select name="acao_id" id="acao" class="form-control" required
    {{isset($supri->acao_id) ? 'disabled' : ''}} >
    <option value="">Selecione uma ação</option>
    @foreach($acoes as $acao)
      <option
      {{isset($supri->acao_id) && $supri->acao_id == $acao->id ? 'selected' : ''}}
      value="{{$acao->id }}">
      {{$acao->CodAcao}} - {{$acao->DescAcao}}
    </option>
  @endforeach
</select>
</div>
</div>
