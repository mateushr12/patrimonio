@extends('layout_admin.principal')
@section ('breadcrumb','Empenho Suprimento')
@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Empenho Suprimento</strong>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body card-block">
                            <form class="" action="{{ route('suprimento.empenho.salvar', $suprimento->id) }}" method="post">
                                {{ csrf_field() }}
                                {{-- <input type="hidden" name="_method" value="put"> --}}
                                <input type="hidden" name="tipoempenho" value="1">
                                <div class="row">
                                  <div class="col-3 form-group">
                                    <label for="numero">Nº Empenho:*</label>
                                    <input type="text" class="form-control" name="numero" id="numero" required
                                    onkeyup="this.value = this.value.toUpperCase();" >
                                  </div>
                                  <div class="col-3 form-group">
                                    <label for="bancoprecoempenho">Banco de Preço:*</label>
                                    <select name="bancoprecoempenho" id="bancoprecoempenho" class="form-control" >
                                      <option value="">Selecione uma opção</option>
                                      <option value="1">Sim</option>
                                      <option value="2">Não</option>
                                    </select>
                                  </div>
                                  <div class="col-3 form-group">
                                    <label for="dataempenho">Data do Empenho:*</label>
                                    <input type="date" name="dataempenho" id="dataempenho" class="form-control" required
                                    value="" >
                                  </div>
                                  <div class="col-3 form-group">
                                    <label for="urgempenho">Urgência:*</label>
                                    <select name="urgempenho" id="urgempenho" class="form-control" >
                                      <option value="">Selecione uma opção</option>
                                      <option value="1">Sim</option>
                                      <option value="2">Não</option>
                                    </select>
                                  </div>
                                </div>
                                <hr>

                                <div class="row">
                                  <div class="col-4 form-group">
                                    <label for="modempenho">Modalidade:*</label>
                                    <select name="modempenho" id="modempenho" class="form-control" >
                                      <option value="">Selecione uma opção</option>
                                      <option value="1" >Ordinário</option>
                                      <option value="2" >Estimativo</option>
                                      <option value="3" >Global</option>
                                    </select>
                                  </div>
                                  <div class="col-4 form-group">
                                    <label for="modlicempenho">Modalidade de Licitação:*</label>
                                    <select name="modlicempenho" id="modlicempenho" class="form-control" >
                                      <option value="">Selecione uma opção</option>
                                      @foreach($modLs as $modL)
                                      <option value="{{$modL->id }}">
                                        {{$modL->descricao}}
                                      </option>
                                    @endforeach
                                  </select>
                                </div>
                                <div class="col-4 form-group">
                                  <label for="despempenho">Tipo de despesa:*</label>
                                  <select name="despempenho" id="despempenho" class="form-control" >
                                    <option value="">Selecione uma opção</option>
                                    <option value="3" >3 - SUPRIMENTO INDIVIDUAL</option>
                                    <option value="4" >4 - SUPRIMENTO INSTITUCIONAL</option>
                                  </select>
                                </div>
                                </div>
                                <hr>

                                <div class="row">
                                  <div class="col-12 form-group">
                                    <label for="acaoempenho">Célula Orçamentária:*</label>
                                    <select name="acaoempenho" id="acaoempenho" class="form-control" required >
                                    <option value="">Selecione uma ação</option>
                                    <option
                                      value="{{$acao->id }}">{{$acao->CodAcao}} - {{$acao->DescAcao}}
                                    </option>
                                </select>
                                </div>
                                </div>

                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="elemempenho">Elemento de Despesa:*</label>
                                    <select name="elemempenho" id="elemempenho" class="form-control"
                                    >
                                      <option value="">Selecione uma elemento</option>
                                      <option value="{{$elemento->id}}">
                                        {{$elemento->CodElemento}} - {{$elemento->DescElemento}}
                                      </option>
                                  </select>
                                  </div>
                                    <div class="col-6 form-group">
                                      <label for="subelem">Sub-Elemento de Despesa:*</label>
                                      {!! Form::select('subelemento', [], ['required'] ) !!}
                                    </div>
                                </div>

                                <div class="row">
                                  <div class="col-12 form-group">
                                    <label for="fichaempenho">Ficha Financeira:</label>
                                    <input type="text" name="fichaempenho" id="fichaempenho" readonly class="form-control" value="">
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="convempenho">Convênio:</label>
                                    <input type="text" name="convempenho" id="convempenho" class="form-control" value="">
                                  </div>
                                  <div class="col-6 form-group">
                                    <label for="valorempenho">Valor da Solicitação (R$):*</label>
                                    <input type="text" name="valorempenho" id="valorempenho" class="form-control" onkeyup="k(this);" required
                                    value="" >
                                  </div>
                                </div>
                                <hr>
                                <!--{ Tela de banco de preço | SIM}-->
                                <div class="precoempenho" id="precosim">
                                  <div class="row">
                                    <div class="col-12 form-group">
                                      <label for="licempenho">Licitação:</label>
                                      <select name="licempenho" id="licempenho" class="form-control"
                                      >
                                      <option value="">Selecione uma opção</option>
                                      @foreach($contratos as $contrato)
                                        <option value="{{$contrato->id }}">
                                          {{$contrato->numerocontrato}} - {{$contrato->fornecedor->nome}}
                                        </option>
                                    @endforeach
                                  </select>
                                </div>
                                </div>
                                <div class="row">
                                  <div class="col-12 form-group">
                                    <label for="protoempenho">Origem do protocolo:</label>
                                    <select name="protoempenho" id="protoempenho" class="form-control" >
                                      <option value="">Selecione uma opção</option>
                                      <option value="1" >i-Gesp</option>
                                      <option value="2" >Outros</option>
                                    </select>
                                  </div>
                                </div>
                                </div>
                                <!--{ Fim da Tela de banco de preço | SIM}-->
                                <!--{ Tela de banco de preço | Não}-->
                                <div class="precoempenho" id="preconao">
                                  <div class="row">
                                    <div class="col-4 form-group">
                                      <label for="credorempenho">Credor:</label>
                                      <select name="credorempenho" id="credorempenho" class="form-control" >
                                        <option value="1" >Individual</option>
                                        <option value="2" >Coletivo</option>
                                      </select>
                                    </div>
                                    <div class="col-4 form-group">
                                      <label for="doccredor">Documento</label>
                                      <select name="doccredor" id="doccredor" class="form-control" >
                                        <option value="">Selecione uma opção</option>
                                        <option value="1" >CPF</option>
                                        <option value="2" >CNPJ</option>
                                        <option value="3" >IG</option>
                                        <option value="4" >UXxGESTÃO</option>
                                      </select>
                                    </div>
                                    <div class="col-4 form-group">
                                      <label for="numerodoccredor">Nº Documento:</label>
                                      <input type="text" name="numerodoccredor" id="numerodoccredor" class="form-control" value="">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-12 form-group">
                                      <label for="nomecredor">Nome do credor:</label>
                                      <input type="text" name="nomecredor" id="nomecredor" class="form-control" value="">
                                    </div>
                                  </div>
                                </div>
                                <!--{ Fim da Tela de banco de preço | Não}-->
                                <div class="row">
                                  <div class="col-8 form-group">
                                    <label for="reflegalempenho">Referência Legal:*</label>
                                    <select name="reflegalempenho" id="reflegalempenho" class="form-control" >
                                      <option value="">Selecione uma opção</option>
                                      @foreach($refs as $ref)
                                        <option value="{{$ref->id }}">
                                          {{$ref->descricao}}
                                        </option>
                                    @endforeach
                                  </select>
                                </div>
                                <div class="col-4 form-group">
                                  <label for="nuproempenho">Número do protocolo:</label>
                                  <input type="text" name="nuproempenho" id="nuproempenho" class="form-control" value="">
                                </div>
                                </div>
                                <hr>

                                <div class="row">
                                  <div class="col-12 form-group">
                                    <label for="obsempenho">Observação do Solicitante:</label>
                                    <input type="text" name="obsempenho" id="obsempenho" class="form-control" value="">
                                  </div>
                                </div>

                                <button type="submit" class="btn btn-primary">Salvar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function k(i) {
                var v = i.value.replace(/\D/g,'');
                v = (v/100).toFixed(2) + '';
                v = v.replace(".", ",");
                v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                i.value = v;
            }
</script>
@endsection
