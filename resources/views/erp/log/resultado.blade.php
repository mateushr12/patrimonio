@extends('layout_admin.principal')
@section ('breadcrumb','Log')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-10">
                                      <h3>Resultado da busca</h3>
                                    </div>
                                    {{-- <div class="col-2">
                                        <a class="au-btn au-btn-icon au-btn--green au-btn--small"
                                            href="{{route('taxas.pdf')}}" target="_blank">
                                            <i class="zmdi zmdi-print"></i>Imprimir</a>
                                    </div> --}}
                                </div>
                            </div>
                            <div class="card-body card-block">
                                @if(count($log) > 0)
                                <table class="table table-sm" >
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>tabela</th>
                                            <th>linha</th>
                                            <th>usuário</th>
                                            <th>data</th>
                                            <th>ação</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($log as $key)
                                        <tr>
                                            <td>{{ $key->id }}</td>
                                            <td>{{ $key->tabela }}</td>
                                            <td>{{ $key->idLinha }}</td>
                                            <td>{{ $key->user }}</td>
                                            <td>{{ $key->data }}</td>
                                            <td>
                                              @switch($key->acao)
                                                @case('salvar') <h4><span class="badge badge-pill badge-success">{{ $key->acao }}</span></h4> @break
                                                @case('deletar') <h4><span class="badge badge-pill badge-danger">{{ $key->acao }}</span></h4> @break
                                                @case('atualizar') <h4><span class="badge badge-pill badge-warning">{{ $key->acao }}</span></h4> @break
                                              @endswitch
                                            </td>
                                            <td>
                                              <form action="{{ route('log.tabela') }}" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="table" value="{{ $key->tabela }}" />
                                                <input type="hidden" name="id" value="{{ $key->idLinha }}" />
                                                <input type="submit" class="btn btn-primary" value="Ir para" />
                                              </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                  Nenhuma taxa encontrada.
                                @endif
                                <a href="{{ route('log') }}" class="btn btn-info">Nova Busca</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>


@endsection
