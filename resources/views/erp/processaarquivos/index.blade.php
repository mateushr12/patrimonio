@extends('layout_admin.principal')
@section ('breadcrumb','Processamento de Arquivos')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                    @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @if($files->count())
                    <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Processando Arquivo de Retorno</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">
                                <table class="table">
                                    <th>Name</th>
                                    <th>Data Envio</th>
                                    <th>Data Processamento</th>
                                    <th>Status</th>
                                    <th>Ação</th>
                                    @foreach($files as $file)
                                    <tr>
                                    <td>{{ $file->filename }}</td>
                                    <td>{{\Carbon\Carbon::parse($file->created_at)->format('d/m/Y')}}</td>
                                    <td>{{ $file->dataprocessamento }}</td>
                                    <td>
                                        @if ($file->status == 0)
                                        <span class="badge badge-danger">Aguardando Processamento.</span>
                                        @else
                                        <span class="badge badge-succes">Processado.</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('ProcessaArquivo.store' , $file->id)}}">
                                            <button type="button" class="item" title="Processar" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">
                                            <i class="zmdi zmdi-edit"></i></button>
                                        </a>&nbsp;



                                    </td>
                                    </tr>
                                    @endforeach
                                </table>
                                @else
                                Você ainda não possui arquivos.!
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
