@extends('layout_admin.principal')
@section ('breadcrumb','Conteúdo Caixas Anexo')
@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Conteúdo Caixa - Anexar arquivos</strong>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body card-block">
                            <form class="" action="{{route('caixa.anexarsalvar')}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="id_conteudos" value="{{ $ids }}" />
                                <div class="row">
                                  <div class="col-5 form-group">
                                    <label for="">Descrição:*</label>
                                    <input type="text" name="descricao" class="form-control"
                                    onkeyup="this.value = this.value.toUpperCase();" required>
                                  </div>
                                  <div class="col-7">
                                    <label for="">Carregar arquivo: (PDF)</label>
                                    <input type="file" name="arquivo" id="" class="form-control" required>
                                  </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Salvar</button>
                            </form><br>
                            <a href="{{ route('caixa.processo', $caixaid) }}">Voltar</a>
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-borderless table-data3">
                                    <thead>
                                        <tr>
                                            <th>Descrição</th>
                                            <th>Imagem</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($arquivo as $arq)
                                        <tr>
                                            <td>{{ $arq->descricao }}</td>
                                            <td><a href="{{ route('caixa.arquivo', $arq->id) }}" target="_blank">{{ $arq->arquivo }}</a></td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <a
                                                    href="{{ route ('caixa.anexardelete', $arq->id) }}"
                                                        data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                        <button type="button" class="item" title="Deletar"
                                                        data-toggle="tooltip" data-placement="top" title=""
                                                            data-original-title="Deletar"
                                                        >
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
