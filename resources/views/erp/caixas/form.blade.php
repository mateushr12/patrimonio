<div class="row">
  <div class="col-12 form-group">
    <label for="elem">Setor:<span id="ob">*</span></label>
    <select name="tipo_id" id="tipo" class="form-control"
    required>
    <option value="">Selecione um setor</option>
    @foreach($tipo as $key)
      <option
      value="{{$key->id }}"> {{$key->descricao}}
    </option>
  @endforeach
  </select>
  </div>
</div>

<div class="row">
  <div class="col-12 form-group">
    <label for="numero">Observação:</label>
    <input type="text" name="obs" class="form-control" />
  </div>
</div>
