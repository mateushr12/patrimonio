@extends('layout_admin.principal')
@section ('breadcrumb','Conteúdo Caixas')
@section('conteudo')


<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
<style>
    #ob{
      color: red;
    }
</style>
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Conteúdo Caixas</strong>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body card-block">
                            <form class="" action="{{ route('caixa.processo.salvar') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="caixa_id" value="{{ $caixa->id }}">
                                {{-- onkeyup="this.value = this.value.toUpperCase();" --}}
                                <div class="row">
                                    <div class="form-group col-sm-12" >
                                        <label for="Permi">Nome/Credor:<span id="ob">*</span></label>
                                        <input type="text" name="nome" class="form-control" id="nome" value=""  onkeyup="this.value = this.value.toUpperCase();" required>
                                    </div>
                                </div>
                                <div class="row">
                                  <div class="col-6 form-group" >
                                    <label for="processo">CPF/CNPJ:</label>
                                    <input type="text" name="cpfCnpj" class="form-control" id="cnpj" value="" onblur="validar(this)">
                                  </div>
                                  <div class="col-6 form-group" >
                                    <label for="processo">Conteúdo:</label>
                                    <input type="text" name="conteudo" class="form-control" value="" >
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-6 form-group" >
                                    <label for="processo">E-DOC/Processo:</label>
                                    <input type="text" name="processo" class="form-control" value="" >
                                  </div>
                                  <div class="col-6 form-group" >
                                    <label for="processo">Data:<span id="ob">*</span></label>
                                    <input type="month" name="data" class="form-control" value="" required>
                                  </div>
                                </div>
                                <span id="ob">*</span> campo obrigatório<br>
                                <button type="submit" class="btn btn-primary">Cadastrar</button>
                                <button type="reset" class="btn btn-warning">Limpar</button>
                            </form>
                            <hr>
                            <div class="table-responsive table-responsive-data2">
                            @if (count($processos))
                            <table class="table table-data2">
                                <thead>
                                    <tr>
                                        <th>Caixa</th>
                                        <th>Nome/Credor</th>
                                        <th>CPF/CNPJ</th>
                                        <th>Conteúdo</th>
                                        <th>E-doc/Processo</th>
                                        <th>Data</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($processos as $key)
                                    <tr>
                                      <td>{{ $key->caixa_id }}</td>
                                      <td>{{ $key->nome }}</td>
                                      <td>{{ $key->cpfCnpj }}</td>
                                      <td>{{ $key->conteudo }}</td>
                                      <td>{{ $key->processo }}</td>
                                      <span hidden>
                                        <?php $dt = explode('-', $key->data); ?>
                                      </span>
                                      <td>
                                        {{ $dt[1].'/'.$dt[0] }}
                                      </td>
                                      <td>
                                          <div class="table-data-feature">
                                              <a href="{{route('caixa.processo.deletar' , $key->id)}}"
                                                  data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                  <button type="button" class="item" title="Deletar"
                                                  data-toggle="tooltip" data-placement="top" title=""
                                                      data-original-title="Deletar"
                                                  >
                                                      <i class="zmdi zmdi-delete"></i>
                                                  </button>
                                              </a>&nbsp;
                                              <a href="{{ route('caixa.anexar',$key->id) }}">
                                                  <button type="button" class="item" title="Anexar Arquivos"
                                                  data-toggle="tooltip" data-placement="top" title=""
                                                      data-original-title="Anexar Arquivos"
                                                  >
                                                      <i class="zmdi zmdi-attachment-alt"></i>
                                                  </button>
                                              </a>
                                          </div>
                                      </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                              Nenhum processo cadastrado.
                            @endif
                            </div>
                            <a href="{{ route('caixas') }}">Caixas</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
    function k(i) {
      var v = i.value.replace(/\D/g,'');
      v = (v/100).toFixed(2) + '';
      v = v.replace(".", ".");
      v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
      v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
      i.value = v;
    }

    $(function () {
      var route = "{{ url('/erp/caixas/auto') }}";
      $('#nome').autocomplete({
        source:function(request,response){
          $.ajax({
            url: route,
            dataType: "json",
            data: {
              term : request.term
            },
            success: function(data) {
              var ar =  $.map( data, function( item ) {
                return {
                  value: item.nome,
                  numeroident: item.numeroident
                }
              })
              response($.ui.autocomplete.filter(ar,request.term));
            }
          });
        },
        minLength:1,
        delay:300,
        select:function(event,ui){
          $('#cnpj').val(ui.item.numeroident)
        }
      })
    })

    $("#cnpj").keydown(function(){
      try {
          $("#cnpj").unmask();
      } catch (e) {}
      var tamanho = $("#cnpj").val().length;
      if(tamanho < 11){
          $("#cnpj").mask("999.999.999-99");
      } else {
          $("#cnpj").mask("99.999.999/9999-99");
      }
      // ajustando foco
      var elem = this;
      setTimeout(function(){
          // mudo a posição do seletor
          elem.selectionStart = elem.selectionEnd = 10000;
      }, 0);
      // reaplico o valor para mudar o foco
      var currentValue = $(this).val();
      $(this).val('');
      $(this).val(currentValue);
    });

    function validar(id) {
      var id = id
      var idt = id.value
      console.log(idt)
      if (idt.length == 14){
        ValidarCPF2(id)
      }else if(idt.length == 18){
        validarCNPJ(id)
      }else if(idt.length != 0){
        $("#cnpj").val("")
        alert('CPF/CNPJ inválidos!')
      }
    }

    </script>

</section>


@endsection
