@extends('layout_admin.principal')
@section ('breadcrumb','Caixas')
@section('conteudo')
  <style>
      #ob{
        color: red;
      }
  </style>
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6">
                                    <strong>Caixas</strong>
                                </div>
                                <div class="col-6 text-right">
                                    <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small"
                                            data-toggle="modal" data-target="#FormCota">
                                        <i class="zmdi zmdi-plus"></i>Adicionar
                                    </button>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @elseif (session('ok'))
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                            {{session('ok')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="table-responsive-md table-responsive-data2">
                                      <table class="table table-data2" id="tabela">
                                          <thead>
                                              <tr>
                                                  <th>#</th>
                                                  <th>Nº Caixa</th>
                                                  <th>Obs.</th>
                                                  <th>Setor</th>
                                                  <th>&nbsp;</th>
                                              </tr>
                                          </thead>
                                          <form class="" action="{{route('caixa.geraretiqueta')}}" method="post">
                                              {{ csrf_field() }}
                                          <tbody>
                                              @foreach ($caixas as $key)
                                              <tr>
                                                <td>
                                                    <input type="checkbox" name="caixa_id[]" value="{{$key->id}}" >
                                                </td>
                                                <td>{{ $key->id }}</td>
                                                <td>{{ isset($key->obs) ? $key->obs : '-' }}</td>
                                                <td>{{ $key->tipo->descricao }}</td>
                                                <td>
                                                  <div class="table-data-feature">
                                                    <a href="{{ route('caixa.processo', $key->id) }}" >
                                                        <button type="button" class="item" title="Conteúdo"
                                                        data-toggle="tooltip" data-placement="top" >
                                                            <i class="zmdi zmdi-folder-outline"></i>
                                                        </button>
                                                    </a>&nbsp;
                                                    <a href="{{ route('caixa.imprimir', $key->id) }}" target="_blank">
                                                        <button type="button" class="item" title="Imprimir Conteúdo"
                                                        data-toggle="tooltip" data-placement="top" >
                                                            <i class="zmdi zmdi-print"></i>
                                                        </button>
                                                    </a>&nbsp;
                                                    {{-- @if (in_array('erp/suprimentos/deletar/{id}', Session::get('permissoes.nomes'))) --}}
                                                    <a href="{{route('caixa.deletar' , $key->id)}}"
                                                      data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                      <button type="button" class="item" title="Deletar"
                                                      data-toggle="tooltip" data-placement="top"
                                                      data-original-title="Deletar">
                                                        <i class="zmdi zmdi-delete"></i>
                                                      </button>
                                                    </a>&nbsp;
                                                    {{-- @else
                                                    @endif --}}
                                                  </div>
                                                </td>
                                              </tr>
                                              @endforeach
                                          </tbody>
                                      </table>
                                          <button type="submit" class="btn btn-primary" formtarget="_blank">Gerar Etiqueta</button>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="FormCota" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="FormModalLabel">Cadastro de Caixa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form class="" action="{{route('caixa.salvar')}}" method="post">
                            {{ csrf_field() }}
                            @include('erp.caixas.form')
                    </div>
                </div>
                <span id="ob">*</span> campo obrigatório<br>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Salvar
                </button>
                <button class="btn btn-warning" type="reset" >
                    Limpar
                </button>
            </div>
        </div>
        </form>
    </div>
</div>


<script>
function k(i) {
  var v = i.value.replace(/\D/g,'');
  v = (v/100).toFixed(2) + '';
  v = v.replace(".", ",");
  v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
  v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
  i.value = v;
}
</script>

@endsection
