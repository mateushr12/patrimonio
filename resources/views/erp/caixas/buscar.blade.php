@extends('layout_admin.principal')
@section ('breadcrumb','Buscar Caixas/Conteúdo')
@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Buscar Caixa/Conteúdo</strong>
                                </div>
                            </div>
                        </div>
                        <div class="card-body card-block">
                            <div class="table-responsive table-responsive-data2">
                            <table class="table" id="tabela">
                                <thead>
                                    <tr>
                                        <th>Caixa</th>
                                        <th>Nome/Credor</th>
                                        <th>CPF/CNPJ</th>
                                        <th>Conteúdo</th>
                                        <th>E-doc/Processo</th>
                                        <th>Data</th>
                                        <th>Setor</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($buscar as $key)
                                    <tr>
                                      <td><a href="{{ route('caixa.processo', $key->id) }}" class="btn btn-primary">N° {{ $key->id }}</a></td>
                                      <td>{{ $key->nome }}</td>
                                      <td>{{ $key->cpfCnpj }}</td>
                                      <td>{{ $key->conteudo }}</td>
                                      <td>@if ($key->processo == null ) - @else {{ $key->processo }} @endif</td>
                                      <span hidden>
                                        <?php $dt = explode('-', $key->data); ?>
                                      </span>
                                      @if (isset($key->data))
                                      <td>
                                        {{ $dt[1].'/'.$dt[0] }}
                                      </td>
                                      @else
                                      <td>
                                        -
                                      </td>
                                      @endif
                                      <td>{{ $key->tipo->descricao }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready( function () {
  $('#tabela').DataTable({
    paging: false,
    scrollY: 600,
    language: {
      search: "Buscar:",
      zeroRecords: "Nenhum registro encontrado.",
      info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      infoEmpty: "Mostrando 0 até 0 de 0 registros",
      lengthMenu: "Mostrar _MENU_ entradas",
      infoFiltered: "(filtrado de _MAX_ registros)",
      paginate: {
        first: "Primeiro",
        previous: "Anterior",
        next: "Proximo",
        last: "Ultimo"
      },
      emptyTable: "Nenhum registro encontrado."
    }
  });
});

    function k(i) {
                var v = i.value.replace(/\D/g,'');
                v = (v/100).toFixed(2) + '';
                v = v.replace(".", ".");
                v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                i.value = v;
            }
</script>
@endsection
