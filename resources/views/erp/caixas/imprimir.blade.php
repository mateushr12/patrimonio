<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="JUCESE - Junta Comercial do Estado de Sergipe">

    <!-- Title Page-->
    <title>ERP - JUCESE</title>

    <link href="<?php echo asset('vendor/bootstrap-4.1/bootstrap.min.css')?>" rel="stylesheet" media="all">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@3.5.9/dist/jspdf.plugin.autotable.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script> --}}

    <style>
      @media print {
        .btn {
          display: none !important;
        }
      }
      #tnPDF{
        background: #2579e6;
        color: white;
        font-weight: bold;
        width: 215px;
      }
    </style>

</head>

<body>


<div class="container">
<div class="row" >
  <div class="col-12 form-group">
    <div class="card" style="border: 0px">
      </div>
        <div class="card-body card-block">
          <button type="button" class="btn btn-block" id="tnPDF" onclick="window.print()">Imprimir</button><br>
            <div class="table-responsive table-responsive-data2">
                <table style="width: 900px">
                  <thead >
                    <tr><td>
                      <img src="<?php echo asset('images/gov-jucese-top-relatorio.png') ?>"  width="915px;"><br>
                    </td></tr>
                    <tr>
                      <td style="font-size: 16px;" ><strong>OBS:</strong> {{ $caixa->obs }}</td>
                    </tr>
                  </thead>
                <tbody>
                <tr><td>
                <table class="table table-data2" id="tabela" border="1">
                    <tbody>
                        <tr>
                          <th>PROCESSO</th>
                          <th>CREDOR</th>
                        </tr>
                        @foreach ($processos as $key)
                        <tr>
                          <td>{{ $key->processo }}</td>
                          <td>{{ $key->nome }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </td></tr>
              </tbody>
                </table>
            </div>
        </div>
    </div>
  </div>
</div>
</div>
</body>

<script>
</script>

</html>
