<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="JUCESE - Junta Comercial do Estado de Sergipe">

    <!-- Title Page-->
    <title>ERP - JUCESE</title>

    <link href="<?php echo asset('vendor/bootstrap-4.1/bootstrap.min.css')?>" rel="stylesheet" media="all">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@3.5.9/dist/jspdf.plugin.autotable.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

    <style>
      @media print {
        .btn {
          display: none !important;
        }
      }
      #btnPDF{
        background: #2579e6;
        color: white;
        font-weight: bold;
        width: 200px;
      }
      #btnPDF:hover{
        background: #0439f1
      }
    </style>

</head>

<body>

<div class="container">
<div class="row">
  <div class="col-12 form-group">
    <div class="card">
        <div class="card-body card-block">
          <button type="button" class="btn" id="btnPDF">Gerar PDF</button><br>
            <div class="table-responsive table-responsive-data2">
              <table style="width: 900px">
                <thead >
                  <tr><td colspan="2">
                    <img src="../../images/gov-jucese-top-relatorio.jpg"  width="915px;"><br>
                  </td></tr>
                  <tr>
                    <td style=" font-size: 20px;"><strong>RELATÓRIO DE CONTRATOS</strong><p></p></td>
                    <td style="font-size: 12px; float: right">Data: {{ date('d/m/Y') }}</td>
                  </tr>
                </thead>
              <tbody>
              <tr><td colspan="2">
                <table class="table table-data2" id="tabela">
                    <thead>
                        <tr>
                            <th>Contrato</th>
                            <th>Ano</th>
                            <th>Licitação</th>
                            <th>Fornecedor</th>
                            <th>Valor Total Contrato (R$)</th>
                            <th>Valor Utilizado (R$)</th>
                            {{-- <th>Valor Disponivel (R$)</th> --}}
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($contrato as $k => $key)
                        <tr class="tr-shadow">
                            <td>{{ $key->numerocontrato }}</td>
                            <td>
                              @if ($key->ano == null)
                                -
                              @else
                                {{ $key->ano }}
                              @endif
                            </td>
                            <td>{{ $key->licitacao }}</td>
                            <td>{{$key->nome}}</td>
                            <td>{{ number_format(($key->valorcontrato + $key->totalPg),2,',','.') }}</td>
                            <td>{{ number_format($key->total,2,',','.') }}</td>
                            {{-- <td>{{ number_format((($key->valorcontrato + $key->totalPg) - $key->total),2,',','.') }}</td> --}}
                            <td></td>
                            {{-- <span hidden>{{ $totalC }}</span> --}}
                            <span hidden>{{ $totalU[] = $key->total }}</span>
                            {{-- <span hidden>{{ $totalD[] = ($totalC) - $key->total }}</span> --}}
                        </tr>
                        <tr class="spacer"></tr>
                        @endforeach
                        <tr>
                          <td colspan="6"></td>
                          <td><strong>Disponível</strong></td>
                        </tr>
                        <tr>
                          <td colspan="4"><strong>TOTAL</strong></td>
                          <td><strong>{{ number_format((isset($totalC) ? $totalC : 0),2,',','.') }}</strong></td>
                          <td><strong>{{ number_format((isset($totalU) ? array_sum($totalU) : 0),2,',','.') }}</strong></td>
                          <td><strong>{{ number_format((isset($totalU) ? ($totalC - array_sum($totalU)) : 0),2,',','.') }}</strong></td>
                        </tr>
                    </tbody>
                </table>
              </td></tr>
            </tbody>
            <tfoot >
              <tr>
                <td style="text-align: center; font-weight: bold" colspan="2">
                  Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020<br>Telefone 79 3234-4100 sítio: www.jucese.se.gov.br
                </td>
              </tr>
            </tfoot>
              </table>
            </div>
        </div>
    </div>
  </div>
</div>

</div>
</body>

<script>
function dataAt(){
    var data = new Date(),
        dia  = data.getDate().toString().padStart(2, '0'),
        mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
        ano  = data.getFullYear();
    return dia+"/"+mes+"/"+ano;
}

$(document).ready(function(){
  $('#btnPDF').click(function() {
    var doc = new jsPDF('portrait', 'pt', 'a4')
    var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"
    var img = new Image()
    img.src = base64Img
    var hoje = new Date()
    doc.autoTable({
      html: '#tabela',
      styles: {
        lineColor: '#ccc',
        lineWidth: 1,
        fontSize: 9
      },
      headStyles: { fillColor: '#424647' },
      didDrawPage: function (data) {
        // Cabeçalho
        if (base64Img) {
          doc.addImage(img, 'JPEG', 40, 15, 515, 66)
        }
        doc.setFontSize(14)
        doc.setTextColor(40)
        doc.setFontType("bold")
        doc.text('RELATÓRIO DE CONTRATOS', data.settings.margin.left + 0, 100)
        doc.setFontSize(9)
        doc.text('Data: ' + dataAt(), data.settings.margin.left + 445, 100)

        // Rodape
        var str = 'Pág. ' + doc.internal.getNumberOfPages()
        doc.setFontSize(9)
        var pageSize = doc.internal.pageSize
        var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
        doc.text(str, data.settings.margin.left + 480, pageHeight - 25)
        doc.setFontType("bold");
        doc.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', data.settings.margin.left + 128, pageHeight - 30)
        doc.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br', data.settings.margin.left + 145, pageHeight - 20)
      },
      margin: {top: 108}
    });
    doc.save("RelatorioContrato-"+ dataAt() +".pdf");
  });
});
</script>

</html>
