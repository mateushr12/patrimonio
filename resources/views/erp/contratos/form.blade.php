<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" data-toggle="tab" role="tab" aria-controls="nav-profile" aria-selected="false" href="#DadosContrato">Dados do Contrato</a>
        <a class="nav-item nav-link" data-toggle="tab" role="tab" aria-controls="nav-profile" aria-selected="false" href="#AnexosContrato">Complementos</a>
    </div>
</nav>
&nbsp;
<div class="tab-content">
    <div class="tab-pane fade show active"  id="DadosContrato" role="tabpanel" aria-labelledby="nav-profile-tab">
        <div class="row">
            <div class="col-4 form-group">
                <label for="">Nº do Contrato (UG)<span id="ob">*</span></label>
                <input type="text" name="numerocontrato" id="" class="form-control" {{isset($registro->numerocontrato) ? 'disabled' : ''}}
                value="{{isset($registro->numerocontrato) ? $registro->numerocontrato : ''}}" required>
            </div>
            <div class="col-4 form-group">
                <label for="">Empenho<span id="ob">*</span></label>
                <input type="text" name="empenho" id="" class="form-control" value="{{isset($registro->empenho) ? $registro->empenho : ''}}" required>
            </div>
            <div class="col-4 form-group">
                <label for="">Procedimento de Origem<span id="ob">*</span></label>
                <select class="form-control" name="origem" required>
                    <option value="">SELECIONE</option>
                    <option value="1" @if(isset($registro->origem) && $registro->origem == 1):
                            selected
                        @endif>CONTRATO CENTRALIZADO</option>
                    <option value="2" @if(isset($registro->origem) && $registro->origem == 2):
                            selected
                        @endif>DISPENSA (ART. 24)</option>
                    <option value="3" @if(isset($registro->origem) && $registro->origem == 3):
                            selected
                        @endif>LICITAÇÃO/DISPENSA/INEXIGIBILIDADE</option>
                    <option value="4" @if(isset($registro->origem) && $registro->origem == 4):
                            selected
                        @endif>ADESÃO A ATA DE REGISTRO DE PREÇO</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-6 form-group">
                  <label for="">Empenho fora Banco de Preço<span id="ob">*</span></label>
                  <select class="form-control" name="bancodepreco" id="" required>
                      <option value="">SELECIONE</option>
                      <option value="1" @if(isset($registro->bancodepreco) && $registro->bancodepreco == 1):
                              selected
                          @endif>SIM</option>
                      <option value="2" @if(isset($registro->bancodepreco) && $registro->bancodepreco == 2):
                              selected
                          @endif>NÃO</option>
                  </select>
            </div>
            <div class="col-6 form-group">
                  <label for="">Licitação<span id="ob">*</span></label>
                  <input type="text" name="licitacao" id="" class="form-control" value="{{isset($registro->licitacao) ? $registro->licitacao : ''}}" required>
            </div>
        </div>
        <div class="row">
            <div class="col-12 form-group">
                <label for="">Fornecedor Contratado<span id="ob">*</span></label>
                <select name="fornecedorcontratado" id="" class="form-control" required
                {{isset($registro->fornecedorcontratado) ? 'disabled' : ''}}>
                  <option value="" >SELECIONE</option>
                  @foreach($fornecedores as $fornecedor)
                  <option {{isset($registro->fornecedorcontratado) && $registro->fornecedorcontratado == $fornecedor->id ? 'selected' : ''}}
                      value="{{ isset($registro->fornecedorcontratado) ? $registro->fornecedorcontratado : $fornecedor->id }}">
                      {{$fornecedor->nome}}
                  </option>
                  @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-6 form-group">
                <label for="">Início da Vigência<span id="ob">*</span></label>
                <input type="date" name="iniciovigencia" id="inicio" class="form-control" value="{{isset($registro->iniciovigencia) ? $registro->iniciovigencia : ''}}" required>
            </div>
            <div class="col-6 form-group">
                <label for="">Final da Vigência<span id="ob">*</span></label>
                <input type="date" name="finalvigencia" id="fim" onblur="data(this);" class="form-control" value="{{isset($registro->finalvigencia) ? $registro->finalvigencia : ''}}" required>
            </div>
        </div>
        <div class="row">
            <div class="col-4 form-group">
                <label for="">Valor do Contrato (R$)<span id="ob">*</span></label>
                <input type="text" name="valorcontrato" id="valorr" onblur="vl();" class="form-control" value="{{isset($registro->valorcontrato) ? $registro->valorcontrato : ''}}" onkeyup="k(this);" required>
            </div>
            <div class="col-4 form-group">
                <label for="">Data da Assinatura<span id="ob">*</span></label>
                <input type="date" name="dataassinatura" id="" class="form-control" value="{{isset($registro->dataassinatura) ? $registro->dataassinatura : ''}}" required>
            </div>
            <div class="col-4 form-group">
                <label for="">Período Aquisitivo<span id="ob">*</span></label>
                <select class="form-control" name="periodoaquisitivo" id="" required>
                    <option value="">SELECIONE</option>
                    <option value="1"  @if(isset($registro->periodoaquisitivo) && $registro->periodoaquisitivo == 1):
                        selected
                    @endif>Anual</option>
                    <option value="2" @if(isset($registro->periodoaquisitivo) && $registro->periodoaquisitivo == 2):
                        selected
                    @endif>Plurianual</option>
                </select>
            </div>
        </div>
    </div>
    <div class="tab-pane fade"  id="AnexosContrato" role="tabpanel" aria-labelledby="nav-profile-tab">
        <div class="row">
            <div class="col-12 form-group">
                <label for="">Motivo de Anulação do Contrato</label>
                <input type="text" name="motivoanulacao" id="" class="form-control" value="{{isset($registro->motivoanulacao) ? $registro->motivoanulacao : ''}}">
            </div>
        </div>
        <div class="row">
          <div class="col-4 form-group">
              <label for="">Anulação do Contrato</label>
              <input type="date" name="anulacaocontrato" id="" class="form-control" value="{{isset($registro->anulacaocontrato) ? $registro->anulacaocontrato : ''}}">
          </div>
        </div>
    </div>
</div>
