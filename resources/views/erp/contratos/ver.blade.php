<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="JUCESE - Junta Comercial do Estado de Sergipe">

    <!-- Title Page-->
    <title>ERP - JUCESE</title>

    <link href="<?php echo asset('vendor/bootstrap-4.1/bootstrap.min.css')?>" rel="stylesheet" media="all">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@3.5.9/dist/jspdf.plugin.autotable.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

    <style>
      @media print {
        .btn {
          display: none !important;
        }
      }
      #btnPDF{
        background: #2579e6;
        color: white;
        font-weight: bold;
        width: 120px;
      }
    </style>

</head>

<body>


<div class="container">
        <div class="card-body card-block">
          <button type="button" class="btn" id="btnPDF">Gerar PDF</button><br>
          <img src="../../../images/gov-jucese-top-relatorio.jpg" hidden>
                <table style="width: 900px">
                  <thead >
                    <tr>
                      <td style=" font-size: 22px;" ><strong>Contrato</strong><p></p></td>
                      <td style="font-size: 14px; float: right;" >Data {{ date('d/m/Y') }}</td>
                    </tr>
                  </thead>
                <tbody>
                <tr><td colspan="2">
                <table class="table table-data2" id="tabela" border="1">
                    <tbody>
                        <tr>
                          <td colspan="2"><strong >Nº Contrato:</strong><br> {{ $contrato->numerocontrato }}</td>
                          <td colspan="3"><strong >Data Da Assinatura:</strong><br> {{ \Carbon\Carbon::parse($contrato->dataassinatura)->format('d/m/Y') }}</td>
                          <td colspan="4"><strong >Procedimento de Origem:</strong><br>
                            @switch($contrato->origem)
                                @case(1) CONTRATO CENTRALIZADO @break
                                @case(2) DISPENSA (ART. 24) @break
                                @case(3) LICITAÇÃO/DISPENSA/INEXIGIBILIDADE @break
                                @case(4) ADESÃO A ATA DE REGISTRO DE PREÇO @break
                            @endswitch
                          </td>
                        </tr>
                        <tr>
                          <td colspan="5"><strong >Empenho:</strong><br> {{ $contrato->empenho }}</td>
                          <td colspan="4"><strong >Valor Contrato:</strong><br>R$ {{ number_format($contrato->valorcontrato,2,',','.') }}</td>
                        </tr>
                        <tr>
                          <td colspan="5"><strong >Licitação:</strong><br> {{ $contrato->licitacao }}</td>
                          <td colspan="4"><strong >Empenho fora Banco de Preço:</strong><br> @if ($contrato->bancodepreco == 1) Sim @else Não @endif</td>
                        </tr>
                        <tr><td colspan="9"><strong>Fornecedor:</strong><br> {{ $contrato->fornecedor->nome }}</td></tr>
                        <tr>
                          <td colspan="5"><strong >Inicio da Vigência:</strong><br> {{ \Carbon\Carbon::parse($contrato->iniciovigencia)->format('d/m/Y') }}</td>
                          <td colspan="4"><strong >Fim da Vigência:</strong><br> {{ \Carbon\Carbon::parse($contrato->finalvigencia)->format('d/m/Y') }}</td>
                        </tr>
                        <tr>
                          <td colspan="5"><strong >Período Aquisitivo:</strong><br> @if ($contrato->bancodepreco == 1) Anual @else Plurianual @endif</td>
                          <td colspan="4"><strong >Data Anulação Contrato:</strong><br>
                            @if (isset($contrato->anulacaocontrato))
                              {{ \Carbon\Carbon::parse($contrato->anulacaocontrato)->format('d/m/Y') }}
                            @else
                              -
                            @endif
                          </td>
                        </tr>
                        <tr><td colspan="9"><strong>Motivo Anulção Contrato:</strong><br> {{ $contrato->motivoanulacao }}</td></tr>
                        <tr><th colspan="9" style="text-align: center">ADITIVOS</th></tr>
                        <tr>
                          <th>Nº</th>
                          <th>Vigência</th>
                          <th>Assinatura</th>
                          <th>Anulação</th>
                          <th>Motivo</th>
                          <th>Tipo</th>
                          <th>Natureza</th>
                          <th>Período</th>
                          <th>Valor (R$)</th>
                        </tr>
                        @foreach ($aditivo as $key)
                        <tr>
                          <td>{{ $key->n_aditivo }}</td>
                          <td>
                            @if (isset($key->vigencia_i))
                              {{ \Carbon\Carbon::parse($key->vigencia_i)->format('d/m/Y') }} a {{ \Carbon\Carbon::parse($key->vigencia_f)->format('d/m/Y') }}
                            @else
                              -
                            @endif
                          </td>
                          <td>@if (isset($key->assinatura)) {{ \Carbon\Carbon::parse($key->assinatura)->format('d/m/Y') }} @else - @endif</td>
                          <td>@if (isset($key->anulacao)) {{ \Carbon\Carbon::parse($key->anulacao)->format('d/m/Y') }} @else - @endif</td>
                          <td>{{ $key->motivo }}</td>
                          <td>
                            @switch($key->tipo)
                                @case(1) Aditivo @break
                                @case(2) Apostilamento @break
                                @case(3) Rerratificação @break
                            @endswitch
                          </td>
                          <td>
                            @switch($key->natureza)
                                @case(1) Acréscimo de valor @break
                                @case(2) Acréscimo de valor e prazo @break
                                @case(3) Ajuste de prazo @break
                                @case(4) Ajuste sem alteração de valor e prazo @break
                                @case(5) Supressão do valor @break
                                @case(6) Supressão do valor e prazo @break
                                @case(7) Supressão do valor e prorrogação do prazo @break
                                @case(8) Termo de rerratificação @break
                                @case(9) Outro tipo @break
                            @endswitch
                          </td>
                          <td>@if ($key->periodo == 1) Anual @else Plurianual @endif</td>
                          <td>{{ number_format($key->valor,2,',','.') }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </td></tr>
              </tbody>
                </table>
        </div>


</div>
</body>

<script>

function dataAt(){
    var data = new Date(),
        dia  = data.getDate().toString().padStart(2, '0'),
        mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
        ano  = data.getFullYear();
    return dia+"/"+mes+"/"+ano;
}

$(document).ready(function(){
  $('#btnPDF').click(function() {
    var numero = "{{ $contrato->numerocontrato }}"
    var doc = new jsPDF('portrait', 'pt', 'a4')
    var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"
    var img = new Image()
    img.src = base64Img
    var hoje = new Date()
    doc.autoTable({
      html: '#tabela',
      styles: {
        lineColor: '#ccc',
        lineWidth: 1,
        fontSize: 10
      },
      headStyles: { fillColor: "#424647", fontSize: 11 },
      didParseCell: function (data, cell) {
        if (data.row.index == 7) {
          data.cell.styles.fontStyle = 'bold';
          data.cell.styles.halign = 'center';
          data.cell.styles.fillColor = '#424647';
          data.cell.styles.textColor = 'white';
        }else if (data.row.index == 8) {
          data.cell.styles.fontStyle = 'bold';
          data.cell.styles.halign = 'center';
          data.cell.styles.textColor = 'black';
        }
      },
      didDrawPage: function (data) {
      ///////////////////// Cabeçalho ///////////////////////////////////////////////
        if (base64Img) {
          doc.addImage(img, 'JPEG', 40, 15, 515, 66)
        }
        doc.setFontSize(14)
        doc.setTextColor(40)
        doc.setFontType("bold")
        doc.text('Contrato', data.settings.margin.left + 0, 100)
        doc.setFontSize(9)
        doc.text('Data: ' + dataAt(), data.settings.margin.left + 445, 100)


        ///////////////////////////// Rodape ////////////////////////////////////////
        var str = 'Pág. ' + doc.internal.getNumberOfPages()
        doc.setFontSize(9)
        var pageSize = doc.internal.pageSize
        var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
        doc.text(str, data.settings.margin.left + 480, pageHeight - 25)
        doc.setFontType("bold");
        doc.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', data.settings.margin.left + 128, pageHeight - 30)
        doc.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br', data.settings.margin.left + 145, pageHeight - 20)
      },
      margin: {top: 108}
    });
    doc.save("Contrato"+ numero +"_"+ dataAt() +".pdf");
  });
});

</script>

</html>
