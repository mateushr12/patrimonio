@extends('layout_admin.principal')
@section ('breadcrumb','Aditivo')
@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Cadastro de Aditivo - Contrato nº {{ $n_contrato }}</strong>
                                </div>
                                <div class="col-3 text-right">
                                    <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small"
                                            data-toggle="modal" data-target="#FormModal">
                                        <i class="zmdi zmdi-plus"></i>Adcionar
                                    </button>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body">
                          <div class="row">
                              <div class="col-12">
                                  <div class="table-responsive table-responsive-data2">
                                      <table class="table table-data2">
                                          <thead>
                                              <tr>
                                                  <th>Nº Aditivo</th>
                                                  <th>Tipo Aditivo</th>
                                                  <th>Natureza Aditivo</th>
                                                  <th>Data de Assinatura</th>
                                                  <th>Vigência Inicio</th>
                                                  <th>Vigência Final</th>
                                                  <th>Valor</th>
                                                  <th>&nbsp;</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              @foreach ($aditivos as $aditivo)
                                              <tr>
                                                  <td>{{ $aditivo->n_aditivo }}</td>
                                                  <td>
                                                  @switch($aditivo->tipo)
                                                      @case(1)
                                                          Aditivo
                                                          @break
                                                      @case(2)
                                                          Apostilamento
                                                          @break
                                                      @case(3)
                                                          Rerratificação
                                                          @break
                                                      @default
                                                  @endswitch
                                                  </td>
                                                  <td>
                                                    @switch($aditivo->natureza)
                                                        @case(1)
                                                            Acréscimo de valor
                                                            @break
                                                        @case(2)
                                                            Acréscimo de valor e prazo
                                                            @break
                                                        @case(3)
                                                            Ajuste de prazo
                                                            @break
                                                        @case(4)
                                                            Ajuste sem alteração de valor e prazo
                                                            @break
                                                        @case(5)
                                                            Supressão do valor
                                                            @break
                                                        @case(6)
                                                            Supressão do valor e prazo
                                                            @break
                                                        @case(7)
                                                            Supressão do valor e prorrogação do prazo
                                                            @break
                                                        @case(8)
                                                            Termo de rerratificação
                                                            @break
                                                        @case(9)
                                                            Outro tipo
                                                            @break
                                                        @default
                                                    @endswitch
                                                  </td>
                                                  <td>{{\Carbon\Carbon::parse($aditivo->assinatura)->format('d/m/Y')}}</td>
                                                  <td>
                                                    @if (isset($aditivo->vigencia_i))
                                                      {{\Carbon\Carbon::parse($aditivo->vigencia_i)->format('d/m/Y')}}
                                                    @else
                                                      -
                                                    @endif
                                                  </td>
                                                  <td>
                                                    @if (isset($aditivo->vigencia_f))
                                                      {{\Carbon\Carbon::parse($aditivo->vigencia_f)->format('d/m/Y')}}
                                                    @else
                                                      -
                                                    @endif
                                                  </td>
                                                  <td>
                                                    {{ $aditivo->valor }}
                                                  </td>
                                                  <td>
                                                      <div class="table-data-feature">
                                                          {{-- <a >
                                                              <button type="button" class="item" title="Editar"
                                                              data-toggle="tooltip" data-placement="top" title=""
                                                                  data-original-title="Editar"
                                                              >
                                                                  <i class="zmdi zmdi-edit"></i>
                                                              </button>
                                                          </a>&nbsp; --}}
                                                          <a href="{{ route('aditivo.deletar', $aditivo->id) }}"
                                                              data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                              <button type="button" class="item" title="Deletar"
                                                              data-toggle="tooltip" data-placement="top" title=""
                                                                  data-original-title="Deletar"
                                                              >
                                                                  <i class="zmdi zmdi-delete"></i>
                                                              </button>
                                                          </a>&nbsp;
                                                          <a href="{{ route ('aditivo.anexar', $aditivo->id) }}">
                                                            <button type="button" class="item" title="Anexar Arquivos"
                                                            data-toggle="tooltip" data-placement="top" title=""
                                                            data-original-title="Anexar Arquivos"
                                                            >
                                                                <i class="zmdi zmdi-attachment-alt"></i>
                                                            </button>
                                                          </a>
                                                      </div>
                                                  </td>
                                              </tr>
                                              @endforeach
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="FormModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="FormModalLabel">Cadastro de Aditivo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form class="" action="{{ route('aditivo.salvar') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id_contrato" value="{{ $contrato }}" />
                                <div class="row">
                                  <div class="col-6 form-group">
                                    <label for="">Nº do Aditivo:*</label>
                                    <input type="text" class="form-control" name="n_aditivo" id="" value="" required>
                                  </div>
                                  <div class="col-6 form-group">
                                      <label for="">Periodo Aquisitivo:*</label>
                                      <select class="form-control" name="periodo" id="" required>
                                          <option value="">Selecione </option>
                                          <option value="1">Anual</option>
                                          <option value="2">Plurianual</option>
                                      </select>
                                  </div>
                                </div>

                                <div class="row">
                                    <div class="col-6 form-group">
                                        <label for="">Tipo de aditivo:*</label>
                                        <select class="form-control" name="tipo" id="">
                                            <option value="">Selecione </option>
                                            <option value="1">Aditivo</option>
                                            <option value="2">Apostilamento</option>
                                            <option value="3">Rerratificação</option>
                                        </select>
                                    </div>
                                    <div class="col-6 form-group">
                                        <label for="">Natureza do aditivo:*</label>
                                        <select class="form-control" name="natureza" id="natureza">
                                            <option value="">Selecione </option>
                                            <option value="1">Acréscimo de valor</option>
                                            <option value="2">Acréscimo de valor e prazo</option>
                                            <option value="3">Ajuste de prazo</option>
                                            <option value="4">Ajuste sem alteração de valor e prazo</option>
                                            <option value="5">Supressão do valor</option>
                                            <option value="6">Supressão do valor e prazo</option>
                                            <option value="7">Supressão do valor e prorrogação do prazo</option>
                                            <option value="8">Termo de rerratificação</option>
                                            <option value="9">Outro tipo</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                  <div class="col-6 form-group" id="">
                                    <label for="">Dt. Assinatuara:*</label>
                                    <input type="date" class="form-control" name="assinatura" value="" required>
                                  </div>
                                  <div class="col-6 form-group" id="">
                                    <label for="">Dt. Anulação:</label>
                                    <input type="date" class="form-control" name="anulacao" value="" >
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-12 form-group" id="">
                                    <label for="">Motivo da Anulação:</label>
                                    <textarea name="motivo" class="form-control"></textarea>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-6 form-group" id="prazoi">
                                    <label for="">Inicio Vigência:</label>
                                    <input type="date" class="form-control" name="vigencia_i" value="" >
                                  </div>
                                  <div class="col-6 form-group" id="prazof">
                                    <label for="">Fim Vigência:</label>
                                    <input type="date" class="form-control" name="vigencia_f" value="" >
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-12 form-group" id="valor">
                                    <label for="">Valor:</label>
                                    <input type="text" class="form-control" name="valor" value="" onkeyup="k(this);">
                                  </div>
                                </div>

                                {{-- <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Cadastrar Aditivo</button>
                                </div>
                        </form> --}}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Salvar
                </button>
                <button class="btn btn-warning" type="reset" >
                    Limpar
                </button>
            </div>
        </div>
        </form>
    </div>
</div>

<script>
    function k(i) {
                        var v = i.value.replace(/\D/g,'');
                        v = (v/100).toFixed(2) + '';
                        v = v.replace(".", ".");
                        v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                        v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                        i.value = v;
                    }
</script>

@endsection
