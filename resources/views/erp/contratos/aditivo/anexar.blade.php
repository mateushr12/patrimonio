@extends('layout_admin.principal')
@section ('breadcrumb','Anexo Aditivo')
@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Anexar arquivos - Aditivo nº {{ $n_aditivo }}</strong>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body card-block">
                            <form class="" action="{{route('aditivo.anexar.salvar')}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="id_aditivo" value="{{ $aditivo }}" />
                                <div class="row">
                                  <div class="col-5 form-group">
                                    <label for="">Arquivo:*</label>
                                    <select class="form-control" name="tipoarquivo" id="" required>
                                      <option value="">Selecione uma opção</option>
                                      <option value="1">Termo Aditivo</option>
                                      <option value="2">Termo de Anuência</option>
                                      <option value="3">Extrato de Publicação do Termo</option>
                                    </select>
                                  </div>
                                  <div class="col-7">
                                    <label for="">Carregar arquivo: (PDF)</label>
                                    <input type="file" name="arquivo" id="" class="form-control" required>
                                  </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Salvar</button>
                            </form>
                            <br>
                            <a href="{{ URL::previous() }}">Voltar</a>
                            <br>
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-borderless table-data3">
                                    <thead>
                                        <tr>
                                            <th>Tipo Arquivo</th>
                                            <th>Arquivo</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($arquivo as $arq)
                                        <tr>
                                            <td>
                                              @switch($arq->tipoarquivo)
                                                  @case(1)
                                                      Contrato
                                                      @break
                                                  @case(2)
                                                      Parecer Jurídico
                                                      @break
                                                  @case(3)
                                                      Extrato
                                                      @break
                                                  @default
                                              @endswitch
                                            </td>
                                            <td><a href="{{ route('aditivo.pdf', $arq->id) }}" target="_blank">{{ $arq->arquivo }}</a></td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <a
                                                    href="{{ route ('aditivo.pdf.delete', $arq->id) }}"
                                                        data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                        <button type="button" class="item" title="Deletar"
                                                        data-toggle="tooltip" data-placement="top" title=""
                                                            data-original-title="Deletar"
                                                        >
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
