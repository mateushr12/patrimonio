@extends('layout_admin.principal')
@section ('breadcrumb','Contratos')
@section('conteudo')
  <style>
  #ob{
    color: red;
  }
  </style>
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Contratos Encerrados</strong>
                                </div>
                                <div class="col-3 text-right">
                                    {{-- <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small"
                                            data-toggle="modal" data-target="#FormModal">
                                        <i class="zmdi zmdi-plus"></i>Adcionar
                                    </button> --}}
                                    <a href="{{ route('contratos.index') }}" class="btn btn-success">VIGENTES</a>
                                </div>
                            </div>
                        </div>

                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="table-responsive table-responsive-data2">
                                        <table class="table" id="tabel">
                                            <thead>
                                                <tr>
                                                    <th>Contrato</th>
                                                    {{-- <th>Licitação</th> --}}
                                                    <th>Vigência</th>
                                                    <th>Fornecedor</th>
                                                    {{-- <th>Empenho</th> --}}
                                                    <th>Valor (R$)</th>
                                                    <th>Fim da Vigência (Dias)</th>
                                                    <th>Ações</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($contratos as $contrato)
                                                <tr>
                                                    <td>{{$contrato->numerocontrato }}</td>
                                                    {{-- <td>{{$contrato->licitacao}}</td> --}}
                                                    <td>
                                                      {{\Carbon\Carbon::parse($contrato->iniciovigencia)->format('d/m/Y')}} a {{\Carbon\Carbon::parse($contrato->finalvigencia)->format('d/m/Y')}}
                                                    </td>
                                                    <td>{{$contrato->fornecedor->nome}}</td>
                                                    {{-- <td>{{$contrato->empenho}}</td> --}}
                                                    <td>{{ number_format($contrato->valorcontrato,2,',','.') }}</td>
                                                    <td>
                                                      @if (isset($contrato->vigencia))
                                                        @if ((strtotime($contrato->vigencia) - strtotime(date('Y-m-d'))) / (86400) > 0)
                                                          @if ((strtotime($contrato->vigencia) - strtotime(date('Y-m-d'))) / (86400) < 30)
                                                              <button type="button" class="btn btn-warning btn-sm" style="cursor:default">
                                                                {{ (strtotime($contrato->vigencia) - strtotime(date('Y-m-d'))) / (86400) }}
                                                              </button>
                                                          @else
                                                            {{ (strtotime($contrato->vigencia) - strtotime(date('Y-m-d'))) / (86400) }}
                                                          @endif
                                                        @else
                                                            <span class="badge badge-danger">ENCERRADO</span>
                                                        @endif
                                                      @else
                                                        @if ((strtotime($contrato->finalvigencia) - strtotime(date('Y-m-d'))) / (86400) > 0)
                                                          @if ((strtotime($contrato->finalvigencia) - strtotime(date('Y-m-d'))) / (86400) < 30)
                                                              <button type="button" class="btn btn-warning btn-sm" style="cursor:default">
                                                                {{ (strtotime($contrato->finalvigencia) - strtotime(date('Y-m-d'))) / (86400) }}
                                                              </button>
                                                          @else
                                                            {{ (strtotime($contrato->finalvigencia) - strtotime(date('Y-m-d'))) / (86400) }}
                                                          @endif
                                                        @else
                                                          <span class="badge badge-danger">ENCERRADO</span>
                                                        @endif
                                                      @endif
                                                    </td>
                                                    <td>
                                                        <div class="table-data-feature">
                                                          <a href="{{ route('contratos.ver', $contrato->id) }}" target="_blank">
                                                              <button type="button" class="item" title="Ver Contrato"
                                                              data-toggle="tooltip" data-placement="top" >
                                                                  <i class="zmdi zmdi-eye"></i>
                                                              </button>
                                                          </a>&nbsp;
                                                          @if (in_array('erp/contratos/editar/{id}', Session::get('permissoes.nomes')))
                                                            <a href="{{route('contratos.editar' , $contrato->id)}}">
                                                                <button type="button" class="item" title="Editar"
                                                                data-toggle="tooltip" data-placement="top" title=""
                                                                    data-original-title="Editar"
                                                                >
                                                                    <i class="zmdi zmdi-edit"></i>
                                                                </button>
                                                            </a>&nbsp;
                                                          @else
                                                          @endif
                                                          @if (in_array('erp/contratos/deletar/{id}', Session::get('permissoes.nomes')))
                                                            <a href="{{ route ('contratos.deletar', $contrato->id) }}"
                                                                data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                                <button type="button" class="item" title="Deletar"
                                                                data-toggle="tooltip" data-placement="top" title=""
                                                                    data-original-title="Deletar"
                                                                >
                                                                    <i class="zmdi zmdi-delete"></i>
                                                                </button>
                                                            </a>&nbsp;
                                                          @else
                                                          @endif
                                                          @if (in_array('erp/contratos/anexar/{id}', Session::get('permissoes.nomes')))
                                                            <a href="{{ route ('contratos.anexar', $contrato->id) }}">
                                                              <button type="button" class="item" title="Anexar Arquivos"
                                                              data-toggle="tooltip" data-placement="top" title=""
                                                              data-original-title="Anexar Arquivos"
                                                              >
                                                                  <i class="zmdi zmdi-attachment-alt"></i>
                                                              </button>
                                                            </a>&nbsp;
                                                          @else
                                                          @endif
                                                          @if (in_array('erp/contratos/aditivo/{id}', Session::get('permissoes.nomes')))
                                                            <a href="{{ route ('aditivo.index', $contrato->id) }}">
                                                              <button type="button" class="item" title="Aditivo"
                                                              data-toggle="tooltip" data-placement="top" title=""
                                                              data-original-title="Aditivo"
                                                              >
                                                                  <i class="zmdi zmdi-plus-square"></i>
                                                              </button>
                                                            </a>
                                                          @else
                                                          @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="FormModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="FormModalLabel">Cadastro de Contratos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form class="" action="{{route('contratos.salvar')}}" method="post">
                            {{ csrf_field() }}
                            @include('erp.contratos.form')
                    </div>
                </div>
                <span id="ob">*</span> campo obrigatório
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Salvar
                </button>
                <button class="btn btn-warning" type="reset"  >
                    Limpar
                </button>
            </div>
        </div>
        </form>
    </div>
</div>

<script>

$(document).ready( function () {
  $('#tabel').DataTable({
    paging: false,

    language: {
      search: "Buscar:",
      zeroRecords: "Nenhum registro encontrado.",
      info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      infoEmpty: "Mostrando 0 até 0 de 0 registros",
      lengthMenu: "Mostrar _MENU_ entradas",
      infoFiltered: "(filtrado de _MAX_ registros)",
      paginate: {
        first: "Primeiro",
        previous: "Anterior",
        next: "Proximo",
        last: "Ultimo"
      },
      emptyTable: "Nenhum registro encontrado."
    }
  });
});


function k(i) {
  var v = i.value.replace(/\D/g,'');
  v = (v/100).toFixed(2) + '';
  v = v.replace(".", ".");
  v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
  v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
  i.value = v;
}

function data(j) {
  var d2 = j.value
  var d1 = document.getElementById('inicio')
  d1 = d1.value
  if (d1 > d2){
    alert('Inicio da Vigência tem que ser menor que o Fim da Vigência.')
    $("#inicio").val("")
    $("#fim").val("")
  }else if(d1 == ''){
    alert('Escolha uma data.')
    $("#fim").val("")
  }
}

function vl(){
  var vlr = document.getElementById('valorr')
  vlr = vlr.value
  if (vlr == 0.00){
    alert('Valor não pode ser zero.')
    $("#valorr").val("")
  }
}



</script>
@endsection
