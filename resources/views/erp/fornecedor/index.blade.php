@extends('layout_admin.principal')
@section ('breadcrumb','Fornecedores')
@section('conteudo')
<style>
#ob{
  color: red;
}
</style>
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6">
                                    <strong>Cadastro de Fornecedores</strong>
                                </div>
                                <div class="col-6 text-right">
                                    <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small"
                                            data-toggle="modal" data-target="#FormCNPJ">
                                        <i class="zmdi zmdi-plus"></i>Adcionar P.J.
                                    </button>
                                    <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small"
                                            data-toggle="modal" data-target="#FormCPF">
                                        <i class="zmdi zmdi-plus"></i>Adcionar Pessoa Física
                                    </button>
                                </div>
                            </div>
                        </div>
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="table-responsive table-responsive-data2">
                                      <table class="table table-data2" id="tabela">
                                          <thead>
                                              <tr>
                                                  <th>Tipo</th>
                                                  <th>Identificação</th>
                                                  <th>Nome / Razão Social </th>
                                                  <th>Situação</th>
                                                  <th>Situação Cadastral</th>
                                                  <th>Status</th>
                                                  <th>Ações</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              @foreach ($fornecedores as $fornecedor)
                                              <tr>
                                                  <td>
                                                    @switch($fornecedor->identificacao)
                                                        @case(1)
                                                            CNPJ
                                                            @break
                                                        @case(2)
                                                            Nº Cartório
                                                            @break
                                                        @case(3)
                                                            NIRE
                                                            @break
                                                        @case(4)
                                                            CPF
                                                            @break
                                                        @default

                                                    @endswitch
                                                  </td>
                                                  <td>{{$fornecedor->numeroident}}</td>
                                                  <td>{{$fornecedor->nome}}</td>
                                                  <td>
                                                      @switch($fornecedor->situacao)
                                                          @case(1)
                                                              Habilitado
                                                              @break
                                                          @case(2)
                                                              Suspenso Parcialmente
                                                              @break
                                                          @case(3)
                                                              Suspenso Totalmente
                                                              @break
                                                          @case(4)
                                                              Não Habilitado
                                                              @break
                                                          @default

                                                      @endswitch
                                                      </td>
                                                  <td>
                                                      @switch($fornecedor->situacaocadastro)
                                                          @case(1)
                                                              Em cadastramento
                                                              @break
                                                          @case(2)
                                                              Cadastrado
                                                              @break
                                                          @default
                                                      @endswitch
                                                  </td>
                                                  <td>
                                                      @switch($fornecedor->status)
                                                          @case(1)
                                                              <h4><span class="badge badge-success">Ativo</span></h4>
                                                              @break
                                                          @case(2)
                                                              <h4><span class="badge badge-danger">Inativo</span></h4>
                                                              @break
                                                          @default

                                                      @endswitch

                                                  </td>
                                                  <td>
                                                      <div class="table-data-feature">
                                                        <a href="{{ route('fornecedor.ver', $fornecedor->id) }}" target="_blank">
                                                            <button type="button" class="item" title="Ver Fornecedor"
                                                            data-toggle="tooltip" data-placement="top" >
                                                                <i class="zmdi zmdi-eye"></i>
                                                            </button>
                                                        </a>&nbsp;
                                                        @if (in_array('erp/fornecedor/editar/{id}', Session::get('permissoes.nomes')))
                                                          <a href="{{route('fornecedor.editar', $fornecedor->id)}}">
                                                              <button type="button" class="item" title="Editar" data-toggle="tooltip" data-placement="top">
                                                                  <i class="zmdi zmdi-edit"></i>
                                                              </button>
                                                          </a>
                                                        @else
                                                        @endif
                                                          {{-- <a href="{{ route ('fornecedor.cnpj.deletar', $fornecedor->id) }}"
                                                              data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                                              <button type="button" class="item" title="Deletar">
                                                                  <i class="zmdi zmdi-delete"></i>
                                                              </button>
                                                          </a> --}}
                                                      </div>
                                                  </td>
                                              </tr>
                                              @endforeach
                                          </tbody>
                                      </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="FormCNPJ" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="FormModalLabel">Cadastro de Fornecedores</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form class="" action="{{route('fornecedor.salvar')}}" method="post">
                            {{ csrf_field() }}
                            @include('erp.fornecedor.cnpj.form')
                    </div>
                </div>
                <span id="ob">*</span> campo obrigatório
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Salvar
                </button>
                <button class="btn btn-warning" type="reset"  >
                    Limpar
                </button>
            </div>
        </div>
        </form>
    </div>
</div>
<div class="modal fade" id="FormCPF" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="FormModalLabel">Cadastro de Fornecedores</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form class="" action="{{route('fornecedor.salvar')}}" method="post">
                            {{ csrf_field() }}
                            @include('erp.fornecedor.cpf.form')
                    </div>
                </div>
                <span id="ob">*</span> campo obrigatório
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Salvar
                </button>
                <button class="btn btn-warning" type="reset" >
                    Limpar
                </button>
            </div>
        </div>
        </form>
    </div>
</div>

<script>

$(document).ready( function () {
  $('#tabela').DataTable({
    paging: false,

    language: {
      search: "Buscar:",
      zeroRecords: "Nenhum registro encontrado.",
      info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      infoEmpty: "Mostrando 0 até 0 de 0 registros",
      lengthMenu: "Mostrar _MENU_ entradas",
      infoFiltered: "(filtrado de _MAX_ registros)",
      paginate: {
        first: "Primeiro",
        previous: "Anterior",
        next: "Proximo",
        last: "Ultimo"
      },
      emptyTable: "Nenhum registro encontrado."
    }
  });
});

function ident(){
  var tp = document.getElementById("identificacao")
  var idt = document.getElementById("numeroident")
  tp = tp.value
  id = idt.value
  switch (tp) {
    case '1':
      if (id.length != 18){
        alert('CNPJ inválido!')
        $("#numeroident").val("")
      }else {
        validarCNPJ(idt)
      }
      break;
    case '2':
      if (id.length != 19){
        alert('Nº Cartório inválido!')
        $("#numeroident").val("")
      }
      break;
    case '3':
      if (id.length != 11){
        alert('NIRE inválido!')
        $("#numeroident").val("")
      }
      break;
  }
}



</script>
@endsection
