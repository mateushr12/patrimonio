<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" data-toggle="tab" role="tab" aria-controls="nav-profile" aria-selected="false" href="#DadosContrato">Dados do Fornecedor</a>
        <a class="nav-item nav-link" data-toggle="tab" role="tab" aria-controls="nav-profile" aria-selected="false" href="#AnexosContrato">Dados Bancários</a>
    </div>
</nav>
&nbsp;
<div class="tab-content">
    <div class="tab-pane fade show active"  id="DadosContrato" role="tabpanel" aria-labelledby="nav-profile-tab">
<div class="row">
    <div class="col-4 form-group">
        <label for="">Tipo de Identificação:<span id="ob">*</span></label>
        <select class="form-control" name="identificacao" id="identificacao"
        {{isset($registro->situacao) ? 'disabled' : ''}}
        required>
            <option value="">Selecione </option>
            <option value="1"
            @if(isset($registro->situacao) && $registro->situacao == 1):
                selected
            @endif
            >CNPJ</option>
            <option value="2"
            @if(isset($registro->situacao) && $registro->situacao == 2):
                selected
            @endif
            >Número Cartório</option>
            <option value="3"
            @if(isset($registro->situacao) && $registro->situacao == 3):
                selected
            @endif
            >NIRE</option>
        </select>
    </div>
    <div class="col-8 form-group">
        <label for="">Número de Identificação:<span id="ob">*</span></label>
        <input type="text" class="form-control" name="numeroident" id="numeroident" onblur="ident()"
        {{-- onblur="ident();" --}}
        {{isset($registro->numeroident) ? 'disabled' : ''}}
        value="{{isset($registro->numeroident) ? $registro->numeroident : ''}}" required>
    </div>
</div>
<div class="row">
    <div class="col-12 form-group">
        <label for="">Nome Fantasia / Razão Social:<span id="ob">*</span></label>
        <input type="text" class="form-control" name="nome" id="" value="{{isset($registro->nome) ? $registro->nome : ''}}" onkeyup="this.value = this.value.toUpperCase();" required>
    </div>
</div>
<div class="row">
    <div class="col-8 form-group">
        <label for="">Representante:<span id="ob">*</span></label>
        <input type="text" class="form-control" name="representante" id="" value="{{isset($registro->representante) ? $registro->representante : ''}}" required>
    </div>
    <div class="col-4 form-group">
        <label for="">CPF:<span id="ob">*</span></label>
        <input type="text" name="cpfrepres" id="cpfrepres" onblur="ValidarCPF2(this)" class="form-control MaskCPF" value="{{isset($registro->cpfrepres) ? $registro->cpfrepres : ''}}" required>
    </div>
</div>
<div class="row">
    <div class="col-4 form-group">
        <label for="">Telefone:<span id="ob">*</span></label>
        <input type="text" class="form-control" name="tel" id="" value="{{isset($registro->tel) ? $registro->tel : ''}}" required>
    </div>
    <div class="col-8 form-group">
        <label for="">E-mail:<span id="ob">*</span></label>
        <input type="email" class="form-control" name="email" id="" value="{{isset($registro->email) ? $registro->email : ''}}" required>
    </div>
</div>
<div class="row">
    <div class="col-4 form-group">
        <label for="">CNAE Fiscal:</label>
        <input type="test" class="form-control" name="cnae" id="" value="{{isset($registro->cnae) ? $registro->cnae : ''}}">
    </div>
    <div class="col-8 form-group">
        <label for="">Especificação:</label>
        <input type="text" class="form-control" name="especificacao" id="" value="{{isset($registro->especificacao) ? $registro->especificacao : ''}}">
    </div>
</div>
<div class="row">
    <div class="col-4 form-group">
        <label for="">Situação:<span id="ob">*</span></label>
        <select class="form-control" name="situacao" id="" required>
            <option>Selecione </option>
            <option value="1"
            @if(isset($registro->situacao) && $registro->situacao == 1):
                selected
            @endif
            >Habilitado</option>
            <option value="2"
            @if(isset($registro->situacao) && $registro->situacao == 2):
                selected
            @endif>Suspenso Parcialmente</option>
            <option value="3"
            @if(isset($registro->situacao) && $registro->situacao == 3):
                selected
            @endif>Suspenso Totalmente</option>
            <option value="4"
            @if(isset($registro->situacao) && $registro->situacao == 4):
                selected
            @endif
            >Não Habilitado</option>
        </select>
    </div>
    <div class="col-4 form-group">
        <label for="">Situação Cadastral:<span id="ob">*</span></label>
        <select class="form-control" name="situacaocadastro" id="" required>
            <option>Selecione </option>
            <option value="1"
            @if(isset($registro->situacaocadastro) && $registro->situacaocadastro == 1):
                selected
            @endif>Em cadastramento</option>
            <option value="2"
            @if(isset($registro->situacaocadastro) && $registro->situacaocadastro == 2):
                selected
            @endif>Cadastrado</option>
        </select>
    </div>
    <div class="col-4 form-group">
        <label for="">Status Fornecedor:<span id="ob">*</span></label>
        <select class="form-control" name="status" id="">
                <option value="1"
                @if(isset($registro->status) && $registro->status == 1):
                    selected
                @endif>Ativo</option>
                <option value="2"
                @if(isset($registro->status) && $registro->status == 2):
                    selected
                @endif>Inativo</option>
        </select>
    </div>
</div>
</div>
<div class="tab-pane fade"  id="AnexosContrato" role="tabpanel" aria-labelledby="nav-profile-tab">
<div class="row">
  <div class="col-4 form-group">
    <label for="">Tipo da Conta:<span id="ob">*</span></label>
    <select class="form-control" name="tp_conta" id="">
      <option value="">Selecione</option>
      <option value="1"
      @if(isset($registro->tp_conta) && $registro->tp_conta == 1):
          selected
      @endif>Corrente</option>
      <option value="2"
      @if(isset($registro->tp_conta) && $registro->tp_conta == 2):
          selected
      @endif>Poupança</option>
    </select>
  </div>
  <div class="col-8 form-group">
    <label for="">Banco:<span id="ob">*</span></label>
    <select class="form-control" name="id_banco" id="">
      <option value="">Selecione</option>
      @foreach($bancos as $banco)
      <option {{isset($registro->id_banco) && $registro->id_banco == $banco->id ? 'selected' : ''}}
          value="{{$banco->id}}">
          {{$banco->CodigoBanco}} - {{$banco->NomeInstituicao}}
      </option>
      @endforeach
    </select>
  </div>
</div>
<div class="row">
  <div class="col-4 form-group">
    <label for="">Conta:<span id="ob">*</span></label>
    <input type="text" class="form-control" name="conta" id="" onkeypress="return event.charCode >= 48 && event.charCode <= 57"
    value="{{isset($registro->conta) ? $registro->conta : ''}}">
  </div>
  <div class="col-2 form-group">
    <label for="">Digito:<span id="ob">*</span></label>
    <input type="text" class="form-control" name="dig_conta" maxlength="2" id="" value="{{isset($registro->dig_conta) ? $registro->dig_conta : ''}}">
  </div>
  <div class="col-4 form-group">
    <label for="">Agência:<span id="ob">*</span></label>
    <input type="text" class="form-control" name="agencia" id="" onkeypress="return event.charCode >= 48 && event.charCode <= 57"
    value="{{isset($registro->agencia) ? $registro->agencia : ''}}">
  </div>
  <div class="col-2 form-group">
    <label for="">Digito:<span id="ob">*</span></label>
    <input type="text" class="form-control" name="dig_agencia" maxlength="2" id="" value="{{isset($registro->dig_agencia) ? $registro->dig_agencia : ''}}">
  </div>
</div>
<div class="row">
  <div class="col-12 form-group">
    <label for="">Nome Titular:<span id="ob">*</span></label>
    <input type="text" class="form-control" name="no_titular" id="" onkeyup="this.value = this.value.toUpperCase();"
    value="{{isset($registro->no_titular) ? $registro->no_titular : ''}}">
  </div>
</div>

</div>
</div>
