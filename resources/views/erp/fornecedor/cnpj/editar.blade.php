@extends('layout_admin.principal')
@section ('breadcrumb','Fornecedores - CPF')
@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Editar Fornecedor P.J.</strong>
                                </div>
                            </div>
                        </div>
                        <div class="card-body card-block">
                            <form class="" action="{{route('fornecedor.atualizar' , $registro->id)}}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="put">
                                @include('erp.fornecedor.cnpj.form')
                                <button type="submit" class="btn btn-primary">Atualizar</button>
                            </form><br>
                            <a href="{{ route('fornecedor.index') }}">Fornecedores</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
