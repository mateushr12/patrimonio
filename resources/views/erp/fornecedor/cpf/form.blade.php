<div class="row">
    <div class="col-4 form-group">
        <label for="">CPF:<span id="ob">*</span></label>
        <input name="identificacao" id="identificacao" type="hidden" value="4" >
        <input name="numeroident" type="text" class="form-control MaskCPF"  id="numeroident" onblur="ValidarCPF2(this)"
        {{isset($registro->numeroident) ? 'disabled' : ''}}
        value="{{isset($registro->numeroident) ? $registro->numeroident : ''}}" required>
    </div>
    <div class="col-8 form-group">
        <label for="">Nome:<span id="ob">*</span></label>
        <input name="nome" type="text" class="form-control"  id="" value="{{isset($registro->nome) ? $registro->nome : ''}}" onkeyup="this.value = this.value.toUpperCase();" required>
    </div>
</div>
<div class="row">
    <div class="col-4 form-group">
        <label for="">Telefone:<span id="ob">*</span></label>
        <input name="tel" type="text" class="form-control MaskTel"  id="" value="{{isset($registro->tel) ? $registro->tel : ''}}" required>
    </div>
    <div class="col-8 form-group">
        <label for="">E-mail:<span id="ob">*</span></label>
        <input name="email" type="email" class="form-control"  id="" value="{{isset($registro->email) ? $registro->email : ''}}" required>
    </div>
</div>
<div class="row">
    <div class="col-4 form-group">
        <label for="">Situação:<span id="ob">*</span></label>
        <select name="situacao" class="form-control" id="" required>
            <option>Selecione </option>
            <option value="1"
            @if(isset($registro->situacao) && $registro->situacao == 1):
                selected
            @endif
            >Habilitado</option>
            <option value="2"
            @if(isset($registro->situacao) && $registro->situacao == 2):
                selected
            @endif>Suspenso Parcialmente</option>
            <option value="3"
            @if(isset($registro->situacao) && $registro->situacao == 3):
                selected
            @endif>Suspenso Totalmente</option>
            <option value="4"
            @if(isset($registro->situacao) && $registro->situacao == 4):
                selected
            @endif
            >Não Habilitado</option>
        </select>
    </div>
    <div class="col-4 form-group">
        <label for="">Situação Cadastral:<span id="ob">*</span></label>
        <select class="form-control" name="situacaocadastro" id="" required>
            <option>Selecione </option>
            <option value="1"
            @if(isset($registro->situacaocadastro) && $registro->situacaocadastro == 1):
                selected
            @endif>Em cadastramento</option>
            <option value="2"
            @if(isset($registro->situacaocadastro) && $registro->situacaocadastro == 2):
                selected
            @endif>Cadastrado</option>
        </select>
    </div>
    <div class="col-4 form-group">
        <label for="">Status Fornecedor:<span id="ob">*</span></label>
        <select class="form-control" name="status" id="">
            <option value="1"
            @if(isset($registro->status) && $registro->status == 1):
                selected
            @endif>Ativo</option>
            <option value="2"
            @if(isset($registro->status) && $registro->status == 2):
                selected
            @endif>Inativo</option>
        </select>
    </div>
</div>
<div class="row">
  <div class="col-4 form-group">
    <label for="">Tipo da Conta:<span id="ob">*</span></label>
    <select class="form-control" name="tp_conta" id="">
      <option value="">Selecione</option>
      <option value="1"
      @if(isset($registro->tp_conta) && $registro->tp_conta == 1):
          selected
      @endif>Corrente</option>
      <option value="2"
      @if(isset($registro->tp_conta) && $registro->tp_conta == 2):
          selected
      @endif>Poupança</option>
    </select>
  </div>
  <div class="col-8 form-group">
    <label for="">Banco:<span id="ob">*</span></label>
    <select class="form-control" name="id_banco" id="">
      <option value="">Selecione</option>
      @foreach($bancos as $banco)
      <option {{isset($registro->id_banco) && $registro->id_banco == $banco->id ? 'selected' : ''}}
          value="{{$banco->id }}">
          {{$banco->CodigoBanco}} - {{$banco->NomeInstituicao}}
      </option>
      @endforeach
    </select>
  </div>
</div>
<div class="row">
  <div class="col-4 form-group">
    <label for="">Conta:<span id="ob">*</span></label>
    <input type="text" class="form-control" name="conta" id="" onkeypress="return event.charCode >= 48 && event.charCode <= 57"
    value="{{isset($registro->conta) ? $registro->conta : ''}}">
  </div>
  <div class="col-2 form-group">
    <label for="">Digito:<span id="ob">*</span></label>
    <input type="text" class="form-control" name="dig_conta" id="" value="{{isset($registro->dig_conta) ? $registro->dig_conta : ''}}">
  </div>
  <div class="col-4 form-group">
    <label for="">Agência:<span id="ob">*</span></label>
    <input type="text" class="form-control" name="agencia" id="" onkeypress="return event.charCode >= 48 && event.charCode <= 57"
    value="{{isset($registro->agencia) ? $registro->agencia : ''}}">
  </div>
  <div class="col-2 form-group">
    <label for="">Digito:<span id="ob">*</span></label>
    <input type="text" class="form-control" name="dig_agencia" id="" value="{{isset($registro->dig_agencia) ? $registro->dig_agencia : ''}}">
  </div>
</div>
<div class="row">
  <div class="col-12 form-group">
    <label for="">Nome Titular:<span id="ob">*</span></label>
    <input type="text" class="form-control" name="no_titular" id=""
    onkeyup="this.value = this.value.toUpperCase();" value="{{isset($registro->no_titular) ? $registro->no_titular : ''}}">
  </div>
</div>
