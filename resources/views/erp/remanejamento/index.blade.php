@extends('layout_admin.principal')
@section ('breadcrumb','Remanejamento')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-10">
                                        <strong>Remanejamento</strong>
                                    </div>
                                    <div class="col-2">
                                        <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#FormModal">
                                            <i class="zmdi zmdi-plus"></i>Adicionar
                                        </button>
                                    </div>
                                </div>
                            </div>
                            @if (session('erro'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            @endif
                            <div class="card-body card-block">
                                <!-- DATA TABLE -->
                                {{-- <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                        <div class="rs-select2--light rs-select2--md">
                                            <select class="js-select2" name="property">
                                                <option selected="selected">All Properties</option>
                                                <option value="">Option 1</option>
                                                <option value="">Option 2</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <div class="rs-select2--light rs-select2--sm">
                                            <select class="js-select2" name="time">
                                                <option selected="selected">Today</option>
                                                <option value="">3 Days</option>
                                                <option value="">1 Week</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <button class="au-btn-filter">
                                            <i class="zmdi zmdi-filter-list"></i>filters
                                        </button>
                                    </div>
                                    <div class="table-data__tool-right">
                                        <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                            <select class="js-select2" name="type">
                                                <option selected="selected">Exportar</option>
                                                <option value="">Option 1</option>
                                                <option value="">Option 2</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="table-responsive table-responsive-data2">

                                    <table class="table table-data2">
                                        <thead>
                                        <tr>
                                            {{-- <th>
                                                <label class="au-checkbox">
                                                    <input type="checkbox">
                                                    <span class="au-checkmark"></span>
                                                </label>
                                            </th> --}}
                                            <th>Ação - Natureza Origem</th>
                                            <th>Ação - Natureza Destino</th>
                                            <th>Valor</th>
                                            <th>Data</th>
                                            <th>Observação</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($remanejamento as $remanejamentos)
                                            <tr class="tr-shadow">
                                                {{-- <td>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </td> --}}
                                                <td>
                                                @foreach($acao as $acoes)
                                                 @if($remanejamentos->codAcao_origem == $acoes->id) {{$acoes->CodAcao}}. {{$acoes->DescAcao}} - {{ $acoes->grupo->DescGrupo }} @endif
                                                @endforeach
                                                </td>
                                                <td>
                                                @foreach($acao as $acoes)
                                                 @if($remanejamentos->codAcao_destino == $acoes->id) {{$acoes->CodAcao}}. {{$acoes->DescAcao}} - {{ $acoes->grupo->DescGrupo }} @endif
                                                @endforeach
                                                </td>
                                                <td>{{ number_format($remanejamentos->valor,2,',','.') }}</td>
                                                <td>{{$remanejamentos->data->format('d/m/Y')}}</td>
                                                <td>{{$remanejamentos->obs}}</td>
                                                <td>
                                                    <div class="table-data-feature">
                                                      @if (in_array('erp/remanejamento/{id}/edit', Session::get('permissoes.nomes')))
                                                        <a href="{{route('remanejamento.edit', $remanejamentos->id)}}">
                                                            <button type="button"  class="item"  title="Editar">
                                                                <i class="zmdi zmdi-edit"></i>
                                                            </button>
                                                        </a>
                                                      @else
                                                      @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="spacer"></tr>
                                        @empty
                                            <tr>
                                                Nenhum Remanejamento Cadastrado.
                                            </tr>

                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>


    <!-- modal form REMANEJAMENTO-->
    <div class="modal fade" id="FormModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="FormModalLabel">Cadastro de Remanejamento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body card-block">
                            <form class="" action="{{route('remanejamento.store')}}" method="post">
                                {{ csrf_field() }}
                                @include('erp.remanejamento._form')
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">
                        Salvar
                    </button>
                    <button type="reset" class="btn btn-warning" data-dismiss="modal">
                        Cancelar
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end modal form REMANEJAMENTO -->

    <script>
            function k(i) {
                var v = i.value.replace(/\D/g,'');
                v = (v/100).toFixed(2) + '';
                v = v.replace(".", ".");
                v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                i.value = v;
            }
        </script>


@endsection
