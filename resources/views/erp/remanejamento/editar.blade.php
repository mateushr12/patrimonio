@extends('layout_admin.principal')
@section ('breadcrumb','Remanejamento')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-10">
                                        <strong>Editar Remanejamento</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">
                                <form class="" action="{{route('remanejamento.update', $remanejamento->id)}}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="put">
                                    @include('erp.remanejamento._form')
                                    <button type="submit" class="btn btn-primary">Atualizar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <script>
            function k(i) {
                var v = i.value.replace(/\D/g,'');
                v = (v/100).toFixed(2) + '';
                v = v.replace(".", ".");
                v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                i.value = v;
            }
        </script>


@endsection
