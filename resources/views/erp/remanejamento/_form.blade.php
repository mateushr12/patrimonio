<div class="row">
    <div class="form-group col-sm-12">
        <label for="UniEspecificacao">Ação de Origem:*</label>
        <select class="form-control" id="codAcao_origem" name="codAcao_origem" required>
                <option value="">ESCOLHA AÇÃO ORIGEM</option>
        @foreach($acao as $ac)
                <option
                @if (isset($remanejamento->codAcao_origem) and $remanejamento->codAcao_origem == $ac->id)
                    selected
                @endif
                value="{{ $ac->id }}">{{$ac->CodAcao}} - {{$ac->DescAcao}}</option>
        @endforeach
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-12">
        <label for="UniEspecificacao">Ação de Destino:*</label>
        <select class="form-control" id="codAcao_destino" name="codAcao_destino" required>
                <option value="">ESCOLHA AÇÃO DESTINO</option>
        @foreach($acao as $ac)
                <option
                @if (isset($remanejamento->codAcao_destino) and $remanejamento->codAcao_destino == $ac->id)
                    selected
                @endif
                value="{{ $ac->id }}">{{$ac->CodAcao}} - {{$ac->DescAcao}}</option>
        @endforeach
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6">
        <label for="valor">Valor <strong>(R$)</strong> :*</label>
        <input type="text" class="form-control InputValor Monetario" onkeyup="k(this);" name="valor" placeholder="0.00" value="{{isset($remanejamento->valor) ? $remanejamento->valor : ''}}"   autocomplete="off"  required/>
    </div>
    <div class="form-group col-sm-6">
        <label for="Data">Data:*</label>
        <input type="date" class="form-control" id="data" name="data" value="{{isset($remanejamento->data) ? $remanejamento->data : ''}}"   autocomplete="off"  required/>
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-12">
        <label for="Observação">Observação:*</label>
        <input type="text" class="form-control" id="obs" name="obs" value="{{isset($remanejamento->obs) ? $remanejamento->obs : ''}}"   autocomplete="off"  required/>
    </div>
</div>
