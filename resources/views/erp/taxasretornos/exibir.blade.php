@extends('layout_admin.principal')
@section ('breadcrumb','Exibir Taxas Retorno')

@section('conteudo')
<section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Relatório Taxas Retorno Banese</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">
                                <form action="{{route('taxasRetorno.retorno')}}" method="get" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-12 form-group">
                                            <label for="nosso_numero"><strong>Por Nosso Número:</strong></label>
                                            <input type="text" name="nosso_numero" id="nosso_numero" class="form-control">
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <p><strong>Por Data do Pagamento:</strong></p>
                                            <div class="row">
                                                <div class="col-6 form-group">
                                                    <label for="dt_paga_in">Início:</label>
                                                    <input type="date" name="dt_paga_in" id="dt_paga_in" class="form-control">
                                                </div>
                                                <div class="col-6 form-group">
                                                    <label for="dt_paga_fl">Final:</label>
                                                    <input type="date" name="dt_paga_fl" id="dt_paga_fl" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 form-group ">
                                            <button type="submit" class="btn btn-primary">Buscar</button>
                                            <button type="reset" class="btn btn-warning">Limpar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
