@extends('layout_admin.principal')
@section ('breadcrumb','Taxas Retorno')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-10">
                                      <h3>Resultado da busca</h3>
                                      <span>Periodo: {{ $ma }}</span>
                                    </div>

                                </div>
                            </div>
                            <div class="card-body card-block" style=" height: 800px; overflow-y: scroll;">
                                @if(count($busca) > 0)
                                <table class="table table-sm" >
                                    <thead>
                                        <tr>
                                            <th>Data</th>
                                            <th>Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($busca as $it)
                                        <tr>
                                            <td>{{ $it->dia }}</td>
                                            {{-- <td>{{ \Carbon\Carbon::parse($it['data_pagamento'])->format('d/m/Y') }}</td> --}}
                                            <td>R$ {{ number_format($it->soma,2,',','.') }}</td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td><strong>TOTAL</strong></td>
                                            {{-- <td>{{ \Carbon\Carbon::parse($it['data_pagamento'])->format('d/m/Y') }}</td> --}}
                                            <td><strong>R$ {{ number_format($tt,2,',','.') }}</strong></td>
                                        </tr>
                                    </tbody>
                                </table>
                                @else
                                  Nenhuma taxa encontrada.
                                @endif
                                {{-- <div class="row">
                                    <div class="col-4"></div>
                                    <div class="col-6">{{ $busca->render() }}</div>
                                    <div class="col-2"></div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>


@endsection
