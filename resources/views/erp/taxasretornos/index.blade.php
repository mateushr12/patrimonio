@extends('layout_admin.principal')
@section ('breadcrumb','Taxas Retorno')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                    @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Retorno de Taxas do BANESE</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">
                                <form action="{{route('taxasRetorno.store')}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-4 form-group">
                                            <label for="protocolo"><strong>Carregar Arquivos:</strong></label>
                                            <input type="file" name="arqretorno[]" id="arqretorno" class="form-control" multiple>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 form-group ">
                                            <button type="submit" class="btn btn-primary">Salvar</button>
                                            <button type="reset" class="btn btn-warning">Limpar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Arquivos Salvos</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">
                                @if($files->count())
                                <table class="table">
                                    <th>Name</th>
                                    <th>Data Envio</th>
                                    <th>Data Processamento</th>
                                    <th>Status</th>
                                    <th>Ação</th>
                                    @foreach($files as $file)
                                    <tr>
                                    <td>{{ $file->filename }}</td>
                                    <td>{{\Carbon\Carbon::parse($file->created_at)->format('d/m/Y')}}</td>
                                    <td>
                                        @if($file->dataprocessamento)
                                        {{ date('d/m/Y', strtotime($file->dataprocessamento)) }}
                                        @else

                                        @endif
                                    </td>
                                    <td>
                                        @if ($file->status == 0)
                                        <span class="badge badge-danger">Aguardando Processamento.</span>
                                        @else
                                        <span class="badge badge-success">Processado.</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($file->status == 0 and substr($file->filename, 0, 3) == 'SPC')
                                        <a href="{{route('taxasRetorno.ProcessaArquivoSPC' , $file->id)}}" title='Processar' class="btn btn-info btn-sm">
                                            <i class="fas fa-bolt"></i>
                                        </a>
                                        @elseif ($file->status == 0 and substr($file->filename, 0, 3) == 'COB')
                                        <a href="{{route('taxasRetorno.ProcessaArquivoCOB' , $file->id)}}" title='Processar' class="btn btn-info btn-sm">
                                            <i class="fas fa-bolt"></i>
                                        </a>
                                        @else
                                        @endif
                                    </td>
                                    </tr>
                                    @endforeach
                                </table>
                                @else
                                Você ainda não possui arquivos!
                                @endif
                            </div>
                            <div class="card-footer">
                                {{ $files->links() }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
    </section>
@endsection
