@extends('layout_admin.principal')
@section ('breadcrumb','Exibir Taxas Retorno')

@section('conteudo')
<section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Relatório Taxas Retorno Banese por Dia</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">
                                <form action="{{route('taxasRetorno.resultadopordia')}}" method="post" >
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-12 form-group">
                                            <label for="nosso_numero"><strong>Por Mês/Ano:</strong></label>
                                            <select class="form-control" name="mesano" >
                                                <option value="">SELECIONE O PERIODO</option>
                                                @foreach($mesano as $key)
                                                <option value="{{$key->mesano }}">
                                                    {{ $key->mesano }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-12 form-group ">
                                            <button type="submit" class="btn btn-primary">Buscar</button>
                                            <button type="reset" class="btn btn-warning">Limpar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
