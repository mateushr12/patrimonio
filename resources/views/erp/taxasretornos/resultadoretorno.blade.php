@extends('layout_admin.principal')
@section ('breadcrumb','Taxas Retorno')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-10">
                                      <h3>Resultado da busca</h3>
                                      <strong>Total:</strong> R$ {{ number_format($somaRec,2,',','.') }}&nbsp;&nbsp;&nbsp;<strong>Quantidade:</strong> {{ $registros }}
                                    </div>

                                </div>
                            </div>
                            <div class="card-body card-block" style=" height: 800px; overflow-y: scroll;">
                                @if(count($busca) > 0)
                                <table class="table table-sm" >
                                    <thead>
                                        <tr>
                                            <th>Nosso Nº</th>
                                            <th>Data de Compensação</th>
                                            <th>Valor</th>
                                            <th>Evento</th>
                                            {{-- <th>Valor Cobrado</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($busca as $it)
                                        <tr>
                                            <td>{{ $it['nosso_numero'] }}</td>
                                            <td>{{ \Carbon\Carbon::parse($it['data_pagamento'])->format('d/m/Y') }}</td>
                                            <td>R$ {{ $it['valor'] }}</td>
                                            <td>{{ $it['descricao']}}</td>
                                            {{-- <td>R$ {{ $it['valor_cobrado']}} --}}

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                  Nenhuma taxa encontrada.
                                @endif
                                {{-- <div class="row">
                                    <div class="col-4"></div>
                                    <div class="col-6">{{ $busca->render() }}</div>
                                    <div class="col-2"></div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>


@endsection
