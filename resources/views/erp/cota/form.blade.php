<div class="row">
    <div class="col-12 form-group">
        <label for="cotaacao">Ação</label>
        <select name="cotaacao" id="cotaacao" class="form-control"
        {{ isset($registro->cotaacao) ? 'disabled' : '' }} >
            <option >Selecione uma ação</option>
            @foreach ($cotaacoes as $cotaacao)
            <option value="{{$cotaacao->id}}"
              {{ isset($registro->cotaacao) && $cotaacao->id == $registro->cotaacao ? 'selected' : '' }} >
                {{$cotaacao->CodAcao}} - {{$cotaacao->DescAcao}}
            </option>
            @endforeach
        </select>
    </div>
</div>
<div class="row">
    <div class="col-6 form-group">
        <label for="mesrefcota">Mês de referência</label>
        <select class="form-control" name="mesrefcota" id="mesrefcota"
        {{ isset($registro->mesrefcota) ? 'disabled' : '' }} >
            <option>Escolha o mês</option>
            <option value="1" @if(isset($registro->mesrefcota) && $registro->mesrefcota == 1):
                selected
            @endif>Janeiro</option>
            <option value="2" @if(isset($registro->mesrefcota) && $registro->mesrefcota == 2):
                    selected
                @endif>Fevereiro</option>
            <option value="3" @if(isset($registro->mesrefcota) && $registro->mesrefcota == 3):
                    selected
                @endif>Março</option>
            <option value="4" @if(isset($registro->mesrefcota) && $registro->mesrefcota == 4):
                    selected
                @endif>Abril</option>
            <option value="5" @if(isset($registro->mesrefcota) && $registro->mesrefcota == 5):
                    selected
                @endif>Maio</option>
            <option value="6" @if(isset($registro->mesrefcota) && $registro->mesrefcota == 6):
                    selected
                @endif>Junho</option>
            <option value="7" @if(isset($registro->mesrefcota) && $registro->mesrefcota == 7):
                    selected
                @endif>Julho</option>
            <option value="8" @if(isset($registro->mesrefcota) && $registro->mesrefcota == 8):
                    selected
                @endif>Agosto</option>
            <option value="9" @if(isset($registro->mesrefcota) && $registro->mesrefcota == 9):
                    selected
                @endif>Setembro</option>
            <option value="15" @if(isset($registro->mesrefcota) && $registro->mesrefcota == 10):
                    selected
                @endif>Outubro</option>
            <option value="11" @if(isset($registro->mesrefcota) && $registro->mesrefcota == 11):
                    selected
                @endif>Novembro</option>
            <option value="12" @if(isset($registro->mesrefcota) && $registro->mesrefcota == 12):
                    selected
                @endif>Dezembro</option>
        </select>
    </div>
    <div class="col-6 form-group">
        <label for="valorcota">Valor <strong>(R$):</strong></label>
        <input type="text" name="valorcota" id="valorcota" class="form-control" onkeyup="k(this);" value="{{isset($registro->valorcota) ? $registro->valorcota : ''}}">
    </div>
</div>
<div class="row">
    @if (isset($registro->valorcota))

    @else
    <div class="col-12 form-group">
        <label for="">Valor da Ação <strong>(R$):</strong></label>
        <input type="text" readonly name="" id="valordisponivel" class="form-control" value="">
    </div>
    @endif
</div>
