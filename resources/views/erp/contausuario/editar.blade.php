@extends('layout_admin.principal')
@section ('breadcrumb','Usuario')

@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                @if (session('sucesso'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{session('sucesso')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif (session('erro'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{session('erro')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                        <strong>Minha Conta</strong>
                        </div>
                        <div class="card-body card-block">
                            <form class="" action="{{route('contausuario.update')}}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{$user->id}}" />
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="Nome">Nome:</label>
                                        <input type="text" class="form-control" id="Nome" name="nome"
                                            value="{{$user->name}}" required />
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="Email">Email:</label>
                                        <input type="email" class="form-control" id="Email" value="{{$user->email}}"
                                            name="Email" disabled />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <label for="Grupocad">Grupo:</label>
                                        <input type="text" class="form-control" id="Grupocad" name="grupocad"
                                            value="{{$user->grupo->nome}}" disabled />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Atualizar Conta</button>
                                </div>
                            </form>
                            <a class="btn btn-warning" href="/contausuario/alterarSenha">Alterar Senha</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
