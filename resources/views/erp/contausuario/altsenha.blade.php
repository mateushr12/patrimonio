@extends('layout_admin.principal')
@section ('breadcrumb','Usuario')

@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                @if (session('sucesso'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{session('sucesso')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif (session('erro'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{session('erro')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Alterar Senha</strong>
                        </div>
                        <div class="card-body card-block">
                            <form class="" action="{{route('contausuario.updatesenha')}}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="senha">Nova Senha:</label>
                                        <input type="password" class="form-control" id="Senha" name="senha" required />
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="csenha">Confirmar Senha:</label>
                                        <input type="password" class="form-control" id="Csenha" name="csenha" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Alterar Senha</button>
                                    <button type="reset" class="btn btn-warning">Limpar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
