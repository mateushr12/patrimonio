@extends('layout_admin.principal')
@section ('breadcrumb','Orçamento Inicial')

@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-10">
                                    <strong>Orçamento Inicial</strong>
                                </div>
                                <div class="col-2">
                                    <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small"
                                        data-toggle="modal" data-target="#FormModal">
                                        <i class="zmdi zmdi-plus"></i>Adicionar
                                    </button>
                                </div>
                            </div>
                        </div>

                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card-body card-block">
                            <!-- DATA TABLE -->
                            <!--<div class="table-data__tool">-->
                            {{-- <div class="table-data__tool-left">
                                    <div class="rs-select2--light rs-select2--md">
                                        <select class="js-select2" name="property">
                                            <option selected="selected">All Properties</option>
                                            <option value="">Option 1</option>
                                            <option value="">Option 2</option>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                    <div class="rs-select2--light rs-select2--sm">
                                        <select class="js-select2" name="time">
                                            <option selected="selected">Today</option>
                                            <option value="">3 Days</option>
                                            <option value="">1 Week</option>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                    <button class="au-btn-filter">
                                        <i class="zmdi zmdi-filter-list"></i>filters
                                    </button>
                                </div> --}}
                            {{-- <div class="table-data__tool-right">
                                    <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                        <select class="js-select2" name="type">
                                            <option selected="selected">Exportar</option>
                                            <option value="">Option 1</option>
                                            <option value="">Option 2</option>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                </div> --}}
                            <!--</div>-->
                            <div class="table-responsive table-responsive-data2">

                                <table class="table table-data2">
                                    <thead>
                                        <tr>
                                            {{-- <th>
                                                <label class="au-checkbox">
                                                    <input type="checkbox">
                                                    <span class="au-checkmark"></span>
                                                </label>
                                            </th> --}}
                                            <th>Especificação</th>
                                            <th>Receita Orçada</th>
                                            <th>Receita Atualizada</th>
                                            <th>Ano</th>
                                            <th>Ativo</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($orcamento_resultado as $orcamentos)
                                        <tr class="tr-shadow">
                                            {{-- <td>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </td> --}}
                                            <td>{{$orcamentos->especificacao}}</td>
                                            <td>R$ {{ number_format($orcamentos->receita_orcada,2,',','.') }}</td>
                                            <td>R$ {{ number_format($orcamentos->atual,2,',','.') }}</td>
                                            <td>{{$orcamentos->Ano->ano}}</td>
                                            <td>{{$orcamentos->ativo == 0 ? 'Sim' : 'Não'}}</td>
                                            <td>
                                                <div class="table-data-feature">
                                                  {{-- <a href="{{ route('orcamentoinicial.ver', $orcamentos->id) }}" target="_blank">
                                                      <button type="button" class="item" title="Ver Orçamento"
                                                      data-toggle="tooltip" data-placement="top" >
                                                          <i class="zmdi zmdi-eye"></i>
                                                      </button>
                                                  </a>&nbsp; --}}
                                                @if (in_array('erp/orcamentoinicial/{id}/edit', Session::get('permissoes.nomes')))
                                                    <a href="{{route('orcamentoinicial.edit', $orcamentos->id)}}">
                                                        <button type="button" class="item" title="Editar">
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </button>
                                                    </a>
                                                @else
                                                @endif
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="spacer"></tr>
                                        @empty
                                        <tr>
                                            Nenhum Orçamento Cadastrado.
                                        </tr>

                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- modal form ORÇAMENTO-->
<div class="modal fade" id="FormModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="FormModalLabel">Cadastro de Orçamento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form class="" action="{{route('orcamentoinicial.store')}}" method="post">
                            {{ csrf_field() }}
                            @include('erp.orcamentoinicial._form')
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Salvar
                </button>
                <button type="reset" class="btn btn-warning" data-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
        </form>
    </div>
</div>
<!-- end modal form ORÇAMENTO -->

<script>
    function k(i) {
                var v = i.value.replace(/\D/g,'');
                v = (v/100).toFixed(2) + '';
                v = v.replace(".", ".");
                v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                i.value = v;
            }
</script>


@endsection
