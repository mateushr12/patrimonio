<div class="row">
    <div class="form-group col-sm-12">
        <label for="UniEspecificacao">Especificação:</label>
        <input type="text" class="form-control" id="especificacao" name="especificacao" placeholder=""
            value="{{isset($orcamento->especificacao) ? $orcamento->especificacao : ''}}" autocomplete="off"
            required />
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6">
            <label for="ReceitaOrcada">Receita <strong>(R$)</strong>:</label>
            <input type="text" class="form-control InputValor Monetario" onkeyup="k(this);" id="receita_orcada"
                name="receita_orcada" placeholder="0.00" {{ isset($orcamento->receita_orcada) ? 'disabled' : '' }}
                value="{{isset($orcamento->receita_orcada) ? $orcamento->receita_orcada : ''}}"
                autocomplete="off" required />
    </div>
    <div class="form-group col-sm-4">
        <label for="AnoOrcamento">Ano:</label>
        <select class="form-control" id="ano" name="ano" required {{ isset($orcamento->ano) ? 'disabled' : '' }} >
        @foreach($anos as $ano)
                <option
                @if (isset($orcamento->ano) and $orcamento->ano == $ano->id)
                    selected
                @endif
                value="{{ $ano->id }}">{{$ano->ano}}</option>
        @endforeach
        </select>
    </div>
    <div class="form-group col-sm-2">
        <label for="Ativo">Ativo:</label>
        <select class="form-control" id="ativo" name="ativo" required>
                <option  @if (isset($orcamento->ativo) and $orcamento->ativo == 0)
                            selected
                         @endif
                            value="0">SIM
                </option>
                <option  @if (isset($orcamento->ativo) and $orcamento->ativo == 1)
                            selected
                         @endif
                            value="1">NÃO
                </option>
        </select>
    </div>
</div>
