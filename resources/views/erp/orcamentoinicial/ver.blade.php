<!DOCTYPE html>
<html lang="pt-br">
<head>
  <!-- Required meta tags-->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="JUCESE - Junta Comercial do Estado de Sergipe">

  <!-- Title Page-->
  <title>ERP - JUCESE</title>

  <link href="<?php echo asset('vendor/bootstrap-4.1/bootstrap.min.css')?>" rel="stylesheet" media="all">


</head>

<body>

  <div class="container">
    <table style="width: 900px">
      <thead >
        <tr>
          <td style=" font-size: 22px;" ><strong>Orçamento Inicial</strong><p></p></td>
          <td style="font-size: 14px; float: right;" >Data {{ date('d/m/Y') }}</td>
        </tr>
      </thead>
      <tbody>
        <tr><td colspan="2">
          <table class="table table-data2" id="tabela" border="1">
            <tbody>
              <tr>
                <td colspan="2"><strong>Especificação:</strong><br> {{$orcamento_resultado->especificacao}}</td>
                <td colspan="1"><strong>Orçamento Atualizado:</strong><br>R$ {{ number_format($totalOrc,2,',','.') }}</td>
              </tr>
              <tr>
                <td><strong>Ano:</strong><br> {{ $orcamento_resultado->Ano->ano }}</td>
                <td><strong>Status:</strong><br> {{ $orcamento_resultado->ativo == 0 ? 'ATIVO' : 'INATIVO' }}</td>
                <td><strong>Orçamento Inicial:</strong><br>R$ {{ number_format($orcamento_resultado->receita_orcada,2,',','.') }}</td>
              </tr>
            </tbody>
          </table>
        </td></tr>
      </tbody>
    </table>
  </div>

</body>
</html>
