@extends('layout_admin.principal')
@section ('breadcrumb','Sub-Elementos')

@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                @if (session('sucesso'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{session('sucesso')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif (session('delete'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{session('delete')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="col-12">
                    <h3>Sub-Elementos</h3>
                    {{-- <div class="table-data__tool">
                        <div class="table-data__tool-left"></div>
                        <div class="table-data__tool-right">
                            <a class="au-btn au-btn-icon au-btn--green au-btn--small"
                                href="{{route('permissao.novo')}}">
                    <i class="zmdi zmdi-plus"></i>Nova Permissão</a>
                </div>
            </div> --}}
            <div class="card">
                <table class="table table-borderless table-data3">
                    <thead>
                        <tr>
                            <th>Cod. Sub-Elemento</th>
                            <th>Sub-Elemento</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($subelemento as $key)
                        <tr>
                        <td>{{ $key->codnatureza }}</td>
                        <td>{{$key->descricao}}</td>
                            <td>
                              <div class="table-data-feature">
                                <a href="{{ route('subelemento.deletar', $key->id) }}"
                                    data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                    <button type="button" class="item" title="Deletar">
                                        <i class="zmdi zmdi-delete"></i>
                                    </button>
                                </a>
                              </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            @if (session('erro'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{session('erro')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <strong>Cadastro de Sub-Elementos</strong>
                </div>
                <div class="card-body card-block">
                    <form class="" action="{{ route('subelemento.salvar') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="id_elemento" value="{{ $idele }}" />
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="grupo">Cod. Sub-Elemento:</label>
                                <input type="text" class="form-control" id="codnatureza" name="codnatureza" onkeyup="this.value = this.value.toUpperCase();" required />
                            </div>
                            <div class="form-group col-sm-8">
                                <label for="grupo">Sub-Elemento:</label>
                                <input type="text" class="form-control" id="descricao" name="descricao" onkeyup="this.value = this.value.toUpperCase();" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Cadastrar Sub-Elemento</button>
                            {{-- <br><br>
                                        <a href="{{route('permissao')}}">Listar Permissões</a> --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</section>

<script>
    function myFunction(i) {

        //var idK = element.id;
        var c = confirm('Tem certeza que deseja deletar?');
        if( c == true){
            var a = "/autorizacao/grupos/excluir/"
            var b = i
            window.location.href = a.concat(b)
        }
    }
</script>




@endsection
