@extends('layout_admin.principal')
@section ('breadcrumb','Corrigir Pagamento')
@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-9">
                                    <strong>Corrigir Pagamento</strong>
                                </div>
                            </div>
                        </div>

                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="table-responsive table-responsive-data2">
                                        <table class="table" id="tabel">
                                            <thead>
                                                <tr>
                                                    <th>Empenho</th>
                                                    <th>O.B.</th>
                                                    <th>Dt. Pagamento</th>
                                                    <th>N.F. / Fatura</th>
                                                    <th>Dt. Vencimento</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($pag as $key)
                                                <tr>
                                                  <form class="" action="{{ route('corrigirObPost') }}" method="post">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="put">
                                                    <td>{{$key->empenho->numero }}</td>
                                                    <td>{{$key->ob }}</td>
                                                    <td>{{\Carbon\Carbon::parse($key->datapag)->format('d/m/Y')}}</td>
                                                    <td><input type="text" name="nf" class="form-control" value="{{ $key->nf }}" required/></td>
                                                    <td><input type="date" name="vencimento" value="{{ $key->vencimento }}" class="form-control" required/></td>
                                                    <input type="hidden" name="id" value="{{ $key->id }}" />
                                                    <td><button type="submit" class="btn btn-primary">Corrigir</button></td>
                                                  </form>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>

$(document).ready( function () {
  $('#tabel').DataTable({
    paging: false,

    language: {
      search: "Buscar:",
      zeroRecords: "Nenhum registro encontrado.",
      info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      infoEmpty: "Mostrando 0 até 0 de 0 registros",
      lengthMenu: "Mostrar _MENU_ entradas",
      infoFiltered: "(filtrado de _MAX_ registros)",
      paginate: {
        first: "Primeiro",
        previous: "Anterior",
        next: "Proximo",
        last: "Ultimo"
      },
      emptyTable: "Nenhum registro encontrado."
    }
  });
});


    function k(i) {
                        var v = i.value.replace(/\D/g,'');
                        v = (v/100).toFixed(2) + '';
                        v = v.replace(".", ".");
                        v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
                        v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
                        i.value = v;
                  }
</script>
@endsection
