@extends('layout_admin.principal')
@section ('breadcrumb','Taxas')

@section('conteudo')
  <section>
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">

              <div class="card-header">
                <div class="row">
                  <div class="col-12">
                    <strong>Boleto Serasa/Boa Vista</strong>
                  </div>
                </div>
              </div>
              <div class="card-body card-block">
                @if (session('sucesso'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{session('sucesso')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif (session('delete'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{session('delete')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <form action="{{route('taxas.serasa.buscar')}}" method="post">
                  {{csrf_field()}}
                <div class="row">
                  <div class="col-6 form-group">
                    <label for="nosso_numero"><strong>Nº do Documento:</strong></label>
                    <input type="text" name="nn" id="nosso_numero"
                    onkeyup="this.value = this.value.toUpperCase();" class="form-control" required>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 form-group ">
                    <button type="submit" class="btn btn-primary">Buscar</button>
                    <button type="reset" class="btn btn-warning">Limpar</button>
                  </div>
                </div>
              </form>
                <hr>
                <div class="row"></div>
              </div>
              <div class="card">
                <div class="card-header">
                  <div class="row">
                    <div class="col-12">
                      <strong>Taxas Serasa/Boa Vista</strong>
                    </div>
                  </div>
                </div>
                @if (isset($serasa))
                <div class="card-body">
                  <form action="{{route('taxas.serasa.atualizar')}}" method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="put">
                  <table class="table table-sm">
                    <tr>
                      <th>N° Documento</th>
                      <th>Data</th>
                      <th>Valor</th>
                      <th>Tipo</th>
                      <th>Serasa/BV</th>
                      <th></th>
                    </tr>
                    @foreach ($serasa as $row)
                      <input type="hidden" name="id" value="{{ $row->id }}" />
                    <tr>
                      <td>{{ $row->nosso_numero }}</td>
                      <td>{{ \Carbon\Carbon::parse($row->data_pagamento)->format('d/m/Y') }}</td>
                      <td>{{ number_format($row->valor,2,',','.') }}</td>
                      @if ($row->tipo === "0000")
                        <td>Serasa</td>
                      @elseif ($row->tipo === "0001")
                        <td>Boa Vista</td>
                      @else
                        <td> - </td>
                      @endif
                      <td>
                        <select name="tipo" class="form-control" required>
                          <option value="">Escolha</option>
                          <option value="0000">Serasa</option>
                          <option value="0001">Boa Vista</option>
                        </select>
                      </td>
                      <td><input type="submit" value="Atualizar" class="btn btn-primary" /></td>
                    </tr>
                  @endforeach
                  </table>
                </form>
                </div>
                @else
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <script>
    function k(i) {
      var v = i.value.replace(/\D/g,'');
      v = (v/100).toFixed(2) + '';
      v = v.replace(".", ",");
      v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
      v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
      i.value = v;
    }
    </script>

  @endsection
