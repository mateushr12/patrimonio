@extends('layout_admin.principal')
@section ('breadcrumb','Taxas')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-10">
                                      <h3>Resultado da busca</h3>
                                      <strong>Total:</strong> R$ {{ number_format($somaRec,2,',','.') }}&nbsp;&nbsp;&nbsp;<strong>Quantidade:</strong> {{ $registros }}
                                    </div>
                                    <div class="col-2">
                                        <a class="au-btn au-btn-icon au-btn--green au-btn--small"
                                            href="{{route('taxas.pdf')}}" target="_blank">
                                            <i class="zmdi zmdi-print"></i>Imprimir</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block" style=" height: 800px; overflow-y: scroll;">
                                @if(count($busca) > 0)
                                <table class="table table-sm" >
                                    <thead>
                                        <tr>
                                            <th>Protocolo</th>
                                            <th>CPF</th>
                                            <th>CNPJ</th>
                                            <th>Nosso Nº</th>
                                            <th>Nº Doc.</th>
                                            <th>Dt. Emissão</th>
                                            <th>Dt. Pagamento</th>
                                            <th>Valor</th>
                                            <th>Evento</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($busca as $it)
                                        <tr>
                                            <td>{{ $it['protocolo'] }}</td>
                                            <td>{{ $it['cpf_solicitante'] }}</td>
                                            <td>{{ $it['cnpj_solicitante'] }}</td>
                                            <td>{{ $it['nosso_numero'] }}</td>
                                            <td>{{ $it['cd_documento'] }}</td>
                                            <td>{{ \Carbon\Carbon::parse($it['dt_emissao'])->format('d/m/Y') }}</td>
                                            <td>{{ \Carbon\Carbon::parse($it['dt_pagamento'])->format('d/m/Y') }}</td>
                                            <td>{{ $it['vl_recebido'] }}</td>
                                            <td>{{ $it['tipo_evento_redesim'] }}</td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <a href="{{route('taxas.detalhes' , $it['nosso_numero'])}}">
                                                        <button type="button" class="item" title="" data-toggle="tooltip" data-placement="top" data-original-title="Detalhes">
                                                            <i class="fas fa-search"></i>
                                                        </button>
                                                    </a>&nbsp;
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                  Nenhuma taxa encontrada.
                                @endif
                                {{-- <div class="row">
                                    <div class="col-4"></div>
                                    <div class="col-6">{{ $busca->render() }}</div>
                                    <div class="col-2"></div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>


@endsection
