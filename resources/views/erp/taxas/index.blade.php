@extends('layout_admin.principal')
@section ('breadcrumb','Taxas')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Buscar Taxas</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">
                                <form action="{{route('taxas.resultado')}}" method="get" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-4 form-group">
                                            <label for="protocolo"><strong>por Protocolo:</strong></label>
                                            <input type="text" name="protocolo" id="protocolo" onkeyup="this.value = this.value.toUpperCase();" class="form-control">
                                        </div>
                                        <div class="col-4 form-group">
                                            <label for="cpf_solicitante"><strong>por CPF:</strong></label>
                                            <input type="text" name="cpf_solicitante" id="cpf_solicitante" class="form-control" maxlength="11">
                                        </div>
                                        <div class="col-4 form-group">
                                            <label for="cnpj_solicitante"><strong>por CNPJ:</strong></label>
                                            <input type="text" name="cnpj_solicitante" id="cnpj_solicitante" class="form-control" maxlength="14">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4 form-group">
                                            <label for="nosso_numero"><strong>por Nosso Nº:</strong></label>
                                            <input type="text" name="nosso_numero" id="nosso_numero" class="form-control">
                                        </div>
                                        <div class="col-4 form-group">
                                            <label for="cd_documento"><strong>por Nº Documento:</strong></label>
                                            <input type="text" name="cd_documento" id="cd_documento" class="form-control">
                                        </div>
                                        <div class="col-4 form-group">
                                            <label for="tipo_evento_redesim"><strong>Tipo do Evento</strong></label>
                                            <select name="tipo_evento_redesim" id="tipo_evento_redesim" class="form-control">
                                                <option value="">Selecione um evento</option>
                                                @foreach ($taxas as $tx)
                                                    <option value="{{$tx->tipo_evento_redesim}}">{{$tx->tipo_evento_redesim}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <p><strong>por data de Emissão:</strong></p>
                                            <div class="row">
                                                <div class="col-6 form-group">
                                                    <label for="dt_emissao_in">Início:</label>
                                                    <input type="date" name="dt_emissao_in" id="dt_emissao_in" class="form-control">
                                                </div>
                                                <div class="col-6 form-group">
                                                    <label for="dt_emissao_fl">Final:</label>
                                                    <input type="date" name="dt_emissao_fl" id="dt_emissao_fl" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <p><strong>por data do Pagamento:</strong></p>
                                            <div class="row">
                                                <div class="col-6 form-group">
                                                    <label for="dt_paga_in">Início:</label>
                                                    <input type="date" name="dt_paga_in" id="dt_paga_in" class="form-control">
                                                </div>
                                                <div class="col-6 form-group">
                                                    <label for="dt_paga_fl">Final:</label>
                                                    <input type="date" name="dt_paga_fl" id="dt_paga_fl" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 form-group ">
                                            <button type="submit" class="btn btn-primary">Buscar</button>
                                            <button type="reset" class="btn btn-warning">Limpar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
