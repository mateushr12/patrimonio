@extends('layout_admin.principal')
@section ('breadcrumb','Taxas')

@section('conteudo')
    <section>
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-12">
                                        <strong>Detalhe da Taxa</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-4">
                                        <h5>Protocolo</h5>
                                        <p>{{$resultado->protocolo}} </p>
                                    </div>
                                    <div class="col-4">
                                        <h5>CPF do Solicitante</h5>
                                        <p>{{$resultado->cpf_solicitante}} </p>
                                    </div>
                                    <div class="col-4">
                                        <h5>CNPJ do Solicitante</h5>
                                        <p>{{$resultado->cnpj_solicitante}} </p>
                                    </div>
                                </div>
                                &nbsp;
                                <div class="row">
                                    <div class="col-4">
                                        <h5>Nosso Número</h5>
                                        <p>{{$resultado->nosso_numero}} </p>
                                    </div>
                                    <div class="col-4">
                                        <h5>Número do Documento</h5>
                                        <p>{{$resultado->cd_documento}} </p>
                                    </div>
                                    <div class="col-4">
                                        <h5>Data do Protocolo</h5>
                                        <p>
                                          @if (isset($resultado->dt_protocolo))
                                              {{ \Carbon\Carbon::parse($resultado->dt_protocolo)->format('d/m/Y') }}
                                          @else
                                              -
                                          @endif
                                        </p>
                                    </div>
                                </div>
                                &nbsp;
                                <div class="row">
                                    <div class="col-4">
                                        <h5>Data de Emissão</h5>
                                        <p>{{ \Carbon\Carbon::parse($resultado->dt_emissao)->format('d/m/Y') }} </p>
                                    </div>
                                    <div class="col-4">
                                        <h5>Data do Vencimento</h5>
                                        <p>{{ \Carbon\Carbon::parse($resultado->dt_vencimento)->format('d/m/Y') }} </p>
                                    </div>
                                    <div class="col-4">
                                        <h5>Data do Pagamento</h5>
                                        <p>{{ \Carbon\Carbon::parse($resultado->dt_pagamento)->format('d/m/Y') }} </p>
                                    </div>
                                </div>
                                &nbsp;
                                <div class="row">
                                    <div class="col-4">
                                        <h5>Pago</h5>
                                        <p>{{$resultado->pago}} </p>
                                    </div>
                                    <div class="col-4">
                                        <h5>Porte</h5>
                                        <p>{{$resultado->porte}} </p>
                                    </div>
                                    <div class="col-4">
                                        <h5>Natureza</h5>
                                        <p>{{$resultado->natureza_juridica}} </p>
                                    </div>
                                </div>
                                &nbsp;
                                <div class="row">
                                    <div class="col-4">
                                        <h5>CPF/CNPJ Contador</h5>
                                        <p>{{$resultado->cpf_cnpj_contador}} </p>
                                    </div>
                                    <div class="col-4">
                                        <h5>Nome Contador</h5>
                                        <p>{{$resultado->nome_contador}} </p>
                                    </div>
                                    <div class="col-4">
                                        <h5>Valor Recebido</h5>
                                        <p>R$ {{$resultado->vl_recebido}} </p>
                                    </div>
                                </div>
                                &nbsp;
                                <div class="row">
                                    <div class="col-12">
                                        <table class="table">
                                          <tr>
                                            <th>Tipo de Evento</th>
                                            <th>Evento Redesim</th>
                                          </tr>
                                          @foreach ($eventos as $key => $value)
                                          <tr>
                                              <td>{{$value->tipo_evento_redesim}}</td>
                                              <td>{{$value->evento_redesim}}</td>
                                          </tr>
                                          @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        &nbsp;<a href="{{  URL::previous()  }}">Voltar</a>
                    </div>
                </div>
            </div>
    </section>
@endsection
