@extends('layout_admin.principal')
@section ('breadcrumb','Taxas')

@section('conteudo')
  <section>
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">            

              <div class="card-header">
                <div class="row">
                  <div class="col-12">
                    <strong>Cadastrar Taxas Serasa/Boa Vista</strong>
                  </div>
                </div>
              </div>
              <div class="card-body card-block">
                @if (session('sucesso'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{session('sucesso')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif (session('delete'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{session('delete')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif

                <form action="{{route('taxas.serasa')}}" method="post">
                  {{csrf_field()}}
                  <div class="row">
                    {{-- <div class="col-6 form-group">
                      <label for="protocolo"><strong>Protocolo:</strong></label>
                      <input type="text" name="protocolo" id="protocolo"
                      onkeyup="this.value = this.value.toUpperCase();" class="form-control" required>
                    </div> --}}
                    <div class="col-6 form-group">
                      <label for="nosso_numero"><strong>Nº do Documento:</strong></label>
                      <input type="text" name="nosso_numero" id="nosso_numero"
                      onkeyup="this.value = this.value.toUpperCase();" class="form-control" required>
                    </div>
                    <div class="col-6 form-group">
                      <label for="nosso_numero"><strong>Tipo:</strong></label>
                      <select class="form-control" name="tipo" required>
                          <option value="">SELECIONE O TIPO</option>
                          <option value="0000">Serasa</option>
                          <option value="0001">Boa Vista</option>
                      </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-6 form-group">
                      <label for="data_pagamento"><strong>Data de Pagamento:</strong></label>
                      <input type="date" name="data_pagamento" id="dt_pagamento" class="form-control" required>
                    </div>
                    <div class="col-6 form-group">
                      <label for="valor"><strong>Valor:</strong></label>
                      <input type="text" name="valor" onkeyup="k(this);" id="" class="form-control" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12 form-group ">
                      <button type="submit" class="btn btn-primary">Cadastrar</button>
                      <button type="reset" class="btn btn-warning">Limpar</button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="card">
                <div class="card-header">
                  <div class="row">
                    <div class="col-12">
                      <strong>Taxas Serasa/Boa Vista</strong>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <table class="table table-sm">
                    <tr>
                      <th>N° Documento</th>
                      <th>Data</th>
                      <th>Valor</th>
                      <th>Tipo</th>
                      <th></th>
                    </tr>
                    @foreach ($serasas as $row)
                    <tr>
                      <td>{{ $row->nosso_numero }}</td>
                      <td>{{ \Carbon\Carbon::parse($row->data_pagamento)->format('d/m/Y') }}</td>
                      <td>{{ number_format($row->valor,2,',','.') }}</td>
                      @if ($row->tipo == '0000')
                        <td>Serasa</td>
                      @elseif ($row->tipo == '0001')
                        <td>Boa Vista</td>
                      @else
                        <td> - </td>
                      @endif
                      <td>
                        <div class="table-data-feature">
                            <a
                            href="{{ route ('taxas.deletar', $row->id) }}"
                                data-confirm="Tem certeza que deseja excluir o item selecionado?">
                                <button type="button" class="item" title="Deletar Taxa"
                                data-toggle="tooltip" data-placement="top" title=""
                                    data-original-title="Deletar"
                                >
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                            </a>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <script>
    function k(i) {
      var v = i.value.replace(/\D/g,'');
      v = (v/100).toFixed(2) + '';
      v = v.replace(".", ",");
      v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
      v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");
      i.value = v;
    }
    </script>

  @endsection
