<!DOCTYPE html>
<html lang="pt-br">
<head>
  <!-- Required meta tags-->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="JUCESE - Junta Comercial do Estado de Sergipe">

  <!-- Title Page-->
  <title>ERP - JUCESE</title>

  <link href="<?php echo asset('vendor/bootstrap-4.1/bootstrap.min.css')?>" rel="stylesheet" media="all">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"></script>
  <script src="https://unpkg.com/jspdf-autotable@3.5.9/dist/jspdf.plugin.autotable.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

  <style>
    @media print {
      .btn {
        display: none !important;
      }
    }
    #btnPDF{
      background: #2579e6;
      color: white;
      font-weight: bold;
      width: 200px;
    }
    #btnPDF:hover{
      background: #0439f1
    }
  </style>

</head>

<body>
          <div class="container">
          <div class="row" style="border: 1px solid black; min-height: 1440px;">
            <div class="col-12 form-group" style="margin-top: 10px">
              <button type="button" class="btn" id="btnPDF">Gerar PDF</button><br>
            <div class="table-responsive table-responsive-data2">
                <table style="width: 940px">
                  <thead >
                    <tr><td colspan="2">
                      <img src="../../images/gov-jucese-top-relatorio.jpg"  width="940px;"><br>
                    </td></tr>
                    <tr>
                      <td style=" font-size: 24px;" ><strong>Relatório de Taxas</strong><p></p></td>
                      <td style="font-size: 14px; "><strong>Total:</strong> R$ {{ number_format($somaRec,2,',','') }}&nbsp;&nbsp;&nbsp;<strong>Quantidade:</strong> {{ $registros }}</td>
                    </tr>
                  </thead>
                  <tbody>
                  <tr><td colspan="2">

          <table class="table table-sm" border="1" id="tabela">
            <thead>
              <tr>
                <th>Protocolo</th>
                <th>CPF</th>
                <th>CNPJ</th>
                <th>Nosso Nº</th>
                <th>Nº Doc.</th>
                <th>Dt. Emissão</th>
                <th>Dt. Pagamento</th>
                <th>Valor</th>
                <th>Evento</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($busca as $it)
                <tr>
                  <td>{{ $it['protocolo'] }}</td>
                  <td>{{ $it['cpf_solicitante'] }}</td>
                  <td>{{ $it['cnpj_solicitante'] }}</td>
                  <td>{{ $it['nosso_numero'] }}</td>
                  <td>{{ $it['cd_documento'] }}</td>
                  <td>{{ \Carbon\Carbon::parse($it['dt_emissao'])->format('d/m/Y') }}</td>
                  <td>{{ \Carbon\Carbon::parse($it['dt_pagamento'])->format('d/m/Y') }}</td>
                  <td>{{ $it['vl_recebido'] }}</td>
                  <td>{{ $it['tipo_evento_redesim'] }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>

                  </td></tr>
                </tbody>
              </table>

            </div>
            </div>
            </div>
            <footer style="text-align: center; font-weight: bold;">Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020<br>Telefone 79 3234-4100 sítio: www.jucese.se.gov.br</footer>
          </div>
    </body>
    </html>

    <script type="text/javascript">

    function dataAt(){
        var data = new Date(),
            dia  = data.getDate().toString().padStart(2, '0'),
            mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
            ano  = data.getFullYear();
        return dia+"/"+mes+"/"+ano;
    }

    $(document).ready(function(){
      $('#btnPDF').click(function() {
        var soma = "{{ $somaRec }}"
        var qts = "{{ $registros }}"
        var doc = new jsPDF('landscape', 'pt', 'a4')
        var base64Img = "{{ asset('images/gov-jucese-top-relatorio.jpg') }}"
        var img = new Image()
        img.src = base64Img
        var hoje = new Date()
        doc.autoTable({
          html: '#tabela',
          // columnStyles: {
          //      1: { cellWidth: 90 },
          //      3: { cellWidth: 90 }
          // },
          styles: {
            lineColor: '#ccc',
            lineWidth: 1,
            fontSize: 10
          },
          headStyles: { fillColor: "#424647", fontSize: 11 },
          didDrawPage: function (data) {
            /////////////////////// CABEÇALHO ////////////////////////////////////////////////////////
            doc.setFontSize(15)
            doc.setTextColor(40)
            doc.setFontType("bold")
            doc.text('RELATÓRIO DE TAXAS', data.settings.margin.left + 0, 110)
            doc.setFontSize(9)
            doc.text('Quantidade: ' + qts, data.settings.margin.left + 580, 110)
            doc.text('Soma: R$ ' + new Intl.NumberFormat('de-DE').format(soma), data.settings.margin.left + 670, 110)
            if (base64Img) {
              doc.addImage(img, 'JPEG', 40, 15, 760, 78)
            }
            // doc.text('Relatório de Pagamento por Fornecedor', data.settings.margin.left + 0, 125)

            //////////////////////// RODAPE ///////////////////////////////////////////////////////
            var str = 'Pág. ' + doc.internal.getNumberOfPages()
            doc.setFontSize(9)
            var pageSize = doc.internal.pageSize
            var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
            doc.text(str, data.settings.margin.left + 730, pageHeight - 25)
            doc.text('Data: ' + dataAt(), data.settings.margin.left + 0, pageHeight - 25)
            doc.setFontType("bold");
            doc.text('Rua Propriá, 315, Centro Aracaju - Sergipe CEP 49010-020', data.settings.margin.left + 280, pageHeight - 30)
            doc.text('Telefone 79 3234-4100 sítio: www.jucese.se.gov.br', data.settings.margin.left + 295, pageHeight - 20)
          },
          margin: {top: 118}
        });
        doc.save("RelatorioTaxas-"+ dataAt() +".pdf");
      });
    });


         //window.onload = function() { window.print(); }
    </script>
