@extends('layout_admin.principal')
@section ('breadcrumb','Grupos Permissões')

@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-5">
                    <h3>Permissões não cadastradas</h3>
                    <div class="card" >
                        <div class="card-header">Grupo: <strong>{{$nome}}</strong></div>
                        <div class="card-body">
                            <form class="" action="{{route('grupo.permissoes.salvar')}}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        @foreach($permissoes as $key)
                                        <p>
                                            <input type="checkbox" name="permissao_id[]" value="{{$key->id}}">
                                            {{$key->descricao}}
                                            {{-- - ({{$key->nome}}) --}}
                                        </p>
                                        @endforeach
                                        <input type="hidden" name="grupouser_id" value="{{$id}}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Adicionar Permissão</button>
                                </div>
                            </form>
                            <a href="/autorizacao/grupos/">Grupos</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <h3>Permissões cadastradas</h3>
                    <div class="card" style="height: 400px; overflow-y: scroll;" >
                        {{-- <div class="row"> --}}
                        <div class="form-group col-sm-12">
                            <table class="table table-top-campaign">
                                <tbody>
                                    @foreach($permissoesCadastradas as $ch)
                                    <tr>
                                        <td>{{isset($ch->descricao) ? $ch->descricao : '-'}}</td>
                                        <td>{{$ch->nome}}</td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button class="item" data-toggle="tooltip" data-placement="top" title=""
                                                    data-original-title="Deletar"
                                                    onclick="return myFunction({{$ch->id}})">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{-- </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<script>
    function myFunction(i) {
        //var idK = element.id;
        var c = confirm('Tem certeza que deseja deletar?');
        if( c == true){
            var a = "/autorizacao/grupos/permissoes/excluir/"
            var b = i
            window.location.href = a.concat(b)
        }
    }
</script>




@endsection
