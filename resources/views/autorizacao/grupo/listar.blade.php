@extends('layout_admin.principal')
@section ('breadcrumb','Grupos Usuario')

@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                @if (session('sucesso'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{session('sucesso')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif (session('delete'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{session('delete')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
              @elseif (session('error'))
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                      {{session('error')}}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                @endif
                <div class="col-12">
                    <h3>Grupos</h3>
                    {{-- <div class="table-data__tool">
                        <div class="table-data__tool-left"></div>
                        <div class="table-data__tool-right">
                            <a class="au-btn au-btn-icon au-btn--green au-btn--small"
                                href="{{route('permissao.novo')}}">
                    <i class="zmdi zmdi-plus"></i>Nova Permissão</a>
                </div>
            </div> --}}
            <div class="card">
                <table class="table table-borderless table-data3">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($grupos as $key)
                        <tr>
                        <td><a href="/autorizacao/grupos/permissoes/inserir/{{$key->id}}">{{$key->nome}}</a></td>
                            <td>
                                <div class="table-data-feature">
                                    <button class="item" data-toggle="tooltip" data-placement="top" title=""
                                        data-original-title="Deletar" onclick="return myFunction({{$key->id}})">
                                        <i class="zmdi zmdi-delete"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            @if (session('erro'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{session('erro')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <strong>Cadastro de Grupos</strong>
                </div>
                <div class="card-body card-block">
                    <form class="" action="{{route('grupoUsuario.salvar')}}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="grupo">Nome do Grupo:</label>
                                <input type="text" class="form-control" id="grupo" name="nome" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Cadastrar Grupo</button>
                            {{-- <br><br>
                                        <a href="{{route('permissao')}}">Listar Permissões</a> --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</section>

<script>
    function myFunction(i) {

        //var idK = element.id;
        var c = confirm('Tem certeza que deseja deletar?');
        if( c == true){
            var a = "/autorizacao/grupos/excluir/"
            var b = i
            window.location.href = a.concat(b)
        }
    }
</script>




@endsection
