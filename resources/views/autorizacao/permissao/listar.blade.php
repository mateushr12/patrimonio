@extends('layout_admin.principal')
@section ('breadcrumb','Permissao')

@section('conteudo')
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                @if (session('sucesso'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{session('sucesso')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif (session('delete'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{session('delete')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
              @elseif (session('erro'))
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                      {{session('erro')}}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                @endif
                <div class="col-12">
                    <h3>Permissões</h3>
                    {{-- <div class="table-data__tool">
                        <div class="table-data__tool-left"></div>
                        <div class="table-data__tool-right">
                            <a class="au-btn au-btn-icon au-btn--green au-btn--small"
                                href="{{route('permissao.novo')}}">
                                <i class="zmdi zmdi-plus"></i>Nova Permissão</a>
                        </div>
                    </div> --}}
                    <div class="card">
                        <table class="table table-borderless table-data3">
                            <thead>
                                <tr>
                                    <th>URL</th>
                                    <th>Descrição</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($permissao as $key)
                                <tr>
                                    <td>{{$key->nome}}</td>
                                    <td>{{$key->descricao}}</td>
                                    <td>
                                        <div class="table-data-feature">
                                            <button class="item" data-toggle="tooltip" data-placement="top" title=""
                                                data-original-title="Deletar" onclick="return myFunction({{$key->id}})"
                                                >
                                                <i class="zmdi zmdi-delete"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-12">
                        @if (session('erro'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('erro')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <strong>Cadastro de Permissões</strong>
                            </div>
                            <div class="card-body card-block">
                                <form class="" action="{{route('permissao.salvar')}}" method="post">
                                    {{ csrf_field() }}
                                    {{-- <div class="row">
                                      <div class="form-group col-sm-12">
                                        <label for="tipop">Tipo de permissão:</label>
                                        <select class="form-control" id="tppermissao" name="tppermissao" required>
                                            <option value="" selected>Selecione</option>
                                            <option value="1">Rotas</option>
                                            <option value="2">Definir manualmente</option>
                                        </select>
                                      </div>
                                    </div> --}}
                                    <div class="row">
                                        <div class="form-group col-sm-12"
                                        {{-- id="rotas" --}}
                                        >
                                            <label for="Permi">Permissão:</label>
                                            <select class="form-control" id="nome" name="nome" required>
                                                <option value="">SELECIONE A PERMISSÃO (NOME DA ROTA)</option>
                                                @foreach($listPermissao as $key)
                                                <option value="{{ $key[0] }}">
                                                    • URL: {{$key[0]}}
                                                    << >>
                                                    ACTION: {{$key[1]}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {{-- <div class="form-group col-sm-12" id="manual">
                                            <label for="Permi">Inserir Permissão:</label>
                                            <input type="text" class="form-control" id="nome" name="nome" placeholder="Desc" />
                                        </div> --}}
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-12">
                                            <label for="Descricao">Descrição:</label>
                                            <input type="text" class="form-control"
                                            onkeyup="this.value = this.value.toUpperCase();"
                                            id="descricao" name="descricao" placeholder="Descrição"
                                            value="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Cadastrar Permissão</button>
                                        {{-- <br><br>
                                        <a href="{{route('permissao')}}">Listar Permissões</a> --}}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</section>

<script>
    function myFunction(i) {

        //var idK = element.id;
        var c = confirm('Tem certeza que deseja deletar?');
        if( c == true){
            var a = "/autorizacao/permissao/excluir/"
            var b = i
            window.location.href = a.concat(b)
        }
    }
</script>




@endsection
