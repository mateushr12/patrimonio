        -- Qual a diferença entre os ambientes de "homologação" e "produção"?
        -- Ambiente de homologação: é o ambiente utilizado apenas para testes. Todos os documentos enviados a esse ambiente não possuem validade jurídica, podendo ser utilizado dados reais ou fictícios. Deve ser utilizado pelas empresas, a qualquer tempo, quando desejarem fazer testes para promoverem ajustes em seu sistema.
        -- Ambiente de produção: é o ambiente utilizado apenas para emissão de NFC-e com validade jurídica. Somente podem acessar esse ambiente as empresas “autorizadas” a emitir NFC-e.
        -- O ambiente de homologação é idêntico ao ambiente de produção, possuindo as mesmas regras de negócio, diferindo apenas nos endereços dos serviços Web Services e na validade jurídica do documento emitido.

        // Verificações de teste pré-produção do Subsistema Callcenter ( E.R.P. )
                - Conversão e equivalência de dados - ok - encerrado em 02/10/2020
                
                - Verificação de códigos em consonância com a nova plataforma framework Laravel -  ok - encerrado em 02/10/2020
                
                - Testar inclusão, alteração e exclusão das seguintes funções:
                        => Tipo de Problema - ok verificado em 05/10/2020
                        => Tipo de Solução - ok verificado em 05/10/2020
                        => Tipo de Cidadão - ok verificado em 05/10/2020
                        => Cidadão - ok verificado em 06/10/2020
                        => Tipo de Atendimento - ok verificado em 05/10/2020
                
                - Testar inclusão da função chamada - ok verificado em 05/10/2020
                
                - Testar Redirecionamento de chamadas para setores e atendentes - ok verificado em 22/10/2020
                
                - Testar Consultas em tela das funções:                 - IMPRIMIR GRÁFICO EM TODOS OS RELATÓRIOS IMPRESSOS
                        => Chamada Geral - ok verificado em 05/10/2020                  - FILTRO NÃO FUNCIONA, EXCETO DATAS.
                        => Produtividade com suas subfunções                            - EM OPÇÕES DE PESQUISA O GRUPO / NIVEL NÃO LISTA CORRETAMENTE AS OPÇÕES
                                => Por Ano / Mês - ok verificado em 06/10/2020
                                => Por Data / Hora - ok verificado em 07/10/2020
                                => Por Dia - ok verificado em 07/10/2020
                                => Por Hora - ok verificado em 07/10/2020
                                => Por Data / Atendente - ok verificado em 07/10/2020
                                => Por Atendente - ok verificado em 07/10/2020
                                => Por Tipo de Problema - ok verificado em 07/10/2020
                                => Problemas com Ocorrências > 2 - ok verificado em 07/10/2020
                                => Por Tipo de Solução - ok verificado em 07/10/2020
                                => Soluções com Ocorrências > 2 - ok verificado em 07/10/2020
                                => Clientes com Chamadas > 2 - ok verificado em 07/10/2020
                                => Agiliza x C.P.F. - ok verificado em 07/10/2020
                                => Por Tipo de Cidadão - ok verificado em 07/10/2020
                                => C.P.F.s distintos por Tipo de Cidadão - ok verificado em 07/10/2020
                        => Problemas por Tipo de Cidadão - ok verificado em 07/10/2020
                        => Atendimentos por C.P.F. - ok verificado em 07/10/2020
                        => Painel On-line - ok verificado em 22/10/2020
                
                - Testar Impressão das funções:
                        => Consulta de Chamada Geral - ok verificado em 22/10/2020      - IMPRESSÃO NÃO FUNCIONA
                        => Consulta Produtividade com suas subfunções - ok verificado em 22/10/2020
                        => Consulta Problemas por Tipo de Cidadão - ok verificado em 22/10/2020
                        => Consulta Atendimentos por C.P.F. - ok verificado em 22/10/2020
                        
                        => Cadastro de Tipo de Problema - adiado
                        => Cadastro de Tipo de Solução - adiado
                        => Cadastro de Tipo de Cidadão - adiado
                        => Cadastro de Cidadão - adiado
                        => Cadastro de Tipo de Atendimento - adiado
                
                - Teste no ambiente de desenvolvimento - ok verificado em 22/10/220
                - Teste no ambiente de homologação - previsão em 23/10/2020
                - Disponibilidade no ambiente de produção - previsão em 03/11/2020

        // Passos a seguir:
                // Indice
                // 00) Criar os grupos de usuarios em autenticacao.gruposuser
                // 01) Criar os usuário em autenticacao.users
                // 02) Limpar as tabelas de Callcenter Novo
                // 03) Migrar dados de callcenter antigo para callcenter novo
                // 04) Renomear gruposuser_id em chamadas
                // 05) Renomear gruposuser_id_red em chamadas
                // 06) Renomear user_id em chamadas
                // 07) Renomear user_id_red em chamadas
                // 08) Renomear user_id_atendeu_red em chamadas
                // 09) Renomear user_id em cidadaos
                // 10) Renomear user_id em tipo de atendimentos
                // 11) Renomear user_id em tipo de cidadãos
                // 12) Renomear user_id em tipo de problemas
                // 13) Renomear user_id em tipo de soluções
                // 14) Criar registros de permissoes
                // 15) Criar registros de permissao_grupo

        -- Banco de Dados autenticação

                CREATE TABLE `sessions` (
                `id` bigint(20) UNSIGNED NOT NULL,
                `user_id` bigint(20) UNSIGNED NULL,
                `ip_address` varchar(255),
                `user_agent` varchar(255),
                `payload` varchar(255),
                `last_activity` int(11)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

                -- atualizado em 18/10/2020
                        SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
                        SET AUTOCOMMIT = 0;
                        START TRANSACTION;
                        SET time_zone = "+00:00";

                        CREATE DATABASE IF NOT EXISTS `autenticacao` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
                        USE `autenticacao`;

                        CREATE TABLE `gruposuser` (
                        `id` bigint(20) NOT NULL,
                        `nome` varchar(200) NOT NULL
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

                        INSERT INTO `gruposuser` (`id`, `nome`) VALUES
                        (6, 'Financeiro'),
                        (8, 'Gestor'),
                        (7, 'Gestor Financeiro'),
                        (9, 'Secretaria'),
                        (5, 'T.I.');

                        CREATE TABLE `permissao_grupo` (
                        `id` bigint(20) NOT NULL,
                        `grupouser_id` int(11) NOT NULL,
                        `permissoes_id` int(11) NOT NULL
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

                        INSERT INTO `permissao_grupo` (`id`, `grupouser_id`, `permissoes_id`) VALUES
                                (242, 5, 28),
                                (243, 5, 25),
                                (244, 5, 91),
                                (245, 5, 90),
                                (246, 5, 39),
                                (247, 5, 38),
                                (248, 5, 50),
                                (249, 5, 48),
                                (250, 5, 56),
                                (251, 5, 51),
                                (252, 5, 41),
                                (253, 5, 14),
                                (254, 5, 24),
                                (255, 5, 84),
                                (256, 5, 83),
                                (257, 5, 82),
                                (258, 5, 26),
                                (259, 5, 81),
                                (260, 5, 47),
                                (261, 5, 40),
                                (262, 5, 30),
                                (263, 5, 60),
                                (264, 5, 66),
                                (265, 5, 69),
                                (266, 5, 87),
                                (267, 5, 88),
                                (268, 5, 89),
                                (269, 5, 86),
                                (270, 5, 75),
                                (271, 5, 34),
                                (272, 5, 33),
                                (273, 5, 85),
                                (274, 5, 31),
                                (275, 5, 20),
                                (276, 5, 36),
                                (277, 5, 80),
                                (278, 5, 35),
                                (279, 5, 3),
                                (280, 6, 28),
                                (281, 6, 25),
                                (282, 6, 91),
                                (283, 6, 90),
                                (284, 6, 39),
                                (285, 6, 38),
                                (286, 6, 50),
                                (287, 6, 48),
                                (288, 6, 56),
                                (289, 6, 51),
                                (290, 6, 41),
                                (291, 6, 24),
                                (292, 6, 84),
                                (293, 6, 83),
                                (294, 6, 82),
                                (295, 6, 26),
                                (296, 6, 81),
                                (297, 6, 47),
                                (298, 6, 40),
                                (299, 6, 30),
                                (300, 6, 60),
                                (301, 6, 66),
                                (302, 6, 69),
                                (303, 6, 87),
                                (304, 6, 88),
                                (305, 6, 89),
                                (306, 6, 86),
                                (307, 6, 34),
                                (308, 6, 33),
                                (309, 6, 85),
                                (310, 6, 31),
                                (311, 6, 36),
                                (312, 6, 80),
                                (313, 6, 35),
                                (314, 8, 28),
                                (315, 8, 25),
                                (316, 8, 91),
                                (317, 8, 90),
                                (318, 8, 39),
                                (319, 8, 38),
                                (320, 8, 50),
                                (321, 8, 48),
                                (322, 8, 56),
                                (323, 8, 51),
                                (324, 8, 41),
                                (325, 8, 24),
                                (326, 8, 84),
                                (327, 8, 83),
                                (328, 8, 82),
                                (329, 8, 26),
                                (330, 8, 81),
                                (331, 8, 47),
                                (332, 8, 40),
                                (333, 8, 30),
                                (334, 8, 60),
                                (335, 8, 66),
                                (336, 8, 69),
                                (337, 8, 87),
                                (338, 8, 88),
                                (339, 8, 89),
                                (340, 8, 86),
                                (341, 8, 75),
                                (342, 8, 34),
                                (343, 8, 33),
                                (344, 8, 85),
                                (345, 8, 31),
                                (346, 8, 36),
                                (347, 8, 80),
                                (348, 8, 35),
                                (349, 8, 3),
                                (350, 7, 28),
                                (351, 7, 25),
                                (352, 7, 91),
                                (353, 7, 90),
                                (354, 7, 39),
                                (355, 7, 38),
                                (356, 7, 50),
                                (357, 7, 48),
                                (358, 7, 56),
                                (359, 7, 51),
                                (360, 7, 41),
                                (361, 7, 24),
                                (362, 7, 84),
                                (363, 7, 83),
                                (364, 7, 82),
                                (365, 7, 26),
                                (366, 7, 81),
                                (367, 7, 47),
                                (368, 7, 40),
                                (369, 7, 30),
                                (370, 7, 60),
                                (371, 7, 66),
                                (372, 7, 69),
                                (373, 7, 87),
                                (374, 7, 88),
                                (375, 7, 89),
                                (376, 7, 86),
                                (377, 7, 75),
                                (378, 7, 34),
                                (379, 7, 33),
                                (380, 7, 85),
                                (381, 7, 31),
                                (382, 7, 36),
                                (383, 7, 80),
                                (384, 7, 35),
                                (385, 5, 92),
                                (386, 5, 93),
                                (387, 5, 94),
                                (388, 6, 93),
                                (389, 6, 92),
                                (390, 7, 93),
                                (391, 7, 92),
                                (392, 7, 94),
                                (393, 5, 95),
                                (394, 8, 95),
                                (395, 6, 95),
                                (396, 7, 95),
                                (397, 5, 96),
                                (398, 5, 98),
                                (399, 5, 97),
                                (400, 5, 99),
                                (401, 6, 96),
                                (402, 7, 96),
                                (403, 9, 98),
                                (404, 9, 97),
                                (405, 9, 99),
                                (406, 5, 100),
                                (407, 6, 100),
                                (408, 7, 100);

                        CREATE TABLE `permissoes` (
                        `id` int(11) NOT NULL,
                        `nome` varchar(200) NOT NULL,
                        `descricao` varchar(200) DEFAULT NULL
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

                        INSERT INTO `permissoes` (`id`, `nome`, `descricao`) VALUES
                                (3, 'usuarios/listar', 'USUÁRIOS'),
                                (5, 'usuarios/inativarAtivar/{id}', '(NÃO USAR) USUÁRIOS-INATIVAR/ATIVAR '),
                                (10, 'usuarios/cadastrar', '(NÃO USAR) USUÁRIOS-CADASTRAR'),
                                (11, 'usuarios/alterarSenha/{id}', '(NÃO USAR) USUÁRIOS-ALTERAR SENHA'),
                                (12, 'usuarios/editar/{id}', '(NÃO USAR) USUÁRIOS-ACESSAR EDITAR'),
                                (14, 'autorizacao/grupos', 'GRUPOS DE USUÁRIOS'),
                                (15, 'autorizacao/grupos/excluir/{id}', '(NÃO USAR) GRUPOS-EXCLUIR'),
                                (16, 'autorizacao/grupos/salvar', '(NÃO USAR) GRUPOS-CADASTRAR'),
                                (17, 'autorizacao/grupos/permissoes/inserir/{id}', '(NÃO USAR) PERMISSÕES P/ GRUPO-LISTAR'),
                                (18, 'autorizacao/grupos/permissoes/excluir/{id}', '(NÃO USAR) PERMISSÕES P/ GRUPO-EXCLUIR'),
                                (19, 'autorizacao/grupos/permissoes/salvar', '(NÃO USAR) PERMISSÕES P/ GRUPO-SALVAR'),
                                (20, 'autorizacao/permissao', 'PERMISSÕES'),
                                (21, 'autorizacao/permissao/excluir/{id}', '(NÃO USAR) PERMISSÃO-EXCLUIR'),
                                (22, 'autorizacao/permissao/salvar', '(NÃO USAR) PERMISSÃO-SALVAR'),
                                (24, 'erp/acoes', 'MENU - AÇÕES'),
                                (25, 'erp/acoes/deletar/{id}', 'AÇÃO - EXCLUIR'),
                                (26, 'erp/contratos', 'MENU - CONTRATOS'),
                                (27, 'erp/acoes/salvar', '(NÃO USAR) AÇÕES-SALVAR'),
                                (28, 'erp/acoes/editar/{id}', 'AÇÃO - EDITAR'),
                                (30, 'erp/orcamentoinicial', 'MENU - ORÇAMENTO INICIAL'),
                                (31, 'erp/orcamentoinicial/{id}/edit', 'ORÇAMENTO - EDITAR'),
                                (33, 'erp/orcamentosuplementar', 'MENU - SUPLEMENTAR'),
                                (34, 'erp/remanejamento', 'MENU - REMANEJAMENTO'),
                                (35, 'erp/orcamentosuplementar/{id}/edit', 'SUPLEMENTAR - EDITAR'),
                                (36, 'erp/remanejamento/{id}/edit', 'REMANEJAMENTO - EDITAR'),
                                (37, 'erp/contratos/salvar', '(NÃO USAR) CONTRATOS - SALVAR'),
                                (38, 'erp/contratos/editar/{id}', 'CONTRATO - EDITAR'),
                                (39, 'erp/contratos/deletar/{id}', 'CONTRATO - DELETAR'),
                                (40, 'erp/fornecedor', 'MENU - FORNECEDORES'),
                                (41, 'erp/fornecedor/editar/{id}', 'FORNECEDOR - EDITAR'),
                                (42, 'erp/fornecedor/salvar', '(NÃO USAR) FORNECEDOR - SALVAR'),
                                (47, 'erp/empenho', 'MENU - EMPENHO'),
                                (48, 'erp/empenho/editar/{id}', 'EMPENHO - EDITAR'),
                                (49, 'erp/empenho/salvar', '(NÃO USAR) EMPENHO - SALVAR'),
                                (50, 'erp/empenho/deletar/{id}', 'EMPENHO - DELETAR'),
                                (51, 'erp/empenho/reforco/{id}', 'EMPENHO - REFORÇO'),
                                (52, 'erp/empenho/newreforco/{id}', '(NÃO USAR) NOVO REFORÇO'),
                                (56, 'erp/empenho/pagamento/{id}', 'EMPENHO - PAGAMENTO'),
                                (57, 'erp/empenho/pagamento/deletar/{id}', '(NÃO USAR) PAGAMENTO - DELETAR'),
                                (58, 'erp/empenho/retencao/{id}', '(NÃO USAR) RETENÇÃO'),
                                (59, 'erp/empenho/retencao/deletar/{id}', '(NÃO USAR) RETENÇÃO - DELETAR'),
                                (60, 'erp/cadastro/elemento', 'MENU - OUTROS CADASTROS ELEMENTO DE DESPESA'),
                                (61, 'erp/cadastro/elemento/deletar/{id}', '(NÃO USAR) ELEMENTO - DELETAR'),
                                (62, 'erp/cadastro/subelemento/{id}', '(NÃO USAR) SUBELEMENTO - LISTAR'),
                                (63, 'erp/cadastro/elemento/salvar', '(NÃO USAR) ELEMENTO - SALVAR'),
                                (64, 'erp/cadastro/subelemento/salvar', '(NÃO USAR) SUBELEMENTO - SALVAR'),
                                (65, 'erp/cadastro/subelemento/deletar/{id}', '(NÃO USAR) SUBELEMENTO - DELETAR'),
                                (66, 'erp/cadastro/modalidadelic', 'MENU - OUTROS CADASTROS MODALIDADE DA LICITAÇÃO'),
                                (67, 'erp/cadastro/modalidadelic/salvar', '(NÃO USAR) MODALIDADE DA LIC. - SALVAR'),
                                (68, 'erp/cadastro/modalidadelic/deletar/{id}', '(NÃO USAR) MODALIDADE DA LIC. - DELETAR'),
                                (69, 'erp/cadastro/reflegal', 'MENU - OUTROS CADASTROS REFERÊNCIA LEGAL'),
                                (70, 'erp/cadastro/reflegal/salvar', '(NÃO USAR) REFERÊNCIA LEGAL - SALVAR'),
                                (71, 'erp/cadastro/reflegal/deletar/{id}', '(NÃO USAR) REFERÊNCIA LEGAL - DELETAR'),
                                (72, 'erp/cadastro/tpretencao', '(NÃO USAR) TIPO RETENÇÃO - LISTAR'),
                                (73, 'erp/cadastro/tpretencao/salvar', '(NÃO USAR) TIPO RETENÇÃO - SALVAR'),
                                (74, 'erp/cadastro/tpretencao/deletar/{id}', '(NÃO USAR) TIPO RETENÇÃO - DELETAR'),
                                (75, 'erp/taxas', 'MENU - RELATÓRIO TAXAS SIGFACIL'),
                                (76, 'erp/taxas/resultado', '(NÃO USAR) TAXAS - SESSION'),
                                (77, 'erp/taxas/detalhes/{id}', '(NÃO USAR) TAXAS - DETALHES'),
                                (78, 'erp/taxas/sessao', '(NÃO USAR) TAXAS - RESULTADO'),
                                (79, 'erp/taxas/pdf', '(NÃO USAR) TAXAS - IMPRIMIR'),
                                (80, 'erp/orcamentosuplementar/{id}/delete', 'SUPLEMENTAR - DELETAR'),
                                (81, 'erp/empenho/showcotas', 'MENU - COTAS'),
                                (82, 'porgrupo', 'MENU - CONTAS A PAGAR POR GRUPO'),
                                (83, 'porelemento', 'MENU - CONTAS A PAGAR POR ELEMENTO'),
                                (84, 'erp/taxas/serasa', 'MENU - CADASTRAR TAXAS SERASA/BV'),
                                (85, 'erp/taxasRetorno', 'MENU - TAXAS RETORNO BANESE'),
                                (86, 'erp/taxasRetorno/exibir', 'MENU - RELATÓRIO TAXAS BANESE'),
                                (87, 'erp/relatorio/acao', 'MENU - RELATÓRIO AÇÕES'),
                                (88, 'erp/relatorio/contrato', 'MENU - RELATÓRIO CONTRATOS'),
                                (89, 'erp/relatorio/pagamento', 'MENU - RELATÓRIO PAGAMENTOS'),
                                (90, 'erp/contratos/anexar/{id}', 'CONTRATO - ANEXAR'),
                                (91, 'erp/contratos/aditivo/{id}', 'CONTRATO - ADITIVO'),
                                (92, 'erp/taxaRetorno/buscarpordia', 'MENU - TAXAS RETORNO BANESE POR DIA'),
                                (93, 'erp/suprimentos', 'MENU - SUPRIMENTOS DE FUNDOS'),
                                (94, 'erp/suprimentos/deletar/{id}', 'SUPRIMENTOS - DELETAR'),
                                (95, 'erp/graficos/tipoevento', 'GRAFICOS'),
                                (96, 'erp/cadastro/listCorrigir', 'CORRIGIR PAGAMENTOS'),
                                (97, 'erp/caixas', 'NOVAS CAIXAS'),
                                (98, 'erp/caixas/buscar', 'BUSCAR CONTEÚDO'),
                                (99, 'erp/caixas/tipo', 'SETORES'),
                                (100, 'erp/taxas/serasaBoleto', 'BOLETOS SERASA/BV');


                        CREATE TABLE `users` (
                        `id` bigint(20) UNSIGNED NOT NULL,
                        `name` varchar(255) NOT NULL,
                        `email` varchar(255) NOT NULL,
                        `email_verified_at` timestamp NULL DEFAULT NULL,
                        `password` varchar(255) NOT NULL,
                        `remember_token` varchar(100) DEFAULT NULL,
                        `created_at` timestamp NULL DEFAULT NULL,
                        `updated_at` timestamp NULL DEFAULT NULL,
                        `grupoid` int(11) NOT NULL,
                        `ativo` int(11) NOT NULL DEFAULT '1'
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

                        INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `grupoid`, `ativo`) VALUES
                        (1, 'Andrius Prado Silva', 'andrius.prado@jucese.se.gov.br', NULL, '$2y$10$FUon3QNk7S.Ia3yQC2n7y.KbNP8xhWumQrGFpOsDMVEg.jp.RoA1m', NULL, '2019-08-19 15:36:57', '2020-07-23 18:26:00', 5, 1),
                        (2, 'Mateus Henrique', 'mateus.henrique@jucese.se.gov.br', NULL, '$2y$10$fZWSYCG6iH6mN.Bcnws2buE082q5yYVVEGMW/Au49KpQS0p6E7one', NULL, '2019-10-18 18:29:17', '2020-07-23 18:25:50', 5, 1),
                        (3, 'Rodrigo Guedes', 'rodrigo.guedes@jucese.se.gov.br', NULL, '$2y$10$rAgA4ObjmfOE0l5sa5zfqeREKStMYMXq1CsHczAmorWbWuRGCXIkm', NULL, '2019-12-03 17:45:17', '2020-07-23 18:30:03', 5, 0),
                        (5, 'Thiara Priscilla', 'thiara.priscilla@jucese.se.gov.br', NULL, '$2y$10$Bgu5.TRp/RCAhUkgTihaPeY.2xbbv58RT/XgP3bjmaXXLwpTbMbcq', NULL, '2020-01-21 13:08:03', '2020-07-23 18:27:43', 7, 1),
                        (6, 'Eduardo Garcez', 'eduardo.garcez@jucese.se.gov.br', NULL, '$2y$10$1tbs5Ou7NHvtz1A/mbydweb3w2FANtPNJUXwF1c3WilzKKuH1KHfW', NULL, '2020-02-03 13:52:10', '2020-07-23 18:26:11', 5, 1),
                        (7, 'Giovanna Pereira', 'giovanna.pereira@jucese.se.gov.br', NULL, '$2y$10$gipAjtBRKpm5tPJ9ExJHMeIJRKv2ef1YuG2Oy9Gob.P19M1gFZESG', NULL, '2020-02-06 15:54:26', '2020-09-18 13:57:26', 6, 1),
                        (8, 'Hermes Correia', 'hermes.correia@jucese.se.gov.br', NULL, '$2y$10$kFYVKKDO4hukQ/QUt4M/8OZ8FE911AYetQYUPeSHsh1zMnkyimMVO', NULL, '2020-02-06 15:57:26', '2020-07-23 18:28:16', 6, 1),
                        (9, 'Patricia Alves', 'patriciaalves.santos@jucese.se.gov.br', NULL, '$2y$10$zmUBcH97ZvSsfHo0mlyYWe7zJ7sNatP/kjPgzmMXaPpRjKunPpGXC', NULL, '2020-02-06 16:03:26', '2020-07-23 18:28:25', 6, 1),
                        (10, 'Gilvaneide Santos', 'gilvaneide.santos@jucese.se.gov.br', NULL, '$2y$10$f8JALgEzDAPXy7dCGWKFQua8bd8xZCkb06jN6wMsXv7RQjF3Eq2Yi', NULL, '2020-02-06 16:08:51', '2020-07-23 18:28:33', 6, 1),
                        (11, 'Jeane Sandes', 'jeanne.sandes@jucese.se.gov.br', NULL, '$2y$10$nOOY3mtnJIPCqtz64ulitO/0CxvWTo4vZJ/QKBRUEMzDxQeTIh2Zq', NULL, '2020-02-06 16:13:30', '2020-07-23 18:28:43', 6, 1),
                        (12, 'Marco Freitas', 'marco.freitas@jucese.se.gov.br', NULL, '$2y$10$GaHGXuEJnnnYaUB9YtjD9O9YVoOUu4IsTCZRoB8Gp6/FhK6Cods1y', NULL, '2020-03-02 18:42:49', '2020-07-23 18:28:53', 8, 1),
                        (14, 'Teste', 'teste@teste.com', NULL, '$2y$10$owzSoVYMfTUSjYzwRCzToetkvSFzX8zkFPzhSij0EQ7DyGGoxBCaG', NULL, '2020-08-04 13:52:34', '2020-08-04 13:52:34', 5, 1),
                        (15, 'Lorrany Lima', 'lorrany.vilanova@jucese.se.gov.br', NULL, '$2y$10$9m1INUi3iVqe4eR9GEcwd.yghERnoTk3js1tB4XmTtZe1DYEuRaBW', NULL, '2020-08-20 17:37:40', '2020-08-20 17:37:40', 6, 1),
                        (16, 'teste secretaria', 'secretaria@ig.com.br', NULL, '$2y$10$yDpPyuG68lmucJ7jWGYYMOAPzgqHX0rvIXEaVbb0QYFGeTIFukyPa', NULL, '2020-09-15 18:36:11', '2020-09-15 18:36:11', 9, 1);

                        -- Índices para tabelas despejadas

                        ALTER TABLE `gruposuser`
                        ADD PRIMARY KEY (`id`),
                        ADD UNIQUE KEY `nome` (`nome`);

                        ALTER TABLE `permissao_grupo`
                        ADD PRIMARY KEY (`id`);

                        ALTER TABLE `permissoes`
                        ADD PRIMARY KEY (`id`),
                        ADD UNIQUE KEY `nome` (`nome`);

                        ALTER TABLE `users`
                        ADD PRIMARY KEY (`id`),
                        ADD UNIQUE KEY `email` (`email`);

                        -- AUTO_INCREMENT de tabelas despejadas
                        
                        ALTER TABLE `gruposuser`
                        MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

                        ALTER TABLE `permissao_grupo`
                        MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=409;

                        ALTER TABLE `permissoes`
                        MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

                        ALTER TABLE `users`
                        MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
                        COMMIT;

                -- modificar tabela gruposuser acrescentando a coluna id_rec_red
                        USE `autenticacao`;
                        ALTER TABLE `gruposuser` ADD COLUMN `id_rec_red` boolean NULL default FALSE AFTER `nome`;

                

        -- 00) Criar os grupos de usuarios em autenticacao.gruposuser - ok
                USE `autenticacao`;
                REPLACE INTO autenticacao.gruposuser (id, nome, id_rec_red) VALUES
                (4, 'Call Center - Atendimento', 1),
                (41, 'Call Center - Gestão', 1),
                (50, 'Assessoria de Comunicação', 0),
                (60, 'Call Center - Atendimento Presencial', 1),
                (10, 'Analista / Programador de TI', 0),
                (11, 'Call Center - Atendimento Técnico', 1);
                -- (90, 'Coordenação de TI', 0),
                -- Grupos 9 e 10 = 5; 7 = 8; 11 = 11; 5 = 50; 6 = 60; 
                
        -- 01) Criar os usuário em autenticacao.users - ok
                USE `autenticacao`;
                REPLACE INTO autenticacao.users(id, name, email, password, ativo, created_at, updated_at, grupoid) VALUES
                -- (60,  'Ricardo S. Lima', 'rsljunior@infonet.com.br', '$2y$10$FxouhUd.FRvhmsw2zCaos.dppbp3ze/ozOrZxdNXVkfWbGts7Vd3e', 1, '2019-02-28 11:16:17', '2019-02-28 11:16:17', 5),
                -- (61,  'Ricardo Gmail', 'sodrelima@gmail.com', '$2y$10$FxouhUd.FRvhmsw2zCaos.dppbp3ze/ozOrZxdNXVkfWbGts7Vd3e', 1, '2019-02-28 11:16:17', '2019-02-28 11:16:17', 10),
                (110, 'Carlos Francisco', 'carlosfrancisco.neto@jucese.se.gov.br', '09aab0c9991ff75fd096bedea4a861ab', 2, '2019-04-05 09:58:44', '2020-02-13 12:21:48', 40),
                (111, 'Jane Eurides', 'jane.santos@jucese.se.gov.br', 'ef60fc8e2d3602e1d01c75324dba5e8d', 1, '2019-04-05 09:59:37', '2020-06-15 11:20:27', 40),
                -- (112, 'Tatianne Santos Melo', 'tatianne.melo@jucese.se.gov.br', 'e10adc3949ba59abbe56e057f20f883e', 2, '2019-04-05 09:59:37', '2020-02-17 09:29:17', 50),
                (113, 'Higor Feliphe Santana Nunes', 'higor.nunes@jucese.se.gov.br', 'e7698405bff04018d090d4b948a8d3e5', 1, '2019-05-27 09:07:06', '2019-08-20 07:29:01', 40),
                -- (115, 'Paulo AragÃ£o', 'paulo.aragao@jucese.se.gov.br', 'ab45b991e8c7ce88653fb1bee8fb0c2c', 1, '2019-06-13 08:46:56', '2020-05-21 13:36:44', 41),
                (19, 'Adryele Santos de Santana', 'adryele.santos@jucese.se.gov.br', 'e10adc3949ba59abbe56e057f20f883e', 1, '2019-08-20 07:33:34', '2020-06-30 08:27:23', 40),
                (21, 'Joana ConceiÃ§Ã£o', 'joana.conceicao@jucese.se.gov.br', '725820cb1f8659b920ed34203afbc160', 1, '2019-11-06 11:44:39', '2020-06-30 08:26:43', 40),
                (22, 'Luana Dias', 'luana.dias@jucese.se.gov.br', 'e2fcaa4e6a47f9a2bb1893d592b4ab1b', 1, '2019-11-06 11:47:34', '2020-02-19 08:31:45', 40),
                (23, 'Luana Ribeiro', 'luana.ribeiro@jucese.se.gov.br', '5f74f428fb76b01263d13fb4ed300920', 2, '2019-11-06 12:04:17', '2020-02-04 08:26:29', 40),
                (24, 'Regina Celly', 'regina.celly@jucese.se.gov.br', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-02-04 08:25:36', '2020-02-04 08:25:36', 40),
                (25, 'Patricia Enedina', 'patricia.amado@jucese.se.gov.br', '54d134c54ffdf56c2206f2edef85242f', 1, '2020-02-17 10:01:40', '2020-06-30 08:26:58', 40),
                (26, 'Silvia Margarida Farias', 'silvia.farias@jucese.se.gov.br', 'a9684711b154ab1d06c566e125d6b5fb', 1, '2020-02-17 10:08:30', '2020-02-17 10:08:30', 40),
                (27, 'Diego Rios Satiro de Moraes ', 'diego.rios@jucese.se.gov.br', '03141b63498738d3dfd86991f66855b1', 1, '2020-06-30 09:06:06', '2020-06-30 09:09:15', 50),
                (28, 'Emily Reis', 'emily.reis@jucese.se.gov.br', '688cd1500d40c7b4e89675852f23ac13', 1, '2020-08-18 10:18:05', '2020-08-18 10:19:57', 40);
                -- (600, 'Ricardo Gmail', 'sodrelima@gmail.com', '$2y$10$FxouhUd.FRvhmsw2zCaos.dppbp3ze/ozOrZxdNXVkfWbGts7Vd3e', 1, '2019-02-28 11:16:17', '2019-02-28 11:16:17', 40),
                -- (116, 'Cristina Maria Andrade de Melo', 'cristina.melo@jucese.se.gov.br', 'ccc3f259d642341939ec12db5c16de6c', 1, '2019-08-19 12:18:17', '2019-08-19 12:18:17', 7),
                
                UPDATE autenticacao.users SET password='$2y$10$FxouhUd.FRvhmsw2zCaos.dppbp3ze/ozOrZxdNXVkfWbGts7Vd3e' WHERE id=60;



        -- 02) Limpar as tabelas de Callcenter Novo - ok
                CREATE DATABASE IF NOT EXISTS jucese_callcenter DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
                USE jucese_callcenter;

                -- Criar tabelas
                        CREATE TABLE `chamadas` (
                        `id` bigint(20) UNSIGNED NOT NULL,
                        `protocolo` varchar(20) COLLATE utf8mb4_0900_ai_ci NOT NULL,
                        `data_hora_inicio` datetime NOT NULL,
                        `data_hora_fim` datetime NOT NULL,
                        `tipoproblema_id` bigint(20) UNSIGNED DEFAULT NULL,
                        `complemento_problema` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                        `tiposolucao_id` bigint(20) UNSIGNED DEFAULT NULL,
                        `complemento_solucao` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                        `id_agiliza` varchar(13) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                        `id_chamada_ref_red` varchar(20) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                        `gruposuser_id_red` bigint(20) UNSIGNED DEFAULT NULL,
                        `user_id_red` bigint(20) UNSIGNED DEFAULT NULL,
                        `user_id_atendeu_red` bigint(20) UNSIGNED DEFAULT NULL,
                        `data_hora_red_finalizado` datetime DEFAULT NULL,
                        `tipoatendimento_id` bigint(20) UNSIGNED DEFAULT NULL,
                        `cidadao_id` bigint(20) UNSIGNED DEFAULT NULL,
                        `tipocidadao_id` bigint(20) UNSIGNED NOT NULL,
                        `user_id` bigint(20) UNSIGNED NOT NULL,
                        `gruposuser_id` bigint(20) UNSIGNED DEFAULT NULL,
                        `created_at` timestamp NULL DEFAULT NULL,
                        `updated_at` timestamp NULL DEFAULT NULL
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

                        CREATE TABLE `cidadaos` (
                        `id` bigint(20) UNSIGNED NOT NULL,
                        `cpf` varchar(11) COLLATE utf8mb4_0900_ai_ci NOT NULL,
                        `nome` varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
                        `fone_fixo` varchar(14) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                        `fone_celular` varchar(15) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                        `fone_celular2` varchar(15) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                        `email` varchar(255) COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                        `receber_informativo` tinyint(1) NULL DEFAULT 0,
                        `created_at` timestamp NULL DEFAULT NULL,
                        `updated_at` timestamp NULL DEFAULT NULL,
                        `tipocidadao_id` bigint(20) UNSIGNED NOT NULL,
                        `user_id` bigint(20) UNSIGNED NOT NULL
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

                        CREATE TABLE `tipoatendimentos` (
                        `id` bigint(20) UNSIGNED NOT NULL,
                        `descricao` varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
                        `created_at` timestamp NULL DEFAULT NULL,
                        `updated_at` timestamp NULL DEFAULT NULL,
                        `user_id` bigint(20) UNSIGNED NOT NULL
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

                        CREATE TABLE `tipocidadaos` (
                        `id` bigint(20) UNSIGNED NOT NULL,
                        `descricao` varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
                        `created_at` timestamp NULL DEFAULT NULL,
                        `updated_at` timestamp NULL DEFAULT NULL,
                        `user_id` bigint(20) UNSIGNED NOT NULL
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

                        CREATE TABLE `tipoproblemas` (
                        `id` bigint(20) UNSIGNED NOT NULL,
                        `descricao` varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
                        `created_at` timestamp NULL DEFAULT NULL,
                        `updated_at` timestamp NULL DEFAULT NULL,
                        `user_id` bigint(20) UNSIGNED NOT NULL
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

                        CREATE TABLE `tiposolucaos` (
                        `id` bigint(20) UNSIGNED NOT NULL,
                        `descricao` varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL,
                        `created_at` timestamp NULL DEFAULT NULL,
                        `updated_at` timestamp NULL DEFAULT NULL,
                        `user_id` bigint(20) UNSIGNED NOT NULL
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

                        -- =================================================================================

                        ALTER TABLE `chamadas`
                        ADD PRIMARY KEY (`id`),
                        ADD UNIQUE KEY `chamadas_protocolo_unique` (`protocolo`),
                        ADD KEY `chamadas_tipoproblema_id_foreign` (`tipoproblema_id`),
                        ADD KEY `chamadas_tiposolucao_id_foreign` (`tiposolucao_id`),
                        ADD KEY `chamadas_tipoatendimento_id_foreign` (`tipoatendimento_id`),
                        ADD KEY `chamadas_tipocidadao_id_foreign` (`tipocidadao_id`),
                        ADD KEY `chamadas_cidadao_id_foreign` (`cidadao_id`);

                        ALTER TABLE `cidadaos`
                        ADD PRIMARY KEY (`id`),
                        ADD UNIQUE KEY `cidadaos_cpf_unique` (`cpf`),
                        ADD KEY `cidadaos_tipocidadao_id_foreign` (`tipocidadao_id`);

                        ALTER TABLE `tipoatendimentos`
                        ADD PRIMARY KEY (`id`),
                        ADD UNIQUE KEY `tipoatendimentos_descricao_unique` (`descricao`);

                        ALTER TABLE `tipocidadaos`
                        ADD PRIMARY KEY (`id`),
                        ADD UNIQUE KEY `tipocidadaos_descricao_unique` (`descricao`);

                        ALTER TABLE `tipoproblemas`
                        ADD PRIMARY KEY (`id`),
                        ADD UNIQUE KEY `tipoproblemas_descricao_unique` (`descricao`);

                        ALTER TABLE `tiposolucaos`
                        ADD PRIMARY KEY (`id`),
                        ADD UNIQUE KEY `tiposolucaos_descricao_unique` (`descricao`);

                        ALTER TABLE `chamadas`
                        MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

                        ALTER TABLE `cidadaos`
                        MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

                        ALTER TABLE `tipoatendimentos`
                        MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

                        ALTER TABLE `tipocidadaos`
                        MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

                        ALTER TABLE `tipoproblemas`
                        MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

                        ALTER TABLE `tiposolucaos`
                        MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

                        ALTER TABLE `chamadas`
                        ADD CONSTRAINT `chamadas_cidadao_id_foreign` FOREIGN KEY (`cidadao_id`) REFERENCES `cidadaos` (`id`),
                        ADD CONSTRAINT `chamadas_tipocidadao_id_foreign` FOREIGN KEY (`tipocidadao_id`) REFERENCES `tipocidadaos` (`id`),
                        ADD CONSTRAINT `chamadas_tipoatendimento_id_foreign` FOREIGN KEY (`tipoatendimento_id`) REFERENCES `tipoatendimentos` (`id`),
                        ADD CONSTRAINT `chamadas_tipoproblema_id_foreign` FOREIGN KEY (`tipoproblema_id`) REFERENCES `tipoproblemas` (`id`),
                        ADD CONSTRAINT `chamadas_tiposolucao_id_foreign` FOREIGN KEY (`tiposolucao_id`) REFERENCES `tiposolucaos` (`id`);

                        ALTER TABLE `cidadaos`
                        ADD CONSTRAINT `cidadaos_tipocidadao_id_foreign` FOREIGN KEY (`tipocidadao_id`) REFERENCES `tipocidadaos` (`id`);



        -- 03) Migrar dados de callcenter antigo para callcenter novo - ok
                        -- Tipo de Atendimento: Migrar jucese_callcenter.tipoatendimento para jucese_callcenter.tipoatendimentos: 
                                -- Tipo de Problema: Migrar jucese_callcenter.tipoproblema para jucese_callcenter.tipoproblemas: 
                                -- Tipo de Solução: Migrar jucese_callcenter.tiposolucao para jucese_callcenter.tiposolucaos: 
                                -- Tipo de Cidadão: Migrar jucese_callcenter.tipocidadao para jucese_callcenter.tipocidadaos: 
                                -- Cidadão: Migrar jucese_callcenter.clientes para jucese_callcenter.cidadaos: 
                                -- Chamada: Migrar jucese_callcenter.chamada para jucese_callcenter.chamadas: 

                -- Limpar tabelas

                        USE jucese_callcenter;
                        DELETE FROM jucese_callcenter.chamadas;
                        DELETE FROM jucese_callcenter.cidadaos;
                        DELETE FROM jucese_callcenter.tipoatendimentos;
                        DELETE FROM jucese_callcenter.tipocidadaos;
                        DELETE FROM jucese_callcenter.tipoproblemas;
                        DELETE FROM jucese_callcenter.tiposolucaos;

                        ALTER TABLE jucese_callcenter.chamadas AUTO_INCREMENT = 1;
                        ALTER TABLE jucese_callcenter.cidadaos AUTO_INCREMENT = 1;
                        ALTER TABLE jucese_callcenter.tipoatendimentos AUTO_INCREMENT = 1;
                        ALTER TABLE jucese_callcenter.tipocidadaos AUTO_INCREMENT = 1;
                        ALTER TABLE jucese_callcenter.tipoproblemas AUTO_INCREMENT = 1;
                        ALTER TABLE jucese_callcenter.tiposolucaos AUTO_INCREMENT = 1;

                        USE jucese_callcenter;
                        DELETE FROM jucese_callcenter.palavras;
                        ALTER TABLE jucese_callcenter.palavras AUTO_INCREMENT = 1;


                USE `jucese_callcenter`;
                INSERT INTO jucese_callcenter.tipoatendimentos( id, descricao, user_id, created_at, updated_at ) SELECT id_tipoatendimento, descricao_tipoatendimento, id_usuario, data_criacao, data_modificacao  FROM callcenter.tipoatendimento;
                INSERT INTO jucese_callcenter.tipoproblemas( id, descricao, user_id, created_at, updated_at ) SELECT id_tipoproblema, descricao_tipoproblema, id_usuario, data_criacao, data_modificacao  FROM callcenter.tipoproblema;
                INSERT INTO jucese_callcenter.tiposolucaos( id, descricao, user_id, created_at, updated_at ) SELECT id_tiposolucao, descricao_tiposolucao, id_usuario, data_criacao, data_modificacao  FROM callcenter.tiposolucao;
                INSERT INTO jucese_callcenter.tipocidadaos( id, descricao, user_id, created_at, updated_at ) SELECT id_tipocidadao, descricao_tipocidadao, id_usuario, data_criacao, data_modificacao  FROM callcenter.tipocidadao;
                INSERT INTO jucese_callcenter.cidadaos( nome, email, cpf, fone_fixo, fone_celular, fone_celular2, tipocidadao_id, receber_informativo, user_id, created_at, updated_at ) 
                SELECT nome_cliente, email_cliente, trim(cpf_cliente), telefone_cliente, celular_cliente_01, celular_cliente_02, id_tipocidadao, informativo_cliente, 60, '2020-01-01 12:00:00', '2020-01-01 12:00:00' 
                FROM callcenter.clientes GROUP BY trim(cpf_cliente) HAVING count(*) <= 2;

                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '201904' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '201905' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '201906' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '201907' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '201908' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '201909' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '201910' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '201911' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '201912' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '202001' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '202002' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '202003' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '202004' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '202005' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '202006' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '202007' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '202008' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '202009' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '202010' AND id_tipoproblema > 0;
                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '202011' AND id_tipoproblema > 0;

                INSERT INTO jucese_callcenter.chamadas ( protocolo, data_hora_inicio, data_hora_fim, complemento_problema, complemento_solucao, id_agiliza, data_hora_red_finalizado, gruposuser_id_red, user_id_atendeu_red, tipoproblema_id, tiposolucao_id, tipoatendimento_id, cidadao_id, tipocidadao_id, user_id, gruposuser_id, created_at, updated_at ) SELECT id_chamada, data_hora_inicio, data_hora_fim, descricao_problema, solucao_problema, id_agiliza, data_hora_redfin, id_nivel, id_usuario_atendeu_red, id_tipoproblema, id_tiposolucao, id_tipoatendimento, ( select id from jucese_callcenter.cidadaos where chamada1.cpf_cidadao = jucese_callcenter.cidadaos.cpf limit 1 ), id_tipocidadao, id_usuario, ( select nivel2.id_nivel from callcenter.chamada chamada2 left join callcenter.usuario usuario2 ON chamada2.id_usuario = usuario2.id_usuario left join callcenter.nivel nivel2 ON usuario2.id_nivel = nivel2.id_nivel where chamada1.id_usuario = usuario2.id_usuario limit 1 ), data_hora_inicio, data_hora_inicio FROM callcenter.chamada as chamada1 WHERE LEFT(id_chamada,6) = '202012' AND id_tipoproblema > 0;

                USE `jucese_callcenter`;
                UPDATE jucese_callcenter.chamadas SET gruposuser_id=41 WHERE gruposuser_id=1;
                UPDATE jucese_callcenter.chamadas SET gruposuser_id=60 WHERE gruposuser_id=6;

                USE `jucese_callcenter`;
                UPDATE jucese_callcenter.chamadas SET gruposuser_id_red=41 WHERE gruposuser_id_red=1;
                UPDATE jucese_callcenter.chamadas SET gruposuser_id_red=60 WHERE gruposuser_id_red=6;

                USE `jucese_callcenter`;
                UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=7   WHERE user_id_atendeu_red=17;
                UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=4   WHERE user_id_atendeu_red=15;
                UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=12  WHERE user_id_atendeu_red=14;
                UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=110 WHERE user_id_atendeu_red=10;
                UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=111 WHERE user_id_atendeu_red=11;
                UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=113 WHERE user_id_atendeu_red=13;

                USE `jucese_callcenter`;
                UPDATE jucese_callcenter.chamadas SET user_id=7   WHERE user_id=17;
                UPDATE jucese_callcenter.chamadas SET user_id=4   WHERE user_id=15;
                UPDATE jucese_callcenter.chamadas SET user_id=12  WHERE user_id=14;
                UPDATE jucese_callcenter.chamadas SET user_id=110 WHERE user_id=10;
                UPDATE jucese_callcenter.chamadas SET user_id=111 WHERE user_id=11;
                UPDATE jucese_callcenter.chamadas SET user_id=113 WHERE user_id=13;

        -- 04) Renomear gruposuser_id em chamadas - ok - corrigido
                USE `jucese_callcenter`;
                UPDATE jucese_callcenter.chamadas SET gruposuser_id=41 WHERE gruposuser_id=1;
                UPDATE jucese_callcenter.chamadas SET gruposuser_id=60 WHERE gruposuser_id=6;
                -- UPDATE jucese_callcenter.chamadas SET gruposuser_id=60 WHERE gruposuser_id=11;
                -- UPDATE jucese_callcenter.chamadas SET gruposuser_id=5  WHERE gruposuser_id=9;
                -- UPDATE jucese_callcenter.chamadas SET gruposuser_id=5  WHERE gruposuser_id=10;
                
        -- 05) Renomear gruposuser_id_red em chamadas - ok - corrigido
                USE `jucese_callcenter`;
                UPDATE jucese_callcenter.chamadas SET gruposuser_id_red=41 WHERE gruposuser_id_red=1;
                UPDATE jucese_callcenter.chamadas SET gruposuser_id_red=60 WHERE gruposuser_id_red=6;
                -- UPDATE jucese_callcenter.chamadas SET gruposuser_id_red=60 WHERE gruposuser_id_red=6;
                -- UPDATE jucese_callcenter.chamadas SET gruposuser_id_red=5  WHERE gruposuser_id_red=9;
                -- UPDATE jucese_callcenter.chamadas SET gruposuser_id_red=5  WHERE gruposuser_id_red=10;
                
        -- 06) Renomear user_id em chamadas - ok - corrigido
                USE `jucese_callcenter`;
                UPDATE jucese_callcenter.chamadas SET user_id=7   WHERE user_id=17;
                UPDATE jucese_callcenter.chamadas SET user_id=4   WHERE user_id=15;
                UPDATE jucese_callcenter.chamadas SET user_id=12  WHERE user_id=14;
                UPDATE jucese_callcenter.chamadas SET user_id=110 WHERE user_id=10;
                UPDATE jucese_callcenter.chamadas SET user_id=111 WHERE user_id=11;
                UPDATE jucese_callcenter.chamadas SET user_id=113 WHERE user_id=13;
                -- UPDATE jucese_callcenter.chamadas SET user_id=7   WHERE user_id=17;
                -- UPDATE jucese_callcenter.chamadas SET user_id=116 WHERE user_id=16;
                        -- UPDATE jucese_callcenter.chamadas SET user_id=19 WHERE user_id=19;
                        -- UPDATE jucese_callcenter.chamadas SET user_id=21 WHERE user_id=21;
                        -- UPDATE jucese_callcenter.chamadas SET user_id=22 WHERE user_id=22;
                        -- UPDATE jucese_callcenter.chamadas SET user_id=23 WHERE user_id=23;
                        -- UPDATE jucese_callcenter.chamadas SET user_id=24 WHERE user_id=24;
                        -- UPDATE jucese_callcenter.chamadas SET user_id=25 WHERE user_id=25;
                        -- UPDATE jucese_callcenter.chamadas SET user_id=26 WHERE user_id=26;
                        -- UPDATE jucese_callcenter.chamadas SET user_id=27 WHERE user_id=27;
                        -- UPDATE jucese_callcenter.chamadas SET user_id=28 WHERE user_id=28;
                        -- UPDATE jucese_callcenter.chamadas SET user_id=7   WHERE user_id=17;

        -- 07) Renomear user_id_red em chamadas - ok
                USE `jucese_callcenter`;
                -- UPDATE jucese_callcenter.chamadas SET user_id_red=60  WHERE user_id_red=6;
                -- UPDATE jucese_callcenter.chamadas SET user_id_red=100 WHERE user_id_red=10;
                -- UPDATE jucese_callcenter.chamadas SET user_id_red=111 WHERE user_id_red=11;
                -- UPDATE jucese_callcenter.chamadas SET user_id_red=112 WHERE user_id_red=12;
                -- UPDATE jucese_callcenter.chamadas SET user_id_red=113 WHERE user_id_red=13;
                -- UPDATE jucese_callcenter.chamadas SET user_id_red=115 WHERE user_id_red=15;
                -- UPDATE jucese_callcenter.chamadas SET user_id_red=7   WHERE user_id_red=17;
                -- UPDATE jucese_callcenter.chamadas SET user_id_red=116 WHERE user_id_red=16;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_red=119 WHERE user_id_red=19;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_red=121 WHERE user_id_red=21;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_red=122 WHERE user_id_red=22;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_red=123 WHERE user_id_red=23;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_red=124 WHERE user_id_red=24;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_red=125 WHERE user_id_red=25;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_red=126 WHERE user_id_red=26;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_red=127 WHERE user_id_red=27;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_red=128 WHERE user_id=28;
                
        -- 08) Renomear user_id_atendeu_red em chamadas - ok - corrigido
                USE `jucese_callcenter`;
                UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=7   WHERE user_id_atendeu_red=17;
                UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=4   WHERE user_id_atendeu_red=15;
                UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=12  WHERE user_id_atendeu_red=14;
                UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=110 WHERE user_id_atendeu_red=10;
                UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=111 WHERE user_id_atendeu_red=11;
                UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=113 WHERE user_id_atendeu_red=13;
                -- UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=7   WHERE user_id_atendeu_red=17;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=116 WHERE user_id_atendeu_red=16;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=119 WHERE user_id_atendeu_red=19;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=121 WHERE user_id_atendeu_red=21;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=122 WHERE user_id_atendeu_red=22;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=123 WHERE user_id_atendeu_red=23;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=124 WHERE user_id_atendeu_red=24;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=125 WHERE user_id_atendeu_red=25;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=126 WHERE user_id_atendeu_red=26;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=127 WHERE user_id_atendeu_red=27;
                        -- UPDATE jucese_callcenter.chamadas SET user_id_atendeu_red=128 WHERE user_id=28;
                
        -- 09) Renomear user_id em cidadãos - ok
                USE `jucese_callcenter`;
                -- UPDATE jucese_callcenter.cidadaos SET user_id=60  WHERE user_id=6;
                -- UPDATE jucese_callcenter.cidadaos SET user_id=110 WHERE user_id=10;
                -- UPDATE jucese_callcenter.cidadaos SET user_id=111 WHERE user_id=11;
                -- UPDATE jucese_callcenter.cidadaos SET user_id=112 WHERE user_id=12;
                -- UPDATE jucese_callcenter.cidadaos SET user_id=113 WHERE user_id=13;
                -- UPDATE jucese_callcenter.cidadaos SET user_id=115 WHERE user_id=15;
                -- UPDATE jucese_callcenter.cidadaos SET user_id=7   WHERE user_id=17;
                -- UPDATE jucese_callcenter.cidadaos SET user_id=116 WHERE user_id=16;
                        -- UPDATE jucese_callcenter.cidadaos SET user_id=119 WHERE user_id=19;
                        -- UPDATE jucese_callcenter.cidadaos SET user_id=121 WHERE user_id=21;
                        -- UPDATE jucese_callcenter.cidadaos SET user_id=122 WHERE user_id=22;
                        -- UPDATE jucese_callcenter.cidadaos SET user_id=123 WHERE user_id=23;
                        -- UPDATE jucese_callcenter.cidadaos SET user_id=124 WHERE user_id=24;
                        -- UPDATE jucese_callcenter.cidadaos SET user_id=125 WHERE user_id=25;
                        -- UPDATE jucese_callcenter.cidadaos SET user_id=126 WHERE user_id=26;
                        -- UPDATE jucese_callcenter.cidadaos SET user_id=127 WHERE user_id=27;
                        -- UPDATE jucese_callcenter.cidadaos SET user_id=128 WHERE user_id=28;
                        -- UPDATE jucese_callcenter.cidadaos SET user_id=7   WHERE user_id=17;

        -- 10) Renomear user_id em tipo de atendimentos - ok
                USE `jucese_callcenter`;
                --UPDATE jucese_callcenter.tipoatendimentos SET user_id=60  WHERE user_id=6;
                --PDATE jucese_callcenter.tipoatendimentos SET user_id=110 WHERE user_id=10;
                --UPDATE jucese_callcenter.tipoatendimentos SET user_id=111 WHERE user_id=11;
                --UPDATE jucese_callcenter.tipoatendimentos SET user_id=112 WHERE user_id=12;
                --UPDATE jucese_callcenter.tipoatendimentos SET user_id=113 WHERE user_id=13;
                --UPDATE jucese_callcenter.tipoatendimentos SET user_id=115 WHERE user_id=15;
                --UPDATE jucese_callcenter.tipoatendimentos SET user_id=7   WHERE user_id=17;
                -- UPDATE jucese_callcenter.tipoatendimentos SET user_id=116 WHERE user_id=16;
                        -- UPDATE jucese_callcenter.tipoatendimentos SET user_id=119 WHERE user_id=19;
                        -- UPDATE jucese_callcenter.tipoatendimentos SET user_id=121 WHERE user_id=21;
                        -- UPDATE jucese_callcenter.tipoatendimentos SET user_id=122 WHERE user_id=22;
                        -- UPDATE jucese_callcenter.tipoatendimentos SET user_id=123 WHERE user_id=23;
                        -- UPDATE jucese_callcenter.tipoatendimentos SET user_id=124 WHERE user_id=24;
                        -- UPDATE jucese_callcenter.tipoatendimentos SET user_id=125 WHERE user_id=25;
                        -- UPDATE jucese_callcenter.tipoatendimentos SET user_id=126 WHERE user_id=26;
                        -- UPDATE jucese_callcenter.tipoatendimentos SET user_id=127 WHERE user_id=27;
                        -- UPDATE jucese_callcenter.tipoatendimentos SET user_id=128 WHERE user_id=28;
                        -- UPDATE jucese_callcenter.tipoatendimentos SET user_id=7   WHERE user_id=17;

        -- 11) Renomear user_id em tipo de cidadãos - ok
                USE `jucese_callcenter`;
                --UPDATE jucese_callcenter.tipocidadaos SET user_id=60  WHERE user_id=6;
                --UPDATE jucese_callcenter.tipocidadaos SET user_id=110 WHERE user_id=10;
                --UPDATE jucese_callcenter.tipocidadaos SET user_id=111 WHERE user_id=11;
                --UPDATE jucese_callcenter.tipocidadaos SET user_id=112 WHERE user_id=12;
                --UPDATE jucese_callcenter.tipocidadaos SET user_id=113 WHERE user_id=13;
                --UPDATE jucese_callcenter.tipocidadaos SET user_id=115 WHERE user_id=15;
                --UPDATE jucese_callcenter.tipocidadaos SET user_id=7   WHERE user_id=17;
                -- UPDATE jucese_callcenter.tipocidadaos SET user_id=116 WHERE user_id=16;
                        -- UPDATE jucese_callcenter.tipocidadaos SET user_id=119 WHERE user_id=19;
                        -- UPDATE jucese_callcenter.tipocidadaos SET user_id=121 WHERE user_id=21;
                        -- UPDATE jucese_callcenter.tipocidadaos SET user_id=122 WHERE user_id=22;
                        -- UPDATE jucese_callcenter.tipocidadaos SET user_id=123 WHERE user_id=23;
                        -- UPDATE jucese_callcenter.tipocidadaos SET user_id=124 WHERE user_id=24;
                        -- UPDATE jucese_callcenter.tipocidadaos SET user_id=125 WHERE user_id=25;
                        -- UPDATE jucese_callcenter.tipocidadaos SET user_id=126 WHERE user_id=26;
                        -- UPDATE jucese_callcenter.tipocidadaos SET user_id=127 WHERE user_id=27;
                        -- UPDATE jucese_callcenter.tipocidadaos SET user_id=128 WHERE user_id=28;
                        -- UPDATE jucese_callcenter.tipocidadaos SET user_id=7   WHERE user_id=17;

        -- 12) Renomear user_id em tipo de problemas - ok
                USE `jucese_callcenter`;
                --UPDATE jucese_callcenter.tipoproblemas SET user_id=60  WHERE user_id=6;
                --UPDATE jucese_callcenter.tipoproblemas SET user_id=110 WHERE user_id=10;
                --UPDATE jucese_callcenter.tipoproblemas SET user_id=111 WHERE user_id=11;
                --UPDATE jucese_callcenter.tipoproblemas SET user_id=112 WHERE user_id=12;
                --UPDATE jucese_callcenter.tipoproblemas SET user_id=113 WHERE user_id=13;
                --UPDATE jucese_callcenter.tipoproblemas SET user_id=115 WHERE user_id=15;
                --UPDATE jucese_callcenter.tipoproblemas SET user_id=7   WHERE user_id=17;
                -- UPDATE jucese_callcenter.tipoproblemas SET user_id=116 WHERE user_id=16;
                        -- UPDATE jucese_callcenter.tipoproblemas SET user_id=119 WHERE user_id=19;
                        -- UPDATE jucese_callcenter.tipoproblemas SET user_id=121 WHERE user_id=21;
                        -- UPDATE jucese_callcenter.tipoproblemas SET user_id=122 WHERE user_id=22;
                        -- UPDATE jucese_callcenter.tipoproblemas SET user_id=123 WHERE user_id=23;
                        -- UPDATE jucese_callcenter.tipoproblemas SET user_id=124 WHERE user_id=24;
                        -- UPDATE jucese_callcenter.tipoproblemas SET user_id=125 WHERE user_id=25;
                        -- UPDATE jucese_callcenter.tipoproblemas SET user_id=126 WHERE user_id=26;
                        -- UPDATE jucese_callcenter.tipoproblemas SET user_id=127 WHERE user_id=27;
                        -- UPDATE jucese_callcenter.tipoproblemas SET user_id=128 WHERE user_id=28;
                        -- UPDATE jucese_callcenter.tipoproblemas SET user_id=7   WHERE user_id=17;

        -- 13) Renomear user_id em tipo de soluções - ok
                USE `jucese_callcenter`;
                --UPDATE jucese_callcenter.tiposolucaos SET user_id=60  WHERE user_id=6;
                --UPDATE jucese_callcenter.tiposolucaos SET user_id=110 WHERE user_id=10;
                --UPDATE jucese_callcenter.tiposolucaos SET user_id=111 WHERE user_id=11;
                --UPDATE jucese_callcenter.tiposolucaos SET user_id=112 WHERE user_id=12;
                --UPDATE jucese_callcenter.tiposolucaos SET user_id=113 WHERE user_id=13;
                --UPDATE jucese_callcenter.tiposolucaos SET user_id=115 WHERE user_id=15;
                --UPDATE jucese_callcenter.tiposolucaos SET user_id=7   WHERE user_id=17;
                -- UPDATE jucese_callcenter.tiposolucaos SET user_id=116 WHERE user_id=16;
                        -- UPDATE jucese_callcenter.tiposolucaos SET user_id=119 WHERE user_id=19;
                        -- UPDATE jucese_callcenter.tiposolucaos SET user_id=121 WHERE user_id=21;
                        -- UPDATE jucese_callcenter.tiposolucaos SET user_id=122 WHERE user_id=22;
                        -- UPDATE jucese_callcenter.tiposolucaos SET user_id=123 WHERE user_id=23;
                        -- UPDATE jucese_callcenter.tiposolucaos SET user_id=124 WHERE user_id=24;
                        -- UPDATE jucese_callcenter.tiposolucaos SET user_id=125 WHERE user_id=25;
                        -- UPDATE jucese_callcenter.tiposolucaos SET user_id=126 WHERE user_id=26;
                        -- UPDATE jucese_callcenter.tiposolucaos SET user_id=127 WHERE user_id=27;
                        -- UPDATE jucese_callcenter.tiposolucaos SET user_id=128 WHERE user_id=28;
                        -- UPDATE jucese_callcenter.tiposolucaos SET user_id=7   WHERE user_id=17;



        -- 14) Criar registros de permissoes - ok
                USE `autenticacao`;
                SELECT * FROM autenticacao.permissao_grupo WHERE permissoes_id in ( SELECT id FROM autenticacao.permissoes WHERE nome like '%callcenter/%' );
                
                SELECT * FROM `permissao_grupo` WHERE permissoes_id >= 500 and permissoes_id <> 539 order by permissoes_id asc;

                        DELETE 
                        FROM autenticacao.permissao_grupo
                        WHERE id >= 1500 and ( grupouser_id = 41 or grupouser_id = 40 or grupouser_id = 10 or grupouser_id = 5 or grupouser_id = 60 or grupouser_id = 11);

                        DELETE 
                        FROM autenticacao.permissoes
                        WHERE nome like '%callcenter/%';

                USE `autenticacao`;
                INSERT INTO autenticacao.permissoes (id, nome, descricao) VALUES
                        (1500, 'callcenter/item_menu/chamada',                                                  'CALLCENTER - ITEM DE MENU CHAMADA'),
                        (1501, 'callcenter/item_menu/tipoproblema',                                             'CALLCENTER - ITEM DE MENU TIPO DE PROBLEMA'),
                        (1502, 'callcenter/item_menu/tiposolucao',                                              'CALLCENTER - ITEM DE MENU TIPO DE SOLUÇÃO'),
                        (1503, 'callcenter/item_menu/tipocidadao',                                              'CALLCENTER - ITEM DE MENU TIPO DE CIDADÃO'),
                        (1504, 'callcenter/item_menu/cidadao',                                                  'CALLCENTER - ITEM DE MENU CIDADÃO'),
                        (1505, 'callcenter/item_menu/tipoatendimento',                                          'CALLCENTER - ITEM DE MENU TIPO DE ATENDIMENTO'),
                        (1506, 'callcenter/item_menu/consulta/chamadageral',                                    'CALLCENTER - ITEM DE MENU CONSULTA CHAMADA GERAL'),
                        (1507, 'callcenter/item_menu/consulta/produtividade',                                   'CALLCENTER - ITEM DE MENU CONSULTA PRODUTIVIDADE'),
                        (1508, 'callcenter/item_menu/consulta/painelonline',                                    'CALLCENTER - ITEM DE MENU PAINEL ON-LINE'),
                        (1509, 'callcenter/item_menu/consulta/problemasporcidadao',                             'CALLCENTER - ITEM DE MENU CONSULTA PROBLEMAS POR CIDADÃO'),
                        (1510, 'callcenter/item_menu/consulta/atendimentosporcpf',                              'CALLCENTER - ITEM DE MENU CONSULTA ATENDIMENTOS POR C.P.F.'),
                        (1511, 'callcenter/chamada/acoes',                                                      'CALLCENTER - CHAMADA - LISTAR CHAMADA'),
                        (1512, 'callcenter/chamada/acoes/pesquisarcpf',                                         'CALLCENTER - CHAMADA - PESQUISAR C.P.F. PARA NOVA CHAMADA'),
                        (1513, 'callcenter/chamada/acoes/cadastroredirecionado/{id}',                           'CALLCENTER - CHAMADA - REDIRECIONAMENTO DE CHAMADAS'),
                        (1514, 'callcenter/chamada/acoes/salvar',                                               'CALLCENTER - CHAMADA - SALVAR CHAMADA'),
                        (1515, 'callcenter/chamada/acoes/salvarredirecionado',                                  'CALLCENTER - CHAMADA - SALVAR REDIRECIONAMENTO DE CHAMADA'),
                        (1516, 'callcenter/chamada/acoes/editar/{id}',                                          'CALLCENTER - CHAMADA - EDITAR CHAMADA'),
                        (1517, 'callcenter/chamada/acoes/adicionar/{id}',                                       'CALLCENTER - CHAMADA - ADICIONAR CHAMADA'),
                        (1518, 'callcenter/chamada/acoes/atualizar/{id}',                                       'CALLCENTER - CHAMADA - ATUALIZAR CHAMADA'),
                        (1519, 'callcenter/chamada/acoes/deletar/{id}',                                         'CALLCENTER - CHAMADA - EXCLUIR CHAMADA'),
                        (1520, 'callcenter/tipoproblema/acoes',                                                 'CALLCENTER - TIPO DE PROBLEMA - LISTAR TIPO DE PROBLEMA'),
                        (1521, 'callcenter/tipoproblema/acoes/salvar',                                          'CALLCENTER - TIPO DE PROBLEMA - SALVAR TIPO DE PROBLEMA'),
                        (1522, 'callcenter/tipoproblema/acoes/editar/{id}',                                     'CALLCENTER - TIPO DE PROBLEMA - EDITAR TIPO DE PROBLEMA'),
                        (1523, 'callcenter/tipoproblema/acoes/atualizar/{id}',                                  'CALLCENTER - TIPO DE PROBLEMA - ATUALIZAR TIPO DE PROBLEMA'),
                        (1524, 'callcenter/tipoproblema/acoes/deletar/{id}',                                    'CALLCENTER - TIPO DE PROBLEMA - EXCLUIR TIPO DE PROBLEMA'),
                        (1525, 'callcenter/tipoproblema/acoes/imprimir',                                        'CALLCENTER - TIPO DE PROBLEMA - IMPRIMIR TIPO DE PROBLEMA'),
                        (1526, 'callcenter/tiposolucao/acoes',                                                  'CALLCENTER - TIPO DE SOLUÇÃO - LISTAR TIPO DE SOLUÇÃO'),
                        (1527, 'callcenter/tiposolucao/acoes/salvar',                                           'CALLCENTER - TIPO DE SOLUÇÃO - SALVAR TIPO DE SOLUÇÃO'),
                        (1528, 'callcenter/tiposolucao/acoes/editar/{id}',                                      'CALLCENTER - TIPO DE SOLUÇÃO - EDITAR TIPO DE SOLUÇÃO'),
                        (1529, 'callcenter/tiposolucao/acoes/atualizar/{id}',                                   'CALLCENTER - TIPO DE SOLUÇÃO - ATUALIZAR TIPO DE SOLUÇÃO'),
                        (1530, 'callcenter/tiposolucao/acoes/deletar/{id}',                                     'CALLCENTER - TIPO DE SOLUÇÃO - EXCLUIR TIPO DE SOLUÇÃO'),
                        (1531, 'callcenter/tiposolucao/acoes/imprimir',                                         'CALLCENTER - TIPO DE SOLUÇÃO - IMPRIMIR TIPO DE SOLUÇÃO'),
                        (1532, 'callcenter/tipoatendimento/acoes',                                              'CALLCENTER - TIPO DE ATENDIMENTO - LISTAR TIPO DE ATENDIMENTO'),
                        (1533, 'callcenter/tipoatendimento/acoes/salvar',                                       'CALLCENTER - TIPO DE ATENDIMENTO - SALVAR TIPO DE ATENDIMENTO'),
                        (1534, 'callcenter/tipoatendimento/acoes/editar/{id}',                                  'CALLCENTER - TIPO DE ATENDIMENTO - EDITAR TIPO DE ATENDIMENTO'),
                        (1535, 'callcenter/tipoatendimento/acoes/atualizar/{id}',                               'CALLCENTER - TIPO DE ATENDIMENTO - ATUALIZAR TIPO DE ATENDIMENTO'),
                        (1536, 'callcenter/tipoatendimento/acoes/deletar/{id}',                                 'CALLCENTER - TIPO DE ATENDIMENTO - EXCLUIR TIPO DE ATENDIMENTO'),
                        (1537, 'callcenter/tipoatendimento/acoes/imprimir',                                     'CALLCENTER - TIPO DE ATENDIMENTO - IMPRIMIR TIPO DE ATENDIMENTO'),
                        (1538, 'callcenter/cidadao/acoes',                                                      'CALLCENTER - CIDADÃO - LISTAR CIDADÃO'),
                        (1539, 'callcenter/cidadao/acoes/salvar',                                               'CALLCENTER - CIDADÃO - SALVAR CIDADÃO'),
                        (1540, 'callcenter/cidadao/acoes/editar/{id}',                                          'CALLCENTER - CIDADÃO - EDITAR CIDADÃO'),
                        (1541, 'callcenter/cidadao/acoes/atualizar/{id}',                                       'CALLCENTER - CIDADÃO - ATUALIZAR CIDADÃO'),
                        (1542, 'callcenter/cidadao/acoes/deletar/{id}',                                         'CALLCENTER - CIDADÃO - EXCLUIR CIDADÃO'),
                        (1543, 'callcenter/cidadao/acoes/imprimir',                                             'CALLCENTER - CIDADÃO - IMPRIMIR CIDADÃO'),
                        (1544, 'callcenter/tipocidadao/acoes',                                                  'CALLCENTER - TIPO DE CIDADÃO - LISTAR TIPO DE CIDADÃO'),
                        (1545, 'callcenter/tipocidadao/acoes/salvar',                                           'CALLCENTER - TIPO DE CIDADÃO - SALVAR TIPO DE CIDADÃO'),
                        (1546, 'callcenter/tipocidadao/acoes/editar/{id}',                                      'CALLCENTER - TIPO DE CIDADÃO - EDITAR TIPO DE CIDADÃO'),
                        (1547, 'callcenter/tipocidadao/acoes/atualizar/{id}',                                   'CALLCENTER - TIPO DE CIDADÃO - ATUALIZAR TIPO DE CIDADÃO'),
                        (1548, 'callcenter/tipocidadao/acoes/deletar/{id}',                                     'CALLCENTER - TIPO DE CIDADÃO - EXCLUIR TIPO DE CIDADÃO'),
                        (1549, 'callcenter/tipocidadao/acoes/imprimir',                                         'CALLCENTER - TIPO DE CIDADÃO - IMPRIMIR TIPO DE CIDADÃO'),
                        (1550, 'callcenter/consulta/chamadageral',                                              'CALLCENTER - CONSULTA - CHAMADA GERAL - PARAMETROS'),
                        (1551, 'callcenter/consulta/chamadageral/resultado',                                    'CALLCENTER - CONSULTA - CHAMADA GERAL - RESULTADO'),
                        (1552, 'callcenter/consulta/chamadageral/imprimir',                                     'CALLCENTER - CONSULTA - CHAMADA GERAL - IMPRIMIR'),
                        (1553, 'callcenter/consulta/problemasportipocidadao',                                   'CALLCENTER - CONSULTA - PROBLEMAS POR TIPO DE CIDADÃO - PARAMETROS'),
                        (1554, 'callcenter/consulta/problemasportipocidadao/resultado',                         'CALLCENTER - CONSULTA - PROBLEMAS POR TIPO DE CIDADÃO - RESULTADO'),
                        (1555, 'callcenter/consulta/problemasportipocidadao/imprimir',                          'CALLCENTER - CONSULTA - PROBLEMAS POR TIPO DE CIDADÃO - IMPRIMIR'),
                        (1556, 'callcenter/consulta/atendimentosporcpf',                                        'CALLCENTER - CONSULTA - ATENDIMENTOS POR C.P.F. - PARAMETROS'),
                        (1557, 'callcenter/consulta/atendimentosporcpf/resultado',                              'CALLCENTER - CONSULTA - ATENDIMENTOS POR C.P.F. - RESULTADO'),
                        (1558, 'callcenter/consulta/atendimentosporcpf/imprimir',                               'CALLCENTER - CONSULTA - ATENDIMENTOS POR C.P.F. - IMPRIMIR'),
                        (1559, 'callcenter/consulta/produtividade',                                             'CALLCENTER - CONSULTA - PRODUTIVIDADE - PARAMETROS'),
                        (1560, 'callcenter/consulta/produtividade/resultado',                                   'CALLCENTER - CONSULTA - PRODUTIVIDADE - RESULTADO'),
                        (1561, 'callcenter/consulta/produtividade/imprimir/imprimir_poranomes',                 'CALLCENTER - CONSULTA - PRODUTIVIDADE - IMPRIMIR - POR ANO / MES'),
                        (1562, 'callcenter/consulta/produtividade/imprimir/imprimir_agilizaxcpf',               'CALLCENTER - CONSULTA - PRODUTIVIDADE - IMPRIMIR - AGILIZA X CPF'),
                        (1563, 'callcenter/consulta/produtividade/imprimir/imprimir_clientescomchamadas',       'CALLCENTER - CONSULTA - PRODUTIVIDADE - IMPRIMIR - CLIENTES COM CHAMADAS'),
                        (1564, 'callcenter/consulta/produtividade/imprimir/imprimir_cpfdistintoportipocidadao', 'CALLCENTER - CONSULTA - PRODUTIVIDADE - IMPRIMIR - CPF DISTINTO POR TIPO DE CIDADAO'),
                        (1565, 'callcenter/consulta/produtividade/imprimir/imprimir_poratendente',              'CALLCENTER - CONSULTA - PRODUTIVIDADE - IMPRIMIR - POR ATENDENTE'),
                        (1566, 'callcenter/consulta/produtividade/imprimir/imprimir_pordataatendente',          'CALLCENTER - CONSULTA - PRODUTIVIDADE - IMPRIMIR - POR DATA / ATENDENTE'),
                        (1567, 'callcenter/consulta/produtividade/imprimir/imprimir_pordatahora',               'CALLCENTER - CONSULTA - PRODUTIVIDADE - IMPRIMIR - POR DATA / HORA'),
                        (1568, 'callcenter/consulta/produtividade/imprimir/imprimir_pordia',                    'CALLCENTER - CONSULTA - PRODUTIVIDADE - IMPRIMIR - POR DIA'),
                        (1569, 'callcenter/consulta/produtividade/imprimir/imprimir_porhora',                   'CALLCENTER - CONSULTA - PRODUTIVIDADE - IMPRIMIR - POR HORA'),
                        (1570, 'callcenter/consulta/produtividade/imprimir/imprimir_portipocidadao',            'CALLCENTER - CONSULTA - PRODUTIVIDADE - IMPRIMIR - POR TIPO DE CIDADAO'),
                        (1571, 'callcenter/consulta/produtividade/imprimir/imprimir_portipoproblema',           'CALLCENTER - CONSULTA - PRODUTIVIDADE - IMPRIMIR - POR TIPO DE PROBLEMA'),
                        (1572, 'callcenter/consulta/produtividade/imprimir/imprimir_portiposolucao',            'CALLCENTER - CONSULTA - PRODUTIVIDADE - IMPRIMIR - POR TIPO DE SOLUCAO'),
                        (1573, 'callcenter/consulta/produtividade/imprimir/imprimir_problemascomocorrencias',   'CALLCENTER - CONSULTA - PRODUTIVIDADE - IMPRIMIR - PROBLEMAS COM OCORRENCIAS'),
                        (1574, 'callcenter/consulta/produtividade/imprimir/imprimir_solucoescomocorrencias',    'CALLCENTER - CONSULTA - PRODUTIVIDADE - IMPRIMIR - SOLUCOES COM OCORRENCIAS'),
                        (1575, 'callcenter/consulta/painelonline',                                              'CALLCENTER - CONSULTA - PAINEL ON-LINE'),
                        (1576, 'callcenter/consulta/painelonline/imprimir_painelonline',                        'CALLCENTER - CONSULTA - PAINEL ON-LINE - IMPRIMIR'),
                        (1577, 'callcenter/consulta/dashboard',                                                 'CALLCENTER - CONSULTA - DASHBOARD');

        -- 15) Criar registros de permissao_grupo - ok
                USE `autenticacao`;
                INSERT INTO autenticacao.permissao_grupo (id, permissoes_id, grupouser_id) VALUES
                        (1500,1500,5),	(1577,1500,10),
                        (1501,1501,5),	(1578,1501,10),
                        (1502,1502,5),	(1579,1502,10),
                        (1503,1503,5),	(1580,1503,10),
                        (1504,1504,5),	(1581,1504,10),
                        (1505,1505,5),	(1582,1505,10),
                        (1506,1506,5),	(1583,1506,10),
                        (1507,1507,5),	(1584,1507,10),
                        (1508,1508,5),	(1585,1508,10),
                        (1509,1509,5),	(1586,1509,10),
                        (1510,1510,5),	(1587,1510,10),
                        (1511,1511,5),	(1588,1511,10),
                        (1512,1512,5),	(1589,1512,10),
                        (1513,1513,5),	(1590,1513,10),
                        (1514,1514,5),	(1591,1514,10),
                        (1515,1515,5),	(1592,1515,10),
                        (1516,1516,5),	(1593,1516,10),
                        (1517,1517,5),	(1594,1517,10),
                        (1518,1518,5),	(1595,1518,10),
                        (1519,1519,5),	(1596,1519,10),
                        (1520,1520,5),	(1597,1520,10),
                        (1521,1521,5),	(1598,1521,10),
                        (1522,1522,5),	(1599,1522,10),
                        (1523,1523,5),	(1600,1523,10),
                        (1524,1524,5),	(1601,1524,10),
                        (1525,1525,5),	(1602,1525,10),
                        (1526,1526,5),	(1603,1526,10),
                        (1527,1527,5),	(1604,1527,10),
                        (1528,1528,5),	(1605,1528,10),
                        (1529,1529,5),	(1606,1529,10),
                        (1530,1530,5),	(1607,1530,10),
                        (1531,1531,5),	(1608,1531,10),
                        (1532,1532,5),	(1609,1532,10),
                        (1533,1533,5),	(1610,1533,10),
                        (1534,1534,5),	(1611,1534,10),
                        (1535,1535,5),	(1612,1535,10),
                        (1536,1536,5),	(1613,1536,10),
                        (1537,1537,5),	(1614,1537,10),
                        (1538,1538,5),	(1615,1538,10),
                        (1539,1539,5),	(1616,1539,10),
                        (1540,1540,5),	(1617,1540,10),
                        (1541,1541,5),	(1618,1541,10),
                        (1542,1542,5),	(1619,1542,10),
                        (1543,1543,5),	(1620,1543,10),
                        (1544,1544,5),	(1621,1544,10),
                        (1545,1545,5),	(1622,1545,10),
                        (1546,1546,5),	(1623,1546,10),
                        (1547,1547,5),	(1624,1547,10),
                        (1548,1548,5),	(1625,1548,10),
                        (1549,1549,5),	(1626,1549,10),
                        (1550,1550,5),	(1627,1550,10),
                        (1551,1551,5),	(1628,1551,10),
                        (1552,1552,5),	(1629,1552,10),
                        (1553,1553,5),	(1630,1553,10),
                        (1554,1554,5),	(1631,1554,10),
                        (1555,1555,5),	(1632,1555,10),
                        (1556,1556,5),	(1633,1556,10),
                        (1557,1557,5),	(1634,1557,10),
                        (1558,1558,5),	(1635,1558,10),
                        (1559,1559,5),	(1636,1559,10),
                        (1560,1560,5),	(1637,1560,10),
                        (1561,1561,5),	(1638,1561,10),
                        (1562,1562,5),	(1639,1562,10),
                        (1563,1563,5),	(1640,1563,10),
                        (1564,1564,5),	(1641,1564,10),
                        (1565,1565,5),	(1642,1565,10),
                        (1566,1566,5),	(1643,1566,10),
                        (1567,1567,5),	(1644,1567,10),
                        (1568,1568,5),	(1645,1568,10),
                        (1569,1569,5),	(1646,1569,10),
                        (1570,1570,5),	(1647,1570,10),
                        (1571,1571,5),	(1648,1571,10),
                        (1572,1572,5),	(1649,1572,10),
                        (1573,1573,5),	(1650,1573,10),
                        (1574,1574,5),	(1651,1574,10),
                        (1575,1575,5),	(1652,1575,10),
                        (1576,1576,5),	(1653,1576,10);

        -- 16) Criar procedure
                USE `jucese_callcenter`;

                DELIMITER //
                REPLACE PROCEDURE acumula_while (limite TINYINT UNSIGNED)
                begin
                        DECLARE contador TINYINT UNSIGNED DEFAULT 0;
                        DECLARE soma INT DEFAULT 0;
                        WHILE contador < limite DO
                                SET contador = contador + 1;
                                SET soma = soma + contador;
                        END WHILE;
                        SELECT soma;
                END 
                // DELIMITER;


                testando ...

                CALL acumula_while(10);
                CALL acumula_while(0);

        -- 17) Renomear palavras erradas
                USE `jucese_callcenter`;

                DELIMITER //
                REPLACE PROCEDURE substituiPalavras (limite TINYINT UNSIGNED)
                begin
                        DECLARE contador TINYINT UNSIGNED DEFAULT 0;
                        DECLARE soma INT DEFAULT 0;
                        WHILE contador < limite DO
                                SET contador = contador + 1;
                                SET soma = soma + contador;
                        END WHILE;
                        SELECT soma;
                END 
                // DELIMITER;


                USE `jucese_callcenter`;

                -- Tipo de Problema
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'NÃƒO', 'NÃO' )                      WHERE descricao like '%NÃƒO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'ALTEARÃ‡ÃƒO', 'ALTERAÇÃO' )         WHERE descricao like '%ALTEARÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'ALTERAÃ‡ÃƒO', 'ALTERAÇÃO' )         WHERE descricao like '%ALTERAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'SÃ“CIO', 'SÓCIO' )                  WHERE descricao like '%SÃ“CIO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'TÃ‰CNICO', 'TÉCNICO' )              WHERE descricao like '%TÃ‰CNICO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'BALANÃ‡O', 'BALANÇO' )              WHERE descricao like '%BALANÃ‡O%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'DIÃRIO', 'DÁRIO' )                 WHERE descricao like '%DIÃRIO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'EXIGÃŠNCIA', 'EXIGÊNCIA' )          WHERE descricao like '%EXIGÃŠNCIA%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'CERTIDÃ•ES', 'CERTIDÕES' )          WHERE descricao like '%CERTIDÃ•ES%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'ELETRÃ”NICO', 'ELETRÔNICO' )        WHERE descricao like '%ELETRÃ”NICO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'CONSTITUIÃ‡ÃƒO', 'CONSTITUIÇÃO' )   WHERE descricao like '%CONSTITUIÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'DÃšVIDAS', 'ALTERAÇÃO' )            WHERE descricao like '%DÃšVIDAS%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'CORREÃ‡ÃƒO', 'CORREÇÃO' )           WHERE descricao like '%CORREÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'RECLAMAÃ‡Ã•ES', 'RECLAMAÇÕES' )     WHERE descricao like '%RECLAMAÃ‡Ã•ES%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'Ã“RGÃƒOS', 'ÓRGÃOS' )               WHERE descricao like '%Ã“RGÃƒOS%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'TRANSFERÃŠNCIA', 'TRANSFERÊNCIA' )  WHERE descricao like '%TRANSFERÃŠNCIA%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'CARTÃ“RIO', 'CARTÓRIO' )            WHERE descricao like '%CARTÃ“RIO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'TRANSFORMAÃ‡ÃƒO', 'TRANSFORMAÇÃO' ) WHERE descricao like '%TRANSFORMAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'JURÃDICA', 'JURÍDICA' )            WHERE descricao like '%JURÃDICA%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'DOCUMENTAÃ‡ÃƒO', 'DOCUMENTAÇÃO' )   WHERE descricao like '%DOCUMENTAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'NECESSÃRIA', 'NECESSÁRIA' )        WHERE descricao like '%NECESSÃRIA%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'ORIENTAÃ‡ÃƒO', 'ORIENTAÇÃO' )       WHERE descricao like '%ORIENTAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'LIGAÃ‡ÃƒO', 'LIGAÇÃO' )             WHERE descricao like '%LIGAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'EMPRESÃRIO', 'EMPRESÁRIO' )        WHERE descricao like '%EMPRESÃRIO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'ESCRITÃ“RIO', 'ESCRITÓRIO' )        WHERE descricao like '%ESCRITÃ“RIO%';
                        UPDATE jucese_callcenter.tipoproblemas SET descricao = replace( descricao, 'EXTINÃ‡ÃƒO', 'EXTINÇÃO' )           WHERE descricao like '%EXTINÃ‡ÃƒO%';

                -- Tipo de Solucao
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'NÃƒO', 'NÃO' )                      WHERE descricao like '%NÃƒO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'ALTEARÃ‡ÃƒO', 'ALTERAÇÃO' )         WHERE descricao like '%ALTEARÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'ALTERAÃ‡ÃƒO', 'ALTERAÇÃO' )         WHERE descricao like '%ALTERAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'SÃ“CIO', 'SÓCIO' )                  WHERE descricao like '%SÃ“CIO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'TÃ‰CNICO', 'TÉCNICO' )              WHERE descricao like '%TÃ‰CNICO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'BALANÃ‡O', 'BALANÇO' )              WHERE descricao like '%BALANÃ‡O%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'DIÃRIO', 'DÁRIO' )                 WHERE descricao like '%DIÃRIO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'EXIGÃŠNCIA', 'EXIGÊNCIA' )          WHERE descricao like '%EXIGÃŠNCIA%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'CERTIDÃ•ES', 'CERTIDÕES' )          WHERE descricao like '%CERTIDÃ•ES%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'ELETRÃ”NICO', 'ELETRÔNICO' )        WHERE descricao like '%ELETRÃ”NICO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'CONSTITUIÃ‡ÃƒO', 'CONSTITUIÇÃO' )   WHERE descricao like '%CONSTITUIÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'DÃšVIDAS', 'ALTERAÇÃO' )            WHERE descricao like '%DÃšVIDAS%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'CORREÃ‡ÃƒO', 'CORREÇÃO' )           WHERE descricao like '%CORREÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'RECLAMAÃ‡Ã•ES', 'RECLAMAÇÕES' )     WHERE descricao like '%RECLAMAÃ‡Ã•ES%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'Ã“RGÃƒOS', 'ÓRGÃOS' )               WHERE descricao like '%Ã“RGÃƒOS%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'TRANSFERÃŠNCIA', 'TRANSFERÊNCIA' )  WHERE descricao like '%TRANSFERÃŠNCIA%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'CARTÃ“RIO', 'CARTÓRIO' )            WHERE descricao like '%CARTÃ“RIO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'TRANSFORMAÃ‡ÃƒO', 'TRANSFORMAÇÃO' ) WHERE descricao like '%TRANSFORMAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'JURÃDICA', 'JURÍDICA' )            WHERE descricao like '%JURÃDICA%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'DOCUMENTAÃ‡ÃƒO', 'DOCUMENTAÇÃO' )   WHERE descricao like '%DOCUMENTAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'NECESSÃRIA', 'NECESSÁRIA' )        WHERE descricao like '%NECESSÃRIA%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'ORIENTAÃ‡ÃƒO', 'ORIENTAÇÃO' )       WHERE descricao like '%ORIENTAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'LIGAÃ‡ÃƒO', 'LIGAÇÃO' )             WHERE descricao like '%LIGAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'EMPRESÃRIO', 'EMPRESÁRIO' )        WHERE descricao like '%EMPRESÃRIO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'ESCRITÃ“RIO', 'ESCRITÓRIO' )        WHERE descricao like '%ESCRITÃ“RIO%';
                        UPDATE jucese_callcenter.tiposolucaos SET descricao = replace( descricao, 'EXTINÃ‡ÃƒO', 'EXTINÇÃO' )           WHERE descricao like '%EXTINÃ‡ÃƒO%';

                -- Tipo de Cidadão
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'NÃƒO', 'NÃO' )                      WHERE descricao like '%NÃƒO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'ALTEARÃ‡ÃƒO', 'ALTERAÇÃO' )         WHERE descricao like '%ALTEARÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'ALTERAÃ‡ÃƒO', 'ALTERAÇÃO' )         WHERE descricao like '%ALTERAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'SÃ“CIO', 'SÓCIO' )                  WHERE descricao like '%SÃ“CIO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'TÃ‰CNICO', 'TÉCNICO' )              WHERE descricao like '%TÃ‰CNICO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'BALANÃ‡O', 'BALANÇO' )              WHERE descricao like '%BALANÃ‡O%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'DIÃRIO', 'DÁRIO' )                 WHERE descricao like '%DIÃRIO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'EXIGÃŠNCIA', 'EXIGÊNCIA' )          WHERE descricao like '%EXIGÃŠNCIA%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'CERTIDÃ•ES', 'CERTIDÕES' )          WHERE descricao like '%CERTIDÃ•ES%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'ELETRÃ”NICO', 'ELETRÔNICO' )        WHERE descricao like '%ELETRÃ”NICO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'CONSTITUIÃ‡ÃƒO', 'CONSTITUIÇÃO' )   WHERE descricao like '%CONSTITUIÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'DÃšVIDAS', 'ALTERAÇÃO' )            WHERE descricao like '%DÃšVIDAS%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'CORREÃ‡ÃƒO', 'CORREÇÃO' )           WHERE descricao like '%CORREÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'RECLAMAÃ‡Ã•ES', 'RECLAMAÇÕES' )     WHERE descricao like '%RECLAMAÃ‡Ã•ES%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'Ã“RGÃƒOS', 'ÓRGÃOS' )               WHERE descricao like '%Ã“RGÃƒOS%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'TRANSFERÃŠNCIA', 'TRANSFERÊNCIA' )  WHERE descricao like '%TRANSFERÃŠNCIA%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'CARTÃ“RIO', 'CARTÓRIO' )            WHERE descricao like '%CARTÃ“RIO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'TRANSFORMAÃ‡ÃƒO', 'TRANSFORMAÇÃO' ) WHERE descricao like '%TRANSFORMAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'JURÃDICA', 'JURÍDICA' )            WHERE descricao like '%JURÃDICA%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'DOCUMENTAÃ‡ÃƒO', 'DOCUMENTAÇÃO' )   WHERE descricao like '%DOCUMENTAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'NECESSÃRIA', 'NECESSÁRIA' )        WHERE descricao like '%NECESSÃRIA%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'ORIENTAÃ‡ÃƒO', 'ORIENTAÇÃO' )       WHERE descricao like '%ORIENTAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'LIGAÃ‡ÃƒO', 'LIGAÇÃO' )             WHERE descricao like '%LIGAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'EMPRESÃRIO', 'EMPRESÁRIO' )        WHERE descricao like '%EMPRESÃRIO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'ESCRITÃ“RIO', 'ESCRITÓRIO' )        WHERE descricao like '%ESCRITÃ“RIO%';
                        UPDATE jucese_callcenter.tipocidadaos SET descricao = replace( descricao, 'EXTINÃ‡ÃƒO', 'EXTINÇÃO' )           WHERE descricao like '%EXTINÃ‡ÃƒO%';

                -- Cidadão
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'NÃƒO', 'NÃO' )                      WHERE nome like '%NÃƒO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'ALTEARÃ‡ÃƒO', 'ALTERAÇÃO' )         WHERE nome like '%ALTEARÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'ALTERAÃ‡ÃƒO', 'ALTERAÇÃO' )         WHERE nome like '%ALTERAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'SÃ“CIO', 'SÓCIO' )                  WHERE nome like '%SÃ“CIO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'TÃ‰CNICO', 'TÉCNICO' )              WHERE nome like '%TÃ‰CNICO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'BALANÃ‡O', 'BALANÇO' )              WHERE nome like '%BALANÃ‡O%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'DIÃRIO', 'DÁRIO' )                 WHERE nome like '%DIÃRIO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'EXIGÃŠNCIA', 'EXIGÊNCIA' )          WHERE nome like '%EXIGÃŠNCIA%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'CERTIDÃ•ES', 'CERTIDÕES' )          WHERE nome like '%CERTIDÃ•ES%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'ELETRÃ”NICO', 'ELETRÔNICO' )        WHERE nome like '%ELETRÃ”NICO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'CONSTITUIÃ‡ÃƒO', 'CONSTITUIÇÃO' )   WHERE nome like '%CONSTITUIÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'DÃšVIDAS', 'ALTERAÇÃO' )            WHERE nome like '%DÃšVIDAS%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'CORREÃ‡ÃƒO', 'CORREÇÃO' )           WHERE nome like '%CORREÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'RECLAMAÃ‡Ã•ES', 'RECLAMAÇÕES' )     WHERE nome like '%RECLAMAÃ‡Ã•ES%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'Ã“RGÃƒOS', 'ÓRGÃOS' )               WHERE nome like '%Ã“RGÃƒOS%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'TRANSFERÃŠNCIA', 'TRANSFERÊNCIA' )  WHERE nome like '%TRANSFERÃŠNCIA%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'CARTÃ“RIO', 'CARTÓRIO' )            WHERE nome like '%CARTÃ“RIO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'TRANSFORMAÃ‡ÃƒO', 'TRANSFORMAÇÃO' ) WHERE nome like '%TRANSFORMAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'JURÃDICA', 'JURÍDICA' )            WHERE nome like '%JURÃDICA%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'DOCUMENTAÃ‡ÃƒO', 'DOCUMENTAÇÃO' )   WHERE nome like '%DOCUMENTAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'NECESSÃRIA', 'NECESSÁRIA' )        WHERE nome like '%NECESSÃRIA%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'ORIENTAÃ‡ÃƒO', 'ORIENTAÇÃO' )       WHERE nome like '%ORIENTAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'LIGAÃ‡ÃƒO', 'LIGAÇÃO' )             WHERE nome like '%LIGAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'EMPRESÃRIO', 'EMPRESÁRIO' )        WHERE nome like '%EMPRESÃRIO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'ESCRITÃ“RIO', 'ESCRITÓRIO' )        WHERE nome like '%ESCRITÃ“RIO%';
                        UPDATE jucese_callcenter.cidadaos SET nome = replace( nome, 'EXTINÃ‡ÃƒO', 'EXTINÇÃO' )           WHERE nome like '%EXTINÃ‡ÃƒO%';

                -- Chamada
                        SET @tabela = '';
                        SET @campo = 'complemento_solucao';

                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'NÃƒO', 'NÃO' )                      WHERE complemento_solucao like '%NÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'ALTEARÃ‡ÃƒO', 'ALTERAÇÃO' )         WHERE complemento_solucao like '%ALTEARÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'ALTERAÃ‡ÃƒO', 'ALTERAÇÃO' )         WHERE complemento_solucao like '%ALTERAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'BALANÃ‡O', 'BALANÇO' )              WHERE complemento_solucao like '%BALANÃ‡O%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'CERTIDÃ•ES', 'CERTIDÕES' )          WHERE complemento_solucao like '%CERTIDÃ•ES%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'DIÃRIO', 'DÁRIO' )                 WHERE complemento_solucao like '%DIÃRIO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'EXIGÃŠNCIA', 'EXIGÊNCIA' )          WHERE complemento_solucao like '%EXIGÃŠNCIA%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'ELETRÃ”NICO', 'ELETRÔNICO' )        WHERE complemento_solucao like '%ELETRÃ”NICO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'SÃ“CIO', 'SÓCIO' )                  WHERE complemento_solucao like '%SÃ“CIO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'TÃ‰CNICO', 'TÉCNICO' )              WHERE complemento_solucao like '%TÃ‰CNICO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'CONSTITUIÃ‡ÃƒO', 'CONSTITUIÇÃO' )   WHERE complemento_solucao like '%CONSTITUIÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'DÃšVIDAS', 'DÚVIDAS' )              WHERE complemento_solucao like '%DÃšVIDAS%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'CORREÃ‡ÃƒO', 'CORREÇÃO' )           WHERE complemento_solucao like '%CORREÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'RECLAMAÃ‡Ã•ES', 'RECLAMAÇÕES' )     WHERE complemento_solucao like '%RECLAMAÃ‡Ã•ES%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'Ã“RGÃƒOS', 'ÓRGÃOS' )               WHERE complemento_solucao like '%Ã“RGÃƒOS%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'TRANSFERÃŠNCIA', 'TRANSFERÊNCIA' )  WHERE complemento_solucao like '%TRANSFERÃŠNCIA%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'CARTÃ“RIO', 'CARTÓRIO' )            WHERE complemento_solucao like '%CARTÃ“RIO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'TRANSFORMAÃ‡ÃƒO', 'TRANSFORMAÇÃO' ) WHERE complemento_solucao like '%TRANSFORMAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'JURÃDICA', 'JURÍDICA' )            WHERE complemento_solucao like '%JURÃDICA%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'DOCUMENTAÃ‡ÃƒO', 'DOCUMENTAÇÃO' )   WHERE complemento_solucao like '%DOCUMENTAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'NECESSÃRIA', 'NECESSÁRIA' )        WHERE complemento_solucao like '%NECESSÃRIA%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'ORIENTAÃ‡ÃƒO', 'ORIENTAÇÃO' )       WHERE complemento_solucao like '%ORIENTAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'LIGAÃ‡ÃƒO', 'LIGAÇÃO' )             WHERE complemento_solucao like '%LIGAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'EMPRESÃRIO', 'EMPRESÁRIO' )        WHERE complemento_solucao like '%EMPRESÃRIO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'ESCRITÃ“RIO', 'ESCRITÓRIO' )        WHERE complemento_solucao like '%ESCRITÃ“RIO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'EXTINÃ‡ÃƒO', 'EXTINÇÃO' )           WHERE complemento_solucao like '%EXTINÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_solucao = replace( complemento_solucao, 'SAÃDA', 'SAÍDA' )                  WHERE complemento_solucao like '%SAÃDA%';

                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'NÃƒO', 'NÃO' )                      WHERE complemento_problema like '%NÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'ALTEARÃ‡ÃƒO', 'ALTERAÇÃO' )         WHERE complemento_problema like '%ALTEARÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'ALTERAÃ‡ÃƒO', 'ALTERAÇÃO' )         WHERE complemento_problema like '%ALTERAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'BALANÃ‡O', 'BALANÇO' )              WHERE complemento_problema like '%BALANÃ‡O%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'CERTIDÃ•ES', 'CERTIDÕES' )          WHERE complemento_problema like '%CERTIDÃ•ES%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'DIÃRIO', 'DÁRIO' )                 WHERE complemento_problema like '%DIÃRIO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'EXIGÃŠNCIA', 'EXIGÊNCIA' )          WHERE complemento_problema like '%EXIGÃŠNCIA%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'ELETRÃ”NICO', 'ELETRÔNICO' )        WHERE complemento_problema like '%ELETRÃ”NICO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'SÃ“CIO', 'SÓCIO' )                  WHERE complemento_problema like '%SÃ“CIO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'TÃ‰CNICO', 'TÉCNICO' )              WHERE complemento_problema like '%TÃ‰CNICO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'CONSTITUIÃ‡ÃƒO', 'CONSTITUIÇÃO' )   WHERE complemento_problema like '%CONSTITUIÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'DÃšVIDAS', 'DÚVIDAS' )              WHERE complemento_problema like '%DÃšVIDAS%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'CORREÃ‡ÃƒO', 'CORREÇÃO' )           WHERE complemento_problema like '%CORREÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'RECLAMAÃ‡Ã•ES', 'RECLAMAÇÕES' )     WHERE complemento_problema like '%RECLAMAÃ‡Ã•ES%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'Ã“RGÃƒOS', 'ÓRGÃOS' )               WHERE complemento_problema like '%Ã“RGÃƒOS%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'TRANSFERÃŠNCIA', 'TRANSFERÊNCIA' )  WHERE complemento_problema like '%TRANSFERÃŠNCIA%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'CARTÃ“RIO', 'CARTÓRIO' )            WHERE complemento_problema like '%CARTÃ“RIO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'TRANSFORMAÃ‡ÃƒO', 'TRANSFORMAÇÃO' ) WHERE complemento_problema like '%TRANSFORMAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'JURÃDICA', 'JURÍDICA' )            WHERE complemento_problema like '%JURÃDICA%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'DOCUMENTAÃ‡ÃƒO', 'DOCUMENTAÇÃO' )   WHERE complemento_problema like '%DOCUMENTAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'NECESSÃRIA', 'NECESSÁRIA' )        WHERE complemento_problema like '%NECESSÃRIA%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'ORIENTAÃ‡ÃƒO', 'ORIENTAÇÃO' )       WHERE complemento_problema like '%ORIENTAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'LIGAÃ‡ÃƒO', 'LIGAÇÃO' )             WHERE complemento_problema like '%LIGAÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'EMPRESÃRIO', 'EMPRESÁRIO' )        WHERE complemento_problema like '%EMPRESÃRIO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'ESCRITÃ“RIO', 'ESCRITÓRIO' )        WHERE complemento_problema like '%ESCRITÃ“RIO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'EXTINÃ‡ÃƒO', 'EXTINÇÃO' )           WHERE complemento_problema like '%EXTINÃ‡ÃƒO%';
                        UPDATE jucese_callcenter.chamadas SET complemento_problema = replace( complemento_problema, 'SAÃDA', 'SAÍDA' )                  WHERE complemento_problema like '%SAÃDA%';


                $arrayPalavraErrada   = array( 'ALTERAÃ‡ÃƒO', 'ALTEARÃ‡ÃƒO', 'BALANÃ‡O', 'CARTÃ“RIO', 'CERTIDÃ•ES', 'CONSTITUIÃ‡ÃƒO', 'CORREÃ‡ÃƒO', 'DOCUMENTAÃ‡ÃƒO', 'DIÃRIO', 'DÃšVIDAS', 'ELETRÃ”NICO', 'EMPRESÃRIO', 'ESCRITÃ“RIO', 'EXIGÃŠNCIA', 'EXTINÃ‡ÃƒO', 'JURÃDICA', 'LIGAÃ‡ÃƒO', 'NECESSÃRIA', 'NÃƒO', 'ORIENTAÃ‡ÃƒO', 'RECLAMAÃ‡Ã•ES', 'SAÃDA', 'SÃ“CIO', 'TRANSFERÃŠNCIA', 'TRANSFORMAÃ‡ÃƒO', 'TÃ‰CNICO', 'Ã“RGÃƒOS' );
                $arrayPalavraCorreta  = array( 'ALTERAÇÃO',   'ALTERAÇÃO',   'BALANÇO',  'CARTÓRIO',  'CERTIDÕES',  'CONSTITUIÇÃO',   'CORREÇÃO',   'DOCUMENTAÇÃO',   'DÁRIO',   'DÚVIDAS',  'ELETRÔNICO',  'EMPRESÁRIO',   'ESCRITÓRIO',  'EXIGÊNCIA',  'EXTINÇÃO',   'JURÍDICA',  'LIGAÇÃO',   'NECESSÁRIA',   'NÃO', 'ORIENTAÇÃO',   'RECLAMAÇÕES',   'SAÍDA',   'SÓCIO',  'TRANSFERÊNCIA',  'TRANSFORMAÇÃO',   'TÉCNICO',  'ÓRGÃOS' );

                $arrayTabelaCampo = array( [ 'jucese_callcenter.tipoproblemas' => 'descricao', 
                                        'jucese_callcenter.tiposolucaos' => 'descricao', 
                                        'jucese_callcenter.tipocidadaos' => 'descricao', 
                                        'jucese_callcenter.tipoatendimentos' => 'descricao', 
                                        'jucese_callcenter.cidadaos' => 'nome', 
                                        'jucese_callcenter.chamadas' => 'complemento_problema', 
                                        'jucese_callcenter.chamadas' => 'complemento_solucao'
                                        ] );

                        foreach ($arrayTabelaCampo as $keyItem => $itemValue) 
                        { 
                                foreach ($arrayPalavraCorreta as $keyItem => $itemValue) 
                                { 
                                echo 'UPDATE ' . $arrayTabelaCampo[ $keyItem ] . ' SET ' . $arrayTabelaCampo[ $itemValue ] . ' = replace( ' . $arrayPalavraErrada[ indice da palavra correta ] . ', ' . $arrayPalavraCorreta[ $itemValue ];
                                $individuals[ $keyItem ]->correcao = retiraLetras( $individuals[ $keyItem ]->palavra ); 
                                }
                        }



                montar as palavras e gerar um unico texto contento todos updates


               -- UPDATE jucese_callcenter.chamadas SET user_id=60  WHERE user_id=6;


-- /////////////////////////////////////////////////////

        -- Query's
                SELECT cpf, count(*)
                FROM chamadas chamadas
                LEFT JOIN cidadaos cidadaos ON chamadas.cidadao_id = cidadaos.id
                GROUP BY cpf
                ORDER BY 2 DESC;

        -- Comentários

                SELECT chamadas.user_id, users.name, count(*) 
                FROM jucese_callcenter.chamadas chamadas
                LEFT JOIN autenticacao.users users ON chamadas.user_id = users.id
                GROUP BY chamadas.user_id, users.name;



                // Query para registros duplicados em determinado campo:
                select campo,campo1,count(*) from tabela group by campo,campo1 having count(*) > 1;
                select callcenter.clientes.cpf_cliente, count(*) from callcenter.clientes GROUP BY callcenter.clientes.cpf_cliente having count(*) > 1;
                SELECT * FROM callcenter.clientes WHERE cpf_cliente = '89186214500';
                SELECT nome_cliente, email_cliente, cpf_cliente, celular_cliente_01, celular_cliente_02, id_tipocidadao FROM callcenter.clientes GROUP BY cpf_cliente HAVING count(*) < 2;

                // SELECT id_nivel, count(*) FROM `chamada` group by id_nivel    => conflitos nos nivel / gruposuser 5 e 6

                // SELECT chamadas.user_id, users.name, count(*) FROM jucese_callcenter.chamadas as chamadas LEFT JOIN autenticacao.users as users ON chamadas.user_id = users.id group by chamadas.user_id, users.name;
                // SELECT chamada.id_usuario, usuario.nome_usuario, count(*) FROM callcenter.chamada as chamada LEFT JOIN callcenter.usuario as usuario ON chamada.id_usuario = usuario.id_usuario group by chamada.id_usuario, usuario.nome_usuario order by usuario.nome_usuario;
                // SELECT chamada.id_nivel, nivel.nome_nivel, count(*) FROM callcenter.chamada as chamada LEFT JOIN callcenter.nivel as nivel ON chamada.id_nivel = nivel.id_nivel group by chamada.id_nivel, nivel.nome_nivel order by nivel.nome_nivel;

                // Niveis: SELECT id_nivel, nome_nivel FROM callcenter.nivel UNION SELECT id, nome FROM autenticacao.gruposuser;
                // Migrar callcenter.nivel para autenticacao.gruposuser: 

                // Usuarios: SELECT id_usuario, nome_usuario FROM callcenter.usuario UNION SELECT id, name FROM autenticacao.users;
                // Migrar callcenter.usuario para autenticacao.users: 
                // INSERT INTO autenticacao.users(id, name, email, created_at, updated_at, ativo, grupoid, password ) 
                                        // SELECT id_usuario, nome_usuario, email, data_criacao, data_modificacao, id_situacao, id_nivel, senha  FROM callcenter.usuario;

                                // antigos
                                INSERT INTO `usuario` (`id_usuario`, `nome_usuario`, `email`, `senha`, `id_situacao`, `data_criacao`, `data_modificacao`, `id_orgao`, `id_nivel`, `id_usuario_logado`) VALUES
                                (2, 'Colaborador AB', 'colaborador@admin.com.br', '202cb962ac59075b964b07152d234b70', 1, '2016-02-14 00:00:04', '2018-08-31 16:34:41', 1, 2, 6),
                                (3, 'Cliente ABCD', 'cliente@admin.com.br', 'e10adc3949ba59abbe56e057f20f883e', 1, '2016-02-14 00:00:03', '2018-09-14 13:22:17', 1, 3, 6),
                                (5, 'Andrius Prado Silva', 'andrius.prado@jucese.se.gov.br', '66e609da2bdf52266a494ea26b59744f', 1, '2018-08-30 14:54:55', NULL, 1, 1, 6),
                                (6, 'Ricardo S. Lima', 'rsljunior@infonet.com.br', 'e218530b03765449feefdfb51fa0ed15', 1, '2019-02-28 11:16:17', NULL, 1, 1, 6),
                                (7, 'Eduardo Silveira Garcez', 'eduardo.garcez@jucese.se.gov.br', 'eedd143d93bd419b2df94e56d51336c7', 1, '2019-04-03 10:31:03', '2019-08-20 07:29:34', 1, 9, 6),
                                (8, 'Ricardo Lima', 'ricardo.sodre@jucese.se.gov.br', 'd006992d059d128c54b265ff5b2b837d', 1, '2019-04-03 10:33:16', '2019-07-03 07:45:00', 1, 11, 6),
                                (9, 'Marcos Macieira', 'marcosvinicius.macieira@jucese.se.gov.br', 'e10adc3949ba59abbe56e057f20f883e', 2, '2019-04-04 07:11:51', '2019-06-14 09:17:10', 1, 4, 6),
                                (10, 'Carlos Francisco', 'carlosfrancisco.neto@jucese.se.gov.br', '09aab0c9991ff75fd096bedea4a861ab', 2, '2019-04-05 09:58:44', '2020-02-13 12:21:48', 1, 4, 6),
                                (11, 'Jane Eurides', 'jane.santos@jucese.se.gov.br', 'ef60fc8e2d3602e1d01c75324dba5e8d', 1, '2019-04-05 09:59:37', '2020-06-15 11:20:27', 1, 4, 6),
                                (12, 'Tatianne Santos Melo', 'tatianne.melo@jucese.se.gov.br', 'e10adc3949ba59abbe56e057f20f883e', 2, '2019-04-05 09:59:37', '2020-02-17 09:29:17', 1, 5, 6),
                                (13, 'Higor Feliphe Santana Nunes', 'higor.nunes@jucese.se.gov.br', 'e7698405bff04018d090d4b948a8d3e5', 1, '2019-05-27 09:07:06', '2019-08-20 07:29:01', 1, 4, 6),
                                (14, 'Marco Freitas', 'marco.freitas@jucese.se.gov.br', 'be82b4be69fce6dad624a4d5459030f2', 1, '2019-05-28 09:37:14', '2019-06-04 06:44:59', 1, 7, 6),
                                (15, 'Paulo AragÃ£o', 'paulo.aragao@jucese.se.gov.br', 'ab45b991e8c7ce88653fb1bee8fb0c2c', 1, '2019-06-13 08:46:56', '2020-05-21 13:36:44', 1, 1, 6),
                                (16, 'Rodrigo Guedes', 'rodrigo.guedes@jucese.se.gov.br', '728e3d53e650d9cfa568a962a713342a', 2, '2019-06-26 07:08:20', '2020-05-26 09:18:53', 1, 11, 6),
                                (17, 'Giovanna Pereira Silva', 'giovanna.pereira@jucese.se.gov.br', '03141b63498738d3dfd86991f66855b1', 2, '2019-08-14 07:34:19', '2020-02-17 09:29:06', 1, 6, 5),
                                (18, 'Cristina Maria Andrade de Melo', 'cristina.melo@jucese.se.gov.br', 'ccc3f259d642341939ec12db5c16de6c', 1, '2019-08-19 12:18:17', '2019-08-19 12:18:17', 1, 7, 5),
                                (19, 'Adryele Santos de Santana', 'adryele.santos@jucese.se.gov.br', 'e10adc3949ba59abbe56e057f20f883e', 1, '2019-08-20 07:33:34', '2020-06-30 08:27:23', 1, 4, 5),
                                (20, 'Mateus Henrique', 'mateushr12@gmail.com', '7e1bb2196d9a2f6d38021612874a68e9', 1, '2019-09-13 08:00:26', '2019-09-13 08:00:26', 1, 1, 5),
                                (21, 'Joana ConceiÃ§Ã£o', 'joana.conceicao@jucese.se.gov.br', '725820cb1f8659b920ed34203afbc160', 1, '2019-11-06 11:44:39', '2020-06-30 08:26:43', 1, 4, 5),
                                (22, 'Luana Dias', 'luana.dias@jucese.se.gov.br', 'e2fcaa4e6a47f9a2bb1893d592b4ab1b', 1, '2019-11-06 11:47:34', '2020-02-19 08:31:45', 1, 4, 5),
                                (23, 'Luana Ribeiro', 'luana.ribeiro@jucese.se.gov.br', '5f74f428fb76b01263d13fb4ed300920', 2, '2019-11-06 12:04:17', '2020-02-04 08:26:29', 1, 4, 5),
                                (24, 'Regina Celly', 'regina.celly@jucese.se.gov.br', 'e10adc3949ba59abbe56e057f20f883e', 1, '2020-02-04 08:25:36', '2020-02-04 08:25:36', 1, 4, 5),
                                (25, 'Patricia Enedina', 'patricia.amado@jucese.se.gov.br', '54d134c54ffdf56c2206f2edef85242f', 1, '2020-02-17 10:01:40', '2020-06-30 08:26:58', 1, 4, 5),
                                (26, 'Silvia Margarida Farias', 'silvia.farias@jucese.se.gov.br', 'a9684711b154ab1d06c566e125d6b5fb', 1, '2020-02-17 10:08:30', '2020-02-17 10:08:30', 1, 4, 5),
                                (27, 'Diego Rios Satiro de Moraes ', 'diego.rios@jucese.se.gov.br', '03141b63498738d3dfd86991f66855b1', 1, '2020-06-30 09:06:06', '2020-06-30 09:09:15', 1, 1, 5),
                                (28, 'EMILY REIS', 'emily.reis@jucese.se.gov.br', '688cd1500d40c7b4e89675852f23ac13', 1, '2020-08-18 10:18:05', '2020-08-18 10:19:57', 1, 4, 15);






                // Chamadas



